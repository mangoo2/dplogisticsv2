-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 13-12-2022 a las 23:42:50
-- Versión del servidor: 5.7.26
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `coecsamx_badm`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilio_origen`
--

DROP TABLE IF EXISTS `domicilio_origen`;
CREATE TABLE IF NOT EXISTS `domicilio_origen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dpl` varchar(50) NOT NULL,
  `calle` varchar(255) NOT NULL,
  `num_ext` varchar(50) NOT NULL,
  `num_int` varchar(50) NOT NULL,
  `colonia` varchar(100) NOT NULL,
  `localidad` varchar(100) NOT NULL,
  `referencia` varchar(255) NOT NULL,
  `municipio` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `codigo_postal` varchar(20) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `domicilio_origen`
--

INSERT INTO `domicilio_origen` (`id`, `dpl`, `calle`, `num_ext`, `num_int`, `colonia`, `localidad`, `referencia`, `municipio`, `estado`, `pais`, `codigo_postal`, `activo`, `reg`) VALUES
(1, '01032016021201', 'aaaa', '3123', '333', '5313', '420', 'Demo', '432', '21', '151', '68101', 1, '2022-12-02 05:06:11'),
(2, 'OAX-APTO', '', '', '', '', '', '', '', 'OAXACA', 'MEXICO', '68101', 1, '2022-12-02 05:06:11'),
(3, 'PBC', '', '', '', '', '', '', '', 'PUEBLA', 'MEXICO', '74160', 1, '2022-12-02 05:06:11'),
(4, 'MID', '', '', '', '', '', '', '', 'YUCATAN', 'MEXICO', '97291', 1, '2022-12-02 05:06:11'),
(5, 'VSA IJC', '', '', '', '', '', '', '', 'TABASCO', 'MEXICO', '86280', 1, '2022-12-02 05:06:11'),
(6, 'VER', '', '', '', '', '', '', '', 'VERACRUZ', 'MEXICO', '91698', 1, '2022-12-02 05:06:11'),
(7, 'HUX', '', '', '', '', '', '', '', 'OAXACA', 'MEXICO', '70980', 1, '2022-12-02 05:06:11'),
(8, 'OAX-CTR', '', '', '', '', '', '', '', 'OAXACA', 'MEXICO', '68000', 1, '2022-12-02 05:06:11'),
(9, '01032016021201', '', '', '', '', '', '', '', 'PUEBLA', 'MEXICO', '72740', 1, '2022-12-02 05:06:11'),
(10, '01032016020339', 's', 's', 's', '', '', '', '', '', '', 's', 1, '2022-12-06 10:11:03');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
