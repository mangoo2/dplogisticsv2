-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 13-12-2022 a las 23:41:08
-- Versión del servidor: 5.7.26
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `coecsamx_badm`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transporte_federal`
--

DROP TABLE IF EXISTS `transporte_federal`;
CREATE TABLE IF NOT EXISTS `transporte_federal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dpl` varchar(50) NOT NULL,
  `nombre_aseguradora` varchar(255) NOT NULL,
  `num_poliza_seguro` varchar(100) NOT NULL,
  `num_permiso_sct` varchar(100) NOT NULL,
  `configuracion_vhicular` varchar(255) NOT NULL,
  `placa_vehiculo_motor` varchar(50) NOT NULL,
  `anio_modelo_vihiculo_motor` varchar(50) NOT NULL,
  `subtipo_remolque` varchar(100) NOT NULL,
  `placa_remolque` varchar(50) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` datetime NOT NULL,
  `tipo_permiso_sct` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `transporte_federal`
--

INSERT INTO `transporte_federal` (`id`, `dpl`, `nombre_aseguradora`, `num_poliza_seguro`, `num_permiso_sct`, `configuracion_vhicular`, `placa_vehiculo_motor`, `anio_modelo_vihiculo_motor`, `subtipo_remolque`, `placa_remolque`, `activo`, `reg`, `tipo_permiso_sct`) VALUES
(1, '01032016020129', 'Grupo Mexicano de Seguros, S.A. de C.V.,', 'campos libres', 'TPAF03', 'VL', 'dasd', 'dsf', 'CTR001', 'fsddfdsdds', 1, '2022-12-02 06:13:14', ''),
(2, 'OAX-APTO', 'Grupo Mexicano de Seguros, S.A. de C.V.,', 'campos libres', '', '', '', '', '', '', 1, '2022-12-02 06:13:14', ''),
(3, 'PBC', 'Grupo Mexicano de Seguros, S.A. de C.V.,', 'campos libres', '', '', '', '', '', '', 1, '2022-12-02 06:13:14', ''),
(4, 'MID', 'Grupo Mexicano de Seguros, S.A. de C.V.,', 'campos libres', '', '', '', '', '', '', 1, '2022-12-02 06:13:14', ''),
(5, 'VSA IJC', 'Grupo Mexicano de Seguros, S.A. de C.V.,', 'campos libres', '', '', '', '', '', '', 1, '2022-12-02 06:13:14', ''),
(6, 'VER', 'Grupo Mexicano de Seguros, S.A. de C.V.,', 'campos libres', '', '', '', '', '', '', 1, '2022-12-02 06:13:14', ''),
(7, 'HUX', 'Grupo Mexicano de Seguros, S.A. de C.V.,', 'campos libres', '', '', '', '', '', '', 1, '2022-12-02 06:13:14', ''),
(8, 'OAX-CTR', 'Grupo Nacional Provincial S.A.B.', 'campos libres', '', '', '', '', '', '', 1, '2022-12-02 06:13:14', ''),
(9, 'PBC-CTR', 'Grupo Nacional Provincial S.A.B.', 'campos libres', '', '', '', '', 'CTR015', '', 1, '2022-12-02 06:13:14', ''),
(10, '01032016020129', 's', 's', 'aaaaa', 'C2R2', 'sss', '100', 'CTR013', 's', 1, '2022-12-06 12:42:23', 'TPAF04');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
