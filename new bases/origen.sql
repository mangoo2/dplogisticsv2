-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 14-12-2022 a las 17:16:15
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `coecsamx_badm`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `origen`
--

DROP TABLE IF EXISTS `origen`;
CREATE TABLE IF NOT EXISTS `origen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dpl` varchar(50) NOT NULL,
  `idorigen` varchar(100) NOT NULL,
  `num_estacion` varchar(50) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  `reg` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `origen`
--

INSERT INTO `origen` (`id`, `dpl`, `idorigen`, `num_estacion`, `activo`, `reg`) VALUES
(1, '21122015201552', 'OR111111', 'EA0452', 1, '2022-12-02 04:08:10'),
(2, '01032016021201', 'OR222222', 'EA0455', 0, '2022-12-02 04:08:10'),
(3, '01032016021201', 'OR222222', 'EA0455', 1, '2022-12-02 04:08:10'),
(4, '03102016221305', 'OR333333', 'EA0481', 1, '2022-12-02 04:08:10'),
(5, '01032016030923', 'OR444444', 'EA0470', 1, '2022-12-02 04:08:10'),
(6, '03102016223621', 'OR555555', 'EA0479', 1, '2022-12-02 04:08:10'),
(7, '14032017162326', 'OR666666', 'EA0450', 1, '2022-12-02 04:08:10'),
(8, '21122015201552', 'OR777777', 'EA45.2', 1, '2022-12-02 04:08:10'),
(9, '01032016021201', 'OR888888', 'EA0455', 1, '2022-12-02 04:08:10');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
