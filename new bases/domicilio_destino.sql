-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 13-12-2022 a las 23:42:44
-- Versión del servidor: 5.7.26
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `coecsamx_badm`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilio_destino`
--

DROP TABLE IF EXISTS `domicilio_destino`;
CREATE TABLE IF NOT EXISTS `domicilio_destino` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dpl` varchar(50) NOT NULL,
  `calle` varchar(255) NOT NULL,
  `num_ext` varchar(50) NOT NULL,
  `num_int` varchar(50) NOT NULL,
  `colonia` varchar(100) NOT NULL,
  `localidad` varchar(100) NOT NULL,
  `referencia` varchar(255) NOT NULL,
  `municipio` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `pais` varchar(100) NOT NULL,
  `codigo_postal` varchar(50) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `domicilio_destino`
--

INSERT INTO `domicilio_destino` (`id`, `dpl`, `calle`, `num_ext`, `num_int`, `colonia`, `localidad`, `referencia`, `municipio`, `estado`, `pais`, `codigo_postal`, `activo`, `reg`) VALUES
(1, '14032017163733', '34', '44', '333', '57376', '68', 'xzcx', '67', '4', '151', '21600', 1, '2022-12-02 02:08:08'),
(2, 'MXL', '', '', '', '', '', '', '', 'BAJA CALIFORNIA', 'MEXICO', '21600', 1, '2022-12-02 02:08:08'),
(3, 'TIJ', '', '', '', '', '', '', '', 'BAJA CALIFORNIA', 'MEXICO', '22435', 1, '2022-12-02 02:08:08'),
(4, 'LAP', '', '', '', '', '', '', '', 'BAJA CALIFORNIA', 'MEXICO', '23206', 1, '2022-12-02 02:08:08'),
(5, 'SJD', '', '', '', '', '', '', '', 'BAJA CALIFORNIA', 'MEXICO', '23420', 1, '2022-12-02 02:08:08'),
(6, 'TAP', '', '', '', '', '', '', '', 'CHIAPAS', 'MEXICO', '30830', 1, '2022-12-02 02:08:08'),
(7, 'TGZ', '', '', '', '', '', '', '', 'CHIAPAS', 'MEXICO', '29176', 1, '2022-12-02 02:08:08'),
(8, 'CJS', '', '', '', '', '', '', '', 'CHIHUAHUA', 'MEXICO', '32698', 1, '2022-12-02 02:08:08'),
(9, 'CUU', '', '', '', '', '', '', '', 'CHIHUAHUA', 'MEXICO', '31390', 1, '2022-12-02 02:08:08'),
(10, 'MEX', '', '', '', '', '', '', '', 'CIUDAD DE MEXICO', 'MEXICO', '15620', 1, '2022-12-02 02:08:08'),
(11, 'TRC', '', '', '', '', '', '', '', 'COAHUILA', 'MEXICO', '27016', 1, '2022-12-02 02:08:08'),
(12, 'BJX', '', '', '', '', '', '', '', 'GUANAJUATO', 'MEXICO', '36270', 1, '2022-12-02 02:08:08'),
(13, 'GDL', '', '', '', '', '', '', '', 'JALISCO', 'MEXICO', '45659', 1, '2022-12-02 02:08:08'),
(14, 'PVR', '', '', '', '', '', '', '', 'JALISCO', 'MEXICO', '48311', 1, '2022-12-02 02:08:08'),
(15, 'MTY', '', '', '', '', '', '', '', 'NUEVO LEON', 'MEXICO', '66600', 1, '2022-12-02 02:08:08'),
(16, 'HUX', '', '', '', '', '', '', '', 'HUATULCO', 'MEXICO', '70980', 1, '2022-12-02 02:08:08'),
(17, 'OAX', '', '', '', '', '', '', '', 'OAXACA', 'MEXICO', '68101', 1, '2022-12-02 02:08:08'),
(18, 'PBC', '', '', '', '', '', '', '', 'PUEBLA', 'MEXICO', '74160', 1, '2022-12-02 02:08:08'),
(19, 'QRO', '', '', '', '', '', '', '', 'QUINTANA ROO', 'MEXICO', '22500', 1, '2022-12-02 02:08:08'),
(20, 'CTM', '', '', '', '', '', '', '', 'QUINTANA ROO', 'MEXICO', '77049', 1, '2022-12-02 02:08:08'),
(21, 'CUN', '', '', '', '', '', '', '', 'QUINTANA ROO', 'MEXICO', '77500', 1, '2022-12-02 02:08:08'),
(22, 'MZT', '', '', '', '', '', '', '', 'SINALOA', 'MEXICO', '82269', 1, '2022-12-02 02:08:08'),
(23, 'HMO', '', '', '', '', '', '', '', 'SONORA', 'MEXICO', '83220', 1, '2022-12-02 02:08:08'),
(24, 'VSA', '', '', '', '', '', '', '', 'VILLAHERMOSA', 'MEXICO', '86280', 1, '2022-12-02 02:08:08'),
(25, 'VER', '', '', '', '', '', '', '', 'VERACRUZ', 'MEXICO', '91698', 1, '2022-12-02 02:08:08'),
(26, 'MID', '', '', '', '', '', '', '', 'MERIDA', 'MEXICO', '97291', 1, '2022-12-02 02:08:08'),
(27, 'CZM', '', '', '', '', '', '', '', 'QUINTANA ROO', 'MEXICO', '77600', 1, '2022-12-02 02:08:08'),
(28, 'zx', 'zXx', 'xZX', 'sad', '', '420', 'aaa', '1739', '21', '151', '4234', 1, '2022-12-05 17:02:45');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
