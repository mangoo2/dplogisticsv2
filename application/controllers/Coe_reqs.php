<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Coe_reqs extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloReqs');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['resultsuc']=$this->ModeloCatalogos->genSelect('coe_sucs');
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_reqs/lista',$data);
        $this->load->view('theme/footer'); 
  	}
    function req($codigo){
        $data['codigo']=$codigo;
        if($codigo=='xxx'){
            $codigo=$this->session->userdata('sess_suc');
            $data['codigo']=$codigo;
            $data['botonatras']=0;
        }else{
            $data['botonatras']=1;
        }
        $data['sess_suc']=$this->sess_suc;
        $resultsuc=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$codigo));
        $data['resultsuc']=$resultsuc->row();

        $data['result_req_proceso']=$this->ModeloReqs->list_reqs_proceso($codigo);
        $data['result_req_ultimas']=$this->ModeloReqs->list_reqs_ultimas($codigo);



        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_reqs/listasuc',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('theme/script_datatable');
        $this->load->view('coe_reqs/listasucjs');
    }
    function cons($codigo){
        $data['codigo']=$codigo;
        if($codigo=='xxx'){
            $codigo=$this->session->userdata('sess_suc');
            $data['codigo']=$codigo;
            $data['botonatras']=0;
        }else{
            $data['botonatras']=1;
        }
        $data['sess_suc']=$this->sess_suc;
        $resultsuc=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$codigo));
        $data['resultsuc']=$resultsuc->row();
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_reqs/listasucsuc',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('theme/script_datatable');
        $this->load->view('coe_reqs/listasucsucjs');
    }
    function insertupdate(){
        $params =$this->input->post();
        $rid = $params['rid'];
        unset($params['rid']);
        if($rid>0){
            $this->ModeloCatalogos->updateCatalogo('suc_reqs',$params,array('rid'=>$rid));
        }else{
            $params['req_estatus']='Solicitado';
            $params['req_fecini']=$this->fechalarga;
            $params['req_fecfin']='000-00-00 00:00:00';
            $params['req_carres']='';
            $this->ModeloCatalogos->Insert('suc_reqs',$params);
        }
    }
    function delete(){
        $params = $this->input->post();
        $rid = $params['rid'];
        $this->ModeloCatalogos->getdeletewheren('suc_reqs',array('rid'=>$rid));
    }
    function update(){
        $params =$this->input->post();
        $rid = $params['rid'];
        unset($params['rid']);
        if($rid>0){
            if($params['req_estatus']=='Finalizado' || $params['req_estatus']=='Cancelado'){
                $params['req_fecfin']=date('Y-m-d H:i:s');
            }else{
                $params['req_fecfin']='0000-00-00 00:00:00';
            }
            $this->ModeloCatalogos->updateCatalogo('suc_reqs',$params,array('rid'=>$rid));
        }
    }
   

 





}