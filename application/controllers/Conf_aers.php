<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Conf_aers extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['resultlist']=$this->ModeloCatalogos->getselectwheren('coe_aers',array('rid >'=>0));
        $data['resultlistempre']=$this->ModeloCatalogos->getselectwheren('coe_emps',array('rid >'=>0));
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('conf_aers/lista',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('conf_aers/listajs');
 
  	}

    function inserupdate(){
        $rid = $_POST['rid'];
        $emp = $_POST['emp'];
        $aer_nom = $_POST['aer_nom'];
        $aer_cla = $_POST['aer_cla'];
        $aer_pos = $_POST['aer_pos'];


        $config['upload_path']          = FCPATH.'_files/_coe/aers/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 5000;
        $config['file_name']=date('YmdGis').'_'.rand(0, 99);       
        $output = [];
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('aer_img')){
            $data = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($data));
            if($rid>0){
                
                $this->ModeloCatalogos->updateCatalogo('coe_aers',array('emp'=>$emp,'aer_cla'=>$aer_cla,'aer_nom'=>$aer_nom,'aer_pos'=>$aer_pos),array('rid'=>$rid));
            }else{
                $this->ModeloCatalogos->Insert('coe_aers',array('emp'=>$emp,'aero'=>$this->fechal,'aer_cla'=>$aer_cla,'aer_nom'=>$aer_nom,'aer_pos'=>$aer_pos));
            }
                        
        }else{
             $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension

            //$this->ModeloCatalogos->updateCatalogo('cli_clis',array('cli_ioimg'=>$file_name),array('rid'=>$rid));
            if($rid>0){
                $this->ModeloCatalogos->updateCatalogo('coe_aers',array('emp'=>$emp,'aer_cla'=>$aer_cla,'aer_nom'=>$aer_nom,'aer_pos'=>$aer_pos,'aer_img'=>'aers/'.$file_name),array('rid'=>$rid));
            }else{
                
                $this->ModeloCatalogos->Insert('coe_aers',array('emp'=>$emp,'aero'=>$this->fechal,'aer_cla'=>$aer_cla,'aer_nom'=>$aer_nom,'aer_pos'=>$aer_pos,'aer_img'=>'aers/'.$file_name));
            }

            $data = array('upload_data' => $this->upload->data());
            //$output = [];
            log_message('error', json_encode($data));
        }
        echo json_encode($output);
    }
    function delete(){
        $params = $this->input->post();
        $rid=$params['rid'];
        $this->ModeloCatalogos->getdeletewheren('coe_aers',array('rid'=>$rid));
    }

}