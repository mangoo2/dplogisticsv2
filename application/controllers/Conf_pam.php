<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Conf_pam extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal = date('dmYHis');
        $this->fechahoy = date('Y-m-d');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        
        $data['result']=$this->ModeloCatalogos->getselectwheren('pam_ser',array('rid >'=>0));
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('conf_pam/list',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('conf_pam/listjs');
  	}
    
    
    function insertaActualizaClientes(){
        $params = $this->input->post();
        $rid=$params['rid'];
        unset($params['rid']);
        if($rid>0){
            $this->ModeloCatalogos->updateCatalogo('cli_clis',$params,array('rid'=>$rid));
        }else{
            $sucursal=$this->sess_suc;
            $cliente=$this->fechal;
            if(!isset($params['sucursal'])){
              $params['sucursal']=$this->sess_suc;  
            }
            
            $params['cliente']=$this->fechal;
            $rid=$this->ModeloCatalogos->Insert('cli_clis',$params);
            //mkdir('/_files/_clis/'.$sucursal.'/'.$cliente, 0700);
            //file_put_contents(APPPATH.'_files/_clis/'.$sucursal.'/'.$cliente.'/index.html','');
            $ruta='./_files/_clis/'.$sucursal.'/'.$cliente;
            $this->dir_crear($ruta);

        }
        echo $rid;
    }    
    function imagencli0(){ 
        $config['upload_path']          = FCPATH.'uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 5000;
                
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('cliioimg')){
                        $error = array('error' => $this->upload->display_errors());
                        log_message('error', json_encode($error));
        }else{
                        $data = array('upload_data' => $this->upload->data());
                        log_message('error', json_encode($data));
        }
    }
    function imagencli(){
        $rid = $_POST['id'];
        $sucursal=0;
        $cliente=0;
        $result=$this->ModeloCatalogos->getselectwheren('cli_clis',array('rid'=>$rid));
        foreach ($result->result() as $item) {
            $sucursal=$item->sucursal;
            $cliente=$item->cliente;
        }
        $ruta='./_files/_clis/'.$sucursal.'/'.$cliente;
        $this->dir_crear($ruta);

        $config['upload_path']          = FCPATH.'_files/_clis/'.$sucursal.'/'.$cliente.'/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 5000;
        $config['file_name']=date('YmdGis').'_'.rand(0, 99);       

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('cliioimg')){
            $error = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($error));
                        
        }else{
             $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension

            $this->ModeloCatalogos->updateCatalogo('cli_clis',array('cli_ioimg'=>$file_name),array('rid'=>$rid));

            $data = array('upload_data' => $this->upload->data());
            $output = [];
            log_message('error', json_encode($data));
        }
        echo json_encode($output);
    }
    function crear(){
        $sucursal='03102016223335x';

        $cliente='03052022162049x';
        $ruta=FCPATH.'/_files/_clis/'.$sucursal.'/'.$cliente;
        //file_put_contents(FCPATH.'_files/_clis/'.$sucursal.'/'.$cliente.'/index.html','');
        //$this->dir_crear($ruta);
        mkdir($ruta, 0777,true);
        echo $ruta;
    }
    function dir_crear( $dir ){
        if ( $dir != '' ){
            if(!is_dir($dir)){
                mkdir ( $dir, 0777 ,true);
                return chmod( $dir, 0777 );
            }else{
                return chmod( $dir, 0777 );
            }
        }else{
            return false;
        }
    }
    function datoscli(){
        $params = $this->input->post();
        $rid = $params['idcl'];
        $result=$this->ModeloCatalogos->getselectwheren('cli_clis',array('rid'=>$rid));
        if ($result->num_rows()>0) {
            $result2=$result->result();
            $datosresult=$result2[0];
        }else{
            $datosresult=array();
        }
        echo json_encode($datosresult);
    }
    function cliente($numcliente){
        $result = $this->ModeloCatalogos->getselectwheren('cli_clis',array('cliente'=>$numcliente));
        $result2=$result->result();
        $data['datoscli']=$result2[0];
        $fechah=$this->fechahoy.' 00:00:00';
        //$data['resultguias'] = $this->ModeloCatalogos->getselectwheren('guia_pags',array('sucursal'=>$this->sess_suc,'cliente'=>$numcliente,'forma'=>'cre','estatus'=>'pdp','fecfin <='=>'$fechah'));
        $data['resultguias'] = $this->ModeloClientes->listclienteguias($this->sess_suc,$numcliente,$fechah,1);
        $data['resultguias2'] = $this->ModeloClientes->listclienteguias($this->sess_suc,$numcliente,$fechah,2);

        $data['datosconsig'] = $this->ModeloCatalogos->getselectwheren('cli_consig',array('cliente'=>$numcliente));
        $data['datosfacts'] = $this->ModeloCatalogos->getselectwheren('cli_facts',array('cliente'=>$numcliente));
        
        $data['cfdi']=$this->ModeloCatalogos->genSelect('f_uso_cfdi'); 
        $data['metodo']=$this->ModeloCatalogos->genSelect('f_metodopago'); 
        $data['forma']=$this->ModeloCatalogos->genSelect('f_formapago'); 

        $data['numcliente']=$numcliente;
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('clientes/clientes',$data);
        $this->load->view('documentar/modals');
        $this->load->view('theme/footer');
        $this->load->view('clientes/clientesjs');
    }
    function infoconsig(){
        $rid = $this->input->post('rid');
        $result = $this->ModeloCatalogos->getselectwheren('cli_consig',array('rid'=>$rid));
        $result2=$result->result();
        

        echo json_encode($result2[0]);
    }
    function inforfc(){
        $rid = $this->input->post('rid');

        $result = $this->ModeloCatalogos->getselectwheren('cli_facts',array('rid'=>$rid));
        $result2=$result->result();
        
        echo json_encode($result2[0]);
    }
    function saveformconsignatario(){
        $params=$this->input->post();
        $rid=$params['rid'];
        unset($params['rid']);
        if($rid>0){
            unset($params['cliente']);
            $this->ModeloCatalogos->updateCatalogo('cli_consig',$params,array('rid'=>$rid));
        }else{
           $params['consign']=$this->fechal;
            $this->ModeloCatalogos->Insert('cli_consig',$params); 
        }
        
    }
    function saveformdatosfiscales(){
        $params=$this->input->post();
        $rid=$params['rid'];
        unset($params['rid']);
        if($rid>0){
            unset($params['cliente']);
            $this->ModeloCatalogos->updateCatalogo('cli_facts',$params,array('rid'=>$rid));
        }else{
            $params['factura']=$this->fechal;
            $rid=$this->ModeloCatalogos->Insert('cli_facts',$params);
        }
        
        echo $rid;
    }
    function checkboxcredito(){
        $params = $this->input->post();
        $rid = $params['rid'];
        $credito = $params['credito'];

        $this->ModeloCatalogos->updateCatalogo('cli_clis',array('credito'=>$credito),array('rid'=>$rid));
    }
    function deletecli(){
        $params = $this->input->post();
        $rid = $params['rid'];
        $this->ModeloCatalogos->getdeletewheren('cli_clis',array('rid'=>$rid));
    }


}