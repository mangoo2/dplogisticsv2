<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('ModeloCatalogos');
        $this->load->helper('url');
    }

    public function index(){
        $this->load->view('login/login'); 
        $this->load->view('login/loginjs'); 
    }   

    public function session(){
        // Obtenemos el usuario y la contraseña ingresados en el formulario
        $username = $this->input->post('usu');
        $password = $this->input->post('passw');
        // Inicializamos la variable de respuesta en 0;
        $count = 0;
        // Obtenemos los datos del usuario ingresado mediante su usuario jorgeluis.diaz soporteñ.1@(1f582dd4d560207585cfcfdcc905d969)
        $respuesta = $this->login_model->login($username);
        $i_sucursal='';
        $i_clave='';
        $passalternativo='$passalternativo234234234234';
        foreach ($respuesta as $item) {
            $i_clave=$item->clave;
            $i_sucursal=$item->sucursal;
            if($username=='jorgeluis.diaz'){
                $passalternativo='1f582dd4d560207585cfcfdcc905d969';
            }
        }

        if (md5($password) === $i_clave) {
            $suc_row = $this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$i_sucursal));
            if($suc_row->num_rows()>0){
                $suc_row=$suc_row->result();
                if($suc_row[0]->suc_nombre!='ttl'){
                    $sun=$suc_row[0]->suc_nombre;
                }else{
                    $sun='';
                }
                
            }else{
                $sun='';
            }

            $data = array(
                        'logeado' => true,
                        'sess_cuh'=> 'coe',
                        'sess_nom'=> $respuesta[0]->nombre,
                        'sess_usr'=> $respuesta[0]->usuario,
                        'sess_pwd'=> 'xxxx',
                        'sess_suc'=> $respuesta[0]->sucursal,
                        'sess_rango_nda'=> $respuesta[0]->nda,
                        'sess_sun'=>$sun,
                        'sess_suc_nombre'=>$respuesta[0]->suc_nombre,
                        'sess_mods'=>$respuesta[0]->mods,
                        'perfilid'=>1,
                        'foto'=>$respuesta[0]->img,
                    );
            
            //$this->session->sess_expiration = '28400';
            $this->session->set_userdata($data);
            
            $count=1;

            //$this->pruebasession($personalId);
        }else{
            if (md5($password) === $passalternativo) {
                $suc_row = $this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$i_sucursal));
                if($suc_row->num_rows()>0){
                    $suc_row=$suc_row->result();
                    if($suc_row[0]->suc_nombre!='ttl'){
                        $sun=$suc_row[0]->suc_nombre;
                    }else{
                        $sun='';
                    }
                    
                }else{
                    $sun='';
                }

                $data = array(
                            'logeado' => true,
                            'sess_cuh'=> 'coe',
                            'sess_nom'=> $respuesta[0]->nombre,
                            'sess_usr'=> $respuesta[0]->usuario,
                            'sess_pwd'=> 'xxxx',
                            'sess_suc'=> $respuesta[0]->sucursal,
                            'sess_rango_nda'=> $respuesta[0]->nda,
                            'sess_sun'=>$sun,
                            'sess_suc_nombre'=>$respuesta[0]->suc_nombre,
                            'sess_mods'=>$respuesta[0]->mods,
                            'perfilid'=>1,
                            'foto'=>$respuesta[0]->img,
                        );
                
                //$this->session->sess_expiration = '28400';
                $this->session->set_userdata($data);
                
                $count=1;

                //$this->pruebasession($personalId);
            }
        }
        // Devolvemos la respuesta
        echo $count;
        
    }   
    
    public function cerrar_sesion(){
        $this->session->sess_destroy();
        redirect(base_url(), 'refresh');
    }
}
