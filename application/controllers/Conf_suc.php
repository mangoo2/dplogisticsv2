<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Conf_suc extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['sess_sucname']=$this->sess_sucname;
        $data['resultlist']=$this->ModeloCatalogos->getlis_conf_suc();
        $data['listrut']=$this->ModeloCatalogos->getselectwheren_orderby('coe_ruts',array('rid >'=>0),'rut_cla','ASC');
        $data['listusu']=$this->ModeloCatalogos->getselectwheren_orderby('jsi_sis_admins',array('usuario !='=>''),'nombre','ASC');
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('conf_suc/lista',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('conf_suc/listajs');
  	}
    function fols($codigo){
        $data['codigo']=$codigo;
        $resultaero=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$codigo));
        $data['resultsuc']=$resultaero->row();
        $data['lisfolis']=$this->ModeloCatalogos->getselectwheren_orderby('coe_fols',array('sucursal'=>$codigo),'fol_est','ASC');
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('conf_suc/fols',$data);
        $this->load->view('theme/footer');
        $this->load->view('conf_suc/folsjs');
    }
    function fols_add(){
        $params = $this->input->post();
        $sucursal=$params['sucursal'];
        $folio=$params['folio'];

        $resultc=$this->ModeloCatalogos->getselectwheren('coe_fols',array('folio'=>$folio));
        if($resultc->num_rows()==0){
            $this->ModeloCatalogos->updateCatalogo('coe_fols',array('fol_est'=>'des'),array('sucursal'=>$sucursal,'fol_est'=>'act'));
            $this->ModeloCatalogos->Insert('coe_fols',array('sucursal'=>$sucursal,'folio'=>$folio,'fol_num'=>0,'fol_est'=>'act'));
        }else{
            $estatus=1;
        }


    }

    function inserupdate(){
        $params = $this->input->post();
        $rid = $params['rid'];
        unset($params['rid']);
        if($rid>0){
            
                $this->ModeloCatalogos->updateCatalogo('coe_sucs',$params,array('rid'=>$rid));
        }else{
                $params['sucursal']=$this->fechal;
                $params['suc_pos']=20;
                $params['tarifas']='';
                $this->ModeloCatalogos->Insert('coe_sucs',$params);
                $this->creadirectorios($this->fechal);
        }
    }
    /*
    function delete(){
        $params = $this->input->post();
        $rid=$params['rid'];
        $this->ModeloCatalogos->getdeletewheren('coe_clas',array('rid'=>$rid));
    }*/
    function creadirectorios($sucursal){
        $DIR_SUC=FCPATH.'_files/_suc/'.$sucursal;
        if (!is_dir($DIR_SUC) && strlen($DIR_SUC)>0){mkdir($DIR_SUC, 0755);} // Sucursal
        if (!is_dir($DIR_SUC.'/dep') && strlen($DIR_SUC.'/dep')>0){mkdir($DIR_SUC.'/dep', 0755);} // Depositos
        if (!is_dir($DIR_SUC.'/pre') && strlen($DIR_SUC.'/pre')>0){mkdir($DIR_SUC.'/pre', 0755);} // Presupuestos
        if (!is_dir($DIR_SUC.'/egr') && strlen($DIR_SUC.'/egr')>0){mkdir($DIR_SUC.'/egr', 0755);} // Gastos
        if (!is_dir($DIR_SUC.'/pag') && strlen($DIR_SUC.'/pag')>0){mkdir($DIR_SUC.'/pag', 0755);} // Pagos
        if (!is_dir($DIR_SUC.'/pam') && strlen($DIR_SUC.'/pam')>0){mkdir($DIR_SUC.'/pam', 0755);} // Pagos
        if (!is_dir($DIR_SUC.'/req') && strlen($DIR_SUC.'/req')>0){mkdir($DIR_SUC.'/req', 0755);} // Requisiciones
        if (!is_dir($DIR_SUC.'/gas') && strlen($DIR_SUC.'/gas')>0){mkdir($DIR_SUC.'/gas', 0755);} // Requisiciones
    }

}