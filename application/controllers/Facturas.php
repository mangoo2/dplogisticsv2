<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Facturas extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('Modelofacturas');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal = date('dmYHis');
        $this->load->library('zip');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['mot_can']= $this->ModeloCatalogos->getselectwheren_o2('motivos_cancelacion',array('activo'=>1));
        $data['sucursal_row']=$this->ModeloCatalogos->getselectwheren_o2('sucursal',array('activo'=>1)); 
        $data['cfdi']=$this->ModeloCatalogos->genSelect('f_uso_cfdi'); 

        $infousuario=$this->ModeloCatalogos->getselectwheren_o2('jsi_sis_admins',array('usuario'=>$this->sess_usr)); 
        $fac=0;
        foreach ($infousuario->result() as $item) {
            $fac=$item->fac;
            // code...
        }
        $data['fac']=$fac;
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('facturacion/list',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('facturacion/listjs');
  	}
    public function getlista() {
        $params = $this->input->post();
        $params['sucursal']=$this->sess_suc;
        $getdata = $this->Modelofacturas->getfacturas($params);
        $totaldata= $this->Modelofacturas->total_facturas($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function add($id=0){
        $data['sess_suc']=$this->sess_suc;
        $data['cfdi']=$this->ModeloCatalogos->genSelect('f_uso_cfdi'); 
        $data['metodo']=$this->ModeloCatalogos->genSelect('f_metodopago'); 
        $data['forma']=$this->ModeloCatalogos->genSelect('f_formapago'); 
        $data['fmoneda']=$this->ModeloCatalogos->getselectwheren_o2('f_c_moneda',array('activo'=>1));
        $data['sucursal_row']=$this->ModeloCatalogos->getselectwheren_o2('sucursal',array('activo'=>1)); 
        if($id==0){
            $id_guia='';
        }else{

        }
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('facturacion/add',$data);
        $this->load->view('facturacion/modales');
        $this->load->view('theme/footer');
        $this->load->view('facturacion/addjs');
    }
    function searchunidadsat(){
        $search = $this->input->get('search');
        $results=$this->Modelofacturas->getseleclikeunidadsat($search);
        echo json_encode($results->result());
    }
    function searchconceptosat(){
        $search = $this->input->get('search');
        $results=$this->Modelofacturas->getseleclikeconceptosa($search);
        echo json_encode($results->result());
    }
    public function searchcli(){
        $usu = $this->input->get('search');
        $results=$this->ModeloCatalogos->getseleclike_cliente2($usu);
        echo json_encode($results->result());
    }
    function obtenerrfc(){
        $id = $this->input->post('id');
        $r=$this->Modelofacturas->getRFC($id);
        echo $r->cli_rfc;
    }
    function facturapdf($FacturasId){
        $data["Nombrerasonsocial"]='Nombrerasonsocial';
        $data['rrfc']='';
        $data['rdireccion']='direccion conocida';
        $data['regimenf']='601 General de personas morales';
        $logotipo = base_url().'public/img/logo.png';

        $data["logotipo"]=$logotipo;
        $factura = $this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$FacturasId));
        foreach ($factura->result() as $item) {
            $MetodoPago=$item->MetodoPago;
            $FormaPago=$item->FormaPago;
            $observaciones=$item->observaciones;
            $Folio=$item->Folio;
            $serie=$item->serie;
            $isr=$item->isr;
            $uuid=$item->uuid;
            $nocertificadosat=$item->nocertificadosat;
            $certificado=$item->nocertificado;
            $fechatimbre=$item->fechatimbre;
            $uso_cfdi=$item->uso_cfdi;
            $cliente=$item->Nombre;
            $clirfc=$item->Rfc;
            $clidireccion=$item->Direccion;
            $data["cp"] = $item->Cp; 
            $data["Lote"] = $item->Lote; 
            $numproveedor=$item->numproveedor;
            $numordencompra=$item->numordencompra;
            $moneda=$item->moneda;
            $subtotal  = $item->subtotal;
            $iva = $item->iva;
            $total = $item->total;
            $ivaretenido = $item->ivaretenido;
            $cedular = $item->cedular;
            $selloemisor = $item->sellocadena;
            $sellosat = $item->sellosat;
            $cadenaoriginal = $item->cadenaoriginal;
            $tarjeta=$item->tarjeta;
            $tipoComprobante=$item->TipoComprobante;

            $Caducidad=$item->Caducidad;
            $Lote=$item->Lote;

            $tipoempresa=$item->tipoempresa;
            if($tipoempresa>0){
                $tipoempresas=$tipoempresa;
            }else{
                $tipoempresas=1;
            }
            $f_relacion = $item->f_relacion;
            $f_r_tipo = $item->f_r_tipo;
            $f_r_uuid = $item->f_r_uuid;
        }
        //==================================================
            $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$tipoempresas));
            $datosconfiguracion=$datosconfiguracion->result();
            $datosconfiguracion=$datosconfiguracion[0];
            $data["Nombrerasonsocial"]=$datosconfiguracion->Nombre;
            $data['rrfc']=$datosconfiguracion->Rfc;
            $data['rdireccion']=$datosconfiguracion->Calle.' No '.$datosconfiguracion->Noexterior.' '.$datosconfiguracion->Nointerior.' Col: '.$datosconfiguracion->Colonia.' , '.
                                    $datosconfiguracion->Municipio.' , '.$datosconfiguracion->localidad.' , '. $datosconfiguracion->Estado.'. ';
            $data['regimenf']=$this->Modelofacturas->regimenf($datosconfiguracion->Regimen);
        //==================================================
        $facturadetalles = $this->Modelofacturas->facturadetalle($FacturasId);

      //=============
          $data['FormaPago']=$MetodoPago;
          //log_message('error', 'FormaPago c: '.$MetodoPago);
          $data['FormaPagol']=$MetodoPago;
          $resultsfp=$this->ModeloCatalogos->getselectwheren('f_formapago',array('clave'=>$MetodoPago));
          foreach ($resultsfp->result() as $item) {
            $data['FormaPagol']=$item->formapago_text;
          }
          //$data['FormaPagol']='vv';
        //=================
          $data['MetodoPago']=$FormaPago;
          
          $data['MetodoPagol']=$FormaPago;
          //$data['MetodoPagol']='cc';
        //=================
          $data['observaciones']=$observaciones;
          $data['Folio']=$Folio;
          $data['serie']=$serie;
        //=================
          $data["isr"]=$isr;
        //=================
          $data['folio_fiscal']=$uuid;
          $data['nocertificadosat']=$nocertificadosat;
          $data['certificado']=$certificado;
          $data['fechatimbre']=$fechatimbre;
          //$data['cfdi']=$this->gt_uso_cfdi($uso_cfdi);
          $data['cfdi']=$uso_cfdi;
          $data['cliente']=$cliente;
          $data['clirfc']=$clirfc;
          $data['clidireccion']=$clidireccion;
          $data['numproveedor']=$numproveedor;
          $data['numordencompra']=$numordencompra;
          $data['moneda']=$moneda;
          $data['subtotal']=$subtotal;
          $data['iva']=$iva;

          /*$data['isr']=$isr;
          $data['ivaretenido']=$ivaretenido;
          $data['cincoalmillarval']=$cincoalmillarval;
          $data['outsourcing']=$outsourcing;*/

          $data['total']=$total;
          $data['ivaretenido']=$ivaretenido;
          $data['cedular']=$cedular;
          $data['selloemisor']=$selloemisor;
          $data['sellosat']=$sellosat;
          $data['cadenaoriginal']=$cadenaoriginal;
          $data['tarjeta']=$tarjeta;
          $data['facturadetalles']=$facturadetalles->result();
          $data['tipoComprobante']=$tipoComprobante;
          $data['f_relacion']=$f_relacion;
          $data['f_r_tipo']=$f_r_tipo;
          $data['f_r_uuid']=$f_r_uuid;

        $data['idfactura']=$FacturasId;
        $data['Caducidad']      =   $Caducidad;
        $data['Lote']           =   $Lote;
            $data['tipoempresas']=$tipoempresas;
            if(isset($_GET['fac'])){
                $this->load->view('reportes/factura',$data);
            }else{
                $this->load->view('reportes/factura2',$data); 
            }
        
    }

    function excel_factura($f1,$f2,$emp,$suc,$cfdi,$ecomp,$cliente){
        $data['f1']=$f1;
        $data['f2']=$f2;
        $data['result'] = $this->Modelofacturas->get_factura($f1,$f2,$emp,$suc,$cfdi,$ecomp,$cliente);
        $this->load->view('facturacion/excel',$data);
    }

  function search_operador(){
    $search = $this->input->get('search');
    $results = $this->Modelofacturas->get_operador_like($search);
    echo json_encode($results);    
  }

  function search_transporte(){
    $search = $this->input->get('search');
    $results = $this->Modelofacturas->get_transporte_like($search);
    echo json_encode($results);    
  }
    function factura_zip($f1,$f2,$emp,$suc,$cfdi,$ecomp,$cliente){
            $result = $this->Modelofacturas->get_factura($f1,$f2,$emp,$suc,$cfdi,$ecomp,$cliente);
            foreach ($result as $r){
                $this->zip->read_file(FCPATH.'/files_sat/facturaspdf/Factura_'.$r->serie.'_'.$r->Folio.'.pdf');
                $this->zip->read_file(FCPATH.$r->rutaXml);
            }
            $this->zip->download('my_archive.zip');
    }
      function infoviewerror(){
        $factura = $this->input->post('factura');
        $detalle= $this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$factura));
        //$mensajeerror=array();
        foreach ($detalle->result() as $item) {
            $mensajeerror=$item->mensajeerror;
        }
        echo $mensajeerror; 
      }
      function infoviewerrorc(){
        $factura = $this->input->post('factura');
        $detalle= $this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$factura));
        //$mensajeerror=array();
        foreach ($detalle->result() as $item) {
            $mensajeerror=$item->mensajeerror;
        }
        echo $mensajeerror; 
      }
    function listasdecomplementos(){
        $factura=$this->input->post('facturas');
        //$resultado=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('FacturasId'=>$factura,'Estado'=>1));
        $resultado=$this->ModeloCatalogos->complementofacturas($factura);
        $html='<table class="table" id="tableliscomplementos">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Fecha</th>
                        <th>Numero de parcialidad</th>
                        <th>Monto</th>
                        <th>Archivo</th>
                    </tr>
                </thead>
                <tbody>';
        foreach ($resultado->result() as $item) {
            $html.='<tr>
                        <td>'.$item->complementoId.'</td>
                        <td>'.$item->FechaPago.'</td>
                        <td>'.$item->NumParcialidad.'</td>
                        <td>'.$item->ImpPagado.'</td>
                        <td>
                            <a class="btn btn-info mr-1 mb-1 tooltipped" href="'.base_url().'Facturas/complementodoc/'.$item->complementoId.'" target="_blank" data-position="top" data-delay="50" data-tooltip="Factura" title="" data-original-title="Factura"><i class="fa fa-file-pdf-o fa-fw"></i></a>
                            <a class="btn btn-info mr-1 mb-1 tooltipped" href="'.base_url().$item->rutaXml.'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" title="" download="" data-original-title="XML"><i class="fa fa-file-code-o"></i></a>


                            
                        </td>
                    </tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    function complementodoc($id){
        $resultscp=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$id));
        $resultscp=$resultscp->row();
        //===================================
            $idconfig=$resultscp->ConfiguracionesId;
        //===================================
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$idconfig));
        $datosconfiguracion=$datosconfiguracion->row();
        $data['confif']=$datosconfiguracion;
        $data['rfcemisor']=$datosconfiguracion->Rfc;
        $data['nombreemisor']=$datosconfiguracion->Nombre;
        $data['regimenf']=$this->Modelofacturas->regimenf($datosconfiguracion->Regimen);
        if($datosconfiguracion->ConfiguracionesId==1){
          $logotipo = base_url().'public/img/coecsa.jpg';
        }else{
          $logotipo = base_url().'public/img/cadipa.jpg';
        }
        $data["logotipo"]=$logotipo;
        $data['rdireccion']=$datosconfiguracion->Calle.' '.$datosconfiguracion->Municipio.' '.$datosconfiguracion->Estado.' '.$datosconfiguracion->PaisExpedicion.' C.P. '.$datosconfiguracion->CodigoPostal;
        //==========================================
            //$data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion($idconfig); 

        //==========================================
            $data['idcomplemento']=$id;
            
            $data['resultscpd']=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('complementoId'=>$id));
            
            $FacturasId=$resultscp->FacturasId;
            $data['uuid']=$resultscp->uuid;
            $data['nocertificadosat']=$resultscp->nocertificadosat;
            $data['LugarExpedicion']=$resultscp->LugarExpedicion;
            $data['fechatimbre']=$resultscp->fechatimbre; 
            $data['fechapago']=$resultscp->FechaPago;
            $data['monto']=$resultscp->Monto;
            $data['IdDocumento']=$resultscp->IdDocumento;
            $data['Folio']=$resultscp->Folio;
            $data['Serie']=$resultscp->Serie;
            $data['ImpSaldoAnt']=$resultscp->ImpSaldoAnt;
            $data['NumParcialidad']=$resultscp->NumParcialidad;
            $data['ImpSaldoInsoluto']=$resultscp->ImpSaldoInsoluto;
            $data['Sello']=$resultscp->Sello;
            $data['sellosat']=$resultscp->sellosat;
            $data['cadenaoriginal']=$resultscp->cadenaoriginal;
            $data['nocertificadosat']=$resultscp->nocertificadosat;
            $data['rfcreceptor']=$resultscp->R_rfc;
            $data['nombrereceptorr']=$resultscp->R_nombre;
            $data['NumOperacion']=$resultscp->NumOperacion;
            $docrelac = $this->Modelofacturas->documentorelacionado($id);
            $data['docrelac']=$docrelac->result();
        //====================================================================
            $resultsfp=$this->ModeloCatalogos->getselectwheren('f_formapago',array('id'=>$resultscp->FormaDePagoP));
            $resultsfp=$resultsfp->row();
            $data['formapago']=$resultsfp->formapago_text;
        //==========================================
            //$resultsfc=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$FacturasId));
            //$resultsfc=$resultsfc->row();
            /*
            if ($resultsfc->moneda=='pesos') {
                $data['moneda']='Peso Mexicano';
            }else{
                $data['moneda']='Dolar';
            }
            */
        //====================================================================
            //$resultsmp=$this->ModeloCatalogos->getselectwheren('f_metodopago',array('metodopago'=>$resultsfc->FormaPago));
            //$resultsmp=$resultsmp->row();
            //$data['metodopago']=$resultsmp->metodopago_text;
        //==========================================
        $this->load->view('Reportes/complemento',$data);
    }

    function obtenerdatosfactura(){
        $params = $this->input->post();
        $idfactura = $params['factura'];
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
        $datosfacturad=$this->ModeloCatalogos->getselectwheren('f_facturas_servicios',array('FacturasId'=>$idfactura));
        $array = array(
            'facturas'=>$datosfactura->row(),
            'facturasd'=>$datosfacturad->result()
        );
        echo json_encode($array);
    }
    function searchclientes(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getclienterazonsociallike($pro);
        //echo $results;
        echo json_encode($results->result());
    }
    function excel_factura_indi(){
        

        $facturas=json_decode($_GET['facturas']);
        $this->db->select("f.FacturasId,f.serie,f.Folio,f.uuid,DATE_FORMAT(f.fechatimbre, '%d/%m/%Y' ) AS fecha,DATE_FORMAT(f.fechatimbre, '%r' ) AS hora,f.Rfc,f.Estado,fp.formapago,fm.metodopago_text,f.moneda,f.subtotal,f.iva,f.total,f.Nombre,s.NoIdentificacion,ser.nombre AS servicio,f.rutaXml");
        $this->db->from('f_facturas f');
        $this->db->join('f_metodopago fm', 'fm.metodopago=f.FormaPago','left');
        $this->db->join('f_formapago fp', 'fp.clave=f.MetodoPago','left');
        $this->db->join('f_facturas_servicios s', 's.FacturasId=f.FacturasId','left');
        $this->db->join('f_servicios ser', 'ser.clave=s.servicioId','left');
        $this->db->group_start();
        foreach ($facturas as $item) {
            $this->db->or_like('f.FacturasId',$item->FIds);
        }
        $this->db->group_end();  
        $query=$this->db->get();
        $data['result'] = $query->result();
        $this->load->view('facturacion/excel',$data);
    }
    function factura_zip_indi(){
        $facturas=json_decode($_GET['facturas']);
        $this->db->select("f.FacturasId,f.serie,f.Folio,f.uuid,DATE_FORMAT(f.fechatimbre, '%d/%m/%Y' ) AS fecha,DATE_FORMAT(f.fechatimbre, '%r' ) AS hora,f.Rfc,f.Estado,fp.formapago,fm.metodopago_text,f.moneda,f.subtotal,f.iva,f.total,f.Nombre,s.NoIdentificacion,ser.nombre AS servicio,f.rutaXml");
        $this->db->from('f_facturas f');
        $this->db->join('f_metodopago fm', 'fm.metodopago=f.FormaPago','left');
        $this->db->join('f_formapago fp', 'fp.clave=f.MetodoPago','left');
        $this->db->join('f_facturas_servicios s', 's.FacturasId=f.FacturasId','left');
        $this->db->join('f_servicios ser', 'ser.clave=s.servicioId','left');
        $this->db->group_start();
        foreach ($facturas as $item) {
            $this->db->or_like('f.FacturasId',$item->FIds);
        }
        $this->db->group_end();  
        $query=$this->db->get();
        $result = $query->result();

        foreach ($result as $r){
            $this->zip->read_file(FCPATH.'/files_sat/facturaspdf/Factura_'.$r->serie.'_'.$r->Folio.'.pdf');
            $this->zip->read_file(FCPATH.$r->rutaXml);
        }
        $this->zip->download('my_archive.zip');
    }


}