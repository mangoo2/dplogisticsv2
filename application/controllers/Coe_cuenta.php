<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Coe_cuenta extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');

            $this->sess_nom =  $this->session->userdata('sess_nom');

        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['resultsuc']=$this->ModeloCatalogos->genSelect('coe_sucs');
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_cuenta/lista',$data);
        $this->load->view('theme/footer');
        
  	}
    function suc($codigo){
        $data['codigo']=$codigo;
        if($codigo=='xxx'){
            $codigo=$this->session->userdata('sess_suc');
            $data['codigo']=$codigo;
            $data['botonatras']=0;
        }else{
            $data['botonatras']=1;
        }
        $data['sess_suc']=$this->sess_suc;
        $resultsuc=$this->ModeloCatalogos->getselectwheren_o3('coe_sucs',array('sucursal'=>$codigo));
        $data['resultsuc']=$resultsuc->row();
        $data['resC']=$this->ModeloCatalogos->getselectwherenlimirorderby('suc_cors',array('sucursal'=>$codigo),'cor_fecha','DESC',60);
        $data['resD']=$this->ModeloCatalogos->getselectwherenlimirorderby('suc_deps',array('sucursal'=>$codigo),'dep_fecha','DESC',20);

        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_cuenta/listasuc',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('theme/script_datatable');
        $this->load->view('coe_cuenta/listasucjs');
    }
    function cor_dep(){
        $params = $this->input->post();
        $rid=$params['rid'];

        $resC=$this->ModeloCatalogos->getselectwheren('suc_cors',array('rid'=>$rid,'cor_est'=>'pen'));
        if($resC->num_rows()>0){
            $resC_row=$resC->row();
            $suc=$resC_row->sucursal;
            $importe=$resC_row->cor_efectivo;
            $pag_total=0;
            $resC=$this->ModeloCatalogos->getselectwheren('suc_deps',array('sucursal'=>$suc,'dep_est'=>'vrf','dep_saldo >'=>'0'));
            foreach ($resC->result() as $item) {
                $pag_total+=$item->dep_saldo;
            }
            $corimp = $importe;
            $pre_pres = '';
            if($importe <= $pag_total){
                $err='';

                for($x=0;$corimp>0;$x++){
                    if($x<20){
                        //list($pid,$pago,$pag_saldo) = $__BASE->registro($__BASE->consulta("SELECT rid,deposito,dep_saldo FROM ".SUC_DEP." WHERE sucursal='$suc' AND dep_est='vrf' AND dep_saldo > 0 ORDER BY rid ASC LIMIT 1"));

                        $ressd=$this->ModeloCatalogos->getselectwheren('suc_deps',array('sucursal'=>$suc,'dep_saldo >'=>0));
                        $ressd=$ressd->row();
                        $pid = $ressd->rid;
                        $pago = $ressd->deposito;
                        $pag_saldo = $ressd->dep_saldo;

                        if($corimp <= $pag_saldo){
                            $pag_salact = $pag_saldo - $corimp;
                            if($pre_pres!=''){
                                $pre_pres .= '|'.$pago;
                            }else{
                                $pre_pres = $pago;
                            }
                            $corimp = 0;
                            $this->ModeloCatalogos->updateCatalogo('suc_deps',array('dep_saldo'=>$pag_salact),array('rid'=>$pid,'sucursal'=>$suc));
                            //$__BASE->consulta("UPDATE ".SUC_DEP." SET dep_saldo='$pag_salact' WHERE rid='$pid' AND sucursal='$suc' LIMIT 1");
                        }else{
                            $pag_salact = 0;
                            if($pre_pres!=''){
                                $pre_pres .= '|'.$pago;
                            }else{
                                $pre_pres = $pago;
                            }
                            $corimp -= $pag_saldo;
                            $this->ModeloCatalogos->updateCatalogo('suc_deps',array('dep_saldo'=>$pag_salact),array('rid'=>$pid,'sucursal'=>$suc));
                            //$__BASE->consulta("UPDATE ".SUC_DEP." SET dep_saldo='$pag_salact' WHERE rid='$pid' AND sucursal='$suc' LIMIT 1");
                        }
                    }else{
                        $err='Error de Importe y Saldo...';
                    }
                }
                
                if($err==''){
                    $fin_coms='Finalizado: '.date('Y-m-d H:i:s').', Admin:'.$this->sess_usr.', Depositos: '.$pre_pres;
                    //$__BASE->consulta("UPDATE ".TSUC_COR." SET cor_est='fin', fin_coms='$fin_coms' WHERE rid='$rid' AND sucursal='$suc' AND cor_est='pen' LIMIT 1");
                    $this->ModeloCatalogos->updateCatalogo('suc_cors',array('cor_est'=>'fin','fin_coms'=>$fin_coms),array('rid'=>$rid,'sucursal'=>$suc,'cor_est'=>'pen'));
                    //$resp->op = 'reload';
                }else{
                    //$resp->error=true;
                    //$resp->msj = $err;
                }
                
            }
        }
    }
    function dep_gua(){
        $params = $this->input->post();
        $dep_verifico = $params['dep_verifico'];
        $dep_obs = $params['dep_obs'];
        $rid = $params['rid'];
        $suc = $this->sess_suc;
        $obs = 'Admin:'.$this->sess_usr.', Verifico:'.$dep_verifico.', Fecha:'.date('d/m/Y H:i:s').', Obs:'.$dep_obs;

        $this->ModeloCatalogos->updateCatalogo('suc_deps',array('dep_est'=>'vrf','dep_verifico'=>$dep_verifico,'dep_obs'=>$obs),array('rid'=>$rid));
    }
    function dep_can(){
        $params = $this->input->post();
        $rid = $params['rid'];
        $this->ModeloCatalogos->updateCatalogo('suc_deps',array('dep_est'=>'can'),array('rid'=>$rid));
    }
    function inserupdate(){
        $rid = $_POST['rid'];
        $sucursal = $_POST['sucursall'];
        $dep_importe = $_POST['dep_importe'];
        $dep_forma = $_POST['dep_forma'];
        $dep_concepto = $_POST['dep_concepto'];
        $dep_fecha = $_POST['dep_fecha'];
        $dep_deposito = $this->sess_nom;
        if(isset($_POST['sucursall'])){
            
        }else{
            $sucursal=$this->sess_suc;
        }
        log_message('error', 'sucursal:'.$sucursal);

        $DIR_SUC0=FCPATH.'_files/_suc/'.$sucursal.'';
        $DIR_SUC=FCPATH.'_files/_suc/'.$sucursal.'/dep';
        if (!is_dir($DIR_SUC) && strlen($DIR_SUC)>0){
            log_message('error',$DIR_SUC);
            //mkdir($DIR_SUC, 0755);
            if(!is_dir($DIR_SUC0)){
                mkdir($DIR_SUC0, 0755);
                mkdir($DIR_SUC, 0755);
            }
        }else{
            file_put_contents($DIR_SUC.'/index.html', '');
        }
    

        $config['upload_path']          = $DIR_SUC;
        $config['allowed_types']        = '*';
        $config['max_size']             = 10000;
        $config['file_name']=date('YmdGis').'_'.rand(0, 99);       
        $output = [];
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('dep_comprobante')){
            $data = array('error' => $this->upload->display_errors());
            log_message('error', 'Error en la carga '.json_encode($data));
            if($rid>0){
                
                $this->ModeloCatalogos->updateCatalogo('suc_deps',array('dep_importe'=>$dep_importe,'dep_saldo'=>$dep_importe,'dep_forma'=>$dep_forma,'dep_concepto'=>$dep_concepto,'dep_fecha'=>$dep_fecha),array('rid'=>$rid));
            }else{
                $this->ModeloCatalogos->Insert('suc_deps',array('sucursal'=>$sucursal,'deposito'=>$this->fechal,'dep_est'=>'dep','dep_verifico'=>'','dep_importe'=>$dep_importe,'dep_saldo'=>$dep_importe,'dep_forma'=>$dep_forma,'dep_concepto'=>$dep_concepto,'dep_fecha'=>$dep_fecha,'dep_deposito'=>$dep_deposito,'dep_feccap'=>$this->fechalarga,'dep_tipo'=>'','dep_obs'=>'','admin'=>$this->sess_usr));
            }
                        
        }else{
             $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension

            if($rid>0){
                $this->ModeloCatalogos->updateCatalogo('suc_deps',array('dep_importe'=>$dep_importe,'dep_saldo'=>$dep_importe,'dep_forma'=>$dep_forma,'dep_concepto'=>$dep_concepto,'dep_fecha'=>$dep_fecha,'dep_comprobante'=>$file_name),array('rid'=>$rid));
            }else{
                $this->ModeloCatalogos->Insert('suc_deps',array('sucursal'=>$sucursal,'deposito'=>$this->fechal,'dep_est'=>'dep','dep_verifico'=>'','dep_importe'=>$dep_importe,'dep_saldo'=>$dep_importe,'dep_forma'=>$dep_forma,'dep_concepto'=>$dep_concepto,'dep_fecha'=>$dep_fecha,'dep_deposito'=>$dep_deposito,'dep_feccap'=>$this->fechalarga,'dep_tipo'=>'','dep_obs'=>'','admin'=>$this->sess_usr,'dep_comprobante'=>$file_name));
            }

            $data = array('upload_data' => $this->upload->data());
            //$output = [];
            log_message('error', 'Carga correcta '.json_encode($data));
        }
        echo json_encode($output);
    }
    function deletedeposito(){
        $rid = $this->input->post('rid');
        $this->ModeloCatalogos->getdeletewheren('suc_deps',array('rid'=>$rid,'sucursal'=>$this->sess_suc));
    }





}