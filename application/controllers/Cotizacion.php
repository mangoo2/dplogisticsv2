<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Cotizacion extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloClientes');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal = date('dmYHis');
        $this->fechal_larga = date('Y-m-d H:i:s');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_nom=$this->session->userdata('sess_nom');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        redirect('Sistema');
  	}
    function file($cotizacion){
        $data['cotizacion']=$cotizacion;
        
        
        $doc_cotiza=$this->ModeloCatalogos->getselectwheren('doc_cotiza',array('cotiza'=>$cotizacion));
        $doc_cotiza_n = $doc_cotiza->result();
        //$url=FCPATH.'/docdirectorio/doc_cotiza_x.txt';
        //file_put_contents($url, json_encode($doc_cotiza_n));
        $doc_cotiza_n = $doc_cotiza_n[0];
        $data['folio']=$doc_cotiza_n->cot_folio;
        $data['cot_feccap']=$doc_cotiza_n->cot_feccap;
        $data['cot_guia']=$doc_cotiza_n->cot_guia;
        $data['cot_descrip']=$doc_cotiza_n->cot_descrip;
        $data['usuario']=$doc_cotiza_n->usuario;
        $data['cot_fdp']=$doc_cotiza_n->cot_fdp;
        
        if($doc_cotiza_n->cot_conta!='si'){
            $this->ModeloCatalogos->updateCatalogo('doc_cotiza',array('cot_feccap'=>$this->fechal_larga),array('cotiza'=>$cotizacion));
            
            $doc_cotiza_n->cot_feccap=$this->fechal_larga;

            $this->demasfunciones($cotizacion);
        }
        //============================================
        $result_aero=$this->ModeloCatalogos->getselectwheren('coe_aers',array('aero'=>$doc_cotiza_n->aero));
        $result_aero = $result_aero->result();
        $result_aero = $result_aero[0];
        if($result_aero->rid==1){
            $data['titleimge']='coecsa_2.jpeg';
            $data['title1']='CDMX | OAXACA CENTRO | OAXACA AEROPUERTO | HUATULCO | MÉRIDA | VILLAHERMOSA | VERACRUZ | PUEBLA AEROPUERTO | PUEBLA CENTRO';
            $data['title2']='Cotización llama o enviá whatsapp al: 951 218 4644 | ¡Queremos clientes felices! Quejas o Sugerencias: 951 228 4251 ó 951 228 7316 | Aviso de privacidad www.coecsa.mx';
            $data['observaciong']='EL REMITENTE FIRMANTE DE ESTA GUÍA ACEPTA LOS TÉRMINOS Y CONDICIONES Y POLÍTICAS DE ENVÍO Y SERVICIO ESTIPULADAS AL REVERSO DE ESTA GUIA.';
            $data['filecontrato']='contrato_coecsa.pdf';
        }
        if($result_aero->rid==2){
            $data['titleimge']='cadipa.jpg';
            $data['title1']='CDMX | OAXACA CENTRO | OAXACA AEROPUERTO | PUEBLA AEROPUERTO | PUEBLA CENTRO';
            $data['title2']='Cotización llama o enviá whatsapp al: 951 218 4644 | ¡Queremos clientes felices! Quejas o Sugerencias: 951 228 4251 ó 951 228 7316 | Aviso de privacidad www.cadipalogistics.com';
            $data['observaciong']='EL REMITENTE FIRMANTE DE ESTA GUÍA ACEPTA LOS TÉRMINOS Y CONDICIONES Y POLÍTICAS DE ENVÍO Y SERVICIO ESTIPULADAS AL REVERSO DE ESTA GUIA.';
            $data['filecontrato']='contrato_cadipa.pdf';
        }
        $data['aerolinea']=$result_aero->aer_nom;
        //==============================================
            $result_rutao=$this->ModeloCatalogos->getselectwheren('coe_ruts',array('ruta'=>$doc_cotiza_n->cot_rutori));
            $result_rutao = $result_rutao->result();
            $result_rutao = $result_rutao[0];

            $data['rutaorigen']=$result_rutao->rut_cla;
        //==============================================
            $result_rutad=$this->ModeloCatalogos->getselectwheren('coe_ruts',array('ruta'=>$doc_cotiza_n->cot_rutdes));
            $result_rutad = $result_rutad->result();
            $result_rutad = $result_rutad[0];

            $data['rutadestino']=$result_rutad->rut_cla;
        //==============================================
            $result_remitente=$this->ModeloCatalogos->getselectwheren('cli_clis',array('cliente'=>$doc_cotiza_n->cot_remite));
            $remitente='';
            foreach ($result_remitente->result() as $item) {
                if($item->cli_telefono==''){
                    $tel_cli=$item->cli_celular;
                }else{
                    $tel_cli=$item->cli_telefono;
                }
                $remitente='NOM.:'.$item->cli_nombre.' '.$item->cli_paterno.' '.$item->cli_materno.'<br>';
                $remitente.='DIR.:'.$item->cli_calle.' '.$item->cli_numext.' '.$item->cli_numint.'<br>';
                $remitente.='COL.:'.$item->cli_colonia.'      <br>C.P.:'.$item->cli_cp.'<br>';
                $remitente.='CIU.:'.$item->cli_municipio.' / '.$item->cli_entidad.'         <br>TEL.:'.$tel_cli.'<br>';
            }
            $data['remitente']=$remitente;

        //==============================================
            $result_consign=$this->ModeloCatalogos->getselectwheren('cli_consig',array('consign'=>$doc_cotiza_n->cot_consign));
            $consign='x';
            foreach ($result_consign->result() as $item) {
                if($item->con_celular!=''){
                    $tel_con=$item->con_celular;
                }else{
                    $tel_con=$item->con_telefono;
                }
                $consign='NOM.:'.$item->con_nombre.' '.$item->con_paterno.' '.$item->con_materno.'<br>';
                $consign.='DIR.:'.$item->con_calle.' '.$item->con_numext.' '.$item->con_numint.'<br>';
                $consign.='COL.:'.$item->con_colonia.'      <br>C.P.:'.$item->con_cp.'<br>';
                $consign.='CIU.:'.$item->con_municipio.' / '.$item->con_entidad.'         <br>TEL.:'.$tel_con.'<br>';
            }
            $data['consign']=$consign;
        //==============================================
            $result_class=$this->ModeloCatalogos->getselectwheren('coe_clas',array('clasif'=>$doc_cotiza_n->cot_clasif));
            $result_class = $result_class->result();
            $result_class = $result_class[0];

            $data['clasificacion']=$result_class->cla_nom;
        //===============================================================================
            $data['result_paquetes']=$this->ModeloCatalogos->getselectwheren('doc_paqs',array('cotiza'=>$cotizacion));
        //================================================================================
            $doc_tars=$this->ModeloCatalogos->getselectwheren('doc_tars',array('cotiza'=>$cotizacion));
            $doc_tars = $doc_tars->result();
            $data['doc_tars'] = $doc_tars[0];
        //============================================
        $data['doc_cotiza_n']=$doc_cotiza_n;
        $data['file']='file1';
        $this->load->view('reportes/cotizacion',$data);
    }
    function file2($cotizacion){
        $data['cotizacion']=$cotizacion;
        
        
        $doc_cotiza=$this->ModeloCatalogos->getselectwheren('guias',array('rid'=>$cotizacion));
        $doc_cotiza_n = $doc_cotiza->result();
        //$url=FCPATH.'/docdirectorio/doc_cotiza_x.txt';
        //file_put_contents($url, json_encode($doc_cotiza_n));
        $doc_cotiza_n = $doc_cotiza_n[0];
        $data['folio']=$doc_cotiza_n->folio;
        $data['cot_feccap']=$doc_cotiza_n->fecha;
        $data['cot_guia']=$doc_cotiza_n->guia;
        //$data['cot_descrip']=$doc_cotiza_n->cot_descrip;
        $data['cot_descrip']='';
        $data['usuario']=$doc_cotiza_n->admin;
        //============================================
        $result_aero=$this->ModeloCatalogos->getselectwheren('coe_aers',array('aero'=>$doc_cotiza_n->aero));
        $result_aero = $result_aero->result();
        $result_aero = $result_aero[0];
        if($result_aero->rid==1){
            $data['titleimge']='coecsa.jpg';
            $data['title1']='OAXACA CENTRO | OAXACA AEROPUERTO | HUATULCO | MÉRIDA | VILLAHERMOSA | VERACRUZ | MINATITLÁN | PUEBLA AEROPUERTO | PUEBLA CENTRO | TUXTLA';
            $data['title2']='Calzada de la República 210, centro, Oaxaca, Oax. CP 68000 | Quejas y sugerencias: Tel: 01(951)5137661 / (222) 2905774 | Aviso de privacidad www.coecsa.mx';
            $data['observaciong']='El remitente al sagnar esta guiá aprueba y acepta las políticas de seguridad de nuestra empresa y la aerolínea aérea asume completamente responsabilidad sobre el contenido enviado y hace constar que este no viola ninguna disposición oficial, así mismo acepta los cargos cobrados en la presente y de requerir factura acepta haberla solicitado antes de la impresión de la presente, sabiendo que la recibirá en un lapso máximo de 7 días hábiles vía email en caso de ser cliente con cuenta de crédito acepta que la presente podrá utilizarse como garantía de pago para actos de cobranza o procesos judiciales, así mismo deslinda de toda responsabilidad a esta empresa en caso de daño de su mercancía o muerte de animal por causas ajenas a nuestra operaciones y personal, de existir alguna observación sobre el envió de destino enviado deberá aceptar su reclamación en la guiá impresa en el momento de recepción de lo contratado, exhibe de toda responsabilidad a CORPORACION OAXAQUEÑA DE EXPRESS Y CARGA S.A. DE C.V. y a la aerolínea aérea, sobre nuestras políticas de privacidad revisar nuestra pagina de internet www.coecsa.mx, si esta no es firmada por el documentador reclame su envió como gratuito.';
        }
        if($result_aero->rid==2){
            $data['titleimge']='cadipa.jpg';
            $data['title1']='';
            $data['title2']='Tehuantepec 7° Infonavit El rosario, San Sebastián Tula, Oaxaca, C.P. 71246 RFC. CAD070509Q18 Tel. 2222905774 www.cadipalogistics.com diazcarga@gmail.com';
            $data['observaciong']='Al firmar esta guiá el remitente manifiesta declarar como verdad que el contenido enviado no es de carácter ilícito o ilegal por lo que de no ser cierto acepta toda responsabilidad alguna en caso de una investigación judicial. así mismo acepta el cobro aquí estipulado y deslinda de toda responsabilidad a esta empresa en caso de daño a su mercancía o muerte de su animal por causas ajenas a nuestra operación. Para toda reclamación se sujeta a los procedimientos de la aerolínea cliente';
        }
        $data['aerolinea']=$result_aero->aer_nom;
        //==============================================
            //$result_rutao=$this->ModeloCatalogos->getselectwheren('coe_ruts',array('ruta'=>$doc_cotiza_n->origen));
            //$result_rutao = $result_rutao->result();
            //$result_rutao = $result_rutao[0];

            $data['rutaorigen']=$doc_cotiza_n->origen;
        //==============================================
            //$result_rutad=$this->ModeloCatalogos->getselectwheren('coe_ruts',array('ruta'=>$doc_cotiza_n->cot_rutdes));
            //$result_rutad = $result_rutad->result();
            //$result_rutad = $result_rutad[0];

            $data['rutadestino']=$doc_cotiza_n->destino;
        //==============================================
            //list($rem,$rem_dir,$rem_col,$rem_ciu,$rem_tel,$rem_cp,$con,$con_dir,$con_col,$con_ciu,$con_tel,$con_cp,$con_com)=$__BASE->registro($__BASE->consulta("SELECT rem,rem_dir,rem_col,rem_ciu,rem_tel,rem_cp,con,con_dir,con_col,con_ciu,con_tel,con_cp,com FROM ".TAB_GRYC." WHERE guia='$gid' AND cliente='$cliente' LIMIT 1"));
            $result_remitente=$this->ModeloCatalogos->getselectwheren('guia_rycs',array('guia'=>$cotizacion));
            //$result_remitente=$this->ModeloCatalogos->getselectwheren('cli_clis',array('cliente'=>$doc_cotiza_n->cot_remite));
            $remitente='';
            foreach ($result_remitente->result() as $item) {
                
                $remitente='NOM.:'.$item->rem.'<br>';
                $remitente.='DIR.:'.$item->rem_dir.'<br>';
                $remitente.='COL.:'.$item->rem_col.'      <br>C.P.:'.$item->rem_cp.'<br>';
                $remitente.='CIU.:'.$item->rem_ciu.'         <br>TEL.:'.$item->rem_tel.'<br>';
            }
            $data['remitente']=$remitente;

        //==============================================
            //$result_consign=$this->ModeloCatalogos->getselectwheren('cli_consig',array('consign'=>$doc_cotiza_n->cot_consign));
            $consign='x';
            foreach ($result_remitente->result() as $item) {
               
                $consign='NOM.:'.$item->con.'<br>';
                $consign.='DIR.:'.$item->con_dir.'<br>';
                $consign.='COL.:'.$item->con_col.'      <br>C.P.:'.$item->con_cp.'<br>';
                $consign.='CIU.:'.$item->con_ciu.'         <br>TEL.:'.$item->con_tel.'<br>';
            }
            $data['consign']=$consign;
        //==============================================
            //$result_class=$this->ModeloCatalogos->getselectwheren('coe_clas',array('clasif'=>$doc_cotiza_n->cot_clasif));
            //$result_class = $result_class->result();
            //$result_class = $result_class[0];

            //$data['clasificacion']=$result_class->cla_nom;
            $data['clasificacion']=$doc_cotiza_n->paq_des;
        //===============================================================================
            $data['result_paquetes']=$this->ModeloCatalogos->getselectwheren('guia_paqs',array('guia'=>$cotizacion));
        //================================================================================
            //$doc_tars=$this->ModeloCatalogos->getselectwheren('doc_tars',array('cotiza'=>$cotizacion));
            $doc_tars=$this->ModeloCatalogos->getselectwheren('guia_tars',array('rid'=>$cotizacion));
            $doc_tars = $doc_tars->result();
            $data['doc_tars'] = $doc_tars[0];
        //============================================
        $data['doc_cotiza_n']=$doc_cotiza_n;
        $data['file']='file2';
        $this->load->view('reportes/cotizacion',$data);
    }
    function demasfunciones($cotizacion){
        $info_text='';
        // agregar inserciones updates de documentar/09fdg.php apartir de la linea 56
        $result_doc=$this->ModeloCatalogos->getselectwheren('doc_cotiza',array('cotiza'=>$cotizacion));
        $info_text.='
        doc_cotiza '.json_encode($result_doc->result());
        $cot_folio='';
        $emp='';
        $cot_remite='';
        $cot_factura='';
        $cot_feccap='';
        $aero='';
        $cla_nom='';
        $importet_efe=0;
        $importet_tar=0;
        $sucursal_doc_cot=$this->sess_suc;
        foreach ($result_doc->result() as $item) {
            $rid=$item->rid;
            $cot_folio=$item->cot_folio;
            $gui_rid=$item->cot_guia;
            $cot_remite=$item->cot_remite;
            $cot_factura=$item->cot_factura;
            $cot_feccap=$item->cot_feccap;
            $aero=$item->aero;
            $cot_descrip=$item->cot_descrip;
            $cot_fdp=$item->cot_fdp;
            $tarifa=$item->cot_cos;
            $importet_efe=$item->cot_cantidad;
            $sucursal_doc_cot=$item->sucursal;
            $importet_tar=floatval($item->banco_efectivo)+floatval($item->banco_credito);
            //================================================
            $result_aero=$this->ModeloCatalogos->getselectwheren('coe_aers',array('aero'=>$item->aero));
            $result_aero=$result_aero->result();
            $result_aero=$result_aero[0];
            $emp=$result_aero->aer_nom;
            //================================================
            //================================================
            $result_origen=$this->ModeloCatalogos->getselectwheren('coe_ruts',array('ruta'=>$item->cot_rutori));
            $result_origen=$result_origen->result();
            $result_origen=$result_origen[0];
            $origen=$result_origen->rut_cla;
            //================================================
            //================================================
            $result_destino=$this->ModeloCatalogos->getselectwheren('coe_ruts',array('ruta'=>$item->cot_rutdes));
            $result_destino=$result_destino->result();
            $result_destino=$result_destino[0];
            $destino=$result_destino->rut_cla;
            //================================================
            //================================================
            $result_clasifi=$this->ModeloCatalogos->getselectwheren('coe_clas',array('clasif'=>$item->cot_clasif));
            $result_clasifi=$result_clasifi->result();
            $result_clasifi=$result_clasifi[0];
            $cla_nom=$result_clasifi->cla_nom;
            //================================================
                $remitente='';
                $consign='';
                $remitente_dir='';
                $consign_dir='';
                $remitente_ciu='';
                $consign_ciu='';
            //==============================================
                $result_remitente=$this->ModeloCatalogos->getselectwheren('cli_clis',array('cliente'=>$item->cot_remite));
                $remitente='';
                foreach ($result_remitente->result() as $itemcc) {
                    $remitente=$itemcc->cli_nombre.' '.$itemcc->cli_paterno.' '.$itemcc->cli_materno;
                    $remitente_dir=$itemcc->cli_calle.' '.$itemcc->cli_numext.' '.$itemcc->cli_numint;
                    $remitente_ciu=$itemcc->cli_municipio.' / '.$itemcc->cli_entidad;
                    $rem_col=$itemcc->cli_colonia;
                    //$rem_tel=$itemcc->cli_celular;
                    if($itemcc->cli_telefono==''){
                        $rem_tel=$itemcc->cli_celular;
                    }else{
                        $rem_tel=$itemcc->cli_telefono;
                    }
                    $rem_cp=$itemcc->cli_cp;
                }
                $data['remitente']=$remitente;

            //==============================================
                $result_consign=$this->ModeloCatalogos->getselectwheren('cli_consig',array('consign'=>$item->cot_consign));
                foreach ($result_consign->result() as $itemcg) {
                    $consign=$itemcg->con_nombre.' '.$itemcg->con_paterno.' '.$itemcg->con_materno;
                    $consign_dir=$itemcg->con_calle.' '.$itemcg->con_numext.' '.$itemcg->con_numint;
                    $consign_ciu=$itemcg->con_municipio.' / '.$itemcg->con_entidad;
                    $con_colonia= $itemcg->con_colonia;
                    $con_tel=$itemcg->con_telefono;
                    $con_cp=$itemcg->con_cp;
                }
            //==============================================


        }
        $result_paqs=$this->ModeloCatalogos->getselectwheren('doc_paqs',array('cotiza'=>$cotizacion));
        $info_text.='
        doc_paqs '.json_encode($result_paqs->result());
        $paq_num=0;
        $pesopaq=0;
        $volument=0;
        foreach ($result_paqs->result() as $itemp) {
            $paq_num=$paq_num+$itemp->paq_num;
            //$pesopaq=$pesopaq+($itemp->paq_num*$itemp->paq_pes);
            $pesopaq=$pesopaq+$itemp->paq_pes;
            //$volumen=round(((($itemp->paq_alt*$itemp->paq_anc*$itemp->paq_lar)/6000)),2);
            $volumen=round(((($itemp->paq_alt*$itemp->paq_anc*$itemp->paq_lar)/6000)*$itemp->paq_num),2);
            $volument=$volument+$volumen;
        }
        $result_pagos=$this->ModeloCatalogos->getselectwheren('doc_tars',array('cotiza'=>$cotizacion));
        $info_text.='
        doc_tars '.json_encode($result_pagos->result());
        $importet=0;

        foreach ($result_pagos->result() as $itemt) {
            $importet=$itemt->tar_tot;
        }


        $result_guias=$this->ModeloCatalogos->getselectwheren('guias',array('sucursal'=>$sucursal_doc_cot,'folio'=>$cot_folio));
        if($result_guias->num_rows()<=0){

            $rid_guias=$this->ModeloCatalogos->Insert('guias',array('sucursal'=>$sucursal_doc_cot,'emp'=>$emp,'aero'=>$aero,'folio'=>$cot_folio,'guia'=>$gui_rid,'origen'=>$origen,'destino'=>$destino,'paqs'=>$paq_num,'paq_des'=>$cot_descrip,'peso'=>$pesopaq,'volumen'=>$volument,'tarifa'=>$tarifa,'importe'=>$importet,'fecha'=>$cot_feccap,'admin'=>$this->sess_usr,'adm_nom'=>$this->sess_nom,'corte'=>'','estatus'=>'fin','clasif'=> $cla_nom));
        
            //TAB_GFAC guia_facts
            $this->ModeloCatalogos->Insert('guia_facts',array('sucursal'=>$sucursal_doc_cot,'emp'=>$emp,'guia'=>$rid_guias,'cliente'=>$cot_remite,'factura'=>$cot_factura,'facturainfo'=>'se obtiene del codigo seleccionado de la factura','fechasol'=>$cot_feccap,'fechafin'=>$cot_feccap,'estatus'=>'sol','coms'=>'','pdf'=>'','xml'=>''));
            //TAB_GUIA guias
            $this->ModeloCatalogos->updateCatalogo('guias',array('estatus'=>'pxf'),array('rid'=>$rid_guias));
            if($cot_fdp=='efe'){
                $this->ModeloCatalogos->Insert('guia_pags',array('sucursal'=>$sucursal_doc_cot,'emp'=>$emp,'guia'=>$rid_guias,'cliente'=>$cot_remite,'forma'=>'efe','importe'=>$importet,'bncorigen'=>'COECSA','bncdestino'=>'COECSA','fecini'=>$cot_feccap,'fecfin'=>$cot_feccap,'comp'=>'efectivo','coms'=>'Pagado','estatus'=>'fin','pago'=>'efectivo','pag_fec'=>$cot_feccap,'pag_adm'=>''));
            }
            if($cot_fdp=='cre'){
                $this->ModeloCatalogos->Insert('guia_pags',array('sucursal'=>$sucursal_doc_cot,'emp'=>$emp,'guia'=>$rid_guias,'cliente'=>$cot_remite,'forma'=>'cre','importe'=>$importet,'bncorigen'=>'COECSA','bncdestino'=>'','fecini'=>$cot_feccap,'fecfin'=>$cot_feccap,'comp'=>'Pendiente','coms'=>'Pendiente de pago','estatus'=>'pdp','pago'=>'','pag_fec'=>'0000-00-00 00:00:00','pag_adm'=>''));

                $this->ModeloCatalogos->updateCatalogo('guias',array('estatus'=>'pdg'),array('rid'=>$rid_guias));
                $this->ModeloCatalogos->updateCatalogo('guia_facts',array('estatus'=>'pdg'),array('guia'=>$rid_guias));
            }
            if($cot_fdp=='tar'){
                 $this->ModeloCatalogos->Insert('guia_pags',array('sucursal'=>$sucursal_doc_cot,'emp'=>$emp,'guia'=>$rid_guias,'cliente'=>$cot_remite,'forma'=>'tar','importe'=>$importet,'bncorigen'=>'COECSA','bncdestino'=>'COECSA','fecini'=>$cot_feccap,'fecfin'=>$cot_feccap,'comp'=>'tarjeta','coms'=>'Pagado','estatus'=>'fin','pago'=>'tarjeta','pag_fec'=>$cot_feccap,'pag_adm'=>''));
            }
            if($cot_fdp=='efetar'){
                $this->ModeloCatalogos->Insert('guia_pags',array('sucursal'=>$sucursal_doc_cot,'emp'=>$emp,'guia'=>$rid_guias,'cliente'=>$cot_remite,'forma'=>'efe','importe'=>$importet_efe,'forma'=>'tar','importe'=>$importet_tar,'bncorigen'=>'COECSA','bncdestino'=>'COECSA','fecini'=>$cot_feccap,'fecfin'=>$cot_feccap,'comp'=>'efectivo','coms'=>'Pagado','estatus'=>'fin','pago'=>'efectivo tarjeta','pag_fec'=>$cot_feccap,'pag_adm'=>''));
            }
            foreach ($result_paqs->result() as $itemp) {
                $this->ModeloCatalogos->Insert('guia_paqs',array('guia'=>$rid_guias,'cliente'=>$cot_remite,'paq_lar'=>$itemp->paq_lar,'paq_anc'=>$itemp->paq_anc,'paq_alt'=>$itemp->paq_alt,'paq_num'=>$itemp->paq_num,'paq_pes'=>$itemp->paq_pes));
            }
             

            $this->ModeloCatalogos->Insert('guia_rycs',array('emp'=>$emp,'guia'=>$rid_guias,'cliente'=>$cot_remite,'rem'=>$remitente,'rem_dir'=>$remitente_dir,'rem_col'=>$rem_col,'rem_ciu'=>$remitente_ciu,'rem_tel'=>$rem_tel,'rem_cp'=>$rem_cp,'con'=>$consign,'con_dir'=>$consign_dir,'con_col'=>$con_colonia,'con_ciu'=>$consign_ciu,'con_tel'=>$con_tel,'con_cp'=>$con_cp,'com'=>''));
            foreach ($result_pagos->result() as $itemt) {
                $guia_tars_datos=array(
                                        'guia'=>$rid_guias,
                                        'cliente'=>$cot_remite,
                                        'tar_ce'=>$itemt->tar_ce,
                                        'tar_cen'=>$itemt->tar_cen,
                                        'tar_cf'=>$itemt->tar_cf,
                                        'tar_cfn'=>$itemt->tar_cfn,
                                        'tar_pov'=>$itemt->tar_pov,
                                        'tar_povn'=>$itemt->tar_povn,
                                        'tar_fer'=>$itemt->tar_fer,
                                        'tar_fern'=>$itemt->tar_fern,
                                        'tar_cpc'=>$itemt->tar_cpc,
                                        'tar_cpcn'=>$itemt->tar_cpcn,
                                        'tar_sub'=>$itemt->tar_sub,
                                        'tar_subn'=>$itemt->tar_subn,
                                        'tar_iva'=>$itemt->tar_iva,
                                        'tar_ivan'=>$itemt->tar_ivan,
                                        'tar_tot'=>$itemt->tar_tot,
                                        'tar_totn'=>$itemt->tar_totn
                                    );
                $this->ModeloCatalogos->Insert('guia_tars',$guia_tars_datos);
            }
        }










        $this->ModeloCatalogos->updateCatalogo('doc_cotiza',array('cot_conta'=>'si'),array('cotiza'=>$cotizacion));
        $this->ModeloCatalogos->updateCatalogo('doc_cotiza',array('cot_conta'=>'si'),array('rid'=>$rid));

        $url=FCPATH.'/docdirectorio/doc_cotiza_'.$cotizacion.'_'.$cot_folio.'.txt';
        file_put_contents($url, $info_text);
    }
    function pruebas($cotizacion){
        $result_paqs=$this->ModeloCatalogos->getselectwheren('doc_paqs',array('cotiza'=>$cotizacion));
        $paq_num=0;
        $pesopaq=0;
        $volument=0;
        foreach ($result_paqs->result() as $itemp) {
             
            $paq_num=$paq_num+$itemp->paq_num;
            //$pesopaq=$pesopaq+($itemp->paq_num*$itemp->paq_pes);
            $pesopaq=$pesopaq+$itemp->paq_pes;
            //var vol=(((parseFloat(alt)*parseFloat(anc)*parseFloat(lar))/6000)*parseFloat(cant));
            $volumen=round(((($itemp->paq_alt*$itemp->paq_anc*$itemp->paq_lar)/6000)*$itemp->paq_num),2);
            $volument=$volument+$volumen;
        }
        echo 'paq_num: '.$paq_num.'<br>';
        echo 'pesopaq: '.$pesopaq.'<br>';
        echo 'volument: '.$volument.'<br>';
    }


}