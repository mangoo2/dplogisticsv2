<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Coe_rechum extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');
            if ($this->ModeloPermisos->SECCION('coe_rechum')){
                //redirect('Sistema');    
            }else{
                redirect('Sistema');    
            }
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['list']=$this->ModeloCatalogos->get_list_coe_rh(0);

        $data['list_suc']=$this->ModeloCatalogos->genSelect('coe_sucs');
        $data['viewsuc']=0;
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_rechum/lista',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('theme/script_datatable');
        $this->load->view('coe_rechum/listajs',$data);
  	}
    function expor(){
        $data['list']=$this->ModeloCatalogos->get_list_coe_rh(0);

        $this->load->view('coe_rechum/lista_expor',$data);
    }
    function inserupdate(){
        $rid=$_POST['rid'];
        $params['sucursal']=$_POST['sucursal'];
        $params['rh_empleado']=$_POST['rh_empleado'];
        $params['rh_nom']=$_POST['rh_nom'];
        $params['rh_pat']=$_POST['rh_pat'];
        $params['rh_mat']=$_POST['rh_mat'];
        $params['rh_depto']=$_POST['rh_depto'];
        $params['rh_cargo']=$_POST['rh_cargo'];
        $params['rh_serviciosa']=$_POST['rh_serviciosa'];
        $params['rh_vigenciaid']=$_POST['rh_vigenciaid'];
        $params['rh_vigenciatia']=$_POST['rh_vigenciatia'];
        $params['rh_fecnac']=$_POST['rh_fecnac'];
        $params['rh_domicilio']=$_POST['rh_domicilio'];
        $params['rh_contacto']=$_POST['rh_contacto'];
        $params['rh_identificacion']=$_POST['rh_identificacion'];
        $params['rh_identificacion']=$_POST['rh_identificacion'];
        $params['rhl_tipo']=$_POST['rhl_tipo'];
        $params['rhl_sueldo']=$_POST['rhl_sueldo'];
        $params['rhl_bonos']=$_POST['rhl_bonos'];
        $params['rhl_altaimss']=$_POST['rhl_altaimss'];
        $params['rhl_nss']=$_POST['rhl_nss'];
        $params['rhl_conini']=$_POST['rhl_conini'];
        $params['rhl_confin']=$_POST['rhl_confin'];
        $params['rhl_horario']=$_POST['rhl_horario'];
        $params['rhb_banco']=$_POST['rhb_banco'];
        $params['rhb_titular']=$_POST['rhb_titular'];
        $params['rhb_cuenta']=$_POST['rhb_cuenta'];
        $params['rhb_plastico']=$_POST['rhb_plastico'];


        $DIR_SUC=FCPATH.'_files/_coe/rechum';
        if (!is_dir($DIR_SUC) && strlen($DIR_SUC)>0){
            log_message('error',$DIR_SUC);
            mkdir($DIR_SUC, 0755);
        }else{
            file_put_contents($DIR_SUC.'/index.html', '');
        }
        $listresult=$this->ModeloCatalogos->getselectwheren('coe_rh',array('rid'=>$rid));
        $logs='';
        foreach ($listresult->result() as $itemrh) {
            $logs=$itemrh->log;
        }

        $config['upload_path']          = $DIR_SUC;
        $config['allowed_types']        = 'gif|jpg|png|pdf';
        $config['max_size']             = 5000;
        $config['file_name']=date('YmdGis').'_'.rand(0, 99);       
        $output = [];
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('rh_foto')){
            $data = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($data));
            if($rid>0){
                $params['log']='Creado: '.$this->fechalarga.' '.$this->sess_usr.'|'.$logs;
                $this->ModeloCatalogos->updateCatalogo('coe_rh',$params,array('rid'=>$rid));
            }else{
                $params['log']='Creado: '.$this->fechalarga.' '.$this->sess_usr;
                $params['rh_est']='act';
                $this->ModeloCatalogos->Insert('coe_rh',$params);
            }
                        
        }else{
             $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension
            $params['rh_foto']=$file_name;

            if($rid>0){
                $params['log']='Creado: '.$this->fechalarga.' '.$this->sess_usr.'|'.$logs;

                $this->ModeloCatalogos->updateCatalogo('coe_rh',$params,array('rid'=>$rid));
            }else{
                $params['log']='Creado: '.$this->fechalarga.' '.$this->sess_usr;
                $params['rh_est']='act';
                $this->ModeloCatalogos->Insert('coe_rh',$params);
            }

            $data = array('upload_data' => $this->upload->data());
            //$output = [];
            log_message('error', json_encode($data));
        }
        echo json_encode($output);
    }
    function delete(){
        $params = $this->input->post();
        $rid = $params['rid'];
        $this->ModeloCatalogos->getdeletewheren('coe_rh',array('rid'=>$rid));
    }
    function reg_evis($sucursal,$rid){
        $data['rrid']=$rid;
        $data['codigo']=$sucursal;
        $resultsuc=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$sucursal));
        $data['resultsuc']=$resultsuc->row();

        $resulrh=$this->ModeloCatalogos->getselectwheren('coe_rh',array('rid'=>$rid));
        $data['resulrh']=$resulrh->row();

        $data['list_evidemcias']=$this->ModeloCatalogos->getselectwheren('coe_rhevi',array('rrid'=>$rid));
        $data['viewsuc']=0;
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_rechum/reg_evis',$data);
        $this->load->view('theme/footer'); 
        //$this->load->view('theme/script_datatable');
        $this->load->view('coe_rechum/reg_evisjs');
    }
    function inserupdateevi(){
        $rid=$_POST['rid'];
        $params['rrid']=$_POST['rrid'];
        $params['evi_nombre']=$_POST['evi_nombre'];

        $DIR_SUC=FCPATH.'_files/_coe/rechum';
        if (!is_dir($DIR_SUC) && strlen($DIR_SUC)>0){
            log_message('error',$DIR_SUC);
            mkdir($DIR_SUC, 0755);
        }else{
            file_put_contents($DIR_SUC.'/index.html', '');
        }
        $listresult=$this->ModeloCatalogos->getselectwheren('coe_rh',array('rid'=>$rid));
        $logs='';
        foreach ($listresult->result() as $itemrh) {
            $logs=$itemrh->log;
        }

        $config['upload_path']          = $DIR_SUC;
        $config['allowed_types']        = 'gif|jpg|png|pdf';
        $config['max_size']             = 5000;
        $config['file_name']=date('YmdGis').'_'.rand(0, 99);       
        $output = [];
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('evi_archivo')){
            $data = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($data));
            if($rid>0){
                $this->ModeloCatalogos->updateCatalogo('coe_rhevi',$params,array('rid'=>$rid));
            }else{
                $this->ModeloCatalogos->Insert('coe_rhevi',$params);
            }
                        
        }else{
             $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension
            $params['evi_archivo']=$file_name;

            if($rid>0){
                $this->ModeloCatalogos->updateCatalogo('coe_rhevi',$params,array('rid'=>$rid));
            }else{
                $this->ModeloCatalogos->Insert('coe_rhevi',$params);
            }

            $data = array('upload_data' => $this->upload->data());
            //$output = [];
            log_message('error', json_encode($data));
        }
        echo json_encode($output);
    }
    function deleteevi(){
        $params = $this->input->post();
        $rid = $params['rid'];
        $this->ModeloCatalogos->getdeletewheren('coe_rhevi',array('rid'=>$rid));
    }

   

 





}