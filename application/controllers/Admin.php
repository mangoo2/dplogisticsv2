<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloAdmin');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->anio=date('Y');
        setlocale(LC_ALL, 'spanish-mexican');
        setlocale(LC_ALL, 'es_MX');
        $this->fechal = date('dmYHis');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){   
  	}
    function reportes(){
        $data['sess_suc']=$this->sess_suc;
        $data['resultsuc']=$this->ModeloCatalogos->genSelect('coe_sucs');
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('admin/reportes',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('admin/reportesjs');
    }
    public function getlistacli() {
        $params = $this->input->post();
        $params['sucursal']=$this->sess_suc;
        $getdata = $this->ModeloAdmin->getlistacli($params);
        $totaldata= $this->ModeloAdmin->getlistacli_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function getlistaclichart(){
        $params = $this->input->post();
        //$anio=$params['anio'];
        //$params['fechai']=$anio.'-01-01';
        //$params['fechaf']=$anio.'-12-30';
        $params['fechai']=$params['fini'];
        $params['fechaf']=$params['ffin'];
        $params['sucursals']=$params['sucursal'];
        $resut = $this->ModeloAdmin->getlistacli_chart($params);
        //$values=array();
        //$valores=array();
        $datos=array();

        foreach ($resut->result() as $item) {
            //$values[]=$item->cliente;
            //$valores[]=$item->paq_pes;
            $datos[]=array('clienteid'=>$item->clienteid,'cliente'=>$item->cliente,'paq_pes'=>round($item->paq_pes, 2),'tar_tot'=>round($item->tar_tot, 2),'anio'=>$item->fechayear,'folios'=>$item->folios,'tarifa_baja'=>$item->tarifa_baja,'tarifa_especial'=>$item->tarifa_especial);
        }
        $datos =array(
                //'values'=>$values,
                //'valores'=>$valores,
                'datos'=>$datos
                ); 
        echo json_encode($datos);
    }
    function tablegindividetalles(){
        $params = $this->input->post();
        $fechai=$params['fini'];
        $fechaf=$params['ffin'];

        //log_message('error',$params);
        //$anio=$params['anio'];
        //unset($params['anio']);
        $semana=array();
        //$anio = json_decode($anio);
        //for ($j=0;$j<count($anio);$j++) {
        //        $aniofor=$anio[$j];
                //=========================================
                //$fecha_inicial = new DateTime($aniofor.'-01-01');
                //$fecha_final = new DateTime($aniofor.'-12-31');
                $fecha_inicial = new DateTime($fechai);
                $fecha_final = new DateTime($fechaf);

                $row=1;
                
                $mesl_info='ENE';
                for($i = $fecha_inicial; $i <= $fecha_final; $i->modify('+1 day')){
                    $fecha_inicial=$i->format("Y-m-d");
                    $aniofor=date('Y',strtotime($fecha_inicial));
                    //echo $fecha_inicial.'<br>';
                    if(date('w',strtotime($fecha_inicial))==0){
                        $masdia=6;
                    }
                    if(date('w',strtotime($fecha_inicial))==1){
                        $masdia=5;
                    }
                    if(date('w',strtotime($fecha_inicial))==2){
                        $masdia=4;
                    }
                    if(date('w',strtotime($fecha_inicial))==3){
                        $masdia=3;
                    }
                    if(date('w',strtotime($fecha_inicial))==4){
                        $masdia=2;
                    }
                    if(date('w',strtotime($fecha_inicial))==5){
                        $masdia=1;
                    }
                    if(date('w',strtotime($fecha_inicial))==6){
                        $masdia=7;
                    }
                    $i->modify('+'.$masdia.' day');
                    $fechafinal=$i->format("Y-m-d");
                    $finalmes=0;
                    if(date('n',strtotime($fecha_inicial))!=date('n',strtotime($fechafinal)) ){
                        $month_end = strtotime('last day of this month', strtotime($fecha_inicial));
                        $i->modify(date('Y-m-d',$month_end));

                        $fechafinal=$i->format("Y-m-d");  
                        $finalmes=1;
                    }
                    $mesl=$this->mesespanol(date('n',strtotime($fecha_inicial)));

                    //==============================================================
                    $params['fechai']=$fecha_inicial;
                    $params['fechaf']=$fechafinal;
                    $resut = $this->ModeloAdmin->getlistacli_chart2($params);
                    $kilos=0;
                    $pesos=0;
                    foreach ($resut->result() as $item) {
                        if($item->paq_pes>0){
                            $kilos=$item->paq_pes;
                        }
                        if($item->tar_tot>0){
                            $pesos=$item->tar_tot;
                        }
                        
                        
                    }
                    
                    //==============================================================
                    ${'mes_'.$mesl}[]=$kilos;
                    
                    $dia_num=date('j',strtotime($fechafinal));
                    if($dia_num==31){
                        $finalmes=1;
                    }
                    if($finalmes==1){
                        $promedio=array_sum(${'mes_'.$mesl})/count(${'mes_'.$mesl});
                        $promedio=round($promedio, 2);
                    }else{
                        $promedio='';
                    }
                    $semana[]=array(
                                    'anio'=>$aniofor,
                                    'mes'=>$mesl,
                                    'semana'=>$row,
                                    'periodo'=>date('d',strtotime($fecha_inicial)).' al '.date('d',strtotime($fechafinal)).' '.$mesl,
                                    'fechainicial'=>$fecha_inicial,
                                    'fechafinal'=>$fechafinal,
                                    'kilos'=>$kilos,
                                    'pesos'=>$pesos,
                                    'promedio'=>$promedio
                                );
                    $row++;
                }
            //=========================================
        //}
        echo json_encode($semana);
    }
    function mesespanol($mes){
        switch ($mes) {
            case 1:
                $mesletra='ENE';
                break;
            case 2:
                $mesletra='FEB';
                break;
            case 3:
                $mesletra='MAR';
                break;
            case 4:
                $mesletra='ABR';
                break;
            case 5:
                $mesletra='MAY';
                break;
            case 6:
                $mesletra='JUN';
                break;
            case 7:
                $mesletra='JUL';
                break;
            case 8:
                $mesletra='AGO';
                break;
            case 9:
                $mesletra='SEP';
                break;
            case 10:
                $mesletra='OCT';
                break;
            case 11:
                $mesletra='NOV';
                break;
            case 12:
                $mesletra='DIC';
                break;

            
            default:
                $mesletra='';
                break;

        }
        return $mesletra;
    }
    function generar_v_c(){
        $params = $this->input->post();
        $clientes = $params['clientes'];
        $DATAc = json_decode($clientes);
        $html='';
        $paqpes=0;
        $g_tartot=0;
        for ($i=0;$i<count($DATAc);$i++) {
            $paqpes=$paqpes+$DATAc[$i]->paqpes;
            $g_tartot=$g_tartot+$DATAc[$i]->tartot;
        }
        $g_v_por_kilo=0;
        $g_valor_tartot=0;
        $g_total_descuento=0;
        $g_ingreso_v_g_a=0;
        $row=0;
        for ($i=0;$i<count($DATAc);$i++) {
            $v_por_kilo=($DATAc[$i]->paqpes*1)/$paqpes;
            $valor_por_kilo=$v_por_kilo*100;
            $g_v_por_kilo=$g_v_por_kilo+$v_por_kilo;

            $v_tartot=($DATAc[$i]->tartot*1)/$g_tartot;
            $valor_tartot=$v_tartot*100;
            $g_valor_tartot=$g_valor_tartot+$v_tartot;

            $primer_anio=$this->ModeloCatalogos->primer_anio($DATAc[$i]->cliid);
            if($primer_anio>0){
                $aniorelacion=$this->anio-$primer_anio;
            }else{
                $aniorelacion=1;
            }
            
            $valor_vida_cli=(($DATAc[$i]->tartot/$DATAc[$i]->folios)*$DATAc[$i]->folios)*$aniorelacion;

            $ticket_prom=$DATAc[$i]->tartot/$DATAc[$i]->folios;

            if($DATAc[$i]->tarifabaja>0){
                $tarifario_voi=$DATAc[$i]->tarifabaja;
            }else{
                $tarifario_voi=0;
            }
            if($DATAc[$i]->tarifaespecial>0){
                $tarifa_especial=$DATAc[$i]->tarifaespecial;
            }else{
                $tarifa_especial=0;
            }
            //$tarifario_voi=12.6;//checar de donde viene este dato
            //$tarifa_especial=14.00;//checar de donde viene este dato
            $diferencia_pesos=$tarifa_especial-$tarifario_voi;

            $total_descuento=$diferencia_pesos*$DATAc[$i]->paqpes;

            $ingreso_v_g_a=$DATAc[$i]->tartot-$total_descuento;

            $g_total_descuento=$g_total_descuento+$total_descuento;
            $g_ingreso_v_g_a=$g_ingreso_v_g_a+$ingreso_v_g_a;
            $html.='<tr>
                        <td class="datasclass datasclass_'.$row.'" 
                            data-cli="'.$DATAc[$i]->cli.'"
                            data-valorxkilo="'.round($valor_por_kilo).'"
                            data-valorvidacli="'.$valor_vida_cli.'"
                            data-valortartot="'.round($valor_tartot).'"
                            data-ticketprom="'.round($ticket_prom).'"
                        >'.$DATAc[$i]->cli.'</td>
                        <td>'.number_format($DATAc[$i]->paqpes).'</td>
                        <td>% '.round($valor_por_kilo).'</td>
                        <td>'.number_format($DATAc[$i]->tartot).'</td>
                        <td>% '.round($valor_tartot).'</td>

                        <td>'.$aniorelacion.'</td>
                        <td>'.number_format($DATAc[$i]->folios).'</td>

                        <td>'.number_format($valor_vida_cli).'</td>
                        <td>$ '.number_format($ticket_prom).'</td>

                        <td>'.$tarifario_voi.'</td>
                        <td>'.$tarifa_especial.'</td>
                        <td>'.$diferencia_pesos.'</td>
                        <td>$ '.number_format($total_descuento).'</td>

                        <td class="ingreso_v_g_a" data-row="'.$row.'" data-ingre="'.$ingreso_v_g_a.'">$ '.number_format($ingreso_v_g_a).'</td>
                        <td class="ingreso_v_g_a_'.$row.'"></td>

                    </tr>';
            $row++;
        }
        if(count($DATAc)>0){
            $html.='<tr>
                        <td>TOTAL</td>
                        <td>'.number_format($paqpes).'</td>
                        <td>% '.($g_v_por_kilo*100).'</td>
                        <td>$ '.number_format($g_tartot).'</td>
                        <td>% '.($g_valor_tartot*100).'</td>

                        <td colspan="7">*La tarifa que al menos debí haber cobrado (la más baja +500) de acuerdo al tipo de servicio (per, gen, nxt, cou) y zona (ctr-nte, ctr-ctr, ctr-sur; sur-nte, sur-ctr, sur-sur) en relación a la tarifa tomada del DPL</td>
                        
                        <td>$ '.number_format($g_total_descuento).'</td>

                        <td class="g_ingreso_v_g_a" data-gingreso="'.$g_ingreso_v_g_a.'">$ '.number_format($g_ingreso_v_g_a).'</td>
                        <td class="t_ingreso_v_g_a"></td>

                    </tr>';
        }
        echo $html;
    }
    function metas(){
        $data['sess_suc']=$this->sess_suc;
        $data['resultsuc']=$this->ModeloCatalogos->genSelect('coe_sucs');
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('admin/reportesmetas',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('admin/reportesmetasjs');
    }
    function cargarmetas(){
        $params = $this->input->post();
        $anio=$params['anio'];
        $resultsuc=$this->ModeloCatalogos->genSelect('coe_sucs');
        $html='';
        //==================
            $my_date = new DateTime();
            $meses_array=array();
            //January_February_March_April_May_June_July_August_September_October_November_December
            $meses_array[]=array('mesid'=>1,'mes'=>'ENE','mesl'=>'january');
            $meses_array[]=array('mesid'=>2,'mes'=>'FEB','mesl'=>'february');
            $meses_array[]=array('mesid'=>3,'mes'=>'MAR','mesl'=>'march');
            $meses_array[]=array('mesid'=>4,'mes'=>'ABR','mesl'=>'april');
            $meses_array[]=array('mesid'=>5,'mes'=>'MAY','mesl'=>'may');
            $meses_array[]=array('mesid'=>6,'mes'=>'JUN','mesl'=>'june');
            $meses_array[]=array('mesid'=>7,'mes'=>'JUL','mesl'=>'july');
            $meses_array[]=array('mesid'=>8,'mes'=>'AGO','mesl'=>'august');
            $meses_array[]=array('mesid'=>9,'mes'=>'SEP','mesl'=>'september');
            $meses_array[]=array('mesid'=>10,'mes'=>'OCT','mesl'=>'october');
            $meses_array[]=array('mesid'=>11,'mes'=>'NOV','mesl'=>'november');
            $meses_array[]=array('mesid'=>12,'mes'=>'DIC','mesl'=>'december');
        //====================
        foreach ($resultsuc as $item_suc) {
            $idsuc=$item_suc->rid;
            $r_suc_m=$this->ModeloCatalogos->getsucursalmetas($anio,$idsuc);
            $html.='<table class="table table-bordered tables_metas">
                        <thead>
                            <tr>
                                <th colspan="3">'.$item_suc->suc_nombre.'</th>
                            </tr>
                            <tr>
                                <th>Mes</th>
                                <th>Meta kg</th>
                                <th>Meta Valor</th>
                            </tr>
                        </thead>
                <tbody>';
                if($r_suc_m->num_rows()>0){
                    foreach ($r_suc_m->result() as $item) {
                        $html.='<tr>
                            <td>
                                <input type="number" class="hidden" id="id" value="'.$item->id.'">
                                <input type="number" class="hidden" id="sucid" value="'.$item->sucid.'">
                                <input type="number" class="hidden" id="anio" value="'.$item->anio.'">
                                <input type="text" class="hidden" id="mes" value="'.$item->mes.'">
                                <input type="date" class="hidden" id="fecha_ini" value="'.$item->fecha_ini.'">
                                <input type="date" class="hidden" id="fecha_fin" value="'.$item->fecha_fin.'">
                                '.$item->mes.'
                            </td>
                            <td>
                                <input type="number" class="form-control input_v" id="meta_kg" value="'.$item->meta_kg.'">
                            </td>
                            <td>
                                <input type="number" class="form-control input_v" id="meta_venta" value="'.$item->meta_venta.'">
                            </td>
                        </tr>';
                    }
                }else{
                    foreach ($meses_array as $a_v) {
                        $my_date->modify('first day of '.$a_v['mesl'].' '.$anio);
                        $fecha_ini=$my_date->format('Y-m-d');

                        $my_date->modify('last day of '.$a_v['mesl'].' '.$anio);
                        $fecha_fin=$my_date->format('Y-m-d');

                        $html.='<tr>
                        <td>
                            <input type="number" class="hidden" id="id" value="0">
                            <input type="number" class="hidden" id="sucid" value="'.$idsuc.'">
                            <input type="number" class="hidden" id="anio" value="'.$anio.'">
                            <input type="text" class="hidden" id="mes" value="'.$a_v['mes'].'">
                            <input type="date" class="hidden" id="fecha_ini" value="'.$fecha_ini.'">
                            <input type="date" class="hidden" id="fecha_fin" value="'.$fecha_fin.'">
                            '.$a_v['mes'].'
                        </td>
                        <td>
                            <input type="number" class="form-control input_v" id="meta_kg" value="">
                        </td>
                        <td>
                            <input type="number" class="form-control input_v" id="meta_venta" value="">
                        </td>
                    </tr>';
                    }
                    
                }
            $html.='</tbody>
                </table>';
        }
        
        echo $html;
    }
    function insertupdatemetas(){
        $params = $this->input->post();
        $metas = $params['metas'];
        $DATAc = json_decode($metas);

        $dataequiposrarray=array();

        for ($i=0;$i<count($DATAc);$i++) {
            $id=$DATAc[$i]->id;

            $datae['sucid'] = $DATAc[$i]->sucid;
            $datae['anio'] = $DATAc[$i]->anio;
            $datae['mes'] = $DATAc[$i]->mes;
            $datae['fecha_ini'] = $DATAc[$i]->fecha_ini;
            $datae['fecha_fin'] = $DATAc[$i]->fecha_fin;
            $datae['meta_kg'] = $DATAc[$i]->meta_kg;
            $datae['meta_venta'] = $DATAc[$i]->meta_venta;

            if($id>0){
                $this->ModeloCatalogos->updateCatalogo('coe_sucs_metas',$datae,array('id'=>$id));
            }else{
                $dataequiposrarray[]=$datae;
            }
        }

        if(count($dataequiposrarray)>0){
            $this->ModeloCatalogos->insert_batch('coe_sucs_metas',$dataequiposrarray);
        }
    }
    function generar_reportemetas(){
        $params = $this->input->post();
        $anio=$params['anio'];
        $resultsuc=$this->ModeloCatalogos->genSelect('coe_sucs');

        $html0='';
        $html1='';
        foreach ($resultsuc as $item_suc) {
            $idsuc=$item_suc->rid;
            $sucursal=$item_suc->sucursal;
            $r_suc_m=$this->ModeloCatalogos->getsucursalmetas($anio,$idsuc);
            if($r_suc_m->num_rows()>0){

                $html0.='<table class="table table-bordered tables_metas_report" id="tables_metas_kilos_'.$idsuc.'">
                        <thead><tr><th colspan="5">'.$item_suc->suc_nombre.'</th></tr><tr><th>Mes</th><th>Meta</th><th>Logro</th><th>Diferencia</th><th>%</th></tr></thead><tbody>';
                $html1.='<table class="table table-bordered tables_metas_report" id="tables_metas_venta_'.$idsuc.'">
                        <thead><tr><th colspan="5">'.$item_suc->suc_nombre.'</th></tr><tr><th>Mes</th><th>Meta</th><th>Logro</th><th>Diferencia</th><th>%</th></tr></thead><tbody>';
                    foreach ($r_suc_m->result() as $item) {
                        //==========================================================
                            $params['fechai']=$item->fecha_ini;
                            $params['fechaf']=$item->fecha_fin;
                            $params['sucursals']=$sucursal;
                            $r_p_i=$this->ModeloAdmin->sql_peso_importe($params);
                            $r_p_i=$r_p_i->row();
                            
                            if($r_p_i->paq_pes>0){
                                $paq_pes=$r_p_i->paq_pes;
                            }else{
                                $paq_pes=0;
                            }
                            if($r_p_i->tar_tot>0){
                                $tar_tot=$r_p_i->tar_tot;
                            }else{
                                $tar_tot=0;
                            }
                            $dif_paq_pes=$paq_pes-$item->meta_kg;
                            $dif_tar_tot=$tar_tot-$item->meta_venta;

                            if($item->meta_kg>0){
                                $por_paq_pes=(($paq_pes/$item->meta_kg)-1)*100;
                            }else{
                                $por_paq_pes=0;
                            }
                            
                            if($item->meta_venta>0){
                                $por_tar_tot=(($tar_tot/$item->meta_venta)-1)*100;
                            }else{
                                $por_tar_tot=0;
                            }
                        //=========================================================
                        
                        $html0.='<tr><td class="datasclass_kg_'.$idsuc.'" data-mes="'.$item->mes.'" data-meta="'.$item->meta_kg.'" data-logro="'.$paq_pes.'" >'.$item->mes.'</td><td>'.$item->meta_kg.'</td><td>'.$paq_pes.'</td><td>'.$dif_paq_pes.'</td><td>'.round($por_paq_pes).'%</td></tr>';

                        $html1.='<tr><td class="datasclass_v_'.$idsuc.'" data-mes="'.$item->mes.'" data-meta="'.$item->meta_venta.'" data-logro="'.$tar_tot.'" >'.$item->mes.'</td><td>'.$item->meta_venta.'</td><td>'.$tar_tot.'</td><td>'.$dif_tar_tot.'</td><td>'.round($por_tar_tot).'%</td></tr>';
                    }
                $html0.='</tbody></table>';
                $html1.='</tbody></table>';
            }
        }
        $html='<div class="col-md-12">KILOS</div><div>'.$html0.'</div><div class="col-md-12">VENTA</div><div>'.$html1.'</div>';
        echo $html;
    }

}