<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ComplementosPago extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        //$this->load->model('General_model');
        //$this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloComplementos');
        $this->load->model('Modelofacturas');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        $this->load->library('zip');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');

            $this->fechahoy = date('Y-m-d G:i:s');
            $this->fecha_reciente = date('Y-m-d');
            $this->fechahoy = date('Y-m-d');
            $this->fechahoyc = date('Y-m-d H:i:s');
            $this->fechahoyL = date('Y-m-d_H_i_s');
        }else{
            redirect('Sistema'); 
        }
    }

	public function index(){
      $data['sess_suc']=$this->sess_suc;
        $data['mot_can']= $this->ModeloCatalogos->getselectwheren('motivos_cancelacion',array('activo'=>1));
        $data['sucursal_row']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1)); 
        $data['cfdi']=$this->ModeloCatalogos->genSelect('f_uso_cfdi'); 
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('complementospago/listado',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('complementospago/listadojs');
    }
    public function add($idfactura){
        $data['btn_active']=3;
        $data['btn_active_sub']=12;
        $data['cfdi']=$this->ModeloCatalogos->genSelect('f_uso_cfdi'); 
        $data['metodo']=$this->ModeloCatalogos->genSelect('f_metodopago'); 
        $data['forma']=$this->ModeloCatalogos->genSelect('f_formapago'); 

        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];
        $tipoempresa=$datosfactura->tipoempresa;
          if($tipoempresa>0){

          }else{
            $tipoempresa=1;
          }
        $clienteId=$datosfactura->Clientes_ClientesId;
        
        $datoscliente=$this->ModeloCatalogos->getselectwheren('cli_facts',array('rid'=>$clienteId));
        $datoscliente=$datoscliente->result();
        $datoscliente=$datoscliente[0];
        $data['fac_email']=$datoscliente->fac_email;
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$tipoempresa));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        $data['ConfiguracionesId']=$datosconfiguracion->ConfiguracionesId;
        $data['rFCEmisor']=$datosconfiguracion->Rfc;
        $data['Nombre']=$datosconfiguracion->Nombre;
        $data['Regimen']=$datosconfiguracion->Regimen;
        $data['LugarExpedicion']=$datosconfiguracion->CodigoPostal;

        
        $data['razonsocialreceptor']=$datosfactura->Nombre;
        $data['rfcreceptor']=$datosfactura->Rfc;
        $data['Fecha']=date('Y-m-d').'T'.date('H:i:s');
        $data['uuid']=$datosfactura->uuid;
        $data['Folio']=$datosfactura->Folio;
        $data['serie']=$datosfactura->serie;
        $data['FacturasId']=$datosfactura->FacturasId;
        $data['Fechafa']=date('Y-m-d',strtotime($datosfactura->fechatimbre)).'T'.date('H:i:s',strtotime($datosfactura->fechatimbre));
        $data['formapago']=$datosfactura->MetodoPago;
        
        $saldo=$this->ModeloComplementos->saldocomplemento($idfactura);
        $datoscopnum=$this->ModeloComplementos->saldocomplementonum($idfactura);

        $data['copnum']=$datoscopnum+1;
        //log_message('error', 'validar saldo: '.$datosfactura->total.'-'.$saldo);
        $data['saldoanterior']=$datosfactura->total-$saldo;
        $data['cliente']=$datosfactura->Clientes_ClientesId;
        $data['MetodoDePagoDR']=$datosfactura->FormaPago;


        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('complementospago/add',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('complementospago/addjs');

    }

    public function getlistcomplementos() {
        $params = $this->input->post();
        $getdata = $this->ModeloComplementos->getcomplementos($params);
        $totaldata= $this->ModeloComplementos->total_complementos($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function complementodoc($id){
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->row();
        if($datosconfiguracion->logotipo!=''){
          $logotipo = base_url().'public/img/'.$datosconfiguracion->logotipo;
        }else{
          $logotipo = base_url().'public/img/logo_hules.png';
        }
        $data["logotipo"]=$logotipo;

        $data['rfcemisor']=$datosconfiguracion->Rfc;
        $data['nombreemisor']=$datosconfiguracion->Nombre;
        $data['rdireccion']=$datosconfiguracion->Calle.' '.$datosconfiguracion->Municipio.' '.$datosconfiguracion->Estado.' '.$datosconfiguracion->PaisExpedicion.' C.P. '.$datosconfiguracion->CodigoPostal;
        $data['regimenf']=$this->gt_regimenfiscal($datosconfiguracion->Regimen);
        //==========================================
            $data['idcomplemento']=$id;
            $resultscp=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$id));
            $data['resultscpd']=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('complementoId'=>$id));
            $resultscp=$resultscp->row();
            $FacturasId=$resultscp->FacturasId;
            $data['uuid']=$resultscp->uuid;
            $data['nocertificadosat']=$resultscp->nocertificadosat;
            $data['LugarExpedicion']=$resultscp->LugarExpedicion;
            $data['fechatimbre']=$resultscp->fechatimbre;
            $data['fechapago']=$resultscp->FechaPago;
            $data['monto']=$resultscp->Monto;
            $data['IdDocumento']=$resultscp->IdDocumento;
            //$data['Folio']=$resultscp->Folio;
            $data['ImpSaldoAnt']=$resultscp->ImpSaldoAnt;
            $data['NumParcialidad']=$resultscp->NumParcialidad;
            $data['ImpSaldoInsoluto']=$resultscp->ImpSaldoInsoluto;
            $data['Sello']=$resultscp->Sello;
            $data['sellosat']=$resultscp->sellosat;
            $data['cadenaoriginal']=$resultscp->cadenaoriginal;
            $data['nocertificadosat']=$resultscp->nocertificadosat;
            $data['rfcreceptor']=$resultscp->R_rfc;
            $data['nombrereceptorr']=$resultscp->R_nombre;
            $data['NumOperacion']=$resultscp->NumOperacion;
            $docrelac = $this->Modelofacturas->documentorelacionado($id);
            $data['docrelac']=$docrelac->result();
        //====================================================================
            $resultsfp=$this->ModeloCatalogos->getselectwheren('f_formapago',array('id'=>$resultscp->FormaDePagoP));
            $resultsfp=$resultsfp->row();
            $data['formapago']=$resultsfp->formapago_text;
        //==========================================
            //$resultsfc=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$FacturasId));
            //$resultsfc=$resultsfc->row();
            /*
            if ($resultsfc->moneda=='pesos') {
                $data['moneda']='Peso Mexicano';
            }else{
                $data['moneda']='Dolar';
            }
            */
        //====================================================================
            //$resultsmp=$this->ModeloCatalogos->getselectwheren('f_metodopago',array('metodopago'=>$resultsfc->FormaPago));
            //$resultsmp=$resultsmp->row();
            //$data['metodopago']=$resultsmp->metodopago_text;
        //==========================================
        $this->load->view('reportes/complemento',$data);
    }
    function correoscliente(){
        $params= $this->input->post();
        $idcomplemento = $params['idcomplemento'];
        $resultcp=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('complementoId'=>$idcomplemento));
        $FacturasId=0;
        foreach ($resultcp->result() as $item) {
            $FacturasId=$item->facturasId;
        }

        $resultcpf=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$FacturasId));
        $clienteId=0;
        foreach ($resultcpf->result() as $item) {
            $clienteId=$item->clienteId;
        }

        $factura = $this->ModeloCatalogos->getselectwheren('cliente_mail',array('clienteId'=>$clienteId,'activo'=>1));
          $option='';
          foreach ($factura->result() as $item) {
            $option.='<option value="'.$item->mail.'">'.$item->mail.'</option>';
          }
          echo $option; 



    }
    function gt_regimenfiscal($text){
          if($text=="601"){ 
            $textl='601 General de Ley Personas Morales';

          }elseif($text=="603"){ 
            $textl='603 Personas Morales con Fines no Lucrativos';

          }elseif($text=="605"){ 
            $textl='605 Sueldos y Salarios e Ingresos Asimilados a Salarios';

          }elseif($text=="606"){ 
            $textl='606 Arrendamiento';

          }elseif($text=="607"){ 
            $textl='607 Régimen de Enajenación o Adquisición de Bienes';

          }elseif($text=="608"){ 
            $textl='608 Demás ingresos';

          }elseif($text=="609"){ 
            $textl='609 Consolidación';

          }elseif($text=="610"){ 
            $textl='610 Residentes en el Extranjero sin Establecimiento Permanente en México';

          }elseif($text=="611"){ 
            $textl='611 Ingresos por Dividendos (socios y accionistas)';

          }elseif($text=="612"){ 
            $textl='612 Personas Físicas con Actividades Empresariales y Profesionales';

          }elseif($text=="614"){ 
            $textl='614 Ingresos por intereses';

          }elseif($text=="615"){ 
            $textl='615 Régimen de los ingresos por obtención de premios';

          }elseif($text=="616"){ 
            $textl='616 Sin obligaciones fiscales';

          }elseif($text=="620"){ 
            $textl='620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos';

          }elseif($text=="621"){ 
            $textl='621 Incorporación Fiscal';

          }elseif($text=="622"){ 
            $textl='622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras';

          }elseif($text=="623"){ 
            $textl='623 Opcional para Grupos de Sociedades';

          }elseif($text=="624"){ 
            $textl='624 Coordinados';

          }elseif($text=="628"){ 
            $textl='628 Hidrocarburos';

          }elseif($text=="629"){ 
            $textl='629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales';

          }elseif($text=="630"){ 
            $textl='630 Enajenación de acciones en bolsa de valores';
          }else{
            $textl='';
          }
          return $textl;
    }
    function obtenerfacturas(){
        $datos = $this->input->post();
        $cli=$datos['cli'];
        $fact=$datos['fact'];
        $rowsfacturas=$this->ModeloCatalogos->getselectwheren('f_facturas',array('Estado'=>1,'Clientes_ClientesId'=>$cli,'Folio !='=>$fact));
        $html='<table id="facturascli" class="table">
                <thead>
                    <tr>
                        <th>Serie</th>
                        <th>Folio</th>
                        <th>IdDocumento(uuid)</th>
                        <th>Monto</th>
                        <th>Fecha Timbre</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
        ';
        foreach ($rowsfacturas->result() as $item) {
            $html.='<tr>
                        <td>'.$item->serie.'</td>
                        <td>'.$item->Folio.'</td>
                        <td class="facturasuuid">'.$item->uuid.'</td>
                        <td>'.$item->total.'</td>
                        <td>'.$item->fechatimbre.'</td>
                        <td>
                            <a class="btn btn-success" 
                                onclick="adddoc('.$item->FacturasId.')"
                                >Agregar</a></td>
                    </tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    function documentoadd(){
        $datos = $this->input->post();
        $idfactura=$datos['factura'];
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];

        $datoscop=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('FacturasId'=>$idfactura,'Estado'=>1));
        $datoscopnum=$datoscop->num_rows();
        $saldo=0;
        foreach ($datoscop->result() as $item) {
            $saldo=$saldo+$item->ImpPagado;
        }
        $NumParcialidad=$datoscopnum+1;
        $array = array(
            'idfactura'=>$idfactura,
            'MetodoDePagoDR'=>$datosfactura->FormaPago,
            'IdDocumento'=>$datosfactura->uuid,
            'serie'=>$datosfactura->serie,
            'folio'=>$datosfactura->Folio,
            'NumParcialidad'=>$NumParcialidad,
            'ImpSaldoAnt'=>$datosfactura->total-$saldo
        );
        echo json_encode($array);
    }
  function excel_comp($f1,$f2,$emp,$suc,$cfdi,$ecomp,$cliente){
    $data['f1']=$f1;
    $data['f2']=$f2;
    $data['result'] = $this->ModeloComplementos->get_factura($f1,$f2,$emp,$suc,$cfdi,$ecomp,$cliente);
    $this->load->view('complementospago/excel',$data);
  }
  function comp_zip($f1,$f2,$emp,$suc,$cfdi,$ecomp,$cliente){
        $result = $this->ModeloComplementos->get_factura($f1,$f2,$emp,$suc,$cfdi,$ecomp,$cliente);
        foreach ($result as $r){
            $this->zip->read_file(FCPATH.'/files_sat/facturaspdf/Complemento_'.$r->Serie.'_'.$r->Folio.'.pdf');
            $this->zip->read_file(FCPATH.$r->rutaXml);
        }
        $this->zip->download('my_archive.zip');
  }
    function excel_comp_indi(){
        $facturas=json_decode($_GET['facturas']);
        $this->db->select("c.complementoId, c.Serie, c.Folio, c.R_nombre, c.R_rfc, c.Monto, c.Estado, c.fechatimbre, c.rutaXml, c.rutaAcuseCancelacion, c.uuid,fp.formapago_text");
        $this->db->from('f_complementopago c');
        $this->db->join('f_formapago fp', 'fp.clave=c.FormaDePagoP','left');
        $this->db->group_start();
        foreach ($facturas as $item) {
            $this->db->or_like('c.complementoId',$item->FIds);
        }
        $this->db->group_end();  
        $query=$this->db->get();
        $data['result'] = $query->result();
        $this->load->view('complementospago/excel',$data);
    }
    function comp_zip_indi(){
        $facturas=json_decode($_GET['facturas']);
        $this->db->select("c.complementoId, c.Serie, c.Folio, c.R_nombre, c.R_rfc, c.Monto, c.Estado, c.fechatimbre, c.rutaXml, c.rutaAcuseCancelacion, c.uuid,fp.formapago_text");
        $this->db->from('f_complementopago c');
        $this->db->join('f_formapago fp', 'fp.clave=c.FormaDePagoP','left');
        $this->db->group_start();
        foreach ($facturas as $item) {
            $this->db->or_like('c.complementoId',$item->FIds);
        }
        $this->db->group_end();  
        $query=$this->db->get();
        $result = $query->result();
        foreach ($result as $r){
            $this->zip->read_file(FCPATH.'/files_sat/facturaspdf/Complemento_'.$r->Serie.'_'.$r->Folio.'.pdf');
            $this->zip->read_file(FCPATH.$r->rutaXml);
        }
        $this->zip->download('my_archive.zip');
    }
}