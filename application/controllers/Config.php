<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends CI_Controller {
    function __construct()    {
        parent::__construct();
        
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloConfig');
        $this->load->model('ModeloUnidadesservicios');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->fechal = date('dmYHis');
        if($this->session->userdata('logeado')==true){
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_suc=$this->session->userdata('sess_suc');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        redirect('Sistema'); 
  	}
    public function facturacion($id=0) {
        if($id==0){
            $id=1;
        }
        if($id==1){
            $urlfiles='';
        }else{
            $urlfiles='2';
        }
        $data['ConfiguracionesId']=$id;
        $data['sess_suc']=$this->sess_suc;

        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$id));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        $data['ConfiguracionesId']=$datosconfiguracion->ConfiguracionesId;
        $data['Rfc']=$datosconfiguracion->Rfc;
        $data['Curp']=$datosconfiguracion->Curp;
        $data['Nombre']=$datosconfiguracion->Nombre;
        $data['Direccion']=$datosconfiguracion->Direccion;
        $data['PaisExpedicion']=$datosconfiguracion->PaisExpedicion;
        $data['Regimen']=$datosconfiguracion->Regimen;
        $data['Calle']=$datosconfiguracion->Calle;
        $data['Estado']=$datosconfiguracion->Estado;
        $data['Municipio']=$datosconfiguracion->Municipio;
        $data['CodigoPostal']=$datosconfiguracion->CodigoPostal;
        $data['Noexterior']=$datosconfiguracion->Noexterior;
        $data['Colonia']=$datosconfiguracion->Colonia;
        $data['localidad']=$datosconfiguracion->localidad;
        $data['Nointerior']=$datosconfiguracion->Nointerior;

        $timbresvigentes=$datosconfiguracion->timbresvigentes;
        $data['timbresvigentes']=$timbresvigentes;
        $tv_inicio=$datosconfiguracion->tv_inicio;
        $tv_fin=$datosconfiguracion->tv_fin;
        $facturastimbradas=$this->ModeloCatalogos->total_facturas(1,$tv_inicio,$tv_fin,$id);
        $facturascanceladas=$this->ModeloCatalogos->total_facturas(0,$tv_inicio,$tv_fin,$id);
        $complementotimbradas=$this->ModeloCatalogos->totalcomplementos($tv_inicio,$tv_fin,$id);
        $data['facturastimbradas'] = $facturastimbradas;
        $data['facturascanceladas'] = $facturascanceladas;
        $data['complementotimbradas'] = $complementotimbradas;
        $data['facturasdisponibles'] = $timbresvigentes-$facturastimbradas-$facturascanceladas-$complementotimbradas;
        //==================================================================================
        $nombre_fichero = 'files_sat/temporalsat'.$urlfiles.'/FinVigencia.txt';
        if (file_exists($nombre_fichero)) {
            $fh = fopen(base_url().$nombre_fichero, 'r') or die("Se produjo un error al abrir el archivo");
                $linea = fgets($fh);
            fclose($fh); 
            if($linea!=''){
                $remplazar = array('notAfter=','  ','GMT'); 
                $remplazarpor = array('',' ','');
                $fechavigente = str_replace($remplazar,$remplazarpor,$linea);
                $fechavigenteex = explode(" ", $fechavigente);
                $fechafin=$fechavigenteex[1].' '.$fechavigenteex[0].' '.$fechavigenteex[3];
            }else{
                $fechafin='';    
            }
            
        }else{
            $fechafin='';
        }
        $data['fechafin']=$fechafin;
        //==================================================================================

        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('config/facturacion',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('config/facturacionjs');
    }
    function datosgenerales(){
        $data = $this->input->post();
        $id = $data['ConfiguracionesId']; 
        unset($data['ConfiguracionesId']);

        $this->ModeloCatalogos->updateCatalogo('f_configuraciones',$data,array('ConfiguracionesId'=>$id));
    }
    public function getlistservicios() {
        $params = $this->input->post();
        $getdata = $this->ModeloUnidadesservicios->getservicios($params);
        $totaldata= $this->ModeloUnidadesservicios->total_servicios($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistunidades() {
        $params = $this->input->post();
        $getdata = $this->ModeloUnidadesservicios->getunidades($params);
        $totaldata= $this->ModeloUnidadesservicios->total_unidades($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function adservicio(){
        $params = $this->input->post();
        $this->ModeloCatalogos->updateCatalogo('f_servicios',array('activo'=>$params['activo']),array('ServiciosId'=>$params['ServiciosId']));
    }
    function adunidad(){
        $params = $this->input->post();
        $this->ModeloCatalogos->updateCatalogo('f_unidades',array('activo'=>$params['activo']),array('UnidadId'=>$params['UnidadId']));
    }
    function cargacertificado(){
        $id = $_POST['id'];

        if($id==1){
            $urlfiles='';
        }else{
            $urlfiles='2';
        }

        $configUpload['upload_path'] = FCPATH.'files_sat/elementos'.$urlfiles.'/';
        $configUpload['allowed_types'] = '*';
        $configUpload['max_size'] = '5000';
        $configUpload['file_name'] = 'archivos';
        $this->load->library('upload', $configUpload);
        $this->upload->do_upload('cerdigital');  
        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension=$upload_data['file_ext'];    // uploded file extension

        $this->ModeloCatalogos->updateCatalogo('f_configuraciones',array('archivocer'=>$file_name),array('ConfiguracionesId'=>$id));

        $output = [];
        echo json_encode($output);
    }
    function cargakey(){
        $id = $_POST['id'];
        $pass = $_POST['pass'];

        if($id==1){
            $urlfiles='';
        }else{
            $urlfiles='2';
        }

        $pass = $_POST['pass'];
        $configUpload['upload_path'] = FCPATH.'files_sat/elementos'.$urlfiles.'/';
        $configUpload['allowed_types'] = '*';
        $configUpload['max_size'] = '5000';
        $configUpload['file_name'] = 'archivos';
        $this->load->library('upload', $configUpload);
        $this->upload->do_upload('claveprivada');  
        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension=$upload_data['file_ext'];    // uploded file extension

        $this->ModeloCatalogos->updateCatalogo('f_configuraciones',array('archivokey'=>$file_name,'paswordkey'=>$pass),array('ConfiguracionesId'=>$id));
        file_put_contents(FCPATH.'files_sat/elementos'.$urlfiles.'/passs.txt', $pass);
        file_put_contents(FCPATH.'files_sat/elementos'.$urlfiles.'/pass.txt', $pass);
        $output = [];
        echo json_encode($output);
    }
    function sucursales(){
        $data['sess_suc']=$this->sess_suc;
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('config/sucursales',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('config/sucursalesjs');
    }
    function getlistsucursales(){
        $params = $this->input->post();
        $getdata = $this->ModeloConfig->getsucursales($params);
        $totaldata= $this->ModeloConfig->getsucursalest($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function sucursalesdelete(){
        $params = $this->input->post();
        $id = $params['id'];
        $this->ModeloCatalogos->updateCatalogo('sucursal',array('activo'=>0,'delete_reg'=>$this->fechalarga,'delete_user'=>$this->sess_usr),array('id'=>$id));
    }
    function insertaactualizasucursal(){
        $params = $this->input->post();
        $id = $params['id'];
        unset($params['id']);
        if(isset($params['conserie'])){

        }else{
            $params['conserie']=0;                
        }
        if($id>0){
            $params['update_reg']=$this->fechalarga;
            $params['update_user']=$this->sess_usr;
            $this->ModeloCatalogos->updateCatalogo('sucursal',$params,array('id'=>$id));
        }else{
            $params['insert_reg']=$this->fechalarga;
            $params['insert_user']=$this->sess_usr;
            $this->ModeloCatalogos->Insert('sucursal',$params);
        }
    }
    
}