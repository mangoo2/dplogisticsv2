<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Operadores extends CI_Controller {

    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloOperadores');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal = date('dmYHis');
        $this->fechahoy = date('Y-m-d G:i:s');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
        }else{
            redirect('Sistema'); 
        }
    }

	function index(){
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('operadores/index');
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('operadores/indexjs');
  	}

    function registro($id=0)
    {
        if($id==0){
            $data['title']='AGREGAR';
            $data['id']=0;
            $data['dpl']='';
            $data['rfc_del_operador']='';
            $data['no_licencia']='';
            $data['operador']='';
            $data['num_identificacion']='';
            $data['residencia_fiscal']='';
            $data['calle']='';
            $data['estado']='';
            $data['pais']='MEXICO';
            $data['codigo_postal']='';

            $data['dpltxt']='';
        }else{
            $data['title']='EDITAR ';
            $result=$this->ModeloCatalogos->getselectwheren('operadores',array('id'=>$id));
            foreach ($result->result() as $item) {
                $data['id']=$item->id;
                $data['dpl']=$item->dpl;
                $data['rfc_del_operador']=$item->rfc_del_operador;
                $data['no_licencia']=$item->no_licencia;
                $data['operador']=$item->operador;
                $data['num_identificacion']=$item->num_identificacion;
                $data['residencia_fiscal']=$item->residencia_fiscal;
                $data['calle']=$item->calle;
                $data['estado']=$item->estado;
                $data['pais']=$item->pais;
                $data['codigo_postal']=$item->codigo_postal;

                $data['dpltxt']=''; 
                $resultdp=$this->ModeloCatalogos->getselectwheren('coe_ruts',array('ruta'=>$item->dpl));
                foreach ($resultdp->result() as $itemp){
                    $data['dpltxt']=$itemp->rut_cla;
                }
            }
        }
        $data['result_estados']=$this->ModeloCatalogos->getselectwheren('f_c_estado',array('activo'=>1));
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('operadores/form',$data);
        $this->load->view('theme/footer');
        $this->load->view('operadores/formjs');
    }

    public function get_listado() {
        $params = $this->input->post();
        $getdata = $this->ModeloOperadores->get_listado($params);
        $totaldata= $this->ModeloOperadores->get_listado_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function insert_registro(){
        $params = $this->input->post();
        $rid=$params['rid'];
        unset($params['rid']);
        if($rid>0){
            $this->ModeloCatalogos->updateCatalogo('operadores',$params,array('id'=>$rid));
        }else{
            $params['reg']=$this->fechahoy;
            $rid=$this->ModeloCatalogos->Insert('operadores',$params);
        }
        echo $rid;
    } 

    public function delete()
    {   
        $id=$this->input->post('id');
        $arrayinfo = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('operadores',$arrayinfo,array('id'=>$id));
    }

}