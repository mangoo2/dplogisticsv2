<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Transporte_federal extends CI_Controller {

    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloTransporte_federal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal = date('dmYHis');
        $this->fechahoy = date('Y-m-d G:i:s');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
        }else{
            redirect('Sistema'); 
        }
    }

	function index(){
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('transporte_federal/index');
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('transporte_federal/indexjs');
  	}

    function registro($id=0)
    {
        if($id==0){
            $data['title']='AGREGAR';
            $data['id']=0;

            $data['dpl']='';
            $data['dpltxt']=''; 

            $data['nombre_aseguradora']='';
            $data['num_poliza_seguro']='';

            $data['num_permiso_sct']='';
            $data['num_permiso_scttxt']='';

            $data['configuracion_vhicular']='';
            $data['configuracion_vhiculartxt']='';

            $data['placa_vehiculo_motor']='';
            $data['anio_modelo_vihiculo_motor']='';
            $data['subtipo_remolque']='';
            $data['placa_remolque']='';
            $data['tipo_permiso_sct']='';
            $data['tipo_permiso_scttxt']='';
        }else{
            $data['title']='EDITAR ';
            $result=$this->ModeloCatalogos->getselectwheren('transporte_federal',array('id'=>$id));
            foreach ($result->result() as $item) {
                $data['id']=$item->id;
                $data['dpl']=$item->dpl;
                $data['nombre_aseguradora']=$item->nombre_aseguradora;
                $data['num_poliza_seguro']=$item->num_poliza_seguro;
                $data['num_permiso_sct']=$item->num_permiso_sct;
                $data['configuracion_vhicular']=$item->configuracion_vhicular;
                $data['placa_vehiculo_motor']=$item->placa_vehiculo_motor;
                $data['anio_modelo_vihiculo_motor']=$item->anio_modelo_vihiculo_motor;
                $data['subtipo_remolque']=$item->subtipo_remolque;
                $data['placa_remolque']=$item->placa_remolque;
                $data['tipo_permiso_sct']=$item->tipo_permiso_sct;
                $data['tipo_permiso_scttxt']='';
                $resultst=$this->ModeloCatalogos->getselectwheren('f_c_tipopermiso',array('clave'=>$item->tipo_permiso_sct));
                foreach ($resultst->result() as $itemst) {
                    $data['tipo_permiso_scttxt']=$itemst->descripcion;
                }
 
                $data['configuracion_vhiculartxt']='';
                $resultv=$this->ModeloCatalogos->getselectwheren('f_c_configautotransporte',array('clave'=>$item->configuracion_vhicular));
                foreach ($resultv->result() as $itemv) {
                    $data['configuracion_vhiculartxt']=$itemv->descripcion;
                }

                $data['dpltxt']=''; 
                $resultdp=$this->ModeloCatalogos->getselectwheren('coe_ruts',array('ruta'=>$item->dpl));
                foreach ($resultdp->result() as $itemp){
                    $data['dpltxt']=$itemp->rut_cla;
                }

            }
        }
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('transporte_federal/form',$data);
        $this->load->view('theme/footer');
        $this->load->view('transporte_federal/formjs');
    }
    
    public function get_listado() {
        $params = $this->input->post();
        $getdata = $this->ModeloTransporte_federal->get_listado($params);
        $totaldata= $this->ModeloTransporte_federal->get_listado_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function insert_registro(){
        $params = $this->input->post();
        $rid=$params['rid'];
        unset($params['rid']);
        if($rid>0){
            $this->ModeloCatalogos->updateCatalogo('transporte_federal',$params,array('id'=>$rid));
        }else{
            $params['reg']=$this->fechahoy;
            $rid=$this->ModeloCatalogos->Insert('transporte_federal',$params);
        }
        echo $rid;
    } 

    public function delete()
    {   
        $id=$this->input->post('id');
        $arrayinfo = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('transporte_federal',$arrayinfo,array('id'=>$id));
    }

    function search_config_transporte(){
        $search = $this->input->get('search');
        $results = $this->ModeloTransporte_federal->get_config_transporte_like($search);
        echo json_encode($results);    
    }

    function search_f_c_tipopermiso(){
        $search = $this->input->get('search');
        $results = $this->ModeloTransporte_federal->get_f_c_tipopermiso_like($search);
        echo json_encode($results);    
    }

}