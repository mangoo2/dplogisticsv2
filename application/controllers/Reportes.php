<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require 'vendor/autoload.php';//php 7.1 como minimo
//use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Reportes extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloClientes');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal = date('dmYHis');
        $this->fechahoy = date('Y-m-d');

        //$this->consult_hijos=1;//incluir consultas de hijos 0 no 1 si
        $this->config->load('custom_config');
        $this->consult_hijos=$this->config->item('consult_hijos');

        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        
  	}

    function exportarguias(){
        $fechaini=$_GET['fechaini'];
        $data['fechaini']=$fechaini;

        $fechafin=$_GET['fechafin'];
        $data['fechafin']=$fechafin;
        
        $numcliente=$_GET['numcliente'];
        $data['numcliente']=$numcliente;
        //$this->load->view('reportes/exportarguias',$data);
        //======================consulta===========================
            if($fechaini!=''){
                $where_f_ini=" and gui.fecha >='$fechaini 00:00:00' ";
            }else{
                $where_f_ini="";
            }
            if($fechafin!=''){
                $where_f_fin="  and gui.fecha <='$fechafin 23:59:59' ";
            }else{
                $where_f_fin="";
            }

            $x=0;
            /*
            $sql="SELECT gur.emp,gur.guia,gur.con,gur.con_dir,gur.con_col,gur.con_ciu,gur.con_tel,gur.con_cp,
                         gui.folio,gui.guia,gui.origen,gui.destino,gui.paqs,gui.paq_des,gui.peso,gui.volumen,gui.tarifa,gui.importe,gui.fecha,gui.admin
                FROM guia_rycs  as gur
                inner join guias as gui on gui.rid=gur.guia and gui.sucursal='$sess_suc'
                WHERE gur.cliente='$numcliente' 
                ORDER BY gur.rid DESC LIMIT 6";
                */
            if($this->consult_hijos==0){
                $where_clientes=" and gur.cliente='$numcliente'";
            }else{
                $where_clientes=" and ( ";
                $r_h = $this->ModeloClientes->consultar_hijos($numcliente);
                $where_clientes.=" gur.cliente='$numcliente' ";
                foreach ($r_h->result() as $item_rh) {
                    $where_clientes.=" or gur.cliente='$item_rh->num_hijo' ";
                }
                $where_clientes.=" ) ";
            }
            $sql="SELECT gur.emp,gur.guia,gur.con,gur.con_dir,gur.con_col,gur.con_ciu,gur.con_tel,gur.con_cp,
                         gui.folio,gui.guia,gui.origen,gui.destino,gui.paqs,gui.paq_des,gui.peso,gui.volumen,gui.tarifa,gui.importe,gui.fecha,gui.admin
                FROM guia_rycs  as gur
                inner join guias as gui on gui.rid=gur.guia
                WHERE gur.rid>0 $where_f_ini $where_f_fin $where_clientes
                ORDER BY gur.rid DESC ";
            $result_h=$this->db->query($sql);
        //=================================================
            $spreadsheet = new Spreadsheet();
            $row_h=0;
            //if($row_h==0){
            $sheet = $spreadsheet->getActiveSheet();//esto solo cuando es una unica hoja
            //}else{
                //$spreadsheet->createSheet(); // esto es cuando es mas de una hoja con el $row_h consecutivos
                //$sheet = $spreadsheet->setActiveSheetIndex($row_h); // esto es cuando es mas de una hoja con el $row_h consecutivos
            //}

                //===========================================
                    $sheet->setCellValue('A1', '#');
                    $sheet->setCellValue('B1', 'Empresa');
                    $sheet->setCellValue('C1', 'Folio');
                    $sheet->setCellValue('D1', 'Guía');
                    $sheet->setCellValue('E1', 'Origen');
                    $sheet->setCellValue('F1', 'Destino');
                    $sheet->setCellValue('G1', 'Remitente');
                    $sheet->setCellValue('H1', 'Consignatario');
                    $sheet->setCellValue('I1', 'Paquetes');
                    $sheet->setCellValue('J1', 'Descripción');
                    $sheet->setCellValue('K1', 'Peso');
                    $sheet->setCellValue('L1', 'Volumen');
                    $sheet->setCellValue('M1', 'Tarifa');
                    $sheet->setCellValue('N1', 'Importe');
                    $sheet->setCellValue('O1', 'Fecha');
                    $sheet->setCellValue('P1', 'Admin');

                //===========================================
                    $rc=2;
                    foreach ($result_h->result() as $item) {
                        $x++;

                        $sheet->setCellValue('A'.$rc, $x);
                        $sheet->setCellValue('B'.$rc, $item->emp);
                        $sheet->setCellValue('C'.$rc, $item->folio);
                        $sheet->setCellValue('D'.$rc, $item->guia);
                        $sheet->setCellValue('E'.$rc, $item->origen);
                        $sheet->setCellValue('F'.$rc, $item->destino);
                        $sheet->setCellValue('G'.$rc, $item->con);
                        $sheet->setCellValue('H'.$rc, $item->con_dir.' '.$item->con_col.' '.$item->con_ciu.' '.$item->con_cp.' '.$item->con_tel);
                        $sheet->setCellValue('I'.$rc, $item->paqs);
                        $sheet->setCellValue('J'.$rc, $item->paq_des);
                        $sheet->setCellValue('K'.$rc, $item->peso);
                        $sheet->setCellValue('L'.$rc, $item->volumen);
                        $sheet->setCellValue('M'.$rc, $item->tarifa);
                        $sheet->setCellValue('N'.$rc, $item->importe);
                        $sheet->setCellValue('O'.$rc, $item->fecha);
                        $sheet->setCellValue('P'.$rc, $item->admin);
                        $rc++;
                    }
                //===========================================


        //=================================================
                    // Guardar el archivo XLSX
                $writer = new Xlsx($spreadsheet);
                $urlarchivo0='fichero_reporte/Reporte_'.$numcliente.'_'.$this->fechahoy.'.xlsx';
                $urlarchivo=FCPATH.$urlarchivo0;
                $writer->save($urlarchivo);

                redirect(base_url().$urlarchivo0); 
    }


}