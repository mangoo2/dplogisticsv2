<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Coe_brdu extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloCoepam');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['resultsuc']=$this->ModeloCatalogos->genSelect('coe_sucs');
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_brdu/lista',$data);
        $this->load->view('theme/footer');
        $this->load->view('coe_brdu/listajs');
        
  	}
    function suc($suc=''){
        if($suc==''){
            $suc=$this->sess_suc;
            $data['view']=0;
        }else{
            $data['view']=1;
        }
        $result_suc=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$suc));
        $suc_name='';
        foreach ($result_suc->result() as $item) {
            $suc_name=$item->suc_nombre;
        }
        $data['sucu']=$suc;
        $data['suc_name']=$suc_name;
        $data['result_equ']=$this->ModeloCatalogos->getselectwheren('pam_equ',array('sucursal'=>$suc));
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_brdu/listasuc',$data);
        $this->load->view('theme/footer');
        $this->load->view('coe_brdu/listasucjs');
    }
    function equ($suc,$equ){
        if($suc==''){
            $suc=$this->sess_suc;
        }
        $result_suc=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$suc));
        $suc_name='';
        foreach ($result_suc->result() as $item) {
            $suc_name=$item->suc_nombre;
        }
        $data['sucu']=$suc;
        $data['equ']=$equ;
        $data['suc_name']=$suc_name;
        $data['result_equ']=$this->ModeloCatalogos->getselectwheren('pam_equ',array('equipo'=>$equ));
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_brdu/listasuceq',$data);
        $this->load->view('theme/footer');
        $this->load->view('coe_brdu/listasuceqjs');
    }
    function ver(){
        $params = $this->input->post();
        $sucu = $params['sucu'];
        $equ = $params['equ'];
        $rev = $params['rev'];

        if($sucu != '' && $equ != '' && $rev>0){
            $resS = $this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$sucu));
            if($resS->num_rows()>0){
                
                $resR_info = "SELECT rev,rev_fecha FROM brdu_revs WHERE rid='$rev' AND sucursal='$sucu' AND rev_est='fin' LIMIT 1";//solo es infomativo para ver la consulta en el log
                log_message('error',$resR_info);

                $resR = $this->ModeloCatalogos->getselectwheren_db2('brdu_revs',array('rid'=>$rev,'sucursal'=>$sucu,'rev_est'=>'fin'));
                if($resR->num_rows()>0){
                    // REV
                    //list($doc,$rev_fecha)=$__BASE->registro($resR);
                    $doc='';$rev_fecha='';
                    foreach ($resR->result() as $itemresR) {
                        $doc=$itemresR->rev;
                        $rev_fecha=$itemresR->rev_fecha;
                    }
                    // SUC
                    //list($suc_nombre)=$__BASE->registro($resS);
                    // EQU
                    //list($equ_nombre,$equ_numeco)=$__BASE->registro($__BASE->consulta("SELECT equ_nombre,equ_numeco FROM ".PAM_EQU." WHERE sucursal='$suc' AND equipo='$equ' LIMIT 1"));
                    $res_pam_equ=$this->ModeloCatalogos->getselectwheren('pam_equ',array('sucursal'=>$sucu,'equipo'=>$equ));
                    $equ_nombre='';$equ_numeco='';
                    foreach ($res_pam_equ->result() as $itempeq) {
                        $equ_nombre =$itempeq->equ_nombre;
                        $equ_numeco = $itempeq->equ_numeco;
                    }

                    // REV EQU
                    //list($uid)=$__BASE->registro($__BASE->consulta("SELECT rid FROM ".BRDU_RUNI." WHERE rev='$rev' AND equipo='$equ'",BD_BRDU));
                    $resrunis = $this->ModeloCatalogos->getselectwheren_db2('rev_unis',array('rev'=>$rev,'equipo'=>$equ));
                    $uid=0;
                    foreach ($resrunis->result() as $itemunis) {
                        $uid=$itemunis->rid;
                    }

                    // REV PRE
                    $tdsRev='';
                    //$resRP = $__BASE->consulta("SELECT rid,pre,pre_pos,pre_tit FROM ".BRDU_RPRE." WHERE rev='$rev' ORDER BY pre_pos ASC",BD_BRDU);
                    //$resRP=$this->ModeloCatalogos->consult_bd2_BRDU_RPRE($rev);
                    $resRP=$this->ModeloCatalogos->consult_bd2_BRDU_RPRE_BRDU_RRES($rev,$uid);
                    foreach ($resRP->result() as $itemRP) {
                        $pid=$itemRP->rid;
                        $pre = $itemRP->pre;
                        $pre_pos = $itemRP->pre_pos;
                        $pre_tit = $itemRP->pre_tit;
                        /*
                        $resBR=$this->ModeloCatalogos->consult_bd2_BRDU_RRES($rev,$uid,$pid);
                        $res_rev='';$res_obs='';
                        foreach ($resBR->result() as $itembr) {
                            $res_rev=$itembr->res;
                            $res_obs=$itembr->res_obs;
                        }
                        */
                        $res_rev = $itemRP->res;
                        $res_obs = $itemRP->res_obs;

                        $tdsRev .= '<tr><td>'.$pre_pos.'</td><td>'.$pre_tit.'</td><td>'.$res_rev.'</td><td>'.$res_obs.'</td></tr>';
                    }
                    

                    $titulo='DOC :: '.$doc;
                    $contenido='';
                        $contenido.='<p>Revisión: '. date('d/m/Y G:i:s a',strtotime($rev_fecha)).'</p>';
                        $contenido.='<p>'.$equ_numeco.' > '.$equ_nombre.'</p>';
                        $contenido.='<table class="table table-bordered table-hover" id="tabRegs">';
                            $contenido.='<thead>';
                                $contenido.='<tr><th rowspan="2">#</th><th rowspan="2" style="width:303px;">Actividad</th></tr>';
                                $contenido.='<tr><th>Revisión</th><th>Observaciones</th></tr>';
                            $contenido.='</thead>';
                            $contenido.='<tbody>'.$tdsRev.'</tbody>';
                        $contenido.='</table>';

                    
                }else{
                    $titulo='Error';
                    $contenido='Error, documento no encontrado...';
                }
            }else{
                $titulo='Error';
                $contenido='Error de sucursal...';
            }
        }else{
            $titulo='Error';
            $contenido='Error de datos...';
        }








        $array=array(
                    'titulo'=>$titulo,
                    'contenido'=>$contenido
                    );
        echo json_encode($array);
    }
    




}