<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Envio extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloClientes');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sess_nom=$this->session->userdata('sess_nom');
            $this->sess_usr=$this->session->userdata('sess_usr');
            
        }else{
            redirect('/Sistema');
        }
    }

	public function index(){
        redirect('/Sistema');
    }

    public function enviocorreo(){
            $params = $this->input->post();
            
            $factura = $params['factura'];
            $correo = $params['correo'];
            $asunto = $params['asunto'];
            $mensaje = $params['mensaje'];
            
            $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
            $datosconfiguracion=$datosconfiguracion->result();
            $datosconfiguracion=$datosconfiguracion[0];

            $this->ModeloCatalogos->updateCatalogo('f_facturas',array('correoenviado'=>1),array('FacturasId'=>$factura));

            $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$factura));
            $datosfactura=$datosfactura->result();
            $datosfactura=$datosfactura[0];
            $Folio=$datosfactura->Folio;
            $serie=$datosfactura->serie;
            //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.cadipalogistics.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'info@cadipalogistics.com';

            //Nuestra contraseña
            $config["smtp_pass"] = 'PC.4RKy,MspX';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
     
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('info@cadipalogistics.com','coecsa');
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
        //======================
       
        
        //$this->email->to('agerardob@gmail.com', 'enviofactura');
        //$this->email->to('contacto@anahuac.sicoi.net', 'enviofactura');
        /*
        $contactosarray = array();
        $DATAc = json_decode($correo); 
        for ($i=0;$i<count($DATAc);$i++) {
            //$this->email->to($DATAc[$i]->correo, '');
            $contactosarray[]=$DATAc[$i]->correo; 
        }
        $this->email->to($contactosarray);
        */
        $correosarray=explode(",", $correo);
        $DATAc = $correosarray; 
        $contactosarray = array();
        for ($i=0;$i<count($DATAc);$i++) {
            $contactosarray[]=$DATAc[$i];
        }

        $this->email->to($contactosarray);
        $this->email->bcc('agerardob@gmail.com');


        //$asunto='Factura';
        
        

      //Definimos el asunto del mensaje
        $this->email->subject($asunto);
         
      //Definimos el mensaje a enviar
      //$this->email->message($body)


        //$message  = $datosconfiguracion->cuerpo;

        $this->email->message($mensaje);

        
        $this->email->attach(base_url().$datosfactura->rutaXml);
        $this->email->attach(base_url().'files_sat/facturaspdf/Factura_'.$serie.'_'.$Folio.'.pdf');
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }

        //==================
        
    
    }
    public function enviocorreo_comp(){
            $params = $this->input->post();
            
            $factura = $params['factura'];
            $correo = $params['correo'];
            $asunto = $params['asunto'];
            $mensaje = $params['mensaje'];
            
            $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
            $datosconfiguracion=$datosconfiguracion->result();
            $datosconfiguracion=$datosconfiguracion[0];

            //$this->ModeloCatalogos->updateCatalogo('f_facturas',array('correoenviado'=>1),array('FacturasId'=>$factura));
            $datoscomplemento=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$factura));
            $datoscomplemento=$datoscomplemento->row();

            $Folio=$datoscomplemento->Folio;
            $serie=$datoscomplemento->Serie;
            //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.cadipalogistics.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'info@cadipalogistics.com';

            //Nuestra contraseña
            $config["smtp_pass"] = 'PC.4RKy,MspX';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
     
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('info@cadipalogistics.com','coecsa');
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
        //======================
       
        
        //$this->email->to('agerardob@gmail.com', 'enviofactura');
        //$this->email->to('contacto@anahuac.sicoi.net', 'enviofactura');
        /*
        $contactosarray = array();
        $DATAc = json_decode($correo); 
        for ($i=0;$i<count($DATAc);$i++) {
            //$this->email->to($DATAc[$i]->correo, '');
            $contactosarray[]=$DATAc[$i]->correo; 
        }
        $this->email->to($contactosarray);
        */
        $correosarray=explode(",", $correo);
        $DATAc = $correosarray; 
        $contactosarray = array();
        for ($i=0;$i<count($DATAc);$i++) {
            $contactosarray[]=$DATAc[$i];
        }

        $this->email->to($contactosarray);
        $this->email->bcc('agerardob@gmail.com');


        //$asunto='Factura';
        
        

      //Definimos el asunto del mensaje
        $this->email->subject($asunto);
         
      //Definimos el mensaje a enviar
      //$this->email->message($body)


        //$message  = $datosconfiguracion->cuerpo;

        $this->email->message($mensaje);

        
        $this->email->attach(base_url().$datoscomplemento->rutaXml);
        $this->email->attach(base_url().'files_sat/facturaspdf/Complemento_'.$serie.'_'.$Folio.'.pdf');
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }

        //==================
        
    
    }
    function notificarcredito(){
        $params = $this->input->post();
            
            $cot = $params['cot'];
            $datoscot=$this->ModeloCatalogos->getselectwheren('doc_cotiza',array('cotiza'=>$cot));
            $datoscot=$datoscot->row();

            $datosclir=$this->ModeloCatalogos->getselectwheren('cli_clis',array('cliente'=>$datoscot->cot_remite));
            $cliente_r='';
            foreach ($datosclir->result() as $item) {
                $cliente_r=$item->cli_nombre.' '.$item->cli_paterno.' '.$item->cli_materno;
            }


            //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.cadipalogistics.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'info@cadipalogistics.com';

            //Nuestra contraseña
            $config["smtp_pass"] = 'PC.4RKy,MspX';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
     
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('info@cadipalogistics.com','coecsa');
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
        //======================
        $pruebas=0;//0 se quitan los correo 1 se adjuntan los correos
        if($pruebas==1){
            //$correosarray=explode(",", $correo);
            //$DATAc = $correosarray; 
            $contactosarray = array();
            //for ($i=0;$i<count($DATAc);$i++) {
            //    $contactosarray[]=$DATAc[$i];
            //}
            $contactosarray[]='jorgeluis.diaz@coecsa.mx';
            $contactosarray[]='viridianamoreno@coecsa.mx';
            $contactosarray[]='guadalupemora@coecsa.mx';

            $this->email->to($contactosarray);
        }
        $this->email->bcc('info@cadipalogistics.com');
    
        //Definimos el asunto del mensaje
        $this->email->subject('Se autoriza crédito');
         $url_documento=base_url().'Documentar/add/'.$cot.'?status=pdg';
        $mensaje='Se realizo una autorizacion de credito.<br>
                  para la siguiente documentacion: <a href="'.$url_documento.'" target="_black">'.$datoscot->cot_folio.' '.$datoscot->cot_feccap.'</a>, del Cliente remitente: '.$cliente_r.'<br>
                  Persona que realizo: '.$this->sess_nom.' ('.$this->sess_usr.')
                 ';
        $this->email->message($mensaje);

        
        //$this->email->attach(base_url().$datoscomplemento->rutaXml);
        //$this->email->attach(base_url().'files_sat/facturaspdf/Complemento_'.$serie.'_'.$Folio.'.pdf');
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
            log_message('error','salio email');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
            log_message('error','no salio email');
        }

        //==================
    }

}    