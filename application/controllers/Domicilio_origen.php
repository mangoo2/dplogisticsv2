<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Domicilio_origen extends CI_Controller {

    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloDomicilio_origen');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal = date('dmYHis');
        $this->fechahoy = date('Y-m-d G:i:s');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
        }else{
            redirect('Sistema'); 
        }
    }

	function index(){
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('domicilio_origen/index');
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('domicilio_origen/indexjs');
  	}

    function registro($id=0)
    {
        if($id==0){
            $data['title']='AGREGAR';
            $data['id']=0;
            $data['dpl']='';
            $data['calle']='';
            $data['num_ext']='';
            $data['num_int']='';

            $data['colonia']='';
            $data['coloniatxt']='';

            $data['localidad']='';
            $data['localidadtxt']='';

            $data['referencia']='';

            $data['municipio']='';
            $data['municipiotxt']='';

            $data['estado']='';
            $data['estadotxt']='';
            $data['c_Estado']='';
            
            $data['pais']='';
            $data['paistxt']='';
            $data['c_Pais']='';
            $data['codigo_postal']='';

            $data['dpltxt']='';
        }else{
            $data['title']='EDITAR ';
            $result=$this->ModeloCatalogos->getselectwheren('domicilio_origen',array('id'=>$id));
            foreach ($result->result() as $item) {
                $data['id']=$item->id;
                $data['dpl']=$item->dpl;
                $data['calle']=$item->calle;
                $data['num_ext']=$item->num_ext;
                $data['num_int']=$item->num_int;
                $data['colonia']=$item->colonia;
                $data['localidad']=$item->localidad;
                $data['referencia']=$item->referencia;
                $data['municipio']=$item->municipio;
                $data['estado']=$item->estado;
                $data['pais']=$item->pais;
                $data['codigo_postal']=$item->codigo_postal;
                
                $data['coloniatxt']=''; 
                $resultc=$this->ModeloCatalogos->getselectwheren('f_colonia',array('id'=>$item->colonia));
                foreach ($resultc->result() as $itemp){
                    $data['coloniatxt']=$itemp->nombre;
                }

                $data['paistxt']='';
                $data['c_Pais']='';
                $resultp=$this->ModeloCatalogos->getselectwheren('f_c_pais',array('id'=>$item->pais));
                foreach ($resultp->result() as $itemp){
                    $data['paistxt']=$itemp->descripcion;
                    $data['c_Pais']=$itemp->c_Pais;
                }
                
                $data['estadotxt']='';
                $data['c_Estado']=''; 
                $resulte=$this->ModeloCatalogos->getselectwheren('f_c_estado',array('id'=>$item->estado));
                foreach ($resulte->result() as $itemp){
                    $data['estadotxt']=$itemp->descripcion;
                    $data['c_Estado']=$itemp->c_Estado;
                }
                
                $data['localidadtxt']=''; 
                $resultl=$this->ModeloCatalogos->getselectwheren('f_c_localidad',array('id'=>$item->localidad));
                foreach ($resultl->result() as $iteml){
                    $data['localidadtxt']=$iteml->descripcion;
                }

                $data['municipiotxt']=''; 
                $resultc=$this->ModeloCatalogos->getselectwheren('f_c_municipio',array('id'=>$item->municipio));
                foreach ($resultc->result() as $itemm){
                    $data['municipiotxt']=$itemm->descripcion;
                }
                
                $data['dpltxt']=''; 
                $resultdp=$this->ModeloCatalogos->getselectwheren('coe_ruts',array('ruta'=>$item->dpl));
                foreach ($resultdp->result() as $itemp){
                    $data['dpltxt']=$itemp->rut_cla;
                }
            }
        }
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('domicilio_origen/form',$data);
        $this->load->view('theme/footer');
        $this->load->view('domicilio_origen/formjs');
    }

    public function get_listado() {
        $params = $this->input->post();
        $getdata = $this->ModeloDomicilio_origen->get_listado($params);
        $totaldata= $this->ModeloDomicilio_origen->get_listado_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function insert_registro(){
        $params = $this->input->post();
        $rid=$params['rid'];
        unset($params['rid']);
        if($rid>0){
            $this->ModeloCatalogos->updateCatalogo('domicilio_origen',$params,array('id'=>$rid));
        }else{
            $params['reg']=$this->fechahoy;
            $rid=$this->ModeloCatalogos->Insert('domicilio_origen',$params);
        }
        echo $rid;
    } 

    public function delete()
    {   
        $id=$this->input->post('id');
        $arrayinfo = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('domicilio_origen',$arrayinfo,array('id'=>$id));
    }

    function search_colonia(){
        $search = $this->input->get('search');
        $results = $this->ModeloDomicilio_origen->get_colonia_like($search);
        echo json_encode($results);    
    }

    function search_localidad(){
        $search = $this->input->get('search');
        $estado = $this->input->get('estado');
        $results = $this->ModeloDomicilio_origen->get_localidad_like($search,$estado);
        echo json_encode($results);    
    }

    function search_municipio(){
        $search = $this->input->get('search');
        $estado = $this->input->get('estado');
        $results = $this->ModeloDomicilio_origen->get_municipio_like($search,$estado);
        echo json_encode($results);    
    }

    function search_pais(){
        $search = $this->input->get('search');
        $results = $this->ModeloDomicilio_origen->get_pais_like($search);
        echo json_encode($results);    
    }

    function search_estado(){
        $search = $this->input->get('search');
        $pais = $this->input->get('pais');
        $results = $this->ModeloDomicilio_origen->get_estado_like($search,$pais);
        echo json_encode($results);    
    }

}