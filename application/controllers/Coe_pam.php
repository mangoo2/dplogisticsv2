<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Coe_pam extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloCoepam');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_pam/lista',$data);
        $this->load->view('theme/footer');
        $this->load->view('coe_pam/listajs');
        
  	}
    function gsucs($ano){
        $data['ano']=$ano;
        $data['resultsuc']=$this->ModeloCatalogos->genSelect('coe_sucs');
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_pam/listaa',$data);
        $this->load->view('theme/footer');
        $this->load->view('coe_pam/listaajs');
    }
    function pam($ano,$sucu='xxx'){
        
        if($ano=='xxx'){
            $data['ano']=date('Y');
        }else{
            $data['ano']=$ano;
        }
        if($sucu=='xxx'){
            $sucu=$this->sess_suc;
        }
        $data['sucu']=$sucu;
        $data['sess_suc']=$this->sess_suc;
        $resultsuc=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$sucu));
        $data['resultsuc']=$resultsuc->row();
        $data['listequipos']=$this->ModeloCoepam->listequipos($sucu);
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_pam/pam',$data);
        $this->load->view('theme/footer');
        $this->load->view('coe_pam/pamjs');
    }
    function pam_pammod(){
        $params =$this->input->post();
        $suc=$params['suc'];
        $rid=$params['rid'];
        $html='';
        if($suc!='' && $rid > 0){
             $result_pam=$this->ModeloCatalogos->getselectwheren('pam',array('rid'=>$rid,));
             $ene=array(); $feb=array(); $mar=array(); $abr=array(); $may=array(); $jun=array(); $jul=array(); $ago=array(); $sep=array(); $oct=array(); $nov=array(); $dic=array();
             foreach ($result_pam->result() as $item) {
                $ene[]=$item->ene;
                $feb[]=$item->feb;
                $mar[]=$item->mar;
                $abr[]=$item->abr;
                $may[]=$item->may;
                $jun[]=$item->jun;
                $jul[]=$item->jul;
                $ago[]=$item->ago;
                $sep[]=$item->sep;
                $oct[]=$item->oct;
                $nov[]=$item->nov;
                $dic[]=$item->dic;
             }
        }
        //============================
            $html.='<div class="col-md-4">';
                $html.='<label>Enero</label>';
                $html.='<select class="form-control" id="ene" name="ene[]" multiple>';
                $html.=$this->ModeloCoepam->pam_sercla($ene);
                $html.='</select>';
            $html.='</div>';
        //============================
            $html.='<div class="col-md-4">';
                $html.='<label>Febrero</label>';
                $html.='<select class="form-control" id="feb" name="feb[]" multiple>';
                $html.=$this->ModeloCoepam->pam_sercla($feb);
                $html.='</select>';
            $html.='</div>';
        //============================
            $html.='<div class="col-md-4">';
                $html.='<label>Merzo</label>';
                $html.='<select class="form-control" id="mar" name="mar[]" multiple>';
                $html.=$this->ModeloCoepam->pam_sercla($mar);
                $html.='</select>';
            $html.='</div>';
        //============================
            $html.='<div class="col-md-4">';
                $html.='<label>Abril</label>';
                $html.='<select class="form-control" id="abr" name="abr[]" multiple>';
                $html.=$this->ModeloCoepam->pam_sercla($abr);
                $html.='</select>';
            $html.='</div>';
        //============================
            $html.='<div class="col-md-4">';
                $html.='<label>Mayo</label>';
                $html.='<select class="form-control" id="may" name="may[]" multiple>';
                $html.=$this->ModeloCoepam->pam_sercla($may);
                $html.='</select>';
            $html.='</div>';
        //============================
            $html.='<div class="col-md-4">';
                $html.='<label>Junio</label>';
                $html.='<select class="form-control" id="jun" name="jun[]" multiple>';
                $html.=$this->ModeloCoepam->pam_sercla($jun);
                $html.='</select>';
            $html.='</div>';
        //============================
            $html.='<div class="col-md-4">';
                $html.='<label>Julio</label>';
                $html.='<select class="form-control" id="jul" name="jul[]" multiple>';
                $html.=$this->ModeloCoepam->pam_sercla($jul);
                $html.='</select>';
            $html.='</div>';
        //============================
            $html.='<div class="col-md-4">';
                $html.='<label>Agosto</label>';
                $html.='<select class="form-control" id="ago" name="ago[]" multiple>';
                $html.=$this->ModeloCoepam->pam_sercla($ago);
                $html.='</select>';
            $html.='</div>';
        //============================
            $html.='<div class="col-md-4">';
                $html.='<label>Septiembre</label>';
                $html.='<select class="form-control" id="sep" name="sep[]" multiple>';
                $html.=$this->ModeloCoepam->pam_sercla($sep);
                $html.='</select>';
            $html.='</div>';
        //============================
            $html.='<div class="col-md-4">';
                $html.='<label>Octubre</label>';
                $html.='<select class="form-control" id="oct" name="oct[]" multiple>';
                $html.=$this->ModeloCoepam->pam_sercla($oct);
                $html.='</select>';
            $html.='</div>';
        //============================
            $html.='<div class="col-md-4">';
                $html.='<label>Noviembre</label>';
                $html.='<select class="form-control" id="nov" name="nov[]" multiple>';
                $html.=$this->ModeloCoepam->pam_sercla($nov);
                $html.='</select>';
            $html.='</div>';
        //============================
            $html.='<div class="col-md-4">';
                $html.='<label>Diciembre</label>';
                $html.='<select class="form-control" id="dic" name="dic[]" multiple>';
                $html.=$this->ModeloCoepam->pam_sercla($dic);
                $html.='</select>';
            $html.='</div>';
        echo $html;
    }
    function pam_pamgua(){
        $params = $this->input->post();
        log_message('error',json_encode($params));
        $rid = $params['rid'];
        $datos=array();
        if(isset($params['ene'])){ 
            $datos['ene']=implode(',',$params['ene']);
        }
        if(isset($params['feb'])){ 
            $datos['feb']=implode(',',$params['feb']);
        }
        if(isset($params['mar'])){ 
            $datos['mar']=implode(',',$params['mar']);
        }
        if(isset($params['abr'])){ 
            $datos['abr']=implode(',',$params['abr']);
        }
        if(isset($params['may'])){ 
            $datos['may']=implode(',',$params['may']);
        }
        if(isset($params['jun'])){ 
            $datos['jun']=implode(',',$params['jun']);
        }
        if(isset($params['jul'])){ 
            $datos['jul']=implode(',',$params['jul']);
        }
        if(isset($params['ago'])){ 
            $datos['ago']=implode(',',$params['ago']);
        }
        if(isset($params['sep'])){ 
            $datos['sep']=implode(',',$params['sep']);
        }
        if(isset($params['oct'])){ 
            $datos['oct']=implode(',',$params['oct']);
        }
        if(isset($params['nov'])){ 
            $datos['nov']=implode(',',$params['nov']);
        }
        if(isset($params['dic'])){ 
            $datos['dic']=implode(',',$params['dic']);
        }
        $this->ModeloCatalogos->updateCatalogo('pam',$datos,array('rid'=>$rid));
    }
    function pam_equgua(){
        $params = $this->input->post();
        $rid = $params['rid'];
        unset($params['rid']);
        if($rid>0){
            unset($params['sucursal']);
            $this->ModeloCatalogos->updateCatalogo('pam_equ',$params,array('rid'=>$rid));
        }else{
            $params['equipo']=$this->fechal;
            $this->ModeloCatalogos->Insert('pam_equ',$params);
        }
    }
    function pam_equbor(){
        $params = $this->input->post();
        $rid = $params['rid'];
        $this->ModeloCatalogos->getdeletewheren('pam_equ',array('rid'=>$rid));
    }
    function equ_evis($suc,$eid){
        //C:\xampp\htdocs\dplogistics\JSIMods\coe_pam\ops\equs.php
        $data['suc']=$suc;
        $data['sucu']=$suc;
        $data['eid']=$eid;

        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_pam/equ_evis',$data);
        $this->load->view('theme/footer');
        $this->load->view('coe_pam/equ_evisjs');
    }
    function equ_eviagr(){
        $eid = $_POST['eid'];
        $rid = $_POST['rid'];
        $evi_nombre = $_POST['evi_nombre'];
        $sucursal = $_POST['sucursal'];
        $equipo = $_POST['equipo'];


        $DIR_SUC0=FCPATH.'_files/_suc/'.$sucursal;
        $DIR_SUC1=FCPATH.'_files/_suc/'.$sucursal.'/pam';
        $DIR_SUC2=FCPATH.'_files/_suc/'.$sucursal.'/pam/equs';
        $DIR_SUC3=FCPATH.'_files/_suc/'.$sucursal.'/pam/equs/'.$equipo;
        //===================================================
            if (!is_dir($DIR_SUC0) && strlen($DIR_SUC0)>0){
                //log_message('error',$DIR_SUC);
                //mkdir($DIR_SUC, 0755);
                if(!is_dir($DIR_SUC0)){
                    mkdir($DIR_SUC0, 0755);
                }
            }else{
                file_put_contents($DIR_SUC0.'/index.html', '');
            }
        //==================================================
        //===================================================
            if (!is_dir($DIR_SUC1) && strlen($DIR_SUC1)>0){
                //log_message('error',$DIR_SUC);
                //mkdir($DIR_SUC, 0755);
                if(!is_dir($DIR_SUC1)){
                    mkdir($DIR_SUC1, 0755);
                }
            }else{
                file_put_contents($DIR_SUC1.'/index.html', '');
            }
        //==================================================
        //===================================================
            if (!is_dir($DIR_SUC2) && strlen($DIR_SUC2)>0){
                //log_message('error',$DIR_SUC);
                //mkdir($DIR_SUC, 0755);
                if(!is_dir($DIR_SUC2)){
                    mkdir($DIR_SUC2, 0755);
                }
            }else{
                file_put_contents($DIR_SUC2.'/index.html', '');
            }
        //==================================================
        //===================================================
            if (!is_dir($DIR_SUC3) && strlen($DIR_SUC3)>0){
                //log_message('error',$DIR_SUC);
                //mkdir($DIR_SUC, 0755);
                if(!is_dir($DIR_SUC3)){
                    mkdir($DIR_SUC3, 0755);
                }
            }else{
                file_put_contents($DIR_SUC3.'/index.html', '');
            }
        //==================================================
    

        $config['upload_path']          = $DIR_SUC3;
        $config['allowed_types']        = '*';
        $config['max_size']             = 10000;
        $config['file_name']=date('YmdGis').'_'.rand(0, 99);       
        $output = [];
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('evi_archivo')){
            $data = array('error' => $this->upload->display_errors());
            log_message('error', 'Error en la carga '.json_encode($data));
            if($rid>0){
                $this->ModeloCatalogos->updateCatalogo('pam_equevi',array('evi_nombre'=>$evi_nombre),array('rid'=>$rid));
            }else{
                $this->ModeloCatalogos->Insert('pam_equevi',array(
                        'eid'=>$eid,
                        'evi_nombre'=>$evi_nombre,
                        'evi_archivo'=>''
                ));
            }
                        
        }else{
             $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension

            if($rid>0){
                $this->ModeloCatalogos->updateCatalogo('pam_equevi',array('evi_nombre'=>$evi_nombre,'evi_archivo'=>$file_name),array('rid'=>$rid));
            }else{
                $this->ModeloCatalogos->Insert('pam_equevi',array(
                        'eid'=>$eid,
                        'evi_nombre'=>$evi_nombre,
                        'evi_archivo'=>$file_name
                ));
            }

            $data = array('upload_data' => $this->upload->data());
            //$output = [];
            log_message('error', 'Carga correcta '.json_encode($data));
        }
        echo json_encode($output);
    }
    function equ_evibor(){
        $params = $this->input->post();
        $rid=$params['rid'];
        $this->ModeloCatalogos->getdeletewheren('pam_equevi',array('rid'=>$rid));
    }
    function reporte_pam($sucu,$ano,$esta){
        $fechaactual=date('d/m/Y h:i:s A');
        $html='';
        header("Pragma: public");
        header("Expires: 0");
        $filename = "PAM_".$esta."_".date('m')."_".date('Y').".xls";
        header("Content-type: application/x-msdownload");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        $html='<table border="0">
                <tr>
                    <td colspan="3" rowspan="3"><img src="'.base_url().'public/img/logo_reportes.jpg" style="width: 56px;"></td>
                    <td colspan="12">'.utf8_decode('CORPORACION OAXAQUEÑA DE EXPRESS Y CARGA SA DE CV').'</td>
                </tr>
                <tr>
                    <td colspan="12" style="color:red;">Programa Anual de Mantenimiento ( PAM )</td>
                </tr>
                <tr>
                    <td colspan="12" style="color:red;"></td>
                </tr>
                </table>';
        $html.='<table border="0"><tr>
                            <td colspan="2"></td>
                            <td colspan="2" style="background-color: #e9ecef;border-color: #E0E0E0;"><b>'.utf8_decode('ESTACIÓN').'</b></td>
                            <td colspan="3">'.$esta.' / '.$ano.'</td>
                            <td colspan="2" style="background-color: #e9ecef;border-color: #E0E0E0;"><b>'.utf8_decode('IMPRECIÓN').'</b></td>
                            <td colspan="3">'.$fechaactual.'</td>
                            <td></td>
                <tr></table>';
        $html.='<table class="table m-0 table-bordered tablevc" id="table_list" border="1">
                    <thead class="thead-light">
                        <tr><th rowspan="2">No.</th><th colspan="13">PROGRAMA ANUAL DE MANTENIMIENTO ( PAM )</th></tr>
                        <tr>
                            
                            <th>UNIDAD</th>
                            <th>ENERO</th>
                            <th>FEBRERO</th>
                            <th>MARZO</th>
                            <th>ABRIL</th>
                            <th>MAYO</th>
                            <th>JUNIO</th>
                            <th>JULIO</th>
                            <th>AGOSTO</th>
                            <th>SEPIEMBRE</th>
                            <th>OCTUBRE</th>
                            <th>NOVIEMBRE</th>
                            <th>DICIEMBRE</th>
                        </tr>
                    </thead>
                    <tbody>';
                         
                        $html.=$this->ModeloCoepam->reporte_prog_anual_mantenimiento($ano,$sucu,1);
                        
                        
                    $html.='</tbody>
                </table>';

        echo $html;
    }
    function reporte_unidades($sucu,$esta){
        $ex_xls=1;
        if($ex_xls==1){
            header("Pragma: public");
            header("Expires: 0");
            $filename = "UEI_".$esta."_".date('m')."_".date('Y').".xls";
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment; filename=$filename");
            header("Pragma: no-cache");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        }
        $fechaactual=date('d/m/Y h:i:s A');

        $html='<table border="0">
                <tr>
                    <td colspan="3" rowspan="3"><img src="'.base_url().'public/img/logo_reportes.jpg" style="width: 56px;"></td>
                    <td colspan="12">'.utf8_decode('DESCRIPCIÓN DE UNIDADES, EQUIPOS E INSTALACIONES').'</td>
                </tr>
                <tr>
                    <td colspan="12" style="color:red;"></td>
                </tr>
                <tr>
                    <td colspan="12" style="color:red;"></td>
                </tr>
                </table>';
        $html.='<table border="0"><tr>
                            <td colspan="1"></td>
                            <td colspan="1" style="background-color: #e9ecef;border-color: #E0E0E0;"><b>'.utf8_decode('ESTACIÓN').'</b></td>
                            <td colspan="2">'.$esta.'</td>
                            <td colspan="1" style="background-color: #e9ecef;border-color: #E0E0E0;"><b>'.utf8_decode('IMPRECIÓN').'</b></td>
                            <td colspan="2">'.$fechaactual.'</td>
                            <td></td>
                <tr></table>';
        $html.='<table class="table table-bordered tablevc" border="1">
                    <thead class="thead-light">
                        <tr>
                            <th rowspan="2">No.</th>
                            <th colspan="8">'.utf8_decode(' DESCRIPCIÓN DE UNIDADES, EQUIPOS E INSTALACIONES').'</th>
                        </tr>
                        <tr>
                            
                            <th>'.utf8_decode('No. Económico').'</th>
                            <th>Nombre</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Placa / Serie</th>
                            <th>'.utf8_decode('Descripción').'</th>
                            <th>Estatus</th>
                            <th>Foto</th></tr></thead><tbody>';
        $html.=$this->ModeloCoepam->reportegeneraunidades($sucu,1);
        $html.='</tbody></table>';
        echo $html;
    }
    function bmau($cic,$suc,$equ,$pam){

        $data['cic'] = $cic;
        $data['suc'] = $suc;
        $data['equ'] = $equ;
        $data['pam'] = $pam;

        $data['resultpamequ']=$this->ModeloCatalogos->getselectwheren('pam_equ',array('equipo'=>$equ));
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_pam/bmau',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('coe_pam/bmaujs');
    }
    function bma_agr(){
        $params=$this->input->post();
        $rid = $params['rid'];
        unset($params['rid']);
        unset($params['suc']);
        unset($params['equ']);
        if($rid>0){
            unset($params['pam']);
            $this->ModeloCatalogos->updateCatalogo('pam_bit',$params,array('rid'=>$rid));
        }else{
             $this->ModeloCatalogos->Insert('pam_bit',$params);
        }
    }
    function conultar_pam_bit(){
        $params=$this->input->post();
        $rid = $params['rid'];
        $result=$this->ModeloCatalogos->getselectwheren('pam_bit',array('rid'=>$rid,));
        $result = $result->row();
        echo json_encode($result);

    }
    function bma_expxls($cic,$suc,$equ,$pam){
        //==================================================
            $resultpamequ=$this->ModeloCatalogos->getselectwheren('pam_equ',array('equipo'=>$equ));
            $equ_nombre='';
            $equ_marca='';
            $equ_modelo='';
            $suc_nombre='';$equ_numeco='';$equ_plaser='';$equ_estatus='';$suc_admin='';
            foreach ($resultpamequ->result() as $item) {
                $equ_nombre=$item->equ_nombre;
                $equ_marca = $item->equ_marca;
                $equ_modelo = $item->equ_modelo;
                $equ_numeco = $item->equ_numeco;
                $equ_plaser=$item->equ_plaser;
                $equ_estatus= $item->equ_estatus;
            }
            $resultsucu=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$suc));
            foreach ($resultsucu->result() as $item) {
                $suc_nombre=$item->suc_nombre;
                $suc_admin=$item->suc_admin;
            }
            $clavedocumento='BMAU_'.$suc_nombre.'_'.date('m-Y');
        //==================================================
        $ex_xls=1;
        if($ex_xls==1){
            header("Pragma: public");
            header("Expires: 0");
            $filename = "$clavedocumento.xls";
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment; filename=$filename");
            header("Pragma: no-cache");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        }
        $fechaactual=date('d/m/Y h:i:s A');

        $html='<table border="0">
                <tr>
                    <td colspan="6" rowspan="3"><img src="'.base_url().'public/img/logo_reportes.jpg" style="width: 56px;"></td>
                    <td colspan="7">'.utf8_decode('DESCRIPCIÓN DE UNIDADES, EQUIPOS E INSTALACIONES').'</td>
                </tr>
                <tr>
                    <td colspan="12" style="color:red;">'.utf8_decode('Bitácora de Mantenimiento Anual por Unidad (BMAU)').'</td>
                </tr>
                <tr>
                    <td colspan="12" style="color:red;"></td>
                </tr>
                </table>';
        $html.='<table border="0">
                    <tr>
                            <td colspan="1"></td>
                            <td colspan="2" ></td>
                            <td colspan="2"></td>
                            <td colspan="1" ></td>
                            <td colspan="1"></td>
                            <td colspan="1" ></td>
                            <td colspan="1"></td>
                            <td colspan="1" style="background-color: #e9ecef;border-color: #E0E0E0;"><b>'.utf8_decode('Clave del documento:').'</b></td>
                            <td colspan="1">'.$clavedocumento.'</td>
                            <td></td>
                    </tr>
                    <tr>
                            <td colspan="1"></td>
                            <td colspan="2" style="background-color: #e9ecef;border-color: #E0E0E0;"><b>'.utf8_decode('Unidad:').'</b></td>
                            <td colspan="2">'.$equ_nombre.'</td>
                            <td colspan="1" style="background-color: #e9ecef;border-color: #E0E0E0;"><b>'.utf8_decode('Marca:').'</b></td>
                            <td colspan="1">'.$equ_marca.'</td>
                            <td colspan="1" style="background-color: #e9ecef;border-color: #E0E0E0;"><b>'.utf8_decode('Modelo:').'</b></td>
                            <td colspan="1">'.$equ_modelo.'</td>
                            <td colspan="1" style="background-color: #e9ecef;border-color: #E0E0E0;"><b>'.utf8_decode('Año bitacora:').'</b></td>
                            <td colspan="1">'.date('Y').'</td>
                            <td></td>
                    </tr>
                    <tr>
                            <td colspan="1"></td>
                            <td colspan="2" style="background-color: #e9ecef;border-color: #E0E0E0;"><b>'.utf8_decode('No.Serie:').'</b></td>
                            <td colspan="2">'.$equ_plaser.'</td>
                            <td colspan="1" style="background-color: #e9ecef;border-color: #E0E0E0;"><b>'.utf8_decode('No.Económico:').'</b></td>
                            <td colspan="1">'.$equ_numeco.'</td>
                            <td colspan="1" style="background-color: #e9ecef;border-color: #E0E0E0;"><b>'.utf8_decode('Estatus:').'</b></td>
                            <td colspan="1">'.$equ_estatus.'</td>
                            <td colspan="1" style="background-color: #e9ecef;border-color: #E0E0E0;"><b>'.utf8_decode('Estación COECSA:').'</b></td>
                            <td colspan="1">'.$suc_nombre.'</td>
                            <td></td>
                    </tr>
                    <tr>
                            <td colspan="1"></td>
                            <td colspan="2" style="background-color: #e9ecef;border-color: #E0E0E0;"><b>'.utf8_decode('Semana:').'</b></td>
                            <td colspan="2">'.date('W').'</td>
                            <td colspan="1" style="background-color: #e9ecef;border-color: #E0E0E0;"><b>'.utf8_decode('Mes:').'</b></td>
                            <td colspan="1">'.date('m').'</td>
                            <td colspan="1" ></td>
                            <td colspan="1"></td>
                            <td colspan="1" style="background-color: #e9ecef;border-color: #E0E0E0;"><b>'.utf8_decode('Encargado de Estación:').'</b></td>
                            <td colspan="1">'.$suc_admin.'</td>
                            <td></td>
                    </tr>
                </table>';
        $html.='<table class="tabla table table-bordered table-hover" id="table_list" border="1">
                    <thead class="thead-light">
                        <tr>
                            <th rowspan="2">No.</th>
                            <th rowspan="2">Fecha</th>
                            <th rowspan="2">'.utf8_decode('Lectura odómetro').'</th>
                            <th rowspan="2">Tipo de Servicio</th>
                            <th rowspan="2">Clase de Servicio</th>
                            <th rowspan="2" style="width:250px;">'.utf8_decode('Descripción Clase de Servicio').'</th>
                            <th>PREVENTIVO</th>
                            <th>CORRECTIVO</th>
                            <th colspan="3">COMPROBANTE SERVICIOS</th>
                            <th>CAMBIO DE PIEZAS O COMPRA DE ADICIONALES</th>
                            <th colspan="2">COMPROBANTE PIEZAS</th>
                            <th rowspan="2" style="width:150px;">Otros Servicios / Observaciones</th>
                            <th rowspan="2" style="width:100px;">Costo Total</th>
                            <th rowspan="2" style="width:150px;">NOMBRE Y FIRMA RESPONSABLE COECSA</th>
                        </tr>
                        <tr>
                            <th style="width:200px;">DETALLAR EXACTAMENTE LOS SERVICIOS QUE SE LE HICIERON</th>
                            <th style="width:200px;">'.utf8_decode('DETALLAR QUÉ SE LE HIZO').'</th>
                            <th style="width:100px;">Facturas No. / Notas No.</th>
                            <th style="width:100px;">'.utf8_decode('¿Anexa Carta Responsiva?').'</th>
                            <th style="width:100px;">'.utf8_decode('¿Anexa INE?').'</th>
                            <th style="width:250px;">'.utf8_decode('DETALLAR CUÁLES PIEZAS SE CAMBIARON O QUÉ SE COMPRÓ').'</th>
                            <th style="width:100px;">Facturas No. / Notas No.</th>
                            <th style="width:100px;">'.utf8_decode('¿Anexa fotos?').'</th>
                        </tr>
                    </thead>
                    <tbody>';
                    $html.=$this->ModeloCoepam->reporte_prog_bmau($cic,$suc,$equ,$pam,0);
            $html.='</tbody></table>';
        echo $html;
    }
    function evis($suc,$equ,$pam,$bit){
        $data['suc']=$suc;
        $data['sucu']=$suc;
        $data['equ']=$equ;
        $data['pam']=$pam;
        $data['bit']=$bit;
        $data['resultpamequ']=$this->ModeloCatalogos->getselectwheren('pam_equ',array('equipo'=>$equ));
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_pam/evis',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('coe_pam/evisjs');
    }
    function evi_bor(){
        $params=$this->input->post();
        $rid = $params['rid'];
        $this->ModeloCatalogos->getdeletewheren('pam_evi',array('rid'=>$rid));
    }
    function evi_agr(){
        $bit = $_POST['bit'];
        $rid = $_POST['rid'];
        $evi_nombre = $_POST['evi_nombre'];
        $sucursal = $_POST['sucursal'];
        $equipo = $_POST['equipo'];


        $DIR_SUC0=FCPATH.'_files/_suc/'.$sucursal;
        $DIR_SUC1=FCPATH.'_files/_suc/'.$sucursal.'/pam';
        $DIR_SUC2=FCPATH.'_files/_suc/'.$sucursal.'/pam/evis';
        $DIR_SUC3=FCPATH.'_files/_suc/'.$sucursal.'/pam/evis/'.$equipo;
        //===================================================
            if (!is_dir($DIR_SUC0) && strlen($DIR_SUC0)>0){
                //log_message('error',$DIR_SUC);
                //mkdir($DIR_SUC, 0755);
                if(!is_dir($DIR_SUC0)){
                    mkdir($DIR_SUC0, 0755);
                }
            }else{
                file_put_contents($DIR_SUC0.'/index.html', '');
            }
        //==================================================
        //===================================================
            if (!is_dir($DIR_SUC1) && strlen($DIR_SUC1)>0){
                //log_message('error',$DIR_SUC);
                //mkdir($DIR_SUC, 0755);
                if(!is_dir($DIR_SUC1)){
                    mkdir($DIR_SUC1, 0755);
                }
            }else{
                file_put_contents($DIR_SUC1.'/index.html', '');
            }
        //==================================================
        //===================================================
            if (!is_dir($DIR_SUC2) && strlen($DIR_SUC2)>0){
                //log_message('error',$DIR_SUC);
                //mkdir($DIR_SUC, 0755);
                if(!is_dir($DIR_SUC2)){
                    mkdir($DIR_SUC2, 0755);
                }
            }else{
                file_put_contents($DIR_SUC2.'/index.html', '');
            }
        //==================================================
        //===================================================
            if (!is_dir($DIR_SUC3) && strlen($DIR_SUC3)>0){
                //log_message('error',$DIR_SUC);
                //mkdir($DIR_SUC, 0755);
                if(!is_dir($DIR_SUC3)){
                    mkdir($DIR_SUC3, 0755);
                }
            }else{
                file_put_contents($DIR_SUC3.'/index.html', '');
            }
        //==================================================
    

        $config['upload_path']          = $DIR_SUC3;
        $config['allowed_types']        = '*';
        $config['max_size']             = 10000;
        $config['file_name']=date('YmdGis').'_'.rand(0, 99);       
        $output = [];
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('evi_archivo')){
            $data = array('error' => $this->upload->display_errors());
            log_message('error', 'Error en la carga '.json_encode($data));
            if($rid>0){
                $this->ModeloCatalogos->updateCatalogo('pam_evi',array('evi_nombre'=>$evi_nombre),array('rid'=>$rid));
            }else{
                $this->ModeloCatalogos->Insert('pam_evi',array(
                        'bit'=>$bit,
                        'evi_nombre'=>$evi_nombre,
                        'evi_archivo'=>''
                ));
            }
                        
        }else{
             $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension

            if($rid>0){
                $this->ModeloCatalogos->updateCatalogo('pam_evi',array('evi_nombre'=>$evi_nombre,'evi_archivo'=>$file_name),array('rid'=>$rid));
            }else{
                $this->ModeloCatalogos->Insert('pam_evi',array(
                        'bit'=>$bit,
                        'evi_nombre'=>$evi_nombre,
                        'evi_archivo'=>$file_name
                ));
            }

            $data = array('upload_data' => $this->upload->data());
            //$output = [];
            log_message('error', 'Carga correcta '.json_encode($data));
        }
        echo json_encode($output);
    }




}