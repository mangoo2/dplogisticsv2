<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
//C:\xampp\htdocs\dplogistics\JSIMods\suc_cors\ops
class Corte_caja extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCortecaja');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal = date('dmYHis');
        $this->fechal2 = date('dmYHi');
        $this->fecha = date('Y-m-d H:i:s');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_nom=$this->session->userdata('sess_nom');

            $this->sess_nom =  $this->session->userdata('sess_nom');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('cortecaja/list',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('cortecaja/listjs');
  	}
    public function getlista() {
        $params = $this->input->post();
        $params['sucursal']=$this->sess_suc;
        $getdata = $this->ModeloCortecaja->getlist($params);
        $totaldata= $this->ModeloCortecaja->getlist_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function cor_ver($num){
        $data['sess_suc']=$this->sess_suc;
        $data['resul_cors']=$this->ModeloCatalogos->getselectwheren_o3('suc_cors',array('corte'=>$num));
        $data['resul_cors_guias']=$this->ModeloCatalogos->getselectwheren_o3('guias',array('corte'=>$num));
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('cortecaja/cver',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('cortecaja/cverjs');
    }
    function realizar(){
        $data['sess_suc']=$this->sess_suc;
        $result_doc=$this->ModeloCatalogos->getselectwheren('doc_cotiza',array('sucursal'=>$this->sess_suc,'cot_folio !='=>'','cot_conta'=>'no'));
        $data['result_doc']=$result_doc;
        $data['num_docd']=$result_doc->num_rows();
        $data['list_efe']   =$this->ModeloCortecaja->listipodoccorte($this->sess_suc,'efe',1);
        $data['list_cre']   =$this->ModeloCortecaja->listipodoccorte($this->sess_suc,'cre',1);
        $data['list_tar']   =$this->ModeloCortecaja->listipodoccorte($this->sess_suc,'tar',1);
        $data['list_efetar']=$this->ModeloCortecaja->listipodoccorte($this->sess_suc,'efetar',1);
        $data['list_nor']   =$this->ModeloCortecaja->listipodoccorte($this->sess_suc,'',0);
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('cortecaja/realizar',$data);
        $this->load->view('theme/footer');
        $this->load->view('cortecaja/realizarjs');
    }
    function cerrarcorte(){
        $params = $this->input->post();
        $cor_com=$params['cor_com'];
        $tar_tot_e=0;
        $tar_tot_c=0;
        $tar_tot_t=0;
        $list_efe=$this->ModeloCortecaja->listipodoccorte($this->sess_suc,'efe',1);
        foreach ($list_efe->result() as $iteme) {
            $tar_tot_e=$tar_tot_e+$iteme->tar_tot;
            $this->ModeloCatalogos->getdeletewheren('doc_cotiza',array('rid'=>$iteme->rid));//ver si no eliminarlos
            $this->ModeloCatalogos->updateCatalogo('guias',array('corte'=>$this->fechal),array('folio'=>$iteme->cot_folio,'guia'=>$iteme->cot_guia));
        }
        $list_cre=$this->ModeloCortecaja->listipodoccorte($this->sess_suc,'cre',1);
        foreach ($list_cre->result() as $iteme) {
            $tar_tot_c=$tar_tot_c+$iteme->tar_tot;
            $this->ModeloCatalogos->getdeletewheren('doc_cotiza',array('rid'=>$iteme->rid));//ver si no eliminarlos
            $this->ModeloCatalogos->updateCatalogo('guias',array('corte'=>$this->fechal),array('folio'=>$iteme->cot_folio,'guia'=>$iteme->cot_guia));
        }
        $list_tar=$this->ModeloCortecaja->listipodoccorte($this->sess_suc,'tar',1);
        foreach ($list_tar->result() as $iteme) {
            $tar_tot_t=$tar_tot_t+$iteme->tar_tot;
            $this->ModeloCatalogos->getdeletewheren('doc_cotiza',array('rid'=>$iteme->rid));//ver si no eliminarlos
            $this->ModeloCatalogos->updateCatalogo('guias',array('corte'=>$this->fechal),array('folio'=>$iteme->cot_folio,'guia'=>$iteme->cot_guia));
        }
        $list_efetar=$this->ModeloCortecaja->listipodoccorte($this->sess_suc,'efetar',1);
        foreach ($list_efetar->result() as $iteme) {
            if($iteme->cot_cantidad>0){
                $tar_tot_e=$tar_tot_e+$iteme->cot_cantidad;
            }
            if($iteme->banco_efectivo>0){
                $tar_tot_t=$tar_tot_t+$iteme->banco_efectivo;
            }
            if($iteme->banco_credito>0){
                $tar_tot_t=$tar_tot_t+$iteme->banco_credito;
            }
            
            $this->ModeloCatalogos->getdeletewheren('doc_cotiza',array('rid'=>$iteme->rid));//ver si no eliminarlos
            $this->ModeloCatalogos->updateCatalogo('guias',array('corte'=>$this->fechal),array('folio'=>$iteme->cot_folio,'guia'=>$iteme->cot_guia));
        }
        $list_nor=$this->ModeloCortecaja->listipodoccorte($this->sess_suc,'',0);
        foreach ($list_nor->result() as $iteme) {
            $this->ModeloCatalogos->getdeletewheren('doc_cotiza',array('rid'=>$iteme->rid));
        }
        $this->ModeloCatalogos->Insert('suc_cors',array('sucursal'=>$this->sess_suc,'corte'=>$this->fechal,'cor_efectivo'=>$tar_tot_e,'cor_credito'=>$tar_tot_c,'cor_tarjeta'=>$tar_tot_t,'cor_fecha'=>$this->fecha,'cor_com'=>$cor_com,'cor_adm'=>$this->sess_usr,'cor_admnom'=>$this->sess_nom,'cor_est'=>'pen','fin_coms'=>''));
        $datosresult = array(
                        'corte'=>$this->fechal
                            );
        echo json_encode($datosresult);

    }
    function comprobanteg(){
        $rid = $_POST['id'];
        
        $config['upload_path']          = FCPATH.'comprobante/';
        $config['allowed_types']        = '*';
        $config['max_size']             = 5000;
        $config['file_name']=$rid.'_'.date('YmdGis').'_'.rand(0, 99);       

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('comprobante')){
            $error = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($error));
                        
        }else{
             $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension

            //$this->ModeloCatalogos->updateCatalogo('guias',array('file_comprobante'=>$file_name),array('corte'=>$rid));
            $this->ModeloCatalogos->actualizarcomprobanteg($file_name,$rid);
            $this->adguntarpagosglobal($rid);
            $data = array('upload_data' => $this->upload->data());
            $output = [];
            //log_message('error', json_encode($data));
        }
        echo json_encode($output);
    }
    function comprobarfiles(){
        $corte = $this->input->post('corte');
        $result=$this->ModeloCortecaja->comprobantesg($corte);
        $files='';
        foreach ($result->result() as $item) {
            
            $info = new SplFileInfo($item->file_comprobante);
            $ext=$info->getExtension();
            if($ext=='jpg' || $ext=='jpeg' || $ext=='png' || $ext=='gif' || $ext=='JPG' || $ext=='JPEG' || $ext=='PNG' || $ext=='GIF'){
                $files.='<a href="'.base_url().'comprobante/'.$item->file_comprobante.'" ><img 
                                src="'.base_url().'comprobante/'.$item->file_comprobante.'" 
                                alt="img" class="comprobantesimg" 
                    ></a>';
                
            }elseif($ext=='pdf' || $ext=='PDF'){
                $files.='<a href="'.base_url().'comprobante/'.$item->file_comprobante.'" target="_blank"><img 
                                src="'.base_url().'public/img/PDF_file_icon.svg" 
                                alt="img" class="comprobantespdf"
                    ></a>';
            }


        }
        //$data = array('files' => $result->result());
        echo $files;
    }
    function comprobantei(){
        $rids = $_POST['guias'];
        $DATAgs = json_decode($rids);

        $config['upload_path']          = FCPATH.'comprobante/';
        $config['allowed_types']        = '*';
        $config['max_size']             = 5000;
        $config['file_name']=date('YmdGis').'_'.rand(0, 99);       
        $montototal=0;
        $rid=0;
        $sucursal=$this->sess_suc;
        $guia='';
        $rid_n='';
        $deposito=$this->fechal;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('comprobante')){
            $error = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($error));
                        
        }else{
             $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension
            $rows=0;
            for ($i=0;$i<count($DATAgs);$i++) {
                
                $rid=$DATAgs[$i]->guiasIds;
               
               //$this->ingresardepositoacuenta($DATAgs[$i]->guiasIds,$rows);
               $rid_suc_deps=$this->ModeloCatalogos->getconsultardepositos($rid);
               if($rid_suc_deps->num_rows()>0){

               }else{
                    $rows++;
                    $this->ModeloCatalogos->updateCatalogo('guias',array('file_comprobante'=>$file_name),array('rid'=>$rid));
                    $rid_n.="|$rid|";
                    $montototal=$montototal+$DATAgs[$i]->monto;

                    $result_g=$this->ModeloCatalogos->getselectwheren('guias',array('rid'=>$rid));
                
                    foreach ($result_g->result() as $itemg) {
                        $sucursal=$itemg->sucursal;
                        $guia.=$itemg->guia.' ';
                    }
               }
               

               
            }
            //=======================================
                if($rows>0){
                    $arraydata=array(
                        'sucursal'=>$sucursal,
                        'deposito'=>$deposito,
                        'dep_est'=>'dep',
                        'dep_verifico'=>'',
                        'dep_importe'=>$montototal,
                        'dep_saldo'=>$montototal,
                        'dep_forma'=>'DEPOSITO',
                        'dep_concepto'=>'PAGO GUIA '.$guia,
                        'dep_fecha'=>$this->fecha,
                        'dep_deposito'=>'',
                        'dep_feccap'=>$this->fecha,
                        'dep_tipo'=>'',
                        'dep_obs'=>'',
                        'admin'=>$this->sess_usr,
                        'relacionguias'=>$rid,
                        'relacionguias_n'=>$rid_n,
                        'dep_deposito'=>$this->sess_nom
                    );
                    $this->ModeloCatalogos->Insert('suc_deps',$arraydata);
                }
            //=======================================
            

            $data = array('upload_data' => $this->upload->data());
            $output = [];
            //log_message('error', json_encode($data));
        }
        echo json_encode($output);
    }
    function adguntarpagosglobal($rid_codigo){
        $result=$this->ModeloCatalogos->getselectwheren('guias',array('corte'=>$rid_codigo));
        $rows=0;
        
        $sucursal=$this->sess_suc;
        $guias='';
        $deposito=$this->fechal;
        $importe=0;
        $fecha=$this->fecha;
        $rid_n='';
        foreach ($result->result() as $item) {
            $sucursal= $item->sucursal;
            $rid= $item->rid;
            
            $fecha = $item->fecha;
            //$this->ingresardepositoacuenta2($rid,$rows);
            //===========================================================
                $result=$this->ModeloCatalogos->getselectwheren('guias',array('rid'=>$rid));
                
                
                foreach ($result->result() as $item) {
                    
                    //$result_suc_deps=$this->ModeloCatalogos->getselectwheren('suc_deps',array('relacionguias'=>$rid));
                    $rid_suc_deps=$this->ModeloCatalogos->getconsultardepositos($rid);
                    if($rid_suc_deps->num_rows()>0){
                    }else{
                        $rid_n.="|$rid|";
                        $guias.=$item->guia.' ';
                        $importe=$importe+$item->importe;
                        $rows++; 
                    }
                }
            //===========================================================
        }
        if($rows>0){
            $arraydata=array(
                            'sucursal'=>$sucursal,
                            'deposito'=>$deposito,
                            'dep_est'=>'dep',
                            'dep_verifico'=>'',
                            'dep_importe'=>$importe,
                            'dep_saldo'=>$importe,
                            'dep_forma'=>'DEPOSITO',
                            'dep_concepto'=>'PAGO GUIA '.$guias,
                            'dep_fecha'=>$fecha,
                            'dep_deposito'=>'',
                            'dep_feccap'=>$this->fecha,
                            'dep_tipo'=>'',
                            'dep_obs'=>'',
                            'admin'=>$this->sess_usr,
                            'relacionguias'=>$rid,
                            'relacionguias_n'=>$rid_n,
                            'dep_deposito'=>$this->sess_nom
                        );
                        $this->ModeloCatalogos->Insert('suc_deps',$arraydata);
        }
    }
    function ingresardepositoacuenta($rid,$tipo){
        if($tipo>0){
            $deposito=$this->fechal.$tipo;
        }else{
            $deposito=$this->fechal;
        }
        $result=$this->ModeloCatalogos->getselectwheren('guias',array('rid'=>$rid));
        foreach ($result->result() as $item) {
            $guia=$item->guia;
            $result_suc_deps=$this->ModeloCatalogos->getselectwheren('suc_deps',array('relacionguias'=>$rid));
            $rid_suc_deps=0;
            foreach ($result_suc_deps->result() as $itemsd) {
                 $rid_suc_deps = $itemsd->rid;
            }
            if($rid_suc_deps>0){
                
            }else{
                $arraydata=array(
                    'sucursal'=>$item->sucursal,
                    'deposito'=>$deposito,
                    'dep_est'=>'dep',
                    'dep_verifico'=>'',
                    'dep_importe'=>$item->importe,
                    'dep_saldo'=>$item->importe,
                    'dep_forma'=>'DEPOSITO',
                    'dep_concepto'=>'PAGO GUIA '.$guia,
                    'dep_fecha'=>$item->fecha,
                    'dep_deposito'=>'',
                    'dep_feccap'=>$this->fecha,
                    'dep_tipo'=>'',
                    'dep_obs'=>'',
                    'admin'=>$this->sess_usr,
                    'relacionguias'=>$rid,
                    'dep_deposito'=>$this->sess_nom
                );
                $this->ModeloCatalogos->Insert('suc_deps',$arraydata);
            }
            

        }
    }
    function comprobantegall(){
        $rids = $_POST['cortes'];
        $DATAgs = json_decode($rids);

        $config['upload_path']          = FCPATH.'comprobante/';
        $config['allowed_types']        = '*';
        $config['max_size']             = 5000;
        $config['file_name']=date('YmdGis').'_'.rand(0, 99);       
        $montototal=0;
        $rid=0;
        //====================
            $rows=0;
        
            $sucursal=$this->sess_suc;
            $guias='';
            $deposito=$this->fechal;
            $importe=0;
            $fecha=$this->fecha;
            $rid_n='';
        //====================
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('comprobanteall')){
            $error = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($error));
                        
        }else{
             $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension
            $rows=0;
            for ($i=0;$i<count($DATAgs);$i++) {
                
                $cortesIds=$DATAgs[$i]->cortesIds;
               
               $result_cortes=$this->ModeloCatalogos->getselectwheren('suc_cors',array('rid'=>$cortesIds));
               foreach ($result_cortes->result() as $item_r_c) {
                    $corte_cl=$item_r_c->corte;
                    //====================================
                        $result_guias=$this->ModeloCatalogos->getselectwheren('guias',array('corte'=>$corte_cl));
                        foreach ($result_guias->result() as $item_gs) {
                            $sucursal= $item_gs->sucursal;
                            $rid= $item_gs->rid;
                            $fecha = $item_gs->fecha;
                            //===============================
                                $result=$this->ModeloCatalogos->getselectwheren('guias',array('rid'=>$rid));
                                foreach ($result->result() as $item) {
                    
                                    //$result_suc_deps=$this->ModeloCatalogos->getselectwheren('suc_deps',array('relacionguias'=>$rid));
                                    $rid_suc_deps=$this->ModeloCatalogos->getconsultardepositos($rid);
                                    if($rid_suc_deps->num_rows()>0){
                                    }else{
                                        $this->ModeloCatalogos->updateCatalogo('guias',array('file_comprobante'=>$file_name),array('rid'=>$rid));
                                        $rid_n.="|$rid|";
                                        $guias.=$item->guia.' ';
                                        $importe=$importe+$item->importe;
                                        $rows++; 
                                    }
                                }
                            //===============================
                        }
                    //===================================
               }
               

               
            }
            if($rows>0){
                $arraydata=array(
                                'sucursal'=>$sucursal,
                                'deposito'=>$deposito,
                                'dep_est'=>'dep',
                                'dep_verifico'=>'',
                                'dep_importe'=>$importe,
                                'dep_saldo'=>$importe,
                                'dep_forma'=>'DEPOSITO',
                                'dep_concepto'=>'PAGO GUIA '.$guias,
                                'dep_fecha'=>$fecha,
                                'dep_deposito'=>'',
                                'dep_feccap'=>$this->fecha,
                                'dep_tipo'=>'',
                                'dep_obs'=>'',
                                'admin'=>$this->sess_usr,
                                'relacionguias'=>$rid,
                                'relacionguias_n'=>$rid_n,
                                'dep_deposito'=>$this->sess_nom
                            );
                            $this->ModeloCatalogos->Insert('suc_deps',$arraydata);
            }
            

            $data = array('upload_data' => $this->upload->data());
            $output = [];
            //log_message('error', json_encode($data));
        }
        echo json_encode($output);
    }

  


}