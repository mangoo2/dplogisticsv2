<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_usrs extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloUsuarios');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal = date('dmYHis');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('usuarios/list',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('usuarios/listjs');
  	}
    public function getlista() {
        $params = $this->input->post();
        $params['sucursal']=$this->sess_suc;
        $getdata = $this->ModeloUsuarios->getlist($params);
        $totaldata= $this->ModeloUsuarios->getlist_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function verificarimg(){
        $usuario = $this->input->post('usuario');
        $html='';
        $nombre_fichero = '_files/usrs/'.$usuario.'.jpg';

        if (file_exists($nombre_fichero)) {
            $html="<img src='".base_url()."_files/usrs/".$usuario.".jpg' class='imgusuario'>";
        } else {
            $html="<img src='".base_url()."_files/usrs/149071.png' class='imgusuario'>";
        }
        $resultrow=$this->ModeloCatalogos->getselectwheren('jsi_sis_admins',array('usuario'=>$usuario));
        foreach ($resultrow->result() as $item) {
            if($item->img!=''){
               $html="<img src='".base_url()."_files/usrs/".$item->img."' class='imgusuario'>"; 
            }
            
        }
        echo $html;
    }
    function add($usuario=''){
        $data['sucursalrow'] = $this->ModeloCatalogos->genSelect('coe_sucs');
        $data['usuario']='';
        $data['clave']='';
        $data['nombre']='';
        $data['correo']='';
        $data['sexo']='';
        $data['nda']='';
        $data['sucursal']='';
        $data['mods']='';
        $data['fac']=0;

        if($usuario!=''){
            $data['idform']=1;
            $data['title']='EDITAR';
            $data['formbtn']='Editar';
            $data['usuario']=$usuario;
            $data['readonly']='readonly';
            $resultrow=$this->ModeloCatalogos->getselectwheren('jsi_sis_admins',array('usuario'=>$usuario));
            foreach ($resultrow->result() as $item) {
                $data['usuario']=$item->usuario;
                //$data['clave']=$item->clave;
                $data['nombre']=$item->nombre;
                $data['correo']=$item->correo;
                $data['sexo']=$item->sexo;
                $data['nda']=$item->nda;
                $data['sucursal']=$item->sucursal;
                $data['mods']=$item->mods;
                $data['fac']=$item->fac;
            }
        }else{
            $data['idform']=0;
            $data['title']='AGREGAR';
            $data['formbtn']='Guardar';
            $data['usuario']='';
            $data['readonly']='';
        }
        
        
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('usuarios/add',$data);
        $this->load->view('theme/footer');
        $this->load->view('usuarios/addjs');
    
    }
    function inserupdate(){
        $params = $this->input->post();
        //log_message('error', json_encode($params));
        //log_message('error', json_encode($params['mods']));
        //log_message('error', implode(',',$params['mods']));
        if(isset($params['fac'])){
            $fac=$params['fac'];
        }else{
            $fac=0;
        }
        if($params['idform']>0){
            $array = array(
                            'nombre'=>$params['nombre'],
                            'correo'=>$params['correo'],
                            'sexo'=>$params['sexo'],
                            'nda'=>$params['nda'],
                            'sucursal'=>$params['sucursal'],
                            'fac'=>$fac
                    );
            
            if(isset($params['mods'])){
                $array['mods']=implode(',',$params['mods']);
            }else{
                $array['mods']='';
            }

            if($params['clave']!=''){
                $array['clave']=md5($params['clave']);
            }
            $this->ModeloCatalogos->updateCatalogo('jsi_sis_admins',$array,array('usuario'=>$params['usuario']));
        }else{
            $array = array(
                            'usuario'=>$params['usuario'],
                            'nombre'=>$params['nombre'],
                            'correo'=>$params['correo'],
                            'sexo'=>$params['sexo'],
                            'nda'=>$params['nda'],
                            'sucursal'=>$params['sucursal'],
                            'fac'=>$fac
                    );
            if(isset($params['mods'])){
                $array['mods']=implode(',',$params['mods']);
            }else{
                $array['mods']='';
            }

            if($params['clave']!=''){
                $array['clave']=md5($params['clave']);
            }
            $this->ModeloCatalogos->Insert('jsi_sis_admins',$array);
        }
        echo 1;
    }
    function imagenusu(){
        $rid = $_POST['id'];
        
        log_message('error', $rid);

        $config['upload_path']          = FCPATH.'_files/usrs/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 5000;
        $config['file_name']=date('YmdGis').'_'.rand(0, 99);       

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('fotografia')){
            $error = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($error));
            $output = [];            
        }else{
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.

            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension

            $this->ModeloCatalogos->updateCatalogo('jsi_sis_admins',array('img'=>$file_name),array('usuario'=>$rid));

            $data = array('upload_data' => $this->upload->data());
            $output = [];
            log_message('error', json_encode($data));
        }
        echo json_encode($output);
    }
    function delete(){
        $usuario = $this->input->post('usuario');
        $this->ModeloCatalogos->getdeletewheren('jsi_sis_admins',array('usuario'=>$usuario));
    }
    

}