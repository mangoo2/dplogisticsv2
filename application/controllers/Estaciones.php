<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Estaciones extends CI_Controller {

    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloEstaciones');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal = date('dmYHis');
        $this->fechahoy = date('Y-m-d G:i:s');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
        }else{
            redirect('Sistema'); 
        }
    }

	function index(){
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('estaciones/index');
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('estaciones/indexjs');
  	}

    public function get_listado() {
        $params = $this->input->post();
        $getdata = $this->ModeloEstaciones->get_listado($params);
        $totaldata= $this->ModeloEstaciones->get_listado_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function estatus_reg()
    {   
        $id=$this->input->post('id');
        $estatus=$this->input->post('estatus');
        $arrayinfo = array('activo'=>$estatus);
        $this->ModeloCatalogos->updateCatalogo('f_c_estaciones',$arrayinfo,array('id'=>$id));
    }

}
