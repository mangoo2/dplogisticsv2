<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Origen extends CI_Controller {

    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloOrigen');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal = date('dmYHis');
        $this->fechahoy = date('Y-m-d G:i:s');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
        }else{
            redirect('Sistema'); 
        }
    }

	function index(){
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('origen/index');
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('origen/indexjs');
  	}

    function registro($id=0)
    {
        if($id==0){
            $data['title']='AGREGAR';
            $data['id']=0;

            $data['dpl']='';
            $data['dpltxt']='';

            $data['idorigen']='';
            $data['num_estacion']='';
            $data['num_estaciontxt']='';
        }else{
            $data['title']='EDITAR ';
            $result=$this->ModeloCatalogos->getselectwheren('origen',array('id'=>$id));
            foreach ($result->result() as $item) {
                $data['id']=$item->id;
                $data['dpl']=$item->dpl;
                $data['idorigen']=$item->idorigen;
                $data['num_estacion']=$item->num_estacion;

                $data['dpltxt']=''; 
                $resultdp=$this->ModeloCatalogos->getselectwheren('coe_ruts',array('ruta'=>$item->dpl));
                foreach ($resultdp->result() as $itemp){
                    $data['dpltxt']=$itemp->rut_cla;
                }
                
                $data['num_estaciontxt']=''; 
                $resultdp=$this->ModeloCatalogos->getselectwheren('f_c_estaciones',array('clave_identificacion'=>$item->num_estacion));
                foreach ($resultdp->result() as $itemp){
                    $data['num_estaciontxt']=$itemp->descripcion;
                }

            }
        }
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('origen/form',$data);
        $this->load->view('theme/footer');
        $this->load->view('origen/formjs');
    }

    public function get_listado() {
        $params = $this->input->post();
        $getdata = $this->ModeloOrigen->get_listado($params);
        $totaldata= $this->ModeloOrigen->get_listado_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function insert_registro(){
        $params = $this->input->post();
        $rid=$params['rid'];
        unset($params['rid']);
        if($rid>0){
            $this->ModeloCatalogos->updateCatalogo('origen',$params,array('id'=>$rid));
        }else{
            $params['reg']=$this->fechahoy;
            $rid=$this->ModeloCatalogos->Insert('origen',$params);
        }
        echo $rid;
    } 

    public function delete()
    {   
        $id=$this->input->post('id');
        $arrayinfo = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('origen',$arrayinfo,array('id'=>$id));
    }

    function search_dpl(){
        $search = $this->input->get('search');
        $results = $this->ModeloOrigen->get_origen_like($search);
        echo json_encode($results);    
    }

    function search_estaciones(){
        $search = $this->input->get('search');
        $results = $this->ModeloOrigen->get_estaciones_like($search);
        echo json_encode($results);    
    }

}