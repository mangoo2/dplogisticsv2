<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicios_Rye extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloRye');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('rye/lista',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('rye/listajs');
  	}
    public function getlista() {
        $params = $this->input->post();
        $params['sucursal']=$this->sess_suc;
        $getdata = $this->ModeloRye->getlist($params);
        $totaldata= $this->ModeloRye->getlist_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function cad_agr(){
        $arraydata = array(
            'sucursal'=>$this->sess_suc,
            'usuario'=>$this->sess_usr,
            'ser'=>$this->fechal,
            'ser_fol'=>0,
            'ser_fec'=>$this->fechalarga,
            'ser_est'=>'ini',
            'ser_fecini'=>$this->fechalarga,
            'ser_fecfin'=>'0000-00-00 00:00:00',
            'ser_guia'=>'',
            'ser_origen'=>'',
            'ser_destino'=>'',
            'ser_piezas'=>0,
            'ser_kilos'=>0.0,
            'ser_fac'=>'',
            'ser_fri'=>'',
            'ser_cajas'=>0,
            'ser_clis'=>'',
            'ser_sers'=>'',
            'ser_pers'=>'',
            'ser_unis'=>'',
            'ser_fdp'=>'',
            'ser_fdpdes'=>0,
            'ser_obs'=>'',
            'ser_enc'=>'',
            'ser_firs'=>'',
            'ser_finusr'=>'',
            'ser_finobs'=>''
        );
        $this->ModeloCatalogos->Insert('ser_cad',$arraydata);
        redirect('Servicios_Rye/cad_ini/'.$this->fechal); 
    }
    function cad_ini($codigo){
        $data['codigo']=$codigo;
        $data['sess_suc'] =$this->sess_suc;
        $data['ser_clis_result'] = $this->ModeloCatalogos->getselectwheren('ser_clis',array('emp'=>'rye'));

        $data['ser_servicios'] = $this->ModeloCatalogos->getselectwheren('ser_sers',array('emp'=>'rye'));

        $data['ser_per_pres'] = $this->ModeloCatalogos->getselectwheren('jsi_sis_admins',array('sucursal'=>$this->sess_suc));
        $data['ser_unis'] = $this->ModeloCatalogos->getselectwheren('pam_equ',array('sucursal'=>$this->sess_suc));
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('rye/add',$data);
        $this->load->view('theme/footer');
        $this->load->view('rye/addjs');
    }
    function updateinser_ser_coe(){
        $params = $this->input->post();
        $ser=$params['ser'];
        $ser_sers='';
        $ser_pers='';
        $ser_unis='';
        $ser_sers_a=$params['ser_sers_a'];
        $ser_pers_a=$params['ser_pers_a'];
        $ser_unis_a=$params['ser_unis_a'];

        unset($params['ser_sers_a']);
        unset($params['ser_pers_a']);
        unset($params['ser_unis_a']);

        $result= $this->ModeloCatalogos->getselectwheren('ser_cad',array('ser'=>$ser));
        $datos=$result->row();
        if ($datos->ser_fol<=0) {
            $result_sf= $this->ModeloCatalogos->getselectwheren('ser_fols',array('sucursal'=>$this->sess_suc,'emp'=>'rye'));
            $d_r_sf=$result_sf->row();
            $num=$d_r_sf->num+1;
            $this->ModeloCatalogos->updateCatalogo('ser_fols',array('num'=>$num),array('sucursal'=>$this->sess_suc,'rid'=>$d_r_sf->rid));
            //================================
                
                $DATAsr = json_decode($ser_sers_a);
                for ($i=0;$i<count($DATAsr);$i++) {
                    $datasr[]=$DATAsr[$i]->ser_sers1.' | '.$DATAsr[$i]->ser_sers2;
                }
                if(count($DATAsr)>0){
                    $ser_sers=implode('~', $datasr);
                }
                $DATAunis = json_decode($ser_unis_a);
                if(count($DATAunis)>0){
                    $ser_unis=implode(',', $DATAunis);
                }
                $DATAper = json_decode($ser_pers_a);
                if(count($DATAper)>0){
                    $ser_pers=implode(',', $DATAper);
                }
                $params['ser_sers']=$ser_sers;
                $params['ser_unis']=$ser_unis;
                $params['ser_pers']=$ser_pers;
                $params['ser_est']='ffi';
                $params['ser_fol']=$num;

            //================================
            $this->ModeloCatalogos->updateCatalogo('ser_cad',$params,array('sucursal'=>$this->sess_suc,'ser'=>$ser));


        }else{
            $this->ModeloCatalogos->updateCatalogo('ser_cad',array('ser_est'=>$this->fechalarga),array('sucursal'=>$this->sess_suc,'ser'=>$ser));
            //redirect('Servicios_RAMPA/coe_fin/'.$ser); 
        }
    }
    function get_cad_ini(){
        $codigo=$this->input->post('codigo');
        $result= $this->ModeloCatalogos->getselectwheren('ser_cad',array('ser'=>$codigo));
        $datos=$result->row();
        $ser_sers_a=explode('~',$datos->ser_sers);
        $ser_pers_a=explode(',',$datos->ser_pers);
        $ser_unis_a=explode(',',$datos->ser_unis);
        $array= array(
                        'datos'=>$datos,
                        'ser_sers_a'=>$ser_sers_a,
                        'ser_pers_a'=>$ser_pers_a,
                        'ser_unis_a'=>$ser_unis_a
                    );
        echo json_encode($array);
    }
    function cad_fin($codigo){
        $data['codigo']=$codigo;
        $result= $this->ModeloCatalogos->getselectwheren('ser_cad',array('ser'=>$codigo));
        $data['datos']=$result->row();

        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('rye/cadfin',$data);
        $this->load->view('theme/footer');
        $this->load->view('rye/cadfinjs');
    }
    function cad_ver($codigo){
        $data['codigo']=$codigo;
        $result= $this->ModeloCatalogos->getselectwheren('ser_cad',array('ser'=>$codigo));
        $data['datos']=$result->row();
        $this->load->view('reportes/cad_ver',$data);
    }
    function updateinser_ser_cadfin(){
        $params = $this->input->post();
        $ser=$params['ser'];
        unset($params['ser']);
        $params['ser_est']='fin';
        $params['ser_finusr']=$this->sess_usr;

        $this->ModeloCatalogos->updateCatalogo('ser_cad',$params,array('sucursal'=>$this->sess_suc,'ser'=>$ser));
    }
    function delete(){
        $params = $this->input->post();
        $rid=$params['codigo'];
        $this->ModeloCatalogos->getdeletewheren('ser_cad',array('rid'=>$rid,'sucursal'=>$this->sess_suc));
    }



}