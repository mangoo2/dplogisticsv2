<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Timbrar extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloClientes');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modelofacturas');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal = date('dmYHis');
        $this->trunquearredondear=0;//0 redondeo 1 trunquear (dos dijitos)
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');

            $this->fechahoy = date('Y-m-d G:i:s');
            $this->fecha_reciente = date('Y-m-d');
            $this->fechahoy = date('Y-m-d');
            $this->fechahoyc = date('Y-m-d H:i:s');
            $this->fechahoyL = date('Y-m-d_H_i_s');
        }else{
            redirect('Sistema'); 
        }
    }
  function index(){
        
    }
     //=============================================================
    //===================tratar de no mover apartir de aqui =======
    //=============================================================
    public function prefactura(){
        /*$datosconfiguracion=$this->ModeloGeneral->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];*/
        $logotipo = base_url().'public/img/logo.png';
        $data["logotipo"]=$logotipo;
        /*$data["Nombrerasonsocial"]=$datosconfiguracion->Nombre;
        $data['rrfc']=$datosconfiguracion->Rfc;
        $data['rdireccion']=$datosconfiguracion->Calle.' '.$datosconfiguracion->Municipio.' '.$datosconfiguracion->Estado.' '.$datosconfiguracion->PaisExpedicion.' C.P. '.$datosconfiguracion->CodigoPostal;
        $data['regimenf']=$this->gt_regimenfiscal($datosconfiguracion->Regimen);*/
        $data["Nombrerasonsocial"]="MADERERIA ETLA";
        $data['rrfc']="ZAAU101010XXX";
        $data['rdireccion']="CALLE VENUSTIANO CARRANZA No. 15, SAN BARTOLOMÉ, C.P. 90970 SAN PABLO DEL MONTE, TLAXCALA";
        $data['regimenf']="601 General de Ley Personas Morales";
        $data['Folio']=0001;
        $data['folio_fiscal']='XX00X000-000X-0X00-0X0X-X0X00XXXX000';
        $data['nocertificadosat']='00001000000000000000';
        $data['certificado']='00001000000000000000';
        $data['cfdi']=$this->gt_uso_cfdi($_GET['uso_cfdi']);
        $data['fechatimbre']='0000-00-00 00:00:00';

        $dcliente=$this->ModeloGeneral->getselectwheren('clientes',array('ClientesId'=>$_GET['idcliente']));
        foreach ($dcliente as $item) {
            $cliente=$item->razon_social;
            $direccion=$item->direccion_fiscal;
            $cp = $item->cp_fiscal;    
        }

        /*
        $destado=$this->ModeloCatalogos->getselectwheren('estado',array('EstadoId'=>$idestado));
        foreach ($destado->result() as $item) {
            $estado=$item->Nombre;
        }
        */

        //$resultcli=$this->ModeloCatalogos->getselectwheren('cliente_has_datos_fiscales',array('id'=>$_GET['rfc']));
        //$cliente='';
        $clirfc=$_GET['rfc'];
        //$clidireccion='';
        /*
        foreach ($resultcli->result() as $item) {
            $cliente=$item->razon_social;
            $clirfc=$item->rfc;
            $cp = $item->cp;
            $num_ext = $item->num_ext;
            $num_int = $item->num_int;
            $colonia = $item->colonia;
            $calle = $item->calle;
            $cp = $item->cp;

            if($item->estado!=null){
                $estado=$item->estado;
            }
            if($item->municipio!=null){
              $municipio=$item->municipio;   
            }
        }
        */
        
        $clidireccion     =       $direccion ;
        $data['cp']             =   $cp;
        $data['cliente']        =   $cliente;
        $data['clirfc']         =   $clirfc;
        $data['clidireccion']   =   $clidireccion;
        //$data['isr']            =   $_GET['visr'];
        $data['numordencompra'] =   $_GET['numordencompra'];
        //$data['ivaretenido']    =   $_GET['vriva'];
        $data['cedular']        =   0;
       //$data['cincoalmillarval']        =   $_GET['v5millar'];
        //$data['outsourcing']        =   $_GET['outsourcing'];
        $data['numproveedor']   =   $_GET['numproveedor'];
        //$data['observaciones']  =   $_GET['observaciones'];
        $data['observaciones']  =   '';     
        $data['iva']            =   $_GET['iva'];
        $data['subtotal']       =   $_GET['subtotal'];
        $data['total']          =   $_GET['total'];
        $data['moneda']         =   $_GET['moneda'];
        //$data['tarjeta']        =   $_GET['tarjeta'];
        $data['tarjeta']        =   '';
        $data['FormaPago']      =   $_GET['MetodoPago'];
        $data['FormaPagol']     =   $this->gf_FormaPago($_GET['MetodoPago']);
        $data['MetodoPago']     =   $_GET['FormaPago'];
        $data['MetodoPagol']    =   $this->gt_MetodoPago($_GET['FormaPago']);
        $data['tipoComprobante']='I-Ingreso';

        //$data['Caducidad']      =   $_GET['Caducidad'];
        $data['Lote']           =   $_GET['lote'];

        $conseptos=json_decode($_GET['conceptos']);
        $arrayconceptos=array();
        foreach ($conseptos as $item) {
            $arrayconceptos[]=array(
                                    'Cantidad'=>$item->Cantidad,
                                    'Unidad'=>$item->Unidad,
                                    'nombre'=>$this->get_f_undades($item->Unidad),
                                    'servicioId'=>$item->servicioId,
                                    'Descripcion'=>$item->Descripcion,
                                    'Descripcion2'=>$item->Descripcion2,
                                    'Cu'=>$item->Cu,
                                    'descuento'=>$item->descuento
                                );
        }
        $arrayconceptos=json_encode($arrayconceptos);
        $arrayconceptos=json_decode($arrayconceptos);

        $data['facturadetalles']=$arrayconceptos;
        $data['selloemisor']    =   'MDExMTAxMDAgMDExMDAxMDEgMDExMTEwMDAgMDExMTAxMDAgMDExMDExMTEgMDAxMDAwMDAgMDExMDAxMDAgMDExMDAxMDEgMDAxMDAwMDAgMDExMTAwMDAgMDExMTAwMTAgMDExMTAxMDEgMDExMDAxMDEgMDExMDAwMTAgMDExMDAwMDEgMDAxMDAwMDAgMDEwMTEwMTEgMDExMTAwMTEgMDExMDAxMDAgMDExMTAwMTAgMDExMDAxMDEgMDExMTAxMTAgMDExMDAwMDEgMDExMDAxMDEgMDExMDAwMTAgMDEwMTExMDEgMDAxMDAwMDAgMDEwMDAwMDEgMDEwMDAxMTEgMDEwMDAwMTA=';
        $data['sellosat']    =   'MDExMTAxMDAgMDExMDAxMDEgMDExMTEwMDAgMDExMTAxMDAgMDExMDExMTEgMDAxMDAwMDAgMDExMDAxMDAgMDExMDAxMDEgMDAxMDAwMDAgMDExMTAwMDAgMDExMTAwMTAgMDExMTAxMDEgMDExMDAxMDEgMDExMDAwMTAgMDExMDAwMDEgMDAxMDAwMDAgMDEwMTEwMTEgMDExMTAwMTEgMDExMDAxMDAgMDExMTAwMTAgMDExMDAxMDEgMDExMTAxMTAgMDExMDAwMDEgMDExMDAxMDEgMDExMDAwMTAgMDEwMTExMDEgMDAxMDAwMDAgMDEwMDAwMDEgMDEwMDAxMTEgMDEwMDAwMTA=';
        $data['cadenaoriginal'] = '||3.3|U|819|0000-00-00T00:00:00|99|00001000000000000000|CONTADO|'.$_GET['subtotal'].'|MXN|'.$_GET['total'].'|I|'.$_GET['FormaPago'].'|72090|APR980122KZ6|MADERERIA ETLA|'.$_GET['uso_cfdi'].'|'.$clirfc.'|'.$cliente.'|G03|44103103|1.00|H87|Pieza|EJEMPLO|1562.88|1562.88|1562.88|002|Tasa|0.160000|250.06|002|Tasa|0.160000|250.06|250.06||';



        $this->load->view('reportes/factura',$data);
    }   

    function gt_uso_cfdi($text){
          if ($text=='G01') {
            $textl=' G01 Adquisición de mercancias';
          }elseif ($text=='G02') {
            $textl=' G02 Devoluciones, descuentos o bonificaciones';
          }elseif ($text=='G03') {
            $textl=' G03 Gastos en general';
          }elseif ($text=='I01') {
            $textl=' I01 Construcciones';
          }elseif ($text=='I02') {
            $textl=' I02 Mobilario y equipo de oficina por inversiones';
          }elseif ($text=='I03') {
            $textl=' I03 Equipo de transporte';
          }elseif ($text=='I04') {
            $textl=' I04 Equipo de computo y accesorios';
          }elseif ($text=='I05') {
            $textl=' I05 Dados, troqueles, moldes, matrices y herramental';
          }elseif ($text=='I06') {
            $textl=' I06 Comunicaciones telefónicas';
          }elseif ($text=='I07') {
            $textl=' I07 Comunicaciones satelitales';
          }elseif ($text=='I08') {
            $textl=' I08 Otra maquinaria y equipo';
          }elseif ($text=='D01') {
            $textl=' D01 Honorarios médicos, dentales y gastos hospitalarios.';
          }elseif ($text=='D02') {
            $textl=' D02 Gastos médicos por incapacidad o discapacidad';
          }elseif ($text=='D03') {
            $textl=' D03 Gastos funerales.';
          }elseif ($text=='D04') {
            $textl=' D04 Donativos.';
          }elseif ($text=='D05') {
            $textl=' D05 Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).';
          }elseif ($text=='D06') {
            $textl=' >D06 Aportaciones voluntarias al SAR.';
          }elseif ($text=='D07') {
            $textl=' D07 Primas por seguros de gastos médicos.';
          }elseif ($text=='D08') {
            $textl=' D08 Gastos de transportación escolar obligatoria.';
          }elseif ($text=='D09') {
            $textl=' D09 Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.';
          }elseif ($text=='D10') {
            $textl=' D10 Pagos por servicios educativos (colegiaturas)';
          }elseif ($text=='P01') {
            $textl=' P01 Por definir';
          }else{
            $textl='';
          }
          return $textl;
    }
    function gf_FormaPago($text){
      //log_message('error', 'gf_FormaPago:'.$text);
        if ($text=='Efectivo') {
          $textl='01 Efectivo';
        }elseif ($text=='ChequeNominativo') {
          $textl='02 Cheque Nominativo';
        }elseif ($text=='TransferenciaElectronicaFondos') {
          $textl='03 Transferencia ElectronicaFondos';
        }elseif ($text=='TarjetasDeCredito') {
          $textl='04 Tarjetas De Credito';
        }elseif ($text=='MonederoElectronico') {
          $textl='05 Monedero Electronico';
        }elseif ($text=='DineroElectronico') {
          $textl='06 Dinero Electronico';
        }elseif ($text=='Tarjetas digitales') {
          $textl='07 Tarjetas digitales';
        }elseif ($text=='ValesDeDespensa') {
          $textl='08 Vales De Despensa';
        }elseif ($text=='TarjetaDebito') {
          $textl='28 Tarjeta Debito';
        }elseif ($text=='TarjetaServicio') {
          $textl='29 Tarjeta Servicio';
        }elseif ($text=='Otros') {
          $textl='99 Otros';
        }elseif ($text=='DacionPago') {
          $textl='12 Dacion Pago';
        }elseif ($text=='PagoSubrogacion') {
          $textl='13 Pago Subrogacion';
        }elseif ($text=='PagoConsignacion') {
          $textl='14 Pago Consignacion';
        }elseif ($text=='Condonacion') {
          $textl='15 Condonacion';
        }elseif ($text=='Compensacion') {
          $textl='17 Compensacion';
        }elseif ($text=='Novacion') {
          $textl='23 Novacion';
        }elseif ($text=='Confusion') {
          $textl='24 Confusion';
        }elseif ($text=='RemisionDeuda') {
          $textl='25 RemisionDeuda';
        }elseif ($text=='PrescripcionoCaducidad') {
          $textl='26 Prescripciono Caducidad';
        }elseif ($text=='SatisfaccionAcreedor') {
          $textl='27 Satisfaccion Acreedor';
        }elseif ($text=='AplicacionAnticipos') {
          $textl='30 Aplicacion Anticipos';
        }elseif ($text=='PorDefinir') {
          $textl='99 Por definir';
        }
        if ($text=='01') {
          $textl='01 Efectivo';
        }elseif ($text=='02') {
          $textl='02 Cheque Nominativo';
        }elseif ($text=='03') {
          $textl='03 Transferencia ElectronicaFondos';
        }elseif ($text=='04') {
          $textl='04 Tarjetas De Credito';
        }elseif ($text=='05') {
          $textl='05 Monedero Electronico';
        }elseif ($text=='06') {
          $textl='06 Dinero Electronico';
        }elseif ($text=='07') {
          $textl='07 Tarjetas digitales';
        }elseif ($text=='08') {
          $textl='08 Vales De Despensa';
        }elseif ($text=='28') {
          $textl='28 Tarjeta Debito';
        }elseif ($text=='29') {
          $textl='29 Tarjeta Servicio';
        }elseif ($text=='99') {
          $textl='99 Otros';
        }elseif ($text=='12') {
          $textl='12 Dacion Pago';
        }elseif ($text=='13') {
          $textl='13 Pago Subrogacion';
        }elseif ($text=='14') {
          $textl='14 Pago Consignacion';
        }elseif ($text=='15') {
          $textl='15 Condonacion';
        }elseif ($text=='17') {
          $textl='17 Compensacion';
        }elseif ($text=='23') {
          $textl='23 Novacion';
        }elseif ($text=='24') {
          $textl='24 Confusion';
        }elseif ($text=='25') {
          $textl='25 RemisionDeuda';
        }elseif ($text=='26') {
          $textl='26 Prescripciono Caducidad';
        }elseif ($text=='27') {
          $textl='27 Satisfaccion Acreedor';
        }elseif ($text=='30') {
          $textl='30 Aplicacion Anticipos';
        }elseif ($text=='99') {
          $textl='99 Por definir';
        }
        //log_message('error', 'gf_FormaPago:'.$textl);
        return $textl; 
    }
    function gt_regimenfiscal($text){
          if($text="601"){ 
            $textl='601 General de Ley Personas Morales';

          }elseif($text="603"){ 
            $textl='603 Personas Morales con Fines no Lucrativos';

          }elseif($text="605"){ 
            $textl='605 Sueldos y Salarios e Ingresos Asimilados a Salarios';

          }elseif($text="606"){ 
            $textl='606 Arrendamiento';

          }elseif($text="607"){ 
            $textl='607 Régimen de Enajenación o Adquisición de Bienes';

          }elseif($text="608"){ 
            $textl='608 Demás ingresos';

          }elseif($text="609"){ 
            $textl='609 Consolidación';

          }elseif($text="610"){ 
            $textl='610 Residentes en el Extranjero sin Establecimiento Permanente en México';

          }elseif($text="611"){ 
            $textl='611 Ingresos por Dividendos (socios y accionistas)';

          }elseif($text="612"){ 
            $textl='612 Personas Físicas con Actividades Empresariales y Profesionales';

          }elseif($text="614"){ 
            $textl='614 Ingresos por intereses';

          }elseif($text="615"){ 
            $textl='615 Régimen de los ingresos por obtención de premios';

          }elseif($text="616"){ 
            $textl='616 Sin obligaciones fiscales';

          }elseif($text="620"){ 
            $textl='620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos';

          }elseif($text="621"){ 
            $textl='selected="">621 Incorporación Fiscal';

          }elseif($text="622"){ 
            $textl='622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras';

          }elseif($text="623"){ 
            $textl='623 Opcional para Grupos de Sociedades';

          }elseif($text="624"){ 
            $textl='624 Coordinados';

          }elseif($text="628"){ 
            $textl='628 Hidrocarburos';

          }elseif($text="629"){ 
            $textl='629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales';

          }elseif($text="630"){ 
            $textl='630 Enajenación de acciones en bolsa de valores';
          }else{
            $textl='';
          }
          return $textl;
    }
    function gt_MetodoPago($text){
      //log_message('error', 'gt_MetodoPago:'.$text);
      if ($text=='PUE') {
          $textl='PUE Pago en una sola exhibicion';
      }else{
          $textl='PPD Pago en parcialidades o diferido';
      }
      //log_message('error', 'gt_MetodoPago:'.$textl);
      return $textl;
    }

    function get_f_undades($unidad){
        $result=$this->ModeloGeneral->getselectwheren('f_unidades',array('Clave'=>$unidad));
        $nombre='';
        foreach ($result as $item) {
            $nombre=$item->nombre;
        }
        return $nombre;
    }

    function generafacturarabierta(){
        $datas = $this->input->post();
        $ubicaciones=$datas['ubicaciones'];
        $mercancias=$datas['mercancias'];
        $transportes=$datas['transportes'];
        $aereo=$datas['aereo'];
        $operadores=$datas['operadores'];
        $propietario=$datas['propietario'];
        $arrendatario=$datas['arrendatario'];
        $TranspInternac = $datas['TranspInternac'];
        $TotalDistRec = $datas['TotalDistRec'];
        $EntradaSalidaMerc = $datas['EntradaSalidaMerc'];
        $ViaEntradaSalida = $datas['ViaEntradaSalida'];

        $PesoBrutoTotal = $datas['PesoBrutoTotal'];
        $UnidadPeso = $datas['UnidadPeso'];
        $PesoNetoTotal = $datas['PesoNetoTotal'];
        $NumTotalMercancias = $datas['NumTotalMercancias'];
        $CargoPorTasacion = $datas['CargoPorTasacion'];
        $tipofac = $datas['tipofac'];
        $rid_relacion_cot_folio=$datas['rid_relacion_cot_folio'];
        if(isset($datas['rid_relacion'])){
          $rid_relacion = $datas['rid_relacion'];
        }else{
          $rid_relacion=0;
        }
        

        if(isset($datas['conceptos'])){
            $conceptos = $datas['conceptos'];
            unset($datas['conceptos']);
        }

        $idcliente=$datas['idcliente'];
        $rfc=$datas['rfc'];
        //log_message('error', 'idcliente:'.$idcliente);
        $dcliente=$this->ModeloCatalogos->getselectwheren('cli_facts',array('rid'=>$idcliente));
        foreach ($dcliente->result() as $item) {
            $razon_social=$item->fac_nrs;
            $direccion=$item->fac_dir;
            $cp=$item->fac_cod;
            $rfc=$item->fac_rfc;
            $tel1=$item->fac_te1;
            $email=$item->fac_email;
        }
      
        $pais = 'MEXICO';
        $TipoComprobante=$datas['TipoDeComprobante'];
        if($datas['tipoempresa']>0){
          $tipoempresa=$datas['tipoempresa'];  
        }else{
          $tipoempresa=1;//1 COECSA 2 CADIPA
        }
        if($tipoempresa==1){
          $serie='CO';
          if($datas['sucursal']>0){
            $infosuc=$this->ModeloCatalogos->getselectwheren('sucursal',array('id'=>$datas['sucursal'],'conserie'=>1,'activo'=>1));
            foreach ($infosuc->result() as $item) {
              $serie=$item->serie;
            }
          }
        }else{
          $serie='CA';
        }
        if($rfc=='XAXX010101000'){
            //$cp=$codigoPostalEmisor;
            //$RegimenFiscalReceptor=616;
            //$uso_cfdinew='S01';
          $datas['uso_cfdi']='S01';
        }
        $ultimoFolioval=$this->ModeloCatalogos->ultimoFolio($serie) + 1;
        
        $data = array(
                
                "nombre"        => $razon_social,
                "direccion"     => $direccion, 
                "cp"            => $cp,
                "rfc"           => $rfc,
                "folio"         => $ultimoFolioval,
                "PaisReceptor"  =>$pais,
                "Clientes_ClientesId"       => $idcliente,
                "serie" =>$serie,
                //"status"        => 1,
                "TipoComprobante"=>$TipoComprobante,
                "usuario_id"       => $this->sess_usr,
                "rutaXml"           => '',
                "FormaPago"         => $datas['FormaPago'],
                "tarjeta"       => '',
                "MetodoPago"        => $datas['MetodoPago'],
                "ordenCompra"   => '',//no se ocupa
                "moneda"        => $datas['moneda'],
                "observaciones" => '',
                "numproveedor"  => $datas['numproveedor'],
                "numordencompra"=> $datas['numordencompra'],
                "Lote"          => $datas['lote'],
                "Paciente"      => '',//no se ocupa
                //"Caducidad"     => $datas['Caducidad'],//no se ocupa
                "Caducidad"     => '',//no se ocupa
                "uso_cfdi"      => $datas['uso_cfdi'],
                "subtotal"      => $datas['subtotal'],
                "iva"           => $datas['iva'],
                "total"         => $datas['total'],
                "honorario"     => $datas['subtotal'],
                /*"ivaretenido"   => $datas['vriva'],
                "isr"           => $datas['visr'],
                "cincoalmillarval"  => $datas['v5millar'],*/
                "ivaretenido"   => '',
                "isr"           => '',
  
                "CondicionesDePago" => $datas['CondicionesDePago'],
                //"outsourcing" => $datas['outsourcing'],
                "facturaabierta"=>1,
                "f_relacion"    => $datas['f_r'],
                "f_r_tipo"      => $datas['f_r_t'],
                "f_r_uuid"      => $datas['f_r_uuid'],
                "cartaporte"    => $datas['cartaporte'],
                'tipoempresa'   => $tipoempresa,
                'tipofac'       => $datas['tipofac'],
                'sucursal'      => $datas['sucursal']
            );
            $facturainfo="Nombre: $razon_social <br>R.F.C.: $rfc <br>Direccion: $direccion <br>C.P.: $cp <br>Tel: $tel1 <br>eMail: $email";
            $data['rid_relacion_cot_folio']=$rid_relacion_cot_folio;//descomentar cuanto tengamos acceso a su base
        if(isset($datas['pg_global'])){
          $data['pg_global']=$datas['pg_global'];
        }
        if(isset($datas['pg_periodicidad'])){
            $data['pg_periodicidad']=$datas['pg_periodicidad'];
        }
        if(isset($datas['pg_meses'])){
            $data['pg_meses']=$datas['pg_meses'];
        }
        if(isset($datas['pg_anio'])){
            $data['pg_anio']=$datas['pg_anio'];
        }
        $FacturasId=$this->ModeloCatalogos->Insert('f_facturas',$data);
        $DATAc = json_decode($conceptos);
        for ($i=0;$i<count($DATAc);$i++) {

            $dataco['FacturasId']=$FacturasId;
            $dataco['NoIdentificacion'] =$DATAc[$i]->NoIdentificacion;
            $dataco['Cantidad'] =$DATAc[$i]->Cantidad;
            $dataco['Unidad'] =$DATAc[$i]->Unidad;
            $dataco['servicioId'] =$DATAc[$i]->servicioId;
            $dataco['Descripcion'] =rtrim($DATAc[$i]->Descripcion);
            $dataco['Descripcion2'] =$DATAc[$i]->Descripcion2;
            $dataco['Cu'] =$DATAc[$i]->Cu;
            $dataco['descuento'] =$DATAc[$i]->descuento;
            $dataco['Importe'] =($DATAc[$i]->Cantidad*$DATAc[$i]->Cu);
            $dataco['iva'] =$DATAc[$i]->iva;
            $this->ModeloCatalogos->Insert('f_facturas_servicios',$dataco);
        }
        //==========================================================================
        if($datas['cartaporte']==1){
                  $this->ModeloCatalogos->Insert('f_facturas_carta_porte',array('FacturasId'=>$FacturasId,'TranspInternac'=>$TranspInternac,'TotalDistRec'=>$TotalDistRec,'EntradaSalidaMerc'=>$EntradaSalidaMerc,'ViaEntradaSalida'=>$ViaEntradaSalida));

                  $this->ModeloCatalogos->Insert('f_facturas_mercancias',array('FacturasId'=>$FacturasId,'PesoBrutoTotal'=>$PesoBrutoTotal,'UnidadPeso'=>$UnidadPeso,'NumTotalMercancias'=>$NumTotalMercancias,'CargoPorTasacion'=>$CargoPorTasacion));
          //=============================Ubicaciones=====================================================
                    $DATAu = json_decode($ubicaciones);
                    for ($j=0;$j<count($DATAu);$j++) {
                        $datafu['FacturasId']=$FacturasId;
                        $datafu['TipoUbicacion'] = $DATAu[$j]->TipoUbicacion;
                        $datafu['TipoEstacion'] = $DATAu[$j]->TipoEstacion;
                        $datafu['IDUbicacion'] = $DATAu[$j]->IDUbicacion;
                        $datafu['RFCRemitenteDestinatario'] = $DATAu[$j]->RFCRemitenteDestinatario;
                        $datafu['NombreRFC'] = $DATAu[$j]->NombreRFC;
                        $datafu['NumRegIdTrib'] = $DATAu[$j]->NumRegIdTrib;
                        $datafu['ResidenciaFiscal'] = $DATAu[$j]->ResidenciaFiscal;
                        $datafu['NumEstacion'] = $DATAu[$j]->NumEstacion;
                        $datafu['NombreEstacion'] = $DATAu[$j]->NombreEstacion;
                        $datafu['FechaHoraSalidaLlegada'] = $DATAu[$j]->FechaHoraSalidaLlegada;
                        $datafu['DistanciaRecorrida'] = $DATAu[$j]->DistanciaRecorrida;
                        $datafu['Calle'] = $DATAu[$j]->Calle;
                        $datafu['NumeroExterior'] = $DATAu[$j]->NumeroExterior;
                        $datafu['NumeroInterior'] = $DATAu[$j]->NumeroInterior;
                        $datafu['Colonia'] = $DATAu[$j]->Colonia;
                        $datafu['Localidad'] = $DATAu[$j]->Localidad;
                        $datafu['Referencia'] = $DATAu[$j]->Referencia;
                        $datafu['Municipio'] = $DATAu[$j]->Municipio;
                        $datafu['Estado'] = $DATAu[$j]->Estado;
                        $datafu['Pais'] = $DATAu[$j]->Pais;
                        $datafu['CodigoPostal'] = $DATAu[$j]->CodigoPostal;

                        $this->ModeloCatalogos->Insert('f_facturas_ubicaciones',$datafu);
                    }
                //=============================mercancias=====================================================
                    $DATAme = json_decode($mercancias);
                    for ($k=0;$k<count($DATAme);$k++) {
                        $datafm['FacturasId'] = $FacturasId;
                        $datafm['BienesTransp']   = $DATAme[$k]->BienesTransp;
                        $datafm['Descripcion']    = $DATAme[$k]->Descripcion;
                        $datafm['Cantidad']       = $DATAme[$k]->Cantidad;
                        $datafm['ClaveUnidad']    = $DATAme[$k]->ClaveUnidad;
                        $datafm['Dimensiones']    = $DATAme[$k]->Dimensiones;
                        if($DATAme[$k]->Embalaje=='null'){
                            $datafm['Embalaje']       = '';
                        }else{
                            $datafm['Embalaje']       = $DATAme[$k]->Embalaje;
                        }
                        
                        $datafm['DescripEmbalaje'] = $DATAme[$k]->DescripEmbalaje;
                        $datafm['PesoEnKg']       = $DATAme[$k]->PesoEnKg;
                        $datafm['ValorMercancia'] = $DATAme[$k]->ValorMercancia;
                        $datafm['Moneda']         = $DATAme[$k]->Moneda;
                        $datafm['Unidad']         = $DATAme[$k]->Unidad;
                        
                        $this->ModeloCatalogos->Insert('f_facturas_mercancia',$datafm);
                    }
                //=============================transporte=====================================================
                    $DATAtrans = json_decode($transportes);
                    for ($l=0;$l<count($DATAtrans);$l++) {
                        $dataft['FacturasId']=$FacturasId;
                        $dataft['PermSCT'] = $DATAtrans[$l]->PermSCT;
                        $dataft['NumPermisoSCT'] = $DATAtrans[$l]->NumPermisoSCT;
                        $dataft['NombreAseg'] = $DATAtrans[$l]->NombreAseg;
                        $dataft['NumPolizaSeguro'] = $DATAtrans[$l]->NumPolizaSeguro;
                        $dataft['ConfigVehicular'] = $DATAtrans[$l]->ConfigVehicular;
                        $dataft['PlacaVM'] = $DATAtrans[$l]->PlacaVM;
                        $dataft['AnioModeloVM'] = $DATAtrans[$l]->AnioModeloVM;
                        $dataft['SubTipoRem'] = $DATAtrans[$l]->SubTipoRem;
                        $dataft['Placa'] = $DATAtrans[$l]->Placa;

                        $this->ModeloCatalogos->Insert('f_facturas_transporte_fed',$dataft);
                    }
                //=============================aerero=====================================================
                    $DATAtaer = json_decode($aereo);
                    for ($a=0;$a<count($DATAtaer);$a++) {
                        $datafta['FacturasId']=$FacturasId;
                        $datafta['PermSCT'] = $DATAtaer[$a]->PermSCT;
                        $datafta['NumPermisoSCT'] = $DATAtaer[$a]->NumPermisoSCT;
                        $datafta['MatriculaAeronave'] = $DATAtaer[$a]->MatriculaAeronave;
                        $datafta['NombreAseg'] = $DATAtaer[$a]->NombreAseg;
                        $datafta['NumPolizaSeguro'] = $DATAtaer[$a]->NumPolizaSeguro;
                        $datafta['NumeroGuia'] = $DATAtaer[$a]->NumeroGuia;
                        $datafta['LugarContrato'] = $DATAtaer[$a]->LugarContrato;
                        $datafta['RFCTransportista'] = $DATAtaer[$a]->RFCTransportista;
                        $datafta['CodigoTransportista'] = $DATAtaer[$a]->CodigoTransportista;
                        $datafta['NumRegIdTribTranspora'] = $DATAtaer[$a]->NumRegIdTribTranspora;
                        $datafta['ResidenciaFiscalTranspor'] = $DATAtaer[$a]->ResidenciaFiscalTranspor;
                        $datafta['NombreTransportista'] = $DATAtaer[$a]->NombreTransportista;
                        $datafta['RFCEmbarcador'] = $DATAtaer[$a]->RFCEmbarcador;
                        $datafta['NumRegIdTribEmbarc'] = $DATAtaer[$a]->NumRegIdTribEmbarc;
                        $datafta['ResidenciaFiscalEmbarc'] = $DATAtaer[$a]->ResidenciaFiscalEmbarc;
                        $datafta['NombreEmbarcador'] = $DATAtaer[$a]->NombreEmbarcador;

                        $this->ModeloCatalogos->Insert('f_facturas_transporte_aereo',$datafta);
                    }
                //==================================================================================
                //=============================operadores=====================================================
                    $DATAtoper = json_decode($operadores);
                    for ($r=0;$r<count($DATAtoper);$r++) {
                        $dataffo['FacturasId'] = $FacturasId;
                        $dataffo['RFCOperador'] = $DATAtoper[$r]->RFCOperador;
                        $dataffo['NumLicencia'] = $DATAtoper[$r]->NumLicencia;
                        $dataffo['NombreOperador'] = $DATAtoper[$r]->NombreOperador;
                        $dataffo['NumRegIdTribOperador'] = $DATAtoper[$r]->NumRegIdTribOperador;
                        if($DATAtoper[$r]->ResidenciaFiscalOperador=='null'){
                            $dataffo['ResidenciaFiscalOperador'] ='';
                        }else{
                            $dataffo['ResidenciaFiscalOperador'] = $DATAtoper[$r]->ResidenciaFiscalOperador;
                        }
                        
                        $dataffo['adddomicilio'] = $DATAtoper[$r]->adddomicilio;
                        $dataffo['Calle'] = $DATAtoper[$r]->Calle;
                        $dataffo['NumeroExterior'] = $DATAtoper[$r]->NumeroExterior;
                        $dataffo['NumeroInterior'] = $DATAtoper[$r]->NumeroInterior;
                        $dataffo['Colonia'] = $DATAtoper[$r]->Colonia;
                        $dataffo['Localidad'] = $DATAtoper[$r]->Localidad;
                        $dataffo['Referencia'] = $DATAtoper[$r]->Referencia;
                        $dataffo['Municipio'] = $DATAtoper[$r]->Municipio;
                        $dataffo['Estado'] = $DATAtoper[$r]->Estado;
                        $dataffo['Pais'] = $DATAtoper[$r]->Pais;
                        $dataffo['CodigoPostal'] = $DATAtoper[$r]->CodigoPostal;

                        $this->ModeloCatalogos->Insert('f_facturas_figtrans_operadores',$dataffo);
                    }
                //==================================================================================
                //=============================propietario=====================================================
                    $DATAprop = json_decode($propietario);
                    for ($r=0;$r<count($DATAprop);$r++) {
                        $dataffp['FacturasId']=$FacturasId;
                        $dataffp['RFCPropietario'] = $DATAprop[$r]->RFCPropietario;
                        $dataffp['NombrePropietario'] = $DATAprop[$r]->NombrePropietario;
                        $dataffp['NumRegIdTribPropietario'] = $DATAprop[$r]->NumRegIdTribPropietario;
                        if($DATAprop[$r]->ResidenciaFiscalPropietario=='null'){
                            $dataffp['ResidenciaFiscalPropietario'] = '';
                        }else{
                            $dataffp['ResidenciaFiscalPropietario'] = $DATAprop[$r]->ResidenciaFiscalPropietario;
                        }
                        
                        $dataffp['adddomicilio'] = $DATAprop[$r]->adddomiciliop;
                        $dataffp['Calle'] = $DATAprop[$r]->Calle;
                        $dataffp['NumeroExterior'] = $DATAprop[$r]->NumeroExterior;
                        $dataffp['NumeroInterior'] = $DATAprop[$r]->NumeroInterior;
                        $dataffp['Colonia'] = $DATAprop[$r]->Colonia;
                        $dataffp['Localidad'] = $DATAprop[$r]->Localidad;
                        $dataffp['Referencia'] = $DATAprop[$r]->Referencia;
                        $dataffp['Municipio'] = $DATAprop[$r]->Municipio;
                        $dataffp['Estado'] = $DATAprop[$r]->Estado;
                        $dataffp['Pais'] = $DATAprop[$r]->Pais;
                        $dataffp['CodigoPostal'] = $DATAprop[$r]->CodigoPostal;

                        $this->ModeloCatalogos->Insert('f_facturas_figtrans_propietario',$dataffp);
                    }
                //=============================arrendatario=====================================================
                    $DATAarre = json_decode($arrendatario);
                    for ($r=0;$r<count($DATAarre);$r++) {
                        $dataffa['FacturasId']=$FacturasId;
                        $dataffa['RFCArrendatario'] = $DATAarre[$r]->RFCArrendatario;
                        $dataffa['NombreArrendatario'] = $DATAarre[$r]->NombreArrendatario;
                        $dataffa['NumRegIdTribArrendatario'] = $DATAarre[$r]->NumRegIdTribArrendatario;
                        $dataffa['ResidenciaFiscalOperador'] = $DATAarre[$r]->ResidenciaFiscalArrendatario;
                        $dataffa['adddomicilio'] = $DATAarre[$r]->adddomicilioa;
                        $dataffa['Calle'] = $DATAarre[$r]->Calle;
                        $dataffa['NumeroExterior'] = $DATAarre[$r]->NumeroExterior;
                        $dataffa['NumeroInterior'] = $DATAarre[$r]->NumeroInterior;
                        $dataffa['Colonia'] = $DATAarre[$r]->Colonia;
                        $dataffa['Localidad'] = $DATAarre[$r]->Localidad;
                        $dataffa['Referencia'] = $DATAarre[$r]->Referencia;
                        $dataffa['Municipio'] = $DATAarre[$r]->Municipio;
                        $dataffa['Estado'] = $DATAarre[$r]->Estado;
                        $dataffa['Pais'] = $DATAarre[$r]->Pais;
                        $dataffa['CodigoPostal'] = $DATAarre[$r]->CodigoPostal;

                        $this->ModeloCatalogos->Insert('f_facturas_figtrans_arrendatario',$dataffa);
                    }
                //==================================================================================
                        if($datas['cartaporte']==1){
                          $CveTransporte=$datas['CveTransporte'];

                          $this->ModeloCatalogos->Insert('f_facturas_figura_transporte',array('FacturasId'=>$FacturasId,'CveTransporte'=>$CveTransporte));
                        }
        }       
                //==================================================================================
        //==========================================================================
        $respuesta=$this->emitirfacturas($FacturasId,0); 
        if($rid_relacion>0){
          $dc_result=$this->ModeloCatalogos->getselectwheren('doc_cotiza',array('rid'=>$rid_relacion));
          foreach ($dc_result->result() as $itemdc) {
            $rid_relacion_cot_folio=$itemdc->cot_folio;
            $params_update['rid_relacion']=$rid_relacion;
            if($itemdc->cot_folio!=''){
              $params_update['rid_relacion_cot_folio']=$rid_relacion_cot_folio;
            }
            $this->ModeloCatalogos->updateCatalogo('f_facturas',$params_update,array('FacturasId'=>$FacturasId));
            $this->ModeloCatalogos->updateCatalogo('doc_cotiza',array('facturado'=>$FacturasId),array('rid'=>$rid_relacion));
          }
          
        }
        if($rid_relacion_cot_folio!=''){
          $dc_result=$this->ModeloCatalogos->getselectwheren('guias',array('folio'=>$rid_relacion_cot_folio));
          foreach ($dc_result->result() as $itemf) {
            $guia_rid = $itemf->rid;
            $this->ModeloCatalogos->updateCatalogo('guia_facts',array('facturainfo'=>$facturainfo),array('guia'=>$guia_rid));

          }
        }
        
        echo json_encode($respuesta);
    }

    function retimbrar(){
        $factura = $this->input->post('factura');
        $id_venta = $this->input->post('id_venta');
        $facturaresult=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$factura));
        $clienteId=0;
        foreach ($facturaresult->result() as $item) {
            $Folio=$item->Folio;
            $serie=$item->serie;
            //if($Folio==0){
            $newfolio=$this->ModeloCatalogos->ultimoFolio($serie) + 1;
            $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Folio'=>$newfolio),array('FacturasId'=>$factura));
            //}
            $clienteId=$item->Clientes_ClientesId;
        }
        if($clienteId>0){
          $datoscliente=$this->ModeloCatalogos->getselectwheren('cli_facts',array('rid'=>$clienteId));
          $datoscliente=$datoscliente->result();
          $datoscliente=$datoscliente[0];
          $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Nombre'=>$datoscliente->fac_nrs,'Rfc'=>$datoscliente->fac_rfc),array('FacturasId'=>$factura));
        }
        $respuesta=$this->emitirfacturas($factura,$id_venta);
        echo json_encode($respuesta);
    }

    function emitirfacturas($facturaId,$id_venta){
        $productivo=0;//0 demo 1 producccion
        //$this->load->library('Nusoap');
        //require_once APPPATH."/third_party/nusoap/nusoap.php"; 
        //$datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        //$datosconfiguracion=$datosconfiguracion->result();
        //$datosconfiguracion=$datosconfiguracion[0];
        //========================
        $datosFacturaa=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$facturaId));
        $datosFacturaa=$datosFacturaa->result();
        $datosFacturaa=$datosFacturaa[0];
        //============
        //===================================
          $tipoempresa=$datosFacturaa->tipoempresa;
          if($tipoempresa>0){

          }else{
            $tipoempresa=1;
          }
          if($tipoempresa==1){
            $tipoempresaurl='';
          }else{
            $tipoempresaurl='2';
          }
          $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$tipoempresa));
          $datosconfiguracion=$datosconfiguracion->result();
          $datosconfiguracion=$datosconfiguracion[0];
        //===================================
        $datosFactura = array(
          'carpeta'=>'files_sat',
          'pwskey'=>'',
          'archivo_key'=>'',
          'archivo_cer'=>'',
          'factura_id'=>$facturaId
        );
        //================================================
          $respuesta=array();
          $passwordLlavePrivada   = $datosFactura['pwskey'];
          $nombreArchivoKey       = base_url() . $datosFactura['archivo_key'];
          $nombreArchivoCer       = base_url() . $datosFactura['archivo_cer'];
          // ---------------- INICIAN COMANDOS OPENSSL --------------------
          //Generar archivo que contendra cadena de certificado (Convertir Certificado .cer a .pem)
          $comando='openssl x509 -inform der -in '.$nombreArchivoCer.' -out ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem';    
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -startdate > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/IniciaVigencia.txt';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -enddate > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/FinVigencia.txt';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -serial > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -text > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/datos.txt';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl pkcs8 -inform DER -in '.$nombreArchivoKey.' -passin pass:'.$passwordLlavePrivada.' > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/llaveprivada_pem.txt';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl pkcs8 -inform DER -in '.$nombreArchivoKey.' -passin pass:'.$passwordLlavePrivada.' > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/llaveprivada.pem';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
        // ---------------- TERMINAN COMANDOS OPENSSL --------------------      
          //GUARDAR NUMERO DE SERIE EN ARCHIVO     
          //$archivo=fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt','r');
          //$numeroCertificado= fread($archivo, filesize(base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt'));   
          $numeroCertificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat'.$tipoempresaurl.'/Serial.txt');
          //fclose($archivo);
          $numeroCertificado=  str_replace("serial=", "", $numeroCertificado);//quitar letras
          $temporal=  str_split($numeroCertificado);//Pasar todos los digitos a un array
          $numeroCertificado="";
          $i=0;  
          foreach ($temporal as $value) {
              if(($i%2))
                  $numeroCertificado .= $value;
              $i++;
          }
        
        $numeroCertificado = str_replace('\n','',$numeroCertificado);     
        $numeroCertificado = trim($numeroCertificado);
        
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('certificado'=>$numeroCertificado),array('FacturasId'=>$facturaId));
        //$this->model_mango_invoice->actualizaCampoFactura('certificado',$numeroCertificado,$datosFactura['factura_id']);
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('nocertificado'=>$numeroCertificado),array('FacturasId'=>$facturaId));
        //$this->model_mango_invoice->actualizaCampoFactura('nocertificado',$numeroCertificado,$datosFactura['factura_id']);
        
        $respuesta['numeroCertificado']=$numeroCertificado;
        //$archivo = fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem', "r");
        //$certificado = fread($archivo, filesize(base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem'));
        $certificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat'.$tipoempresaurl.'/certificate.pem');
        
        //fclose($archivo);
        $certificado=  str_replace("-----BEGIN CERTIFICATE-----", "", $certificado);
        $certificado=  str_replace("-----END CERTIFICATE-----", "", $certificado);
        
        $certificado=  str_replace("\n", "", $certificado);
        $certificado=  str_replace(" ", "", $certificado);
        $certificado=  str_replace("\n", "", $certificado);
        $certificado= preg_replace("[\n|\r|\n\r]", '', $certificado);
        $certificado= trim($certificado);
        
        $respuesta['certificado']=$certificado;
            
        $arrayDatosFactura=  $this->getArrayParaCadenaOriginal($facturaId);  
        //=================================================================
          $horaactualm1=date('H:i:s');
          $horaactualm1 = strtotime ( '-1 hours -1 minute' , strtotime ( $horaactualm1 ) ) ;
          $horaactualm1=date ( 'H:i:s' , $horaactualm1 );
          $datosfacturaadd['fechageneracion']=date('Y-m-d').'T'.$horaactualm1;
        //==================================================================
        $xml=  $this->getXML($facturaId,'',$certificado,$numeroCertificado,$datosfacturaadd);
        //echo "contruccion de xml ===========================";  //borrar despues 
        //echo $xml;
        //echo "=====";
        
        log_message('error', 'generaCadenaOriginalReal xml1: '.$xml);
        //log_message('error', '-');log_message('error', '-');log_message('error', '-');log_message('error', '-');log_message('error', '-');
        $str = trim($this->generaCadenaOriginalReal($xml,$datosFactura['carpeta']));    
        log_message('error', 'generaCadenaOriginalReal xml2: '.$str);
        $cadena=$str;
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('cadenaoriginal'=>$str),array('FacturasId'=>$datosFactura['factura_id']));
        //$clienteSOAP = new nusoap_client('https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL',array('soap_version' => SOAP_1_2)); //productivo
        //$clienteSOAP = new nusoap_client('https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL');//productivo
        if($productivo==0){
          $URL_WS='https://app.appfacturainteligente.com/WSTimbrado33Test/WSCFDI33.svc?WSDL';
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.appfacturainteligente.com/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        
        //$clienteSOAP = new nusoap_client('https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL');//pruebas
        $clienteSOAP = new SoapClient($URL_WS);
        //$clienteSOAP->soap_defencoding='UTF-8';
        $referencia=$datosFactura['factura_id']."-C-". $this->idpersonal;
        
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "COE960119D33";
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
          if($tipoempresa==1){
            // parece que si tiene permisos
            $usuario= "COE960119HI6";
            $password= "C@9eCai5";
          }
          if($tipoempresa==2){
            // no tiene permisos
            //$usuario= "CAD070509Q18";
            //$password= "Cad18@3F";
            $usuario= "CAD070509Q18";
            $password= 'Vh8$yvolup';
          }
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "COE960119D33";
        }
        
        $fp = fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat'.$tipoempresaurl.'/llaveprivada.pem', "r");       
        $priv_key = fread($fp, 8192); 
        fclose($fp); 
        $pkeyid = openssl_get_privatekey($priv_key);
    
        //openssl_sign($cadena, $sig, $pkeyid);
        openssl_sign(utf8_encode($cadena), $sig, $pkeyid,'sha256');
        openssl_free_key($pkeyid);
        
        $sello = base64_encode($sig);
        //echo $sello;
        $respuesta['sello']=$sello;
        
        //$this->model_mango_invoice->actualizaCampoFactura('sellocadena',$sello,$datosFactura['factura_id']);
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('sellocadena'=>$sello),array('FacturasId'=>$datosFactura['factura_id']));
        //Gererar XML 2013-12-05T12:05:56
        $xml=  $this->getXML($facturaId,$sello,$certificado,$numeroCertificado,$datosfacturaadd);                
        //$fp = fopen(DIR_FILES . $datosFactura['carpeta'] . '/facturas/preview_'. $datosFactura['rfcEmisor'].'_'. $datosFactura['folio'].'.xml', 'w');//Cadena original 
        file_put_contents($datosFactura['carpeta'].'/facturas/preview_'. $datosconfiguracion->Rfc.'_'. $datosFacturaa->Folio.'.xml',$xml);
                
        //Preparar parametros para web service    
        //cadenaXML

        $result = $clienteSOAP->TimbrarCFDI(array(
                    'usuario' => $usuario,
                    'password' => $password,
                    'cadenaXML' => $xml,
                    'referencia' => $referencia));
        file_put_contents($datosFactura['carpeta'] . '/facturaslog/'.'log_facturas_result_'.$this->fechahoyL.'_.txt', json_encode($result));
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Referencia'=>$referencia),array('FacturasId'=>$datosFactura['factura_id']));
    
        if( $result->TimbrarCFDIResult->CodigoRespuesta!= '0' ) {                       
            log_message('error', 'resultadoerrineo');
            $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                          'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                          'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                          'facturaId'=>$facturaId,
                          'info'=>$result
                          );
            //$this->session->data['error'] = $resultado['CodigoRespuesta'] . ' ' . $resultado['MensajeError'] . ' ' . $resultado['MensajeErrorDetallado']. ' No se puede timbrar la factura verifique la informacion por favor <BR/> PORFAVOR RETIMBRAR MAS TARDE';
                        
            //$this->load->model("setting/general");  
            
            //$this->model_setting_general->saveLog($this->session->data['id_usuario'],$this->session->data['activo'],$resultado,2);
            $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>2,'mensajeerror'=>json_encode($result)),array('FacturasId'=>$datosFactura['factura_id']));
            //$this->model_mango_invoice->actualizaCampoFactura('Estado','2',$datosFactura['factura_id']);
            
            //quirar restriccion de salida de sesion por error 12/02/2014 david garduño
            //$this->redirect($this->url->link('common/logout', 'token=' . $this->session->data['token'], 'SSL'));
            
            return $resultado;
        
        } else {
            try {
                log_message('error', 'resultado correcto'.json_encode($result));
                $xmlCompleto=utf8_decode($result->TimbrarCFDIResult->XMLResultado);//Contiene XML
                //Guardar en archivo     y la ruta en la bd
                $ruta = $datosFactura['carpeta'] . '/facturas/'.$datosconfiguracion->Rfc.'_'.$datosFacturaa->Folio.'.xml';
                file_put_contents($ruta,"\xEF\xBB\xBF".$xmlCompleto);
                $this->ModeloCatalogos->updateCatalogo('f_facturas',array('rutaXml'=>$ruta),array('FacturasId'=>$datosFactura['factura_id']));            
                //$sxe = new SimpleXMLElement($xmlCompleto);
                //$ns = $sxe->getNamespaces(true);
                //$sxe->registerXPathNamespace('c', $ns['cfdi']);
                //$sxe->registerXPathNamespace('t', $ns['tfd']);
                //$uuid = '';
                //foreach ($sxe->xpath('//t:TimbreFiscalDigital') as $tfd) {          
                    //Actualizamos el Folio Fiscal UUID
                    //$uuid = $tfd['UUID'];
                    $updatedatossat=array(
                      'uuid'=>$result->TimbrarCFDIResult->Timbre->UUID,
                      'sellosat'=>$result->TimbrarCFDIResult->Timbre->SelloSAT,
                      'nocertificadosat'=>$result->TimbrarCFDIResult->Timbre->NumeroCertificadoSAT,
                      'nocertificado'=>$numeroCertificado,
                      'certificado'=>$numeroCertificado,
                      'fechatimbre'=>date('Y-m-d G:i:s'),
                      'Estado'=>1
                    );
                    $this->ModeloCatalogos->updateCatalogo('f_facturas',$updatedatossat,array('FacturasId'=>$datosFactura['factura_id'])); 
                //} 
            
                //$this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>1),array('FacturasId'=>$datosFactura['factura_id']));

                $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Generada',
                          'facturaId'=>$facturaId
                          );
            return $resultado;
            
           }  catch (Exception $e) {
              $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                          'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                          'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                          'facturaId'=>$facturaId,
                          'info'=>$result
                          );
             
            $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>2,'mensajeerror'=>json_encode($result)),array('FacturasId'=>$datosFactura['factura_id']));
            return $resultado;
           }
           
        }
        return $resultado;
    }

    function getArrayParaCadenaOriginal($facturaId){
      $datosFactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$facturaId));
      $datosFactura=$datosFactura->result();
      $datosFactura=$datosFactura[0];

      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      //=======================================================================
            $conceptos = array();
            $datosconceptos=$this->ModeloCatalogos->getselectwheren('f_facturas_servicios',array('FacturasId'=>$facturaId));
            foreach ($datosconceptos->result() as $item) {
              $unidad=$this->ModeloCatalogos->nombreunidadfacturacion($item->Unidad);
              $conceptos[]    =   array(
                                       'cantidad' => $item->Cantidad,
                                       'unidad' =>  $unidad,
                                       'descripcion' =>  $item->Descripcion,
                                       'valorUnnitario' =>  $item->Cu,
                                       'importe' =>  $item->Importe
                                    );
            }
      //=======================================================================
      $datos=array(
                  'comprobante' => array(
                                            'version' => '3.2',  //Requerido                     
                                            'fecha' => $this->fechahoyc,       //Requerido                
                                            'tipoDeComprobante' => $datosFactura->TipoComprobante,//Requedido                       
                                            'formaDePago' => $datosFactura->FormaPago,                       
                                            'subtotal' => $datosFactura->subtotal,                       
                                            'total' => $datosFactura->total,                       
                                            'metodoDePago' => $datosFactura->MetodoPago,                       
                                            'lugarExpedicion' => $datosconfiguracion->LugarExpedicion                       
                                        ),
                  'emisor'    => array(
                                            'rfc' => $datosconfiguracion->Rfc,//Requedido
                                            'domocilioFiscal' => array(
                                                                        'calle' => $datosconfiguracion->Calle,//Requerido
                                                                        'municipio' => $datosconfiguracion->Municipio,//Requerido 
                                                                        'estado' => $datosconfiguracion->Estado,//Requerido 
                                                                        'pais' => $datosconfiguracion->PaisExpedicion,//Requedido
                                                                        'codigoPostal' => $datosconfiguracion->CodigoPostal//Requedido
                                                                        ),
                                            'expedidoEn' => array(
                                                                        'pais' => $datosconfiguracion->PaisExpedicion//Requedido
                                                                 ),
                                            'regimenFiscal' => array(
                                                                        'regimen' => $datosconfiguracion->CodigoPostal//Requedido
                                                                    )
                                      ),
                  'receptor'    => array(
                                            'rfc' => $datosFactura->Rfc,//Requedido
                                            'nombreCliente' => $datosFactura->Nombre,//Opcional
                                            'domicilioFiscal' => array( 
                                                            'calle' => '',//Opcional
                                                            'noExterior' => '',//Opcional
                                                            'colonia' => '',//Opcional                   todos son datos del receptor
                                                            'municipio' => '',//Opcional
                                                            'estado' => '',//Opcional
                                                            'pais' => $datosFactura->PaisReceptor //Requedido
                                                           )
                                      ),
                  'conceptos' => $conceptos,
                  'traslados' => array(
                                        'impuesto' => '002',//iva
                                        'tasa' => 16.00,
                                        'importe' => $datosFactura->subtotal,
                                        'totalImpuestosTrasladados' => $datosFactura->iva
                                      )
                                      
                );
         return $datos;
    }
    public function generaCadenaOriginalReal($xml,$carpeta) {  
        error_reporting(0); 
        $paso = new DOMDocument();
        //log_message('error', 'generaCadenaOriginalReal xml: '.$xml);
        $paso->loadXML($xml);
        $xsl = new DOMDocument();            
        //$file= base_url().$carpeta."/xslt33/cadenaoriginal_3_3.xslt";
        $file= base_url().$carpeta."/xslt40/cadenaoriginal_4_0.xslt";
        $xsl->load($file);
        $proc = new XSLTProcessor();
        // activar php_xsl.dll
        $proc->importStyleSheet($xsl); 
        $cadena_original = $proc->transformToXML($paso);
        //echo '<br><br>' . $cadena_original . '<br><br>';
        $str = mb_convert_encoding($cadena_original, 'UTF-8');                      
        //GUARDAR CADENA ORIGINAL EN ARCHIVO  
        $sello = utf8_decode($str);
        $sello = str_replace('#13','',$sello);
        $sello = str_replace('#10','',$sello);
        //$fp = fopen(DIR_FILES . $carpeta . '/temporalsat/CadenaOriginal.txt', 'w');//Cadena original 
        /*
        $fp = fopen(base_url() . $carpeta . '/temporalsat/CadenaOriginal.txt', 'w');//Cadena original 
        fwrite($fp, $sello);
        fclose($fp);
        */
        file_put_contents( $carpeta . '/temporalsat/CadenaOriginal.txt',$sello);
        return $sello;    
    }

    private function getXML($facturaId,$sello,$certificado,$numeroCertificado,$datosfacturaadd) {
      log_message('error', '$sello c: '.$sello);
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        //==============================================================================================================

        $datosFactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$facturaId));
        $datosFactura=$datosFactura->result();
        $datosFactura=$datosFactura[0];
        //====================================
        $clienteId=$datosFactura->Clientes_ClientesId;
        $datoscliente=$this->ModeloCatalogos->getselectwheren('cli_facts',array('rid'=>$clienteId));
        $datoscliente=$datoscliente->result();
        $datoscliente=$datoscliente[0];
        //===============================================================================================================
          $cartaporte=$datosFactura->cartaporte;
          $tipoempresa=$datosFactura->tipoempresa;
          if($tipoempresa>0){
            $tipoempresas=$tipoempresa;
          }else{
            $tipoempresas=1;
          }

          $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$tipoempresas));
          $datosconfiguracion=$datosconfiguracion->result();
          $datosconfiguracion=$datosconfiguracion[0];
        //====================================================================
            $xmlConceptos = '';
            $conceptos = array();
            $datosconceptos=$this->ModeloCatalogos->getselectwheren('f_facturas_servicios',array('FacturasId'=>$facturaId));
            foreach ($datosconceptos->result() as $item) {
                $unidad=$this->ModeloCatalogos->nombreunidadfacturacion($item->Unidad);
                $xmlConceptos.= '<cfdi:Concepto cantidad="' . chop(ltrim(trim($item->Cantidad))) . '" ';
                $xmlConceptos.= 'unidad="' . chop(ltrim($unidad)) . '" descripcion="' . chop(ltrim($item->servicioId)) . '"';
                $xmlConceptos.= ' valorUnitario="' . chop(ltrim(trim(round($item->Cu, 2)))) . '" importe="' . round(($item->Importe), 2) . '"/>';

              $conceptos[]    =   array(
                                       'cantidad' => $item->Cantidad,
                                       'unidad' =>  $item->Unidad,
                                       'descripcion' =>  $item->Descripcion,
                                       'valorUnnitario' =>  $item->Cu,
                                       'importe' =>  $item->Importe
                                    );
            }
            $datosFactura_cp=$this->ModeloCatalogos->getselectwheren('f_facturas_carta_porte',array('FacturasId'=>$facturaId));
            $datosFactura_cp=$datosFactura_cp->row();
            $datosFactura_u=$this->ModeloCatalogos->getselectwheren('f_facturas_ubicaciones',array('FacturasId'=>$facturaId));
            $datosFactura_fms=$this->ModeloCatalogos->getselectwheren('f_facturas_mercancias',array('FacturasId'=>$facturaId));
            $datosFactura_fms=$datosFactura_fms->row();
            $datosFactura_fm=$this->ModeloCatalogos->getselectwheren('f_facturas_mercancia',array('FacturasId'=>$facturaId));
            $datosFactura_tf=$this->ModeloCatalogos->getselectwheren('f_facturas_transporte_fed',array('FacturasId'=>$facturaId));
            $datosFactura_ta=$this->ModeloCatalogos->getselectwheren('f_facturas_transporte_aereo',array('FacturasId'=>$facturaId));
            $datosFactura_fa=$this->ModeloCatalogos->getselectwheren('f_facturas_figtrans_arrendatario',array('FacturasId'=>$facturaId));
            $datosFactura_fo=$this->ModeloCatalogos->getselectwheren('f_facturas_figtrans_operadores',array('FacturasId'=>$facturaId));
            $datosFactura_fp=$this->ModeloCatalogos->getselectwheren('f_facturas_figtrans_propietario',array('FacturasId'=>$facturaId));
            $datosFactura_ft=$this->ModeloCatalogos->getselectwheren('f_facturas_figura_transporte',array('FacturasId'=>$facturaId));
            $datosFactura_ft=$datosFactura_ft->row();
        //===============================================================================================================
        
        /******************************************************/
        /*          INICIA ASIGNACION DE VARIABLES           */
        /****************************************************/
        //$certificado=str_replace("\n", "", $certificado);
        $caract   = array('&','ñ','Ñ','°','á','é','í','ó','ú','Á','É','Í','Ó','Ú');
        $caract2  = array('&amp;','&ntilde;','&ntilde;','','a','e','i','o','u','A','E','I','O','U');
        $caractc   = array('&','ñ','Ñ','°','á','é','í','ó','ú','Á','É','Í','Ó','Ú');
        $caract2c  = array('&amp;','&ntilde;','&ntilde;','','a','e','i','o','u','A','E','I','O','U');

        $caractx   = array('&','°','á','é','í','ó','ú','Á','É','Í','Ó','Ú');
        $caract2x  = array('&amp;','','a','e','i','o','u','A','E','I','O','U');
        $fecha=$this->fechahoyc;
        $calleEmisor=$datosconfiguracion->Calle;
        $NoexteriorEmisor=$datosconfiguracion->Noexterior;
        $coloniaEmisor=$datosconfiguracion->Colonia;
        $localidadEmisor=$datosconfiguracion->localidad;
        $estadoEmisor=$datosconfiguracion->Estado;
        $municipioEmisor=$datosconfiguracion->Municipio;
        $codigoPostalEmisor=$datosconfiguracion->CodigoPostal;
        $formaPago=$datosFactura->FormaPago;
        $subtotal=$datosFactura->subtotal;
        $total=$datosFactura->total;
        $uso_cfdi=$datosFactura->uso_cfdi;
        $FacturasId=$datosFactura->FacturasId;
        $serie=$datosFactura->serie;
        $f_relacion=$datosFactura->f_relacion;
        $f_r_tipo=$datosFactura->f_r_tipo;
        $f_r_uuid=$datosFactura->f_r_uuid;
        $impuesto='002';
        $tasa=16.00;
        $impuestosTrasladados=$datosFactura->iva;
        if($this->trunquearredondear==0){
          $impuestosTrasladados=round($impuestosTrasladados,2);
        }else{
          $impuestosTrasladados=floor(($impuestosTrasladados*100))/100;
        }
        $tipoComprobante='Ingreso';
        $TipoDeComprobante=$datosFactura->TipoComprobante;
        $metodoPago=$datosFactura->MetodoPago;
        $lugarExpedicion=$datosconfiguracion->LugarExpedicion;
        $rfcEmisor=$datosconfiguracion->Rfc;
        $nombreEmisor=strval(str_replace($caract, $caract2, $datosconfiguracion->Nombre));
        $paisEmisor=$datosconfiguracion->Pais;
        $paisExpedicion=$datosconfiguracion->PaisExpedicion;
        $regimenFiscal=$datosconfiguracion->Regimen;
        $rfcReceptor=str_replace(array(' ','&'),array('','&amp;'), $datosFactura->Rfc);
        $paisReceptor=$datosFactura->PaisReceptor;
        
        $localidadReceptor='';
        
        $xmlConceptos=$xmlConceptos;
        $nombreCliente=strval(str_replace($caract, $caract2, $datosFactura->Nombre));
        $CondicionesDePago = strval(str_replace($caractc, $caract2c, $datosFactura->CondicionesDePago));
        //$calleReceptor=$datosFactura['calleReceptor'];
        //$noExteriorReceptor=$datosFactura['noExteriorReceptor'];
        //$municipioReceptor=$datosFactura['municipioReceptor'];
        //$estadoReceptor=$datosFactura['estadoReceptor'];
        //$coloniaReceptor=$datosFactura['coloniaReceptor'];
        $folio=$datosFactura->Folio;

        $isr=$datosFactura->isr;
        $ivaretenido=$datosFactura->ivaretenido;
        $cedular=$datosFactura->cedular;
        $cincoalmillarval=$datosFactura->cincoalmillarval;
        $outsourcing=$datosFactura->outsourcing;
        $totalimpuestosretenido=$isr+$ivaretenido+$outsourcing;

        if ($totalimpuestosretenido==0) {
            $TotalImpuestosRetenidoss='';
        }else{
            $TotalImpuestosRetenidoss='TotalImpuestosRetenidos="'.round($totalimpuestosretenido,2).'"';
        }
        if ($cedular!=0) {
            $impuestoslocal='xmlns:implocal="http://www.sat.gob.mx/implocal"';
            $impuestoslocal2=' http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd';
        }else{
            $impuestoslocal='';
            $impuestoslocal2='';
        }
        if ($cincoalmillarval>0) {
            $impuestoslocal='xmlns:implocal="http://www.sat.gob.mx/implocal"';
            $impuestoslocal2=' http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd';
        }else{
            $impuestoslocal='';
            $impuestoslocal2='';
        }

        
        //cambios para poner el acr?nimo de la moneda en el XML
        if ($datosFactura->moneda=="USD") {
            $moneda="USD";
            $TipoCambio=' TipoCambio="19.35" ';
        }elseif ($datosFactura->moneda=="EUR") {
            $moneda="EUR";
            $TipoCambio='';
        }else{
            $moneda="MXN";
            $TipoCambio='';
        }
        //DATOS DE TIMBRE
        if(isset($datosFactura->uuid)) { 
        
            $uuid = $datosFactura->uuid;
            
        } else {
            
            $uuid= '';   
        }
        if(isset($datosFactura->sellosat)) {
            
            $selloSAT = $datosFactura->sellosat;
        } else {
            
            $selloSAT = '';
        }
        if(isset($datosFactura->certificado)) {   
            $certificadoSAT=$datosFactura->certificado;
        } else {
            $certificadoSAT='';
        }
        /*
        if(!empty($datosFactura['noInteriorReceptor'])) {   
            $noInt = '" noInterior="' . $datosFactura['noInteriorReceptor'] . '';
        } else {
            
            $noInt = '';
        }
        */
        /*  $minuto=date('i');
        $m=$minuto-1;
        $contar=strlen($m);
        if($contar==1)
        {
        $min='0'.$m;
        }else{$min=$m;} */  
        
        
        
        //.date('Y-m-d').'T'.date('H:i').':'.$segundos.

        $importabasetotal=0; 
        
        /******************************************************/
        /*          TERMINA ASIGNACION DE VARIABLES          */
        /****************************************************/   
        /*
        $horaactualm1=date('H:i:s');
        $horaactualm1 = strtotime ( '-1 minute' , strtotime ( $horaactualm1 ) ) ;
        $horaactualm1=date ( 'H:i:s' , $horaactualm1 );
        */
        $factura_detalle = $this->ModeloCatalogos->traeProductosFactura($FacturasId); 
        $descuentogeneral=0;
        foreach ($factura_detalle->result() as $facturadell) {
          $descuentogeneral=$descuentogeneral+$facturadell->descuento;
        }

        if ($descuentogeneral>0) {
          $xmldescuentogene='Descuento="'.$descuentogeneral.'" ';
        }else{
          $xmldescuentogene='';
        }
        if($rfcReceptor=='XAXX010101000'){
            $DomicilioFiscalReceptor=$codigoPostalEmisor;
            $RegimenFiscalReceptor=616;
            $uso_cfdinew='S01';
        }else{
            $DomicilioFiscalReceptor=str_pad($datoscliente->fac_cod, 5, "0", STR_PAD_LEFT);
            $RegimenFiscalReceptor=$datoscliente->RegimenFiscalReceptor;
            $uso_cfdinew=$uso_cfdi;
        }
        log_message('error', '$cartaporte '.$cartaporte);
        if($cartaporte==1){
          $cp20=' xmlns:cartaporte20="http://www.sat.gob.mx/CartaPorte20" ';
          $cp20_2=' http://www.sat.gob.mx/CartaPorte20 http://www.sat.gob.mx/sitio_internet/cfd/CartaPorte/CartaPorte20.xsd ';
           log_message('error', '$cp20 '.$cp20);
           $subtotal=0;
           $total=0;
           //$moneda='XXX';
           $uso_cfdinew='S01';
        }else{
          $cp20='';
          $cp20_2='';
        }
        //$nombrereceptor=$datoscliente->fac_nrs;
        $nombrereceptor=strval(str_replace($caractx, $caract2x, $datoscliente->fac_nrs));
        $tang_FormaPago='FormaPago="'.$metodoPago.'"';
        $tang_MetodoPago='MetodoPago="'.$formaPago.'"';
        $tang_CondicionesDePago='CondicionesDePago="'.$CondicionesDePago.'"';
        if($cartaporte==1){
          if($TipoDeComprobante=='T'){
            $rfcReceptor=$rfcEmisor;
            $tang_FormaPago='';
            $tang_MetodoPago='';
            $tang_CondicionesDePago='';
            //$nombrereceptor=$nombreEmisor;
            $moneda='XXX';

          }
        }
        //$nombrereceptor='Compuhipermegared';//solo para pruebas
        $fechageneracion=$datosfacturaadd['fechageneracion'];
        $xml= '<?xml version="1.0" encoding="utf-8"?>';
        $xml.='<cfdi:Comprobante '.$cp20.' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '.$impuestoslocal.' xsi:schemaLocation="http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd'.$impuestoslocal2.' '.$cp20_2.'" Exportacion="01" ';
        $xml .= 'Version="4.0" Serie="'.$serie.'" Folio="'.$folio.'" Fecha="'.$fechageneracion.'" '.$tang_FormaPago.' '.$tang_CondicionesDePago.' ';
        //$xml .= 'SubTotal="'.$subtotal.'" Descuento="0.00" Moneda="'.$moneda.'" TipoCambio="1" Total="'.$total.'" TipoDeComprobante="I" MetodoPago="'.$formaPago.'" LugarExpedicion="' .$codigoPostalEmisor . '" ';
        $xml .= 'SubTotal="'.$subtotal.'" Moneda="'.$moneda.'" '.$xmldescuentogene.''.$TipoCambio.'Total="'.round($total,2).'" TipoDeComprobante="'.$TipoDeComprobante.'" '.$tang_MetodoPago.' LugarExpedicion="' .$codigoPostalEmisor . '" ';
        $xml .='xmlns:cfdi="http://www.sat.gob.mx/cfd/4" NoCertificado="'.$numeroCertificado.'" Certificado="'.$certificado.'" Sello="'.$sello.'">';
                if($f_relacion==1){
                    $xml .='<cfdi:CfdiRelacionados TipoRelacion="'.$f_r_tipo.'">
                                <cfdi:CfdiRelacionado UUID="'.$f_r_uuid.'"/>
                            </cfdi:CfdiRelacionados>';
                }
                if($rfcReceptor=='XAXX010101000' and $datosFactura->pg_global==1){
                    $xml .= '<cfdi:InformacionGlobal Año="'.$datosFactura->pg_anio.'" Meses="'.$datosFactura->pg_meses.'" Periodicidad="'.$datosFactura->pg_periodicidad.'"/>';
                }
        $xml .= '<cfdi:Emisor Nombre="'.$nombreEmisor.'" RegimenFiscal="'.$regimenFiscal.'" Rfc="'.$rfcEmisor.'"></cfdi:Emisor>';
        $xml .= '<cfdi:Receptor Rfc="'.$rfcReceptor.'" Nombre="'.$nombrereceptor.'" DomicilioFiscalReceptor="'.$DomicilioFiscalReceptor.'" RegimenFiscalReceptor="'.$RegimenFiscalReceptor.'" UsoCFDI="'.$uso_cfdinew.'" ></cfdi:Receptor>';
        $xml .='<cfdi:Conceptos>';
        
        //$isr=$datosFactura['isr'];
        //$ivaretenido=$datosFactura['ivaretenido'];
        //$cedular=$datosFactura['cedular'];

        
        $partidaproducto=1;
        $niva=$tasa/100;
        $comparariva = $datosFactura->iva;//colocar
        foreach ($factura_detalle->result() as $facturadell) {
            $valorunitario=$facturadell->Cu;
            if($facturadell->descuento>0){
              $xmldescuento=' Descuento="'.$facturadell->descuento.'" ';
            }else{
              $xmldescuento='';
            }
            if($comparariva>0){
                $ObjetoImp='02';//si, objeto de impuestos
            }else{
                $ObjetoImp='01';//no objeto de impuesto
            }
            if($rfcReceptor=='XAXX010101000'){
                $xml_unidad='';
            }else{
                $xml_unidad=' Unidad="'.$facturadell->nombre.'" ';
            }
            
            //$xml .='<cfdi:Concepto ClaveUnidad="'.$facturadell['cunidad'].'" Unidad="'.$facturadell['nombre'].'" ClaveProdServ="'.$facturadell['ClaveProdServ'].'" NoIdentificacion="'.str_pad((int) $partidaproducto,10,"0",STR_PAD_LEFT).'" Cantidad="'.$facturadell['Cantidad'].'" Descripcion="'.$facturadell['Descripcion'].'" ValorUnitario="'.$facturadell['Cu'].'" Importe="'.$facturadell['Importe'].'" Descuento="0.00">';
            $Importedell=$facturadell->Importe;
            if($TipoDeComprobante=='T'){
              $valorunitario=0;
              $Importedell=0;
              $ObjetoImp='01';
            }
            if($facturadell->NoIdentificacion!=''){
              $tag_ni=' NoIdentificacion="'.$facturadell->NoIdentificacion.'" ';
            }else{
              $tag_ni='';
            }
            $xml .='<cfdi:Concepto '.$tag_ni.' ClaveUnidad="'.$facturadell->cunidad.'" ClaveProdServ="'.$facturadell->ClaveProdServ.'"';
             //$xml.='NoIdentificacion="'.str_pad((int) $partidaproducto,10,"0",STR_PAD_LEFT).'" ';
             $xml.=' Cantidad="'.$facturadell->Cantidad.'" '.$xml_unidad.' Descripcion="'.utf8_encode(str_replace($caract, $caract2, $facturadell->Descripcion)).'" ValorUnitario="'.$valorunitario.'" Importe="'.round($Importedell,2).'" '.$xmldescuento.' ObjetoImp="'.$ObjetoImp.'">';
                if($comparariva>0 and $TipoDeComprobante!='T'){
                  $importabase=$facturadell->Importe-$facturadell->descuento;
                  if($this->trunquearredondear==0){
                    $trasladadoimporte=round($importabase*$niva,4);
                  }else{
                    $trasladadoimporte=$importabase*$niva;
                    $trasladadoimporte=floor(($trasladadoimporte*100))/100;
                  }
                $xml .='<cfdi:Impuestos>';
                    $xml .='<cfdi:Traslados>';
                    $importabasetotal=$importabasetotal+$importabase; 
                        $xml .='<cfdi:Traslado Base="'.$importabase.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="'.$niva.'0000" Importe="'.$trasladadoimporte.'"></cfdi:Traslado>';
                    $xml .='</cfdi:Traslados>';
                    //-----------------------------------------------------------------------------------
                    if($isr!=0 ||$ivaretenido!=0||$outsourcing>0){
                        $xml .='<cfdi:Retenciones>';
                        if ($isr!=0) {
                            $xml .='<cfdi:Retencion Base="'.$facturadell->Importe.'" Impuesto="001" TipoFactor="Tasa" TasaOCuota="0.100000" Importe="'.round($facturadell->Importe*0.100000,2).'"></cfdi:Retencion>';
                        }
                        if ($ivaretenido!=0) {
                            $xml .='<cfdi:Retencion Base="'.$facturadell->Importe.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.106666" Importe="'.round($facturadell->Importe*0.106666,2).'"></cfdi:Retencion>'; 
                        }   
                        if ($outsourcing!=0) {
                            $xml .='<cfdi:Retencion Base="'.$facturadell->Importe.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.060000" Importe="'.round($facturadell->Importe*0.06,2).'"></cfdi:Retencion>'; 
                        }      
                        $xml .='</cfdi:Retenciones>';
                    }
                    //------------------------------------------------------------------------------------- 
                    //$xml .='<cfdi:Retenciones>';
                    //    $xml .='<cfdi:Retencion Base="1000" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="0.00" />';      
                    //$xml .='</cfdi:Retenciones>';
                $xml .='</cfdi:Impuestos>';
            }
            $xml .='</cfdi:Concepto>';
            $partidaproducto++;
        }
        $xml .='</cfdi:Conceptos>';
        if($comparariva>0 and $TipoDeComprobante!='T'){
            //$xml .='<cfdi:Impuestos TotalImpuestosRetenidos="0.00" TotalImpuestosTrasladados="'.$impuestosTrasladados.'">';
            $xml .='<cfdi:Impuestos '.$TotalImpuestosRetenidoss.' TotalImpuestosTrasladados="'.$impuestosTrasladados.'">';
                if($isr!=0 ||$ivaretenido!=0||$outsourcing>0){
                    $xml .='<cfdi:Retenciones>';
                    if ($isr!=0) {
                        $xml .='<cfdi:Retencion Impuesto="001" Importe="'.number_format(round($isr,2), 2, ".", "").'"></cfdi:Retencion>';
                    }
                    if ($ivaretenido!=0) {
                        $xml .='<cfdi:Retencion Impuesto="002" Importe="'.number_format(round($ivaretenido,2), 2, ".", "").'"></cfdi:Retencion>'; 
                    } 
                    if ($outsourcing>0) {
                        $xml .='<cfdi:Retencion Impuesto="002" Importe="'.number_format(round($outsourcing,2), 2, ".", "").'"></cfdi:Retencion>'; 
                    }      
                    $xml .='</cfdi:Retenciones>';
                }
               
                
                
                $xml .='<cfdi:Traslados>';
                    
                        $xml .='<cfdi:Traslado Base="'.$importabasetotal.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="'.$impuestosTrasladados.'"></cfdi:Traslado>';    
                $xml .='</cfdi:Traslados>';
            $xml .='</cfdi:Impuestos>';
        }
        /*
        if($cedular!=0){
            
        }
        */
        if($cedular!=0||$uuid||$cincoalmillarval>0||$cartaporte==1) {            
            $xml .= '<cfdi:Complemento>';
            if($cartaporte==1){    //true incluye carta porte false la descarta        
                        $xmlcp='<cartaporte20:CartaPorte ';
                                    if($datosFactura_cp->TotalDistRec!=''){ $xmlcp.=' TotalDistRec="'.$datosFactura_cp->TotalDistRec.'" ';}
                                    if($datosFactura_cp->EntradaSalidaMerc!=''){ $xmlcp.=' EntradaSalidaMerc="'.$datosFactura_cp->EntradaSalidaMerc.'" ';}
                                    $xmlcp.='TranspInternac="'.$datosFactura_cp->TranspInternac.'" ';
                                    $xmlcp.='Version="2.0">';
                        $xmlcp.='<cartaporte20:Ubicaciones>';
                            foreach ($datosFactura_u->result() as $dF_u_row) {
                                $xmlcp.='<cartaporte20:Ubicacion ';
                                        //if($dF_u_row->TipoEstacion!=''){$xmlcp.='TipoEstacion="'.$dF_u_row->TipoEstacion.'" ';}//
                                        //if($dF_u_row->NumEstacion!=''){ $xmlcp.='NumEstacion="'.$dF_u_row->NumEstacion.'" ';}
                                        //if($dF_u_row->NombreEstacion!=''){$xmlcp.='NombreEstacion="'.$dF_u_row->NombreEstacion.'" ';}
                                        $xmlcp.='FechaHoraSalidaLlegada="'.$dF_u_row->FechaHoraSalidaLlegada.':00" ';
                                        $xmlcp.='IDUbicacion="'.$dF_u_row->IDUbicacion.'" ';
                                        $xmlcp.='RFCRemitenteDestinatario="'.$dF_u_row->RFCRemitenteDestinatario.'" ';
                                        $xmlcp.='NombreRemitenteDestinatario="'.$dF_u_row->NombreRFC.'" ';
                                        $xmlcp.='TipoUbicacion="'.$dF_u_row->TipoUbicacion.'" ';
                                        if($dF_u_row->TipoUbicacion=='Origen'){
                                            $IDOrigen=$dF_u_row->IDUbicacion;
                                        }
                                        if($dF_u_row->TipoUbicacion=='Destino'){
                                            $IDDestino=$dF_u_row->IDUbicacion;
                                        }
                                        if($dF_u_row->DistanciaRecorrida>0){
                                            $xmlcp.='DistanciaRecorrida="'.$dF_u_row->DistanciaRecorrida.'" ';
                                        }
                                        if($dF_u_row->NumRegIdTrib!=''){
                                            //$xmlcp.='NumRegIdTrib="'.$dF_u_row->NumRegIdTrib.'" ';
                                        }
                                        if($dF_u_row->ResidenciaFiscal!=''){
                                            $xmlcp.='ResidenciaFiscal="'.$dF_u_row->ResidenciaFiscal.'" ';
                                        }
                                        $xmlcp.='>';

                                    $xmlcp.='<cartaporte20:Domicilio ';
                                                $xmlcp.='Calle="'.$dF_u_row->Calle.'" ';
                                                $xmlcp.='CodigoPostal="'.$dF_u_row->CodigoPostal.'" ';
                                                if($dF_u_row->Colonia!=''){$xmlcp.='Colonia="'.$dF_u_row->Colonia.'" ';}
                                                $xmlcp.='Estado="'.$dF_u_row->Estado.'" ';
                                                if($dF_u_row->Localidad!=''){$xmlcp.='Localidad="'.$dF_u_row->Localidad.'" ';}
                                                if($dF_u_row->Municipio!=''){$xmlcp.='Municipio="'.$dF_u_row->Municipio.'" ';}
                                                if($dF_u_row->NumeroExterior!=''){$xmlcp.='NumeroExterior="'.$dF_u_row->NumeroExterior.'" ';}
                                                if($dF_u_row->NumeroInterior!=''){$xmlcp.='NumeroInterior="'.$dF_u_row->NumeroInterior.'" ';}
                                                $xmlcp.='Pais="'.$dF_u_row->Pais.'" ';
                                    $xmlcp.='/>';
                                $xmlcp.='</cartaporte20:Ubicacion>';
                            }
                        $xmlcp.='</cartaporte20:Ubicaciones>';
                        $xmlcp.='<cartaporte20:Mercancias ';
                                $xmlcp.='PesoBrutoTotal="'.$datosFactura_fms->PesoBrutoTotal.'" ';
                                $xmlcp.='UnidadPeso="'.$datosFactura_fms->UnidadPeso.'" ';
                                $xmlcp.='NumTotalMercancias="'.$datosFactura_fms->NumTotalMercancias.'" ';
                                if($datosFactura_fms->CargoPorTasacion>0){$xmlcp.='CargoPorTasacion="'.$datosFactura_fms->CargoPorTasacion.'" ';}
                        $xmlcp.='>';
                            foreach ($datosFactura_fm->result() as $dF_fm_row) {
                                $xmlcp.='<cartaporte20:Mercancia ';
                                    if($dF_fm_row->BienesTransp!=''){$xmlcp.='BienesTransp="'.$dF_fm_row->BienesTransp.'" ';}
                                    $xmlcp.='Descripcion="'.strval(str_replace(array('í'), array('i'), $dF_fm_row->Descripcion)).'" ';
                                    $xmlcp.='Cantidad="'.$dF_fm_row->Cantidad.'" ';
                                    $xmlcp.='ClaveUnidad="'.$dF_fm_row->ClaveUnidad.'" ';
                                    if($dF_fm_row->Dimensiones!=''){$xmlcp.='Dimensiones="'.$dF_fm_row->Dimensiones.'" ';}
                                    if($dF_fm_row->Embalaje!=''){$xmlcp.='Embalaje="'.$dF_fm_row->Embalaje.'" ';}
                                    if($dF_fm_row->DescripEmbalaje!=''){$xmlcp.='DescripEmbalaje="'.$dF_fm_row->DescripEmbalaje.'" ';}
                                    if($dF_fm_row->ValorMercancia>0){$xmlcp.='ValorMercancia="'.$dF_fm_row->ValorMercancia.'" ';}
                                    if($dF_fm_row->Unidad!=''){$xmlcp.='Unidad="'.$dF_fm_row->Unidad.'" ';}
                                    $xmlcp.='PesoEnKg="'.$dF_fm_row->PesoEnKg.'" ';
                                    $xmlcp.='Moneda="'.$dF_fm_row->Moneda.'" ';
                                    $xmlcp.='MaterialPeligroso="No" ';
                                $xmlcp.='>';
                                    $xmlcp.='<cartaporte20:CantidadTransporta Cantidad="'.$dF_fm_row->Cantidad.'" IDOrigen="'.$IDOrigen.'" IDDestino="'.$IDDestino.'" />';
                                $xmlcp.='</cartaporte20:Mercancia>';
                            }
                            foreach ($datosFactura_tf->result() as $dF_tf_row) {
                                $xmlcp.='<cartaporte20:Autotransporte ';
                                        $xmlcp.='PermSCT="'.$dF_tf_row->PermSCT.'" ';
                                        $xmlcp.='NumPermisoSCT="'.$dF_tf_row->NumPermisoSCT.'">';
                                    $xmlcp.='<cartaporte20:IdentificacionVehicular ';
                                        $xmlcp.='ConfigVehicular="'.$dF_tf_row->ConfigVehicular.'" ';
                                        $xmlcp.='PlacaVM="'.$dF_tf_row->PlacaVM.'" ';
                                        $xmlcp.='AnioModeloVM="'.$dF_tf_row->AnioModeloVM.'" />';
                                    $xmlcp.='<cartaporte20:Seguros ';
                                            $xmlcp.='AseguraCarga="'.$dF_tf_row->NombreAseg.'" ';
                                            $xmlcp.='AseguraRespCivil="'.$dF_tf_row->NombreAseg.'" ';
                                            $xmlcp.='PolizaRespCivil="'.$dF_tf_row->NumPolizaSeguro.'" />';
                                    /*<cartaporte20:Remolques>
                                        <cartaporte20:Remolque SubTipoRem="CTR004" Placa="VL45K98" />
                                    </cartaporte20:Remolques>*/
                                
                                $xmlcp.='<cartaporte20:Remolques><cartaporte20:Remolque Placa="'.$dF_tf_row->Placa.'" SubTipoRem="'.$dF_tf_row->SubTipoRem.'"/></cartaporte20:Remolques>';
                                $xmlcp.='</cartaporte20:Autotransporte>';
                            }
                            foreach($datosFactura_ta->result() as $dF_ta_row){
                                $xmlcp.='<cartaporte20:TransporteAereo ';
                                    $xmlcp.='CodigoTransportista="'.$dF_ta_row->CodigoTransportista.'" ';
                                    if($dF_ta_row->LugarContrato!=''){$xmlcp.='LugarContrato="'.$dF_ta_row->LugarContrato.'" ';}
                                    $xmlcp.='MatriculaAeronave="'.$dF_ta_row->MatriculaAeronave.'" ';
                                    $xmlcp.='NombreAseg="'.$dF_ta_row->NombreAseg.'" ';
                                    
                                    $xmlcp.='NumPermisoSCT="'.$dF_ta_row->NumPermisoSCT.'" ';
                                    $xmlcp.='NumPolizaSeguro="'.$dF_ta_row->NumPolizaSeguro.'" ';
                                    $xmlcp.='NumeroGuia="'.$dF_ta_row->NumeroGuia.'" ';
                                    $xmlcp.='PermSCT="'.$dF_ta_row->PermSCT.'" ';

                                    if($dF_ta_row->NumRegIdTribTranspor!=''){$xmlcp.='NumRegIdTribTranspor="'.$dF_ta_row->NumRegIdTribTranspor.'" ';}
                                    //if($dF_ta_row['ResidenciaFiscalTranspor']!=''){$xmlcp.='ResidenciaFiscalTranspor="'.$dF_ta_row['ResidenciaFiscalTranspor'].'" ';}
                                    //if($dF_ta_row['NombreTransportista']!=''){$xmlcp.='NombreTransportista="'.$dF_ta_row['NombreTransportista'].'" ';}
                                    if($dF_ta_row->NumRegIdTribEmbarc!=''){$xmlcp.='NumRegIdTribEmbarc="'.$dF_ta_row->NumRegIdTribEmbarc.'" ';}
                                    if($dF_ta_row->RFCEmbarcador!=''){$xmlcp.='RFCEmbarcador="'.$dF_ta_row->RFCEmbarcador.'" ';}
                                    if($dF_ta_row->ResidenciaFiscalEmbarc!=''){$xmlcp.='ResidenciaFiscalEmbarc="'.$dF_ta_row->ResidenciaFiscalEmbarc.'" ';}
                                    if($dF_ta_row->NombreEmbarcador!=''){$xmlcp.='NombreEmbarcador="'.$dF_ta_row->NombreEmbarcador.'" ';}
                                $xmlcp.='/>';
                            }
                        $xmlcp.='</cartaporte20:Mercancias>';
                        $FiguraTransporte_row=0;
                        foreach($datosFactura_fa as $dF_fa){
                            $FiguraTransporte_row++;
                        }
                        foreach($datosFactura_fo as $dF_fo){
                            $FiguraTransporte_row++;
                        }
                        foreach($datosFactura_fp as $dF_fp){
                            $FiguraTransporte_row++;
                        }
                        if($FiguraTransporte_row>0){
                            $xmlcp.='<cartaporte20:FiguraTransporte>';
                            foreach($datosFactura_fo->result() as $dF_fo){
                                $xmlcp.='<cartaporte20:TiposFigura ';
                                    $xmlcp.='NombreFigura="'.$dF_fo->NombreOperador.'" ';
                                    if($dF_fo->NumLicencia!=''){$xmlcp.='NumLicencia="'.$dF_fo->NumLicencia.'" ';}
                                    $xmlcp.='RFCFigura="'.preg_replace("[\n|\r|\n\r]", "",strval(str_replace(' ', '', $dF_fo->RFCOperador))).'" ';
                                    if($dF_fo->NumRegIdTribOperador!=''){$xmlcp.='NumRegIdTribFigura="'.$dF_fo->NumRegIdTribOperador.'" ';}
                                    if($dF_fo->ResidenciaFiscalOperador!=''){$xmlcp.='ResidenciaFiscalFigura="'.$dF_fo->ResidenciaFiscalOperador.'" ';}
                                    $xmlcp.='TipoFigura="01"/>';
                                    if($dF_fo->adddomicilio==1){
                                        $xmlcp.='<cartaporte20:Domicilio ';
                                            $xmlcp.='Calle="'.$dF_fo->Calle.'" ';
                                            $xmlcp.='CodigoPostal="'.$dF_fo->CodigoPostal.'" ';
                                            $xmlcp.='Colonia="'.$dF_fo->Colonia.'" ';
                                            $xmlcp.='Estado="'.$dF_fo->Estado.'" ';
                                            $xmlcp.='Localidad="'.$dF_fo->Localidad.'" ';
                                            $xmlcp.='Municipio="'.$dF_fo->Municipio.'" ';
                                            $xmlcp.='NumeroExterior="'.$dF_fo->NumeroExterior.'" ';
                                            $xmlcp.='NumeroInterior="'.$dF_fo->NumeroInterior.'" ';
                                            $xmlcp.='Pais="'.$dF_fo->Pais.'" ';
                                            $xmlcp.='/>';
                                    }
                            }
                            foreach($datosFactura_fa->result() as $dF_fa){
                                $xmlcp.='<cartaporte20:TiposFigura ';
                                    $xmlcp.='NombreFigura="'.$dF_fa->NombreArrendatario.'" ';
                                    $xmlcp.='RFCFigura="'.strval(str_replace(' ', '', $dF_fa->RFCArrendatario)).'" ';
                                    if($dF_fa->NumRegIdTribArrendatario!=''){$xmlcp.='NumRegIdTribFigura="'.$dF_fa->NumRegIdTribArrendatario.'" ';}
                                    if($dF_fa->ResidenciaFiscalOperador!='' and $dF_fa->ResidenciaFiscalOperador!='null'){$xmlcp.='ResidenciaFiscalFigura="'.$dF_fa->ResidenciaFiscalOperador.'" ';}
                                    $xmlcp.='TipoFigura="03"/>';
                                if($dF_fa->adddomicilio==1){
                                        $xmlcp.='<cartaporte20:Domicilio ';
                                            $xmlcp.='Calle="'.$dF_fa->Calle.'" ';
                                            $xmlcp.='CodigoPostal="'.$dF_fa->CodigoPostal.'" ';
                                            $xmlcp.='Colonia="'.$dF_fa->Colonia.'" ';
                                            $xmlcp.='Estado="'.$dF_fa->Estado.'" ';
                                            $xmlcp.='Localidad="'.$dF_fa->Localidad.'" ';
                                            $xmlcp.='Municipio="'.$dF_fa->Municipio.'" ';
                                            $xmlcp.='NumeroExterior="'.$dF_fa->NumeroExterior.'" ';
                                            if($dF_fa->NumeroInterior!=''){$xmlcp.='NumeroInterior="'.$dF_fa->NumeroInterior.'" ';}
                                            $xmlcp.='Pais="'.$dF_fa->Pais.'" ';
                                            $xmlcp.='/>';
                                    }
                            }
                            
                            foreach($datosFactura_fp->result() as $dF_fp){
                                $xmlcp.='<cartaporte20:TiposFigura ';
                                    $xmlcp.='NombreFigura="'.$dF_fp->NombrePropietario.'" ';
                                    $xmlcp.='RFCFigura="'.strval(str_replace(' ', '', $dF_fp->RFCPropietario)).'" ';
                                    if($dF_fp->NumRegIdTribPropietario!=''){$xmlcp.='NumRegIdTribFigura="'.$dF_fp->NumRegIdTribPropietario.'" ';}
                                    if($dF_fp->ResidenciaFiscalPropietario!=''){$xmlcp.='ResidenciaFiscalFigura="'.$dF_fp->ResidenciaFiscalPropietario.'" ';}
                                    $xmlcp.='TipoFigura="02">';
                                if($datosFactura_ft->CveTransporte=='03'){
                                    $xmlcp.='<cartaporte20:PartesTransporte ParteTransporte="PT08"/>';
                                }
                                    if($dF_fp->adddomicilio==1){
                                        $xmlcp.='<cartaporte20:Domicilio ';
                                            $xmlcp.='Calle="'.$dF_fp->Calle.'" ';
                                            $xmlcp.='CodigoPostal="'.$dF_fp->CodigoPostal.'" ';
                                            $xmlcp.='Colonia="'.$dF_fp->Colonia.'" ';
                                            $xmlcp.='Estado="'.$dF_fp->Estado.'" ';
                                            $xmlcp.='Localidad="'.$dF_fp->Localidad.'" ';
                                            $xmlcp.='Municipio="'.$dF_fp->Municipio.'" ';
                                            $xmlcp.='NumeroExterior="'.$dF_fp->NumeroExterior.'" ';
                                            if($dF_fp->NumeroInterior!=''){$xmlcp.='NumeroInterior="'.$dF_fp->NumeroInterior.'" ';}
                                            $xmlcp.='Pais="'.$dF_fp->Pais.'" ';
                                            $xmlcp.='/>';
                                    }
                                $xmlcp.='</cartaporte20:TiposFigura>';
                                
                            }
                                
                            $xmlcp.='</cartaporte20:FiguraTransporte>';
                        }
                        $xmlcp.='</cartaporte20:CartaPorte>';
                        $xml.=$xmlcp;
                    }
            if ($cedular!=0) {
                //$xml .= '<cfdi:Complemento xmlns:cfdi="http://www.sat.gob.mx/cfd/3" >';
                $xml .= '<implocal:ImpuestosLocales version="1.0" TotaldeTraslados="0.00" TotaldeRetenciones="'.$cedular.'" >';
                $xml .= '<implocal:RetencionesLocales  TasadeRetencion="01.00" ImpLocRetenido="CEDULAR" Importe="'.$cedular.'"/>';
                $xml .= '</implocal:ImpuestosLocales>';
            }
            if ($cincoalmillarval>0) {
                $xml .= '<implocal:ImpuestosLocales version="1.0" TotaldeTraslados="0.00" TotaldeRetenciones="'.$cincoalmillarval.'" >';
                  $xml .= '<implocal:RetencionesLocales Importe="'.$cincoalmillarval.'" TasadeRetencion="0.50" ImpLocRetenido=".005 Insp y Vig" />';
                $xml .= '</implocal:ImpuestosLocales>';
            }
            if ($uuid) {
                $xml .='<tfd:TimbreFiscalDigital Version="1.1" RfcProvCertif="FLI081010EK2" ';
                $xml .='UUID="'.$uuid.'" FechaTimbrado="'.$fecha.'" ';
                $xml .='SelloCFD="'.$sello.'" NoCertificadoSAT="'.$certificadoSAT.'" SelloSAT="'.$selloSAT.'" ';
                $xml .='xmlns:tfd="http://www.sat.gob.mx/TimbreFiscalDigital" ';
                $xml .= 'xsi:schemaLocation="http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd" />';
            }
            $xml .= '</cfdi:Complemento>';
        }
        $xml .= '</cfdi:Comprobante>';
        return $xml;
    }

    /*function cancelarCfdi(){
      $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/etla';//rutalocal
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      $ruta=$rutainterna.'/etla_sat/temporalsat/';
      $rutaf=$rutainterna.'/etla_sat/facturas/';
      $rutalog=$rutainterna.'/etla_sat/facturaslog/';

      $productivo=1;//0 demo 1 producccion
      $datas = $this->input->post();
      $facturas=$datas['facturas'];
      unset($datas['facturas']);

      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      $rFCEmisor=$datosconfiguracion->Rfc;
      $passwordClavePrivada=$datosconfiguracion->paswordkey;

      $datosCancelacion=array();
      $DATAf = json_decode($facturas);
      for ($i=0;$i<count($DATAf);$i++) {
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$DATAf[$i]->FacturasIds));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];
        $FacturasId=$datosfactura->FacturasId;
        $datosCancelacion[] = array(
                                  'Propiedad'=>'cancelacion',
                                  'RFCReceptor'=>$datosfactura->Rfc,
                                  'Total'=>round($datosfactura->total, 2),
                                  'UUID'=>$datosfactura->uuid

                                );
      }
      //===========================================================
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
      //=======================================================
        
        $fp = fopen($ruta.'pfx.pem', "r");
        $clavePrivada = fread($fp, filesize($ruta.'pfx.pem'));
        fclose($fp);
        
        
        $rutaarchivos=$rutainterna.'/hulesyg/elementos/';
        $rutapass=$rutaarchivos.'passs.txt';
        $passwordClavePrivada = file_get_contents($rutapass);
        
        //$passwordClavePrivada='Daniel182 ';// solo para las pruebas
        
        $parametros = array(
            'usuario' => $usuario,
            'password' => $password,
            'rFCEmisor' => $rFCEmisor,
            'listaCFDI' => $datosCancelacion,
            'clavePrivada_Base64'=>$clavePrivada,
            'passwordClavePrivada'=> $passwordClavePrivada  );
        file_put_contents($rutalog.'log_'.$this->fechahoyL.'xxxxx_.txt', json_encode($parametros));
        $clienteSOAP = new SoapClient($URL_WS);

        $result = $clienteSOAP->CancelarCFDIConValidacion($parametros);

        if($result->CancelarCFDIConValidacionResult->OperacionExitosa) {
          $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Cancelada',
                          'info'=>$result
                          );
          //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
                $fileacuse='acuseCancelacion_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
                $ruta = $rutaf .$fileacuse;
                //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
                file_put_contents($ruta,$result->CancelarCFDIConValidacionResult->XMLAcuse);

                $datosdecancelacion = array(
                                            'Estado'=>0,
                                            'rutaAcuseCancelacion '=>$fileacuse,
                                            'fechacancelacion'=>$this->fechahoyc
                                          );
                $this->ModeloCatalogos->updateCatalogo('f_facturas',$datosdecancelacion,array('FacturasId'=>$FacturasId));
          
        }else{
          $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->CancelarCFDIConValidacionResult->MensajeError,
                          'MensajeError'=>$result->CancelarCFDIConValidacionResult->MensajeErrorDetallado,
                          'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                          'info'=>$result
                          );
          file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
        }
        
        echo json_encode($resultado);
    }*/

    function cancelarCfdi(){
      $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/dplogisticsv2';//rutalocal
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      $ruta=$rutainterna.'/files_sat/temporalsat/';
      $rutaf=$rutainterna.'/files_sat/facturas/';
      $rutalog=$rutainterna.'/files_sat/facturaslog/';

      $productivo=0;//0 demo 1 producccion
      $datas = $this->input->post();
      $facturas=$datas['facturas'];
      $motivo=$datas['motivo'];
      $uuidrelacionado=$datas['uuidrelacionado'];
      $motselect=$datas['motselect'];

      unset($datas['facturas']);
      unset($datas['motivo']);
      unset($datas['uuidrelacionado']);
      unset($datas['motselect']);

      

      $datosCancelacion=array();
      $DATAf = json_decode($facturas);
      for ($i=0;$i<count($DATAf);$i++) {
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$DATAf[$i]->FacturasIds));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];
        $FacturasId=$datosfactura->FacturasId;
        //===================================
          $tipoempresa=$datosfactura->tipoempresa;
          if($tipoempresa>0){

          }else{
            $tipoempresa=1;
          }
          if($tipoempresa==1){
            $tipoempresaurl='';
          }else{
            $tipoempresaurl='2';
            $ruta=$rutainterna.'/files_sat/temporalsat2/';
          }
          $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$tipoempresa));
          $datosconfiguracion=$datosconfiguracion->result();
          $datosconfiguracion=$datosconfiguracion[0];
          $rFCEmisor=$datosconfiguracion->Rfc;
          $passwordClavePrivada=$datosconfiguracion->paswordkey;
        //===================================
          $totalf=$datosfactura->total;
        $datosCancelacion[] = array(
                                  'RFCReceptor'=>$datosfactura->Rfc,
                                  'Total'=>round($totalf, 2),
                                  'UUID'=>$datosfactura->uuid,
                                  'Motivo'=>$motivo,
                                  'FolioSustitucion'=>$uuidrelacionado

                                );
      }
      //===========================================================
        if($productivo==0){
          $URL_WS='https://app.appfacturainteligente.com/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.appfacturainteligente.com/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "COE960119D33";
        }elseif ($productivo==1) {
          //Produccion
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
          if($tipoempresa==1){
            // parece que si tiene permisos
            $usuario= "COE960119HI6";
            $password= "C@9eCai5";
          }
          if($tipoempresa==2){
            // no tiene permisos
            //$usuario= "CAD070509Q18";
            //$password= "Cad18@3F";
            $usuario= "CAD070509Q18";
            $password= 'Vh8$yvolup';
          }
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "COE960119D33";
        }
      //=======================================================
        
        $fp = fopen($ruta.'pfx.pem', "r");
        $clavePrivada = fread($fp, filesize($ruta.'pfx.pem'));
        fclose($fp);
        
        
        $rutaarchivos=$rutainterna.'/files_sat/elementos'.$tipoempresaurl.'/';
        $rutapass=$rutaarchivos.'passs.txt';
        $passwordClavePrivada = file_get_contents($rutapass);
        
        //$passwordClavePrivada='Daniel182 ';// solo para las pruebas
        
        $parametros = array(
            'usuario' => $usuario,
            'password' => $password,
            'rFCEmisor' => $rFCEmisor,
            'listaCFDI' => $datosCancelacion,
            'clavePrivada_Base64'=>$clavePrivada,
            'passwordClavePrivada'=> $passwordClavePrivada  );
        file_put_contents($rutalog.'log_'.$this->fechahoyL.'_paramt_cancelacion_.txt', json_encode($parametros));
        $clienteSOAP = new SoapClient($URL_WS);

        $result = $clienteSOAP->CancelarCFDI($parametros);

        $fileacuse1='log_cancelcion2_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
        $ruta1 = $rutalog .$fileacuse1;

        file_put_contents($ruta1,json_encode($result));

        if($result->CancelarCFDIResult->OperacionExitosa) {
          $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Cancelada',
                          'info'=>$result
                          );
          //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
                $fileacuse='acuseCancelacion_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
                $ruta = $rutaf .$fileacuse;
                //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
                file_put_contents($ruta,$result->CancelarCFDIResult->XMLAcuse);

                $datosdecancelacion = array(
                                            'Estado'=>0,
                                            'rutaAcuseCancelacion '=>$fileacuse,
                                            'fechacancelacion'=>$this->fechahoyc,
                                            'tipo_cancelacion'=>$motselect
                                          );
                $this->ModeloCatalogos->updateCatalogo('f_facturas',$datosdecancelacion,array('FacturasId'=>$FacturasId));
          
        }else{
          $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->CancelarCFDIResult->MensajeError,
                          'MensajeError'=>$result->CancelarCFDIResult->MensajeErrorDetallado,
                          'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                          'info'=>$result
                          );
          file_put_contents($rutalog.'log_cancelacion_error_'.$this->fechahoyL.'_.txt', json_encode($result));
        }
        
        echo json_encode($resultado);
    }
    function cancelarCfdicomplemento(){
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'].'/dplogisticsv2';//rutalocal
      $rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      $ruta=$rutainterna.'/files_sat/temporalsat/';
      $rutaf=$rutainterna.'/files_sat/facturas/';
      $rutalog=$rutainterna.'/files_sat/facturaslog/';

      $productivo=0;//0 demo 1 producccion
      $datas = $this->input->post();
      $facturas=$datas['facturas'];
      $motivo=$datas['motivo'];
      $uuidrelacionado=$datas['uuidrelacionado'];
      $motselect=$datas['motselect'];

      unset($datas['facturas']);
      unset($datas['motivo']);
      unset($datas['uuidrelacionado']);
      unset($datas['motselect']);

      

      $datosCancelacion=array();
      $DATAf = json_decode($facturas);
      for ($i=0;$i<count($DATAf);$i++) {
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$DATAf[$i]->FacturasIds));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];
        $FacturasId=$datosfactura->complementoId;
        //===================================
          $tipoempresa=$datosfactura->ConfiguracionesId;
          if($tipoempresa>0){

          }else{
            $tipoempresa=1;
          }
          if($tipoempresa==1){
            $tipoempresaurl='';
          }else{
            $tipoempresaurl='2';
            $ruta=$rutainterna.'/files_sat/temporalsat2/';
          }
          $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$tipoempresa));
          $datosconfiguracion=$datosconfiguracion->result();
          $datosconfiguracion=$datosconfiguracion[0];
          $rFCEmisor=$datosconfiguracion->Rfc;
          $passwordClavePrivada=$datosconfiguracion->paswordkey;
        //===================================
        $datosCancelacion[] = array(
                                  'RFCReceptor'=>$datosfactura->R_rfc,
                                  'Total'=>0,
                                  'UUID'=>$datosfactura->uuid,
                                  'Motivo'=>$motivo,
                                  'FolioSustitucion'=>$uuidrelacionado

                                );
      }
      //===========================================================
        if($productivo==0){
          $URL_WS='https://app.appfacturainteligente.com/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.appfacturainteligente.com/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "COE960119D33";
        }elseif ($productivo==1) {
          //Produccion
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
          if($tipoempresa==1){
            // parece que si tiene permisos
            $usuario= "COE960119HI6";
            $password= "C@9eCai5";
          }
          if($tipoempresa==2){
            // no tiene permisos
            //$usuario= "CAD070509Q18";
            //$password= "Cad18@3F";
            $usuario= "CAD070509Q18";
            $password= 'Vh8$yvolup';
          }
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "COE960119D33";
        }
      //=======================================================
        
        $fp = fopen($ruta.'pfx.pem', "r");
        $clavePrivada = fread($fp, filesize($ruta.'pfx.pem'));
        fclose($fp);
        
        
        $rutaarchivos=$rutainterna.'/files_sat/elementos'.$tipoempresaurl.'/';
        $rutapass=$rutaarchivos.'passs.txt';
        $passwordClavePrivada = file_get_contents($rutapass);
        
        //$passwordClavePrivada='Daniel182 ';// solo para las pruebas
        
        $parametros = array(
            'usuario' => $usuario,
            'password' => $password,
            'rFCEmisor' => $rFCEmisor,
            'listaCFDI' => $datosCancelacion,
            'clavePrivada_Base64'=>$clavePrivada,
            'passwordClavePrivada'=> $passwordClavePrivada  );
        file_put_contents($rutalog.'log_'.$this->fechahoyL.'xxxxx_.txt', json_encode($parametros));
        $clienteSOAP = new SoapClient($URL_WS);

        $result = $clienteSOAP->CancelarCFDI($parametros);

        $fileacuse1='log_cancelcion2_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
        $ruta1 = $rutalog .$fileacuse1;

        file_put_contents($ruta1,json_encode($result));

        if($result->CancelarCFDIResult->OperacionExitosa) {
          $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Cancelada',
                          'info'=>$result
                          );
          //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
                $fileacuse='acuseCancelacion_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
                $ruta = $rutaf .$fileacuse;
                //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
                file_put_contents($ruta,$result->CancelarCFDIResult->XMLAcuse);

                $datosdecancelacion = array(
                                            'Estado'=>0,
                                            'rutaAcuseCancelacion '=>$fileacuse,
                                            'fechacancelacion'=>$this->fechahoyc
                                          );
                $this->ModeloCatalogos->updateCatalogo('f_complementopago',$datosdecancelacion,array('complementoId'=>$FacturasId));
          
        }else{
          $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->CancelarCFDIResult->MensajeError,
                          'MensajeError'=>$result->CancelarCFDIResult->MensajeErrorDetallado,
                          'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                          'info'=>$result
                          );
          file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
        }
        
        echo json_encode($resultado);
    }
    function estatuscancelacionCfdi_all(){
        $datos = $this->input->post('datos');
        //log_message('error', $datos);
        $DATA = json_decode($datos);
        $facturas=array();
        for ($i=0;$i<count($DATA);$i++) {
            $complemento=0;
            $uuid=$DATA[$i]->uuid;
            $resultado=$this->estatuscancelacionCfdi($uuid);

            $facturas[]=array(
                    'uuid'=>$uuid,
                    'resultado'=>$resultado
                );
        }
        echo json_encode($facturas);
    }
    function estatuscancelacionCfdi($uuid){
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'].'/kyocera2';//rutalocal
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      //$ruta=$rutainterna.'/kyocera/temporalsat/';
      //$rutaf=$rutainterna.'/kyocera/facturas/';
      //$rutalog=$rutainterna.'/kyocera/facturaslog/';

      $productivo=1;//0 demo 1 producccion
      
        $res_uuid=$uuid;
        $res_facturaid=0;
      
      

      //var_dump($datosfactura->uuid);
      //===========================================================
        if($productivo==0){
          $URL_WS='https://app.appfacturainteligente.com/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.appfacturainteligente.com/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "COE960119D33";
        }elseif ($productivo==1) {
          //Produccion
          $usuario= "COE960119HI6";
          $password= "C@9eCai5";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "COE960119D33";
        }
      //=======================================================

        $parametros = array(
            'usuario' => $usuario,
            'password' => $password,
            'uUID' => $res_uuid);
        //echo json_encode($parametros);
        $clienteSOAP = new SoapClient($URL_WS);

        $result = $clienteSOAP->ObtenerAcuseCancelacion($parametros);

        
        $resultado=$result;
        
        
          $MensajeError='';
          if($resultado->ObtenerAcuseCancelacionResult->CodigoRespuesta=='825'){
            $MensajeError=$resultado->ObtenerAcuseCancelacionResult->MensajeError;
          }else{

          }
        
          return $MensajeError;
    }
    function addcomplemento(){
      $data = $this->input->post();
        $arraydoc=$data['arraydocumento'];
        unset($data['arraydocumento']);
        //$idfactura=$data['idfactura'];
        //==================================================================
          //$datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
          //$datosfactura=$datosfactura->result();
          //$datosfactura=$datosfactura[0];
        //==================================================================
          $idsucursal=$data['ConfiguracionesId'];
          if($idsucursal==1){
            $serie='CCO';
          }else{
            $serie='CCA';
          }
          $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$idsucursal));
          $datosconfiguracion=$datosconfiguracion->result();
          $datosconfiguracion=$datosconfiguracion[0];
        //==================================================================

        //$Sello=$datosfactura->sellosat;
        //$Sello=$datosfactura->sellocadena;
        $ultimoFolioval=$this->ModeloCatalogos->ultimoFoliocp($serie) + 1;
        $datacp['Folio']=$ultimoFolioval;
        $datacp['Fecha']=$data['Fecha'];
        $datacp['Sello']='';
        //$datacp['NoCertificado']=$datosfactura->nocertificado;
        //$datacp['Certificado']=$datosfactura->certificado;
        $datacp['Certificado']='';
        $datacp['ConfiguracionesId']=$data['ConfiguracionesId'];//para saber a con que usuario consumira los folios
        $datacp['LugarExpedicion']=$data['LugarExpedicion'];
        $datacp['E_rfc']=$datosconfiguracion->Rfc;
        $datacp['E_nombre']=$datosconfiguracion->Nombre;
        $datacp['E_regimenfiscal']=$datosconfiguracion->Regimen;
        $datacp['R_rfc']=$data['rfcreceptor'];
        $datacp['R_nombre']=$data['razonsocialreceptor'];
        $datacp['R_regimenfiscal']='';
        $datacp['FechaPago']=$data['Fechatimbre'];
        $datacp['FormaDePagoP']=$data['FormaDePagoP'];
        $datacp['MonedaP']=$data['ModedaP'];
        $datacp['Monto']=$data['Monto'];
        $datacp['NumOperacion']=str_replace(' ', '', $data['NumOperacion']);
        $datacp['CtaBeneficiario']=$data['CtaBeneficiario'];
        
        $datacp['Serie']=$serie;//xxx
        $datacp['Foliod']=$data['Folio'];
        $datacp['MonedaDR']=$data['ModedaP'];

        $datacp['f_relacion']    = $data['f_r'];
        $datacp['f_r_tipo']      = $data['f_r_t'];
        $datacp['f_r_uuid']      = $data['f_r_uuid'];
        
    
        $datacp['UsoCFDI']='';
        $idcomplemento=$this->ModeloCatalogos->Insert('f_complementopago',$datacp);
        $DATAdoc = json_decode($arraydoc);
        for ($j=0;$j<count($DATAdoc);$j++){
          $datacdoc['complementoId']=$idcomplemento;
          $datacdoc['facturasId']=$DATAdoc[$j]->idfactura;
          $datacdoc['IdDocumento']=$DATAdoc[$j]->IdDocumento;
          $datacdoc['serie']=$DATAdoc[$j]->serie;
          $datacdoc['folio']=$DATAdoc[$j]->folio;
          $datacdoc['NumParcialidad']=$DATAdoc[$j]->NumParcialidad;
          $datacdoc['ImpSaldoAnt']=$DATAdoc[$j]->ImpSaldoAnt;
          $datacdoc['ImpPagado']=$DATAdoc[$j]->ImpPagado;
          $datacdoc['ImpSaldoInsoluto']=$DATAdoc[$j]->ImpSaldoInsoluto;
          $datacdoc['MetodoDePagoDR']=$DATAdoc[$j]->MetodoDePagoDR;
          $this->ModeloCatalogos->Insert('f_complementopago_documento',$datacdoc);
        }

        $respuesta=$this->procesarcomplemento($idcomplemento);

        echo json_encode($respuesta);
    }
    function retimbrarcomplemento(){
      $complemento = $this->input->post('complemento');
      $respuesta=$this->procesarcomplemento($complemento);
        echo json_encode($respuesta);
    }
    function procesarcomplemento($idcomplemento){
      //==================================================================
          $datoscomplemento=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$idcomplemento));
          $datoscomplemento=$datoscomplemento->result();
          $datoscomplemento=$datoscomplemento[0];
          $tipoempresa=$datoscomplemento->ConfiguracionesId;
          $folio=$datoscomplemento->Folio;
          $serie=$datoscomplemento->Serie;
          if($tipoempresa==1){
            $tipoempresaurl='';
          }else{
            $tipoempresaurl='2';
          }
      //==================================================================
      //========================================================================
      $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/dplogisticsv2';//rutalocal
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      $ruta=$rutainterna.'/files_sat/temporalsat'.$tipoempresaurl.'/';
      $rutaf=$rutainterna.'/files_sat/facturas/';
      $rutalog=$rutainterna.'/files_sat/facturaslog/';
        $datosFactura = array(
          'carpeta'=>'files_sat',
          'pwskey'=>'',
          'archivo_key'=>'',
          'archivo_cer'=>'',
          'factura_id'=>0
        );
        $numeroCertificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat'.$tipoempresaurl.'/Serial.txt');
          //fclose($archivo);
          $numeroCertificado=  str_replace("serial=", "", $numeroCertificado);//quitar letras
        
          $temporal=  str_split($numeroCertificado);//Pasar todos los digitos a un array
        
          $numeroCertificado="";
          $i=0;  
          foreach ($temporal as $value) {
              
              if(($i%2))
              
                  $numeroCertificado .= $value;
          
              $i++;
          }
        
        $numeroCertificado = str_replace('\n','',$numeroCertificado);
        
        $numeroCertificado = trim($numeroCertificado);

        $certificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat'.$tipoempresaurl.'/certificate.pem');
        
        //fclose($archivo);
        $certificado=  str_replace("-----BEGIN CERTIFICATE-----", "", $certificado);
        
        $certificado=  str_replace("-----END CERTIFICATE-----", "", $certificado);
        
        $certificado=  str_replace("\n", "", $certificado);
        $certificado=  str_replace(" ", "", $certificado);
        $certificado=  str_replace("\n", "", $certificado);
        $certificado= preg_replace("[\n|\r|\n\r]", '', $certificado);
        $certificado= trim($certificado);
      //========================================================================================
        $xml=$this->generaxmlcomplemento('',$certificado,$numeroCertificado,$idcomplemento);
        log_message('error', 'se genera xml2: '.$xml);
        $str = trim($this->generaCadenaOriginalReal($xml,$datosFactura['carpeta']));    
        
        $cadena=$str;
      //===========================================================================================
        $fp = fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat'.$tipoempresaurl.'/llaveprivada.pem', "r"); 
        
        $priv_key = fread($fp, 8192); 
        
        fclose($fp); 
        
        $pkeyid = openssl_get_privatekey($priv_key);
        
        //openssl_sign($cadena, $sig, $pkeyid);
        openssl_sign($cadena, $sig, $pkeyid,'sha256');
        
        openssl_free_key($pkeyid);
        
        $sello = base64_encode($sig);
      //===============accessos al webservice===================================================

        $productivo=0;//0 demo 1 productivo
        if($productivo==0){
          $URL_WS='https://app.appfacturainteligente.com/WSTimbrado33Test/WSCFDI33.svc?WSDL';
          //$URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.appfacturainteligente.com/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "COE960119D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
          if($tipoempresa==1){
            // parece que si tiene permisos
            $usuario= "COE960119HI6";
            $password= "C@9eCai5";
          }
          if($tipoempresa==2){
            // no tiene permisos
            //$usuario= "CAD070509Q18";
            //$password= "Cad18@3F";
            $usuario= "CAD070509Q18";
            $password= 'Vh8$yvolup';
          }
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "COE960119D33";
        }
        //$idsucursal=$this->idsucursal;
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$tipoempresa));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        $rFCEmisor=$datosconfiguracion->Rfc;
        $passwordClavePrivada=$datosconfiguracion->paswordkey;

        $clienteSOAP = new SoapClient($URL_WS);

      
          
          $xmlcomplemento=$this->generaxmlcomplemento($sello,$certificado,$numeroCertificado,$idcomplemento);
          //log_message('error', 'se genera xml3: '.$xmlcomplemento);
            file_put_contents('files_sat/facturas/preview_complemento_'.$serie.'_'.$folio.'.xml',$xmlcomplemento);
            
            $referencia=$datoscomplemento->E_rfc.'_'.$idcomplemento;
        //======================================================================================
            
          $result = $clienteSOAP->TimbrarCFDI(array(
                    'usuario' => $usuario,
                    'password' => $password,
                    'cadenaXML' => $xmlcomplemento,
                    'referencia' => $referencia));
          file_put_contents($rutalog.'log_complemento'.$this->fechahoyL.'_'.$serie.'_'.$folio.'.txt', json_encode($result));
          if($result->TimbrarCFDIResult->CodigoRespuesta > 0 ) {                       
            
            $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                          'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                          'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                          'idcomplemento'=>$idcomplemento,
                          'info'=>$result,
                          'info2'=>$result->TimbrarCFDIResult->MensajeErrorDetallado
                          );
            
            $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('Estado'=>2,'mensajeerror'=>json_encode($result)),array('complementoId'=>$idcomplemento));
                        
            return $resultado;
        
        } else {
          
            try {
           
                $xmlCompleto=utf8_decode($result->TimbrarCFDIResult->XMLResultado);//Contiene XML
            
                //Guardar en archivo     y la ruta en la bd
                $ruta = $datosFactura['carpeta'] . '/facturas/complemento_'.$serie.'_'.$folio.'.xml';
            
                file_put_contents($ruta,"\xEF\xBB\xBF".$xmlCompleto);
                $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('rutaXml'=>$ruta),array('complementoId'=>$idcomplemento));            
            
                $sxe = new SimpleXMLElement($xmlCompleto);
                         
                $ns = $sxe->getNamespaces(true);
            
                $sxe->registerXPathNamespace('c', $ns['cfdi']);
            
                $sxe->registerXPathNamespace('t', $ns['tfd']);
 
                $uuid = '';
            
                foreach ($sxe->xpath('//t:TimbreFiscalDigital') as $tfd) {          
                 
                    //Actualizamos el Folio Fiscal UUID
                    //$uuid = $tfd['UUID'];
                    $updatedatossat=array(
                      'uuid'=>$tfd['UUID'],
                      'Sello'=>$tfd['SelloCFD'],
                      'sellosat'=>$tfd['SelloSAT'],
                      'NoCertificado'=>$tfd['NoCertificado'],
                      'nocertificadosat'=>$tfd['NoCertificadoSAT'],
                      'fechatimbre'=>date('Y-m-d G:i:s'),
                      'cadenaoriginal'=>$cadena,
                      'Estado'=>1
                    );
                    $this->ModeloCatalogos->updateCatalogo('f_complementopago',$updatedatossat,array('complementoId'=>$idcomplemento)); 
                } 
            
                //$this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>1),array('FacturasId'=>$datosFactura['factura_id']));

                $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Complemento Generado',
                          'idcomplemento'=>$idcomplemento
                          );
            return $resultado;
           }  catch (Exception $e) {
              $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                          'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                          'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                          'idcomplemento'=>$idcomplemento,
                          'info'=>$result,
                          'info2'=>$result->TimbrarCFDIResult->MensajeErrorDetallado
                          );
             
            $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('Estado'=>2,'mensajeerror'=>json_encode($result)),array('complementoId'=>$idcomplemento));
            
            return $resultado;
                
           }
        
        }
          //echo json_encode($result);
          
          return $resultado;

    }
    function generaxmlcomplemento($sello,$certificado,$numeroCertificado,$idcomplemento){
        
          $datoscomplemento=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$idcomplemento));
          $datoscomplemento=$datoscomplemento->result();
          $datoscomplemento=$datoscomplemento[0];

        $caract   = array('&','ñ','Ñ','°','á','é','í','ó','ú','Á','É','Í','Ó','Ú');
        $caract2  = array('&amp;','&ntilde;','&ntilde;','','a','e','i','o','u','A','E','I','O','U');
        $caractc   = array('&','ñ','Ñ','°','á','é','í','ó','ú','Á','É','Í','Ó','Ú');
        $caract2c  = array('&amp;','&ntilde;','&ntilde;','','a','e','i','o','u','A','E','I','O','U');
          $E_nombre=strval(str_replace($caract, $caract2, $datoscomplemento->E_nombre));
          $R_nombre=strval(str_replace($caract, $caract2, $datoscomplemento->R_nombre));
            $f_relacion=$datoscomplemento->f_relacion;
            $f_r_tipo=$datoscomplemento->f_r_tipo;
            $f_r_uuid=$datoscomplemento->f_r_uuid;

          $dcompdoc=$this->Modelofacturas->documentorelacionado($idcomplemento);
          foreach ($dcompdoc->result() as $itemdoc) {
            $clienteId=$itemdoc->Clientes_ClientesId;
          }
        //==================================================================
            $datoscliente=$this->ModeloCatalogos->getselectwheren('cli_facts',array('rid'=>$clienteId));
            $datoscliente=$datoscliente->result();
            $datoscliente=$datoscliente[0];
        //==================================================================
          $NumOperacion=str_replace(' ', '', $datoscomplemento->NumOperacion);
          $TotalTrasladosBaseIVA16=0;
            $TotalTrasladosImpuestoIVA16=0;
            foreach ($dcompdoc->result() as $itemdoc) {
              if($itemdoc->iva>0){                
                $basedr0=$itemdoc->ImpPagado/1.16;
                $ImporteDR0=$basedr0*0.16;
                //$TotalTrasladosBaseIVA16=$TotalTrasladosBaseIVA16+$basedr0;
                $TotalTrasladosBaseIVA16=$TotalTrasladosBaseIVA16+round($basedr0,2);
                //$TotalTrasladosImpuestoIVA16=$TotalTrasladosImpuestoIVA16+$ImporteDR0;
                $TotalTrasladosImpuestoIVA16=$TotalTrasladosImpuestoIVA16+round($ImporteDR0,2);
              }
            }
          $xmlcomplemento='<?xml version="1.0" encoding="utf-8"?>';
          $xmlcomplemento.='
            <cfdi:Comprobante xmlns:pago20="http://www.sat.gob.mx/Pagos20" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:cfdi="http://www.sat.gob.mx/cfd/4" xsi:schemaLocation="http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd http://www.sat.gob.mx/Pagos20 http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos20.xsd" Version="4.0" Exportacion="01"
              Folio="'.$datoscomplemento->Folio.'" Serie="'.$datoscomplemento->Serie.'" Fecha="'.date('Y-m-d',strtotime($datoscomplemento->Fecha)).'T'.date('H:i:s',strtotime($datoscomplemento->Fecha)).'" Sello="'.$sello.'" NoCertificado="'.$numeroCertificado.'" Certificado="'.$certificado.'" SubTotal="'.$datoscomplemento->SubTotal.'" Moneda="'.$datoscomplemento->Moneda.'" Total="'.$datoscomplemento->Total.'" TipoDeComprobante="'.$datoscomplemento->TipoDeComprobante.'" LugarExpedicion="'.$datoscomplemento->LugarExpedicion.'">';
              if($f_relacion==1){
                    $xmlcomplemento .='<cfdi:CfdiRelacionados TipoRelacion="'.$f_r_tipo.'">
                                <cfdi:CfdiRelacionado UUID="'.$f_r_uuid.'"/>
                            </cfdi:CfdiRelacionados>';
                }$xmlcomplemento .='
            <cfdi:Emisor Rfc="'.$datoscomplemento->E_rfc.'" Nombre="'.$E_nombre.'" RegimenFiscal="'.$datoscomplemento->E_regimenfiscal.'"/>
            <cfdi:Receptor Rfc="'.$datoscomplemento->R_rfc.'" Nombre="'.$R_nombre.'"  DomicilioFiscalReceptor="'.$datoscliente->fac_cod.'" RegimenFiscalReceptor="'.$datoscliente->RegimenFiscalReceptor.'" UsoCFDI="CP01"/>
            <cfdi:Conceptos>
              <cfdi:Concepto ClaveUnidad="'.$datoscomplemento->ClaveUnidad.'" ClaveProdServ="'.$datoscomplemento->ClaveProdServ.'" Cantidad="'.$datoscomplemento->Cantidad.'" Descripcion="'.$datoscomplemento->Descripcion.'" ValorUnitario="'.$datoscomplemento->ValorUnitario.'" Importe="'.$datoscomplemento->Importe.'" ObjetoImp="01"/>
            </cfdi:Conceptos>
            <cfdi:Complemento>
            <pago20:Pagos Version="2.0">
                <pago20:Totales MontoTotalPagos="'.$datoscomplemento->Monto.'" TotalTrasladosBaseIVA16="'.number_format(round($TotalTrasladosBaseIVA16,2), 2, ".", "").'" TotalTrasladosImpuestoIVA16="'.number_format(round($TotalTrasladosImpuestoIVA16,2), 2, ".", "").'"/>
                <pago20:Pago FechaPago="'.date('Y-m-d',strtotime($datoscomplemento->FechaPago)).'T'.date('H:i:s',strtotime($datoscomplemento->FechaPago)).'" FormaDePagoP="'.$datoscomplemento->FormaDePagoP.'" MonedaP="MXN" Monto="'.$datoscomplemento->Monto.'" TipoCambioP="1"  ';
            $xmlcomplemento.=' NumOperacion="'.$NumOperacion.'" '; 
            //$xmlcomplemento.=' CtaBeneficiario="9680096800" ';
            $xmlcomplemento.=' >';
            //$dcompdoc=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('complementoId'=>$idcomplemento));
            $RetencionesP=0;
            $TrasladosP=0;
            $ImporteDRtotal=0;
            $basedrtotal=0;
            $ImporteDRtotal0=0;
            foreach ($dcompdoc->result() as $itemdoc) {
             $xmlcomplemento.='<pago20:DoctoRelacionado IdDocumento="'.$itemdoc->IdDocumento.'" Serie="'.$itemdoc->serie.'" Folio="'.$itemdoc->Folio.'" MonedaDR="'.$datoscomplemento->MonedaDR.'" NumParcialidad="'.$itemdoc->NumParcialidad.'" ImpSaldoAnt="'.$itemdoc->ImpSaldoAnt.'" ImpPagado="'.$itemdoc->ImpPagado.'" ImpSaldoInsoluto="'.$itemdoc->ImpSaldoInsoluto.'" ObjetoImpDR="02" EquivalenciaDR="1">';
                                
                $xmlcomplemento.='<pago20:ImpuestosDR>';
                                    if($itemdoc->iva>0){
                                  $TrasladosP++;
                                  
                                  $basedr=$itemdoc->ImpPagado/1.16;
                                  $ImporteDR=$basedr*0.16;
                                  //$basedrtotal=$basedrtotal+$basedr;
                                  $basedrtotal=$basedrtotal+round($basedr,2);
                                  //$ImporteDRtotal0=$ImporteDRtotal0+$ImporteDR;
                                  $ImporteDRtotal0=$ImporteDRtotal0+round($ImporteDR,2);
                                  $xmlcomplemento.='<pago20:TrasladosDR>
                                                        <pago20:TrasladoDR BaseDR="'.number_format(round($basedr,2), 2, ".", "").'" ImporteDR="'.number_format(round($ImporteDR,2), 2, ".", "").'" ImpuestoDR="002" TasaOCuotaDR="0.160000" TipoFactorDR="Tasa"/>
                                                      </pago20:TrasladosDR>';
                                }
                                if($itemdoc->ivaretenido>0){
                                  $RetencionesP++;
                                  $basedr=$itemdoc->ImpPagado/1.16;
                                  $ImporteDR=$basedr*0.16;
                                  $ImporteDRtotal=$ImporteDRtotal+$ImporteDR;
                                  $xmlcomplemento.='<pago20:RetencionesDR>
                                                      <pago20:RetencionDR BaseDR="'.number_format(round($basedr,2), 2, ".", "").'" ImporteDR="'.number_format(round($ImporteDR,2), 2, ".", "").'" ImpuestoDR="002" TasaOCuotaDR="0.160000" TipoFactorDR="Tasa"/>
                                                    </pago20:RetencionesDR>';
                                }
                                    
                $xmlcomplemento.='</pago20:ImpuestosDR>';
                                  
             $xmlcomplemento.='</pago20:DoctoRelacionado>';
            }
            $xmlcomplemento.='<pago20:ImpuestosP>';
                              if($RetencionesP>0){
                                $xmlcomplemento.='<pago20:RetencionesP>';
                                    $xmlcomplemento.='<pago20:RetencionP ImporteP="'.number_format(round($ImporteDRtotal,2), 2, ".", "").'" ImpuestoP="002"/>';
                                $xmlcomplemento.='</pago20:RetencionesP>';
                              }
                              if($TrasladosP>0){
                                $xmlcomplemento.='<pago20:TrasladosP>';
                                   $xmlcomplemento.='<pago20:TrasladoP BaseP="'.number_format(round($basedrtotal,2), 2, ".", "").'" ImporteP="'.number_format(round($ImporteDRtotal0,2), 2, ".", "").'" ImpuestoP="002" TasaOCuotaP="0.160000" TipoFactorP="Tasa"/>';
                                $xmlcomplemento.='</pago20:TrasladosP>';
                              }
            $xmlcomplemento.='</pago20:ImpuestosP>';
            $xmlcomplemento.='</pago20:Pago>
            </pago20:Pagos>
            </cfdi:Complemento>
            </cfdi:Comprobante>';
            //log_message('error', 'se genera xml: '.$xmlcomplemento);
      return $xmlcomplemento;
    }
   
}