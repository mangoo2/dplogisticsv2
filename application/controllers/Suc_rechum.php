<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Suc_rechum extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');
            if ($this->ModeloPermisos->SECCION('coe_rechum')){
                //redirect('Sistema');    
            }else{
                redirect('Sistema');    
            }
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['list']=$this->ModeloCatalogos->get_list_coe_rh($this->sess_suc);

        $data['list_suc']=$this->ModeloCatalogos->genSelect('coe_sucs');
        $data['viewsuc']=1;
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_rechum/lista',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('theme/script_datatable');
        $this->load->view('coe_rechum/listajs',$data);
  	}

 

    function reg_evis($sucursal,$rid){
        $data['rrid']=$rid;
        $data['codigo']=$sucursal;
        $resultsuc=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$sucursal));
        $data['resultsuc']=$resultsuc->row();

        $resulrh=$this->ModeloCatalogos->getselectwheren('coe_rh',array('rid'=>$rid));
        $data['resulrh']=$resulrh->row();

        $data['list_evidemcias']=$this->ModeloCatalogos->getselectwheren('coe_rhevi',array('rrid'=>$rid));
        $data['viewsuc']=1;
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_rechum/reg_evis',$data);
        $this->load->view('theme/footer'); 
        //$this->load->view('theme/script_datatable');
        $this->load->view('coe_rechum/reg_evisjs');
    }



   

 





}