<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Conf_cla extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['resultlist']=$this->ModeloCatalogos->getselectwheren('coe_aers',array('rid >'=>0));
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('conf_cla/lista',$data);
        $this->load->view('theme/footer'); 
  	}
    function clas($codigo){
        $data['codigo']=$codigo;
        $resultaero=$this->ModeloCatalogos->getselectwheren('coe_aers',array('aero'=>$codigo));
        $data['resultaero']=$resultaero->row();
        $data['resultlist']=$this->ModeloCatalogos->getselectwheren_orderby('coe_clas',array('aero'=>$codigo),'cla_nom','ASC');
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('conf_cla/lista_tar',$data);
        $this->load->view('theme/footer');
        $this->load->view('conf_cla/lista_tarjs');
    }

    function inserupdate(){
        $params = $this->input->post();
        $rid = $params['rid'];
        unset($params['rid']);
        if($rid>0){
            unset($params['aero']);
            
                $this->ModeloCatalogos->updateCatalogo('coe_clas',$params,array('rid'=>$rid));
        }else{
                $this->ModeloCatalogos->Insert('coe_clas',$params);
        }
    }
    function delete(){
        $params = $this->input->post();
        $rid=$params['rid'];
        $this->ModeloCatalogos->getdeletewheren('coe_clas',array('rid'=>$rid));
    }

}