<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Coe_sers extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloCoesers');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['resultsuc']=$this->ModeloCatalogos->genSelect('coe_sucs');
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_sers/lista',$data);
        $this->load->view('theme/footer');
        
  	}
    function sers($codigo){
        $data['codigo']=$codigo;
        $data['sess_suc']=$this->sess_suc;
        $resultsuc=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$codigo));
        $data['resultsuc']=$resultsuc->row();
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_sers/listasuc',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('theme/script_datatable');
        $this->load->view('coe_sers/listasucjs');
    }
    function coe_cns($codigo){
        $data['codigo']=$codigo;
        $data['sess_suc']=$this->sess_suc;
        $resultsuc=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$codigo));
        $data['resultsuc']=$resultsuc->row();
        $data['resulcli']=$this->ModeloCatalogos->getselectwheren('ser_clis',array('emp'=>'rampa'));
        $data['resulser']=$this->ModeloCatalogos->getselectwheren('ser_sers',array('emp'=>'rampa'));
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_sers/listasuc_coecns',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('theme/script_datatable');
        $this->load->view('coe_sers/listasucjs');
    }
    function cad_cns($codigo){
        $data['codigo']=$codigo;
        $data['sess_suc']=$this->sess_suc;
        $resultsuc=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$codigo));
        $data['resultsuc']=$resultsuc->row();
        $data['resulcli']=$this->ModeloCatalogos->getselectwheren('ser_clis',array('emp'=>'rampa'));
        $data['resulser']=$this->ModeloCatalogos->getselectwheren('ser_sers',array('emp'=>'rampa'));
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_sers/listasuc_cadcns',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('theme/script_datatable');
        $this->load->view('coe_sers/listasucjs');
    }
   





}