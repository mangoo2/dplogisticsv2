<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Documentar extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloDocumentar');
        $this->load->model('Modelovuelos');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal = date('dmYHis');
        $this->feccap=date('Y-m-d H:i:s');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['mot_can']= $this->ModeloCatalogos->getselectwheren_o2('motivos_cancelacion',array('activo'=>1));
        //$mot_can = $this->ModeloCatalogos->getselectwheren('motivos_cancelacion',array('activo'=>1));
        //log_message('error', json_encode($mot_can->result()));
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('documentar/list',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('documentar/listjs');
  	}
    function bitacora(){
        $data['sess_suc']=$this->sess_suc;
        //$data['mot_can']= $this->ModeloCatalogos->getselectwheren('motivos_cancelacion',array('activo'=>1));
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('documentar/listbit',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('documentar/listbitjs');
    }
    public function getlista() {
        $params = $this->input->post();
        $params['sucursal']=$this->sess_suc;
        $getdata = $this->ModeloDocumentar->getlist($params);
        $totaldata= $this->ModeloDocumentar->getlist_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistaedicioneliminado() {
        $params = $this->input->post();
        $params['sucursal']=$this->sess_suc;
        $getdata = $this->ModeloDocumentar->getlistedicioneliminado($params);
        $totaldata= $this->ModeloDocumentar->getlistedicioneliminado_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function deletecot(){
        $params = $this->input->post();
        $coti = $params['coti'];
        //==========================
            $this->savebitacora_edit_delete($coti,0,$this->sess_usr);
        //==========================
        $results=$this->ModeloCatalogos->getselectwheren('doc_cotiza',array('rid'=>$coti));
        $cotiza='';
        $cot_folio='';
        $sucursal='';
        foreach ($results->result() as $item) {
            $cotiza=$item->cotiza;
            $cot_folio=$item->cot_folio;
            $sucursal=$item->sucursal;
        }
        if($cotiza!=''){
            $this->ModeloCatalogos->getdeletewheren('doc_cotiza',array('cotiza'=>$cotiza));
            $this->ModeloCatalogos->getdeletewheren('doc_paqs',array('cotiza'=>$cotiza));
            $this->ModeloCatalogos->getdeletewheren('doc_tars',array('cotiza'=>$cotiza));
        }
        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('rid'=>$coti,'suc'=>$sucursal,'usuario'=>$this->sess_usr,'tipo'=>'eliminación','descripcion'=>'Se realizo la eliminación, Documentación:'.$coti.'('.$cotiza.') Folio:'.$cot_folio));

        
    }
    function addi(){
        $cot=$this->fechal;
        $dataarrya=array(
                        'sucursal'=>$this->sess_suc, 
                        'emp'=>'', 
                        'aero'=>'', 
                        'usuario'=>$this->sess_usr, 
                        'cotiza'=>$cot, 
                        'cot_clasif'=>'', 
                        'cot_descrip'=>'', 
                        'cot_rutori'=>'', 
                        'cot_rutdes'=>'', 
                        'cot_remite'=>'', 
                        'cot_consign'=>'', 
                        'cot_factura'=>'', 
                        'cot_folio'=>'', 
                        'cot_guia'=>'', 
                        'cot_fdp'=>'', 
                        'cot_importe'=>0.0, 
                        'cot_cantidad'=>0.0, 
                        'cot_cambio'=>0.0, 
                        'cot_fecpag'=>'0000-00-00', 
                        'cot_conta'=>'no', 
                        'cot_estatus'=>'doc', 
                        'cot_tip'=>'', 
                        'cot_cos'=>'0.0', 
                        'cot_feccap'=>$this->feccap
                        );
        $this->ModeloCatalogos->Insert('doc_cotiza',$dataarrya);
        redirect('Documentar/add/'.$cot);
    }

    function add($numcoti){
        $data['cotizacion']=$numcoti;
        $data['result_aereo']=$this->ModeloCatalogos->getselectwheren('coe_aers','rid > 0');
        $data['result_ruta_origen']=$this->ModeloCatalogos->sucursalorigen($this->sess_suc);
        $data['result_ruta_destino']=$this->ModeloCatalogos->getselectwheren_orderby('coe_ruts','rid > 0','rut_cla','ASC');
        $doc_cotiza=$this->ModeloCatalogos->getselectwheren('doc_cotiza',array('cotiza'=>$numcoti));
        $data['doc_cotiza']=$doc_cotiza;

        $data['cfdi']=$this->ModeloCatalogos->genSelect('f_uso_cfdi'); 
        $data['metodo']=$this->ModeloCatalogos->genSelect('f_metodopago'); 
        $data['forma']=$this->ModeloCatalogos->genSelect('f_formapago'); 

        $data['sucursal']=$this->sess_suc;
        
        $doc_cotiza=$doc_cotiza->result();
        $doc_cotiza=$doc_cotiza[0];
        if($doc_cotiza->cot_estatus=='fdg'){
            if($doc_cotiza->sol_editar==0 or $doc_cotiza->sol_editar==1){
                
                redirect('Documentar/fdg/'.$doc_cotiza->cotiza); 
            }
        }

        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('documentar/add',$data);
        $this->load->view('documentar/modals');
        $this->load->view('theme/footer');
        $this->load->view('documentar/addjs');
    }
    function fdg($numcoti){
        $data['cotizacion']=$numcoti;
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('documentar/fdg',$data);
        $this->load->view('theme/footer');
    }
    function ob_cotclasif(){
        $params = $this->input->post();
        $aereo = $params['aereo'];
        $result_clas=$this->ModeloCatalogos->getselectwheren_orderby2('coe_clas',array('aero'=>$aereo),'cla_nom','ASC');
        $html='';
        $html.='<option 
                            data-ce="0"
                            data-cen=""
                            data-cf="0"
                            data-cfn=""
                            data-fer="0"
                            data-fern=""
                            data-cpc="0"
                            data-cpcn=""
                            data-ctipo=""
                    ></option>';
        foreach ($result_clas->result() as $item) {
            $html.='<option 
                            value="'.$item->clasif.'"
                            data-ce="'.$item->cla_ce.'"
                            data-cen="'.$item->cla_cen.'"
                            data-cf="'.$item->cla_cf.'"
                            data-cfn="'.$item->cla_cfn.'"
                            data-fer="'.$item->cla_fer.'"
                            data-fern="'.$item->cla_fern.'"
                            data-cpc="'.$item->cla_cpc.'"
                            data-cpcn="'.$item->cla_cpcn.'"
                            data-ctipo="'.$item->cla_tip.'"

                    >'.$item->cla_nom.' -> '.$item->cla_des.'</option>';
        }
        $aereotipo=0;
        if($aereo=='10102016224826'){
            $aereotipo=1;
        }
        if($aereo=='10102016224842'){
            $aereotipo=2;
        }
        $result_ts=$this->ModeloCatalogos->obtenertiposervicio($aereotipo);
        $tiposervicio='';
        foreach ($result_ts->result() as $item) {
            $tiposervicio.='<option value="'.$item->idtipo.'">'.$item->name.'</option>';
        }


        $array = array(
                    'clasificacion'=>$html,
                    'tiposervicio'=>$tiposervicio
                        );
        echo json_encode($array);
    }
    function searchclientes(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getseleclike_cliente($pro);
        //echo $results;
        echo json_encode($results->result());
    }
    function ob_datoscli(){
        $params = $this->input->post();
        $codigo = $params['codigo'];
        $results=$this->ModeloCatalogos->getselectwheren('cli_clis',array('cliente'=>$codigo));
        if($results->num_rows()>0){
            $num=1;
            $datos=$results->result();
            $datos=$datos[0];
            $results_consig=$this->ModeloCatalogos->getselectwheren('cli_consig',array('cliente'=>$codigo));
            $consignatarios=$results_consig->result();
            $results_cfac=$this->ModeloCatalogos->getselectwheren('cli_facts',array('cliente'=>$codigo));
            $facturas=$results_cfac->result();
            if($results_cfac->num_rows()==0){
                $paramsf['factura']=$this->fechal;
                $paramsf['cliente']=$codigo;
                $fac_nrs=$datos->cli_nombre;
                if($datos->cli_paterno!='' and $datos->cli_paterno!='-' and $datos->cli_paterno!='.'){
                    $fac_nrs.=' '.$datos->cli_paterno;
                }
                if($datos->cli_materno!='' and $datos->cli_materno!='-' and $datos->cli_materno!='.'){
                    $fac_nrs.=' '.$datos->cli_materno;
                }
                $paramsf['fac_nrs']=strtoupper($fac_nrs);
                $paramsf['fac_dir']='-';
                $paramsf['fac_col']='-';
                $paramsf['fac_mc']='-';
                $paramsf['fac_est']='Puebla';
                $paramsf['fac_cod']='72030';
                $paramsf['fac_la1']='52';
                $paramsf['fac_te1']='1254783690';
                $paramsf['fac_rfc']='XAXX010101000';
                $paramsf['fac_email']=' ';
                $paramsf['RegimenFiscalReceptor']='616';
                $paramsf['forma_pago']='01';
                $paramsf['metodo_pago']='PUE';
                $paramsf['uso_cfdi']='S01';
                $this->ModeloCatalogos->Insert('cli_facts',$paramsf);
            }
        }else{
            $num=0;
            $datos=array();
            $consignatarios=array();
            $facturas=array();
        }
        $array =array(
                    'num'=>$num,
                    'datos'=>$datos,
                    'consignatarios'=>$consignatarios,
                    'facturas'=>$facturas
                        );
        echo json_encode($array);
    }
    function obtenerinfor(){
        $params = $this->input->post();
        $descr='';
        $cotizacion=$params['cotiz'];
        $doc_cotiza=$this->ModeloCatalogos->getselectwheren_o3('doc_cotiza',array('cotiza'=>$cotizacion));
        $doc_paqs=$this->ModeloCatalogos->getselectwheren_o3('doc_paqs',array('cotiza'=>$cotizacion));
        $doc_tars=$this->ModeloCatalogos->getselectwheren_o3('doc_tars',array('cotiza'=>$cotizacion));
        $cliente='';
        $credito=1;
        $folio='';
        $tiposervicio=0;
        if($doc_cotiza->num_rows()>0){
            $doc_cotiza_n = $doc_cotiza->result();
            $doc_cotiza_n = $doc_cotiza_n[0];
            $tiposervicio = $doc_cotiza_n->tiposervicio;
            $cot_folio= $doc_cotiza_n->cot_folio;
            $results_cli = $this->ModeloCatalogos->getselectwheren_o3('cli_clis',array('cliente'=>$doc_cotiza_n->cot_remite));
            foreach ($results_cli->result() as $item) {
                $cliente= $item->cli_nombre.' '.$item->cli_paterno.' '.$item->cli_materno;
                $credito=$item->credito;
            }
            /*
            $results_aero= $this->ModeloCatalogos->getselectwheren('coe_aers',array('aero'=>$doc_cotiza_n->aero));
            foreach ($results_aero->result() as $item) {
                $descr.=$item->aer_nom.' ';
            }
            */
            $results_r1= $this->ModeloCatalogos->getselectwheren_o3('coe_ruts',array('ruta'=>$doc_cotiza_n->cot_rutori));
            foreach ($results_r1->result() as $item) {
                $descr.=$item->rut_cla.'-';
            }
            $results_r2= $this->ModeloCatalogos->getselectwheren_o3('coe_ruts',array('ruta'=>$doc_cotiza_n->cot_rutdes));
            foreach ($results_r2->result() as $item) {
                $descr.=$item->rut_cla;
            }
            
        }
        $resul_tp=$this->ModeloCatalogos->getselectwheren_o3('tiposervicio',array('idtipo'=>$tiposervicio));
        $array = array(
                        'doc_cotiza'=>$doc_cotiza->result(),
                        'doc_paqs'=>$doc_paqs->result(),
                        'doc_tars'=>$doc_tars->result(),
                        'cliente'=>$cliente,
                        'credito'=>$credito,
                        'descr'=>$descr,
                        'tiposervicio'=>$resul_tp->result(),
                        'cot_folio'=>$cot_folio
                    );
        echo json_encode($array);
    }
    function deletepaquete(){
        $params=$this->input->post();
        $id=$params['id'];
        $this->ModeloCatalogos->getdeletewheren('doc_paqs',array('rid'=>$id));
    }
    function saveinsertupdate1(){
        $params=$this->input->post();
        $cotizacion=$params['cotizacion'];
        //======================================================================
            $results_clas=$this->ModeloCatalogos->getselectwheren('coe_clas',array('clasif'=>$params['cot_clasif']));
            $cla_tip='';
            foreach ($results_clas->result() as $item) {
                $cla_tip=$item->cla_tip;
            }
            $dataarrya=array(
                            'aero'=>$params['aero'], 
                            'tiposervicio'=>$params['tiposervicio'],
                            'cot_clasif'=>$params['cot_clasif'], 
                            'cot_descrip'=>$params['cot_descrip'], 
                            'cot_rutori'=>$params['cot_rutori'], 
                            'cot_rutdes'=>$params['cot_rutdes'], 
                            'cot_remite'=>$params['cot_remite'],
                            'cot_tip'=>$cla_tip, 
                            'cot_cos'=>$params['cot_cos'], 

                            'v_origen'=>$params['v_origen'], 
                            'v_destino'=>$params['v_destino'], 
                            'vfecha'=>$params['vfecha'], 
                            'novuelo'=>$params['novuelo'], 
                            );

            $this->ModeloCatalogos->updateCatalogo('doc_cotiza',$dataarrya,array('cotiza'=>$cotizacion));
        //==========================================================================================
            $arraypaquetes=$params['arraypaquetes'];
            $DATA = json_decode($arraypaquetes);
            for ($i=0;$i<count($DATA);$i++) { 
                if($DATA[$i]->rid>0){
                    $dataarrarp=array(
                                        'paq_lar'=>$DATA[$i]->paq_lar,
                                        'paq_anc'=>$DATA[$i]->paq_anc,
                                        'paq_alt'=>$DATA[$i]->paq_alt,
                                        'paq_num'=>$DATA[$i]->paq_num,
                                        'paq_pes'=>$DATA[$i]->paq_pes
                                    );
                    $this->ModeloCatalogos->updateCatalogo('doc_paqs',$dataarrarp,array('rid'=>$DATA[$i]->rid));
                }else{
                    $dataarrarp=array(
                                        'cotiza'=>$cotizacion,
                                        'paquete'=>$this->fechal,
                                        'paq_lar'=>$DATA[$i]->paq_lar,
                                        'paq_anc'=>$DATA[$i]->paq_anc,
                                        'paq_alt'=>$DATA[$i]->paq_alt,
                                        'paq_num'=>$DATA[$i]->paq_num,
                                        'paq_pes'=>$DATA[$i]->paq_pes
                                    );
                    $this->ModeloCatalogos->Insert('doc_paqs',$dataarrarp);
                }
            }
        //==========================================================================================
            $results_doc_tars=$this->ModeloCatalogos->getselectwheren('doc_tars',array('cotiza'=>$cotizacion));
            if($results_doc_tars->num_rows()>0){
                $dataarrartr= array(
                    'tar_ce'=>$params['tar_ce'],
                    'tar_cen'=>$params['tar_cen'],
                    'tar_cf'=>$params['tar_cf'],
                    'tar_cfn'=>$params['tar_cfn'],
                    'tar_pov'=>$params['tar_pov'],
                    'tar_povn'=>$params['tar_povn'],
                    'tar_fer'=>$params['tar_fer'],
                    'tar_fern'=>$params['tar_fern'],
                    'tar_cpc'=>$params['tar_cpc'],
                    'tar_cpcn'=>$params['tar_cpcn'],
                    'tar_sub'=>$params['tar_sub'],
                    'tar_subn'=>$params['tar_subn'],
                    'tar_iva'=>$params['tar_iva'],
                    'tar_ivan'=>$params['tar_ivan'],
                    'tar_tot'=>$params['tar_tot'],
                    'tar_totn'=>$params['tar_totn'],
                );
                $this->ModeloCatalogos->updateCatalogo('doc_tars',$dataarrartr,array('cotiza'=>$cotizacion));
            }else{
                $dataarrartr= array(
                    'cotiza'=>$cotizacion,
                    'tar_ce'=>$params['tar_ce'],
                    'tar_cen'=>$params['tar_cen'],
                    'tar_cf'=>$params['tar_cf'],
                    'tar_cfn'=>$params['tar_cfn'],
                    'tar_pov'=>$params['tar_pov'],
                    'tar_povn'=>$params['tar_povn'],
                    'tar_fer'=>$params['tar_fer'],
                    'tar_fern'=>$params['tar_fern'],
                    'tar_cpc'=>$params['tar_cpc'],
                    'tar_cpcn'=>$params['tar_cpcn'],
                    'tar_sub'=>$params['tar_sub'],
                    'tar_subn'=>$params['tar_subn'],
                    'tar_iva'=>$params['tar_iva'],
                    'tar_ivan'=>$params['tar_ivan'],
                    'tar_tot'=>$params['tar_tot'],
                    'tar_totn'=>$params['tar_totn'],
                );
                $this->ModeloCatalogos->Insert('doc_tars',$dataarrartr);
            }
        //==========================================================================================
    }
    function ob_datoscli_consigtarario(){
        $params = $this->input->post();
        $codigo = $params['codigo'];
        $results=$this->ModeloCatalogos->getselectwheren('cli_consig',array('consign'=>$codigo));
        if($results->num_rows()>0){
            $num=1;
            $datos=$results->result();
            $datos=$datos[0];
            
        }else{
            $num=0;
            $datos=array();
        }
        $array =array(
                    'num'=>$num,
                    'datos'=>$datos
                        );
        echo json_encode($array);
    }
    function ob_datoscli_factura(){
        $params = $this->input->post();
        $codigo = $params['codigo'];
        $results=$this->ModeloCatalogos->getselectwheren('cli_facts',array('factura'=>$codigo));
        if($results->num_rows()>0){
            $num=1;
            $datos=$results->result();
            $datos=$datos[0];
            
        }else{
            $num=0;
            $datos=array();
        }
        $array =array(
                    'num'=>$num,
                    'datos'=>$datos
                        );
        echo json_encode($array);
    }
    function saveformconsignatario(){
        $params=$this->input->post();
        unset($params['rid']);
        $params['consign']=$this->fechal;
        $this->ModeloCatalogos->Insert('cli_consig',$params);
    }
    function saveformdatosfiscales(){
        $params=$this->input->post();
        unset($params['rid']);
        $params['factura']=$this->fechal;
        $rid=$this->ModeloCatalogos->Insert('cli_facts',$params);
        echo $rid;
    }
    function saveinsertupdate2(){
        $params=$this->input->post();
        $cotizacion=$params['cotizacion'];
        if(isset($params['facturar'])){
            $facturar=$params['facturar'];
        }else{
            $facturar=0;
        }
        //======================================================================
            $dataarrya=array(
                            
                            'cot_consign'=>$params['cot_consign'], 
                            'cot_factura'=>$params['cot_factura'],
                            'facturar'=>$facturar,
                            'cot_estatus'=>'cyf', 
                            );

            $this->ModeloCatalogos->updateCatalogo('doc_cotiza',$dataarrya,array('cotiza'=>$cotizacion));
        
    }
    function generarfoliofyg(){
        $params=$this->input->post();
        $cotizacion=$params['cotizacion'];
        $sucursal = $this->sess_suc;
        $newfolio='';
        //======================================================================
        $results_cot=$this->ModeloCatalogos->getselectwheren('doc_cotiza',array('cotiza'=>$cotizacion));
        $cot_folio_status=0;
        foreach ($results_cot->result() as $item) {
            if($item->cot_folio!=''){
                $cot_folio_status=0;
            }else{
                $cot_folio_status=1;
            }
        }
        if($cot_folio_status===1){
            $results_fyg=$this->ModeloCatalogos->getselectwheren('coe_fols',array('sucursal'=>$sucursal,'fol_est'=>'act'));
            $folio='';
            $fol_num=0;
            foreach ($results_fyg->result() as $item) {
                $folio=$item->folio;
                $fol_num=$item->fol_num;
            }
            $fol_num=$fol_num+1;

            $newfolio=$folio.$fol_num;

            $this->ModeloCatalogos->updateCatalogo('doc_cotiza',array('cot_folio'=>$newfolio),array('cotiza'=>$cotizacion));
            $this->ModeloCatalogos->updateCatalogo('coe_fols',array('fol_num'=>$fol_num),array('sucursal'=>$sucursal,'fol_est'=>'act'));
        }
    }
    function saveinsertupdate3(){
        $params=$this->input->post();
        $cotizacion=$params['cotizacion'];
        $cot_guia=$params['guia'];
        //======================================================================
            $dataarrya=array(
                            
                            'cot_guia'=>$cot_guia 
                            );

            $this->ModeloCatalogos->updateCatalogo('doc_cotiza',$dataarrya,array('cotiza'=>$cotizacion));

            $results_cot=$this->ModeloCatalogos->getselectwheren('doc_cotiza',array('cotiza'=>$cotizacion));
            foreach ($results_cot->result() as $item) {
                $sucursal_doc_cot=$item->sucursal;
                $cot_folio=$item->cot_folio;

                $result_guias=$this->ModeloCatalogos->getselectwheren('guias',array('sucursal'=>$sucursal_doc_cot,'folio'=>$cot_folio));
                if($result_guias->num_rows()>0){
                    $this->ModeloCatalogos->updateCatalogo('guias',array('guia'=>$cot_guia),array('sucursal'=>$sucursal_doc_cot,'folio'=>$cot_folio));
                }
            }
        
    }
    function saveinsertupdate4(){
        $params=$this->input->post();
        $cotizacion=$params['cotizacion'];
        unset($params['cotizacion']);
        $params['cot_estatus']='fdg';
        $params['sol_editar']='0';
        $this->ModeloCatalogos->updateCatalogo('doc_cotiza',$params,array('cotiza'=>$cotizacion));
    }
    function editsol(){
        $params=$this->input->post();
        $cotizacion=$params['coti'];
        $codigo=$params['codigo'];
        $sol=$params['sol'];
        $suc=$params['suc'];

        //========================
            $results=$this->ModeloCatalogos->getselectwheren('doc_cotiza',array('rid'=>$cotizacion));
            $cotiza='';
            $cot_folio='';
            $sucursal='';
            foreach ($results->result() as $item) {
                $cotiza=$item->cotiza;
                $cot_folio=$item->cot_folio;
                $sucursal=$item->sucursal;
            }
        //========================
        if($sol==2){
            $paramsdata['sol_editar']=$sol;
            $paramsdata['cot_estatus']='doc';
        }else{
            $paramsdata['sol_editar']=$sol;
            
        }
        if($sol==1){
            $paramsdata['sol_edit_delete_personal']=$this->sess_usr;
        }
        if($sol==1){
            $bt_tipo='Solicitud de edición';
            $bt_descr='Se realizo una solicitud de edición, Documentación:'.$cotizacion.'('.$codigo.') Folio:'.$cot_folio;
        }elseif($sol==2){
            $bt_tipo='Permitió la edición';
            $bt_descr='Se permitió la modificación de la cotización, Documentación:'.$cotizacion.'('.$codigo.') Folio:'.$cot_folio;
            //=====================================================
            $paramsdata['cot_conta']='no';
            $result_cot=$this->ModeloCatalogos->getselectwheren('doc_cotiza',array('rid'=>$cotizacion));
            foreach ($result_cot->result() as $itemdc) {
                $guiag=$itemdc->cot_guia;
                $folio=$itemdc->cot_folio;
                $result_guia=$this->ModeloCatalogos->getselectwheren('guias',array('folio'=>$folio,'guia'=>$guiag));
                foreach ($result_guia->result() as $itemg) {
                    $guia_rid=$itemg->rid;
                    $this->ModeloCatalogos->getdeletewheren('guia_facts',array('guia'=>$guia_rid));
                    $this->ModeloCatalogos->getdeletewheren('guia_pags',array('guia'=>$guia_rid));
                    $this->ModeloCatalogos->getdeletewheren('guia_paqs',array('guia'=>$guia_rid));
                    $this->ModeloCatalogos->getdeletewheren('guia_rycs',array('guia'=>$guia_rid));
                    $this->ModeloCatalogos->getdeletewheren('guia_tars',array('guia'=>$guia_rid));
                    $this->ModeloCatalogos->getdeletewheren('guias',array('rid'=>$guia_rid));
                    
                }
            }
            

            //=====================================================
        }elseif($sol==3){
            $bt_tipo='Denegó la edición';
            $bt_descr='Se Denegó la modificación de la cotización, Documentación:'.$cotizacion.'('.$codigo.') Folio:'.$cot_folio;
        }else{
            $bt_tipo='';
            $bt_descr='';
        }
        $this->ModeloCatalogos->updateCatalogo('doc_cotiza',$paramsdata,array('rid'=>$cotizacion));



        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('rid'=>$cotizacion,'usuario'=>$this->sess_usr,'tipo'=>$bt_tipo,'descripcion'=>$bt_descr,'suc'=>$sucursal));
        if($sol==2){
            $this->savebitacora_edit_delete($cotizacion,1,$this->sess_usr);
        }
    }
    function soldeletecot(){
        $params = $this->input->post();
        $coti = $params['coti'];
        $sol = $params['sol'];
        $motselect=$params['motselect'];
        $descrip=$params['descrip'];
        $codigo=$params['codigo'];
        $suc=$params['suc'];

        $datas['sol_delete']=$sol;
        $datas['tipo_cancelacion']=$motselect;
        $datas['descrip_can']=$descrip;
        //========================
            $results=$this->ModeloCatalogos->getselectwheren('doc_cotiza',array('rid'=>$coti));
            $cotiza='';
            $cot_folio='';
            $sucursal='';
            foreach ($results->result() as $item) {
                $cotiza=$item->cotiza;
                $cot_folio=$item->cot_folio;
                $sucursal=$item->sucursal;
            }
        //========================
        //$this->ModeloCatalogos->updateCatalogo('doc_cotiza',array('sol_delete'=>$sol,'tipo_cancelacion'=>$motselect,'descrip_can'=>$descrip),array('rid'=>$coti));
        if($sol==1){
            $datas['sol_edit_delete_personal']=$this->sess_usr;
        }
        $this->ModeloCatalogos->updateCatalogo('doc_cotiza',$datas,array('rid'=>$coti));

        if($sol==1){
            $bt_tipo='Solicitud de eliminación';
            $bt_descr='Se realizo una solicitud de eliminación, Documentación:'.$coti.'('.$codigo.'), Folio: '.$cot_folio.' '.$descrip;
        }elseif($sol==0){
            $bt_tipo='No se permitió la eliminación';
            $bt_descr='No se permite la eliminación, Documentación:'.$coti.'('.$codigo.') Folio: '.$cot_folio;
        }else{
            $bt_tipo='';
            $bt_descr='';
        }
        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('rid'=>$coti,'usuario'=>$this->sess_usr,'tipo'=>$bt_tipo,'descripcion'=>$bt_descr,'suc'=>$suc));
        
    }
    function datosfiscales(){
        $this->load->view('reportes/datosfiscales');
    }
    function imagenrfc_escaneo(){
        $rid = $_POST['id'];
       
        if (empty($_FILES['escaneofile']['name'])) {
            $output = [];
        }else{

            $config['upload_path']          = FCPATH.'_files/_documentos/';
            $config['allowed_types']        = '*';
            $config['max_size']             = 5000;
            $config['file_name']='escaneofile_'.date('YmdGis').'_'.rand(0, 99);       

            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('escaneofile')){
                $error = array('error' => $this->upload->display_errors());
                log_message('error', json_encode($error));
                            
            }else{
                 $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                $file_name = $upload_data['file_name']; //uploded file name
                $extension=$upload_data['file_ext'];    // uploded file extension

                $this->ModeloCatalogos->updateCatalogo('cli_facts',array('escaneofile'=>$file_name),array('rid'=>$rid));

                $data = array('upload_data' => $this->upload->data());
                $output = [];
                log_message('error', json_encode($data));
            }
        }
        echo json_encode($output);
    }
    function imagenrfc_situacionf(){
        $rid = $_POST['id'];
       
        if (empty($_FILES['situacion_fiscal_file']['name'])) {
            $output = [];
        }else{
            $config['upload_path']          = FCPATH.'_files/_documentos/';
            $config['allowed_types']        = '*';
            $config['max_size']             = 5000;
            $config['file_name']='situacion_fiscal_file_'.date('YmdGis').'_'.rand(0, 99);       

            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('situacion_fiscal_file')){
                $error = array('error' => $this->upload->display_errors());
                log_message('error', json_encode($error));
                            
            }else{
                 $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                $file_name = $upload_data['file_name']; //uploded file name
                $extension=$upload_data['file_ext'];    // uploded file extension

                $this->ModeloCatalogos->updateCatalogo('cli_facts',array('situacion_fiscal_file'=>$file_name),array('rid'=>$rid));

                $data = array('upload_data' => $this->upload->data());
                $output = [];
                log_message('error', json_encode($data));
            }
        }   
        echo json_encode($output);
    }
    function consultarvuelos(){
        $params = $this->input->post();
        $vo = $params['vo'];
        $vd = $params['vd'];
        $vf = $params['vf'];

        $results=$this->ModeloCatalogos->getselectwheren('config_vuelos',array('origen'=>$vo,'destino'=>$vd,'fecha'=>$vf,'activo >='=>1));

        echo json_encode($results->result()); 
    }

    function search_origen(){
        $search = $this->input->get('search');
        $results = $this->ModeloDocumentar->get_origen_like($search);
        echo json_encode($results);    
    }

    function search_origen_domicilio(){
        $search = $this->input->get('search');
        $results = $this->ModeloDocumentar->get_origen_domicilio_like($search);
        echo json_encode($results);    
    }

    function search_destino(){
        $search = $this->input->get('search');
        $results = $this->ModeloDocumentar->get_destino_like($search);
        echo json_encode($results);    
    }

    function search_destino_domicilio(){
        $search = $this->input->get('search');
        $results = $this->ModeloDocumentar->get_destino_domicilio_like($search);
        echo json_encode($results);    
    }
    function savebitacora_edit_delete($coti,$tipo){
        $results=$this->ModeloDocumentar->get_info_guia_cotizacion($coti);
        foreach ($results->result() as $item) {
            $item->tipo_ed_de=$tipo;
            $item->personal_confirmo=$this->sess_usr;
            $this->ModeloCatalogos->Insert('bitacora_edicion_eliminacion',$item);
        }

    }


}