<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal_larga = date('Y-m-d H:i:s');
        if($this->session->userdata('logeado')==true){
        }else{
            redirect('Sistema'); 
        }
    }
    function index(){
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('inicio');
        $this->load->view('theme/footer');
    }
    function consultar($id=0){
        if(isset($_GET['folio'])){
            $folio=$_GET['folio'];
        }else{
            $folio='';
        }
        $serie='CA';
        //$strq = "SELECT max(Folio) as Folio FROM f_facturas WHERE activo=1 and serie='$serie' ";
        if($id==0){
            //$strq = "SELECT * FROM guias where rid>660 and folio like 'CORP24%' ";suc_cors
            //$strq = "SELECT * FROM suc_cors where sucursal='21122015201618' and rid='7190'";
            //$strq = "SELECT * FROM cli_clis where cli_nombre like'%tsunami%'";
            //$strq = "SELECT * FROM guias where cliente='03012024161456'";
            
           // $strq = "ALTER TABLE `doc_cotiza` ADD `sol_edit_delete_personal` VARCHAR(100) NULL DEFAULT NULL AFTER `sol_delete`";
            $strq = "SELECT * FROM f_facturas WHERE FacturasId > '303'";
            $strq="SELECT * from guia_facts where rid>1042";
            $strq="ALTER TABLE `bitacora_edicion_eliminacion` CHANGE `sol_edit_delete_personal` `sol_edit_delete_personal` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL";
            $strq = "SELECT * FROM guias where rid>660 and folio like 'CORP24%' ";//suc_cors
            
            $strq = "ALTER TABLE `suc_deps` ADD `relacionguias_n` TEXT NULL DEFAULT NULL AFTER `relacionguias`";
            $strq = "SELECT * FROM f_facturas WHERE FacturasId = '954'";

         
        }
        if($id==1){
            //$strq = "Describe  bitacora_edicion_eliminacion"; 
            //$strq = "SELECT * FROM guia_pags where guia='$folio'";
            //$strq = "SELECT * FROM f_facturas WHERE FacturasId > '320'";
            $strq="SELECT * FROM doc_cotiza where rid>1198 or cotiza='05022024164911'";
            $strq = "SELECT * FROM f_facturas WHERE FacturasId > '280' and FacturasId < '300'";
            $strq = "Describe `suc_deps` ";

            

        }
        
        
        //$strq = "SELECT * FROM `doc_cotiza` ";
        
        $Folio = 0;
        $query = $this->db->query($strq);
        //var_dump($query->result());
        echo json_encode($query->result());
    }
    function consultar2(){
        $serie='CO';
        //$strq = "SELECT max(Folio) as Folio FROM f_facturas WHERE activo=1 and serie='$serie' ";
        //$strq = "SELECT * FROM f_facturas WHERE activo=1 and serie='$serie' ";
        $strq = "SELECT * FROM guias  ";
        
        $Folio = 0;
        $query = $this->db->query($strq);

        echo json_encode($query->result());
    }
    function delete($id){
        echo $this->fechal_larga;
        //$this->ModeloCatalogos->getdeletewheren('coe_sucs',array('rid'=>$id));
        //$this->ModeloCatalogos->getdeletewheren('f_facturas',array('FacturasId'=>$id));

        //echo json_encode($query->result());
    }
    function update(){
        //$this->ModeloCatalogos->updateCatalogo('f_facturas',array('FormaPago'=>'PUE'),array('FacturasId'=>88));
        $params_up['peso']=373.50;
        $params_up['volumen']=162.13;

        //$this->ModeloCatalogos->updateCatalogo('guias',$params_up,array('rid'=>166));

        //$this->ModeloCatalogos->updateCatalogo('suc_cors',array('cor_tarjeta'=>'4078'),array('rid'=>7117));
        //$this->ModeloCatalogos->updateCatalogo('f_facturas',array('Cp'=>'03303'),array('FacturasId'=>62));
        //$this->ModeloCatalogos->updateCatalogo('f_facturas',array('Nombre'=>'COMERCIALIZADORA MEDICA MAVA'),array('FacturasId'=>62));
        //$this->ModeloCatalogos->updateCatalogo('cli_facts',array('fac_cod'=>'03303'),array('rid'=>47));
        //$this->ModeloCatalogos->updateCatalogo('cli_facts',array('fac_nrs'=>'COMERCIALIZADORA MEDICA MAVA'),array('rid'=>47));
        //$this->ModeloCatalogos->updateCatalogo('guias',array('corte'=>'03012024185833'),array('rid'=>37));
        //$this->ModeloCatalogos->updateCatalogo('f_facturas',array('Nombre'=>'PUBLICO EN GENERAL'),array('FacturasId'=>152));
        //$this->ModeloCatalogos->updateCatalogo('cli_facts',array('fac_nrs'=>'PUBLICO EN GENERAL'),array('rid'=>350));
        //$this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>'2'),array('FacturasId'=>184)); 
        //$this->ModeloCatalogos->updateCatalogo('guias',array('corte'=>'22012024160456'),array('rid'=>668));
        //$this->ModeloCatalogos->updateCatalogo('guias',array('sucursal'=>'21122015201618'),array('rid'=>668));
        //$this->ModeloCatalogos->updateCatalogo('suc_cors',array('cor_efectivo'=>'19041'),array('rid'=>7190));
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('uso_cfdi'=>'S01'),array('FacturasId'=>954));
    }
    function update2(){
        //$_SESSION['sess_suc']='21122015201618';
        //$_SESSION['sess_suc']='09022017190102';
        //$_SESSION['sess_suc']='01032016012634';
        
        //$_SESSION['sess_suc']='03102016223335';//pbc24
        //$_SESSION['sess_suc']='11022022151938';
        $_SESSION['sess_suc']='21122015201618';
        echo $_SESSION['sess_suc']; 
        //echo $this->session->userdata('sess_suc');
    }
    function obtenerguias($numcliente){
        $strq = "SELECT emp,guia,con,con_dir,con_col,con_ciu,con_tel,con_cp FROM guia_rycs WHERE cliente='$numcliente'";
        $query = $this->db->query($strq);

        echo json_encode($query->result());
    }
    

}