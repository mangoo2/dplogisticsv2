<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Conf_sers extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['resultlist']=$this->ModeloCatalogos->getselectwheren_orderby('coe_sers',array('rid >'=>0),'emp_pos','ASC');
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('conf_sers/lista',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('conf_sers/listajs');
  	}
    function emp($empresa){
        $data['empresa']=$empresa;
        $data['sess_suc']=$this->sess_suc;
        $resultrow=$this->ModeloCatalogos->getselectwheren('coe_sers',array('emp'=>$empresa));
        $data['resultrow']=$resultrow->row();

        $data['list_clis']=$this->ModeloCatalogos->getselectwheren_orderby('ser_clis',array('emp'=>$empresa),'cli_pos','ASC');
        $data['list_sers']=$this->ModeloCatalogos->getselectwheren_orderby('ser_sers',array('emp'=>$empresa),'ser_pos','ASC');

        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('conf_sers/emp',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('conf_sers/empjs');    
    }

    function inserupdate_c(){
        $params = $this->input->post();
        $rid = $params['rid'];
        unset($params['rid']);
        if($rid>0){
                $this->ModeloCatalogos->updateCatalogo('ser_clis',$params,array('rid'=>$rid));
        }else{
                $this->ModeloCatalogos->Insert('ser_clis',$params);
        }
    }
    function inserupdate_s(){
        $params = $this->input->post();
        $rid = $params['rid'];
        unset($params['rid']);
        if($rid>0){
                $this->ModeloCatalogos->updateCatalogo('ser_sers',$params,array('rid'=>$rid));
        }else{
                $this->ModeloCatalogos->Insert('ser_sers',$params);
        }
    }
    function delete(){
        $params = $this->input->post();
        $rid=$params['rid'];
        $tipo=$params['tipo'];
        if($tipo==1){
            $this->ModeloCatalogos->getdeletewheren('ser_clis',array('rid'=>$rid));    
        }
        if($tipo==2){
            $this->ModeloCatalogos->getdeletewheren('ser_sers',array('rid'=>$rid));    
        }
        
    }

}