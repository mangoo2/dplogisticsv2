<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
//C:\xampp\htdocs\dplogistics\JSIMods\coe_pyg\ops\pyg.php
//C:\xampp\htdocs\dplogistics\JSIMods\coe_pyg\ops\var.php
//C:\xampp\htdocs\dplogistics\JSIMods\coe_pyg\ops\pag.php
class Coe_pyg extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['resultsuc']=$this->ModeloCatalogos->genSelect('coe_sucs');
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_pyg/lista',$data);
        $this->load->view('theme/footer'); 
 
  	}
    function pyg($codigo){
        $data['codigo']=$codigo;
        $data['sess_suc']=$this->sess_suc;
        $resultsuc=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$codigo));
        $data['resultsuc']=$resultsuc->row();
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_pyg/listasuc',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('theme/script_datatable');
        $this->load->view('coe_pyg/listasucjs');
    }
    function inserupdate_comprobante(){
        $rid = $_POST['rid'];
        $pre_importe = $_POST['pre_importe'];
        $pre_forma = $_POST['pre_forma'];
        $pre_concepto = $_POST['pre_concepto'];
        $pre_fecha = $_POST['pre_fecha'];
        $pre_asigno = $_POST['pre_asigno'];
        $sucursal = $_POST['sucursal'];
        $presupuesto=$this->fechal;
        $pre_saldo=$pre_importe;
        $pre_feccap=$this->fechalarga;
        $pre_tipo = $_POST['pre_tipo'];
        $pre_obs = '';
        $admin= $this->sess_usr;

        $DIR_SUC0=FCPATH.'_files/_suc/'.$sucursal.'';
        $DIR_SUC=FCPATH.'_files/_suc/'.$sucursal.'/pre';
        if (!is_dir($DIR_SUC) && strlen($DIR_SUC)>0){
            log_message('error',$DIR_SUC);
            //mkdir($DIR_SUC, 0755);
            if(!is_dir($DIR_SUC0)){
                mkdir($DIR_SUC0, 0755);
                mkdir($DIR_SUC, 0755);
            }else{
                if(!is_dir($DIR_SUC)){
                    //mkdir($DIR_SUC0, 0755);
                    mkdir($DIR_SUC, 0755);
                    //log_message('error','Crea las carpetas');
                }   
            }
        }else{
            file_put_contents($DIR_SUC.'/index.html', '');
        }
    

        $config['upload_path']          = $DIR_SUC;
        $config['allowed_types']        = '*';
        $config['max_size']             = 10000;
        $config['file_name']=date('YmdGis').'_'.rand(0, 99);       
        $output = [];
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('pre_comprobante')){
            $data = array('error' => $this->upload->display_errors());
            log_message('error', 'Error en la carga '.json_encode($data));
            if($rid>0){
                $this->ModeloCatalogos->updateCatalogo('suc_pres',array('pre_importe'=>$pre_importe,'pre_forma'=>$pre_forma,'pre_concepto'=>$pre_concepto,'pre_fecha'=>$pre_fecha,'pre_asigno'=>$pre_asigno,'pre_saldo'=>$pre_saldo),array('rid'=>$rid,'pre_verifico'=>''));
            }else{
                $this->ModeloCatalogos->Insert('suc_pres',array(
                        'pre_importe'=>$pre_importe,
                        'pre_forma'=>$pre_forma,
                        'pre_concepto'=>$pre_concepto,
                        'pre_fecha'=>$pre_fecha,
                        'pre_asigno'=>$pre_asigno,
                        'sucursal'=>$sucursal,
                        'presupuesto'=>$presupuesto,
                        'pre_saldo'=>$pre_saldo,
                        'pre_feccap'=>$pre_feccap,
                        'pre_tipo'=>$pre_tipo,
                        'pre_obs'=>$pre_obs,
                        'admin'=>$admin,
                        'pre_verifico'=>''
                ));
            }
                        
        }else{
             $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension

            if($rid>0){
                $this->ModeloCatalogos->updateCatalogo('suc_pres',array('pre_importe'=>$pre_importe,'pre_forma'=>$pre_forma,'pre_concepto'=>$pre_concepto,'pre_fecha'=>$pre_fecha,'pre_asigno'=>$pre_asigno,'pre_saldo'=>$pre_saldo,'pre_comprobante'=>$file_name),array('rid'=>$rid,'pre_verifico'=>''));
            }else{
                $this->ModeloCatalogos->Insert('suc_pres',array(
                        'pre_importe'=>$pre_importe,
                        'pre_forma'=>$pre_forma,
                        'pre_concepto'=>$pre_concepto,
                        'pre_fecha'=>$pre_fecha,
                        'pre_asigno'=>$pre_asigno,
                        'sucursal'=>$sucursal,
                        'presupuesto'=>$presupuesto,
                        'pre_saldo'=>$pre_saldo,
                        'pre_feccap'=>$pre_feccap,
                        'pre_tipo'=>$pre_tipo,
                        'pre_obs'=>$pre_obs,
                        'admin'=>$admin,
                        'pre_comprobante'=>$file_name,
                        'pre_verifico'=>''
                ));
            }

            $data = array('upload_data' => $this->upload->data());
            //$output = [];
            log_message('error', 'Carga correcta '.json_encode($data));
        }
        echo json_encode($output);
    }
    function inserupdate_compfin(){
        $rid = $_POST['sprid'];
        $egr_est = $_POST['egrest'];
        $sucursal = $_POST['sucursal'];
        $egr_obs='';
        $result=$this->ModeloCatalogos->getselectwheren('suc_egrb',array('rid'=>$rid));
        foreach ($result->result() as $item) {
            $egr_obs.=$item->egr_obs;
        }
        $egr_obs.='|'.date('d/m/Y H:i:s').', Admin:'.$this->sess_usr.', Estatus:'.$egr_est;

        if($egr_est=='Cancelado' || $egr_est=='Finalizado'){
            $egr_fecfin=date('Y-m-d H:i:s');
        }else{
            $egr_fecfin='0000-00-00 00:00:00';
        }
        //===========================================================
        $DIR_SUC0=FCPATH.'_files/_suc/'.$sucursal.'';
        $DIR_SUC=FCPATH.'_files/_suc/'.$sucursal.'/pag';
        if (!is_dir($DIR_SUC) && strlen($DIR_SUC)>0){
            log_message('error',$DIR_SUC);
            //mkdir($DIR_SUC, 0755);
            if(!is_dir($DIR_SUC0)){
                mkdir($DIR_SUC0, 0755);
                mkdir($DIR_SUC, 0755);
                //log_message('error','Crea las carpetas');
            }else{
                if(!is_dir($DIR_SUC)){
                    //mkdir($DIR_SUC0, 0755);
                    mkdir($DIR_SUC, 0755);
                    //log_message('error','Crea las carpetas');
                }   
            }
        }else{
            file_put_contents($DIR_SUC.'/index.html', '');
        }
    

        $config['upload_path']          = $DIR_SUC;
        $config['allowed_types']        = '*';
        $config['max_size']             = 10000;
        $config['file_name']=date('YmdGis').'_'.rand(0, 99);       
        $output = [];
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('egr_compfin')){
            $data = array('error' => $this->upload->display_errors());
            log_message('error', 'Error en la carga '.json_encode($data));
            if($rid>0){
                $this->ModeloCatalogos->updateCatalogo('suc_egrb',array('egr_est'=>$egr_est,'egr_obs'=>$egr_obs,'egr_fecfin'=>$egr_fecfin),array('rid'=>$rid));
            }else{
                
            }
            $output = $data;            
        }else{
             $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension

            if($rid>0){
                $this->ModeloCatalogos->updateCatalogo('suc_egrb',array('egr_est'=>$egr_est,'egr_obs'=>$egr_obs,'egr_fecfin'=>$egr_fecfin,'egr_compfin'=>$file_name),array('rid'=>$rid));
            }else{
                
            }

            $data = array('upload_data' => $this->upload->data());
            //$output = $data;
            log_message('error', 'Carga correcta '.json_encode($data));
        }
        echo json_encode($output);
    }
    function delete_pres(){
        $params = $this->input->post();
        $rid = $params['rid'];

        $this->ModeloCatalogos->getdeletewheren('suc_pres',array('rid'=>$rid ));
    }
   

 





}