<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Coe_creds extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['resultsuc']=$this->ModeloCatalogos->genSelect('coe_sucs');
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_creds/lista',$data);
        $this->load->view('theme/footer');  
  	}
    function suc($codigo){
        $data['codigo']=$codigo;
        $data['sess_suc']=$this->sess_suc;
        $resultsuc=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$codigo));
        $data['resultsuc']=$resultsuc->row();
        $data['resulgpags']=$this->ModeloCatalogos->list_coe_creds($codigo);
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_creds/listasuc',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('theme/script_datatable');
        $this->load->view('coe_creds/listasucjs');
    }
    function cons($codigo){
        $data['codigo']=$codigo;
        $data['sess_suc']=$this->sess_suc;
        $resultsuc=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$codigo));
        $data['resultsuc']=$resultsuc->row();
        
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_creds/listasucs',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('theme/script_datatable');
        $this->load->view('coe_creds/listasucjs');
    }
 





}