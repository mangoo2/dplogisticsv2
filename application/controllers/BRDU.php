<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class BRDU extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloBrdu');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('brdu/lista',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('brdu/listajs');
  	}
    public function getlista() {
        $params = $this->input->post();
        $params['sucursal']=$this->sess_suc;
        $getdata = $this->ModeloBrdu->getlist($params);
        $totaldata= $this->ModeloBrdu->getlist_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function rev_nue($id=0){
        $data['sess_suc']=$this->sess_suc;
        $data['result_pam_equ']=$this->ModeloCatalogos->getselectwheren('pam_equ',array('sucursal'=>$this->sess_suc));
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('brdu/rev_nue',$data);
        $this->load->view('theme/footer');
        $this->load->view('brdu/rev_nuejs');
    }
    function rev_gua(){
        $params = $this->input->post();
        $rev_fecha=$params['rev_fecha'];
        $uni_equi_a=$params['uni_equi_a'];
        unset($params['uni_equi_a']);
        $dataarray=array(
                        'sucursal'=>$this->sess_suc,
                        'usuario'=>$this->sess_usr,
                        'rev'=>'',
                        'rev_est'=>'ini',
                        'rev_fec'=>$this->fechahoy,
                        'rev_cap'=>$this->fechalarga,
                        'rev_fecha'=>$rev_fecha,
                        'rev_mes'=>$this->mesactual,
                        'rev_sem'=>$this->semanaactual,
                        'suc_resp'=>$this->sess_usr,
                    );
        $rid=$this->ModeloCatalogos->Insertbd2('brdu_revs',$dataarray);

        $rev = 'BRDU-'.$this->sess_sucname.'-'.date('dmY').'-'.$rid;

        $this->ModeloCatalogos->updateCatalogobd2('brdu_revs',array('rev'=>$rev),array('rid'=>$rid));

        $resultpres=$this->ModeloCatalogos->genSelectbd2('brdu_pres');
        $dataeqarrayinsert=array();
        foreach ($resultpres->result() as $itempres) {
            $dataeqarrayinsert[] = array(
                                            'rev'=>$rid,
                                            'pre'=>$itempres->pre,
                                            'pre_pos'=>$itempres->pre_pos,
                                            'pre_tit'=>$itempres->pre_tit
                                        );
        }
        log_message('error', json_encode($dataeqarrayinsert));
        if(count($dataeqarrayinsert)>0){
            $this->ModeloCatalogos->insert_batchbd2('rev_pres',$dataeqarrayinsert);
        }
        $DATAue = json_decode($uni_equi_a);
        for ($i=0;$i<count($DATAue);$i++) {
            //$DATAue[$i];
            $resultweq=$this->ModeloCatalogos->getselectwheren('pam_equ',array('sucursal'=>$this->sess_suc,'equipo'=>$DATAue[$i]));
            $resultweq=$resultweq->row();

            $this->ModeloCatalogos->Insertbd2('rev_unis',array('rev'=>$rid,'rev_fec'=>$rev_fecha,'equipo'=>$DATAue[$i],'equ_nombre'=>$resultweq->equ_nombre,'equ_numeco'=>$resultweq->equ_numeco,'equ_des'=>''));
        }
        $resultrunis=$this->ModeloCatalogos->getselectwherenbd2('rev_unis',array('rev'=>$rid));
        foreach ($resultrunis->result() as $itemru) {
            $resultrpr=$this->ModeloCatalogos->getselectwherenbd2('rev_pres',array('rev'=>$rid));
            foreach ($resultrpr->result() as $itemrpr) {
                $this->ModeloCatalogos->Insertbd2('rev_ress',array('rev'=>$rid,'uid'=>$itemru->rid,'pid'=>$itemrpr->rid,'res'=>'','res_obs'=>''));
            }
        }
        echo $rev;

    }
    function rev_mod($codigo){
        $data['codigo']=$codigo;

        $result_revs=$this->ModeloCatalogos->getselectwherenbd2('brdu_revs',array('sucursal'=>$this->sess_suc,'rev'=>$codigo));
        $result_revs_mum=$result_revs->num_rows();
        $result_revs_r=$result_revs->row();
        $data['result_revs']=$result_revs_r;
        $data['result_runis']=$this->ModeloCatalogos->getselectwherenbd2('rev_unis',array('rev'=>$result_revs_r->rid));
        

        $data['result_rpress']=$this->ModeloCatalogos->getselectwherenbd2('rev_pres',array('rev'=>$result_revs_r->rid));
        $data['rrevs_rid']=$result_revs_r->rid;

        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('brdu/rev_mod',$data);
        $this->load->view('theme/footer');
        $this->load->view('brdu/rev_modjs');
    }
    function rev_gua2(){
        $params = $this->input->post();
        $rid=$params['formrid'];
        $savefin=$params['savefin'];
        unset($params['formrid']);
        unset($params['savefin']);

        $resultrpr=$this->ModeloCatalogos->getselectwherenbd2('rev_ress',array('rev'=>$rid));

        foreach ($resultrpr->result() as $item) {
            $rrid=$item->rid;
            $rrev = $params['rr_'.$rrid];
            $robs = $params['ro_'.$rrid];
            $this->ModeloCatalogos->updateCatalogobd2('rev_ress',array('res'=>$rrev,'res_obs'=>$robs),array('rid'=>$rrid));
        }
        if ($savefin==1) {
            $this->ModeloCatalogos->updateCatalogobd2('brdu_revs',array('rev_est'=>'fin'),array('rid'=>$rid));
        }
    }
    function rev_ver($codigo){
        $data['codigo']=$codigo;

        $result_revs=$this->ModeloCatalogos->getselectwherenbd2('brdu_revs',array('sucursal'=>$this->sess_suc,'rev'=>$codigo));
        $result_revs_mum=$result_revs->num_rows();
        $result_revs_r=$result_revs->row();
        $data['result_revs']=$result_revs_r;
        $data['result_runis']=$this->ModeloCatalogos->getselectwherenbd2('rev_unis',array('rev'=>$result_revs_r->rid));
        

        $data['result_rpress']=$this->ModeloCatalogos->getselectwherenbd2('rev_pres',array('rev'=>$result_revs_r->rid));
        $data['rrevs_rid']=$result_revs_r->rid;

        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('brdu/rev_mod2',$data);
        $this->load->view('theme/footer');
        $this->load->view('brdu/rev_modjs');
    }
    function delete(){
        $params = $this->input->post();
        $rid = $params['codigo'];
        $this->ModeloCatalogos->getdeletewherenbd2('brdu_revs',array('rid'=>$rid));
        $this->ModeloCatalogos->getdeletewherenbd2('rev_pres',array('rev'=>$rid));
        $this->ModeloCatalogos->getdeletewherenbd2('rev_unis',array('rev'=>$rid));
        $this->ModeloCatalogos->getdeletewherenbd2('rev_ress',array('rev'=>$rid));
    }




}