<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Bitacora extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloBitacora');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal = date('dmYHis');
        $this->fechahoy = date('Y-m-d');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['resultsuc']=$this->ModeloCatalogos->genSelect('coe_sucs');
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('bitacora/list',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('bitacora/listjs');
  	}
    public function getlista() {
        $params = $this->input->post();
        //$params['sucursal']=$this->sess_suc;
        $getdata = $this->ModeloBitacora->getlist($params);
        $totaldata= $this->ModeloBitacora->getlist_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }



}