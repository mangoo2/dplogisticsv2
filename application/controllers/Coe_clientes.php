<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Coe_clientes extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloClientes');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['resultsuc']=$this->ModeloCatalogos->genSelect('coe_sucs');
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_clientes/lista',$data);
        $this->load->view('theme/footer'); 
 
  	}
    function clientes($codigo){
        if($codigo=='xxxxxxxxxx'){
            $data['suc_nombre']='';
    }else{
        $resultsuc=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$codigo));
        $resultsuc=$resultsuc->row();
        $data['resultsuc']=$resultsuc;
        $data['suc_nombre']=$resultsuc->suc_nombre;
    }
        
        $data['codigo']=$codigo;

        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_clientes/list',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('coe_clientes/listjs');
    }
    public function getlista() {
        $params = $this->input->post();
        //$params['sucursal']=$this->sess_suc;
        $getdata = $this->ModeloClientes->getlist($params);
        $totaldata= $this->ModeloClientes->getlist_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

 





}