<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require 'vendor/autoload.php';//php 7.1 como minimo
//use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;

class Coe_guias extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->fechalargac   = date('Y-m-d_His');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['resultsuc']=$this->ModeloCatalogos->genSelect('coe_sucs');
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_guias/lista',$data);
        $this->load->view('theme/footer');
        $this->load->view('coe_guias/listajs');
        
  	}
    function suc($codigo){
        $data['codigo']=$codigo;
        if($codigo=='xxx'){
            $codigo=$this->session->userdata('sess_suc');
            $data['codigo']=$codigo;
            $data['botonatras']=0;
        }else{
            $data['botonatras']=1;
        }
        $data['sess_suc']=$this->sess_suc;
        $resultsuc=$this->ModeloCatalogos->getselectwheren_o3('coe_sucs',array('sucursal'=>$codigo));
        $data['resultsuc']=$resultsuc->row();
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_guias/listasuc',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('theme/script_datatable');
        $this->load->view('coe_guias/listasucjs');
    }
    function cons($codigo){
        $data['codigo']=$codigo;
        if($codigo=='xxx'){
            $codigo=$this->session->userdata('sess_suc');
            $data['codigo']=$codigo;
            $data['botonatras']=0;
        }else{
            $data['botonatras']=1;
        }
        $data['sess_suc']=$this->sess_suc;
        $resultsuc=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$codigo));
        $data['resultsuc']=$resultsuc->row();
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('coe_guias/listasucsuc',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('theme/script_datatable');
        $this->load->view('coe_guias/listasucsucjs');
    }
    function cons_reporte($codigo){
        if($codigo=='xxx'){
            $codigo=$this->session->userdata('sess_suc');
        }
        $sess_suc=$this->sess_suc;
        $resultsuc=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('sucursal'=>$codigo));
        $resultsuc=$resultsuc->row();
        //===========================================
            if( isset($_GET["rep_fecini"]) ){
                $rep_fecini=$_GET["rep_fecini"];
            }else{
                $rep_fecini=date('Y-m-d');
            }
            if( isset($_GET["rep_fecfin"]) ){
                $rep_fecfin=$_GET["rep_fecfin"];
            }else{
                $rep_fecfin=date('Y-m-d');
            }
            $trs='';$trs_array=array();
            $x=0;
            $trsSC='';$xSC=0;
            $efeTot=0;$creTot=0;
            $efeTotSC=0;$creTotSC=0;
            $tarTotSC=0;

            $efetarTotSC=0;$tarTot=0;$efetarTot=0;
        //==================================la estructura de esta seccion debera de ser igual a la que se encuentra en la vista listsucsuc.php=========
            //$resG=$this->ModeloCatalogos->getselectwherenlimirorderby('guias',array('sucursal'=>$codigo),'rid','DESC',100);
            //$resG=$this->ModeloCatalogos->list_coe_guias($codigo);
            $resG=$this->ModeloCatalogos->list_coe_guias_fechas($codigo,$rep_fecini,$rep_fecfin);
            foreach ($resG->result() as $itemrg) {
                $fecha=date("d/m/Y", strtotime($itemrg->fecha)).' <br> '.date("h:i:s A", strtotime($itemrg->fecha));
                //================================================ cuando se solucione list_coe_guias esta se quitara
                //$resG_rem_con=$this->ModeloCatalogos->getselectwheren('guia_rycs',array('guia'=>$itemrg->rid));
                //$resG_rem_con=$resG_rem_con->row();
                $rem = $itemrg->rem.'<br>Dir.: '.$itemrg->rem_dir.' Col.: '.$itemrg->rem_col.' Ciu.: '.$itemrg->rem_ciu.' Tel.: '.$itemrg->rem_tel.' C.P.: '.$itemrg->rem_cp;
                $con = $itemrg->con.'<br>Dir.: '.$itemrg->con_dir.' Col.: '.$itemrg->con_col.' Ciu.: '.$itemrg->con_ciu.' Tel.: '.$itemrg->con_tel.' C.P.: '.$itemrg->con_cp;
                if($itemrg->tar_ce>0){ $tarce=$itemrg->tar_cen.'<br />'.$itemrg->tar_ce;}else{ $tarce=0.0;}
                //=====================================

                if($itemrg->corte!=''){
                    //==============================
                        if($itemrg->formaf=='efe'){
                            if($itemrg->importe2>0){
                                $efeTot+=$itemrg->importe1;
                            }else{
                                $efeTot+=$itemrg->importe;
                            }
                            
                            $formal='Efectivo';
                        }elseif($itemrg->formaf=='cre'){
                            $creTot+=$itemrg->importe;
                            $formal='Crédito';
                        }elseif($itemrg->formaf=='tar'){
                            
                               $tarTot+=$itemrg->importe; 
                            
                            
                            $formal='Tarjeta';
                        }elseif($itemrg->formaf=='efetar'){
                            $efetarTot+=$itemrg->importe;
                            $formal='Efectivo Tarjeta';
                        }else{
                            $formal='';
                        }
                        if($itemrg->importe2>0){
                            $formal.=' Tarjeta';
                            $tarTot+=$itemrg->importe2;
                        }
                    //==============================
                    $x++;
                    $trs.='<tr>
                                <td>'.$x.'<!--('.$itemrg->rid.')--></td><td>'.$fecha.'</td><td>'.$itemrg->emp_as.'</td><td>'.$itemrg->folio.'</td><td>'.$itemrg->guia_as.'</td>
                                <td>'.$itemrg->origen.'</td><td>'.$itemrg->destino.'</td><td>'.str_replace(',',', ',$itemrg->paq_des).'</td><td>'.$rem.'</td><td>'.$con.'</td>
                                <td>'.$itemrg->paqs.'</td><td>'.$itemrg->peso.'</td><td>'.$itemrg->volumen.'</td>
                                <td>'.$itemrg->paquetes.'</td>
                                <td>'.$itemrg->clasif.'</td>

                                <td>'.$itemrg->tarifa.'</td>
                                <td class="ce">'.$tarce.'</td><td class="cf">'.$itemrg->tar_cf.'</td>
                                <td>'.$itemrg->tar_pov.'</td><td>'.$itemrg->tar_fer.'</td><td>'.$itemrg->tar_cpc.'</td><td>'.$itemrg->tar_sub.'</td><td>'.$itemrg->tar_iva.'</td><td>'.number_format(floatval($itemrg->tar_tot),2).'</td><td>'.$formal.'</td>
                                <td>'.$itemrg->facturainfo.'</td>
                                <td>'.$itemrg->fechasol.'</td>
                                <td>'.$itemrg->fechafin.'</td>
                                <td>'.$itemrg->admin.'</td>
                                <td>'.$itemrg->cor_fecha.'</td>
                            </tr>';
                    $trs_array[]=array('col_a'=>$x,
                                    'col_b'=>$fecha,
                                    'col_c'=>$itemrg->emp_as,
                                    'col_d'=>$itemrg->folio,
                                    'col_e'=>$itemrg->guia_as,
                                    'col_f'=>$itemrg->origen,
                                    'col_g'=>$itemrg->destino,
                                    'col_h'=>str_replace(',',', ',$itemrg->paq_des),
                                    'col_i'=>$rem,
                                    'col_j'=>$con,
                                    'col_k'=>$itemrg->paqs,
                                    'col_l'=>$itemrg->peso,
                                    'col_m'=>$itemrg->volumen,
                                    'col_n'=>$itemrg->paquetes,
                                    'col_o'=>$itemrg->clasif,
                                    'col_p'=>$itemrg->tarifa,
                                    'col_q'=>$tarce,
                                    'col_r'=>$itemrg->tar_cf,
                                    'col_s'=>$itemrg->tar_pov,
                                    'col_t'=>$itemrg->tar_fer,
                                    'col_u'=>$itemrg->tar_cpc,
                                    'col_v'=>$itemrg->tar_sub,
                                    'col_w'=>$itemrg->tar_iva,
                                    'col_x'=>$itemrg->tar_tot,
                                    'col_y'=>$formal,
                                    'col_z'=>$itemrg->facturainfo,
                                    'col_aa'=>$itemrg->fechasol,
                                    'col_ab'=>$itemrg->fechafin,
                                    'col_ac'=>$itemrg->admin,
                                    'col_ad'=>$itemrg->cor_fecha
                                    );
                }else{
                    //==============================
                        if($itemrg->formaf=='efe'){
                            $efeTotSC+=$itemrg->importe;
                            $formal='Efectivo';
                        }elseif($itemrg->formaf=='cre'){
                            $creTotSC+=$itemrg->importe;
                            $formal='Crédito';
                        }elseif($itemrg->formaf=='tar'){
                            $tarTotSC+=$itemrg->importe;
                            $formal='Tarjeta';
                        }elseif($itemrg->formaf=='efetar'){
                            $efetarTotSC+=$itemrg->importe;
                            $formal='Efectivo Tarjeta';
                        }else{
                            $formal='';
                        }
                    //==============================
                    $xSC++;
                    $trsSC.='<tr>
                                <td>'.$xSC.'<!--('.$itemrg->rid.')--></td><td>'.$fecha.'</td><td>'.$itemrg->emp_as.'</td><td>'.$itemrg->folio.'</td><td>'.$itemrg->guia_as.'</td>
                                <td>'.$itemrg->origen.'</td><td>'.$itemrg->destino.'</td><td>'.str_replace(',',', ',$itemrg->paq_des).'</td><td>'.$rem.'</td><td>'.$con.'</td>
                                <td>'.$itemrg->paqs.'</td><td>'.$itemrg->peso.'</td><td>'.$itemrg->volumen.'</td>
                                <td>'.$itemrg->paquetes.'</td><td>'.$itemrg->clasif.'</td><td>'.$itemrg->tarifa.'</td>
                                <td class="ce">'.$tarce.'</td><td class="cf">'.$itemrg->tar_cf.'</td>
                                <td>'.$itemrg->tar_pov.'</td><td>'.$itemrg->tar_fer.'</td><td>'.$itemrg->tar_cpc.'</td><td>'.$itemrg->tar_sub.'</td><td>'.$itemrg->tar_iva.'</td><td>'.number_format($itemrg->tar_tot,2).'</td><td>'.$formal.'</td>
                                <td>'.$itemrg->facturainfo.'</td>
                                <td>'.$itemrg->fechasol.'</td>
                                <td>'.$itemrg->fechafin.'</td>
                                <td>'.$itemrg->admin.'</td>
                                <td>'.$itemrg->cor_fecha.'</td>
                            </tr>';
                }
            }
        //===========================================
            $spreadsheet = new Spreadsheet();
            $row_h=0;
            //if($row_h==0){
            $sheet = $spreadsheet->getActiveSheet();//esto solo cuando es una unica hoja
            //}else{
                //$spreadsheet->createSheet(); // esto es cuando es mas de una hoja con el $row_h consecutivos
                //$sheet = $spreadsheet->setActiveSheetIndex($row_h); // esto es cuando es mas de una hoja con el $row_h consecutivos
            //}
            //===========================================
                    /*
                    $sheet->setCellValue('A1', '#');
                    $sheet->setCellValue('B1', '');
                    $sheet->setCellValue('C1', '');
                    $sheet->setCellValue('D1', '');
                    $sheet->setCellValue('E1', '');
                    $sheet->setCellValue('F1', '');
                    $sheet->setCellValue('G1', '');
                    $sheet->setCellValue('H1', '');*/
                    $sheet->setCellValue('I1', 'EFECTIVO');
                    $sheet->setCellValue('J1', 'CREDITO');
                    $sheet->setCellValue('K1', 'TARJETA');
                    $sheet->setCellValue('L1', 'TOTAL');
                    /*
                    
                    $sheet->setCellValue('M1', '');
                    $sheet->setCellValue('N1', '');
                    $sheet->setCellValue('O1', '');
                    $sheet->setCellValue('P1', '');
                    */

                    $sheet->setCellValue('A2', '');
                    $sheet->setCellValue('B2', '');
                    $sheet->setCellValue('C2', 'ESTADISTICAS DE GUIAS USADAS');
                    $sheet->setCellValue('D2', '');
                    $sheet->setCellValue('E2', '');
                    $sheet->setCellValue('F2', '');
                    $sheet->setCellValue('G2', '');
                    $sheet->setCellValue('H2', 'Guiás finalizadas');
                    $sheet->setCellValue('I2', '$'.number_format($efeTot,2));
                    $sheet->setCellValue('J2', '$'.number_format($creTot,2));
                    $sheet->setCellValue('K2', '$'.number_format($tarTot,2));
                    $sheet->setCellValue('L2', '$'.number_format($efeTot+$creTot+$tarTot,2));
                    $sheet->setCellValue('M2', '');
                    $sheet->setCellValue('N2', '');
                    $sheet->setCellValue('O2', '');
                    $sheet->setCellValue('P2', '');


                    $sheet->setCellValue('A3', '');
                    $sheet->setCellValue('B3', '');
                    $sheet->setCellValue('C3', 'Fecha');
                    $sheet->setCellValue('D3', $rep_fecini.' al '.$rep_fecfin);
                    $sheet->setCellValue('E3', '');
                    $sheet->setCellValue('F3', '');
                    $sheet->setCellValue('G3', '');
                    $sheet->setCellValue('H3', 'Guiás sin corte de caja');
                    $sheet->setCellValue('I3', '$'.number_format($efeTotSC,2));
                    $sheet->setCellValue('J3', '$'.number_format($creTotSC,2));
                    $sheet->setCellValue('K3', '$'.number_format($tarTotSC,2));
                    $sheet->setCellValue('L3', '$'.number_format($efeTotSC+$creTotSC+$tarTotSC,2));
                    $sheet->setCellValue('M3', '');
                    $sheet->setCellValue('N3', '');
                    $sheet->setCellValue('O3', '');
                    $sheet->setCellValue('P3', '');

                    $sheet->setCellValue('A4', '');
                    $sheet->setCellValue('B4', '');
                    $sheet->setCellValue('C4', 'Sucursal');
                    $sheet->setCellValue('D4', $resultsuc->suc_nombre);
                    $sheet->setCellValue('E4', '');
                    $sheet->setCellValue('F4', '');
                    $sheet->setCellValue('G4', '');
                    $sheet->setCellValue('H4', 'Total');
                    $sheet->setCellValue('I4', '$'.number_format($efeTot+$efeTotSC,2));
                    $sheet->setCellValue('J4', '$'.number_format($creTot+$creTotSC,2));
                    $sheet->setCellValue('K4', '$'.number_format($tarTot+$tarTotSC,2));
                    $sheet->setCellValue('L4', '$'.number_format(($efeTot+$creTot)+($efeTotSC+$creTotSC)+($tarTot+$tarTotSC),2));
                    $sheet->setCellValue('M4', '');
                    $sheet->setCellValue('N4', '');
                    $sheet->setCellValue('O4', '');
                    $sheet->setCellValue('P4', '');
                    $rc=6;
                //===========================================
                                                                                       

                    $sheet->setCellValue('A'.$rc, '#');
                    $sheet->setCellValue('B'.$rc, 'Fecha');
                    $sheet->setCellValue('C'.$rc, 'Empresa');
                    $sheet->setCellValue('D'.$rc, 'Folio');
                    $sheet->setCellValue('E'.$rc, 'Guía');
                    $sheet->setCellValue('F'.$rc, 'Origen');
                    $sheet->setCellValue('G'.$rc, 'Destino');
                    $sheet->setCellValue('H'.$rc, 'Descripción');
                    $sheet->setCellValue('I'.$rc, 'Remitente');
                    $sheet->setCellValue('J'.$rc, 'Consignatario');
                    $sheet->setCellValue('K'.$rc, 'Paquetes');
                    $sheet->setCellValue('L'.$rc, 'Peso');
                    $sheet->setCellValue('M'.$rc, 'Volumen');
                    $sheet->setCellValue('N'.$rc, 'Detalles');
                    $sheet->setCellValue('O'.$rc, 'Product_type');
                    $sheet->setCellValue('P'.$rc, 'Ck/U');
                    $sheet->setCellValue('Q'.$rc, 'CE');
                    $sheet->setCellValue('R'.$rc, 'CF');
                    $sheet->setCellValue('S'.$rc, 'Pov');
                    $sheet->setCellValue('T'.$rc, 'FER');
                    $sheet->setCellValue('U'.$rc, 'CpC');
                    $sheet->setCellValue('V'.$rc, 'SUB');
                    $sheet->setCellValue('W'.$rc, 'IVA');
                    $sheet->setCellValue('X'.$rc, 'TOTAL');
                    $sheet->setCellValue('Y'.$rc, 'FDP');
                    $sheet->setCellValue('Z'.$rc, 'RFC');
                    $sheet->setCellValue('AA'.$rc, 'Solicitada');
                    $sheet->setCellValue('AB'.$rc, 'Procesada');
                    $sheet->setCellValue('AC'.$rc, 'ADMIN');
                    $sheet->setCellValue('AD'.$rc, 'CORTE');


                    foreach ($trs_array as $item_arr) {
                        //var_dump($item_arr);
                        $rc++;
                        $sheet->setCellValue('A'.$rc, $item_arr['col_a']);
                        $sheet->setCellValue('B'.$rc, $item_arr['col_b']);
                        $sheet->setCellValue('C'.$rc, $item_arr['col_c']);
                        $sheet->setCellValue('D'.$rc, $item_arr['col_d']);
                        $sheet->setCellValue('E'.$rc, $item_arr['col_e']);
                        $sheet->setCellValue('F'.$rc, $item_arr['col_f']);
                        $sheet->setCellValue('G'.$rc, $item_arr['col_g']);
                        $sheet->setCellValue('H'.$rc, $item_arr['col_h']);
                        $sheet->setCellValue('I'.$rc, $item_arr['col_i']);
                        $sheet->setCellValue('J'.$rc, $item_arr['col_j']);
                        $sheet->setCellValue('K'.$rc, $item_arr['col_k']);
                        $sheet->setCellValue('L'.$rc, $item_arr['col_l']);
                        $sheet->setCellValue('M'.$rc, $item_arr['col_m']);
                        $sheet->setCellValue('N'.$rc, $item_arr['col_n']);
                        $sheet->setCellValue('O'.$rc, $item_arr['col_o']);
                        $sheet->setCellValue('P'.$rc, $item_arr['col_p']);
                        $sheet->setCellValue('Q'.$rc, $item_arr['col_q']);
                        $sheet->setCellValue('R'.$rc, $item_arr['col_r']);
                        $sheet->setCellValue('S'.$rc, $item_arr['col_s']);
                        $sheet->setCellValue('T'.$rc, $item_arr['col_t']);
                        $sheet->setCellValue('U'.$rc, $item_arr['col_u']);
                        $sheet->setCellValue('V'.$rc, $item_arr['col_v']);
                        $sheet->setCellValue('W'.$rc, $item_arr['col_w']);
                        $sheet->setCellValue('X'.$rc, $item_arr['col_x']);
                        $sheet->setCellValue('Y'.$rc, $item_arr['col_y']);
                        $sheet->setCellValue('Z'.$rc, $item_arr['col_z']);
                        $sheet->setCellValue('AA'.$rc, $item_arr['col_aa']);
                        $sheet->setCellValue('AB'.$rc, $item_arr['col_ab']);
                        $sheet->setCellValue('AC'.$rc, $item_arr['col_ac']);
                        $sheet->setCellValue('AD'.$rc, $item_arr['col_ad']);
                    }

                    $sheet->getStyle('A6:AD6')->applyFromArray([
                        'font' => [
                            'bold' => true, // Negritas
                        ],
                        'fill' => [
                            'fillType' => Fill::FILL_SOLID,
                            'startColor' => [
                                'argb' => 'ececec', 
                            ],
                        ],
                    ]);
                    $sheet->getStyle('C2:C4')->applyFromArray([
                        'font' => [
                            'bold' => true, // Negritas
                        ],
                        'fill' => [
                            'fillType' => Fill::FILL_SOLID,
                            'startColor' => [
                                'argb' => 'ececec', 
                            ],
                        ],
                    ]);
                    $sheet->getStyle('H2:H4')->applyFromArray([
                        'font' => [
                            'bold' => true, // Negritas
                        ],
                        'fill' => [
                            'fillType' => Fill::FILL_SOLID,
                            'startColor' => [
                                'argb' => 'ececec', 
                            ],
                        ],
                    ]);
                    $sheet->getStyle('I1:L1')->applyFromArray([
                        'font' => [
                            'bold' => true, // Negritas
                        ],
                        'fill' => [
                            'fillType' => Fill::FILL_SOLID,
                            'startColor' => [
                                'argb' => 'ececec', 
                            ],
                        ],
                    ]);
                    

                //===========================================

                    $writer = new Xlsx($spreadsheet);
                $urlarchivo0='fichero_reporte/Reporte_guias_'.$this->fechalargac.'.xlsx';
                $urlarchivo=FCPATH.$urlarchivo0;
                $writer->save($urlarchivo);

                redirect(base_url().$urlarchivo0); 
    }





}