<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Conf_emps extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['sess_suc']=$this->sess_suc;
        $data['resultemp']=$this->ModeloCatalogos->getselectwheren_orderby('coe_emps',array('rid >'=>0),'emp_pos','ASC');
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('conf_emps/lista',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('conf_emps/listajs');
 
  	}
    function saveform(){
        $params = $this->input->post();
        $rid=$params['rid'];
        unset($params['rid']);
        $this->ModeloCatalogos->updateCatalogo('coe_emps',$params,array('rid'=>$rid));
    }

}