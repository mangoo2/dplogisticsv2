<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';//php 7.1 como minimo
use PhpOffice\PhpSpreadsheet\IOFactory;

class Vuelos extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloClientes');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modelovuelos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal = date('dmYHis');
        $this->fechahoy = date('Y-m-d');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        $data['fecha']=$this->fechahoy;
        $data['result_o'] = $this->Modelovuelos->group_origen();
        $data['result_d'] = $this->Modelovuelos->group_destino();
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');
        $this->load->view('vuelos/list',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('vuelos/listjs');
  	}
    function prueba(){
        $file=FCPATH.'file_vuelos/malla.xlsx';
        $documento = IOFactory::load($file);

        $totalHojas = $documento->getSheetCount();

        //for($indiceHoja=0; $indiceHoja<$totalHojas; $indiceHoja++){
          //  $hojaActual = $documento->getsheet($indiceHoja);
              $hojaActual = $documento->getsheet(0);
              $numeroFilas = $hojaActual->getHighestDataRow();
              $letra = $hojaActual->getHighestColumn();
              $html='<table border="1">';
              for($indiceFila = 2; $indiceFila<=$numeroFilas; $indiceFila++){
                $valor1=$hojaActual->GetCellByColumnAndRow('1',$indiceFila);
                $valor2=$hojaActual->GetCellByColumnAndRow('2',$indiceFila);
                $valor3=$hojaActual->GetCellByColumnAndRow('3',$indiceFila);
                $valor4=$hojaActual->GetCellByColumnAndRow('4',$indiceFila);
                
                $valor5=$hojaActual->getCell('E'.$indiceFila)->getFormattedValue();
                if(strlen($valor5)<=5){
                    $valor5=$valor5.':00';
                }
                $valor6=$hojaActual->getCell('F'.$indiceFila)->getFormattedValue();
                if(strlen($valor6)<=5){
                    $valor6=$valor6.':00';
                }


                $valor7=$hojaActual->getCell('G'.$indiceFila)->getValue();
                $valor7 = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($valor7);
                $valor7=date('Y-m-d',strtotime("+1 day",$valor7));



                $html.='<tr><td>'.$valor1.'</td><td>'.$valor2.'</td><td>'.$valor3.'</td><td>'.$valor4.'</td><td>'.$valor5.'</td><td>'.$valor6.'</td><td>'.$valor7.'</td></tr>';
              }
              $html.='</table>';
              echo $html;
        //}
    }
    function inserupdate(){
        $DIR_SUC=FCPATH.'file_vuelos';
        $config['upload_path']          = $DIR_SUC;
        $config['allowed_types']        = 'xlsx';
        $config['max_size']             = 5000;
        $file_names=date('YmdGis');
        $config['file_name']=$file_names;       
        $output = [];
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('documento')){
            $data = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($data));                    
        }else{
             $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension

            $this->procesardocumento($file_name);

            $data = array('upload_data' => $this->upload->data());
            //$output = [];
            //log_message('error', json_encode($data));
        }
        echo json_encode($output);
    }
    function procesardocumento($file_name){
        log_message('error', $file_name);
        $dataeqarrayinsert=array();


        $file=FCPATH.'file_vuelos/'.$file_name;
        $documento = IOFactory::load($file);

        $totalHojas = $documento->getSheetCount();

        //for($indiceHoja=0; $indiceHoja<$totalHojas; $indiceHoja++){
          //  $hojaActual = $documento->getsheet($indiceHoja);
              $hojaActual = $documento->getsheet(0);
              $numeroFilas = $hojaActual->getHighestDataRow();
              $letra = $hojaActual->getHighestColumn();
              $html='<table border="1">';
              for($indiceFila = 2; $indiceFila<=$numeroFilas; $indiceFila++){
                $valor1=$hojaActual->GetCellByColumnAndRow('1',$indiceFila);
                $valor2=$hojaActual->GetCellByColumnAndRow('2',$indiceFila);
                $valor3=$hojaActual->GetCellByColumnAndRow('3',$indiceFila);
                $valor4=$hojaActual->GetCellByColumnAndRow('4',$indiceFila);
                
                $valor5=$hojaActual->getCell('E'.$indiceFila)->getFormattedValue();
                if(strlen($valor5)<=5){
                    $valor5=$valor5.':00';
                }
                $valor6=$hojaActual->getCell('F'.$indiceFila)->getFormattedValue();
                if(strlen($valor6)<=5){
                    $valor6=$valor6.':00';
                }


                $valor7=$hojaActual->getCell('G'.$indiceFila)->getValue();
                $valor7 = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($valor7);
                $valor7=date('Y-m-d',strtotime("+1 day",$valor7));

                $arraydata=array('origen'=>$valor1,'destino'=>$valor2,'aerolinea'=>$valor3,'vuelo'=>$valor4,'h_salida'=>$valor5,'h_llegada'=>$valor6,'fecha'=>$valor7);
                $dataeqarrayinsert[]=$arraydata;

                
              }
                if(count($dataeqarrayinsert)>0){
                    $this->ModeloCatalogos->insert_batch('config_vuelos',$dataeqarrayinsert);
                }
              
        //}
    }
    function listado(){
        $data = $this->input->post();
        
        $inicio = date('Y-m-d',$data['start']);
        $fin = date('Y-m-d',$data['end']);
        $origen = $data['origen'];
        $destino = $data['destino'];
        $vuelo = $data['vuelo'];


        $result= $this->Modelovuelos->listvuelos($inicio,$fin,$origen,$destino,$vuelo);

        echo json_encode($result);
    }

    public function edit_vuelo()
    {   
        $id_r=$this->input->post('id_r');
        $salida=$this->input->post('salida');
        $llegada=$this->input->post('llegada');
        $fecha=$this->input->post('fecha');
        $arrayinfo = array('h_salida'=>$salida,'h_llegada'=>$llegada,'fecha'=>$fecha);
        $this->ModeloCatalogos->updateCatalogo('config_vuelos',$arrayinfo,array('id'=>$id_r));
    }

    public function cancelar_vuelo()
    {   
        $id_r=$this->input->post('id');
        $arrayinfo = array('activo'=>2);
        $this->ModeloCatalogos->updateCatalogo('config_vuelos',$arrayinfo,array('id'=>$id_r));
    }

    public function eliminar_vuelo()
    {   
        $id_r=$this->input->post('id');
        $arrayinfo = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('config_vuelos',$arrayinfo,array('id'=>$id_r));
    }




}