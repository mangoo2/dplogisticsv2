<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechal       = date('dmYHis');
        $this->fechahoy     = date('Y-m-d');
        $this->fechalarga   = date('Y-m-d H:i:s');
        $this->mesactual=date('m');
        $this->semanaactual=date('W');
        if($this->session->userdata('logeado')==true){
            $this->sess_suc=$this->session->userdata('sess_suc');
            $this->sess_usr=$this->session->userdata('sess_usr');
            $this->sess_sucname=$this->session->userdata('sess_suc_nombre');
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
        redirect('Sistema');
 
  	}
    function NumEstacion(){
        $search= $this->input->get('search');
        $strq = "SELECT * FROM f_c_estaciones WHERE activo=1 and (iata like '%$search%' or clave_identificacion like '%$search%')";
        $query = $this->db->query($strq);
        echo json_encode($query->result());
    }
    function Colonia(){
        $search= $this->input->get('search');
        $strq = "SELECT * FROM f_colonia WHERE activo=1 and (nombre like '%$search%' or c_CodigoPostal like '%$search%')";
        $query = $this->db->query($strq);
        echo json_encode($query->result());
    }
    function Localidad(){
        $search= $this->input->get('search');
        $strq = "SELECT * FROM f_c_localidad WHERE activo=1 and (c_Estado like '%$search%' or descripcion like '%$search%')";
        $query = $this->db->query($strq);
        echo json_encode($query->result());
    }
    function Municipio(){

        $search= $this->input->get('search');
        $strq = "SELECT * FROM f_c_municipio WHERE activo=1 and (c_Estado like '%$search%' or descripcion like '%$search%')";
        $query = $this->db->query($strq);
        echo json_encode($query->result());   
    }
    function Estado(){
        $search= $this->input->get('search');
        $strq = "SELECT * FROM f_c_estado WHERE activo=1 and (c_Pais like '%$search%' or c_Estado like '%$search%' or descripcion like '%$search%')";
        $query = $this->db->query($strq);
        echo json_encode($query->result());   
    }
    function Pais(){
        $search= $this->input->get('search');
        $strq = "SELECT * FROM f_c_pais WHERE activo=1 and (c_Pais like '%$search%' or descripcion like '%$search%')";
        $query = $this->db->query($strq);
        echo json_encode($query->result());   
    }
    function BienesTransp(){
        $search= $this->input->get('search');
        $strq = "SELECT * FROM f_servicios WHERE Clave like '%$search%' or nombre like '%$search%'";
        $query = $this->db->query($strq);
        echo json_encode($query->result());   
    }
    function Embalaje(){
        $search= $this->input->get('search');
        $strq = "SELECT * FROM f_c_embalaje WHERE clave like '%$search%' or descripcion like '%$search%'";
        $query = $this->db->query($strq);
        echo json_encode($query->result());   
    }
    function ClaveSTCC(){
        $search= $this->input->get('search');
        $strq = "SELECT * FROM f_c_claveprodstcc WHERE clave like '%$search%' or descripcion like '%$search%'";
        $query = $this->db->query($strq);
        echo json_encode($query->result());   
    }
    function CveMaterialPeligroso(){
        $search= $this->input->get('search');
        $strq = "SELECT * FROM f_c_materialpeligroso WHERE clave like '%$search%' or descripcion like '%$search%'";
        $query = $this->db->query($strq);
        echo json_encode($query->result());   
    }
    function FraccionArancelaria(){
        $search= $this->input->get('search');
        $strq = "SELECT * FROM f_c_fraccionarancelaria WHERE clave like '%$search%' or descripcion like '%$search%'";
        $query = $this->db->query($strq);
        echo json_encode($query->result());   
    }
    function PermSCT(){
        $search= $this->input->get('search');
        $strq = "SELECT * FROM f_c_tipopermiso WHERE clave like '%$search%' or descripcion like '%$search%'";
        $query = $this->db->query($strq);
        echo json_encode($query->result());   
    }
    function ConfigVehicular(){
        $search= $this->input->get('search');
        $strq = "SELECT * FROM f_c_configautotransporte WHERE clave like '%$search%' or descripcion like '%$search%'";
        $query = $this->db->query($strq);
        echo json_encode($query->result());   
    }
    function CodigoTransportista(){
        $search= $this->input->get('search');
        $strq = "SELECT * FROM f_c_codigotransporteaereo WHERE clave like '%$search%' or descripcion like '%$search%'";
        $query = $this->db->query($strq);
        echo json_encode($query->result());   
    }
    function obteneridod(){
        $params = $this->input->post();
        $id=$params['id'];
        $tipo=$params['tipo'];
        if($tipo==0){
             $strq = "SELECT * FROM origen WHERE num_estacion = '$id' and activo=1";
        }
        if($tipo==1){
            $strq = "SELECT * FROM destino WHERE num_estacion = '$id' and activo=1";
        }

        $query = $this->db->query($strq);

        echo json_encode($query->result()); 
    }
    function validarcot_guia(){
        $params = $this->input->post();
        $cot_guia=$params['cot_guia'];
        $total=0;
        $result1=$this->ModeloCatalogos->getselectwheren('doc_cotiza',array('cot_guia'=>$cot_guia));
        $total=$total+$result1->num_rows();

        $result2=$this->ModeloCatalogos->getselectwheren('guias',array('guia'=>$cot_guia));
        $total=$total+$result2->num_rows();

        $data=array('total'=>$total);
        echo json_encode($data); 
    }

 





}