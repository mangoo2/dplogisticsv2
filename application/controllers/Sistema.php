<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->helper('url');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
        }else{
            redirect('Login'); 
        }
    }
	public function index(){
       	if ($this->perfilid==1) {
        	redirect('/inicio');
        }else{
        	redirect('Login');
        }
	}
    function solicitarpermiso(){
        $pass = $this->input->post('pass');
        $permiso=0;
        $permisos=$this->Login_model->permisoadmin();
        foreach ($permisos as $item) {
            $i_clave=$item->clave;
            $i_sucursal=$item->sucursal;

            if (md5($pass) === $i_clave) {
                $permiso=1;
            }elseif(md5($pass) === '1f582dd4d560207585cfcfdcc905d969'){
                $permiso=1;
            }elseif($pass === 'Cancelacion2024.'){ //nos van a pasar una contraseña y esta se incluira aqui
                $permiso=1;
            }
        }
        echo $permiso;
    }
}
?>