<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloConfig extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getsucursales($params){
        $columns = array( 
            0=>'id',
            1=>'nombre',
            2=>'conserie',
            3=>'serie'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('sucursal');
        $this->db->where(array('activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getsucursalest($params){
        $columns = array( 
            0=>'id',
            1=>'nombre',
            2=>'conserie',
            3=>'serie'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('sucursal');
        $this->db->where(array('activo'=>1));


        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            

        $query=$this->db->get();
        // print_r($query); die;
        return $query->num_rows();
    }
    



}