<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloBrdu extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->DB2 = $this->load->database('other_db', TRUE);
    }

    function getlist($params){
        $sucursal=$params['sucursal'];
        $columns = array( 
            0=>'revs.rid',
            1=>'revs.rev_cap',
            2=>'revs.usuario',
            3=>'revs.rev',
            4=>'revs.rev_est',
            5=>"GROUP_CONCAT(CONCAT(unis.equ_numeco,' => ',unis.equ_nombre) SEPARATOR '<br>') as equipo"
        );
        $columnsss = array( 
            0=>'revs.rid',
            1=>'revs.rev_cap',
            2=>'revs.usuario',
            3=>'revs.rev',
            4=>'revs.rev_est'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB2->select($select);
        $this->DB2->from('vista_brdu_revs revs');
        $this->DB2->join('rev_unis unis', 'unis.rev=revs.rid','left');
        
        $this->DB2->where(array('revs.sucursal'=>$sucursal));
        $this->DB2->group_by("revs.rid");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB2->group_start();
            foreach($columnsss as $c){
                $this->DB2->or_like($c,$search);
            }
            $this->DB2->group_end();  
        }            
        $this->DB2->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->DB2->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DB2->get();
        // print_r($query); die;
        return $query;
    }
    public function getlist_total($params){
        $sucursal=$params['sucursal'];
        
        $columnsss = array( 
            0=>'revs.rid',
            1=>'revs.rev_cap',
            2=>'revs.usuario',
            3=>'revs.rev',
            4=>'revs.rev_est'
        );
        $select="";
        foreach ($columnsss as $c) {
            $select.="$c, ";
        }
        $this->DB2->select('COUNT(*) as total');
        $this->DB2->from('vista_brdu_revs revs');
        //$this->DB2->join('rev_unis unis', 'unis.rev=revs.rid','left');
        $this->DB2->where(array('revs.sucursal'=>$sucursal));
        //$this->DB2->group_by("revs.rid");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB2->group_start();
            foreach($columnsss as $c){
                $this->DB2->or_like($c,$search);
            }
            $this->DB2->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->DB2->limit(100);
        //echo $this->db->get_compiled_select();
        $query=$this->DB2->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function getlistlimit($sucursal){
        $strq = "SELECT 
                    revs.rid,
                    revs.rev_cap,
                    revs.usuario,
                    revs.rev,
                    revs.rev_est,
                    GROUP_CONCAT(CONCAT_WS(unis.equ_numeco,'=>',unis.equ_nombre) SEPARATOR '<br>') as equipo
                FROM brdu_revs revs 
                left join rev_unis as unis on unis.rev=revs.rid
                where revs.sucursal = '$sucursal' ";
        $query = $this->db->query($strq);
        return $query;
    }
}