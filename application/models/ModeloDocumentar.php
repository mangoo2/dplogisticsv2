<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloDocumentar extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->DBo2 = $this->load->database('other2_db', TRUE); 
        $this->DBo3 = $this->load->database('other3_db', TRUE); 
    }

    function getlist($params){
        $sucursal=$params['sucursal'];
        /*
            SELECT 
                doc.rid,doc.usuario, aer.aer_nom,concat(doc.cot_folio,' ',doc.cot_guia) as folioguia,cla.cla_nom,cla.cla_des,doc.cot_descrip,concat(ruto.rut_cla,' ',ruto.rut_nom) as rutao,
                concat(rutd.rut_cla,' ',rutd.rut_nom) as rutad,concat(cli.cli_nombre,' ',cli.cli_paterno,' ',cli.cli_materno) as cliente,
                doc.cot_feccap,doc.cotiza,doc.cot_estatus 
                FROM doc_cotiza as doc 
                INNER JOIN coe_aers as aer on aer.aero=doc.aero
                LEFT JOIN coe_clas as cla on cla.aero=doc.aero AND cla.clasif=doc.cot_clasif
                LEFT JOIN coe_ruts as ruto on ruto.ruta=doc.cot_rutori
                LEFT JOIN coe_ruts as rutd on rutd.ruta=doc.cot_rutdes
                LEFT JOIN cli_clis as cli on cli.cliente=doc.cot_remite
                WHERE doc.sucursal='03102016223335' ORDER BY rid DESC
        */
        $columns = array( 
            0=>"doc.rid",
            1=>"doc.cot_feccap",
            2=>"doc.usuario", 
            3=>"aer.aer_nom",
            4=>"concat(doc.cot_folio,' ',doc.cot_guia) as folioguia",
            5=>"concat(cla.cla_nom,' ',cla.cla_des) as clasifi",
            6=>"doc.cot_descrip",
            7=>"concat(ruto.rut_cla,' ',ruto.rut_nom) as rutao",
            8=>"concat(rutd.rut_cla,' ',rutd.rut_nom) as rutad",
            9=>"concat(cli.cli_nombre,' ',cli.cli_paterno,' ',cli.cli_materno) as cliente",
            10=>"doc.cot_estatus",
            11=>"doc.cotiza",
        );
        $columnsss = array( 
            0=>"doc.rid",
            1=>"doc.cot_feccap",
            2=>"doc.usuario", 
            3=>"aer.aer_nom",
            4=>"doc.cot_folio",
            5=>"cla.cla_nom",
            6=>"doc.cot_descrip",
            7=>"ruto.rut_cla",
            8=>"rutd.rut_cla",
            9=>"cli.cli_nombre",
            10=>"doc.cot_estatus",
            11=>"doc.cotiza",
            12=>"concat(doc.cot_folio,' ',doc.cot_guia)",
            13=>"concat(ruto.rut_cla,' ',ruto.rut_nom)",
            14=>"concat(rutd.rut_cla,' ',rutd.rut_nom)",
            15=>"concat(cli.cli_nombre,' ',cli.cli_paterno,' ',cli.cli_materno)",
            16=>"concat(cla.cla_nom,' ',cla.cla_des)",
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DBo2->select($select);
        $this->DBo2->from('doc_cotiza doc');
        $this->DBo2->join('coe_aers aer', 'aer.aero=doc.aero','left');
        $this->DBo2->join('coe_clas cla', 'cla.aero=doc.aero AND cla.clasif=doc.cot_clasif','left');
        $this->DBo2->join('coe_ruts ruto', 'ruto.ruta=doc.cot_rutori','left');
        $this->DBo2->join('coe_ruts rutd', 'rutd.ruta=doc.cot_rutdes','left');
        $this->DBo2->join('cli_clis cli', 'cli.cliente=doc.cot_remite','left');
        $this->DBo2->where(array('doc.sucursal'=>$sucursal));
        $this->DBo2->group_by("doc.rid");
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DBo2->group_start();
            foreach($columnsss as $c){
                $this->DBo2->or_like($c,$search);
            }
            $this->DBo2->group_end();  
        }            
        $this->DBo2->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->DBo2->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DBo2->get();
        // print_r($query); die;
        return $query;
    }
    public function getlist_total($params){
        $sucursal=$params['sucursal'];
        $columns = array( 
            0=>"doc.rid",
            1=>"doc.cot_feccap",
            2=>"doc.usuario", 
            3=>"aer.aer_nom",
            4=>"doc.cot_folio",
            5=>"cla.cla_nom",
            6=>"doc.cot_descrip",
            7=>"ruto.rut_cla",
            8=>"rutd.rut_cla",
            9=>"cli.cli_nombre",
            10=>"doc.cot_estatus",
            11=>"doc.cotiza",
            12=>"concat(doc.cot_folio,' ',doc.cot_guia)",
            13=>"concat(ruto.rut_cla,' ',ruto.rut_nom)",
            14=>"concat(rutd.rut_cla,' ',rutd.rut_nom)",
            15=>"concat(cli.cli_nombre,' ',cli.cli_paterno,' ',cli.cli_materno)",
            16=>"concat(cla.cla_nom,' ',cla.cla_des)",
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DBo2->select('COUNT(*) as total');
        $this->DBo2->from('doc_cotiza doc');
        $this->DBo2->join('coe_aers aer', 'aer.aero=doc.aero','left');
        $this->DBo2->join('coe_clas cla', 'cla.aero=doc.aero AND cla.clasif=doc.cot_clasif','left');
        $this->DBo2->join('coe_ruts ruto', 'ruto.ruta=doc.cot_rutori','left');
        $this->DBo2->join('coe_ruts rutd', 'rutd.ruta=doc.cot_rutdes','left');
        $this->DBo2->join('cli_clis cli', 'cli.cliente=doc.cot_remite','left');
        $this->DBo2->where(array('doc.sucursal'=>$sucursal));
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DBo2->group_start();
            foreach($columns as $c){
                $this->DBo2->or_like($c,$search);
            }
            $this->DBo2->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DBo2->get();
        // print_r($query); die;
        return $query->row()->total;
    }

    public function get_origen_like($search){
        $strq = "SELECT o.*,coe.rut_cla,coe.rut_nom,est.descripcion,est.iata
        from doc_cotiza AS c
        INNER JOIN origen AS o ON o.dpl=c.cot_rutori
        INNER JOIN coe_ruts AS coe ON coe.ruta=o.dpl
        left JOIN f_c_estaciones AS est ON est.clave_identificacion=o.num_estacion
        WHERE o.activo=1 AND c.cotiza='$search'";
        $query = $this->db->query($strq);
        return $query->result();
    }


    public function get_origen_domicilio_like($search){
        $strq = "SELECT o.*,coe.rut_cla,coe.rut_nom,
                    col.c_Colonia,col.c_CodigoPostal AS cp_c,col.nombre AS coloniatxt,
                    loc.descripcion AS localidadtxt,loc.c_Localidad,
                    mun.descripcion AS municipiotxt,mun.c_Municipio,
                    est.descripcion AS estadotxt,est.c_Estado,
                    pai.descripcion AS paistxt,pai.c_Pais
        from doc_cotiza AS c
        INNER JOIN domicilio_origen AS o ON o.dpl=c.cot_rutori
        INNER JOIN coe_ruts AS coe ON coe.ruta=o.dpl
        left JOIN f_colonia AS col ON col.id=o.colonia
        left JOIN f_c_localidad AS loc ON loc.id=o.localidad
        left JOIN f_c_municipio AS mun ON mun.id=o.municipio
        left JOIN f_c_estado AS est ON est.id=o.estado 
        left JOIN f_c_pais AS pai ON pai.id=o.pais
        WHERE o.activo=1 AND c.cotiza='$search'";
        $query = $this->db->query($strq);
        return $query->result();
    }


    public function get_destino_like($search){
        $strq = "SELECT o.*,coe.rut_cla,coe.rut_nom,est.descripcion,est.iata
        from doc_cotiza AS c
        INNER JOIN destino AS o ON o.dpl=c.cot_rutdes
        INNER JOIN coe_ruts AS coe ON coe.ruta=o.dpl
        left JOIN f_c_estaciones AS est ON est.clave_identificacion=o.num_estacion
        WHERE o.activo=1 AND c.cotiza='$search'";
        $query = $this->db->query($strq);
        return $query->result();
    }


    public function get_destino_domicilio_like($search){
        $strq = "SELECT o.*,coe.rut_cla,coe.rut_nom,
                    col.c_Colonia,col.c_CodigoPostal AS cp_c,col.nombre AS coloniatxt,
                    loc.descripcion AS localidadtxt,loc.c_Localidad,
                    mun.descripcion AS municipiotxt,mun.c_Municipio,
                    est.descripcion AS estadotxt,est.c_Estado,
                    pai.descripcion AS paistxt,pai.c_Pais
        from doc_cotiza AS c
        INNER JOIN domicilio_destino AS o ON o.dpl=c.cot_rutdes
        INNER JOIN coe_ruts AS coe ON coe.ruta=o.dpl
        left JOIN f_colonia AS col ON col.id=o.colonia
        left JOIN f_c_localidad AS loc ON loc.id=o.localidad
        left JOIN f_c_municipio AS mun ON mun.id=o.municipio
        left JOIN f_c_estado AS est ON est.id=o.estado 
        left JOIN f_c_pais AS pai ON pai.id=o.pais
        WHERE o.activo=1 AND c.cotiza='$search'";
        $query = $this->db->query($strq);
        return $query->result();
    }
    function get_info_guia_cotizacion($rid){
        $strq="SELECT 
            `doc`.`rid`, /* id guia */
            `doc`.`cot_feccap`, 
            `doc`.`usuario`, 
            `aer`.`aer_nom`,
            doc.cot_folio,
            doc.cot_guia, 
            concat(cla.cla_nom, ' ', cla.cla_des) as clasifi, 
            `doc`.`cot_descrip`, 
            
            concat(ruto.rut_cla, ' ', ruto.rut_nom) as rutao, 
            `doc`.`cot_rutori`,

            concat(rutd.rut_cla, ' ', rutd.rut_nom) as rutad, 
            `doc`.`cot_rutdes`,
            
            concat(cli.cli_nombre, ' ', `cli`.`cli_paterno`, ' ', cli.cli_materno) as cliente,
            `doc`.`cot_remite`,

            `doc`.`cot_estatus`, 
            `doc`.`cotiza`,/* id guia visual vinculado al file*/
            `doc`.`sucursal`/* sucursal*/,
            doc.sol_edit_delete_personal
        FROM `doc_cotiza` `doc`
        LEFT JOIN `coe_aers` `aer` ON `aer`.`aero`=`doc`.`aero`
        LEFT JOIN `coe_clas` `cla` ON `cla`.`aero`=`doc`.`aero` AND `cla`.`clasif`=`doc`.`cot_clasif`
        LEFT JOIN `coe_ruts` `ruto` ON `ruto`.`ruta`=`doc`.`cot_rutori`
        LEFT JOIN `coe_ruts` `rutd` ON `rutd`.`ruta`=`doc`.`cot_rutdes`
        LEFT JOIN `cli_clis` `cli` ON `cli`.`cliente`=`doc`.`cot_remite`
        WHERE  `doc`.`rid`='$rid'
        GROUP BY `doc`.`rid` limit 1";
        $query = $this->db->query($strq);
        return $query;
    }

    function getlistedicioneliminado($params){
        $sucursal=$params['sucursal'];
        $tipo = $params['tipo'];
        $columns = array( 
            0=>"doc.rid",
            1=>"doc.cot_feccap",
            2=>"doc.usuario", 
            3=>"doc.aer_nom",
            4=>"doc.cot_folio",
            5=>"doc.clasifi",
            6=>"doc.cot_descrip",
            7=>"doc.rutao",
            8=>"doc.rutad",
            9=>"doc.cliente",
            10=>"doc.cot_estatus",
            11=>"doc.cotiza",
            12=>"doc.cot_guia",
            13=>"doc.tipo_ed_de",
            14=>"doc.sol_edit_delete_personal",
            15=>"doc.personal_confirmo",
        );
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('bitacora_edicion_eliminacion doc');

        $this->db->where(array('doc.sucursal'=>$sucursal));
        if($tipo=='all'){

        }else{
            $this->db->where(array('doc.tipo_ed_de'=>$tipo));
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlistedicioneliminado_total($params){
        $sucursal=$params['sucursal'];
        $tipo = $params['tipo'];
        $columns = array( 
            0=>"doc.rid",
            1=>"doc.cot_feccap",
            2=>"doc.usuario", 
            3=>"doc.aer_nom",
            4=>"doc.cot_folio",
            5=>"doc.clasifi",
            6=>"doc.cot_descrip",
            7=>"doc.rutao",
            8=>"doc.rutad",
            9=>"doc.cliente",
            10=>"doc.cot_estatus",
            11=>"doc.cotiza",
            12=>"doc.cot_guia",
            13=>"doc.tipo_ed_de",
            14=>"doc.sol_edit_delete_personal",
            15=>"doc.personal_confirmo",
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('bitacora_edicion_eliminacion doc');
        $this->db->where(array('doc.sucursal'=>$sucursal));
        if($tipo=='all'){

        }else{
            $this->db->where(array('doc.tipo_ed_de'=>$tipo));
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }


}