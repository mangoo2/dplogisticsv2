<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelovuelos extends CI_Model {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->fechal = date('dmYHis');
        $this->fechalarga = date('Y-m-d H:i:s');
        $this->fecha = date('Y-m-d');
    }
    
    
    function group_origen(){
        $sql="SELECT origen FROM config_vuelos WHERE activo=1 GROUP BY origen ORDER BY origen ASC";
        $query=$this->db->query($sql);
        return $query;    
    }
    function group_destino(){
        $sql="SELECT destino FROM `config_vuelos` WHERE activo=1 GROUP BY destino ORDER BY destino ASC";
        $query=$this->db->query($sql);
        return $query;    
    }
    function listvuelos($inicio,$fin,$origen,$destino,$vuelo){
        if($origen!=''){
            $where_o=" and origen='$origen' ";
        }else{
            $where_o="";
        }
        if($destino!=''){
            $where_d=" and destino='$destino' ";
        }else{
            $where_d="";
        }
        if($vuelo!=''){
            $where_v=" and vuelo='$vuelo' ";
        }else{
            $where_v="";
        }
        $sql="SELECT * FROM config_vuelos WHERE activo>0 $where_o $where_d $where_v and fecha BETWEEN '$inicio' and '$fin' ";
        $query=$this->db->query($sql);
        return $query->result();    
    }
    function doc_group_origen(){
        $sql="SELECT origen FROM config_vuelos WHERE activo>0 and fecha>='$this->fecha' GROUP BY origen ORDER BY origen ASC";
        $query=$this->db->query($sql);
        return $query;    
    }
    function doc_group_destino(){
        $sql="SELECT destino FROM `config_vuelos` WHERE activo>0 and fecha>='$this->fecha' GROUP BY destino ORDER BY destino ASC";
        $query=$this->db->query($sql);
        return $query;    
    }

}