<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloRye extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function getlist($params){
        $sucursal=$params['sucursal'];
        $columns = array( 
            0=>'ser_fol',
            1=>'ser_fecini',
            2=>'ser_guia',
            3=>'ser_origen',
            4=>'ser_destino',
            5=>'ser_clis',
            6=>'ser_sers',
            7=>'ser_obs',
            8=>'ser_est',
            9=>'ser_finobs',
            10=>'rid',
            11=>'ser'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('ser_cad');
        
        $this->db->where(array('sucursal'=>$sucursal));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlist_total($params){
        $sucursal=$params['sucursal'];
        $columns = array( 
            0=>'ser_fol',
            1=>'ser_fecini',
            2=>'ser_guia',
            3=>'ser_origen',
            4=>'ser_destino',
            5=>'ser_clis',
            6=>'ser_sers',
            7=>'ser_obs',
            8=>'ser_est',
            9=>'ser_finobs',
            10=>'rid',
            11=>'ser'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('ser_cad');
        
        
        $this->db->where(array('sucursal'=>$sucursal));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function listclienteguias($sucursal,$cliente,$fecha,$tipo){
        if($tipo==1){
            $conficionw='<=';
        }else{
            $conficionw='>';
        }


        $strq = "SELECT 
                    gpags.rid,
                    gpags.emp,
                    gpags.guia,
                    gpags.fecini,
                    gpags.fecfin,
                    gpags.coms,
                    gui.folio,
                    gui.guia,
                    gui.importe
                FROM guia_pags as gpags 
                left join guias as gui on gui.rid=gpags.guia and gui.sucursal='$sucursal'
                WHERE 
                gpags.sucursal='$sucursal' and 
                gpags.cliente ='$cliente' and 
                gpags.forma='cre' and
                gpags.estatus='pdp' and
                gpags.fecfin $conficionw '$fecha'
                ";
        $query = $this->db->query($strq);
        return $query;
    }
    






}