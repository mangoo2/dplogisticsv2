<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloReqs extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function list_reqs_proceso($codigo) {
        $strq = "SELECT * FROM suc_reqs where sucursal='$codigo' and (req_estatus != 'Finalizado' AND req_estatus != 'Cancelado') ORDER BY req_fecini DESC";
        $query = $this->db->query($strq);
        return $query;
    }
    function list_reqs_ultimas($codigo) {
        $strq = "SELECT * FROM suc_reqs where sucursal='$codigo' and (req_estatus = 'Finalizado' or req_estatus = 'Cancelado') ORDER BY req_fecfin DESC limit 50";
        $query = $this->db->query($strq);
        return $query;
    }
    function list_reqs_ultimas_search($codigo,$fini,$ffin) {
        $strq = "SELECT * FROM suc_reqs where sucursal='$codigo' and (req_estatus = 'Finalizado' or req_estatus = 'Cancelado') and req_fecfin>='$fini 00:00:00' and req_fecfin<='$ffin 23:59:59'";
        $query = $this->db->query($strq);
        return $query;
    }
 
}