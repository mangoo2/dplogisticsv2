<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloOperadores extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_listado($params){
        $columns = array( 
            1=>'o.id',
			2=>'coe.rut_cla AS dpl',
			3=>'o.rfc_del_operador',
			4=>'o.no_licencia',
			5=>'o.operador',
			6=>'o.num_identificacion',
			7=>'o.residencia_fiscal',
			8=>'o.calle',
			9=>'es.descripcion',
			10=>'o.pais',
			11=>'o.codigo_postal',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('operadores o');
        $this->db->join('f_c_estado es', 'es.c_Estado=o.estado','left');
        $this->db->join('coe_ruts coe', 'coe.ruta=o.dpl','left');
        $this->db->where(array('o.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function get_listado_total($params){
        $columns = array( 
            1=>'o.id',
			2=>'coe.rut_cla AS dpl',
			3=>'o.rfc_del_operador',
			4=>'o.no_licencia',
			5=>'o.operador',
			6=>'o.num_identificacion',
			7=>'o.residencia_fiscal',
			8=>'o.calle',
			9=>'o.estado',
			10=>'o.pais',
			11=>'o.codigo_postal',
        );
        $this->db->select('COUNT(*) as total');
        $this->db->join('coe_ruts coe', 'coe.ruta=o.dpl','left');
        $this->db->from('operadores o');
        $this->db->where(array('o.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }
    
}