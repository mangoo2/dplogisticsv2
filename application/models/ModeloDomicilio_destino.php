<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloDomicilio_destino extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_listado($params){
        $columns = array( 
            1=>'o.id',
            2=>'coe.rut_cla AS dpl',
            3=>'o.calle',
            4=>'o.num_ext',
            5=>'o.num_int',
            6=>'col.nombre AS colonia',
            7=>'loc.descripcion AS localidad',
            8=>'o.referencia',
            9=>'mun.descripcion AS municipio',
            10=>'est.descripcion AS estado',
            11=>'pai.descripcion AS pais',
            12=>'o.codigo_postal',
        );

        $columnsy = array( 
            1=>'o.id',
            2=>'coe.rut_cla',
            3=>'o.calle',
            4=>'o.num_ext',
            5=>'o.num_int',
            6=>'col.nombre',
            7=>'loc.descripcion',
            8=>'o.referencia',
            9=>'mun.descripcion',
            10=>'est.descripcion',
            11=>'pai.descripcion',
            12=>'o.codigo_postal',
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('domicilio_destino o');
        $this->db->join('f_colonia col', 'col.id=o.colonia','left');
        $this->db->join('f_c_pais pai', 'pai.id=o.pais','left');
        $this->db->join('f_c_localidad loc', 'loc.id=o.localidad','left');
        $this->db->join('f_c_municipio mun', 'mun.id=o.municipio','left');
        $this->db->join('f_c_estado est', 'est.id=o.estado','left');
        $this->db->join('coe_ruts coe', 'coe.ruta=o.dpl','left');
        $this->db->where(array('o.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsy as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsy[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function get_listado_total($params){
        $columns = array( 
            1=>'o.id',
            2=>'o.dpl',
            3=>'o.calle',
            4=>'o.num_ext',
            5=>'o.num_int',
            6=>'o.colonia',
            7=>'o.referencia',
            8=>'o.municipio',
            9=>'o.estado',
            10=>'o.pais',
            11=>'o.codigo_postal',
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('domicilio_destino o');
        $this->db->join('f_colonia col', 'col.id=o.colonia','left');
        $this->db->join('f_c_pais pai', 'pai.id=o.pais','left');
        $this->db->join('f_c_localidad loc', 'loc.id=o.localidad','left');
        $this->db->join('f_c_municipio mun', 'mun.id=o.municipio','left');
        $this->db->join('f_c_estado est', 'est.id=o.estado','left');
        $this->db->join('coe_ruts coe', 'coe.ruta=o.dpl','left');
        $this->db->where(array('o.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }
    
}