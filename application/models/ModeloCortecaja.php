<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCortecaja extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function getlist($params){
        $sucursal=$params['sucursal'];
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $columns = array( 
            0=>'rid',
            1=>'cor_fecha',
            2=>'cor_efectivo',
            3=>'cor_credito',
            4=>'cor_com',
            5=>'cor_admnom',
            6=>'corte',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('suc_cors');
        $this->db->where(array('sucursal'=>$sucursal));
        if($finicio!=''){
            $this->db->where(array('cor_fecha >='=>$finicio.' 00:00:00'));
        }
        if($ffin!=''){
            $this->db->where(array('cor_fecha <='=>$ffin.' 23:59:59'));
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlist_total($params){
        $sucursal=$params['sucursal'];
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $columns = array( 
            0=>'rid',
            1=>'cor_fecha',
            2=>'cor_efectivo',
            3=>'cor_credito',
            4=>'cor_com',
            5=>'cor_admnom',
            6=>'corte',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('suc_cors');
        
        if($finicio!=''){
            $this->db->where(array('cor_fecha >='=>$finicio.' 00:00:00'));
        }
        if($ffin!=''){
            $this->db->where(array('cor_fecha <='=>$ffin.' 23:59:59'));
        }
        
        $this->db->where(array('sucursal'=>$sucursal));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function listipodoccorte($sucursal,$tipo,$estatus){
        $columns = array( 
            0=>"doc.rid",
            1=>"doc.cot_feccap",
            2=>"doc.usuario", 
            3=>"aer.aer_nom",
            4=>"doc.cot_folio",
            5=>"concat(cla.cla_nom,' ',cla.cla_des) as clasifi",
            6=>"doc.cot_descrip",
            7=>"concat(ruto.rut_cla,' ',ruto.rut_nom) as rutao",
            8=>"concat(rutd.rut_cla,' ',rutd.rut_nom) as rutad",
            9=>"concat(cli.cli_nombre,' ',cli.cli_paterno,' ',cli.cli_materno) as cliente",
            10=>"doc.cot_estatus",
            11=>"doc.cotiza",
            12=>"doc.cot_guia",
            13=>"doc.cot_importe",
            14=>"tar.tar_tot",
            15=>"doc.cot_cantidad",
            16=>"doc.banco_efectivo",
            17=>"doc.banco_credito",
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('doc_cotiza doc');
        $this->db->join('coe_aers aer', 'aer.aero=doc.aero','left');
        $this->db->join('coe_clas cla', 'cla.aero=doc.aero AND cla.clasif=doc.cot_clasif','left');
        $this->db->join('coe_ruts ruto', 'ruto.ruta=doc.cot_rutori','left');
        $this->db->join('coe_ruts rutd', 'rutd.ruta=doc.cot_rutdes','left');
        $this->db->join('cli_clis cli', 'cli.cliente=doc.cot_remite','left');
        $this->db->join('doc_tars tar', 'tar.cotiza=doc.cotiza','left');
        $this->db->where(array('doc.sucursal'=>$sucursal,'doc.cot_fdp'=>$tipo));
        $this->db->group_by("doc.rid");
        if($tipo!=''){
            $this->db->where(array('doc.cot_fdp'=>$tipo));
        }else{
            $this->db->where(array('doc.cot_fdp'=>''));
        }
        if($estatus==1){
           $this->db->where(array('doc.cot_folio !='=>'','doc.cot_conta'=>'si')); 
        }else{
            $this->db->where(array('doc.cot_folio'=>'','doc.cot_conta'=>'no'));
        }
        $this->db->order_by('doc.rid', 'DESC');

        $query=$this->db->get();

        return $query;
    }
    function comprobantesg($corte){
        $strq = "SELECT file_comprobante FROM `guias` WHERE corte='$corte' GROUP BY file_comprobante";
        $query = $this->db->query($strq);
        return $query;
    
    }






}