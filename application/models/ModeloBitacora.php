<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloBitacora extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function getlist($params){
        $sucursal=$params['suc'];
        $columns = array( 
            0=>'id',
            1=>"usuario",
            2=>'tipo',
            3=>'descripcion',
            4=>'reg'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('bitacora_movimientos');
        if($sucursal!='0'){
            $this->db->where(array('suc'=>$sucursal));
        }
        $this->db->where(array('activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlist_total($params){
        $sucursal=$params['suc'];
        $columns = array( 
            0=>'id',
            1=>"usuario",
            2=>'tipo',
            3=>'descripcion',
            4=>'reg'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('bitacora_movimientos');
        if($sucursal!='0'){
            $this->db->where(array('suc'=>$sucursal));
        }
        
        $this->db->where(array('activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
 
    






}