<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloAdmin extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    /*
        SELECT 
            doc.cotiza, 
            cli.cli_nombre,cli.cli_paterno,cli.cli_materno,
            sum(paqs.paq_pes),
            sum(doct.tar_tot),
            doc.cot_feccap 
            FROM doc_cotiza as doc 
            INNER JOIN cli_clis as cli on cli.cliente=doc.cot_remite 
            INNER JOIN doc_paqs as paqs on paqs.cotiza=doc.cotiza 
            INNER JOIN doc_tars as doct on doct.cotiza=doc.cotiza
    */
    function sql_topcli($params){
        $cliente=$params['cliente'];
        //$anio=$params['anio'];
        $fechai=$params['fechai'];
        $fechaf=$params['fechaf'];
        $sucursal =$params['sucursals'];
        $top=$params['top'];
        log_message('error','sucuarsal:'.$sucursal);
        if($sucursal=='0'){
            
            $where_suc='';
        }else{
            $where_suc="and doc.sucursal='$sucursal' ";
        }
        if($cliente>0){
            $where_cli="and cli.cliente='$cliente' ";
        }else{
            $where_cli='';
        }
        //log_message('error',$anio);
        //$anio = json_decode($anio);
        $consulta='';
        //====================================================
            //for ($i=0;$i<count($anio);$i++) {
                //$aniofor=$anio[$i];
                //$where_anio=" and doc.fecha >='$aniofor-01-01 00:00:00' and doc.fecha <='$aniofor-12-30 23:59:59' ";
                $where_anio=" and doc.fecha >='$fechai 00:00:00' and doc.fecha <='$fechaf 23:59:59' ";

                $consulta.="(SELECT
                    CONCAT(cli.cli_nombre,' ',cli.cli_paterno,' ',cli.cli_materno)  as cliente,
                    sum(doc.peso) paq_pes,
                    sum(doc.importe) tar_tot,
                    doc.fecha, DATE_FORMAT(doc.fecha, '%Y') fechayear,
                    cli.cliente as clienteid,
                    count(*) as folios,
                    cli.tarifa_baja,
                    cli.tarifa_especial
                from guias doc
                INNER JOIN cli_clis as cli on cli.cliente=doc.cliente
                where doc.rid>0
                    $where_suc
                    $where_cli
                    $where_anio
                    group by cli.cliente,fechayear
                    ORDER BY `paq_pes` DESC
                    limit $top) union";
            //}
            $consulta.="(SELECT '' as clienteid,'' as clientes,'' as paq_pes,'' as tar_tot,'' as fecha,0 as fechayear,0 as folios,0 as tarifa_baja, 0 as tarifa_especial)";
        //====================================================
        $strq="($consulta) as datos";
        return $strq;
    }
    function sql_topcli2($params){
        $cliente=$params['cliente'];
        $sucursal =$params['sucursals'];
        $fechai=$params['fechai'];
        $fechaf=$params['fechaf'];
        
        if($sucursal!=0){
            $where_suc="and doc.sucursal='$sucursal' ";
        }else{
            $where_suc='';
        }
        if($cliente>0){
            $where_suc="and cli.cliente='$cliente' ";
        }else{
            $where_suc='';
        }
        //log_message('error',$anio);
        //====================================================
                $where_anio=" and doc.fecha >='$fechai 00:00:00' and doc.fecha <='$fechaf 23:59:59' ";

                $consulta="(SELECT
                    CONCAT(cli.cli_nombre,' ',cli.cli_paterno,' ',cli.cli_materno)  as cliente,
                    sum(doc.peso) paq_pes,
                    sum(doc.importe) tar_tot,
                    doc.fecha, DATE_FORMAT(doc.fecha, '%Y') fechayear
                from guias doc
                INNER JOIN cli_clis as cli on cli.cliente=doc.cliente
                where doc.rid>0
                    $where_suc
                    $where_suc
                    $where_anio
                ) as datos";
            
        //====================================================
        return $consulta;
    }

    function getlistacli_chart($params){
        $sqlcli=$this->sql_topcli($params);
        $columns = array( 
            0=>'cliente',
            1=>"paq_pes",
            2=>'tar_tot',
            3=>'fechayear',
            4=>'clienteid',
            5=>'folios',
            6=>'tarifa_baja',
            7=>'tarifa_especial'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from($sqlcli);

        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function getlistacli_chart2($params){
        $sqlcli=$this->sql_topcli2($params);
        $columns = array( 
            0=>'cliente',
            1=>"paq_pes",
            2=>'tar_tot',
            3=>'fechayear'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from($sqlcli);

        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function sql_peso_importe($params){

        $fechai=$params['fechai'];
        $fechaf=$params['fechaf'];
        $sucursal =$params['sucursals'];
        log_message('error','sucuarsal:'.$sucursal);
        
        $where_suc="and doc.sucursal='$sucursal' ";
        
        
        
        //log_message('error',$anio);
        //$anio = json_decode($anio);
        $consulta='';
        //====================================================
            //for ($i=0;$i<count($anio);$i++) {
                //$aniofor=$anio[$i];
                //$where_anio=" and doc.fecha >='$aniofor-01-01 00:00:00' and doc.fecha <='$aniofor-12-30 23:59:59' ";
                $where_anio=" and doc.fecha >='$fechai 00:00:00' and doc.fecha <='$fechaf 23:59:59' ";

                $consulta.="SELECT
                    sum(doc.peso) paq_pes,
                    sum(doc.importe) tar_tot
                from guias doc
                INNER JOIN cli_clis as cli on cli.cliente=doc.cliente
                where doc.rid>0
                    $where_suc
                    $where_anio
                ";
            //}
        //====================================================
        $query=$this->db->query($consulta);
        return $query;
    }

 
    






}