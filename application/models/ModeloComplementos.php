<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloComplementos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getcomplementos($params){
        $cliente=$params['cliente'];
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $tipoempresa=$params['tipoempresa'];
        $sucursal=$params['sucursalrow'];
        $cfdi=$params['cfdi'];
        $ecomp=$params['ecomp'];
        $columns = array( 
            0=>'c.complementoId',
            1=>'c.Serie',
            2=>'c.Folio',
            3=>'c.R_nombre',
            4=>'c.R_rfc',
            5=>'c.Monto',
            6=>'c.Estado',
            7=>'c.fechatimbre',
            8=>'c.rutaXml',
            9=>'c.rutaAcuseCancelacion',
            10=>'c.uuid',
            11=>'cli.fac_email',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('f_complementopago as c');
        $this->db->join('f_complementopago_documento cd', 'cd.complementoId = c.complementoId');
        $this->db->join('f_facturas fac', 'fac.FacturasId = cd.facturasId');
        $this->db->join('cli_facts cli', 'cli.rid = fac.Clientes_ClientesId','left');
        if($cliente!='0'){
            $this->db->where(array('c.R_rfc'=>$cliente));
        }
        if($finicio!=''){
            $this->db->where(array('c.fechatimbre >='=>$finicio.' 00:00:00'));
        }
        if($ffin!=''){
            $this->db->where(array('c.fechatimbre <='=>$ffin.' 23:59:59'));
        }
        if($tipoempresa>0){
            $this->db->where(array('c.ConfiguracionesId'=>$tipoempresa));
        }
        if($sucursal>0){
            $this->db->where(array('fac.sucursal'=>$sucursal));
        }
        if($cfdi>0){
            //$this->db->where(array('c.uso_cfdi'=>$cfdi));
        }
        if ($ecomp>0) {
            if($ecomp==1){
                $Estado=1;
            }
            if($ecomp==2){
                $Estado=0;
            }
            if($ecomp==3){
                $Estado=2;
            }
            $this->db->where(array('c.Estado'=>$Estado));
        }
        $this->db->where(array('c.activo'=>1));
        
        $this->db->group_by("c.complementoId");

        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_complementos($params){
        $cliente=$params['cliente'];
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $tipoempresa=$params['tipoempresa'];
        $sucursal=$params['sucursalrow'];
        $cfdi=$params['cfdi'];
        $ecomp=$params['ecomp'];
        $columns = array( 
            0=>'c.complementoId',
            1=>'c.Serie',
            2=>'c.Folio',
            3=>'c.R_nombre',
            4=>'c.R_rfc',
            5=>'c.Monto',
            6=>'c.Estado',
            7=>'c.fechatimbre',
            8=>'c.rutaXml',
            9=>'c.rutaAcuseCancelacion',
            10=>'c.uuid'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('f_complementopago as c');
        $this->db->join('f_complementopago_documento cd', 'cd.complementoId = c.complementoId');
        $this->db->join('f_facturas fac', 'fac.FacturasId = cd.facturasId');
        if($cliente!='0'){
            $this->db->where(array('c.R_rfc'=>$cliente));
        }
        if($finicio!=''){
            $this->db->where(array('c.fechatimbre >='=>$finicio.' 00:00:00'));
        }
        if($ffin!=''){
            $this->db->where(array('c.fechatimbre <='=>$ffin.' 23:59:59'));
        }
        if($tipoempresa>0){
            $this->db->where(array('c.ConfiguracionesId'=>$tipoempresa));
        }
        if($sucursal>0){
            $this->db->where(array('fac.sucursal'=>$sucursal));
        }
        if($cfdi>0){
            //$this->db->where(array('c.uso_cfdi'=>$cfdi));
        }
        if ($ecomp>0) {
            if($ecomp==1){
                $Estado=1;
            }
            if($ecomp==2){
                $Estado=0;
            }
            if($ecomp==3){
                $Estado=2;
            }
            $this->db->where(array('c.Estado'=>$Estado));
        }
        $this->db->where(array('c.activo'=>1));
        $this->db->group_by("c.complementoId");
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->num_rows();
    }
    function saldocomplemento($factura){
        $strq = "SELECT doc.ImpPagado
                FROM f_complementopago_documento as doc 
                INNER JOIN f_complementopago as com on com.complementoId=doc.complementoId 
                WHERE com.Estado=1 AND doc.facturasId=$factura
                "; 
        $datoscop = $this->db->query($strq);
        $saldo=0;
        $numcomplem=0;
        foreach ($datoscop->result() as $item) {
            //log_message('error', 'validar saldo0: '.$item->ImpPagado);
            $saldo=$saldo+$item->ImpPagado;
            $numcomplem++;
        }
        //log_message('error', 'validar saldo1: '.$saldo);
        return $saldo; 
    }
    function saldocomplementonum($factura){
        $strq = "SELECT doc.ImpPagado
                FROM f_complementopago_documento as doc 
                INNER JOIN f_complementopago as com on com.complementoId=doc.complementoId 
                WHERE com.Estado=1 AND doc.facturasId=$factura
                "; 
        $datoscop = $this->db->query($strq);
        $saldo=0;
        $numcomplem=0;
        foreach ($datoscop->result() as $item) {
            $saldo=$saldo+$item->ImpPagado;
            $numcomplem++;
        }
        return $numcomplem; 
    }
    function get_factura($f1,$f2,$emp,$suc,$cfdi,$ecomp,$cliente){
        if($suc>0){
            $suc_w=" and fac.sucursal=$suc ";
        }else{
            $suc_w="";
        }

        if($cfdi>0){
            $cfdi_w=" and f.uso_cfdi=$cfdi ";
        }else{
            $cfdi_w="";
        }
        if ($ecomp>0) {
            if($ecomp==1){
                $Estado=1;
            }
            if($ecomp==2){
                $Estado=0;
            }
            if($ecomp==3){
                $Estado=2;
            }
            $ecomp_w=" and c.Estado=$Estado ";
        }else{
            $ecomp_w="";
        }
        if($cliente!='0'){
            $rfc_where=" and c.R_rfc='$cliente' ";
        }else{
            $rfc_where='';
        }

        $strq="SELECT `c`.`complementoId`, `c`.`Serie`, `c`.`Folio`, `c`.`R_nombre`, `c`.`R_rfc`, `c`.`Monto`, `c`.`Estado`, `c`.`fechatimbre`, `c`.`rutaXml`, `c`.`rutaAcuseCancelacion`, `c`.`uuid`,fp.formapago_text
                FROM `f_complementopago` as `c`
                JOIN `f_complementopago_documento` `cd` ON `cd`.`complementoId` = `c`.`complementoId`
                JOIN `f_facturas` `fac` ON `fac`.`FacturasId` = `cd`.`facturasId`
                inner join f_formapago as fp on fp.clave=c.FormaDePagoP
                WHERE 
                    c.ConfiguracionesId = $emp
                    AND c.activo = 1
                    $suc_w
                    $ecomp_w
                    $rfc_where
                    and c.fechatimbre BETWEEN '$f1 00:00:00' AND '$f2 23:59:59'
                GROUP BY `c`.`complementoId`";
        $query = $this->db->query($strq);
        return $query->result(); 
    }



}