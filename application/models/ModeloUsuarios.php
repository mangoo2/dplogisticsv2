<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloUsuarios extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function getlist($params){
        $columns = array( 
            0=>'usu.usuario',
            1=>'usu.usuario',
            2=>'usu.nda',
            3=>'usu.nombre',
            4=>'usu.correo',
            5=>'usu.sexo',
            6=>'sucs.suc_nombre'
        );
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('jsi_sis_admins usu');
        $this->db->join('coe_sucs sucs', 'sucs.sucursal=usu.sucursal','left');

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlist_total($params){
        $columns = array( 
            0=>'usu.usuario',
            1=>'usu.usuario',
            2=>'usu.nda',
            3=>'usu.nombre',
            4=>'usu.correo',
            5=>'usu.sexo',
            6=>'sucs.suc_nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('jsi_sis_admins usu');
        $this->db->join('coe_sucs sucs', 'sucs.sucursal=usu.sucursal','left');
        
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }







}