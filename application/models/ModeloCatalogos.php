<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->DB2 = $this->load->database('other_db', TRUE); 
        $this->DBo2 = $this->load->database('other2_db', TRUE); 
        $this->DBo3 = $this->load->database('other3_db', TRUE); 
    }
    
    function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function insert_batch($Tabla,$data){
        $this->db->insert_batch($Tabla, $data);
    }
    function updateCatalogo($Tabla,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($Tabla);
        //return $id;
    }

    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }
    function getselectwheren_o2($table,$where){
        $this->DBo2->select('*');
        $this->DBo2->from($table);
        $this->DBo2->where($where);
        $query=$this->DBo2->get(); 
        return $query;
    }
    function getselectwheren_o3($table,$where){
        $this->DBo3->select('*');
        $this->DBo3->from($table);
        $this->DBo3->where($where);
        $query=$this->DBo3->get(); 
        return $query;
    }
    function getselectwheren_db2($table,$where){
        $this->DB2->select('*');
        $this->DB2->from($table);
        $this->DB2->where($where);
        $query=$this->DB2->get(); 
        return $query;
    }

    function getselectwheren2($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query->result();
    }

    function getselectwherenlimirorderby($table,$where,$colm,$order,$limit){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $this->db->order_by($colm, $order);
        $this->db->limit($limit);
        $query=$this->db->get(); 
        return $query;
    }
    function getselectwheren_orderby($table,$where,$col_order,$order){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $this->db->order_by($col_order, $order);
        $query=$this->db->get(); 
        return $query;
    }
    function getselectwheren_orderby2($table,$where,$col_order,$order){
        $this->DBo2->select('*');
        $this->DBo2->from($table);
        $this->DBo2->where($where);
        $this->DBo2->order_by($col_order, $order);
        $query=$this->DBo2->get(); 
        return $query;
    }
    function getdeletewheren($table,$where){
        $this->db->where($where);
        $this->db->delete($table);
    }

    public function getselectwherelike($tables,$where,$likec1,$likev1){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);
        $this->db->like($likec1,$likev1, 'both'); 
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    //===================================
        function Insertbd2($Tabla,$data){
            $this->DB2->insert($Tabla, $data);
            $id=$this->DB2->insert_id();
            return $id;
        }
        function insert_batchbd2($Tabla,$data){
            $this->DB2->insert_batch($Tabla, $data);
        }
        function updateCatalogobd2($Tabla,$data,$where){
            $this->DB2->set($data);
            $this->DB2->where($where);
            $this->DB2->update($Tabla);
            //return $id;
        }

        function getselectwherenbd2($table,$where){
            $this->DB2->select('*');
            $this->DB2->from($table);
            $this->DB2->where($where);
            $query=$this->DB2->get(); 
            return $query;
        }
        function getselectwheren_orderbybd2($table,$where,$col_order,$order){
            $this->DB2->select('*');
            $this->DB2->from($table);
            $this->DB2->where($where);
            $this->DB2->order_by($col_order, $order);
            $query=$this->DB2->get(); 
            return $query;
        }
        function getdeletewherenbd2($table,$where){
            $this->DB2->where($where);
            $this->DB2->delete($table);
        }
        public function genSelectbd2($tables){
            $this->DB2->select("*");
            $this->DB2->from($tables);
            $query=$this->DB2->get();
            return $query;
        }
    //===================================
    function sucursalorigen($sucursal){
        $strq = "SELECT suc.suc_ruta,rut.rut_cla,rut.rut_nom
                FROM coe_sucs as suc 
                inner join coe_ruts as rut on rut.ruta=suc.suc_ruta
                where suc.sucursal = '$sucursal'";
        $query = $this->db->query($strq);
        return $query;
    }
    public function getseleclike($tables,$cols,$values){
        $strq = "SELECT * from $tables WHERE  $cols like '%".$values."%'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    public function getseleclike_cliente($values){
        $strq = "SELECT * from cli_clis WHERE  concat(cli_nombre,' ',cli_paterno,' ',cli_materno) like '%".$values."%'";
        $query = $this->DBo2->query($strq);
        //$this->db->close();
        return $query;
    }
    public function getseleclike_cliente2($values){
        $strq = "SELECT * from cli_facts WHERE  fac_nrs like '%".$values."%'";
        $query = $this->DBo2->query($strq);
        //$this->db->close();
        return $query;
    }
    public function genSelect($tables){
        $this->DBo2->select("*");
        $this->DBo2->from($tables);
        $query=$this->DBo2->get();
        return $query->result();
    }
    function ultimoFolio($serie) {
        //$strq = "SELECT Folio FROM f_facturas WHERE activo=1 and Folio!=0 ORDER BY FacturasId DESC limit 1";
        $strq = "SELECT max(Folio) as Folio FROM f_facturas WHERE activo=1 and Estado=1 and serie='$serie' "; // se van a descartar las facturas sin timbrar pero si se timbra la pendiente se modificaria el folio y agarraria el consecutivo
        $Folio = 0;
        $query = $this->db->query($strq);
        foreach ($query->result() as $row) {
            $Folio =$row->Folio;
        } 
        return $Folio;
    }
    function ultimoFoliocp($serie) {
        //$strq = "SELECT Folio FROM f_facturas WHERE activo=1 and Folio!=0 ORDER BY FacturasId DESC limit 1";
        $strq = "SELECT max(Folio) as Folio FROM f_complementopago WHERE activo=1 and Serie='$serie' ";
        $Folio = 0;
        $query = $this->db->query($strq);
        foreach ($query->result() as $row) {
            $Folio =$row->Folio;
        } 
        return $Folio;
    }
    function nombreunidadfacturacion($id){
        $sql="SELECT nombre
                from f_unidades
                WHERE UnidadId='$id'";
        $query=$this->db->query($sql);
        $nombre='';
        foreach ($query->result() as $item) {
            $nombre=$item->nombre;
        }
        return $nombre;
    }
    function traeProductosFactura($FacturasId){
        $sql="SELECT 
                dt.Unidad AS ClaveUnidad, 
                ser.Clave AS ClaveProdServ,
                dt.Descripcion2 as Descripcion, 
                dt.Cu, 
                dt.descuento, 
                dt.Cantidad, 
                dt.Importe, 
                u.nombre,
                u.Clave AS cunidad,
                dt.NoIdentificacion
                FROM f_facturas_servicios AS dt 
                left JOIN f_unidades AS u ON dt.Unidad = u.Clave 
                LEFT JOIN f_servicios AS ser ON ser.Clave = dt.ServicioId 
                WHERE dt.FacturasId =$FacturasId";
        $query=$this->db->query($sql);
        return $query;
    }
    function actualizarcomprobanteg($file,$corte){
        $sql="UPDATE guias SET file_comprobante = '$file'
        WHERE corte = '$corte'
        AND (file_comprobante = '' or  file_comprobante is null)";
        $query=$this->db->query($sql);
        return $query;
    }
    function total_facturas($tipo,$finicio, $ffin,$emp){
        if($tipo==1){
            $estado =' Estado = 1 ';
        }else{
            $estado =' Estado = 0 ';
        }
        $sql="SELECT count(*) as total FROM f_facturas 
                WHERE $estado and tipoempresa=$emp and activo =1 and fechatimbre between '$finicio 00:00:00' and '$ffin 23:59:59'";

        $query=$this->db->query($sql);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        return $total;
    }
    function totalcomplementos($finicio, $ffin,$emp){
        $sql="SELECT count(*) as total FROM f_complementopago 
                WHERE Estado=1 and ConfiguracionesId=$emp and fechatimbre between '$finicio 00:00:00' and '$ffin 23:59:59'";

        $query=$this->db->query($sql);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        return $total;
    }
    function obtenertiposervicio($aerolinea){
        $sql="SELECT * FROM tiposervicio where (coecad=$aerolinea or coecad=3) and activo=1";
        $query=$this->DBo2->query($sql);
        return $query;
    }
    function regimenf($numero){
        switch (intval($numero)) {            
            case 601:
                $regimen="601 General de Ley Personas Morales";
                break;
            case 603:
                $regimen="603 Personas Morales con Fines no Lucrativos";
                break;
            case 605:
                $regimen="605 Sueldos y Salarios e Ingresos Asimilados a Salarios";
                break;
            case 606:
                $regimen="606 Arrendamiento";
                break;
            case 607:
                $regimen="607 Régimen de Enajenación o Adquisición de Bienes";
                break;
            case 608:
                $regimen="608 Demás ingresos";
                break;
            case 609:
                $regimen="609 Consolidación";
                break;
            case 610:
                $regimen="610 Residentes en el Extranjero sin Establecimiento Permanente en México";
                break;
            case 611:
                $regimen="611 Ingresos por Dividendos (socios y accionistas)";
                break;
            case 612:
                $regimen="612 Personas Físicas con Actividades Empresariales y Profesionales";
                break;
            case 614:
                $regimen="614 Ingresos por intereses";
                break;
            case 615:
                $regimen="615 Régimen de los ingresos por obtención de premios";
                break;
            case 616:
                $regimen="616 Sin obligaciones fiscales";
                break;
            case 620:
                $regimen="620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos";
                break;
            case 621:
                $regimen="621 Incorporación Fiscal";
                break;
            case 622:
                $regimen="622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras";
                break;
            case 623:
                $regimen="623 Opcional para Grupos de Sociedades";
                break;
            case 624:
                $regimen="624 Coordinados";
                break;
            case 625:
                $regimen="625 Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas";
                break;
            case 626:
                $regimen="626 Régimen Simplificado de Confianza";
                break;
            case 628:
                $regimen="628 Hidrocarburos";
                break;
            case 629:
                $regimen="629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales";
                break;
            case 630:
                $regimen="630 Enajenación de acciones en bolsa de valores";
                break;
            default:
                $regimen="";
                break;
        }
        return $regimen;
    }
    function list_coe_guias($codigo){
        //vista_guias es un resumen de la tabla 'guias'
        $sql="SELECT 
                gui.*,
                gui.emp as emp_as,
                gui.guia as guia_as, 
                guiry.*,
                group_concat(concat('Paqs:',paqs.paq_num,' Peso:',paqs.paq_pes,'KG., Volumen:',paqs.paq_lar,'x',paqs.paq_anc,'x',paqs.paq_alt) SEPARATOR '<br>') as paquetes,
                gutars.*,
                paqsf.forma as formaf,
                gufac.*,
                succor.cor_fecha
             FROM vista_guias  as gui 
             left join guia_rycs as guiry on guiry.guia=gui.rid
             left join guia_paqs as paqs on paqs.guia=gui.rid
             left join guia_tars as gutars on gutars.guia=gui.rid
             left join guia_pags as paqsf on paqsf.guia=gui.rid
             left join guia_facts as gufac on gufac.guia=gui.rid
             left join suc_cors as succor on succor.sucursal=gui.sucursal and succor.corte=gui.corte
             where 
             gui.sucursal='$codigo'
             group by gui.rid 
             ORDER BY gui.rid DESC LIMIT 100 ";
        $query=$this->DBo3->query($sql);
        return $query;
    }
    function list_coe_guias_fechas($codigo,$fechaini,$fechafin){
        //vista_guias es un resumen de la tabla 'guias'
        // misma que la funcion list_coe_guias solo que es por filtros de fecha
        $sql="SELECT 
                gui.*, 
                gui.emp as emp_as,
                gui.guia as guia_as,
                guiry.*,
                group_concat(concat('Paqs:',paqs.paq_num,' Peso:',paqs.paq_pes,'KG., Volumen:',paqs.paq_lar,'x',paqs.paq_anc,'x',paqs.paq_alt) SEPARATOR '<br>') as paquetes,
                gutars.*,
                paqsf.forma as formaf,
                paqsf.forma2 as formaf2,
                paqsf.importe as importe1,
                paqsf.importe2,
                gufac.*,
                succor.cor_fecha
             FROM guias  as gui 
             left join guia_rycs as guiry on guiry.guia=gui.rid
             left join guia_paqs as paqs on paqs.guia=gui.rid
             left join guia_tars as gutars on gutars.guia=gui.rid
             left join guia_pags as paqsf on paqsf.guia=gui.rid
             left join guia_facts as gufac on gufac.guia=gui.rid
             left join suc_cors as succor on succor.sucursal=gui.sucursal and succor.corte=gui.corte
             where 
             gui.sucursal='$codigo' and gui.fecha>='$fechaini 00:00:00' and gui.fecha<='$fechafin 23:59:59'
             group by gui.rid
             ORDER BY gui.rid DESC ";
        $query=$this->DBo3->query($sql);
        return $query;
    }
    function list_coe_creds($codigo){
        $sql="SELECT 
                gpag.fecini, 
                gpag.emp, 
                caer.aer_nom, 
                gu.folio, 
                gu.guia, 
                gu.origen, 
                gu.destino, 
                gu.importe, 
                cli.cli_nombre, 
                cli.cli_paterno, 
                cli.cli_materno, 
                gpag.pag_adm 
            FROM guia_pags as gpag 
            LEFT JOIN guias as gu on gu.rid=gpag.guia 
            LEFT JOIN coe_aers as caer on caer.aero=gu.aero 
            LEFT JOIN cli_clis as cli on cli.cliente=gpag.cliente 
            WHERE gpag.sucursal='$codigo' AND gpag.forma='cre' ORDER BY gpag.fecini DESC LIMIT 50
            ";
        $query=$this->db->query($sql);
        return $query;
    }
    function list_coe_creds_filtros($codigo,$fechaini,$fechafin){
        $sql="SELECT 
                gpag.fecini, 
                gpag.emp, 
                caer.aer_nom, 
                gu.folio, 
                gu.guia, 
                gu.origen, 
                gu.destino, 
                gu.importe, 
                cli.cli_nombre, 
                cli.cli_paterno, 
                cli.cli_materno, 
                gpag.pag_adm 
            FROM guia_pags as gpag 
            LEFT JOIN guias as gu on gu.rid=gpag.guia 
            LEFT JOIN coe_aers as caer on caer.aero=gu.aero 
            LEFT JOIN cli_clis as cli on cli.cliente=gpag.cliente 
            WHERE gpag.sucursal='$codigo' AND gpag.forma='cre' and gpag.fecini>='$fechaini 00:00:00' and gpag.fecini<='$fechafin 23:59:59'
            ORDER BY gpag.fecini DESC 
            ";
        $query=$this->db->query($sql);
        return $query;
    }
    function solcotidelete(){
        $sql="SELECT 
                doc.*,
                mot.motivo 
            FROM doc_cotiza as doc 
            LEFT JOIN motivos_cancelacion as mot on mot.id=doc.tipo_cancelacion
            WHERE doc.sol_delete=1 
            ";
        $query=$this->DBo2->query($sql);
        return $query;
    }
    function getlis_conf_suc(){
        $sql="SELECT 
                csuc.rid,
                csuc.sucursal,
                csuc.suc_nombre,
                csuc.suc_ubicacion,
                csuc.suc_ruta,
                csuc.suc_admin,
                cfol.folio,
                cfol.fol_num
            FROM coe_sucs as csuc 
            left join coe_fols as cfol on cfol.sucursal=csuc.sucursal and cfol.fol_est='act'
            ORDER BY csuc.suc_nombre ASC, csuc.rid ASC 
            ";
        $query=$this->db->query($sql);
        return $query;    
    }
    function get_list_coe_rh($suc){
        if($suc==0){
            $suc_where="";
        }else{
            $suc_where=" and rh.sucursal='$suc'";
        }
        $sql="SELECT 
                    rh.*,suc.suc_nombre
                    FROM coe_rh as rh 
                    left join coe_sucs as suc on suc.sucursal=rh.sucursal
                    where rh.rh_est='act' $suc_where ORDER BY rh.sucursal ASC, rh.rh_nom ASC";
        $query=$this->db->query($sql);
        return $query; 
    }
    function complementofacturas($facturaId){
        $sql="SELECT comp.complementoId,comp.FechaPago,comp.rutaXml,compd.NumParcialidad,compd.ImpPagado
                FROM f_complementopago AS comp
                inner join f_complementopago_documento as compd on compd.complementoId=comp.complementoId
                where comp.Estado=1 and compd.facturasId=$facturaId
            ";
        $query=$this->db->query($sql);
        return $query;
    }
    function getclienterazonsociallike($search){
        $sql="SELECT * FROM cli_facts WHERE fac_nrs like '%$search%' or fac_rfc like '%$search%' group by fac_rfc";
        $query=$this->DBo2->query($sql);
        return $query;
    }
    function primer_anio($cliente){
        $sql="SELECT * FROM `guias` WHERE cliente='$cliente' LIMIT 1";
        $query=$this->db->query($sql);
        $anio=0;
        foreach ($query->result() as $item) {
            $anio=date("Y", strtotime($item->fecha));
        }
        return $anio;
    }
    function getsucursalmetas($anio,$suc){
        $sql="SELECT met.* 
        FROM coe_sucs_metas as met
        WHERE met.anio='$anio' AND met.activo=1 AND met.sucid=$suc";
        $query=$this->db->query($sql);
        return $query;
    }
    function getconsultardepositos($id){
        $sql="SELECT * FROM `suc_deps` WHERE relacionguias='$id' OR relacionguias_n LIKE '%|$id|%'";
        $query=$this->db->query($sql);
        return $query;
    }
    function consult_bd2_rev_unis($equ,$rep_fecini,$rep_fecfin){
        $strq = "SELECT rev,rev_fec FROM rev_unis WHERE equipo='$equ' AND rev_fec >= '$rep_fecini 00:00:00' AND rev_fec <= '$rep_fecfin 23:59:59' ORDER BY rev_fec DESC";
        log_message('error',$strq);
        $query = $this->DB2->query($strq);
        return $query;
    }
    function consult_bd2_BRDU_REV($rev){
        $strq = "SELECT rev FROM brdu_revs WHERE rid='$rev'";
        log_message('error',$strq);
        $query = $this->DB2->query($strq);
        return $query;
    }
    function consult_bd2_BRDU_RPRE($rev){
        $strq = "SELECT rid,pre,pre_pos,pre_tit FROM rev_pres WHERE rev='$rev' ORDER BY pre_pos ASC";
        log_message('error',$strq);
        $query = $this->DB2->query($strq);
        return $query;
    }

    function consult_bd2_BRDU_RRES($rev,$uid,$pid){
        $strq = "SELECT res,res_obs FROM rev_ress WHERE rev='$rev' AND uid='$uid' AND pid='$pid' ORDER BY rid ASC LIMIT 1";
        log_message('error',$strq);
        $query = $this->DB2->query($strq);
        return $query;
    }
    function consult_bd2_BRDU_RPRE_BRDU_RRES($rev,$uid){
        $strq = "SELECT 
                    rp.rid,
                    rp.pre,
                    rp.pre_pos,
                    rp.pre_tit,
                    rps.res,
                    rps.res_obs
                FROM rev_pres as rp
                left join rev_ress as rps on rps.pid=rp.rid and rps.uid='$uid' and rps.rev=rp.rev
                WHERE rp.rev='$rev' 
                group by rp.rid
                ORDER BY rp.pre_pos,rps.rid ASC ";
        log_message('error',$strq);
        $query = $this->DB2->query($strq);
        return $query;
    }
    function find_pdf_with_suffix($directory, $suffix) {
        // Asegúrate de que el directorio termine con una barra
        $directory = rtrim($directory, '/') . '/';
        
        // Usa glob para buscar todos los archivos PDF en el directorio
        $pattern = $directory . '*.pdf';
        $files = glob($pattern);
        
        // Filtra los archivos que terminan con el sufijo especificado
        $matching_files = array_filter($files, function($file) use ($suffix) {
            return substr(basename($file), -strlen($suffix)) === $suffix;
        });

        return $matching_files;
    }
    function not_sol_pam_edit(){
        $sql="SELECT * FROM pam WHERE sol_editar=1 ";
        $query=$this->DBo2->query($sql);
        return $query;
    }
    function not_sol_pam_add_mat_corre(){
        $sql="SELECT pam.rid,pam.cic,pam.sucursal,suc.suc_nombre,pam.equipo,eq.equ_nombre,eq.equ_numeco,pam.sol_correctivo_personal,pam.sol_correctivo_reg
                FROM pam
                INNER JOIN coe_sucs as suc on suc.sucursal=pam.sucursal
                INNER JOIN pam_equ as eq on eq.equipo=pam.equipo
                WHERE pam.sol_correctivo=1";
        $query=$this->db->query($sql);
        return $query;
    }
    function not_sol_bmau_edit(){
        $sql="SELECT 
                pb.rid,
                pa.cic,
                suc.suc_nombre,
                eq.equ_numeco,
                pb.bit_fecha,pb.bit_tipo,pb.bit_clase,
                pb.sol_editar_info
                FROM pam_bit as pb 
                INNER JOIN pam as pa on pa.rid=pb.pam
                INNER JOIN coe_sucs as suc on suc.sucursal=pa.sucursal
                INNER JOIN pam_equ as eq on eq.equipo=pa.equipo
                WHERE pb.sol_editar=1";
        $query=$this->db->query($sql);
        return $query;
    }
    function get_solicitudes_add_cliente_sub(){
        $strq="SELECT cli.rid,cli.cliente,cli.sucursal,cli.cli_nombre,cli.cli_paterno,cli.cli_materno,suc.suc_nombre FROM cli_clis as cli INNER JOIN coe_sucs as suc on suc.sucursal=cli.sucursal WHERE cli.sol_add_cli_prisub=1";
        $query=$this->db->query($strq);
        return $query;
    }
    function ger_sol_delete_hijossubcli(){
        $strq="SELECT 
                    clich.id,
                    
                    clip.rid as id_padre,
                    concat(clip.cli_nombre,'', clip.cli_paterno, '',clip.cli_materno) as padre,
                    sucp.suc_nombre as suc_padre,
                    sucp.sucursal,

                    clih.rid as id_hijo,
                    concat(clih.cli_nombre,'', clih.cli_paterno, '',clih.cli_materno) as hijo,
                    such.suc_nombre as suc_hijo
                        
                    FROM cli_clis_hijos as clich
                    JOIN cli_clis as clih ON clih.rid=clich.rid_hijo
                    JOIN coe_sucs as such on such.sucursal=clih.sucursal
                    JOIN cli_clis as clip ON clip.rid=clich.rid_padre
                    JOIN coe_sucs as sucp on sucp.sucursal=clip.sucursal
                    
                    WHERE clich.activo = 1 AND clich.sol_delete=1";
        $query=$this->db->query($strq);
        return $query;
    }
}