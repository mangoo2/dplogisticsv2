<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelofacturas extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->DBo2 = $this->load->database('other2_db', TRUE); 
        $this->DBo3 = $this->load->database('other3_db', TRUE); 
    }
    function getfacturas($params){
        $subconsulta=$this->get_query_lis_fac($params);
        $columns = array( 
            0=>'FacturasId',
            1=>'serie',
            2=>'Folio',
            3=>'Nombre',
            4=>'Rfc',
            5=>'total',
            6=>'fechatimbre',
            7=>'Estado',
            8=>'rutaXml',
            9=>'rutaAcuseCancelacion',
            10=>'cadenaoriginal',
            11=>'FormaPago',
            12=>'Clientes_ClientesId',
            13=>'correoenviado',
            14=>'fac_email',
            15=>'uuid',
            16=>'tipo',
            17=>'cot_fdp',
            18=>'cotiza'
            
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from($subconsulta);
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_facturas($params){
        $subconsulta=$this->get_query_lis_fac($params);
        $columns = array( 
            0=>'FacturasId',
            1=>'Folio',
            2=>'Nombre',
            3=>'Rfc',
            4=>'total',
            5=>'Estado',
            6=>'fechatimbre',
            7=>'rutaXml'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from($subconsulta);
       
        
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function documentorelacionado($complemento){
        $strq = "SELECT 
                    compd.facturasId,
                    compd.IdDocumento,
                    compd.NumParcialidad,
                    compd.ImpSaldoAnt,
                    compd.ImpPagado,
                    compd.ImpSaldoInsoluto,
                    compd.MetodoDePagoDR,
                    fac.moneda,
                    fac.Folio,
                    fac.serie,
                    fac.FormaPago,
                    fac.iva,
                    fac.ivaretenido,
                    fac.subtotal,
                    fac.serie,
                    fac.Clientes_ClientesId
                FROM f_complementopago_documento as compd
                inner join f_facturas as fac on fac.FacturasId=compd.facturasId
                where compd.complementoId=$complemento";
        $query = $this->db->query($strq);
        return $query;
    }
    function facturadetalle($facturaId){
        $strq = "SELECT * FROM `f_facturas_servicios` as fas 
                inner join f_unidades as uni on uni.Clave=fas.Unidad
                WHERE fas.FacturasId=".$facturaId;
        $query = $this->db->query($strq);
        return $query; 
    }
    public function getseleclikeunidadsat($search){
        $strq = "SELECT * from f_unidades WHERE activo=1 and (Clave like '%".$search."%' or nombre like '%".$search."%' )";
        $query = $this->DBo2->query($strq);
        //$this->db->close();
        return $query;
    }
    public function getseleclikeconceptosa($search){
        $strq = "SELECT * from f_servicios WHERE activo=1 and (Clave like '%".$search."%' or nombre like '%".$search."%' )";
        $query = $this->DBo2->query($strq);
        //$this->db->close();
        return $query;
    }
    public function getRFC($id){
        $this->db->select("cli_rfc");
        $this->db->from('cli_clis');
        $this->db->where('rid',$id);
        $query=$this->db->get();
        return $query->row();
    }
    function regimenf($numero){
        switch (intval($numero)) {            
            case 601:
                $regimen="601 General de Ley Personas Morales";
                break;
            case 603:
                $regimen="603 Personas Morales con Fines no Lucrativos";
                break;
            case 605:
                $regimen="605 Sueldos y Salarios e Ingresos Asimilados a Salarios";
                break;
            case 606:
                $regimen="606 Arrendamiento";
                break;
            case 607:
                $regimen="607 Régimen de Enajenación o Adquisición de Bienes";
                break;
            case 608:
                $regimen="608 Demás ingresos";
                break;
            case 609:
                $regimen="609 Consolidación";
                break;
            case 610:
                $regimen="610 Residentes en el Extranjero sin Establecimiento Permanente en México";
                break;
            case 611:
                $regimen="611 Ingresos por Dividendos (socios y accionistas)";
                break;
            case 612:
                $regimen="612 Personas Físicas con Actividades Empresariales y Profesionales";
                break;
            case 614:
                $regimen="614 Ingresos por intereses";
                break;
            case 615:
                $regimen="615 Régimen de los ingresos por obtención de premios";
                break;
            case 616:
                $regimen="616 Sin obligaciones fiscales";
                break;
            case 620:
                $regimen="620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos";
                break;
            case 621:
                $regimen="621 Incorporación Fiscal";
                break;
            case 622:
                $regimen="622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras";
                break;
            case 623:
                $regimen="623 Opcional para Grupos de Sociedades";
                break;
            case 624:
                $regimen="624 Coordinados";
                break;
            case 625:
                $regimen="625 Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas";
                break;
            case 626:
                $regimen="626 Régimen Simplificado de Confianza";
                break;
            case 628:
                $regimen="628 Hidrocarburos";
                break;
            case 629:
                $regimen="629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales";
                break;
            case 630:
                $regimen="630 Enajenación de acciones en bolsa de valores";
                break;
            default:
                $regimen="";
                break;
        }
        return $regimen;
    }
    function get_factura($f1,$f2,$emp,$suc,$cfdi,$ecomp,$cliente){
        if($suc>0){
            $suc_w=" and f.sucursal=$suc ";
        }else{
            $suc_w="";
        }

        if($cfdi>0){
            $cfdi_w=" and f.uso_cfdi=$cfdi ";
        }else{
            $cfdi_w="";
        }
        if ($ecomp>0) {
            if($ecomp==1){
                $Estado=1;
            }
            if($ecomp==2){
                $Estado=0;
            }
            if($ecomp==3){
                $Estado=2;
            }
            $ecomp_w=" and f.Estado=$Estado ";
        }else{
            $ecomp_w="";
        }
        if($cliente!='0'){
            $rfc_where=" and f.Rfc='$cliente' ";
        }else{
            $rfc_where='';
        }
        if($f1==0){
            $where_f1='';
        }else{
            $where_f1=" and f.fechatimbre >= '$f1 00:00:00' ";
        }
        if($f2==0){
            $where_f2='';
        }else{
            $where_f2=" and f.fechatimbre >= '$f1 23:59:59'";
        }



        $strq = "SELECT f.FacturasId,f.serie,f.Folio,f.uuid,DATE_FORMAT(f.fechatimbre, '%d/%m/%Y' ) AS fecha,DATE_FORMAT(f.fechatimbre, '%r' ) AS hora,f.Rfc,f.Estado,fp.formapago,fm.metodopago_text,f.moneda,f.subtotal,f.iva,f.total,f.Nombre,s.NoIdentificacion,ser.nombre AS servicio,f.rutaXml
            FROM f_facturas AS f
            LEFT JOIN f_metodopago AS fm ON fm.metodopago=f.FormaPago
            LEFT JOIN f_formapago AS fp ON fp.clave=f.MetodoPago
            LEFT JOIN f_facturas_servicios AS s ON s.FacturasId=f.FacturasId
            LEFT JOIN f_servicios AS ser ON ser.clave=s.servicioId
            WHERE f.tipoempresa=$emp $suc_w $cfdi_w $ecomp_w $rfc_where $where_f1 $where_f2";
        $query = $this->db->query($strq);
        return $query->result(); 
    }
    public function get_operador_like($search){
        $strq = "SELECT * from operadores WHERE activo=1 AND rfc_del_operador like '%".$search."%'";
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function get_transporte_like($search){
        $strq = "SELECT tf.*,cf.descripcion AS configuracion_vhiculartxt,tp.descripcion AS tipo_permiso_scttxt  
            from transporte_federal AS tf
            LEFT JOIN f_c_configautotransporte AS cf ON cf.clave=tf.configuracion_vhicular
            LEFT JOIN f_c_tipopermiso AS tp ON tp.clave=tf.tipo_permiso_sct
            WHERE tf.activo=1 AND tf.id like '%".$search."%'";
        $query = $this->db->query($strq);
        return $query->result();
    }
    function get_query_lis_fac($params){
        $cliente=$params['cliente'];
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $tipoempresa=$params['tipoempresa'];
        $sucursal=$params['sucursalrow'];
        $cfdi=$params['cfdi'];
        $ecomp=$params['ecomp'];
        //==================================
            if($cliente!='0'){
                $cliente_w=" and fac.Rfc='$cliente'";
                $cliente_w2=" and clif.fac_rfc='$cliente'";
            }else{
                $cliente_w='';
                $cliente_w2='';
            }
            if($finicio!=''){
                $finicio_w=" and fac.fechatimbre >='$finicio 00:00:00'";
                $finicio_w2=" and dcot.cot_feccap >='$finicio 00:00:00'";
            }else{
                $finicio_w='';
                $finicio_w2='';
            }
            if($ffin!=''){
                $ffin_w=" and fac.fechatimbre <='$ffin 23:59:59'";
                $ffin_w2=" and dcot.cot_feccap <='$ffin 23:59:59'";
            }else{
                $ffin_w='';
                $ffin_w2='';
            }
            if($tipoempresa>0){
                $tipoempresa_w=" and fac.tipoempresa='$tipoempresa'";
                $tipoempresacodigo='';
                if($tipoempresa==1){
                    $tipoempresacodigo='10102016224826';
                }
                if($tipoempresa==2){
                    $tipoempresacodigo='10102016224842';
                }
                $tipoempresa_w2=" and dcot.aero='$tipoempresacodigo'";
            }else{
                $tipoempresa_w='';
                $tipoempresa_w2="";
            }
            if($sucursal>0){
                $sucursal_w=" and fac.sucursal='$sucursal' ";
            }else{
                $sucursal_w="";
            }
            if($cfdi>0){
                $cfdi_w=" and fac.uso_cfdi='$cfdi' ";
            }else{
                $cfdi_w="";
            }
            if ($ecomp>0) {
                if($ecomp==1){
                    $Estado=1;
                }
                if($ecomp==2){
                    $Estado=0;
                }
                if($ecomp==3){
                    $Estado=2;
                }
                $ecomp_w=" and fac.Estado='$Estado' ";
            }else{
                $ecomp_w="";
            }
        //==================================
        $strq="((";
        $strq.="SELECT 
                    fac.FacturasId, 
                    fac.serie, 
                    fac.Folio, 
                    fac.Nombre, 
                    fac.Rfc, 
                    fac.total, 
                    fac.fechatimbre, 
                    fac.Estado, 
                    fac.rutaXml, 
                    fac.rutaAcuseCancelacion, 
                    fac.cadenaoriginal, 
                    fac.FormaPago, 
                    fac.Clientes_ClientesId, 
                    fac.correoenviado, 
                    cli.fac_email, 
                    fac.uuid,
                    0 tipo,
                    '' cot_fdp,
                    '' cotiza
                FROM f_facturas fac
                JOIN cli_facts cli ON cli.rid = fac.Clientes_ClientesId
                WHERE fac.activo=1 $cliente_w $finicio_w $ffin_w $tipoempresa_w $sucursal_w $cfdi_w $ecomp_w";
        $strq.=") union (";
        $strq.="SELECT 
                    dcot.rid as FacturasId,
                    '' as serie,
                    CONCAT(dcot.rid,' (',dcot.cotiza,')') as Folio,
                    clif.fac_nrs as Nombre,
                    clif.fac_rfc as Rfc,
                    dcot.cot_importe as total,
                    dcot.cot_feccap as fechatimbre,
                    2 as Estado,
                    '' as rutaXml,
                    '' as rutaAcuseCancelacion,
                    '' as cadenaoriginal,
                    '' as FormaPago,
                    cli.rid as Clientes_ClientesId,
                    0 as correoenviado,
                    0 as fac_email,
                    '' as uuid,
                    1 as tipo,
                    dcot.cot_fdp,
                    dcot.cotiza
                FROM doc_cotiza as dcot
                INNER JOIN cli_clis as cli on  cli.cliente=dcot.cot_remite
                INNER JOIN cli_facts as clif on clif.factura=dcot.cot_factura
                WHERE dcot.facturar=1 AND dcot.facturado=0 $cliente_w2 $finicio_w2 $ffin_w2 $tipoempresa_w2";
        $strq.=")) as datos";
        return $strq; 
    }

}