<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCoesers extends CI_Model {
    public function __construct() {
        parent::__construct();
        //$this->DB2 = $this->load->database('other_db', TRUE); 
    }
    
    function lista_coe($codigo){
        //vista_guias es un resumen de la tabla 'vista_ser_coe' 'ser_coe'
        $sql="SELECT scoe.*
             FROM ser_coe  as scoe 
             where 
                scoe.sucursal='$codigo' and scoe.ser_est='fin'
                group by scoe.rid 
                ORDER BY scoe.ser_fec DESC
                LIMIT 20";
        $query=$this->db->query($sql);
        return $query;
    }
    function lista_coe_filtros($codigo,$empresa,$servicio,$fechaini,$fechafin){
        //vista_guias es un resumen de la tabla 'vista_ser_coe' 'ser_coe'
        if($empresa==='0'){
            $wempresa="";
        }else{
            $wempresa=" and scoe.ser_clis='$empresa' ";
        }
        if($servicio==='0'){
            $wservicio="";
        }else{
            $wservicio=" and scoe.ser_sers LIKE '%".strtoupper($servicio)."%' ";
        }
        $sql="SELECT scoe.*
             FROM ser_coe  as scoe 
             where 
                scoe.sucursal='$codigo' and scoe.ser_est='fin' and scoe.ser_fecini>='$fechaini 00:00:00' and scoe.ser_fecini<='$fechafin 23:59:59' $wempresa $wservicio
                group by scoe.rid 
                ORDER BY scoe.ser_fec DESC";
        $query=$this->db->query($sql);
        return $query;
    }
    function lista_cad($codigo){
        //vista_guias es un resumen de la tabla 'vista_ser_coe' 'ser_coe'
        $sql="SELECT scad.*
             FROM ser_cad  as scad 
             where 
                scad.sucursal='$codigo' and scad.ser_est='fin'
                group by scad.rid 
                ORDER BY scad.ser_fec DESC
                LIMIT 20";
        $query=$this->db->query($sql);
        return $query;
    }
    function lista_cad_filtros($codigo,$empresa,$servicio,$fechaini,$fechafin){
        if($empresa==='0'){
            $wempresa="";
        }else{
            $wempresa=" and scad.ser_clis='$empresa' ";
        }
        if($servicio==='0'){
            $wservicio="";
        }else{
            $wservicio=" and scad.ser_sers LIKE '%".strtoupper($servicio)."%' ";
        }
        $sql="SELECT scad.*
             FROM ser_cad  as scad 
             where 
                scad.sucursal='$codigo' and 
                scad.ser_est='fin' and 
                scad.ser_fecini>='$fechaini 00:00:00' and 
                scad.ser_fecini<='$fechafin 23:59:59' $wempresa $wservicio
                group by scad.rid 
                ORDER BY scad.ser_fec DESC
                LIMIT 20";
        $query=$this->db->query($sql);
        return $query;
    }
    

}