<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloPermisos extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->sess_usr=$this->session->userdata('sess_usr');
        $this->sess_rango_nda=$this->session->userdata('sess_rango_nda');
        $this->sess_mods=$this->session->userdata('sess_mods');
    }
    function RANGO(){
        //JSI_ADM (hace referencia a la tabla jsi_sis_admins)
        $strq = "SELECT nda FROM jsi_sis_admins where usuario='$this->sess_usr'";
        $query = $this->db->query($strq);
        return $query;
    }
    function SECCION( $seccion ){
            $aut = '';
            if ( $this->sess_rango_nda === 'ttl' || $this->sess_rango_nda === 'adm'){ 
                $aut = 'aUTr'; 
            }else{
                $permisosrow=explode(',',$this->sess_mods);
                for ($i = 0; $i < count($permisosrow); ++$i) {
                    if($permisosrow[$i]===$seccion){
                        $aut = 'aUTr';
                    }
                }
            }
            if ( $aut != '' && $aut === 'aUTr' ){ 
                return true; 
            }else{ 
                return false; 
            }
    }


    



}