<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloEstaciones extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_listado($params){
        $columns = array( 
            1=>'o.id',
			2=>'o.clave_identificacion',
			3=>'o.descripcion',
			4=>'o.clave_transporte',
			5=>'o.nacionalidad',
			6=>'o.iata',
			7=>'o.linea_ferrea',
            8=>'o.activo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('f_c_estaciones o');
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function get_listado_total($params){
        $columns = array( 
            1=>'o.id',
            2=>'o.clave_identificacion',
            3=>'o.descripcion',
            4=>'o.clave_transporte',
            5=>'o.nacionalidad',
            6=>'o.iata',
            7=>'o.linea_ferrea',
            8=>'o.activo'
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('f_c_estaciones o');
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }
    
}