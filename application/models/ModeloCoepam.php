<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCoepam extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }
    function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function getselectwherenlimirorderby($table,$where,$colm,$order,$limit){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $this->db->order_by($colm, $order);
        $this->db->limit($limit);
        $query=$this->db->get(); 
        return $query;
    }
    function listequipos($sucursal){
        $strq = "SELECT 
                    pameq.equipo,
                    pameq.equ_nombre,
                    pameq.equ_numeco 
                FROM pam_equ as pameq
                WHERE 
                    pameq.sucursal='$sucursal' 
                    ORDER BY pameq.equ_nombre ASC
                ";
        $query = $this->db->query($strq);
        return $query;
    }
    function reportegeneraunidades($sucu,$view){
        //$listequipos=$this->listequipos($sucu);
        $trse='';$x=0;
        $strq="SELECT * FROM pam_equ WHERE sucursal='$sucu' ORDER BY equ_nombre ASC";
        $resE = $this->db->query($strq);
        foreach ($resE->result() as $item) {
            $x++;
            //===============================
                $DIR_SUC=FCPATH.'_files/_suc/'.$sucu.'/pam';
                $DIR_SUC1=FCPATH.'_files/_suc/'.$sucu.'/pam/equs';
                $DIR_SUC2=FCPATH.'_files/_suc/'.$sucu.'/pam/equs/'.$item->equipo;
                //=================
                    if (!is_dir($DIR_SUC) && strlen($DIR_SUC)>0){
                        //mkdir($DIR_SUC, 0755);
                        if(!is_dir($DIR_SUC)){
                            mkdir($DIR_SUC, 0755);
                        }else{
                            
                             
                        }
                    }else{
                        file_put_contents($DIR_SUC.'/index.html', '');
                    }
                //=============================
                    if (!is_dir($DIR_SUC1) && strlen($DIR_SUC1)>0){
                        //mkdir($DIR_SUC, 0755);
                        if(!is_dir($DIR_SUC1)){
                            mkdir($DIR_SUC1, 0755);
                        }else{
                            
                             
                        }
                    }else{
                        file_put_contents($DIR_SUC1.'/index.html', '');
                    }
                //=============================
                //=============================
                    if (!is_dir($DIR_SUC2) && strlen($DIR_SUC2)>0){
                        //mkdir($DIR_SUC, 0755);
                        if(!is_dir($DIR_SUC2)){
                            mkdir($DIR_SUC2, 0755);
                        }else{
                             
                        }
                    }else{
                        file_put_contents($DIR_SUC2.'/index.html', '');
                    }
                //============================= 
                $ioimg='Sin evidencia';
                $ioimg_height='';
                $strq_pe="SELECT * FROM pam_equevi WHERE eid='$item->rid' ORDER BY rid ASC ";
                $res_pe = $this->db->query($strq_pe);
                $equ_foto='';
                foreach ($res_pe->result() as $itempe) {
                    $equ_foto=$itempe->evi_archivo;
                }
                if($equ_foto!=''){
                    $urlimg=FCPATH.'_files/_suc/'.$sucu.'/pam/equs/'.$item->equipo.'/'.$equ_foto;
                    $urlimg2=base_url().'_files/_suc/'.$sucu.'/pam/equs/'.$item->equipo.'/'.$equ_foto;

                    if(file_exists($urlimg)){
                        $img = @getimagesize($urlimg) ? true : false;
                       if($img){
                            $ioimg='<a href="'.$urlimg2.'" target="_blank"><img src="'.$urlimg2.'" alt="" style="width:50px" width="100"></a>';
                            $ioimg_height=' style="height:100px" ';
                        }else{
                            $ioimg='<a href="'.$urlimg2.'" target="_blank">Ver archivo</a>';
                        }
                    }
                }
                $accv='';
                $accv.='<a href="'.base_url().'Coe_pam/equ_evis/'.$sucu.'/'.$item->rid.'" class="btn btn-info btn-sm" ><i class="fa fa-folder-o"></i></a> ';
                $accv.='<a class="btn btn-info btn-sm edit_pam_u'.$item->rid.'" onclick="edit_pam_u('.$item->rid.')"
                                data-equnumeco="'.$item->equ_numeco.'"
                                data-equnombre="'.$item->equ_nombre.'"
                                data-equmarca="'.$item->equ_marca.'"
                                data-equmodelo="'.$item->equ_modelo.'"
                                data-equplaser="'.$item->equ_plaser.'"
                                data-equdescrip="'.$item->equ_descrip.'"
                                data-equestatus="'.$item->equ_estatus.'"
                            ><i class="fa fa-edit"></i></a> ';
                $accv.='<a class="btn btn-danger btn-sm" onclick="delete_pam_u('.$item->rid.')"><i class="fa fa-trash"></i></a> ';
            //=================
            $trse.='<tr>
                    <td>'.$x.'</td>
                    <td>'.$item->equ_numeco.'</td>
                    <td>'.$item->equ_nombre.'</td>
                    <td>'.$item->equ_marca.'</td>
                    <td>'.$item->equ_modelo.'</td>
                    <td>'.$item->equ_plaser.'</td>
                    <td>'.$item->equ_descrip.'</td>
                    <td>'.$item->equ_estatus.'</td>
                    <td '.$ioimg_height.'>'.$ioimg.'</td>';
                    if($view==0){
                        $trse.='<td>'.$accv.'</td>';
                    }
                    $trse.='</tr>';
        }
        return $trse;
    }
    function reporte_prog_anual_mantenimiento($ano,$sucu,$view){
        $listequipos=$this->listequipos($sucu);
        $html='';
        $xrow=1;
            foreach ($listequipos->result() as $itemleq) { 
                    $listpam=$this->getselectwheren('pam',array('cic'=>$ano,'sucursal'=>$sucu,'equipo'=>$itemleq->equipo));
                    if($listpam->num_rows()==0){
                        $this->Insert('pam',array('cic'=>$ano,'sucursal'=>$sucu,'equipo'=>$itemleq->equipo,'ene'=>'','feb'=>'','mar'=>'','abr'=>'','may'=>'','jun'=>'','jul'=>'','ago'=>'','sep'=>'','oct'=>'','nov'=>'','dic'=>''));
                    }
                    $listpam2=$this->getselectwheren('pam',array('cic'=>$ano,'sucursal'=>$sucu,'equipo'=>$itemleq->equipo));
                    $listpam2=$listpam2->row();
                    $a_bmau = base_url().'Coe_pam/bmau/'.$ano.'/'.$sucu.'/'.$listpam2->equipo.'/'.$listpam2->rid;
          
                    $html.='<tr>';
                        $html.='<td>'.$xrow.'</td>';
                        $html.='<td>'.$itemleq->equ_numeco.'</td>';
                        $html.='<td>'.$listpam2->ene.'</td>';
                        $html.='<td>'.$listpam2->feb.'</td>';
                        $html.='<td>'.$listpam2->mar.'</td>';
                        $html.='<td>'.$listpam2->abr.'</td>';
                        $html.='<td>'.$listpam2->may.'</td>';
                        $html.='<td>'.$listpam2->jun.'</td>';
                        $html.='<td>'.$listpam2->jul.'</td>';
                        $html.='<td>'.$listpam2->ago.'</td>';
                        $html.='<td>'.$listpam2->sep.'</td>';
                        $html.='<td>'.$listpam2->oct.'</td>';
                        $html.='<td>'.$listpam2->nov.'</td>';
                        $html.='<td>'.$listpam2->dic.'</td>';
                        if($view==0){
                        $html.='<td class="ocultar_xls">';
                            $html.='<div class="btn-group btn-group-toggle" data-toggle="buttons">';
                              $html.='<label class="btn btn-secondary">';
                                $html.='<input type="radio" name="options"  autocomplete="off" onclick="modalpam('.$listpam2->rid.')"> PAM';
                              $html.='</label>';
                              $html.='<label class="btn btn-secondary">';
                                $html.='<a href="'.$a_bmau.'" class="a_bmau"><input type="radio" name="options"  autocomplete="off"> BMAU</a>';
                              $html.='</label>';
                            $html.='</div>';
                        $html.='</td>';
                        }
                    $html.='</tr>';
             $xrow++;
        }
        return $html;
    }
    function reporte_prog_bmau($cic,$suc,$equ,$pam,$view){
        $html='';
        //$equ_id=$equ;
        if($cic>=2017 && ($suc && $equ) !='' && $pam>0){

            //$resS = $__BASE->consulta("SELECT suc_nombre FROM ".TAB_SUC." WHERE sucursal='$suc' LIMIT 1");
            $resS = $this->getselectwheren('coe_sucs',array('sucursal'=>$suc));
            if($resS->num_rows()>0){
                //list($suc_nombre)=$__BASE->registro($resS);
                $resS=$resS->row();
                $suc_nombre=$resS->suc_nombre;
                //list($equ_numeco)=$__BASE->registro($__BASE->consulta("SELECT equ_numeco FROM ".PAM_EQU." WHERE equipo='$equ' LIMIT 1"));
                log_message('error','$equ_1'.$equ);
                $resreq = $this->getselectwheren('pam_equ',array('equipo'=>$equ));
                if($resreq->num_rows()>0){
                    $resreq=$resreq->row();
                    $equ_numeco=$resreq->equ_numeco;
                }else{
                    $equ_numeco='';
                }
                // Pam
                $trsp='';$x=0;
                //$resE=$__BASE->consulta("SELECT rid,bit,bit_fecha,bit_odometro,bit_tipo,bit_clase,bit_clades,bit_preventivo,bit_correctivo,bit_comfon,bit_comcr,bit_comine,bit_piecoc,bit_piefon,bit_piefot,bit_oso,bit_costo,bit_respon FROM ".PAM_BIT." WHERE pam='$pam' ORDER BY bit_fecha DESC");
                $resE_strq="SELECT * FROM pam_bit WHERE pam='$pam' ORDER BY bit_fecha DESC";
                $resE = $this->db->query($resE_strq);
                foreach ($resE->result() as $item) {
                    $x++;
                    $btn='';
                    $btn.='<a href="'.base_url().'Coe_pam/evis/'.$suc.'/'.$equ.'/'.$pam.'/'.$item->rid.'" class="btn btn-info btn-sm"><i class="fa fa-folder-o"></i></a> ';
                    $btn.='<a class="btn btn-info btn-sm edit_pam_u172" onclick="add_bit('.$item->rid.')"><i class="fa fa-edit"></i></a> ';
                    $btn.='<a class="btn btn-danger btn-sm" onclick="delete_pam_bit('.$item->rid.')"><i class="fa fa-trash"></i></a>';
                    $trsp.='<tr>
                        <td>'.$x.'</td><td>'.$item->bit_fecha.'</td><td>'.$item->bit_odometro.'</td><td>'.$item->bit_tipo.'</td><td>'.$item->bit_clase.'</td><td>'.$item->bit_clades.'</td>
                        <td>'.$item->bit_preventivo.'</td><td>'.$item->bit_correctivo.'</td><td>'.$item->bit_comfon.'</td><td>'.$item->bit_comcr.'</td><td>'.$item->bit_comine.'</td><td>'.$item->bit_piecoc.'</td>
                        <td>'.$item->bit_piefon.'</td><td>'.$item->bit_piefot.'</td><td>'.$item->bit_oso.'</td><td>'.$item->bit_costo.'</td><td>'.$item->bit_respon.'</td>';
                        if($view==0){
                            $trsp.='<td>'.$btn.'</td>';
                        }
                    $trsp.='</tr>';
                }
                
                $html=$trsp;
                
            }else{
                $html ='Error de sucursal...';
            }
        }else{
            $html = 'Error de datos...';
        }
        return $html;
    }
    
    function pam_sercla($ser){
        log_message('error',json_encode($ser));
        $ret='';
        if($ser!=''){
            $ret.='<option value="">Ninguno</option>';
        }else{
            $ret.='<option value="" selected>Ninguno</option>';
        }
        //$ser=explode(',',$ser);
        //$ser=json_encode($ser);
        $sn=count($ser);

        $result_pam_ser=$this->getselectwherenlimirorderby('pam_ser',array('rid >'=>0),'ser_clave','ASC',30);
        foreach ($result_pam_ser->result() as $item) {
            $ta='';
            if($ser!=''){
                for($x=0;$x<$sn;$x++){
                    log_message('error','$ser[$x]= '.$ser[$x]);
                    $ser_array=explode(',',$ser[$x]);
                    
                    for ($i = 0; $i < count($ser_array); $i++) {
                        //echo $ser_array[$i] . "<br>";
                        if($ser_array[$i]==$item->ser_clave){
                            $ta='selected';
                        }
                    }
                }
            }
            $ret.='<option value="'.$item->ser_clave.'" '.$ta.'>'.$item->ser_clave.' -> '.$item->ser_nombre.'</option>';
        }



        return $ret;
    }






}