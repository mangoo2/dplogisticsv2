<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloClientes extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->DBo2 = $this->load->database('other2_db', TRUE); 
        $this->DBo3 = $this->load->database('other3_db', TRUE); 
        //$this->consult_hijos=1;//incluir consultas de hijos 0 no 1 si
        $this->config->load('custom_config');
        $this->consult_hijos=$this->config->item('consult_hijos');
    }

    function getlist($params){
        $sucursal=$params['sucursal'];
        $columns = array( 
            0=>'rid',
            1=>"concat(cli_nombre,' ',cli_paterno,' ',cli_materno) as clientename",
            2=>'cli_telefono',
            3=>'cli_celular',
            4=>'cli_email',
            5=>'cli_io',
            6=>'cli_cum',
            7=>'cli_com',
            8=>'cli_des',
            9=>'credito',
            10=>'cliente',
            
            
        );
        $columnsss = array( 
            0=>'rid',
            1=>"cli_nombre",
            2=>'cli_telefono',
            3=>'cli_celular',
            4=>'cli_email',
            5=>'cli_io',
            6=>'cli_cum',
            7=>'cli_com',
            8=>'cli_des',
            9=>'credito',
            10=>'cliente',
            11=>'cli_nombre',
            12=>'cli_paterno',
            13=>'cli_materno',
            14=>"concat(cli_nombre,' ',cli_paterno,' ',cli_materno)",
            15=>"concat(cli_nombre,' ',cli_paterno,'  ',cli_materno)",
            16=>"concat(cli_nombre,'  ',cli_paterno,' ',cli_materno)",
            17=>"concat(cli_nombre,'  ',cli_paterno,'  ',cli_materno)",
            18=>"concat(cli_nombre,' ',cli_paterno,' ',cli_materno,' ')",
            19=>"concat(cli_nombre,' ',cli_paterno,cli_materno)",
            20=>"concat(cli_nombre,cli_paterno,' ',cli_materno)",
            20=>"concat(cli_nombre,cli_paterno,cli_materno)",
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('cli_clis');
        if($sucursal!='xxxxxxxxxx'){
            $this->db->where(array('sucursal'=>$sucursal));
        }
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlist_total($params){
        $sucursal=$params['sucursal'];
        $columns = array( 
            0=>'rid',
            1=>"cli_nombre",
            2=>'cli_telefono',
            3=>'cli_celular',
            4=>'cli_email',
            5=>'cli_io',
            6=>'cli_cum',
            7=>'cli_com',
            8=>'cli_des',
            9=>'cliente',
            10=>'cli_nombre',
            11=>'cli_paterno',
            12=>'cli_materno',
            13=>"concat(cli_nombre,' ',cli_paterno,' ',cli_materno)",
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('cli_clis');
        
        
        if($sucursal!='xxxxxxxxxx'){
            $this->db->where(array('sucursal'=>$sucursal));
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function listclienteguias($sucursal,$cliente,$fecha,$tipo){
        if($tipo==1){
            $conficionw='<=';
        }else{
            $conficionw='>';
        }


        $strq = "SELECT 
                    gpags.rid,
                    gpags.emp,
                    gpags.guia,
                    gpags.fecini,
                    gpags.fecfin,
                    gpags.coms,
                    gui.folio,
                    gui.guia,
                    gui.importe,
                    gui.rid as ridg
                FROM guia_pags as gpags 
                left join guias as gui on gui.rid=gpags.guia and gui.sucursal='$sucursal'
                WHERE 
                gpags.sucursal='$sucursal' and 
                gpags.cliente ='$cliente' and 
                gpags.forma='cre' and
                gpags.estatus='pdp' and
                gpags.fecfin $conficionw '$fecha'
                ";
        $query = $this->db->query($strq);
        return $query;
    }
    function getlist_ug($params){
        $numcliente=$params['numcliente'];
        $fechaini=$params['fechaini'];
        $fechafin=$params['fechafin'];
        $columns = array( 
            0=>'gur.rid',
            1=>'gur.emp',
            2=>'gui.folio',
            3=>'gur.guia',
            4=>'gui.origen',
            5=>'gui.destino',
            6=>'gur.con',
            7=>'gur.con_dir',
            8=>'gui.paqs',
            9=>'gui.paq_des',
            10=>'gui.peso',
            11=>'gui.volumen',
            12=>'gui.tarifa',
            13=>'gui.importe',
            14=>'gui.fecha',
            15=>'gui.admin',
            16=>'gur.con_col',
            17=>'gur.con_ciu',
            18=>'gur.con_tel',
            19=>'gur.con_cp',
            20=>'gui.guia',
            21=>'gui.sucursal'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        

        $this->db->select($select);
        $this->db->from('guia_rycs gur');
        $this->db->join('guias gui', 'gui.rid=gur.guia');
        //$this->db->where(array('gur.cliente'=>$numcliente));
        if($this->consult_hijos==0){
            $this->db->where(array('gur.cliente'=>$numcliente));
        }else{
            $this->db->group_start();
                $this->db->or_like('gur.cliente',$numcliente);
                $r_h = $this->consultar_hijos($numcliente);
                foreach ($r_h->result() as $item_rh) {
                    $this->db->or_like('gur.cliente',$item_rh->num_hijo);
                }
            $this->db->group_end();
        }
        if($fechaini!=''){
            $this->db->where(array('gui.fecha >='=>$fechaini.' 00:00:00'));
        }
        if($fechafin!=''){
            $this->db->where(array('gui.fecha <='=>$fechafin.' 23:59:59'));
        }
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlist_ug_total($params){
        $numcliente=$params['numcliente'];
        $fechaini=$params['fechaini'];
        $fechafin=$params['fechafin'];
        $columns = array( 
            0=>'gur.rid',
            1=>'gur.emp',
            2=>'gui.folio',
            3=>'gur.guia',
            4=>'gui.destino',
            5=>'gur.con',
            6=>'gur.con_dir',
            7=>'gui.paqs',
            8=>'gui.paq_des',
            9=>'gui.peso',
            10=>'gui.volumen',
            11=>'gui.tarifa',
            12=>'gui.importe',
            13=>'gui.fecha',
            14=>'gui.admin',
            15=>'gur.con_col',
            16=>'gur.con_ciu',
            17=>'gur.con_tel',
            18=>'gur.con_cp',
            19=>'gui.guia',
            20=>'gui.origen',
            21=>'gui.sucursal'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('guia_rycs gur');
        $this->db->join('guias gui', 'gui.rid=gur.guia');
        //$this->db->where(array('gur.cliente'=>$numcliente));
        if($this->consult_hijos==0){
            $this->db->where(array('gur.cliente'=>$numcliente));
        }else{
            $this->db->group_start();
                $this->db->or_like('gur.cliente',$numcliente);
                $r_h = $this->consultar_hijos($numcliente);
                foreach ($r_h->result() as $item_rh) {
                    $this->db->or_like('gur.cliente',$item_rh->num_hijo);
                }
            $this->db->group_end();
        }

        if($fechaini!=''){
            $this->db->where(array('gui.fecha >='=>$fechaini.' 00:00:00'));
        }
        if($fechafin!=''){
            $this->db->where(array('gui.fecha <='=>$fechafin.' 23:59:59'));
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function getlist_clisb($params){
        $rid_padre=$params['rid_padre'];
        $columns = array( 
            0=>'cli.rid',
            1=>"cli.rid",
            2=>"cli.cli_nombre",
            3=>"cli.cli_paterno",
            4=>"cli.cli_materno",
            5=>"clih.id",
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('cli_clis_hijos clih');
        $this->db->join('cli_clis cli', 'cli.rid=clih.rid_hijo');
        $this->db->where(array('clih.activo'=>1,'clih.rid_padre'=>$rid_padre));
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlist_clisb_total($params){
        $rid_padre=$params['rid_padre'];
        $columns = array( 
            0=>'cli.rid',
            1=>"cli.rid",
            2=>"cli.cli_nombre",
            3=>"cli.cli_paterno",
            4=>"cli.cli_materno",
            5=>"clih.id",
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('cli_clis_hijos clih');
        $this->db->join('cli_clis cli', 'cli.rid=clih.rid_hijo');
        $this->db->where(array('clih.activo'=>1,'clih.rid_padre'=>$rid_padre));
        
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function verificli($cli){
        $strq = "SELECT cli.rid,cli.cli_nombre,cli.cli_paterno,cli.cli_materno,cli.sucursal,suc.suc_nombre,suc.suc_ubicacion,cch.id as id_h_p
                FROM cli_clis as cli
                LEFT JOIN coe_sucs as suc on suc.sucursal=cli.sucursal
                LEFT JOIN cli_clis_hijos as cch on cch.activo=1 and (cch.rid_padre=cli.rid OR cch.rid_hijo=cli.rid)
                WHERE cli.rid='$cli'";
        $query = $this->db->query($strq);
        return $query;
    }
    function eshijo($cli){
        $strq = "SELECT cli.rid,cli.cliente,cli.cli_nombre,cli.cli_paterno,cli.cli_materno,cli.sucursal,suc.suc_nombre,suc.suc_ubicacion,cch.id as id_h_p
                FROM cli_clis as cli
                LEFT JOIN coe_sucs as suc on suc.sucursal=cli.sucursal
                inner JOIN cli_clis_hijos as cch on cch.activo=1 and cch.rid_padre=cli.rid
                WHERE cch.rid_hijo='$cli'";
        $query = $this->db->query($strq);
        return $query;
    }
    function consultar_hijos($numcliente){
        $strq="SELECT 
                clip.rid as rid_padre,clip.cliente as num_padre,
                clih.rid as rid_hijo ,clih.cliente as num_hijo 
            FROM cli_clis_hijos as cch
            INNER JOIN cli_clis as clip on clip.rid=cch.rid_padre
            INNER JOIN cli_clis as clih on clih.rid=cch.rid_hijo
            WHERE clip.cliente='$numcliente' AND cch.activo=1";
        $query = $this->db->query($strq);
        return $query;
    }






}