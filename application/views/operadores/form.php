<!-- BEGIN : Main Content-->
<div class="main-content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="content-header"><?php echo $title;?> OPERADOR</div>
                <!--<p class="content-sub-header mb-1">All table styles are inherited in Bootstrap 4.3.1, meaning any nested tables will be styled in the same manner as the parent.</p>-->
            </div>
        </div>
        <!--Basic Table Starts-->
        <section id="simple-table">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <!--<h4 class="card-title">Default Table</h4>-->
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <!--------------->
                                <form id="form_registro" method="POST">
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="hidden" name="rid" id="rid" value="<?php echo $id;?>">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5 class="box-title">Datos generales</h5>
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 form-group">
                                                    <label>DPL</label>
                                                    <select class="form-control" name="dpl" id="dpl" required style="width: 100%">   
                                                        <?php if($dpl!=''){?> 
                                                            <option value="<?php echo $dpl ?>"><?php echo $dpltxt ?></option>
                                                        <?php } ?>  
                                                    </select>
                                                </div>
                                                <div class="col-md-5 form-group">
                                                    <label>RFC DEL OPERADOR</label>
                                                    <input type="text" class="form-control" name="rfc_del_operador" value="<?php echo $rfc_del_operador;?>" required>
                                                </div>
                                                <div class="col-md-4 form-group">
                                                    <label>NO. LICENCIA </label>
                                                    <input type="text" class="form-control" name="no_licencia" value="<?php echo $no_licencia;?>" required>
                                                </div>
                                            </div>    
                                            <div class="row">    
                                                <div class="col-md-6 form-group">
                                                    <label>NOMBRE DEL OPERADOR</label>
                                                    <input type="text" class="form-control" name="operador" value="<?php echo $operador;?>" required>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label>NÚMERO DE IDENTIFICACIÓN O REGISTRO FISCAL DEL OPERADOR</label>
                                                    <input type="text" class="form-control" name="num_identificacion" value="<?php echo $num_identificacion;?>">
                                                </div>
                                            </div>    
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label>RESIDENCIA FISCAL DEL OPERADOR</label>
                                                    <input type="text" class="form-control" name="residencia_fiscal" value="<?php echo $residencia_fiscal;?>">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label>CALLE</label>
                                                    <input type="text" class="form-control" name="calle" value="<?php echo $calle;?>">
                                                </div>
                                            </div>    
                                            <div class="row">
                                                <div class="col-md-4 form-group">
                                                    <label>ESTADO</label>
                                                    <select class="form-control" name="estado">
                                                        <?php foreach ($result_estados->result() as $item) { ?>
                                                            <option value="<?php echo $item->c_Estado ?>" <?php if($item->c_Estado==$estado) echo 'selected' ?>><?php echo $item->descripcion ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4 form-group">
                                                    <label>PAÍS</label>
                                                    <input type="text" class="form-control" name="pais" value="<?php echo $pais;?>">
                                                </div>
                                                <div class="col-md-4 form-group">
                                                    <label>CÓDIGO POSTAL</label>
                                                    <input type="text" class="form-control" name="codigo_postal" value="<?php echo $codigo_postal;?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-primary btn_registro" onclick="guardar_registro()">Guardar</button>
                                            <a onclick="javascript:history.back()" class="btn btn-danger">Regresar</a>
                                        </div>
                                    </div>
                            </div>            
                        </div>
                    </div>
                </div>            
            </div>
        </section>

    </div>
</div>            