<style type="text/css">
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
</style>  
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Rutas</div>
                            <p class="content-sub-header mb-1">Config</p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a onclick="editar(0)" type="button" class="btn btn-success mr-1 mb-1">Agregar</a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 table-bordered table-striped thead-light tablevc" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                
                                                                    <th>Clave</th>
                                                                    <th>Nombre</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($resultlist->result() as $item) { ?>
                                                                    <tr>
                                                                        <td><?php echo $item->rut_cla;?></td>
                                                                        <td><?php echo $item->rut_nom;?></td>
                                                                        <td>
                                                                            <a type="button" 
                                                                                class="btn btn-sm btn-success mr-1 mb-1 edit_form_<?php echo $item->rid;?>"
                                                                                data-rut_cla="<?php echo $item->rut_cla;?>"
                                                                                data-rut_nom="<?php echo $item->rut_nom;?>"  
                                                                                onclick="editar(<?php echo $item->rid;?>)"><i class="fa fa-pencil fa-fw"></i></a>
                                                                            <a type="button" onclick="deleteaer(<?php echo $item->rid;?>)" class="btn btn-sm btn-danger mr-1 mb-1"><i class="fa fa-trash fa-fw"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->



<div id="modalform" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Modificar empresa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_datos">
            <input type="hidden" id="rid" name="rid" value="0">
            <div class="row">
                <div class="col-md-12">
                    <label>* Clave</label>
                    <input type="text" name="rut_cla" id="rut_cla" class="form-control" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>* Nombre</label>
                    <input type="text" name="rut_nom" id="rut_nom" class="form-control" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>* Campos importantes</label>
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary saveform" onclick="saveform()">Actualizar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>