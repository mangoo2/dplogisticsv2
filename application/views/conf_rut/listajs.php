<script src="<?php echo base_url();?>app-assets/vendors/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>app-assets/css/plugins/form-validation.css">

<script src="<?php echo base_url();?>public/plugins/fileinput/fileinput.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>public/plugins/fileinput/fileinput.min.css">


<script type="text/javascript">
    var var_rid;
    var var_aer_nom;
    var var_aer_cla;
    var var_aer_pos;
    var var_emp;

    $("#aer_img").fileinput({
            showCaption: false,
            dropZoneEnabled: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["png","jpg","jpeg","bmp"],
            browseLabel: 'Seleccionar Imagen',
            uploadUrl: '<?php echo base_url();?>Conf_aers/inserupdate',
            inputGroupClass: "input-group-sm",
            maxFilePreviewSize: 5000,
            uploadExtraData: function (previewId, index) {
                var info = {
                            rid:var_rid,
                            emp:var_emp,
                            aer_cla:var_aer_cla,
                            aer_nom:var_aer_nom,
                            aer_pos:var_aer_pos

                        };
                return info;
            }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
        }).on('filebatchuploadsuccess', function(event, files, extra) {
          location.reload();
          //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        });

    
    function editar(id){
        if(id>0){
        $('#exampleModalLiveLabel').html('Editar Ruta');
        $('.saveform').html('Actualizar');
        $('#modalform').modal({backdrop: 'static', keyboard: false});

        $('#rid').val(id);

        var rut_cla = $('.edit_form_'+id).data('rut_cla');
        $('#rut_cla').val(rut_cla);

        var rut_nom = $('.edit_form_'+id).data('rut_nom');
        $('#rut_nom').val(rut_nom);

        }else{
            $('#exampleModalLiveLabel').html('Agregar ruta');
            $('.saveform').html('Agregar');
            $('#modalform').modal({backdrop: 'static', keyboard: false});
            $('#form_datos')[0].reset();

        }
    }
    function saveform() {
        var varform=$('#form_datos');
        if(varform.valid()){
            var datos = varform.serialize();
            $.ajax({
                type:'POST',
                url: "<?php echo base_url();?>index.php/Conf_rut/inserupdate",
                data: datos,
                success: function (response){
                  $('#modalform').modal('hide');
                  
                  toastr["success"]("Informacion Actualizada", "Hecho"); 
                  
                  setTimeout(function(){
                    location.reload();
                  }, 2000);

                },
                error: function(response){
                    toastr["error"]("Algo salio mal", "Error"); 
                     
                }
              });                
        }
    }
    function deleteaer(id){
        $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma borrar la ruta',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: "<?php echo base_url();?>index.php/Conf_rut/delete",
                        data: {
                            rid:id
                        },
                        success: function (response){
                            toastr["success"]("Aerolinea eliminada", "Hecho"); 
                            location.reload();                         
                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
    }
</script>