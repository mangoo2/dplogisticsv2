<style type="text/css">
    .imgusuario{
        width: 80px;
    }
    .imgusuario:hover{
        width: 150px;
    }
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
</style>
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Usuarios</div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="<?php echo base_url();?>Adm_usrs/add" type="button" class="btn btn-success mr-1 mb-1">Agregar</a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 tablevc table-bordered" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>Foto</th>
                                                                    <th>Usuario</th>
                                                                    <th>Tipo</th>
                                                                    <th>Nombre</th>
                                                                    <th>Correo</th>
                                                                    <th>Sexo</th>
                                                                    <th>Sucursal</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->
