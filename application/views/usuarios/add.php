            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header"><?php echo $title;?> CLIENTE </div>
                            <!--<p class="content-sub-header mb-1">All table styles are inherited in Bootstrap 4.3.1, meaning any nested tables will be styled in the same manner as the parent.</p>-->
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                
                                                    <form id="form_usuario">
                                                        <input type="hidden" name="idform" id="idform" value="<?php echo $idform;?>">
                                                        <div class="row">
                                                            <div class="col-md-6 form-group">
                                                                <label>usuario</label>
                                                                <input type="text" name="usuario" value="<?php echo $usuario;?>" id="usuario" class="form-control" required <?php echo $readonly;?>>
                                                                <span style="font-size: 11px;">El nombre de usuario solamente puede contener Letras y Numeros. ( * No espacios, acentos, tildes, etc.)</span>
                                                            </div>
                                                            <div class="col-md-6 form-group">
                                                                <label>Contraseña</label>
                                                                <input type="text" name="clave" value="<?php echo $clave;?>" id="clave" class="form-control"  >
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6 form-group">
                                                                <label>nombre</label>
                                                                <input type="text" name="nombre" value="<?php echo $nombre;?>" id="nombre" class="form-control" required>
                                                            </div>
                                                            <div class="col-md-6 form-group">
                                                                <label>Sexo</label>
                                                                <select name="sexo" id="sexo" class="form-control">
                                                                    <option value="f">Mujer</option>
                                                                    <option value="m">Hombre</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6 form-group">
                                                                <label>Correo</label>
                                                                <input type="mail" name="correo" value="<?php echo $correo;?>" id="correo" class="form-control">
                                                            </div>
                                                            <div class="col-md-6 form-group">
                                                                <label>Fotografia</label>
                                                                <input type="file" name="fotografia" id="fotografia" class="form-control" >
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6 form-group">
                                                                <label>Sucursal</label>
                                                                <select name="sucursal" id="sucursal" class="form-control">

                                                                    <?php 
                                                                    foreach ($sucursalrow as $item) { ?>
                                                                        <option value="<?php echo $item->sucursal?>" <?php if($item->sucursal==$sucursal){echo 'selected';}?> ><?php echo $item->suc_nombre;?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6 form-group">
                                                                <label>Tipo de usuario</label>
                                                                <select name="nda" id="nda" class="form-control">
                                                                    <option value="adm" <?php if('adm'==$nda){echo 'selected';}?> >Administrador</option>
                                                                    <option value="lmt" <?php if('lmt'==$nda){echo 'selected';}?> >Limitado</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="box-header with-border">
                                                                    <h3 class="box-title">General</h3>
                                                                </div>
                                                                <div class="box-body">
                                                                    <div class="scrollX">
                                                                        <div>
                                                                            <label><input type="checkbox" value="panel" name="mods[]"> Panel</label>
                                                                        </div>
                                                                        <div><label><input type="checkbox" value="documentar" name="mods[]"> Documentar</label></div>
                                                                        <div><label><input type="checkbox" value="ser_rampa" name="mods[]"> Servicios RAMPA</label></div>
                                                                        <div><label><input type="checkbox" value="ser_rye" name="mods[]"> Servicios RyE</label></div>
                                                                        <div><label><input type="checkbox" value="clientes" name="mods[]"> Clientes</label></div>
                                                                        <div><label><input type="checkbox" value="ope_pam" name="mods[]"> PAM</label></div>
                                                                        <div><label><input type="checkbox" value="ope_brdu" name="mods[]"> BRDU</label></div><hr>
                                                                        <div><label><input type="checkbox" value="coe_docs" name="mods[]"> Documentos</label></div>
                                                                        <div><label><input type="checkbox" value="1" name="fac" id="fac" <?php if($fac==1){ echo 'checked';} ?> > Cancelación de facturas</label></div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="box-header with-border"><h3 class="box-title">Administración</h3> </div>
                                                                <div class="box-body">
                                                                    <div class="scrollX">
                                                                        <div><label><input type="checkbox" value="coe_guias" name="mods[]"> Guiás</label></div>
                                                                        <div><label><input type="checkbox" value="coe_sers" name="mods[]"> Servicios</label></div>
                                                                        <div><label><input type="checkbox" value="coe_cuenta" name="mods[]"> Cuentas de sucursal</label></div>
                                                                        <div><label><input type="checkbox" value="coe_facts" name="mods[]"> Facturas</label></div>
                                                                        <div><label><input type="checkbox" value="coe_creds" name="mods[]"> Créditos</label></div>
                                                                        <div><label><input type="checkbox" value="coe_clientes" name="mods[]"> Clientes</label></div>
                                                                        <div><label><input type="checkbox" value="coe_pyg" name="mods[]"> Presupuestos y Gastos</label></div>
                                                                        <div><label><input type="checkbox" value="coe_reqs" name="mods[]"> Requisiciones</label></div>
                                                                        <div><label><input type="checkbox" value="coe_rechum" name="mods[]"> Recursos humanos</label></div>
                                                                        <div><label><input type="checkbox" value="coe_pam" name="mods[]"> PAM</label></div>
                                                                        <div><label><input type="checkbox" value="coe_brdu" name="mods[]"> BRDU</label></div>
                                                                        <div><label><input type="checkbox" value="coe_bit_guias" name="mods[]"> Bitacora Guias</label></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="box-header with-border"><h3 class="box-title">Responsable de estación</h3> </div>
                                                                <div class="box-body">
                                                                    <div class="scrollX">
                                                                        <div><label><input type="checkbox" value="suc_cors" name="mods[]"> Cortes de caja</label></div>
                                                                        <div><label><input type="checkbox" value="suc_guias" name="mods[]"> Guiás</label></div>
                                                                        <div><label><input type="checkbox" value="suc_serrampa" name="mods[]"> Servicios RAMPA</label></div>
                                                                        <div><label><input type="checkbox" value="suc_serrye" name="mods[]"> Servicios RyE</label></div>
                                                                        <div><label><input type="checkbox" value="suc_cuenta" name="mods[]"> Cuenta de estación</label></div>
                                                                        <div><label><input type="checkbox" value="suc_clientes" name="mods[]"> Clientes</label></div>
                                                                        <div><label><input type="checkbox" value="suc_pyg" name="mods[]"> Presupuestos y Gastos</label></div>
                                                                        <div><label><input type="checkbox" value="suc_usogas" name="mods[]"> Uso combustible</label></div>
                                                                        <div><label><input type="checkbox" value="suc_eyv" name="mods[]"> Entregas y Viajes</label></div>
                                                                        <div><label><input type="checkbox" value="suc_usotel" name="mods[]"> Uso telefónico</label></div>
                                                                        <div><label><input type="checkbox" value="suc_reqs" name="mods[]"> Requisiciones</label></div>
                                                                        <div><label><input type="checkbox" value="suc_rechum" name="mods[]"> Recursos humanos</label></div>
                                                                        <div><label><input type="checkbox" value="suc_pam" name="mods[]"> PAM</label></div>
                                                                        <div><label><input type="checkbox" value="suc_brdu" name="mods[]"> BRDU</label></div>
                                                                        <div><label><input type="checkbox" value="suc_bit_guias" name="mods[]"> Bitacora Guias</label></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </form>
                                                        
                                                    
                                                
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a onclick="javascript:history.back()" class="btn btn-danger">Regresar</a>
                                                        <button id="okform" class="btn btn-primary" style="float: right;"><?php echo $formbtn;?></button>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->