<script src="<?php echo base_url();?>app-assets/vendors/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>app-assets/css/plugins/form-validation.css">

<script src="<?php echo base_url();?>public/plugins/fileinput/fileinput.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>public/plugins/fileinput/fileinput.min.css">

<script src="<?php echo base_url();?>public/js/usuariosform.js?v=<?php echo date('Ymdgis')?>"></script>

<script type="text/javascript">
    $(document).ready(function($) {
        <?php 
            if($idform==1){ 
                $permisosrow=$mods;
                $permisosrow=explode(',',$permisosrow);
                //echo $permisosrow;

                for ($i = 0; $i < count($permisosrow); ++$i) { 
                    $valuepermisosrow = $permisosrow[$i];
                    
                    ?>
                        $("input[type='checkbox'][value='<?php echo $valuepermisosrow;?>']").attr('checked',true);
                    <?php 
                }
            } 
        ?>
    });
</script>