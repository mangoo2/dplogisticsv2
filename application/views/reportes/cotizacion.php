<?php
require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF/tcpdf.php';
require_once dirname(__FILE__) . '/FPDI/autoload.php';
use setasign\Fpdi\Tcpdf\Fpdi;
$this->load->helper('url');
$piezas=0;
$peso=0;
$volument=0;
foreach ($result_paquetes->result() as $item) {
  $piezas=$piezas+$item->paq_num;
  $peso=$peso+$item->paq_pes;
  $volumen=round(((($item->paq_alt*$item->paq_anc*$item->paq_lar)/6000)*$item->paq_num),2);
  //log_message('error','('.$item->paq_alt.'*'.$item->paq_anc.'*'.$item->paq_lar.')/6000 *'.$item->paq_num);
  $volument=$volument+$volumen;
}

if($cot_fdp=='cre'){
  $obser='FOLIO/GUIA A CRÉDITO';
}else{
  $obser='FOLIO/GUIA PAGADA';
}



$pdf = new FPDI(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('agb');
$pdf->SetTitle('Cotizacion '.$cotizacion);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);


$margin_footer=10;
// set margins
$pdf->SetMargins('6', '5', '6');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin($margin_footer);

// set auto page breaks
$pdf->SetAutoPageBreak(true, $margin_footer);// margen del footer

$pdf->SetFont('dejavusans', '', 12);
$pdf->AddPage();
//$logos = base_url().'public/img/alta.png';
//      $logos2 = base_url().'public/img/kyocera.png';
$styleQR = array(
  'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => array(255,255,255),
    'module_width' => 1, // ancho de cada módulo en puntos
    'module_height' => 1 // altura de cada módulo en puntos
       );
$urlfolio='https://coecsa.mx/rastrea.html';
$params='';
$params = $pdf->serializeTCPDFtagParameters(array($urlfolio, 'QRCODE,L', '', '', 18, 18, $styleQR, 'N'));
//$pdf->write2DBarcode('www.tcpdf.org', 'QRCODE,L', 20, 30, 50, 50, $style, 'N');
//$pdf->write2DBarcode($urlfolio, 'QRCODE,H', 20, 30, 18, 18, $styleQR, 'N');
$urlimage=base_url().'public/img/'.$titleimge;
$html='<style type="text/css">
            .title_font_size{
              font-size: 7px;
            }
            .border{
              border: 1px solid #9e9e9e;
            }
            .border_bottom{
              border-bottom: 1px solid #9e9e9e;
            }
           
          </style>';
$url_rs=base_url()."public/img/icon_rs.jpg";
$url_i=base_url()."public/img/icon_i.jpg";
$url_t=base_url()."public/img/icon_t.jpg";
$html.='<table border="0">
          <tr>
            <td width="34%" rowspan="3" colspan="3">
              <img src="'.$urlimage.'" width="160px">
            </td>
            <td width="66%" colspan="7" style="font-size:8px;" >'.$title1.'</td>
          </tr>
          <tr>
            <td colspan="7" align="right" style="font-size:8px;">Encuéntranos en <img src="'.$url_rs.'" height="10px"/></td>
          </tr>
          <tr>
            <td colspan="4"></td>
            <td rowspan="3" colspan="2" style="font-size:7px;padding:10px" align="center" cellpadding="9" >
              <table>
                <tr><td> <span style="color:white">sssssss</span><br>PARA RASTREO DE TU ENVÍO POR FAVOR ESCANEAR ESTE QR</td></tr>
              </table></td>
            <td rowspan="5" colspan="1" align="center" style="text-align:center" ><tcpdf method="write2DBarcode" params="' . $params . '" /></td>
          </tr>
          <tr>
            <td colspan="7" style="font-size:6.4px;">'.$title2.'</td>
          </tr>
          <tr>
            <td colspan="3"></td>
            <td colspan="3" align="center">GUÍA '.$aerolinea.'</td>
          </tr>
          <tr>
            <td width="12%" class="title_font_size">NO. FOLIO:</td>
            <td width="13%" class="title_font_size border" align="center">'.$folio.'</td>
            <td width="12%" class="title_font_size"> HORA:</td>
            <td width="13%" class="title_font_size border" align="center">'.date("h:i:s A",strtotime($cot_feccap)).'</td>
            <td  class="title_font_size"> FECHA:</td>
            <td  class="title_font_size border" align="center">'.date("d/m/Y",strtotime($cot_feccap)).'</td>
          </tr>
          <tr>
            <td class="title_font_size">NO. GUIA AÉREA:</td>
            <td class="title_font_size border" align="center">'.$cot_guia.'</td>
            <td class="title_font_size"> AEROLÍNEA:</td>
            <td class="title_font_size border" align="center">'.$aerolinea.'</td>
            <td class="title_font_size"> ORIGEN:</td>
            <td class="title_font_size border" align="center">'.$rutaorigen.'</td>
            <td  class="title_font_size">DESTINO:</td>
            <td  class="title_font_size border" align="center">'.$rutadestino.'</td>
          </tr>
        </table>
        
        
        
        <table border="0" cellpadding="1">
          <tr>
            <td width="50%" class="title_font_size" align="center">REMITENTE</td>
            <td width="5%" rowspan="8"></td>
            <td width="18%" colspan="2" class="title_font_size" align="right">DESCRIPCIÓN DEL CONTENIDO</td>
            <td width="27%" colspan="3" class="title_font_size border" rowspan="2" align="center">'.$cot_descrip.'</td>
          </tr>
          <tr>
            <td class="title_font_size border" rowspan="6">'.$remitente.'</td>
            <td colspan="2" style="font-size:6px;">(Declaración por Remitente)</td>
          </tr>
          <tr>
            <td class="title_font_size" colspan="2" align="center">TOTAL DE PIEZAS</td>
            <td class="title_font_size" colspan="3" align="center">CLAVE CARGA</td>
          </tr>
          <tr>
            <td class="title_font_size border" colspan="2" align="center">'.$piezas.'</td>
            <td class="title_font_size border" colspan="3" align="center">'.$clasificacion.'</td>
          </tr>
          <tr>
            <td class="title_font_size" colspan="2" align="center">TOTAL DE PESO</td>
            <td class="title_font_size" colspan="3" align="center">TOTAL VOLUMEN</td>
          </tr>
          <tr>
            <td class="title_font_size border" colspan="2" align="center">'.$peso.'</td>
            <td class="title_font_size border" colspan="3" align="center">'.$volument.' M <sup>3</sup></td>
          </tr>
          <tr>
            <td class="title_font_size "  align="center">PESO</td>
            <td class="title_font_size "  align="center">VOL.</td>
            <td class="title_font_size "  align="center">LARGO</td>
            <td class="title_font_size "  align="center">ANCHO</td>
            <td class="title_font_size "  align="center">ALTO</td>
          </tr>
          <tr>
            <td class="title_font_size" align="center">CONSIGNATARIO</td>
            <td colspan="5" rowspan="5">
              <table border="1">';
              foreach ($result_paquetes->result() as $item) {
                  $volumen=round(((($item->paq_alt*$item->paq_anc*$item->paq_lar)/6000)*$item->paq_num),2);
                $html.='<tr>
                          <td class="title_font_size border" >'.$item->paq_pes.'</td>
                          <td class="title_font_size border" >'.$volumen.'</td>
                          <td class="title_font_size border" >'.$item->paq_lar.'</td>
                          <td class="title_font_size border" >'.$item->paq_anc.'</td>
                          <td class="title_font_size border" >'.$item->paq_alt.'</td>
                        </tr>';
              }
            $html.='</table>
            </td>
          </tr>
          <tr>
            <td class="title_font_size border" rowspan="6" >'.$consign.'</td>
            <td class="title_font_size" ></td>
          </tr>
          <tr>
            <td class="title_font_size"></td>
          </tr>
          <tr>
            <td class="title_font_size"></td>
          </tr>
          <tr>
            <td class="title_font_size"></td>
          </tr>
          <tr>
            <td class="title_font_size"></td>
            <td class="title_font_size" colspan="5"></td>
          </tr>
          <tr>
            <td class="title_font_size"></td>
            <td colspan="5" class="title_font_size" align="center">CARGOS</td>
          </tr>
          <tr>
            <td class="title_font_size" align="center">OBSERVACIONES</td>
            <td class="title_font_size"></td>
            <td class="title_font_size border" align="right">'.number_format($doc_tars->tar_ce,2,'.',',').'</td>
            <td class="title_font_size" align="center">0</td>
            <td class="title_font_size border" colspan="3">'.$doc_tars->tar_cen.'</td>
          </tr>
          <tr>
            <td class="title_font_size border" rowspan="3" align="center">'.$obser.'</td>
            <td class="title_font_size"></td>
            <td class="title_font_size border" align="right">'.number_format($doc_tars->tar_cf,2,'.',',').'</td>
            <td class="title_font_size" align="center">1</td>
            <td class="title_font_size" colspan="3">CARGO FIJO</td>
          </tr>
          <tr>
            <td class="title_font_size"></td>
            <td class="title_font_size border" align="right">'.number_format($doc_tars->tar_pov,2,'.',',').'</td>
            <td class="title_font_size" align="center">2</td>
            <td class="title_font_size" colspan="3">PESO O VOLUMEN</td>
          </tr>
          <tr>
            <td class="title_font_size"></td>
            <td class="title_font_size border" align="right">'.number_format($doc_tars->tar_fer,2,'.',',').'</td>
            <td class="title_font_size" align="center">3</td>
            <td class="title_font_size" colspan="3">FLETE / EMPAQUE SEGURO</td>
          </tr>
          <tr>
            <td class="border" rowspan="3" style="font-size:7px">'.$observaciong.'</td>
            <td class="title_font_size"></td>
            <td class="title_font_size border" align="right">'.number_format($doc_tars->tar_cpc,2,'.',',').'</td>
            <td class="title_font_size" align="center">4</td>
            <td class="title_font_size" colspan="3">COMBUSTIBLE</td>
          </tr>
          <tr>

            <td class="title_font_size"></td>
            <td class="title_font_size border" align="right">'.number_format($doc_tars->tar_sub,2,'.',',').'</td>
            <td class="title_font_size" align="center">5</td>
            <td class="title_font_size" colspan="3">SUBTOTAL</td>
          </tr>
          <tr>
            <td class="title_font_size"></td>
            <td class="title_font_size border" align="right">'.number_format($doc_tars->tar_iva,2,'.',',').'</td>
            <td class="title_font_size" align="center">6</td>
            <td class="title_font_size" colspan="3">IMPORTE I.V.A</td>
          </tr>
          <tr>
            <td class="title_font_size"></td>
            <td class="title_font_size"></td>
            <td class="title_font_size border" align="right">'.number_format($doc_tars->tar_tot,2,'.',',').'</td>
            <td class="title_font_size" align="center">7</td>
            <td class="title_font_size" colspan="3">TOTAL</td>
          </tr>
          <tr>
            <td class="title_font_size" colspan="6" ></td>
            
          </tr>
          <tr>
            <td class="title_font_size border_bottom"></td>
            <td class="title_font_size"></td>
            <td class="title_font_size border_bottom" colspan="5" align="center">'.$usuario.'</td>
          </tr>
          <tr>
            <td class="title_font_size" align="center">NOMBRE Y FIRMA DE REMITENTE DEL ENVIO</td>
            <td class="title_font_size"></td>
            <td class="title_font_size" align="center" colspan="5">FIRMA POR '.$aerolinea.'</td>
          </tr>
          

        </table>';
$html1='<table border="0">
        <tr>
          <td style="font-size:7px" align="right">Guia para estación</td>
        </tr>
       </table>';
$pdf->writeHTML($html.$html1, true, false, true, false, '');

$html1='<table border="0">
        <tr>
          <td style="font-size:7px" align="left">Copia Cliente</td>
        </tr>
       </table>';
$pdf->writeHTML($html.$html1, true, false, true, false, '');
//=============================================
  // Ruta al PDF externo
  $externalPDF = FCPATH.'public/files/'.$filecontrato;
  //log_message('error',$externalPDF);
  
  // Obtener el número de páginas del PDF externo
  $pageCount = $pdf->setSourceFile($externalPDF);
  // Importar cada página del PDF externo
  for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
      $tplIdx = $pdf->importPage($pageNo);
      $pdf->AddPage();
      $pdf->useTemplate($tplIdx, 0, 10, 210);//file, posición x posición y, ancho(200 por defecto)
  }
  




//=============================================
$url=FCPATH.'/docdirectorio/';
//$url=$_SERVER['DOCUMENT_ROOT'].'/hulesyg/facturaspdf/';
if($file=='file1'){
  $urlg=$url.'cotizacion_'.$cotizacion.'_'.$folio.'.pdf';  
}else{
  $urlg=$url.'cotizacion_'.$folio.'.pdf';  
}


$pdf->Output($urlg, 'FI');

//$pdf->Output('Cotizacion_'.$cotizacion.'.pdf', 'I');

//$pdf->Output('../../facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
//$pdf->Output('/facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
?>