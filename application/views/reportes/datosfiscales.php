<?php
require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF/tcpdf.php';
$this->load->helper('url');
/*
fac_rfc=XAXX010101000
fac_nrs=general
fac_dir=general
fac_cod=72030
fac_email=ejemplo%40hotmail.com
fac_te1=1254783690&RegimenFiscalReceptor=601&forma_pago=01&metodo_pago=PUE&uso_cfdi=G03
*/
$RegimenFiscalReceptor = $_GET['RegimenFiscalReceptor'];
$RegimenFiscalReceptor = $this->ModeloCatalogos->regimenf($RegimenFiscalReceptor);

$forma_pago = $_GET['forma_pago'];
$result_fp=$this->ModeloCatalogos->getselectwheren('f_formapago',array('clave'=>$forma_pago));
foreach ($result_fp->result() as $item) {
  $forma_pago=$item->formapago_text;
}

$metodo_pago = $_GET['metodo_pago'];
$result_mp=$this->ModeloCatalogos->getselectwheren('f_metodopago',array('metodopago'=>$metodo_pago));
foreach ($result_mp->result() as $item) {
  $metodo_pago=$item->metodopago_text;
}

$uso_cfdi = $_GET['uso_cfdi'];
$result_cfdi=$this->ModeloCatalogos->getselectwheren('f_uso_cfdi',array('uso_cfdi'=>$uso_cfdi));
foreach ($result_cfdi->result() as $item) {
  $uso_cfdi=$item->uso_cfdi_text;
}
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('agb');
$pdf->SetTitle('Datosfiscales ');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);



// set margins
$pdf->SetMargins('15', '10', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, '10');// margen del footer

$pdf->SetFont('dejavusans', '', 9);
$pdf->AddPage();
//$logos = base_url().'public/img/alta.png';
//      $logos2 = base_url().'public/img/kyocera.png';
$urlimage=base_url().'public/img/section-image-1.png';
$table='<table border="0" align="center">
        <tr>
          <td><img src="'.$urlimage.'" width="140px"></td>
        </tr>
      </table>
      <p></p>
      <table border="0" cellpadding="6">
        <tr>
          <td class="border" width="35%">RFC:</td>
          <td class="border" width="65%">'.$_GET['fac_rfc'].'</td>
        </tr>
        <tr>
          <td class="border">NOMBRE O RAZON SOCIAL:</td>
          <td class="border">'.$_GET['fac_nrs'].'</td>
        </tr>
        <tr>
          <td class="border">DOMICILIO FISCAL:</td>
          <td class="border">'.$_GET['fac_dir'].'</td>
        </tr>
        <tr>
          <td class="border">CODIGO POSTAL:</td>
          <td class="border">'.$_GET['fac_cod'].'</td>
        </tr>
        <tr>
          <td class="border">CORREO ELECTRONICO:</td>
          <td class="border">'.$_GET['fac_email'].'</td>
        </tr>
        <tr>
          <td class="border">REGIMEN FISCAL:</td>
          <td class="border">'.$RegimenFiscalReceptor.'</td>
        </tr>
        <tr>
          <td class="border">FORMA DE PAGO:</td>
          <td class="border">'.$forma_pago.'</td>
        </tr>
        <tr>
          <td class="border">METODO DE PAGO:</td>
          <td class="border">'.$metodo_pago.'</td>
        </tr>
        <tr>
          <td class="border">USO CFDI:</td>
          <td class="border">'.$uso_cfdi.'</td>
        </tr>
      </table>
      <table align="center" cellpadding="6">
        <tr>
          <td width="48%" class="border" height="70PX" ></td>
          <td width="4%" rowspan="2"></td>
          <td width="48%" class="border"></td>
        </tr>
        <tr>
          <td class="border">NOMBRE DEL CLIENTE, FIRMA Y FECHA</td>
          <td class="border">NOMBRE DEL DOCUMENTADOR, FECHA Y FIRMA</td>
        </tr>
      </table>';
$html='<style type="text/css">
                .borderbottom{
                    border-bottom:1px solid black;
                }
                .borderright{
                    border-right:1px solid black;
                }
                .border{
                    border:1px solid #525659;
                    padding;10px;
                }
                .backg{
                    background-color:#c6c9cb;
                }
      </style>
      '.$table.'<p></p><p></p>'.$table;

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Datosfiscales.pdf', 'I');

//$pdf->Output('../../facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
//$pdf->Output('/facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
?>