<?php
require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF/tcpdf.php';
$this->load->helper('url');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('agb');
$pdf->SetTitle('Orden de servicio '.$codigo);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);



// set margins
$pdf->SetMargins('10', '10', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);// margen del footer

$pdf->SetFont('dejavusans', '', 8);
$pdf->AddPage();
  $ser_sers_a=explode('~',$datos->ser_sers); 
  $ser_sers_al='';
  foreach ($ser_sers_a as $item) {
    $ser_sers_al.=$item.', ';
  }

  $ser_unis=explode(',',$datos->ser_unis); 
  $ser_unisl='';
  foreach ($ser_unis as $item) {
      $resulte= $this->ModeloCatalogos->getselectwheren('pam_equ',array('equipo'=>$item));
      foreach ($resulte->result() as $iteme) {
          $ser_unisl.=$iteme->equ_numeco.' -> '.$iteme->equ_descrip.', ';
      }
  }
  $resultu=$this->ModeloCatalogos->getselectwheren('jsi_sis_admins',array('usuario'=>$datos->usuario));
  $usuarioe='';
  foreach ($resultu->result() as $itemu) {
    $usuarioe=$itemu->nombre;
  }
                                                            

$urlimage=base_url().'public/img/cadipa.jpg';
$html='<style type="text/css">
            .title_font_size{
              font-size: 7px;
            }
            .borders{
              border:1px solid #808080;
            }
            .border_bottom{
              border-bottom: 1px solid #808080;
            }
           
          </style>';
$html.='<table border="1" cellpadding="5">
          <tr>
            <td width="20%">
              <img src="'.$urlimage.'" width="170px">
            </td>
            <td width="60%" style="font-size:7px;">Tehuantepec 7° Infonavit El rosario, San Sebastián Tula, Oaxaca, C.P. 71246 RFC. CAD070509Q18 Tel. 2222905774  www.cadipalogistics.com diazcarga@gmail.com</td>
            <td width="20%">
              ORDEN DE SERVICIO
            </td>
          </tr>
        </table>
        <table border="0"><tr><td></td></tr></table>

        <table border="0" cellpadding="2" align="center">
          <tr>
            <td>NO SERVICIO</td>
            <td class="borders">'.$datos->ser_fol.'</td>
            <td>FECHA/HORA</td>
            <td class="borders">'.$datos->ser_fec.'</td>
            <td>NO. CAJAS</td>
            <td class="borders">'.$datos->ser_cajas.'</td>
            <td></td>
          </tr>
          <tr>
            <td rowspan="2">ORIGEN</td>
            <td rowspan="2" class="borders">'.$datos->ser_origen.'</td>
            <td rowspan="2">DESTINO</td>
            <td rowspan="2" class="borders">'.$datos->ser_destino.'</td>
            <td>NO. GUIA</td>
            <td class="borders">'.$datos->ser_guia.'</td>
            <td>KG</td>
          </tr>
          <tr>
            <td>NO. PIEZAS</td>
            <td class="borders" >'.$datos->ser_piezas.'</td>
            <td class="borders" >'.$datos->ser_kilos.'</td>
          </tr>
          <tr>
            <td>CLIENTE CORPORATIVO</td>
            <td class="borders" colspan="2"></td>
            <td colspan="2">EMPRESA SERVICIO</td>
            <td colspan="2" class="borders">'.$datos->ser_clis.'</td>
          </tr>
          <tr>
            <td>SERVICIO ESPECÍFICO</td>
            <td class="borders" colspan="7">'.$ser_sers_al.'</td>
          </tr>
          <tr>
            <td>UNIDADES O EQUIPO UTILIZADO</td>
            <td class="borders" colspan="7">'.$ser_unisl.'</td>
          </tr>
          <tr>
            <td>PERSONAL</td>
            <td class="borders" colspan="7">'.$datos->ser_pers.'</td>
          </tr>
          <tr>
            <td>TIPO COBRO</td>
            <td class="borders">'.$datos->ser_fdp.'</td>
            <td>ESPECIFICADO</td>
            <td class="borders">'.$datos->ser_fdpdes.'</td>
          </tr>
          <tr>
            <td>OBSERVACIONES</td>
            <td class="borders" colspan="7">'.$datos->ser_obs.'</td>
          </tr>
        </table>
        <table border="0"><tr><td height="40px"></td></tr></table>
        <table border="0" align="center">
          <tr>
            <td class="border_bottom">'.$usuarioe.'</td>
            <td class="border_bottom">'.$datos->ser_enc.'</td>
            <td class="border_bottom">'.$datos->ser_firs.'</td>
          </tr>
          <tr>
            <td>ELABORA</td>
            <td>ENCARGADO DE OPERACIÓN</td>
            <td>SE RECABA FIRMA DE</td>
          </tr>
        </table>
        ';
    $html0='<style type="text/css">
            .border_bottom{
              border-bottom: 1px solid #808080;
            }
           
          </style>
          <table border="0" align="center">
          <tr><td></td></tr>
          <tr>
            <td class="border_bottom"></td>
          </tr>
          <tr><td height="40px"></td></tr>
        </table>
        ';
$pdf->writeHTML($html.$html0.$html, true, false, true, false, '');

$pdf->Output('Orden_de_servicio'.$codigo.'.pdf', 'I');

//$pdf->Output('../../facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
//$pdf->Output('/facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
?>