<style type="text/css">
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
    .fc-header-toolbar{
        width: 100%;
    }
</style>  
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Vuelos</div>
                            <p class="content-sub-header mb-1">Listado de vuelos</p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12" style="text-align: end;">
                                                        <a type="button" class="btn btn-success mr-1 mb-1" onclick="cargarvuelos()">Agregar</a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label>Origen</label>
                                                        <select class="form-control" id="select_o" onchange="updatecalendar()">
                                                            <option value=""></option>
                                                            <?php foreach ($result_o->result() as $item) { ?>
                                                                <option><?php echo $item->origen;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Destino</label>
                                                        <select class="form-control" id="select_d" onchange="updatecalendar()">
                                                            <option value=""></option>
                                                            <?php foreach ($result_d->result() as $item) { ?>
                                                                <option><?php echo $item->destino;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>No de vuelo</label>
                                                        <input type="text" id="novuelo" class="form-control" onchange="updatecalendar()">
                                                    </div>
                                                </div>
                                                <div class="row" id="calendario">
                                                    
                                                </div>
                                                <div class="row" id="calendario2">
                                                    
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->
<div class="modal fade text-left" id="modalcargavuelos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Carga Vuelos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <a href="<?php echo base_url();?>files_vuelos/malla.xlsx" class="btn btn-social-icon btn-outline-linkedin mr-2" title="Plantilla de ejemplo"><i class="fa fa-file-excel-o"></i> </a>
                    </div>
                    <div class="col-md-12">
                        <input type="file" name="documento" id="documento" >
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modalinfocalendario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Información</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <label><b>Origen:</b></label>
                    </div>
                    <div class="col-md-6 vorigen">
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label><b>Destino:</b></label>
                    </div>
                    <div class="col-md-6 vdestino">
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 ">
                        <label><b>Aerolínea:</b></label>
                    </div>
                    <div class="col-md-3 vaerolinea">
                        
                    </div>
                    <div class="col-md-3">
                        <label><b>Vuelo:</b></label>
                    </div>
                    <div class="col-md-3 vvuelo">
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 ">
                        <label><b>Salida</b></label>
                    </div>
                    <div class="col-md-3 vsalida">
                        
                    </div>
                    <div class="col-md-3 ">
                        <label><b>Llegada</b></label>
                    </div>
                    <div class="col-md-3 vllegada">
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 ">
                        <label><b>Fecha:</b></label>
                    </div>
                    <div class="col-md-6 vfecha">
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-success btn_edit" onclick="editar_reg()">Editar</button>
                <button type="button" class="btn bg-light-warning btn_canl" onclick="cancelar_reg()">Cancelar</button>
                <button type="button" class="btn bg-light-danger" onclick="eliminar_reg()">Eliminar</button>
                <!--<button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>-->
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modalinfocalendario_editar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-ls">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Editar horario</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3 form-group">
                        <label><b>Salida</b></label>
                    </div>    
                    <div class="col-md-6 form-group">
                        <input type="hidden" class="form-control" id="id_reg">
                        <input type="time" class="form-control" id="salida_edit">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 form-group">
                        <label><b>Llegada</b></label>
                    </div>    
                    <div class="col-md-6 form-group">
                        <input type="time" class="form-control" id="llegada_edit">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 form-group">
                        <label><b>Fecha</b></label>
                    </div>    
                    <div class="col-md-6 form-group">
                        <input type="date" class="form-control" id="fecha_edit">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-success" onclick="editar_vuelo()">Aceptar</button>
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>