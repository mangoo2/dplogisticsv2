<style type="text/css">
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
</style>  
<input type="hidden" id="sucursal" value="<?php echo $codigo?>">          
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Clientes</div>
                            <p class="content-sub-header mb-1"><?php echo $suc_nombre?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                        <div class="box-tools pull-right">
                                            <a href="<?php echo base_url();?>Coe_clientes" type="button" class="btn btn-sm btn-light mr-1 mb-1"><i class="fa fa-arrow-left" style="color:black;"></i></a>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="<?php echo base_url();?>Clientes/add?suc=<?php echo $codigo?>" type="button" class="btn btn-success mr-1 mb-1" target="_blank">Agregar</a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 table-bordered table-striped thead-light tablevc" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Cliente</th>
                                                                    <th>Teléfono</th>
                                                                    <th>Celular</th>
                                                                    <th>Email</th>
                                                                    <th>Identificación</th>
                                                                    <th>Cumpleaños</th>
                                                                    <th>Comentario</th>
                                                                    <th>Descuento</th>
                                                                    <th>Crédito</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->