<script src="<?php echo base_url();?>app-assets/vendors/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>app-assets/css/plugins/form-validation.css">

<script src="<?php echo base_url();?>public/plugins/fileinput/fileinput.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>public/plugins/fileinput/fileinput.min.css">


<script type="text/javascript">
    var var_rid;
    var var_aer_nom;
    var var_aer_cla;
    var var_aer_pos;
    var var_emp;

    $("#aer_img").fileinput({
            showCaption: false,
            dropZoneEnabled: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["png","jpg","jpeg","bmp"],
            browseLabel: 'Seleccionar Imagen',
            uploadUrl: '<?php echo base_url();?>Conf_aers/inserupdate',
            inputGroupClass: "input-group-sm",
            maxFilePreviewSize: 5000,
            uploadExtraData: function (previewId, index) {
                var info = {
                            rid:var_rid,
                            emp:var_emp,
                            aer_cla:var_aer_cla,
                            aer_nom:var_aer_nom,
                            aer_pos:var_aer_pos

                        };
                return info;
            }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
        }).on('filebatchuploadsuccess', function(event, files, extra) {
          location.reload();
          //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        });

    
    function editar(id){
        if(id>0){
        $('#exampleModalLiveLabel').html('Editar aerolinea');
        $('.saveform').html('Actualizar');
        $('#modalform').modal({backdrop: 'static', keyboard: false});

        $('#rid').val(id);
        var emp = $('.edit_form_'+id).data('emp');
        $('#emp').val(emp);

        var aer_cla = $('.edit_form_'+id).data('aer_cla');
        $('#aer_cla').val(aer_cla);

        var aer_nom = $('.edit_form_'+id).data('aer_nom');
        $('#aer_nom').val(aer_nom);

        var aer_pos = $('.edit_form_'+id).data('aer_pos');
        $('#aer_pos').val(aer_pos);

        var aer_img = $('.edit_form_'+id).data('aer_img');
        $('.aer_img').html('<img src="<?php echo base_url()?>_files/_coe/'+aer_img+'">');
        }else{
            $('#exampleModalLiveLabel').html('Agregar aerolinea');
            $('.saveform').html('Agregar');
            $('#modalform').modal({backdrop: 'static', keyboard: false});
            $('#form_datos')[0].reset();

        }
    }
    function saveform() {
        var_rid=$('#rid').val();
        var_emp=$('#emp option:selected').val();
        var_aer_cla=$('#aer_cla').val();
        var_aer_nom=$('#aer_nom').val();
        var_aer_pos = $('#aer_pos').val();
        var varform=$('#form_datos');
        if(varform.valid()){
             $('#aer_img').fileinput('upload'); 
              
        }
    }
    function deleteaer(id){
        $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma borrar la aerolinea',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: "<?php echo base_url();?>index.php/Conf_aers/delete",
                        data: {
                            rid:id
                        },
                        success: function (response){
                            toastr["success"]("Aerolinea eliminada", "Hecho"); 
                            location.reload();                         
                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
    }
</script>