<style type="text/css">
    #table_ser_unis label{
        font-size: 13px;
    }
</style>        
 
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Servicios RAMPA</div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <h5 id="title_new_edit">Nuevo Servicio</h5>
                                                    </div>
                                                    <div class="col-md-9" style="text-align:right;">
                                                        <a class="btn btn-secondary mr-1 mb-1" href="<?php echo base_url();?>Servicios_RAMPA">Cerrar</a>
                                                        <button class="btn btn-success mr-1 mb-1 saveform" id="saveform">Guardar</button>
                                                    </div>
                                                </div>
                                                <form class="form" id="form_datos">
                                                    <input type="hidden" id="ser" name="ser" value="<?php echo $codigo;?>" readonly>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label>Fecha / Hora</label>
                                                            <input type="datetime-local" name="ser_fecini" id="ser_fecini" value="" class="form-control">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>NO. GUIA</label>
                                                            <input type="text" name="ser_guia" id="ser_guia" value="" class="form-control">
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label>Forma de pago</label>
                                                            <select class="form-control" name="ser_fdp" id="ser_fdp">
                                                                <option value="">N / A</option>
                                                                <option value="corporativo">Corporativo</option>
                                                                <option value="efectivo">Efectivo</option>
                                                                <option value="credito">Crédito</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label>Especificar</label>
                                                            <input type="text" name="ser_fdpdes" id="ser_fdpdes" value="" class="form-control">
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label>Vuelo</label>
                                                            <input type="text" name="ser_vuelo" id="ser_vuelo" value="" class="form-control">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>NO. PIEZAS</label>
                                                            <input type="number" name="ser_piezas" id="ser_piezas" value="" class="form-control">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>ENCARGADO DE OPERACIÓN</label>
                                                            <input type="text" name="ser_enc" id="ser_enc" value="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label>Aeronave</label>
                                                            <input type="text" name="ser_aeronave" id="ser_aeronave" value="" class="form-control">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>KG</label>
                                                            <input type="number" name="ser_kilos" id="ser_kilos" value="" class="form-control">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>SE RECABA FIRMA DE</label>
                                                            <input type="text" name="ser_firs" id="ser_firs" value="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label>RUTA ORIGEN</label>
                                                            <input type="text" name="ser_origen" id="ser_origen" value="" class="form-control">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>CLIENTE CORPORATIVO</label>
                                                            <input type="text" name="ser_clicorp" id="ser_clicorp" value="" class="form-control">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>OBSERVACIONES</label>
                                                            <input type="text" name="ser_obs" id="ser_obs" value="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label>RUTA DESTINO</label>
                                                            <input type="text" name="ser_destino" id="ser_destino" value="" class="form-control">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>EMPRESA A LA QUE SE PRESTA EL SERVICIO</label>
                                                            <select class="form-control" name="ser_clis" id="ser_clis">
                                                                <?php foreach ($ser_clis_result->result() as $item) { ?>
                                                                    <option value="<?php echo $item->cli;?>"><?php echo $item->cli.' -> '.$item->cli_nom;?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </form>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <div class="row">
                        <div class="col-md-6" style="padding-right:6px; padding-left: 6px;">
                            <div class="card" style="height: 478px;">
                                <div class="card-header">
                                    <h3 class="card-title">SERVICIO ESPECÍFICO</h3>
                                </div>
                                <div class="card-content">
                                    <div class="card-body" style="padding:10px;">
                                        <table id="table_ser_sers">
                                            <tbody>
                                            <?php 
                                                $servicios=array();
                                                $servicios[]=array('valor'=>'carga-llegada','titulo'=>'CARGA LLEGADA');
                                                $servicios[]=array('valor'=>'carga-salida','titulo'=>' CARGA SALIDA');
                                                $servicios[]=array('valor'=>'atencion-vuelo','titulo'=>' ATENCION VUELO');
                                                $servicios[]=array('valor'=>'atencion-extra-vuelo','titulo'=>' ATENCION EXTRA VUELO');
                                                $servicios[]=array('valor'=>'comat','titulo'=>'COMAT');
                                                $servicios[]=array('valor'=>'guia-internacional','titulo'=>'LIBERACION GUIA INTERNACIONAL');
                                                $servicios[]=array('valor'=>'renta-equipo','titulo'=>'RENTA EQUIPO');
                                                $servicios[]=array('valor'=>'otro','titulo'=>'OTRO');
                                                foreach ($servicios as $item) { ?>
                                                    <tr>
                                                        <td>
                                                            <div class="input-group servicio-especifico_<?php echo $item['valor']?>">
                                                                <div class="input-group-prepend">
                                                                    <div class="input-group-text">
                                                                        <label>
                                                                            <input type="checkbox" id="servicio-especifico" name="servicio-especifico" value="<?php echo $item['valor']?>" >
                                                                            <?php echo $item['titulo']?>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <input type="text" class="form-control <?php echo $item['valor']?>" placeholder="Especificar" id="especificar">
                                                            </div>
                                                        </td>
                                                    </tr>

                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3" style="padding-right:6px; padding-left: 6px;">
                            <div class="card" style="height: 478px;">
                                <div class="card-header">
                                    <h3 class="card-title">PERSONAL QUE PRESTA EL SERVICIO</h3>
                                </div>
                                <div class="card-content">
                                    <div class="card-body" style="padding:10px;">
                                        <table id="table_ser_per_pres">
                                            <tbody>
                                            <?php foreach ($ser_per_pres->result() as $item) { ?>
                                                <tr>
                                                    <td>
                                                        <label><input type="checkbox" id="per_pre_ser" name="per_pre_ser" value="<?php echo $item->usuario?>" >
                                                        <?php echo $item->nombre;?></label>
                                                    </td>
                                                </tr>
                                            <?php }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3" style="padding-right:6px; padding-left: 6px;">
                            <div class="card" style="height: 478px;">
                                <div class="card-header">
                                    <h3 class="card-title">UNIDADES O EQUIPO UTILIZADO PARA EL SERVICIO</h3>
                                </div>
                                <div class="card-content">
                                    <div class="card-body" style="padding:10px;">
                                        <table id="table_ser_unis">
                                            <tbody>
                                            <?php foreach ($ser_unis->result() as $item) { ?>
                                                <tr>
                                                    <td>
                                                        <label><input type="checkbox" id="ser_sers" name="ser_sers" value="<?php echo $item->equipo?>" >
                                                        <?php echo $item->equ_numeco.' > '.$item->equ_nombre;?></label>
                                                    </td>
                                                </tr>
                                            <?php }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->