<style type="text/css">
    #table_ser_unis label{
        font-size: 13px;
    }
</style>        
 
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Servicios RAMPA</div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <h5 id="title_new_edit">Nuevo Servicio</h5>
                                                    </div>
                                                    <div class="col-md-9" style="text-align:right;">
                                                        <a class="btn btn-secondary mr-1 mb-1" href="<?php echo base_url();?>Servicios_RAMPA">Cerrar</a>
                                                        <a class="btn btn-info mr-1 mb-1" href="<?php echo base_url()?>Servicios_Rye/cad_ver/<?php echo $codigo;?>" target="_blank">Imprimir</a>
                                                        <button class="btn btn-success mr-1 mb-1 saveform" id="saveform">Finalizar</button>
                                                    </div>
                                                </div>
                                                <form class="form" id="form_datos">
                                                    <input type="hidden" id="ser" name="ser" value="<?php echo $codigo;?>" readonly>
                                                    <div class="row">
                                                        <div class="col-md-5 form-group">
                                                            <label>Fecha / Hora</label>
                                                            <input type="datetime-local" name="ser_fecfin" id="ser_fecfin" value="" class="form-control" required>
                                                        </div>
                                                        <div class="col-md-7 form-group">
                                                            <label>Observaciones / Incidencias RAMPA</label>
                                                            <input type="text" name="ser_finobs" id="ser_finobs" value="" class="form-control">
                                                        </div>
                                                    </div>
                                                </form>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5>Detalles</h5>
                                                        </div>
                                                        <div class="col-4">
                                                            <p><b>Fecha: </b><?php echo $datos->ser_fec;?></p>
                                                            <p><b>RUTA ORIGEN: </b><?php echo $datos->ser_origen;?></p>
                                                            <p><b>RUTA DESTINO: </b><?php echo $datos->ser_destino;?></p>
                                                            <p><b>NO. GUÍA: </b><?php echo $datos->ser_guia;?></p>
                                                        </div>
                                                        <div class="col-4">
                                                            
                                                            <p><b>NO. PIEZAS: </b><?php echo $datos->ser_piezas;?></p>
                                                            <p><b>KG: </b><?php echo $datos->ser_kilos;?></p>
                                                            <p><b>NO. CAJAS: </b><?php echo $datos->ser_cajas;?></p>
                                                            <p><b>FOLIO / REMISIÓN / IMR: </b><?php echo $datos->ser_fri;?></p>
                                                            <p><b>EMPRESA A LA QUE SE PRESTA EL SERVICIO: </b><?php echo $datos->ser_clis;?></p>
                                                        </div>
                                                        <div class="col-4">
                                                            <p><b>Forma de pago: </b><?php echo $datos->ser_fdp;?></p>
                                                            <p><b>Especificar: </b><?php echo $datos->ser_fdpdes;?></p>
                                                            <p><b>ENCARGADO DE OPERACIÓN: </b><?php echo $datos->ser_enc;?></p>
                                                            <p><b>SE RECABA FIRMA DE: </b><?php echo $datos->ser_firs;?></p>
                                                            <p><b>OBSERVACIONES: </b><?php echo $datos->ser_obs;?></p>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top:20px;">
                                                        <div class="col-4">
                                                            <h5>SERVICIO ESPECÍFICO</h5>
                                                            <?php $ser_sers_a=explode('~',$datos->ser_sers); 
                                                                foreach ($ser_sers_a as $item) {
                                                                ?><p><?php echo $item;?></p><?php
                                                                }
                                                            ?>
                                                        </div>
                                                        <div class="col-4">
                                                            <h5>PERSONAL QUE PRESTA EL SERVICIO</h5>
                                                            <?php $ser_pers=explode(',',$datos->ser_pers); 
                                                                foreach ($ser_pers as $item) {
                                                                ?><p><?php echo $item;?></p><?php
                                                                }
                                                            ?>
                                                        </div>
                                                        <div class="col-4">
                                                            <h5>UNIDADES O EQUIPO UTILIZADO PARA EL SERVICIO</h5>
                                                            <?php $ser_unis=explode(',',$datos->ser_unis); 
                                                                foreach ($ser_unis as $item) {
                                                                    $resulte= $this->ModeloCatalogos->getselectwheren('pam_equ',array('equipo'=>$item));
                                                                    foreach ($resulte->result() as $iteme) {
                                                                        echo '<p>'.$iteme->equ_numeco.' -> '.$iteme->equ_descrip.'</p>';
                                                                    }
                                                                }
                                                            ?>
                                                        </div>
                                                    </div>
                                            <!--------------->
                                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                   
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->