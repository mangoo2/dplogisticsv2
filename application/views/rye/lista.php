<style type="text/css">
    #table_list th,#table_list td{
        font-size: 12px;
        padding-left: 5px;
        padding-right: 5px;
    }
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
    .td_colum_9{
        max-width: 90px;
    }
    #table_list{
        width: 100% !important;
    }
</style>            
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Servicios RyE</div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Últimos servicios</h4>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-11">
                                                        <a href="<?php echo base_url();?>Servicios_Rye/cad_agr" type="button" class="btn btn-success mr-1 mb-1">Agregar</a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table table-bordered m-0" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>Folio</th>
                                                                    <th>Fecha</th>
                                                                    <th>Guia</th>
                                                                    <th>Origen</th>
                                                                    <th>Destino</th>
                                                                    <th>Cliente</th>
                                                                    <th>Servicio</th>
                                                                    <th>Observaciones / incidencias<br>Inicio / *Fin</th>
                                                                    <th class="td_colum_9">Estatus</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->