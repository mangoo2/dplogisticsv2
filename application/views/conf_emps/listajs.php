<script src="<?php echo base_url();?>app-assets/vendors/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>app-assets/css/plugins/form-validation.css">
<script type="text/javascript">
    function editar(id){
        $('#modalform').modal({backdrop: 'static', keyboard: false});
        $('#rid').val(id);
        var nombre = $('.edit_form_'+id).data('nombre');
        var pos = $('.edit_form_'+id).data('pos');
        $('#emp_nom').val(nombre);
        $('#emp_pos').val(pos);
    }
    function saveform() {
        var varform=$('#form_datos');
        if(varform.valid()){
            var datos =varform.serialize(); 
              $.ajax({
                type:'POST',
                url: "<?php echo base_url();?>index.php/Conf_emps/saveform",
                data: datos,
                success: function (response){
                  $('#modalform').modal('hide');
                  
                  toastr["success"]("Informacion Actualizada", "Hecho"); 
                  
                  setTimeout(function(){
                    location.reload();
                  }, 2000);

                },
                error: function(response){
                    toastr["error"]("Algo salio mal", "Error"); 
                     
                }
              });  
        }
    }
</script>