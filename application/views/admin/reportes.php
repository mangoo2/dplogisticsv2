<style type="text/css">
    #table_list th,#table_list td{
        font-size: 12px;
    }
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
    #tablegindivi td{
        padding-top: 10px;
        padding-bottom: 10px;
    }
    #tablegindivi,#tableg,#table_valor_cli{
        font-size: 10px;
    }
    #tablegindivi td,#tablegindivi th{
        padding: 1.15rem 1rem !important;
    }
    #table_valor_cli td,#table_valor_cli th{
        padding: 0.9rem 0.4rem !important;
    }
    .select2-container{
        width: 100% !important;
    }
</style>            
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Reporte</div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <form id="formfintros">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label>Sucursal</label>
                                                            <select id="sucursal" name="sucursal" class="form-control" >
                                                                <option value="0">Todas</option>
                                                                <?php foreach ($resultsuc as $item) { ?>
                                                                    <option value="<?php echo $item->sucursal;?>"><?php echo $item->suc_nombre;?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label>Fecha inicial</label>
                                                            <input type="date" class="form-control" id="f_inicial">
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label>Fecha final</label>
                                                            <input type="date" class="form-control" id="f_final">
                                                        </div>
                                                        <!--<div class="col-md-2">
                                                            <label>Año</label>
                                                            <select id="anio" name="anio[]" class="form-control" multiple="multiple">
                                                                <?php 
                                                                    $anioactual=date('Y');
                                                                    $anioactual2=$anioactual-10;

                                                                    for ($i = $anioactual; $i >= $anioactual2; $i--) {
                                                                        echo $i.'<br>';
                                                                        ?><option value="<?php echo $i;?>" <?php if($i==date('Y')){ echo 'selected'; }?> ><?php echo $i;?></option><?php
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>-->
                                                        <div class="col-md-2">
                                                            <label>Top</label>
                                                            <select id="top" name="top" class="form-control">
                                                                <option>5</option>
                                                                <option>10</option>
                                                                <option>20</option>
                                                                <option>30</option>
                                                            </select>
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                </form>
                                                <!-- Nav tabs -->
                                                <br>
                                            <ul class="nav nav-tabs" role="tablist">
                                              <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#General" role="tab">General</a>
                                              </li>
                                              <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#Detalles" role="tab">Detalles</a>
                                              </li>
                                              <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#ficha" role="tab">Ficha de valor del cliente</a>
                                              </li>
                                              <li class="nav-item">
                                                <!--<a class="nav-link" data-toggle="tab" href="#settings" role="tab">Settings</a>-->
                                              </li>
                                            </ul>
                                            <br>
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="General" role="tabpanel">
                                                    <!--------------------------------------------------------------->
                                                        <div class="row">
                                                            <div class="col-md-12" style="text-align: end;">
                                                                <a onclick="generarclig()" type="button" class="btn btn-success mr-1 mb-1">Generar reporte</a>
                                                            </div>
                                                        </div>
                                                    
                                                        <div class="row">
                                                            <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5 divtableg">
                                                                
                                                            </div>
                                                            <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7 classmybarChart">
                                                                <canvas id="mybarChart"></canvas>
                                                            </div>
                                                        </div>
                                                    <!--------------------------------------------------------------->
                                                </div>
                                              <div class="tab-pane" id="Detalles" role="tabpanel">
                                                    <!--------------------------------------------------------------->
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label for="Cliente" class="label">Cliente</label>
                                                                <select id="Cliente" name="Cliente" class="form-control">

                                                                </select>
                                                            </div>
                                                            <div class="col-md-1" style="text-align: left;">
                                                                <a onclick="select2_cliente()" type="button" class="btn btn-success"><i class="fa fa-eraser"></i></a>
                                                            </div>
                                                            <div class="col-md-10" style="text-align: end;">
                                                                <a onclick="tablegindividetalles()" type="button" class="btn btn-success mr-1 mb-1">Generar reporte</a>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 col-sm-12 col-mdxx-5 col-lg-5 col-xl-5 div_tablegindivi">
                                                                
                                                            </div>
                                                            <div class="col-12 col-sm-12 col-mdxx-7 col-lg-7 col-xl-7 classmybarChart div_mybarChartind">
                                                                
                                                            </div>
                                                        </div>
                                                    <!--------------------------------------------------------------->
                                              </div>
                                              <div class="tab-pane" id="ficha" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-md-12" style="text-align: end;">
                                                            <a onclick="generar_v_c()" type="button" class="btn btn-success mr-1 mb-1">Generar reporte</a>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <table class="table table-bordered table-hover" id="table_valor_cli">
                                                                <thead>
                                                                    <tr>
                                                                        <th colspan="5">VALOR DEL CLIENTE POR VENTAS</th>
                                                                        <th colspan="2">DATOS</th>
                                                                        <th colspan="2">INDICADORES</th>
                                                                        <th colspan="4">GASTOS Y APOYOS</th>
                                                                        <th colspan="2">PARTICIPACIÓN DEL CLIENTE</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>NOMBRE DEL CLIENTE *</th>
                                                                        <th>TOTAL HISTÓRICO KG</th>
                                                                        <th>VALOR % KILOS</th>
                                                                        <th>TOTAL HISTORICO PESOS ANUAL ($)</th>
                                                                        <th>VALOR % PESOS</th>

                                                                        <th>AÑOS DE RELACION</th>
                                                                        <th>ORDENES DE COMPRA EN EL AÑO (FOLIOS)</th>

                                                                        <th>VALOR DE VIDA DEL CLIENTE</th>
                                                                        <th>TICKET PROMEDIO DE COMPRA</th>

                                                                        <th>*TARIFARIO COECSA LA MÁS BAJA (TARIFARIO VOI)</th>
                                                                        <th>**TARIFA ESPECIAL COECSA (DPL)</th>
                                                                        <th>DIFEREN PESOS</th>
                                                                        <th>TOTAL DESCUENTO</th>

                                                                        <th>INGRESO DE VENTAS - GASTOS Y APOYOS</th>
                                                                        <th>PARTICIPACION DEL CLIENTE %</th>

                                                                    </tr>
                                                                </thead>
                                                                <tbody class="tbody_valor_cli">
                                                                    
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6 grafica1"></div>
                                                        <div class="col-md-6 grafica2"></div>
                                                        <div class="col-md-6 grafica3"></div>
                                                        <div class="col-md-6 grafica4"></div>
                                                        <div class="col-md-6 grafica5"></div>
                                                    </div>
                                              </div>
                                              <div class="tab-pane" id="settings" role="tabpanel">D</div>
                                            </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->