<?php 
    $anioactual=date('Y');
    $anioactual2=$anioactual-10;
    $options_anio='';
    for ($i = $anioactual; $i >= $anioactual2; $i--) {
        $options_anio.='<option value="'.$i.'">'.$i.'</option>';
    }
?>
<style type="text/css">
    #table_list th,#table_list td{
        font-size: 12px;
    }
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
    #tablegindivi td{
        padding-top: 10px;
        padding-bottom: 10px;
    }
    #tablegindivi,#tableg,#table_valor_cli{
        font-size: 10px;
    }
    #tablegindivi td,#tablegindivi th{
        padding: 1.15rem 1rem !important;
    }
    #table_valor_cli td,#table_valor_cli th{
        padding: 0.9rem 0.4rem !important;
    }
    .select2-container{
        width: 100% !important;
    }
    .hidden{
        display: none;
    }
    .input_v{
        width: 105px;
    }
    .tables_metas,.tables_metas_report{
        max-width: 277px;
        text-align: center;
        float: left;
        margin: 2px;
    }
    .tables_metas td, .tables_metas th,.tables_metas_report td,.tables_metas_report th{
        padding: 5px;
        vertical-align: middle;
    }
    .load_metas_reporte_char .col-md-3{
        padding-right: 2px;
        padding-left: 2px;
    }
</style>            
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Reporte</div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                
                                                <!-- Nav tabs -->
                                                <br>
                                            <ul class="nav nav-tabs" role="tablist">
                                              <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#General" role="tab">Metas</a>
                                              </li>
                                              <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#Detalles" role="tab">Detalles</a>
                                              </li>
                                              
                                            </ul>
                                            <br>
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="General" role="tabpanel">
                                                    <!--------------------------------------------------------------->
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>Año</label>
                                                                <select id="anio" name="anio" class="form-control" onchange="cargarmetas()">
                                                                    <?php echo $options_anio;?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-10"></div>
                                                            <div class="col-md-2">
                                                                <a onclick="actualizar_metas()" type="button" class="btn btn-success mr-1 mb-1">Actualizar</a>
                                                            </div>
                                                            <div class="col-md-12 load_metas"></div>

                                                            
                                                        </div>
                                                    <!--------------------------------------------------------------->
                                                </div>
                                              <div class="tab-pane" id="Detalles" role="tabpanel">
                                                    <!--------------------------------------------------------------->
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>Año</label>
                                                                <select id="anio2" name="anio2" class="form-control">
                                                                    <?php echo $options_anio;?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-8"></div>
                                                            <div class="col-md-2">
                                                                <a onclick="generar_reporte()" type="button" class="btn btn-success mr-1 mb-1">Generar</a>
                                                            </div>

                                                        </div>
                                                        <div class="row load_metas_reporte"></div>
                                                        <div class="row load_metas_reporte_char"></div>
                                                    <!--------------------------------------------------------------->
                                              </div>
                                              
                                            </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->

            