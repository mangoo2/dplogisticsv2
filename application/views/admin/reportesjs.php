<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/js/chart.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/chartjs-plugin-datalabels.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/admin_reportes.js?v=<?php echo date('Ymdgis');?>"></script>