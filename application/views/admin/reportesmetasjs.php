<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/js/chart.min.js"></script>
<!--<script src="<?php echo base_url(); ?>app-assets/vendors/js/chartjs-plugin-datalabels.js"></script>-->
<script src="<?php echo base_url(); ?>app-assets/vendors/js/chartjs-plugin-regression-0.2.1.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/admin_metas.js?v=<?php echo date('Ymdgis');?>"></script>
<?php $resultsuc=$this->ModeloCatalogos->genSelect('coe_sucs'); ?>
<script type="text/javascript">
	$(document).ready(function($) {

	});
	var arraylabels_metas=[];
	    arraylabels_metas.push('ENE');
	    arraylabels_metas.push('FEB');
	    arraylabels_metas.push('MAR');
	    arraylabels_metas.push('ABR');
	    arraylabels_metas.push('MAY');
	    arraylabels_metas.push('JUN');
	    arraylabels_metas.push('JUL');
	    arraylabels_metas.push('AGO');
	    arraylabels_metas.push('SEP');
	    arraylabels_metas.push('OCT');
	    arraylabels_metas.push('NOV');
	    arraylabels_metas.push('DIC');

	function agregarcanvas(){
		$('.load_metas_reporte_char').html('');
		<?php foreach ($resultsuc as $item_suc) { 
				$idsuc=$item_suc->rid;
		?>
              $('.load_metas_reporte_char').append('<div class="col-md-3"><canvas id="mybarChar_report_<?php echo $idsuc;?>_a"></canvas></div>');
              $('.load_metas_reporte_char').append('<div class="col-md-3"><canvas id="mybarChar_report_<?php echo $idsuc;?>_b"></canvas></div>');
              $('.load_metas_reporte_char').append('<div class="col-md-3"><canvas id="mybarChar_report_<?php echo $idsuc;?>_c"></canvas></div>');
              $('.load_metas_reporte_char').append('<div class="col-md-3"><canvas id="mybarChar_report_<?php echo $idsuc;?>_d"></canvas></div>');
        <?php } ?>
	}
	function generarcharts(){
		<?php foreach ($resultsuc as $item_suc) { 
				$idsuc=$item_suc->rid;
		?>		//=============================== char A ================================
					//=====================================================
	              		var arraylabels=[];
				    	var arrayvalores_meta=[];
				    	var arrayvalores_logro=[];
				    
				        $("#tables_metas_kilos_<?php echo $idsuc;?> tbody > tr").each(function(){
				            var al   = $(this).find("td[class*='datasclass_kg_<?php echo $idsuc;?>']").data('mes');
				            var avm   = $(this).find("td[class*='datasclass_kg_<?php echo $idsuc;?>']").data('meta');
				            var avl   = $(this).find("td[class*='datasclass_kg_<?php echo $idsuc;?>']").data('logro');
				            
				            arraylabels.push(al);
				            arrayvalores_meta.push(avm);
				            arrayvalores_logro.push(avl);
				        });
				        
				    //========================================
				     var ctx = document.getElementById('mybarChar_report_<?php echo $idsuc;?>_a').getContext('2d');
	                   myChart = new Chart(ctx, {
	                    type: 'bar',
	                    data: {
	                        labels: arraylabels,
	                        datasets: [{
	                            label: 'Meta',
	                            data: arrayvalores_meta,
	                            borderColor:'#3080d0',
	                            backgroundColor:'#3080d0',
	                            borderWidth: 1
	                        },{
	                            label: 'Logros',
	                            data: arrayvalores_logro,
	                            borderColor:'#ff6384',
	                            backgroundColor:'#ff6384',
	                            borderWidth: 1
	                        }]
	                    },
	                    options: {
	                        scales: {
	                            yAxes: [{
	                                ticks: {
	                                    beginAtZero: true
	                                }
	                            }]
	                        },
	                        legend: {
						        position: 'bottom',
						    },
						    title: {
						        display: true,
						        text: 'Kilos <?php echo $item_suc->suc_nombre;?>'
						    }
	                    }
	                });
                //============================================================
	            //=============================== char B ================================
					//=====================================================
	              		var arraylabels=[];
				    	var arrayvalores_meta=[];
				    	var arrayvalores_logro=[];
				    
				        $("#tables_metas_venta_<?php echo $idsuc;?> tbody > tr").each(function(){
				            var al   = $(this).find("td[class*='datasclass_v_<?php echo $idsuc;?>']").data('mes');
				            var avm   = $(this).find("td[class*='datasclass_v_<?php echo $idsuc;?>']").data('meta');
				            var avl   = $(this).find("td[class*='datasclass_v_<?php echo $idsuc;?>']").data('logro');
				            
				            arraylabels.push(al);
				            arrayvalores_meta.push(avm);
				            arrayvalores_logro.push(avl);
				        });
				        
				    //========================================
				     var ctx = document.getElementById('mybarChar_report_<?php echo $idsuc;?>_b').getContext('2d');
	                   myChart = new Chart(ctx, {
	                    type: 'bar',
	                    data: {
	                        labels: arraylabels,
	                        datasets: [{
	                            label: 'Meta',
	                            data: arrayvalores_meta,
	                            borderColor:'#3080d0',
	                            backgroundColor:'#3080d0',
	                            borderWidth: 1
	                        },{
	                            label: 'Logros',
	                            data: arrayvalores_logro,
	                            borderColor:'#ff6384',
	                            backgroundColor:'#ff6384',
	                            borderWidth: 1
	                        }]
	                    },
	                    options: {
	                        scales: {
	                            yAxes: [{
	                                ticks: {
	                                    beginAtZero: false
	                                }
	                            }]
	                        },
	                        legend: {
						        position: 'bottom',
						    },
						    title: {
						        display: true,
						        text: 'Venta <?php echo $item_suc->suc_nombre;?>'
						    }
	                    }
	                });
                //============================================================
	            //=============================== char C ================================
					//=====================================================
	              		var arraylabels=[];
				    	//var arrayvalores_meta=[];
				    	var arrayvalores_logro=[];
				    
				        $("#tables_metas_kilos_<?php echo $idsuc;?> tbody > tr").each(function(){
				            var al   = $(this).find("td[class*='datasclass_kg_<?php echo $idsuc;?>']").data('mes');
				            //var avm   = $(this).find("td[class*='datasclass_kg_<?php echo $idsuc;?>']").data('meta');
				            var avl   = $(this).find("td[class*='datasclass_kg_<?php echo $idsuc;?>']").data('logro');
				            
				            arraylabels.push(al);
				            //arrayvalores_meta.push(avm);
				            arrayvalores_logro.push(avl);
				        });
				        
				    //========================================
				     var ctx = document.getElementById('mybarChar_report_<?php echo $idsuc;?>_c').getContext('2d');
	                   myChart = new Chart(ctx, {
	                    type: 'line',
	                    data: {
	                        labels: arraylabels,
	                        datasets: [{
	                            label: 'Logros',
	                            data: arrayvalores_logro,
	                            borderColor:'#ff6384',
	                            
	                            borderWidth: 1
	                        }]
	                    },
	                    options: {
	                        scales: {
	                            yAxes: [{
	                            	type:'linear',
	                                ticks: {
	                                    beginAtZero: true
	                                }
	                            }]
	                        },
	                        legend: {
						      	display: true,
						        position: 'bottom',
						    },
						    title: {
						        display: true,
						        text: 'Kilos <?php echo $item_suc->suc_nombre;?>'
						    },
						    regression: {
					            type: "linear",
					        }
	                    }
	                });
                //============================================================
	            //=============================== char D ================================
					//=====================================================
	              		var arraylabels=[];
				    	//var arrayvalores_meta=[];
				    	var arrayvalores_logro=[];
				    
				        $("#tables_metas_venta_<?php echo $idsuc;?> tbody > tr").each(function(){
				            var al   = $(this).find("td[class*='datasclass_v_<?php echo $idsuc;?>']").data('mes');
				            //var avm   = $(this).find("td[class*='datasclass_kg_<?php echo $idsuc;?>']").data('meta');
				            var avl   = $(this).find("td[class*='datasclass_v_<?php echo $idsuc;?>']").data('logro');
				            
				            arraylabels.push(al);
				            //arrayvalores_meta.push(avm);
				            arrayvalores_logro.push(avl);
				        });
				        
				    //========================================
				     var ctx = document.getElementById('mybarChar_report_<?php echo $idsuc;?>_d').getContext('2d');
	                   myChart = new Chart(ctx, {
	                    type: 'line',
	                    data: {
	                        labels: arraylabels,
	                        datasets: [{
	                            label: 'Logros',
	                            data: arrayvalores_logro,
	                            borderColor:'#ff6384',
	                            
	                            borderWidth: 1
	                        }]
	                    },
	                    options: {
	                        scales: {
	                            yAxes: [{
	                            	type:'linear',
	                                ticks: {
	                                    beginAtZero: true
	                                }
	                            }]
	                        },
	                        legend: {
						      	display: true,
						        position: 'bottom',
						    },
						    title: {
						        display: true,
						        text: 'Venta <?php echo $item_suc->suc_nombre;?>'
						    },
						    regression: {
					            type: "linear",
					        }
	                    }
	                });
                //============================================================
        <?php } ?>
	}
		
</script>
