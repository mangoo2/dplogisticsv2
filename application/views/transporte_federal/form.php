<!-- BEGIN : Main Content-->
<div class="main-content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="content-header"><?php echo $title;?> TRANSPORTE FEDERAL</div>
                <!--<p class="content-sub-header mb-1">All table styles are inherited in Bootstrap 4.3.1, meaning any nested tables will be styled in the same manner as the parent.</p>-->
            </div>
        </div>
        <!--Basic Table Starts-->
        <section id="simple-table">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <!--<h4 class="card-title">Default Table</h4>-->
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <!--------------->
                                <form id="form_registro" method="POST">
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="hidden" name="rid" id="rid" value="<?php echo $id;?>">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5 class="box-title">Datos generales</h5>
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 form-group">
                                                    <label>DPL</label>
                                                    <select class="form-control" name="dpl" id="dpl" required style="width: 100%">   
                                                        <?php if($dpl!=''){?> 
                                                            <option value="<?php echo $dpl ?>"><?php echo $dpltxt ?></option>
                                                        <?php } ?>  
                                                    </select>
                                                </div>
                                                <div class="col-md-5 form-group">
                                                    <label>NOMBRE DE LA ASEGURADORA </label>
                                                    <input type="text" class="form-control" name="nombre_aseguradora" value="<?php echo $nombre_aseguradora;?>" required>
                                                </div>
                                                <div class="col-md-4 form-group">
                                                    <label>NÚMERO PÓLIZA DE SEGURO</label>
                                                    <input type="text" class="form-control" name="num_poliza_seguro" value="<?php echo $num_poliza_seguro;?>" required>
                                                </div>
                                            </div>    
                                            <div class="row">  
                                                <div class="col-md-12 form-group">
                                                    <label>Tipo permiso SCT</label>
                                                    <select class="form-control" name="tipo_permiso_sct" id="tipo_permiso_sct">
                                                        <?php if($tipo_permiso_sct!=''){?> 
                                                            <option value="<?php echo $tipo_permiso_sct ?>"><?php echo $tipo_permiso_scttxt ?></option>
                                                        <?php } ?>     
                                                    </select>
                                                </div>   
                                                <div class="col-md-12 form-group">
                                                    <label>Número de permiso SCT</label>
                                                    <input type="text" class="form-control" name="num_permiso_sct" value="<?php echo $num_permiso_sct;?>">
                                                </div>
                                                <div class="col-md-12 form-group">
                                                    <label>Configuración vehícular</label>
                                                    <select class="form-control" name="configuracion_vhicular" id="configuracion_vhicular">
                                                        <?php if($configuracion_vhicular!=''){?> 
                                                            <option value="<?php echo $configuracion_vhicular ?>"><?php echo $configuracion_vhiculartxt ?></option>
                                                        <?php } ?>     
                                                    </select>
                                                </div>
                                            </div>    
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label>Placa vehículo motor</label>
                                                    <input type="text" class="form-control" name="placa_vehiculo_motor" value="<?php echo $placa_vehiculo_motor;?>">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label>Año modelo vehículo motor</label>
                                                    <input type="text" class="form-control" name="anio_modelo_vihiculo_motor" value="<?php echo $anio_modelo_vihiculo_motor;?>">
                                                </div>
                                            </div>    
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label>Subtipo de remolque</label>
                                                    <select class="form-control" name="subtipo_remolque">
                                                        <option value="CTR001" <?php if($subtipo_remolque=='CTR001') echo 'selected' ?>> Caballete</option>
                                                        <option value="CTR002" <?php if($subtipo_remolque=='CTR002') echo 'selected' ?>> Caja</option>
                                                        <option value="CTR003" <?php if($subtipo_remolque=='CTR003') echo 'selected' ?>> Caja Abierta</option>
                                                        <option value="CTR004" <?php if($subtipo_remolque=='CTR004') echo 'selected' ?>> Caja Cerrada</option>
                                                        <option value="CTR005" <?php if($subtipo_remolque=='CTR005') echo 'selected' ?>> Caja De Recolección Con Cargador Frontal</option>
                                                        <option value="CTR006" <?php if($subtipo_remolque=='CTR006') echo 'selected' ?>> Caja Refrigerada</option>
                                                        <option value="CTR007" <?php if($subtipo_remolque=='CTR007') echo 'selected' ?>> Caja Seca</option>
                                                        <option value="CTR008" <?php if($subtipo_remolque=='CTR008') echo 'selected' ?>> Caja Transferencia</option>
                                                        <option value="CTR009" <?php if($subtipo_remolque=='CTR009') echo 'selected' ?>> Cama Baja o Cuello Ganso</option>
                                                        <option value="CTR010" <?php if($subtipo_remolque=='CTR010') echo 'selected' ?>> Chasis Portacontenedor</option>
                                                        <option value="CTR011" <?php if($subtipo_remolque=='CTR011') echo 'selected' ?>> Convencional De Chasis</option>
                                                        <option value="CTR012" <?php if($subtipo_remolque=='CTR012') echo 'selected' ?>> Equipo Especial</option>
                                                        <option value="CTR013" <?php if($subtipo_remolque=='CTR013') echo 'selected' ?>> Estacas</option>
                                                        <option value="CTR014" <?php if($subtipo_remolque=='CTR014') echo 'selected' ?>> Góndola Madrina</option>
                                                        <option value="CTR015" <?php if($subtipo_remolque=='CTR015') echo 'selected' ?>> Grúa Industrial</option>
                                                        <option value="CTR016" <?php if($subtipo_remolque=='CTR016') echo 'selected' ?>> Grúa </option>
                                                        <option value="CTR017" <?php if($subtipo_remolque=='CTR017') echo 'selected' ?>> Integral</option>
                                                        <option value="CTR018" <?php if($subtipo_remolque=='CTR018') echo 'selected' ?>> Jaula</option>
                                                        <option value="CTR019" <?php if($subtipo_remolque=='CTR019') echo 'selected' ?>> Media Redila</option>
                                                        <option value="CTR020" <?php if($subtipo_remolque=='CTR020') echo 'selected' ?>> Pallet o Celdillas</option>
                                                        <option value="CTR021" <?php if($subtipo_remolque=='CTR021') echo 'selected' ?>> Plataforma</option>
                                                        <option value="CTR022" <?php if($subtipo_remolque=='CTR022') echo 'selected' ?>> Plataforma Con Grúa</option>
                                                        <option value="CTR023" <?php if($subtipo_remolque=='CTR023') echo 'selected' ?>> Plataforma Encortinada</option>
                                                        <option value="CTR024" <?php if($subtipo_remolque=='CTR024') echo 'selected' ?>> Redilas</option>
                                                        <option value="CTR025" <?php if($subtipo_remolque=='CTR025') echo 'selected' ?>> Refrigerador</option>
                                                        <option value="CTR026" <?php if($subtipo_remolque=='CTR026') echo 'selected' ?>> Revolvedora</option>
                                                        <option value="CTR027" <?php if($subtipo_remolque=='CTR027') echo 'selected' ?>> Semicaja</option>
                                                        <option value="CTR028" <?php if($subtipo_remolque=='CTR028') echo 'selected' ?>> Tanque</option>
                                                        <option value="CTR029" <?php if($subtipo_remolque=='CTR029') echo 'selected' ?>> Tolva</option>
                                                        <option value="CTR031" <?php if($subtipo_remolque=='CTR031') echo 'selected' ?>> Volteo</option>
                                                        <option value="CTR032" <?php if($subtipo_remolque=='CTR032') echo 'selected' ?>> Volteo Desmontable</option>
                                                    </select> 
                                                </div>
                                                <div class="col-md-4 form-group">
                                                    <label>Placa remolque</label>
                                                    <input type="text" class="form-control" name="placa_remolque" value="<?php echo $placa_remolque;?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-primary btn_registro" onclick="guardar_registro()">Guardar</button>
                                            <a onclick="javascript:history.back()" class="btn btn-danger">Regresar</a>
                                        </div>
                                    </div>
                            </div>            
                        </div>
                    </div>
                </div>            
            </div>
        </section>

    </div>
</div>            