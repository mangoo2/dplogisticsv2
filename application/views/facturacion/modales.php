<div id="modal_addmodalmercancias" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Mercancia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-3">
            <label>Bienes transportados</label>
            <select id="BienesTransp_m" name="BienesTransp_m" class="form-control">
              
            </select>
          </div>
          <div class="col-md-5">
            <label>Descripcion<span class="camporequerido">*</span></label>
            <input type="text" id="Descripcion_m" name="Descripcion_m" class="form-control">
          </div>
          <div class="col-md-2">
            <label>Cantidad<span class="camporequerido">*</span></label>
            <input type="number" id="Cantidad_m" name="Cantidad_m" class="form-control">
          </div>
          <div class="col-md-2">
            <label>Unidad<span class="camporequerido">*</span></label>
            <select id="ClaveUnidad_m" name="ClaveUnidad_m" class="form-control">
              <option value="KGM">Kilogramo</option>
              <option value="MC">Microgramo</option>
              <option value="DJ">Decagramo</option>
            </select><!--con la descripcion de autocompleta el campo "Unidad"-->
          </div>
          <div class="col-md-2">
            <label>Dimensiones<span class="camporequerido">*</span></label>
            <input type="text" id="Dimensiones_m" name="Dimensiones_m" class="form-control " placeholder="30/40/30cm" title="Se debe registrar la longitud, la altura y la anchura en centímetros o en pulgadas separados dichos valores por una diagonal." data-inputmask="'mask': '9{1,4}/9{1,4}/9{1,4}a{1,3}'">
          </div>
          <div class="col-md-3">
            <label>Embalaje</label>
            <select id="Embalaje_m" name="Embalaje_m" class="form-control">
              
            </select><!--cuando se inserte se agregara Embalaje y DescripEmbalaje(opcional)-->
          </div>
          <div class="col-md-2">
            <label>Peso en Kg<span class="camporequerido">*</span></label>
            <input type="number" id="PesoEnKg_m" name="PesoEnKg_m" class="form-control" >
          </div>
          <div class="col-md-2">
            <label>Valor mercancía<span class="camporequerido">*</span></label>
            <input type="number" id="ValorMercancia_m" name="ValorMercancia_m" class="form-control" >
          </div>
          <div class="col-md-3">
            <label>Moneda<span class="camporequerido">*</span></label>
            <select id="Moneda_m" name="Moneda_m" class="form-control">
              <?php foreach ($fmoneda->result() as $key) { ?>
                <option value="<?php echo $key->c_moneda; ?>"><?php echo $key->c_moneda; ?> <?php echo $key->descripcion; ?></option>
              <?php } ?>
            </select><!--cuando se inserte se agregara Embalaje y DescripEmbalaje-->
          </div>

        </div>

      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</a>
        <a type="button" class="btn btn-primary" data-dismiss="modal" onclick="addmercancia()">Agregar</a>
      </div>
    </div>
  </div>
</div>
<div id="modal_addmodaltransporte" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Autotransporte</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <label>Transporte federal<span class="camporequerido">*</span></label>
            <select id="transportef" class="form-control">
            </select>
          </div>  
          <div class="col-md-3">
            <label>Tipo permiso SCT<span class="camporequerido">*</span></label>
            <select id="PermSCT_m" name="PermSCT_m" class="form-control">
              
            </select>
          </div>
          <div class="col-md-3">
            <label>Número de permiso SCT<span class="camporequerido">*</span></label>
            <input id="NumPermisoSCT_m" name="NumPermisoSCT_m" class="form-control" placeholder="0X2XTXZ0X5X0X3X2X1X0">
          </div>
          <div class="col-md-3">
            <label>Nombre aseguradora<span class="camporequerido">*</span></label>
            <input id="NombreAseg_m" name="NombreAseg_m" class="form-control" placeholder="ejemplo Seguros S.A. de C.V.">
          </div>
          <div class="col-md-3">
            <label>Número póliza seguro<span class="camporequerido">*</span></label>
            <input id="NumPolizaSeguro_m" name="NumPolizaSeguro_m" class="form-control" placeholder="000000000">
          </div>
          <div class="col-md-3">
            <label>Config. Vehicular<span class="camporequerido">*</span></label>
            <select id="ConfigVehicular_m" name="ConfigVehicular_m" class="form-control" placeholder="000000000">
            </select>
          </div>
          <div class="col-md-3">
            <label>Placa Vehículo Motor<span class="camporequerido">*</span></label>
            <input id="PlacaVM_m" name="PlacaVM_m" class="form-control" placeholder="501&&">
          </div>
          <div class="col-md-3">
            <label>Año modelo Vehículo Motor<span class="camporequerido">*</span></label>
            <input type="number" id="AnioModeloVM_m" name="AnioModeloVM_m" class="form-control" placeholder="2021">
          </div>
          <div class="col-md-3">
            <label>Subtipo de remolque<span class="camporequerido">*</span></label>
            <select id="SubTipoRem_m" name="SubTipoRem_m" class="form-control" placeholder="000000000">
              <option value="CTR001"> Caballete</option>
              <option value="CTR002"> Caja</option>
              <option value="CTR003"> Caja Abierta</option>
              <option value="CTR004"> Caja Cerrada</option>
              <option value="CTR005"> Caja De Recolección Con Cargador Frontal</option>
              <option value="CTR006"> Caja Refrigerada</option>
              <option value="CTR007"> Caja Seca</option>
              <option value="CTR008"> Caja Transferencia</option>
              <option value="CTR009"> Cama Baja o Cuello Ganso</option>
              <option value="CTR010"> Chasis Portacontenedor</option>
              <option value="CTR011"> Convencional De Chasis</option>
              <option value="CTR012"> Equipo Especial</option>
              <option value="CTR013"> Estacas</option>
              <option value="CTR014"> Góndola Madrina</option>
              <option value="CTR015"> Grúa Industrial</option>
              <option value="CTR016"> Grúa </option>
              <option value="CTR017"> Integral</option>
              <option value="CTR018"> Jaula</option>
              <option value="CTR019"> Media Redila</option>
              <option value="CTR020"> Pallet o Celdillas</option>
              <option value="CTR021"> Plataforma</option>
              <option value="CTR022"> Plataforma Con Grúa</option>
              <option value="CTR023"> Plataforma Encortinada</option>
              <option value="CTR024"> Redilas</option>
              <option value="CTR025"> Refrigerador</option>
              <option value="CTR026"> Revolvedora</option>
              <option value="CTR027"> Semicaja</option>
              <option value="CTR028"> Tanque</option>
              <option value="CTR029"> Tolva</option>
              <option value="CTR031"> Volteo</option>
              <option value="CTR032"> Volteo Desmontable</option>

            </select>
          </div>
          <div class="col-md-3">
            <label>Placa remolque<span class="camporequerido">*</span></label>
            <input id="Placa_m" name="Placa_m" class="form-control" placeholder="501&&">
          </div>
          
        </div>

      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</a>
        <a type="button" class="btn btn-primary" data-dismiss="modal" onclick="addtrasporte()">Agregar</a>
      </div>
    </div>
  </div>
</div>
<div id="modal_addmodalaereo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Transporte Aereo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-3" style="padding-right: 9px; padding-left: 9px;">
            <label>Tipo permiso SCT<span class="camporequerido">*</span></label>
            <select id="PermSCT2_m" name="PermSCT2_m" class="form-control">
              
            </select>
          </div>
          <div class="col-md-3"style="padding-right: 9px; padding-left: 9px;">
            <label>Número de permiso SCT<span class="camporequerido">*</span></label>
            <input id="NumPermisoSCT2_m" name="NumPermisoSCT2_m" class="form-control" placeholder="0X2XTXZ0X5X0X3X2X1X0">
          </div>
          <div class="col-md-3" style="padding-right: 9px; padding-left: 9px;">
            <label>Matrícula de Aeronave<span class="camporequerido">*</span></label>
            <input id="MatriculaAeronave_m" name="MatriculaAeronave_m" class="form-control" placeholder="AB1-MEX">
          </div>

          <div class="col-md-3" style="padding-right: 9px; padding-left: 9px;">
            <label>Nombre aseguradora<span class="camporequerido">*</span></label>
            <input id="NombreAseg2_m" name="NombreAseg2_m" class="form-control" placeholder="ejemplo Seguros S.A. de C.V.">
          </div>
        </div>
        <div class="row">
          <div class="col-md-3" style="padding-right: 9px; padding-left: 9px;">
            <label>Número póliza seguro<span class="camporequerido">*</span></label>
            <input id="NumPolizaSeguro2_m" name="NumPolizaSeguro2_m" class="form-control" placeholder="000000000">
          </div>

          <div class="col-md-3" style="padding-right: 9px; padding-left: 9px;">
            <label>Número de guía<span class="camporequerido">*</span></label>
            <input id="NumeroGuia_m" name="NumeroGuia_m" class="form-control" placeholder="803117555500">
          </div>
          
          <div class="col-md-3" style="padding-right: 9px; padding-left: 9px;">
            <label>Lugar del contrato</label>
            <input type="text" id="LugarContrato_m" name="LugarContrato_m" class="form-control" placeholder="MEX">
          </div>
          <div class="col-md-3" style="padding-right: 9px; padding-left: 9px;">
            <label>RFC del transportista<span class="camporequerido">*</span></label>
            <input type="text" id="RFCTransportista_m" name="RFCTransportista_m" class="form-control" placeholder="XAXX010101000">
          </div>
        </div>
        <div class="row">
          <div class="col-md-3" style="padding-right: 9px; padding-left: 9px;">
            <label>Código de Transportista<span class="camporequerido">*</span></label>
            <select id="CodigoTransportista_m" name="CodigoTransportista_m" class="form-control">
              
            </select>
          </div>
          <div class="col-md-3" style="padding-right: 9px; padding-left: 9px;">
            <label>Número de identificación o registro fiscal del transportista</label>
            <input type="text" id="NumRegIdTribTranspora_m" name="NumRegIdTribTranspor_m" class="form-control" title="Atributo condicional para incorporar el número de identificación o registro fiscal del país de residencia para los efectos fiscales del transportista, cuando sea residente en el extranjero.">
          </div>
          <div class="col-md-3" style="padding-right: 9px; padding-left: 9px; display: none;">
            <label>Residencia fiscal transportista</label>
            <input id="ResidenciaFiscalTranspor_m" name="ResidenciaFiscalTranspor_m" class="form-control" placeholder="MEX">
          </div>
          <div class="col-md-3" style="padding-right: 9px; padding-left: 9px;">
            <label>Nombre del transportista</label>
            <input id="NombreTransportista_m" name="NombreTransportista_m" class="form-control" placeholder="MEX">
          </div>
        </div>
        <div class="row">
          <div class="col-md-3" style="padding-right: 9px; padding-left: 9px;">
            <label>RFC del embarcador<span class="camporequerido">*</span></label>
            <input type="text" id="RFCEmbarcador_m" name="RFCEmbarcador_m" class="form-control" placeholder="XAXX010101000">
          </div>
          <div class="col-md-3" style="padding-right: 9px; padding-left: 9px;">
            <label>Número de identificación o registro fiscal del embarcador</label>
            <input type="text" id="NumRegIdTribEmbarc_m" name="NumRegIdTribEmbarc_m" class="form-control" placeholder="XAXX010101000">
          </div>
          <div class="col-md-3" style="padding-right: 9px; padding-left: 9px;">
            <label>Residencia fiscal embarcador</label>
            <input id="ResidenciaFiscalEmbarc_m" name="ResidenciaFiscalEmbarc_m" class="form-control" placeholder="MEX">
          </div>
          <div class="col-md-3" style="padding-right: 9px; padding-left: 9px;">
            <label>Nombre del embarcador</label>
            <input id="NombreEmbarcador_m" name="NombreEmbarcador_m" class="form-control">
          </div>
          
        </div>

      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</a>
        <a type="button" class="btn btn-primary" data-dismiss="modal" onclick="addtrasporteaereo()">Agregar</a>
      </div>
    </div>
  </div>
</div>
<div id="modal_addmodaloperadores" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Operadores</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-3">
            <label>RFC del operador<span class="camporequerido">*</span></label>
            <select class="form-control" name="RFCOperador_m" id="RFCOperador_m" required style="width: 100%">    
            </select>
          </div>
          <div class="col-md-3 divNumLicencia_m">
            <label>Número de licencia<span class="camporequerido">*</span></label>
            <input id="NumLicencia_m" name="NumLicencia_m" class="form-control" placeholder="000004">
          </div>
          <div class="col-md-6">
            <label>Nombre del Operador<span class="camporequerido">*</span></label>
            <input id="NombreOperador_m" name="NombreOperador_m" class="form-control" placeholder="">
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Número de identificación o registro fiscal del Operador</label>
            <input id="NumRegIdTribOperador_m" name="NumRegIdTribOperador_m" class="form-control" placeholder="121585958">
          </div>
          <div class="col-md-3">
            <label>Residencia fiscal operador</label>
            <select id="ResidenciaFiscalOperador_m" name="ResidenciaFiscalOperador_m" class="form-control"></select>
          </div>
          <div class="col-md-3" style="display:none;">
            <label><input type="checkbox" id="adddomicilio_m" name="adddomicilio_m" onclick="adddomicilio_m()"> Agregar Domicilio</label>
          </div>
        </div>
        <div class="row adddomicilio_m" style="display:none;">
          <div class="col-md-3">
            <label>Calle*</label>
            <input id="Calle_m" name="Calle_m" class="form-control" placeholder="">
          </div>
          <div class="col-md-3">
            <label>Número exterior</label>
            <input id="NumeroExterior_m" name="NumeroExterior_m" class="form-control" placeholder="">
          </div>
          <div class="col-md-3">
            <label>Número interior</label>
            <input id="NumeroInterior_m" name="NumeroInterior_m" class="form-control" placeholder="">
          </div>
          <div class="col-md-3">
            <label>Colonia</label>
            <select id="Colonia_m" name="Colonia_m" class="form-control" placeholder=""></select>
          </div>
          <div class="col-md-3">
            <label>Localidad</label>
            <select id="Localidad_m" name="Localidad_m" class="form-control" placeholder=""></select>
          </div>
          <div class="col-md-3">
            <label>Referencia</label>
            <input id="Referencia_m" name="Referencia_m" class="form-control" placeholder="25.91799767951801, -89.95132314223306" title="Campo opcional para registrar una referencia geográfica adicional que permita una más fácil o precisa ubicación del operador del autotransporte de carga federal en el que se trasladan los bienes o mercancías; por ejemplo, las coordenadas GPS.">
          </div>
          <div class="col-md-3">
            <label>Municipio</label>
            <select id="Municipio_m" name="Municipio_m" class="form-control" placeholder=""></select>
          </div>
          <div class="col-md-3">
            <label>Estado</label>
            <select id="Estado_m" name="Estado_m" class="form-control" placeholder=""></select>
          </div>
          <div class="col-md-3">
            <label>Pais</label>
            <select id="Pais_m" name="Pais_m" class="form-control" placeholder=""></select>
          </div>
          <div class="col-md-3">
            <label>Código Postal</label>
            <input id="CodigoPostal_m" name="CodigoPostal_m" class="form-control" placeholder="">
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</a>
        <a type="button" class="btn btn-primary" data-dismiss="modal" onclick="addoperadores()">Agregar</a>
      </div>
    </div>
  </div>
</div>
<div id="modal_addmodalpropietario" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Propietario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-3">
            <label>RFC del Propietario<span class="camporequerido">*</span></label>
            <input id="RFCPropietario_m" name="RFCPropietario_m" class="form-control" placeholder="XAXX010101000">
          </div>
          <div class="col-md-6">
            <label>Nombre del Propietario<span class="camporequerido">*</span></label>
            <input id="NombrePropietario_m" name="NombrePropietario_m" class="form-control" placeholder="">
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Número de identificación o registro fiscal del Operador</label>
            <input id="NumRegIdTribPropietario_m" name="NumRegIdTribPropietario_m" class="form-control" placeholder="121585958">
          </div>
          <div class="col-md-3">
            <label>Residencia fiscal Propietario</label>
            <select id="ResidenciaFiscalPropietario_m" name="ResidenciaFiscalPropietario_m" class="form-control"></select>
          </div>
          <div class="col-md-3">
            <label><input type="checkbox" id="adddomiciliop_m" name="adddomiciliop_m" onclick="adddomiciliop_m()"> Agregar Domicilio</label>
          </div>
        </div>
        <div class="row adddomiciliop_m" style="display:none;">
          <div class="col-md-3">
            <label>Calle*</label>
            <input id="Calle_m_p" name="Calle_m_p" class="form-control" placeholder="">
          </div>
          <div class="col-md-3">
            <label>Número exterior</label>
            <input id="NumeroExterior_m_p" name="NumeroExterior_m_p" class="form-control" placeholder="">
          </div>
          <div class="col-md-3">
            <label>Número interior</label>
            <input id="NumeroInterior_m_p" name="NumeroInterior_m_p" class="form-control" placeholder="">
          </div>
          <div class="col-md-3">
            <label>Colonia</label>
            <select id="Colonia_m_p" name="Colonia_m_p" class="form-control" placeholder=""></select>
          </div>
          <div class="col-md-3">
            <label>Localidad</label>
            <select id="Localidad_m_p" name="Localidad_m_p" class="form-control" placeholder=""></select>
          </div>
          <div class="col-md-3">
            <label>Referencia</label>
            <input id="Referencia_m_p" name="Referencia_m_p" class="form-control" placeholder="25.91799767951801, -89.95132314223306" title="Campo opcional para registrar una referencia geográfica adicional que permita una más fácil o precisa ubicación del operador del autotransporte de carga federal en el que se trasladan los bienes o mercancías; por ejemplo, las coordenadas GPS.">
          </div>
          <div class="col-md-3">
            <label>Municipio</label>
            <select id="Municipio_m_p" name="Municipio_m_p" class="form-control" placeholder=""></select>
          </div>
          <div class="col-md-3">
            <label>Estado</label>
            <select id="Estado_m_p" name="Estado_m_p" class="form-control" placeholder=""></select>
          </div>
          <div class="col-md-3">
            <label>Pais</label>
            <select id="Pais_m_p" name="Pais_m_p" class="form-control" placeholder=""></select>
          </div>
          <div class="col-md-3">
            <label>Código Postal</label>
            <input id="CodigoPostal_m_p" name="CodigoPostal_m_p" class="form-control" placeholder="">
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</a>
        <a type="button" class="btn btn-primary" data-dismiss="modal" onclick="addpropietario()">Agregar</a>
      </div>
    </div>
  </div>
</div>
<div id="modal_addmodalarrendatario" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Arrendatario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-3">
            <label>RFC del Arrendatario</label>
            <input id="RFCArrendatario_m" name="RFCArrendatario_m" class="form-control" placeholder="XAXX010101000">
          </div>
          <div class="col-md-6">
            <label>Nombre del Arrendatario</label>
            <input id="NombreArrendatario_m" name="NombreArrendatario_m" class="form-control" placeholder="">
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Número de identificación o registro fiscal del Arrendatario</label>
            <input id="NumRegIdTribArrendatario_m" name="NumRegIdTribArrendatario_m" class="form-control" placeholder="121585958">
          </div>
          <div class="col-md-3" style="padding-right: 10px;padding-left: 10px;">
            <label>Residencia fiscal Arrendatario</label>
            <select id="ResidenciaFiscalArrendatario_m" name="ResidenciaFiscalArrendatario_m" class="form-control"></select>
          </div>
          <div class="col-md-3">
            <label><input type="checkbox" id="adddomicilioa_m" name="adddomicilioa_m" onclick="adddomicilioa_m()"> Agregar Domicilio</label>
          </div>
        </div>
        <div class="row adddomicilioa_m" style="display:none;">
          <div class="col-md-3">
            <label>Calle*</label>
            <input id="Calle_m_a" name="Calle_m_a" class="form-control" placeholder="">
          </div>
          <div class="col-md-3">
            <label>Número exterior</label>
            <input id="NumeroExterior_m_a" name="NumeroExterior_m_a" class="form-control" placeholder="">
          </div>
          <div class="col-md-3">
            <label>Número interior</label>
            <input id="NumeroInterior_m_a" name="NumeroInterior_m_a" class="form-control" placeholder="">
          </div>
          <div class="col-md-3">
            <label>Colonia</label>
            <select id="Colonia_m_a" name="Colonia_m_a" class="form-control" placeholder=""></select>
          </div>
          <div class="col-md-3">
            <label>Localidad</label>
            <select id="Localidad_m_a" name="Localidad_m_a" class="form-control" placeholder=""></select>
          </div>
          <div class="col-md-3">
            <label>Referencia</label>
            <input id="Referencia_m_a" name="Referencia_m_a" class="form-control" placeholder="25.91799767951801, -89.95132314223306" title="Campo opcional para registrar una referencia geográfica adicional que permita una más fácil o precisa ubicación del operador del autotransporte de carga federal en el que se trasladan los bienes o mercancías; por ejemplo, las coordenadas GPS.">
          </div>
          <div class="col-md-3">
            <label>Municipio</label>
            <select id="Municipio_m_a" name="Municipio_m_a" class="form-control" placeholder=""></select>
          </div>
          <div class="col-md-3">
            <label>Estado</label>
            <select id="Estado_m_a" name="Estado_m_a" class="form-control" placeholder=""></select>
          </div>
          <div class="col-md-3">
            <label>Pais</label>
            <select id="Pais_m_a" name="Pais_m_a" class="form-control" placeholder=""></select>
          </div>
          <div class="col-md-3">
            <label>Código Postal</label>
            <input id="CodigoPostal_m_a" name="CodigoPostal_m_a" class="form-control" placeholder="">
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</a>
        <a type="button" class="btn btn-primary" data-dismiss="modal" onclick="addarrendatario()">Agregar</a>
      </div>
    </div>
  </div>
</div>

<div id="modal_origen" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Origen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="">
              <select class="form-control" id="origen_ubicacion" required style="width: 100%">     
              </select>
            </div>
            <br><br>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="modal_origen_domicilio" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Origen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="">
              <select class="form-control" id="origen_ubicacion_domicilio" required style="width: 100%">     
              </select>
            </div>
            <br><br>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="modal_destino" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Destino</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="">
              <select class="form-control" id="destino_ubicacion" required style="width: 100%">     
              </select>
            </div>
            <br><br>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="modal_destino_domicilio" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Destino</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="">
              <select class="form-control" id="destino_ubicacion_domicilio" required style="width: 100%">     
              </select>
            </div>
            <br><br>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>