<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css">

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">

<script src="<?php echo base_url();?>public/js/facturas.js?v=<?php echo date('YmdGis') ?>"></script>
<script type="text/javascript">
	$(document).ready(function($) {
		<?php if(isset($_GET['envio'])){ ?>
			setTimeout(function(){ 
				enviomodal(<?php echo $_GET['envio']; ?>);
			}, 1000);
			
		<?php } ?>
	});
	
</script>