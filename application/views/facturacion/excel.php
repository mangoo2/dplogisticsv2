<?php



header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=reporte_facturas.xls");

?>

<table  border="1">
    <thead>
        <tr>
            <th style="background-color: #ccecff;">Serie</th>
            <th style="background-color: #ccecff;">Folio</th>
            <th style="background-color: #ccecff;">cfiUUDID</th>
            <th style="background-color: #ccecff;">Tipo</th>
            <th style="background-color: #ccecff;">Fecha</th>
            <th style="background-color: #ccecff;">Hora</th>
            <th style="background-color: #ccecff;">RFC</th>
            <th style="background-color: #ccecff;">Receptor</th>
            <th style="background-color: #ccecff;">Estado</th>
            <th style="background-color: #ccecff;">Primer Concepto</th>
            <th style="background-color: #ccecff;"><?php echo utf8_decode('No. Identificación'); ?></th>
            <th style="background-color: #ccecff;">Forma de pago</th>
            <th style="background-color: #ccecff;"><?php echo utf8_decode('Método de pago'); ?></th>
            <th style="background-color: #ccecff;">Moneda</th>
            <th style="background-color: #ccecff;">Subtotal</th>
            <th style="background-color: #ccecff;">Impuesto Traslado IVA</th>
            <th style="background-color: #ccecff;">Total</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($result as $r){ ?>
        <tr>
            <td><?php echo $r->serie ?></td>
            <td><?php echo $r->Folio ?></td>
            <td><?php echo $r->uuid ?></td>
            <td><?php echo 'Factura' ?></td>
            <td><?php echo $r->fecha ?></td>
            <td><?php echo $r->hora ?></td>
            <td><?php echo $r->Rfc ?></td>
            <td><?php echo utf8_decode($r->Nombre) ?></td>
            <td><?php 
                $valor=''; 
                if($r->Estado==0){
                    $valor='Cancelada'; 
                }else if($r->Estado==1){
                    $valor='Timbrada'; 
                }else if($r->Estado==2){
                    $valor='error'; 
                } 
                echo $valor;
                ?></td>
            <td><?php echo utf8_decode($r->servicio); ?></td>
            <td><?php echo utf8_decode($r->NoIdentificacion); ?></td>
            <td><?php echo $r->formapago ?></td>
            <td><?php echo utf8_decode($r->metodopago_text); ?></td>
            <td><?php echo $r->moneda ?></td>
            <td><?php echo $r->subtotal ?></td>
            <td><?php echo $r->iva ?></td>
            <td><?php echo $r->total ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>