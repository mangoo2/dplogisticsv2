<script src="<?php echo base_url();?>app-assets/vendors/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>app-assets/css/plugins/form-validation.css">


<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css">

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">
<script src="<?php echo base_url();?>public/plugins/jquery.inputmask.507.js"></script>
<script src="<?php echo base_url();?>public/js/facturasadd.js?v=<?php echo date('YmdGis') ?>"></script>
<script src="<?php echo base_url();?>public/js/afacturaestra.js?v=<?php echo date('YmdGis') ?>"></script>
<?php if(isset($_GET['cot'])){ 
	$cot=$_GET['cot'];
?>
<script type="text/javascript">
	$(document).ready(function($) {
		ontenerinformacion('<?php echo $_GET['cot'];?>','<?php echo $_GET['metodo'];?>');
		$('#tipofac').val(1);
	});
</script>
<?php }else{
	$cot=0;
} ?>
<script type="text/javascript">
	$(document).ready(function($) {
		setTimeout(function(){ 
			$('#sidebarToggle').click(); 
		}, 1000);
		<?php if(isset($_GET['idfac'])){ ?>
			obtenerdatosfactura(<?php echo $_GET['idfac'];?>);
			$('#facturarelacionada').prop('checked',true).change();
			$('.divfacturarelacionada').show('show');
            $('#TipoRelacion').val('<?php echo $_GET['tr']; ?>');
            <?php if($_GET['tr']=='01'){ ?>
            	$('#TipoDeComprobante').val('E');
            <?php }else{ ?>
            	$('#TipoDeComprobante').val('I');
            <?php } ?>
            $('#uuid_r').val('<?php echo $_GET['uuid']; ?>');
		<?php } ?>
		
	});

	function btn_origen_modal(){
		var idc = '<?php echo $cot;?>';
		btn_origen(idc);
	}

	function btn_origen_domicilio_modal(){
		var idc = '<?php echo $cot;?>';
		btn_origen_domicilio(idc);
	}

	function btn_destino_modal(){
		var idc = '<?php echo $cot;?>';
		btn_destino(idc);
	}

	function btn_destino_domicilio_modal(){
		var idc = '<?php echo $cot;?>';
		btn_destino_domicilio(idc);
	}
</script>
