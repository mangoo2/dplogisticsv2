<style type="text/css">
    #table_conceptos th,#table_conceptos td{
        padding: 3px;
    }
    .rowtrconceptos td{
      color: black !important;
    }
    .panel-default{
      border-color: #ddd;
    }
    .panel-default>.panel-heading{
      color: #333;
      background-color: #f5f5f5;
      border-color: #ddd;
    }
    .panel{
      margin-bottom: 20px;
    background-color: #fff;
    border: 1px solid #bdb5b5ad;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgb(0 0 0 / 5%);
    box-shadow: 0 1px 1px rgb(0 0 0 / 5%);
    }
    .select2-container{
      width: 100% !important;
    }
    .detallemercancia{
        border: 1px solid #ccc;
        min-height: 42px;
    }
    .view_cartaporte{
      display: none;
    }
    .preview_iframe iframe{
      width: 100%;
      border: 0;
      min-height: 400px;
    }
    .infodatosfiscales{
      font-size: 12px;
    }
</style> 
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Facturación</div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <input type="hidden" id="mesactual" value="<?php echo date('m');?>">
                                                <input type="hidden" id="anioactual" value="<?php echo date('Y');?>">
                                            <form id="validateSubmitForm" role="form" method="post" autocomplete="off" required> 
                                                <input type="hidden" name="tipofac" id="tipofac" value="0">
                                                <!--<input type="hidden" name="tipoempresa" id="tipoempresa">-->
                                                <input type="hidden" name="rid_relacion" id="rid_relacion">
                                                <input type="hidden" name="rid_relacion_cot_folio" id="rid_relacion_cot_folio">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                      <div class="form-group">
                                                        <label for="">Empresa</label> <br>
                                                        <select id="tipoempresa" name="tipoempresa" class="form-control" onchange="tipoempresas()">
                                                          <option disabled selected>Seleccione</option>
                                                          <option value="1">COECSA</option>
                                                          <option value="2">CADIPA</option>
                                                        </select>
                                                      </div>  
                                                    </div>
                                                    <div class="col-md-6 sucursales_view" style="display:none;">
                                                      <label>Sucursal</label>
                                                      <select name="sucursal" id="sucursal" class="form-control">
                                                        <option value="0">Seleccione</option>
                                                        <?php  foreach ($sucursal_row->result() as $item) { ?>
                                                            <option value="<?php echo $item->id; ?>"><?php echo $item->nombre; ?></option>
                                                          <?php } ?>
                                                      </select>
                                                    </div>
                                                    
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                      <div class="form-group">
                                                        <label for="">Cliente</label> <br>
                                                        <select id="idcliente" name="idcliente" class="form-control" >
                                                        </select>
                                                      </div>  
                                                    </div>
                                                    <div class="col-md-6">
                                                      <div class="form-group">
                                                        <label for="">RFC</label> <!--Llenar -->
                                                        <input class="form-control" id="rfc" name="rfc" type="text" value="" required onchange="verficartiporfc()">
                                                      </div> 
                                                    </div>
                                                    
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-6"></div>
                                                  <div class="row col-md-6 infodatosfiscales"></div>
                                                </div>
                                                <div class="row">
                                                  <div class="agregardatospublicogeneral">
                  
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                      <div class="form-group">
                                                        <i class="fa fa-usd"></i>
                                                        <label class="active">Método de pago</label>
                                                        
                                                        <select class="form-control" id="FormaPago" name="FormaPago">
                                                          <option value="PUE">Pago en una sola exhibición</option>
                                                          <option value="PPD">Pago en parcialidades o diferido</option>
                                                        </select>
                                                      </div> 
                                                    </div>
                                                    <div class="col-md-6">
                                                      <div class="form-group">
                                                        <i class="fa fa-usd"></i>
                                                        <label class="active">Forma de pago</label>
                                                        <select class="form-control" id="MetodoPago" name="MetodoPago">
                                                            <?php foreach ($forma as $item) { ?>
                                                                    <option value="<?php echo $item->clave?>"><?php echo $item->formapago_text?></option>
                                                            <?php } ?>
                                                          
                                                        </select>
                                                      </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                      <i class="fa fa-calendar"></i>
                                                      <label for="">Fecha Aplicación</label>
                                                      <input class="form-control" id="fecha_aplica" name="fecha_aplica" type="date" value="<?php echo date("Y-m-d"); ?>">
                                                    </div> 
                                                    <div class="col-md-6">
                                                      <div class="form-group">
                                                        <i class="fa fa-map-marker"></i>
                                                        <label for="">Lugar Expedición</label>
                                                        <input class="form-control" name="lugar_aplica" id="lugar_aplica" type="text" value="Tlaxcala">
                                                      </div>
                                                    </div> 
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                      <div class="form-group">
                                                        <i class="fa fa-usd"></i> <label for=""> Tipo Moneda</label>
                                                        <select id="moneda" name="moneda" class="form-control" required>
                                                          <option value="pesos">Pesos</option>
                                                          <option value="USD">Dólares Norteamericanos</option>
                                                           <option value="EUR">Euro</option>
                                                        </select>
                                                      </div>
                                                    </div> 
                                                    <div class="col-md-6">
                                                      <div class="form-group">
                                                        <i class="fa fa-file"></i> <label> Uso CFDI</label>
                                                        <select id="uso_cfdi" name="uso_cfdi" class="form-control" required>
                                                          <option value="" disabled selected>Selecciona una opción</option>
                                                          <?php foreach ($cfdi as $key) { ?>
                                                            <option value="<?php echo $key->uso_cfdi; ?>" ><?php echo $key->uso_cfdi_text; ?></option>
                                                          <?php } ?>
                                                        </select>
                                                      </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                      <div class="form-group">
                                                        <label for="CondicionesDePago">Condición de Pago</label>
                                                        <input type="text" name="CondicionesDePago" id="CondicionesDePago" class="form-control" onpaste="return false;" required value="Contado">
                                                      </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                      <div class="form-group">
                                                        <label for="CondicionesDePago">Tipo de Comprobante</label>
                                                        <select id="TipoDeComprobante" name="TipoDeComprobante" class="form-control " required="">
                                                        <option value="I">Ingreso</option>
                                                        <option value="T">Traslado</option>
                                                        <option value="E">Egreso</option>
                                                      </select>
                                                      </div>
                                                    </div>
                                                </div>
                                                <div class="card collapse-icon accordion-icon-rotate panel panel-default">
                                                    <div id="headingCollapse11" class="card-header border-bottom pb-1 pt-1 panel-heading">
                                                        <a data-toggle="collapse" href="#collapse11" aria-expanded="false" aria-controls="collapse11" class="card-title collapsed">Datos adicionales</a>
                                                    </div>
                                                    <div id="collapse11" role="tabpanel" aria-labelledby="headingCollapse11" class="collapse" style="">
                                                        <div class="card-content">
                                                            <div class="card-body">
                                                              <div class="row">
                                                                    <div class="col-md-6">
                                                                      <div class="form-group">
                                                                        <label class="control-label"># Proveedor</label>
                                                                        <input type="text" name="numproveedor" id="numproveedor" value="" class="form-control">
                                                                      </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                      <div class="form-group">
                                                                        <label class="control-label"># Orden de compra</label>
                                                                        <input type="text" name="numordencompra" id="numordencompra" value="" class="form-control">
                                                                      </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                      <div class="form-group">
                                                                        <label class="control-label">Lote</label>
                                                                        <input type="text" name="lote" id="lote" value="" class="form-control">
                                                                      </div>
                                                                    </div>
                                                              </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-header border-bottom pb-1 pt-1 panel-heading">
                                                        <a data-toggle="collapse" href="#collapse12" aria-expanded="false" aria-controls="collapse12" class="card-title collapsed">Complemento</a>
                                                    </div>
                                                    <div id="collapse12" role="tabpanel" aria-labelledby="headingCollapse12" class="collapse" style="">
                                                        <div class="card-content">
                                                            <div class="card-body">
                                                              <div class="row">
                                                                <div class="col-md-12">
                                                                  <div class="custom-switch custom-switch-success custom-control-inline">
                                                                    <input type="checkbox" class="custom-control-input" id="cartaporte" value="1" onchange="cartaporteview()">
                                                                    <label class="custom-control-label mr-1" for="cartaporte">Carta porte</label>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                              <div class="view_cartaporte">
                                                                <!----------------------->
                                                                  <div class="row">
                                                                    <div class="col-md-12">
                                                                      <h3>Ubicaciones</h3>
                                                                      <hr>
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-3">
                                                                      <label>Transporte internacional<span class="camporequerido">*</span></label>
                                                                      <select class="form-control" id="TranspInternac" name="TranspInternac" >
                                                                        <option>No</option>
                                                                        <option>Sí</option>
                                                                        
                                                                      </select>
                                                                    </div>
                                                                    <div class="col-md-3 divTotalDistRec">
                                                                      <label>Total distancia recorrida <span class="camporequerido">*</span></label>
                                                                      <input type="number" id="TotalDistRec" name="TotalDistRec" class="form-control">
                                                                    </div>
                                                                    <div class="col-md-3 divEntradaSalidaMerc" style="display:none;">
                                                                      <label>Entrada o salida de mercancía</label>
                                                                      <!-- si “TranspInternac” es no este campo se omitira -->
                                                                      <select class="form-control" id="EntradaSalidaMerc" name="EntradaSalidaMerc">
                                                                        <option></option>
                                                                        <option>Entrada</option>
                                                                        <option>Salida</option>
                                                                      </select>                                                                        
                                                                    </div>
                                                                    <div class="col-md-3 divViaEntradaSalida" style="display:none;">
                                                                      <label>Vía de entrada o salida</label>
                                                                      <!-- si “TranspInternac” es no este campo se omitira -->
                                                                      <select type="text" id="ViaEntradaSalida" name="ViaEntradaSalida" class="form-control">
                                                                        <option></option>
                                                                        <option value="01">Autotransporte</option>
                                                                        <option value="02">Transporte Marítimo</option>
                                                                        <option value="03">Transporte Aéreo</option>
                                                                        <option value="04">Transporte Ferroviario</option>
                                                                      </select>
                                                                      <!--01 Autotransporte -->
                                                                      <!--02 Transporte Marítimo -->
                                                                      <!--03 Transporte Aéreo -->
                                                                      <!--04 Transporte Ferroviario -->
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                      <table class="table" id="table_ubicaciones">
                                                                        <tbody>
                                                                          <tr>
                                                                              <td>
                                                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                                                  <div class="panel panel-default">
                                                                                      <div class="panel-heading" role="tab" id="headingOne">
                                                                                        <h4 class="panel-title" style="padding: 7px;">
                                                                                          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">Ubicacion Origen</a>
                                                                                        </h4>
                                                                                      </div>
                                                                                      <div id="collapse3" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                                                                        <div class="panel-body" style="padding: 7px;">
                                                                                          <div class="row">
                                                                                            <div class="col-md-2">
                                                                                              <button type="button" class="btn btn-primary" onclick="btn_origen_modal()">Origen</button>
                                                                                            </div>
                                                                                          </div>    
                                                                                          <div class="row">
                                                                                            <div class="col-md-2" style="display:none;">
                                                                                              <label>Tipo Estacion<span class="camporequerido">*</span></label>
                                                                                              <select name="TipoUbicacion_0" id="TipoUbicacion_0" class="TipoUbicacion form-control">
                                                                                                <option>Origen</option>
                                                                                                <!--<option>Destino</option>-->
                                                                                              </select>
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Tipo Estacion<span class="camporequerido">*</span></label>
                                                                                              <select name="TipoEstacion_0" id="TipoEstacion_0" class="TipoEstacion form-control">
                                                                                                <option value="01">01 Origen nacional</option>
                                                                                                <option value="02">02 Intermedia</option>
                                                                                                <option value="03">03 Destino Final Nacional</option>
                                                                                              </select>
                                                                                            </div>
                                                                                            <div class="col-md-2">
                                                                                              <label>ID de ubicación <span class="camporequerido">*</span></label>
                                                                                              <input name="IDUbicacion_0" id="IDUbicacion_0" class="IDUbicacion form-control mascarainput" placeholder="OR000123 / DE000123" data-inputmask="'mask': 'OR999999'">
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>RFC del remitente<span class="camporequerido">*</span></label>
                                                                                              <input name="RFCRemitenteDestinatario_0" id="RFCRemitenteDestinatario_0" class="RFCRemitenteDestinatario form-control" readonly>
                                                                                            </div>
                                                                                            <div class="col-md-4">
                                                                                              <label>Nombre del remitente<span class="camporequerido">*</span></label>
                                                                                              <select name="NombreRFC_0" id="NombreRFC_0" class="NombreRFC form-control"></select>
                                                                                            </div>
                                                                                          </div>
                                                                                          <div class="row">
                                                                                            <div class="col-md-4">
                                                                                              <label>Número de identificación o registro fiscal</label><!--Solo estara disponible si el remitente o detinatario tiene el rfc Generico(XEXX010101000)-->
                                                                                              <input type="number" name="NumRegIdTrib_0" id="NumRegIdTrib_0" class="NumRegIdTrib form-control" title="Solo si RFC del remitente se haya registrado la clave en el RFC genérica de residentes en el extranjero XEXX010101000">
                                                                                            </div>
                                                                                            <div class="col-md-2">
                                                                                              <label>Residencia Fiscal</label><!--Solo estara disponible si se agrego un valor en "NumRegIdTrib"-->
                                                                                              <select name="ResidenciaFiscal_0" id="ResidenciaFiscal_0" class="ResidenciaFiscal form-control" title="solo si se agrego Número de identificación o registro fiscal">
                                                                                                  <option value="">México</option>
                                                                                                  <option value="USA">Estados Unidos </option>
                                                                                                  <option value="FSM">Micronesia</option>
                                                                                                  <option value="MDA">Moldavia</option>
                                                                                              </select>
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Número de estación<span class="camporequerido">*</span></label>
                                                                                              <select name="NumEstacion_0" id="NumEstacion_0" class="NumEstacion form-control" ></select>
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Nombre de la estación<span class="camporequerido">*</span></label>
                                                                                              <input name="NombreEstacion_0" id="NombreEstacion_0" class="NombreEstacion form-control" readonly>
                                                                                            </div>
                                                                                          </div>
                                                                                          <div class="row">
                                                                                            <div class="col-md-4">
                                                                                              <label>Fecha y hora de salida<span class="camporequerido">*</span></label>
                                                                                              <input type="datetime-local" name="FechaHoraSalidaLlegada_0" id="FechaHoraSalidaLlegada_0" class="FechaHoraSalidaLlegada form-control" >
                                                                                            </div>
                                                                                            <div class="col-md-2" style="display:none;">
                                                                                              <label>Tipo Estacion</label>
                                                                                              <input name="DistanciaRecorrida_0" id="DistanciaRecorrida_0" class="DistanciaRecorrida form-control" value="0">
                                                                                            </div>
                                                                                            
                                                                                          </div>

                                                                                        </div>
                                                                                      </div>
                                                                                      <div class="panel-heading" role="tab" id="headingOne">
                                                                                        <h4 class="panel-title" style="padding: 7px;">
                                                                                          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">Domicilio origen</a>
                                                                                        </h4>
                                                                                      </div>
                                                                                      <div id="collapse4" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                                                                        <div class="panel-body" style="padding: 7px;">
                                                                                          <div class="row">
                                                                                            <div class="col-md-2">
                                                                                              <button type="button" class="btn btn-primary" onclick="btn_origen_domicilio_modal()">Origen</button>
                                                                                            </div>
                                                                                          </div>    
                                                                                          <div class="row">
                                                                                            <div class="col-md-6">
                                                                                              <label>Calle<span class="camporequerido">*</span></label>
                                                                                              <input type="text" name="Calle_0" id="Calle_0" class="Calle form-control">
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Número exterior</label>
                                                                                              <input type="text" name="NumeroExterior_0" id="NumeroExterior_0" class="NumeroExterior form-control">
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Número interior</label>
                                                                                              <input type="text" name="NumeroInterior_0" id="NumeroInterior_0" class="NumeroInterior form-control">
                                                                                            </div>
                                                                                          </div>
                                                                                          <div class="row">
                                                                                            <div class="col-md-3">
                                                                                              <label>Código postal<span class="camporequerido">*</span></label>
                                                                                              <input type="text" name="CodigoPostal_0" id="CodigoPostal_0" class="CodigoPostal form-control" readonly>
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Colonia</label>
                                                                                              <select type="text" name="Colonia_0" id="Colonia_0" class="Colonia form-control">
                                                                                              </select>
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Localidad</label>
                                                                                              <select type="text" name="Localidad_0" id="Localidad_0" class="Localidad form-control">
                                                                                              </select>
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Municipio</label>
                                                                                              <select type="text" name="Municipio_0" id="Municipio_0" class="Municipio form-control">
                                                                                              </select>
                                                                                            </div>
                                                                                            
                                                                                            
                                                                                          </div>
                                                                                          <div class="row">
                                                                                            <div class="col-md-3">
                                                                                              <label>Estado<span class="camporequerido">*</span></label>
                                                                                              <select type="text" name="Estado_0" id="Estado_0" class="Estado form-control">
                                                                                              </select>
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Pais<span class="camporequerido">*</span></label>
                                                                                              <select type="text" name="Pais_0" id="Pais_0" class="Pais form-control">
                                                                                              </select>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                              <label>Referencia</label>
                                                                                              <input type="text" name="Referencia_0" id="Referencia_0" class="Referencia form-control">
                                                                                            </div>
                                                                                            
                                                                                          </div>

                                                                                        </div>
                                                                                      </div>
                                                                                  </div>
                                                                                </div>
                                                                              </td>
                                                                          </tr>
                                                                          <tr>
                                                                              <td>
                                                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                                                  <div class="panel panel-default">
                                                                                      <div class="panel-heading" role="tab" id="headingOne">
                                                                                        <h4 class="panel-title" style="padding: 7px;">
                                                                                          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="true" aria-controls="collapse5">Ubicacion Destino</a>
                                                                                        </h4>
                                                                                      </div>
                                                                                      <div id="collapse5" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                                                                        <div class="panel-body" style="padding: 7px;">
                                                                                          <div class="row">
                                                                                            <div class="col-md-2">
                                                                                              <button type="button" class="btn btn-primary" onclick="btn_destino_modal()">Destino</button>
                                                                                            </div>
                                                                                          </div> 
                                                                                          <div class="row">
                                                                                            <div class="col-md-2" style="display:none;">
                                                                                              <label>Tipo Estacion<span class="camporequerido">*</span></label>
                                                                                              <select name="TipoUbicacion_1" id="TipoUbicacion_1" class="TipoUbicacion form-control">
                                                                                                <!--<option>Origen</option>-->
                                                                                                <option>Destino</option>
                                                                                              </select>
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Tipo Estacion<span class="camporequerido">*</span></label>
                                                                                              <select name="TipoEstacion_1" id="TipoEstacion_1" class="TipoEstacion form-control">
                                                                                                <option value="01">01 Origen nacional</option>
                                                                                                <option value="02">02 Intermedia</option>
                                                                                                <option value="03" selected>03 Destino Final Nacional</option>
                                                                                              </select>
                                                                                            </div>
                                                                                            <div class="col-md-2">
                                                                                              <label>ID de ubicación<span class="camporequerido">*</span></label>
                                                                                              <input name="IDUbicacion_1" id="IDUbicacion_1" class="IDUbicacion form-control mascarainput" placeholder="DE000123" data-inputmask="'mask': 'DE999999'">
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>RFC del destinatario<span class="camporequerido">*</span></label>
                                                                                              <input name="RFCRemitenteDestinatario_1" id="RFCRemitenteDestinatario_1" class="RFCRemitenteDestinatario form-control" readonly>
                                                                                            </div>
                                                                                            <div class="col-md-4">
                                                                                              <label>Nombre del destinatario<span class="camporequerido">*</span></label>
                                                                                              <select name="NombreRFC_1" id="NombreRFC_1" class="NombreRFC form-control"></select>
                                                                                            </div>
                                                                                          </div>
                                                                                          <div class="row">
                                                                                            <div class="col-md-4">
                                                                                              <label>Número de identificación o registro fiscal</label><!--Solo estara disponible si el remitente o detinatario tiene el rfc Generico(XEXX010101000)-->
                                                                                              <input type="number" name="NumRegIdTrib_1" id="NumRegIdTrib_1" class="NumRegIdTrib form-control" title="Solo si RFC del destinatario se haya registrado la clave en el RFC genérica de residentes en el extranjero XEXX010101000">
                                                                                            </div>
                                                                                            <div class="col-md-2">
                                                                                              <label>Residencia Fiscal</label><!--Solo estara disponible si se agrego un valor en "NumRegIdTrib"-->
                                                                                              <select name="ResidenciaFiscal_1" id="ResidenciaFiscal_1" class="ResidenciaFiscal form-control"  title="solo si se registro Número de identificación o registro fiscal">
                                                                                                  <option value="">México</option>
                                                                                                  <option value="USA">Estados Unidos </option>
                                                                                                  <option value="FSM">Micronesia</option>
                                                                                                  <option value="MDA">Moldavia</option>
                                                                                              </select>
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Número de estación<span class="camporequerido">*</span></label>
                                                                                              <select name="NumEstacion_1" id="NumEstacion_1" class="NumEstacion form-control" ></select>
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Nombre de la estación<span class="camporequerido">*</span></label>
                                                                                              <input name="NombreEstacion_1" id="NombreEstacion_1" class="NombreEstacion form-control" readonly>
                                                                                            </div>
                                                                                          </div>
                                                                                          <div class="row">
                                                                                            
                                                                                            <div class="col-md-4">
                                                                                              <label>Fecha y hora de llegada</label>
                                                                                              <input type="datetime-local" name="FechaHoraSalidaLlegada_1" id="FechaHoraSalidaLlegada_1" class="FechaHoraSalidaLlegada form-control" >
                                                                                            </div>
                                                                                            <div class="col-md-2" >
                                                                                              <label>Distancia recorrida<span class="camporequerido">*</span></label>
                                                                                              <input type="number" name="DistanciaRecorrida_1" id="DistanciaRecorrida_1" class="DistanciaRecorrida form-control" value="0">
                                                                                            </div>
                                                                                            
                                                                                          </div>

                                                                                        </div>
                                                                                      </div>
                                                                                      <div class="panel-heading" role="tab" id="headingOne">
                                                                                        <h4 class="panel-title" style="padding: 7px;">
                                                                                          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="true" aria-controls="collapse6">Domicilio Destino</a>
                                                                                        </h4>
                                                                                      </div>
                                                                                      <div id="collapse6" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                                                                        <div class="panel-body" style="padding: 7px;">
                                                                                          <div class="row">
                                                                                            <div class="col-md-2">
                                                                                              <button type="button" class="btn btn-primary" onclick="btn_destino_domicilio_modal()">Destino</button>
                                                                                            </div>
                                                                                          </div> 
                                                                                          <div class="row">
                                                                                            <div class="col-md-6">
                                                                                              <label>Calle<span class="camporequerido">*</span></label>
                                                                                              <input type="text" name="Calle_1" id="Calle_1" class="Calle form-control">
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Número exterior</label>
                                                                                              <input type="text" name="NumeroExterior_1" id="NumeroExterior_1" class="NumeroExterior form-control">
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Número interior</label>
                                                                                              <input type="text" name="NumeroInterior_1" id="NumeroInterior_1" class="NumeroInterior form-control">
                                                                                            </div>
                                                                                          </div>
                                                                                          <div class="row">
                                                                                            <div class="col-md-3">
                                                                                              <label>Código postal<span class="camporequerido">*</span></label>
                                                                                              <input type="text" name="CodigoPostal_1" id="CodigoPostal_1" class="CodigoPostal form-control" readonly>
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Colonia</label>
                                                                                              <select type="text" name="Colonia_1" id="Colonia_1" class="Colonia form-control">
                                                                                              </select>
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Localidad</label>
                                                                                              <select type="text" name="Localidad_1" id="Localidad_1" class="Localidad form-control">
                                                                                              </select>
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Municipio</label>
                                                                                              <select type="text" name="Municipio_1" id="Municipio_1" class="Municipio form-control">
                                                                                              </select>
                                                                                            </div>
                                                                                          </div>
                                                                                          <div class="row">
                                                                                            
                                                                                            <div class="col-md-3">
                                                                                              <label>Estado<span class="camporequerido">*</span></label>
                                                                                              <select type="text" name="Estado_1" id="Estado_1" class="Estado form-control">
                                                                                              </select>
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                              <label>Pais<span class="camporequerido">*</span></label>
                                                                                              <select type="text" name="Pais_1" id="Pais_1" class="Pais form-control">
                                                                                              </select>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                              <label>Referencia</label>
                                                                                              <input type="text" name="Referencia_1" id="Referencia_1" class="Referencia form-control">
                                                                                            </div>
                                                                                            
                                                                                          </div>

                                                                                        </div>
                                                                                      </div>
                                                                                  </div>
                                                                                </div>
                                                                              </td>
                                                                          </tr>
                                                                          
                                                                        </tbody>
                                                                      </table>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-12">
                                                                      <h3>Mercancias</h3>
                                                                      <hr>
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-2">
                                                                      <label>Peso bruto total<span class="camporequerido">*</span></label>
                                                                      <input type="number" id="PesoBrutoTotal" name="PesoBrutoTotal" class="form-control">
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                      <label>Unidad de peso<span class="camporequerido">*</span></label>
                                                                      <!--<input type="number" id="UnidadPeso" name="UnidadPeso" class="form-control">-->
                                                                      <select id="UnidadPeso" name="UnidadPeso" class="form-control">
                                                                        <option value="KGM">Kilogramo</option>
                                                                        <option value="MC">Microgramo</option>
                                                                        <option value="DJ">Decagramo</option>
                                                                      </select>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                      <label>Peso neto total<span class="camporequerido">*</span></label>
                                                                      <input type="number" id="PesoNetoTotal" name="PesoNetoTotal" class="form-control">
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                      <label>Número total de mercancías<span class="camporequerido">*</span></label>
                                                                      <input type="number" id="NumTotalMercancias" name="NumTotalMercancias" class="form-control" readonly> 
                                                                      <!--es el total de row o mercacioas-->
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                      <label>Cargo por tasación</label>
                                                                      <input type="number" id="CargoPorTasacion" name="CargoPorTasacion" class="form-control"> 
                                                                      <!--es el total de row o mercacioas-->
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-10"></div>
                                                                    <div class="col-md-2">
                                                                      <button type="button" class="btn btn-primary" onclick="addmodalmercancias()">Agregar Mercancias</button>
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-12">
                                                                      <table class="table" id="table_mercancias">
                                                                        <tbody class="add_table_mercancia">
                                                                          
                                                                        </tbody>
                                                                      </table>
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-10"></div>
                                                                    <div class="col-md-2">
                                                                      <button type="button" class="btn btn-primary" onclick="addmodaltransporte()">Agregar transporte Federal</button>
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-12">
                                                                      <table class="table" id="table_transporte">
                                                                        <tbody class="add_table_transporte">
                                                                          
                                                                        </tbody>
                                                                      </table>
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-10"></div>
                                                                    <div class="col-md-2">
                                                                      <button type="button" class="btn btn-primary" onclick="addmodalaereo()">Agregar transporte Aereo</button>
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-12">
                                                                      <table class="table" id="table_aereo">
                                                                        <tbody class="add_table_aereo">
                                                                          
                                                                        </tbody>
                                                                      </table>
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-12">
                                                                      <h3>Figura Transporte</h3>
                                                                      <hr>
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-3">
                                                                      <label>Clave del transporte*</label>
                                                                      <select id="CveTransporte" name="CveTransporte" class="form-control">
                                                                        <option value=""></option>
                                                                        <option value="01">01 Autotransporte</option>
                                                                        <option value="02" disabled>02 Transporte Marítimo</option>
                                                                        <option value="03">03 Transporte Aéreo</option>
                                                                        <option value="04" disabled>04 Transporte Ferroviario</option>
                                                                      </select>
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-10"></div>
                                                                    <div class="col-md-2">
                                                                      <button type="button" class="btn btn-primary" onclick="addmodaloperadores()">Agregar Operadores</button>
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-12">
                                                                      <table class="table" id="table_operador">
                                                                        <tbody class="add_table_operadores">
                                                                          
                                                                        </tbody>
                                                                      </table>
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-10"></div>
                                                                    <div class="col-md-2">
                                                                      <button type="button" class="btn btn-primary" onclick="addmodalpropietario()">Agregar Propietario</button>
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-12">
                                                                      <table class="table" id="table_propietario">
                                                                        <tbody class="add_table_propietario">
                                                                          
                                                                        </tbody>
                                                                      </table>
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-10"></div>
                                                                    <div class="col-md-2">
                                                                      <button type="button" class="btn btn-primary" onclick="addmodalarrendatario()">Agregar Arrendatario</button>
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-12">
                                                                      <table class="table" id="table_arrendatario">
                                                                        <tbody class="add_table_arrendatario">
                                                                          
                                                                        </tbody>
                                                                      </table>
                                                                    </div>
                                                                  </div>
                                                                <!------------------------>
                                                              </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="row">
                                                    <div class="col-md-12">
                                                      <label> Factura Relacionada</label>
                                                      <label class="switch">
                                                        <input title="Relacionar Factura" data-toggle="tooltip" type="checkbox" id="facturarelacionada">
                                                        <span class="slider round"></span>
                                                      </label>

                                                      <div class="form-group divfacturarelacionada" style="display:none;">
                                                        <div class="col-md-4">
                                                          <div class="form-group">
                                                            <label>Tipo Relación</label>
                                                            <select class="form-control" id="TipoRelacion" onchange="TipoRelacionselected()">
                                                              
                                                              <option value="01">01 Nota de crédito de los documentos relacionados</option>
                                                              <!--<option value="02">02 Nota de débito de los documentos relacionados</option>
                                                              <option value="03">03 Devolución de mercancía sobre facturas o traslados previos</option>
                                                              -->
                                                              <option value="04">04 Sustitución de los CFDI previos</option>
                                                              <!--
                                                              <option value="05">05 Traslados de mercancias facturados previamente</option>
                                                              <option value="06">06 Factura generada por los traslados previos</option>
                                                              <option value="07">07 CFDI por aplicación de anticipo</option>
                                                              -->
                                                            </select>
                                                          </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                          <div class="form-group">
                                                            <label>Folio Fiscal</label>
                                                            <input type="text" id="uuid_r" class="form-control" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="row">
                                                <div class="col-md-12" style="padding: 0px;">
                                                  <!--<div class="card-block">-->
                                                    <table id="table_conceptos" class="table table-responsive table-striped" style="width: 100%">
                                                      <thead>
                                                        <tr>
                                                          <th width="7%">NoIdentificacion</th>
                                                          <th width="5%">Cantidad</th>
                                                          <th width="15%">Unidad SAT</th>
                                                          <th width="15%">Concepto SAT</th>
                                                          <th width="20%">Descripción</th>
                                                          <th width="9%">Precio</th>
                                                          <th width="9%">Descuento</th>
                                                          <th width="10%">Monto</th>
                                                          <th width="5%">iva</th>
                                                          <th width="5%"></th>
                                                        </tr>
                                                        <tr>
                                                          <th >
                                                            <input type="text" name="sNoIdentificacion" id="sNoIdentificacion" class="form-control" value="">
                                                          </th>
                                                          <th >
                                                            <input type="number" name="scantidad" id="scantidad" class="form-control" value="1" onchange="calculartotal()">
                                                          </th>
                                                          <th >
                                                            <select name="ssunidadsat" id="sunidadsat" class="sunidadsat form-control span12 form-control browser-default chosen-select"></select>
                                                          </th>
                                                          <th >
                                                            <select name="sconseptosat" id="sconseptosat" class="sconseptosat form-control span12 form-control browser-default chosen-select"></select>
                                                          </th>
                                                          <th >
                                                            <input type="text" name="sdescripcion" id="sdescripcion" class="form-control">
                                                          </th>
                                                          <th >
                                                            <input type="number" name="sprecio" id="sprecio" class="form-control" value="0" onchange="calculartotal()">
                                                          </th>
                                                          <th>
                                                            <input type="number" name="descuento" id="descuento" data-diva="0" class="form-control"
                                                            value="0" style="" onchange="calculardescuento()" readonly>
                                                          </th>
                                                          <th class="montototal" style="text-align: center;"></th>
                                                          <th >
                                                            <!--<input title="Aplica IVA" type="checkbox" class="filled-in switch" id="aplicariva" checked="checked">
                                                            <label for="aplicariva"></label>-->
                                                            <label class="switch">
                                                              <input title="Aplica IVA" data-toggle="tooltip" type="checkbox" id="aplicariva" checked="checked">
                                                              <span class="slider round"></span>
                                                            </label>
                                                          </th>
                                                          <th>
                                                            <button type="button" class="btn btn-info" style="border-radius: 25px;background-color: rgb(6,20,55)" title="Agregar Concepto" onclick="agregarconcepto()"><i class="fa fa-plus"></i></button>
                                                          </th>
                                                        </tr>
                                                      </thead>
                                                      <tbody class="addcobrar">
                                                        
                                                        
                                                      </tbody>
                                                    </table>
                                                </div>
                                                
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-7">
                  
                                                </div>
                                                <div class="col-sm-5">
                                                  <div class="row">
                                                    <div class="col s5" style="text-align: right;">
                                                      Subtotal
                                                    </div>
                                                    <div class="col s7">
                                                      <span style="float: left;">$</span>
                                                      <input type="" name="Subtotal" id="Subtotal" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                                                    </div>
                                                  </div>
                                                  <div class="row">
                                                    <div class="col s5" style="text-align: right;">
                                                      Descuento
                                                    </div>
                                                    <div class="col s7">
                                                      <span style="float: left;">$</span>
                                                      <input type="" name="descuentof" id="descuentof" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                                                    </div>
                                                  </div> 
                                 
                                                  <div class="row">
                                                    <div class="col s5" style="text-align: right;">
                                                      I.V.A
                                                    </div>
                                                    <div class="col s7">
                                                      <span style="float: left;">$</span>
                                                      <input type="" name="iva" id="iva" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                                                    </div>
                                                  </div>
                                                  <!--<div class="row">
                                                      <div class="col s7" style="text-align: right;" onclick="calculartotales_set(1000)">
                                                        <input type="checkbox" id="risr" />
                                                        <label for="risr">10 % Retencion I.S.R.</label>
                                                      </div>
                                                      <div class="col s5">
                                                        <span style="float: left;">$</span>
                                                        <input type="" name="isr" id="isr" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                                                      </div>
                                                  </div> 
                                                  <div class="row">
                                                    <div class="col s7" style="text-align: right;" onclick="calculartotales_set(1000)">
                                                      <input type="checkbox" id="riva" />
                                                      <label for="riva">10.67 % Retencion I.V.A.</label>
                                                    </div>
                                                    <div class="col s5">
                                                      <span style="float: left;">$</span>
                                                      <input type="" name="ivaretenido" id="ivaretenido" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                                                    </div>
                                                  </div>
                                                  <div class="row">
                                                    <div class="col s7" style="text-align: right;" onclick="calculartotales_set(1000)">
                                                      <input type="checkbox" id="5almillar" />
                                                      <label for="5almillar" >5 al millar.</label>
                                                    </div>
                                                    <div class="col s5">
                                                      <span style="float: left;">$</span>
                                                      <input type="" name="5almillarval" id="5almillarval" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                                                    </div>
                                                  </div>
                                                  <div class="row">
                                                    <div class="col s7" style="text-align: right;" onclick="calculartotales_set(1000)">
                                                      <input type="checkbox" id="aplicaout" />
                                                      <label for="aplicaout" >6% Retención servicios de personal.</label>
                                                    </div>
                                                    <div class="col s5">
                                                      <span style="float: left;">$</span>
                                                      <input type="" name="outsourcing" id="outsourcing" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                                                    </div>
                                                  </div>  -->
                                 
                                                  <div class="row">
                                                    <div class="col s5" style="text-align: right;">
                                                      Total
                                                    </div>
                                                    <div class="col s7">
                                                      <span style="float: left;">$</span>
                                                      <input type="" name="total" id="total" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                                                    </div>
                                                  </div> 
                                                
                                                  <!-- fin --> 
                                                  <div class="row">
                                                    <div class="botonfinalizar"></div>
                                                    <div class="col-md-9">
                                                    </div>
                                                    <!--<div class="col-md-3 guadarremove">
                                                      <button class="btn cyan waves-effect waves-light right" onclick="verPrefactura()" type="button">Ver Prefactura <i class="fa fa-arrow-right"></i>
                                                      </button>
                                                    </div>-->
                                                  </div>
                                                </div>
                                                <div class="col-md-12">
                                                  <div class="col-md-5">
                                                  </div>
                                                  <div class="col-md-3">
                                                    <button class="btn btn-success mr-1 mb-1 registrofac" type="button">Facturar <i class="fa fa-file-pdf-o"></i>
                                                    </button>
                                                    <button class="btn btn-success mr-1 mb-1" type="button" onclick="registrofac_prew()">vista Previa <i class="fa fa-file-pdf-o"></i>
                                                    </button>
                                                  </div>
                                                </div>
                                            </div>




                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->







<div class="modal fade" id="modal_previefactura" tabindex="-1" aria-labelledby="exampleModalSizeXl" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Preview Factura</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body preview_iframe">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal" style="background: #fcb520;color: white;">Cerrar</button>
                <button class="btn btn-success mr-1 mb-1 registrofac" type="button">Facturar <i class="fa fa-file-pdf-o"></i></button>
                <!--<button type="button" class="btn btn-primary registrofac" id="btn_savecomplemento" style="background: #12264b;color: white;">Timbrar complemento</button>-->
            </div>
        </div>
    </div>
</div>