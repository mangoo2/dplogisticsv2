<script type="text/javascript">
	$(document).ready(function($) {
		$("#table_list").DataTable({
			dom: 'Bfrtip',
        	buttons: [
            	'excel', 'pdf', 'print','pageLength'
        	]
        });
		$('.dt-buttons').addClass('btn-group');
  		$('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel, .buttons-page-length').addClass('btn btn-primary mb-2');
	});
	
	
</script>