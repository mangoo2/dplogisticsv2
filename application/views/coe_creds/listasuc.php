
<style type="text/css">
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
</style>            
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header"><?php echo $resultsuc->suc_nombre?></div>
                            <!--<p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>-->
                            <p class="content-sub-header mb-1">Guías en crédito</p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Últimos créditos</h4>
                                        <div class="box-tools pull-right">
                                            <a href="<?php echo base_url();?>Coe_creds/cons/<?php echo $codigo;?>" type="button" class="btn btn-success mr-1 mb-1">Buscar..</a>
                                            <a href="<?php echo base_url();?>Coe_creds" type="button" class="btn btn-sm btn-light mr-1 mb-1"><i class="fa fa-arrow-left" style="color:black;"></i></a>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 table-bordered table-striped thead-light tablevc" id="table_list">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>FECHA</th>
                                                                    <th>EMPRESA</th>
                                                                    <th>AEROLINEA</th>
                                                                    <th>FOLIO</th>
                                                                    <th>GUÍA</th>
                                                                    <th>ORIGEN</th>
                                                                    <th>DESTINO</th>
                                                                    <th>IMPORTE</th>
                                                                    <th>CLIENTE</th>
                                                                    <th>ADMIN</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php $xrow=1; 
                                                                    foreach ($resulgpags->result() as $item) { ?>
                                                                    <tr>
                                                                        <td><?php echo $xrow;?></td>
                                                                        <td><?php echo $item->fecini;?></td>
                                                                        <td><?php echo $item->emp;?></td>
                                                                        <td><?php echo $item->aer_nom;?></td>
                                                                        <td><?php echo $item->folio;?></td>
                                                                        <td><?php echo $item->guia;?></td>
                                                                        <td><?php echo $item->origen;?></td>
                                                                        <td><?php echo $item->destino;?></td>
                                                                        <td><?php echo $item->importe;?></td>
                                                                        <td><?php echo $item->cli_nombre.' '.$item->cli_paterno.' '.$item->cli_materno;?></td>
                                                                        <td><?php echo $item->pag_adm;?></td>
                                                                    </tr>
                                                                <?php $xrow++;
                                                                    } ?>
                                                                
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->