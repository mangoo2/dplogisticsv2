<?php 
    if( isset($_GET["rep_fecini"]) ){
        $rep_fecini=$_GET["rep_fecini"];
    }else{
        $rep_fecini=date('Y-m-d');
    }
    if( isset($_GET["rep_fecfin"]) ){
        $rep_fecfin=$_GET["rep_fecfin"];
    }else{
        $rep_fecfin=date('Y-m-d');
    }

    $result_req_ultimas = $this->ModeloReqs->list_reqs_ultimas_search($codigo,$rep_fecini,$rep_fecfin);

?>
<style type="text/css">
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
</style>            
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header"><?php echo $resultsuc->suc_nombre?></div>
                            <!--<p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>-->
                            <p class="content-sub-header mb-1">Requisiciones de la estación</p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                        <div class="box-tools pull-right">
                                            <?php 
                                                        if($botonatras==1){
                                                    ?>
                                            <a href="<?php echo base_url();?>Coe_reqs/req/<?php echo $codigo;?>" type="button" class="btn btn-sm btn-light mr-1 mb-1"><i class="fa fa-arrow-left" style="color:black;"></i></a>
                                            <?php }else{ ?>
                                                <a onclick="cargaloginpage('<?php echo base_url();?>Suc_guias')" type="button" class="btn btn-sm btn-light mr-1 mb-1"><i class="fa fa-arrow-left" style="color:black;"></i></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h4>Fecha de consulta</h4>
                                                    </div>
                                                </div>
                                                <form action="<?php echo base_url();?>Coe_reqs/cons/<?php echo $codigo;?>" method="get">
                                                    <div class="row">
                                                        
                                                        <div class="col-md-2 form-group">
                                                            <label >Fecha Inicio</label>
                                                            <input type="date" name="rep_fecini" value="<?php echo $rep_fecini;?>" class="form-control">
                                                        </div>
                                                        
                                                        <div class="col-md-2 form-group">
                                                            <label >Fecha Termino</label>
                                                            <input type="date" name="rep_fecfin" value="<?php echo $rep_fecfin;?>" class="form-control">
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button  type="submit" class="btn btn-success mr-1 mb-1 loaderbtn">Buscar..</button> 
                                                        </div>
                                                    </div>
                                                </form>
                                                
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 table-striped table-bordered tablevc" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Finalizado</th>
                                                                    <th>Solicitado</th>
                                                                    <th>Nombre</th>
                                                                    <th>Tipo de requisición</th>
                                                                    <th>Playera COECSA</th>
                                                                    <th>Camisa COECSA</th>
                                                                    <th>Bota industrial de seguridad</th>
                                                                    <th>Impermeable</th>
                                                                    <th>Botas de seguridad para lluvia</th>
                                                                    <th>Faja</th>
                                                                    <th>Chaleco reflejante</th>
                                                                    <th>Guantes</th>
                                                                    <th>Tapones</th>
                                                                    <th>Observación</th>
                                                                    <th>Estatus</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                    $x_row=1; 
                                                                    foreach ($result_req_ultimas->result() as $item) { 
                                                                            
                                                                        ?>
                                                                    <tr>
                                                                        <td><?php echo $x_row;?></td>
                                                                        <td><?php echo date('d/m/Y',strtotime($item->req_fecfin)).'<br>'.date('H:i:s',strtotime($item->req_fecfin));?></td>
                                                                        <td><?php echo date('d/m/Y',strtotime($item->req_fecini)).'<br>'.date('H:i:s',strtotime($item->req_fecini));?></td>
                                                                        <td><?php echo $item->req_nombre;?></td>
                                                                        <td><?php echo $item->req_tipo;?></td>
                                                                        <td><?php echo $item->req_playera;?></td>
                                                                        <td><?php echo $item->req_camisa;?></td>
                                                                        <td><?php echo $item->req_botaind;?></td>
                                                                        <td><?php echo $item->req_impermeable;?></td>
                                                                        <td><?php echo $item->req_botaseg;?></td>
                                                                        <td><?php echo $item->req_faja;?></td>
                                                                        <td><?php echo $item->req_chalecoref;?></td>
                                                                        <td><?php echo $item->req_guantes;?></td>
                                                                        <td><?php echo $item->req_tapones;?></td>
                                                                        <td><?php echo $item->req_obs;?></td>
                                                                        <td><?php echo $item->req_carres.$item->req_estatus;?></td>
                                                                    </tr>
                                                                <?php $x_row++; 
                                                                    } ?>
                                                                
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->