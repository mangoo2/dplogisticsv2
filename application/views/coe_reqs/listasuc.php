
<style type="text/css">
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }

</style>            
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header"><?php echo $resultsuc->suc_nombre?></div>
                            <!--<p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>-->
                            <p class="content-sub-header mb-1">Guías utilizadas</p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Requisiciones en proceso</h4>
                                        <?php 
                                            if($botonatras==1){
                                        ?>
                                        <div class="box-tools pull-right">
                                            <a href="<?php echo base_url();?>Coe_reqs" type="button" class="btn btn-sm btn-light mr-1 mb-1"><i class="fa fa-arrow-left" style="color:black;"></i></a>
                                        </div>
                                        <?php }else{ ?>
                                            <div class="box-tools pull-right">
                                                <a onclick="editar(0)" type="button" class="btn btn-success mr-1 mb-1">Agregar requisición</a> 
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 table-striped table-bordered tablevc" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Fecha</th>
                                                                    <th>Nombre</th>
                                                                    <th>Tipo de requisición</th>
                                                                    <th>Playera COECSA</th>
                                                                    <th>Camisa COECSA</th>
                                                                    <th>Bota industrial de seguridad</th>
                                                                    <th>Impermeable</th>
                                                                    <th>Botas de seguridad para lluvia</th>
                                                                    <th>Faja</th>
                                                                    <th>Chaleco reflejante</th>
                                                                    <th>Guantes</th>
                                                                    <th>Tapones</th>
                                                                    <th>Observación</th>
                                                                    <th>Estatus</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                    $x_row=1; 
                                                                    foreach ($result_req_proceso->result() as $item) { 
                                                                        $btn_acciones='<a onclick="editar_s('.$item->rid.','.$botonatras.')" type="button" class="btn btn-sm btn-success mr-1 mb-1 edit_res_'.$item->rid.'"
                                                                            data-req_nombre="'.$item->req_nombre.'"
                                                                            data-req_tipo="'.$item->req_tipo.'"
                                                                            data-req_playera="'.$item->req_playera.'"
                                                                            data-req_camisa="'.$item->req_camisa.'"
                                                                            data-req_botaind="'.$item->req_botaind.'"
                                                                            data-req_impermeable="'.$item->req_impermeable.'"
                                                                            data-req_botaseg="'.$item->req_botaseg.'"
                                                                            data-req_faja="'.$item->req_faja.'"
                                                                            data-req_chalecoref="'.$item->req_chalecoref.'"
                                                                            data-req_guantes="'.$item->req_guantes.'"
                                                                            data-req_tapones="'.$item->req_tapones.'"
                                                                            data-req_obs="'.$item->req_obs.'"
                                                                            data-req_estatus="'.$item->req_estatus.'"
                                                                            ><i class="fa fa-pencil fa-fw"></i></a>';
                                                                            if($botonatras==0){
                                                                                $btn_acciones.='<a type="button" onclick="deletereq('.$item->rid.')" class="btn btn-sm btn-danger mr-1 mb-1"><i class="fa fa-trash fa-fw"></i></a>';
                                                                            }
                                                                        ?>
                                                                    <tr>
                                                                        <td><?php echo $x_row;?></td>
                                                                        <td><?php echo date('d/m/Y',strtotime($item->req_fecini)).'<br>'.date('H:i:s',strtotime($item->req_fecini));?></td>
                                                                        <td><?php echo $item->req_nombre;?></td>
                                                                        <td><?php echo $item->req_tipo;?></td>
                                                                        <td><?php echo $item->req_playera;?></td>
                                                                        <td><?php echo $item->req_camisa;?></td>
                                                                        <td><?php echo $item->req_botaind;?></td>
                                                                        <td><?php echo $item->req_impermeable;?></td>
                                                                        <td><?php echo $item->req_botaseg;?></td>
                                                                        <td><?php echo $item->req_faja;?></td>
                                                                        <td><?php echo $item->req_chalecoref;?></td>
                                                                        <td><?php echo $item->req_guantes;?></td>
                                                                        <td><?php echo $item->req_tapones;?></td>
                                                                        <td><?php echo $item->req_obs;?></td>
                                                                        <td><?php echo $item->req_carres.$item->req_estatus;?></td>
                                                                        <td><?php echo $btn_acciones;?></td>
                                                                    </tr>
                                                                <?php $x_row++; 
                                                                    } ?>
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Últimas requisiciones</h4>
                                        
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-4">
                                                    </div>
                                                    <?php 
                                                        if($botonatras==1){
                                                    ?>
                                                    <div class="col-md-8" style="text-align:end;">
                                                        <a href="<?php echo base_url();?>Coe_reqs/cons/<?php echo $codigo;?>" type="button" class="btn btn-success mr-1 mb-1">Buscar..</a>  
                                                    </div>
                                                <?php }else{ ?>
                                                    <div class="col-md-8" style="text-align:end;">
                                                        
                                                    </div>
                                                <?php } ?>
                                                    
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 table-striped table-bordered tablevc" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Finalizado</th>
                                                                    <th>Solicitado</th>
                                                                    <th>Nombre</th>
                                                                    <th>Tipo de requisición</th>
                                                                    <th>Playera COECSA</th>
                                                                    <th>Camisa COECSA</th>
                                                                    <th>Bota industrial de seguridad</th>
                                                                    <th>Impermeable</th>
                                                                    <th>Botas de seguridad para lluvia</th>
                                                                    <th>Faja</th>
                                                                    <th>Chaleco reflejante</th>
                                                                    <th>Guantes</th>
                                                                    <th>Tapones</th>
                                                                    <th>Observación</th>
                                                                    <th>Estatus</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                    $x_row=1; 
                                                                    foreach ($result_req_ultimas->result() as $item) { 
                                                                            
                                                                        ?>
                                                                    <tr>
                                                                        <td><?php echo $x_row;?></td>
                                                                        <td><?php echo date('d/m/Y',strtotime($item->req_fecfin)).'<br>'.date('H:i:s',strtotime($item->req_fecfin));?></td>
                                                                        <td><?php echo date('d/m/Y',strtotime($item->req_fecini)).'<br>'.date('H:i:s',strtotime($item->req_fecini));?></td>
                                                                        <td><?php echo $item->req_nombre;?></td>
                                                                        <td><?php echo $item->req_tipo;?></td>
                                                                        <td><?php echo $item->req_playera;?></td>
                                                                        <td><?php echo $item->req_camisa;?></td>
                                                                        <td><?php echo $item->req_botaind;?></td>
                                                                        <td><?php echo $item->req_impermeable;?></td>
                                                                        <td><?php echo $item->req_botaseg;?></td>
                                                                        <td><?php echo $item->req_faja;?></td>
                                                                        <td><?php echo $item->req_chalecoref;?></td>
                                                                        <td><?php echo $item->req_guantes;?></td>
                                                                        <td><?php echo $item->req_tapones;?></td>
                                                                        <td><?php echo $item->req_obs;?></td>
                                                                        <td><?php echo $item->req_carres.$item->req_estatus;?></td>
                                                                    </tr>
                                                                <?php $x_row++; 
                                                                    } ?>
                                                                
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->


<div id="modalform" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_datos">
            <input type="hidden" id="rid" name="rid" value="0">
            <input type="hidden" id="sucursal" name="sucursal" value="<?php echo $codigo;?>">
            <div class="row">
                <div class="col-md-6">
                    <label>Nombre</label>
                    <input type="text" name="req_nombre" id="req_nombre" class="form-control" required>
                </div>
                <div class="col-md-6">
                    <label>Tipo de requisición</label>
                    <select name="req_tipo" id="req_tipo" class="form-control">
                        <option value="Nuevo">Nuevo</option>
                        <option value="Reposición">Reposición</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Player</label>
                    <input type="text" class="form-control" placeholder="Playera COECSA" name="req_playera" id="req_playera" required>                
                </div>
                <div class="col-md-6">
                    <label>Camisa</label>
                    <input type="text" class="form-control" placeholder="Camisa COECSA" name="req_camisa" id="req_camisa" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Bota de industrial</label>
                    <input type="text" class="form-control" placeholder="Bota industrial de seguridad" name="req_botaind" id="req_botaind" >
                </div>
                <div class="col-md-6">
                    <label>Impermeable</label>
                    <input type="text" class="form-control" placeholder="Impermeable" name="req_impermeable" id="req_impermeable" >
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Bota de seguridad</label>
                    <input type="text" class="form-control" placeholder="Botas de seguridad para lluvia" name="req_botaseg" id="req_botaseg" >
                </div>
                <div class="col-md-6">
                    <label>Faja</label>
                    <input type="text" class="form-control" placeholder="Faja" name="req_faja" id="req_faja" >
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Chaleco reflejante</label>
                    <input type="text" class="form-control" placeholder="Chaleco reflejante" name="req_chalecoref" id="req_chalecoref" >
                </div>
                <div class="col-md-6">
                    <label>Guantes</label>
                    <input type="text" class="form-control" placeholder="Guantes" name="req_guantes" id="req_guantes" >
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Tapones</label>
                    <input type="text" class="form-control" placeholder="Tapones" name="req_tapones" id="req_tapones" >
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>Observaciones</label>
                    <textarea class="form-control" id="req_obs" name="req_obs" ></textarea>
                </div>
            </div>
            
            
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary saveform" onclick="saveform()">Actualizar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div id="modalform2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Modificar requisición</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_datos2">
            <input type="hidden" id="rid2" name="rid" value="0">
            <div class="row">
                <div class="col-md-12">
                    <label>Estatus</label>
                    <select class="form-control" name="req_estatus" id="req_estatus">
                        <option value="Solicitado">Solicitado</option>
                        <option value="Proceso">En Proceso</option>
                        <option value="Pausado">En pausa</option>
                        <option value="Cancelado">Cancelado</option>
                        <option value="Finalizado">Finalizar</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Nombre: </label>
                    <span id="info_req_nombre"></span>
                </div>
                <div class="col-md-6">
                    <label>Tipo de requisición: </label>
                    <span id="info_req_tipo"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Player: </label>
                    <span id="info_req_playera"></span>
                </div>
                <div class="col-md-6">
                    <label>Camisa: </label>
                    <span id="info_req_camisa"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Bota de industrial: </label>
                    <span id="info_req_botaind"></span>
                </div>
                <div class="col-md-6">
                    <label>Impermeable: </label>
                    <span id="info_req_impermeable"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Bota de seguridad: </label>
                    <span id="info_req_botaseg"></span>
                </div>
                <div class="col-md-6">
                    <label>Faja: </label>
                    <span id="info_req_faja"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Chaleco reflejante: </label>
                    <span id="info_req_chalecoref"></span>
                </div>
                <div class="col-md-6">
                    <label>Guantes: </label>
                    <span id="info_req_guantes"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Tapones: </label>
                    <span id="info_req_tapones"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>Observaciones: </label>
                    <span id="info_req_obs"></span>
                </div>
            </div>
            
            
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary saveform" onclick="updatestatus()">Actualizar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>