<!-- BEGIN : Main Content-->
<div class="main-content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="content-header"><?php echo $title;?> Origen</div>
                <!--<p class="content-sub-header mb-1">All table styles are inherited in Bootstrap 4.3.1, meaning any nested tables will be styled in the same manner as the parent.</p>-->
            </div>
        </div>
        <!--Basic Table Starts-->
        <section id="simple-table">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <!--<h4 class="card-title">Default Table</h4>-->
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <!--------------->
                                <form id="form_registro" method="POST">
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="hidden" name="rid" id="rid" value="<?php echo $id;?>">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5 class="box-title">Datos generales</h5>
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 form-group">
                                                    <label>DPL</label>
                                                    <select class="form-control" name="dpl" id="dpl" required style="width: 100%">   
                                                        <?php if($dpl!=''){?> 
                                                            <option value="<?php echo $dpl ?>"><?php echo $dpltxt ?></option>
                                                        <?php } ?>  
                                                    </select>
                                                </div>
                                                <div class="col-md-5 form-group">
                                                    <label>ID ORIGEN</label>
                                                    <input name="idorigen" class="form-control mascarainput" placeholder="OR000123 / DE000123" data-inputmask="'mask': 'OR999999'" inputmode="text" value="<?php echo $idorigen ?>" required>
                                                    <!--
                                                      <input name="IDUbicacion_1" id="IDUbicacion_1" class="IDUbicacion form-control mascarainput" placeholder="DE000123" data-inputmask="'mask': 'DE999999'">
                                                      <input name="IDUbicacion_0" id="IDUbicacion_0" class="IDUbicacion form-control mascarainput" placeholder="OR000123 / DE000123" data-inputmask="'mask': 'OR999999'" inputmode="text">
                                                    -->
                                                </div>
                                                <div class="col-md-4 form-group">
                                                    <label>NUM DE ESTACIÓN</label>
                                                    <select class="form-control" name="num_estacion" id="num_estacion" required style="width: 100%">
                                                        <?php if($num_estacion!=''){?> 
                                                            <option value="<?php echo $num_estacion ?>"><?php echo $num_estaciontxt.' ('.$num_estacion.')' ?></option>
                                                        <?php } ?>  
                                                    </select>
                                                </div>
                                            </div>    
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-primary btn_registro" onclick="guardar_registro()">Guardar</button>
                                            <a onclick="javascript:history.back()" class="btn btn-danger">Regresar</a>
                                        </div>
                                    </div>
                            </div>            
                        </div>
                    </div>
                </div>            
            </div>
        </section>

    </div>
</div>            