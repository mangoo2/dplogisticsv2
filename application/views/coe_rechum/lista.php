
<style type="text/css">
    .tablevc th,.tablevc td{font-size: 12px;text-align: center;padding: 8px;}
    .table-responsive{padding: 0px;}
    .divbuttonstable{width: 135px;}
    .file_user_default{
        background: url(<?php echo base_url()?>public/img/thumbnail.png);
    }
    #table_list{
        width: 100% !important;
    }
</style>   
       
            <!-- BEGIN : Main Content-->
            <input type="hidden" id="viewsuc" value="<?php echo $viewsuc;?>">
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Recursos Humanos</div>
                            <!--<p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>-->
                            <p class="content-sub-header mb-1">administración</p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Empleados actuales</h4>
                                        
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12" style="text-align: end;">
                                                        <a onclick="editar(0)" type="button" class="btn btn-success mr-1 mb-1">Agregar</a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive"><i class="fa fa-file-excel"></i>
                                                        <table class="table m-0 table-striped table-bordered tablevc" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Estación</th>
                                                                    <th>No. De empleado</th>
                                                                    <th>Foto</th>
                                                                    <th>Nombre</th>
                                                                    <th>Departamento</th>
                                                                    <th>Cargo / Puesto</th>
                                                                    <th>Brinda servicios a</th>
                                                                    <th>Vigencia ID</th>
                                                                    <th>Vigencia TIA</th>
                                                                    <th>Fecha de nacimiento</th>
                                                                    <th>Domicilio</th>
                                                                    <th>Contacto</th>

                                                                    <th>Identificación</th>
                                                                    <th>Observaciones</th>
                                                                    <th>Tipo de empleado</th>
                                                                    <th>Sueldo base</th>
                                                                    <th>Bonificaciones</th>
                                                                    <th>Fecha alta IMSS</th>
                                                                    <th>NSS</th>
                                                                    <th>Inicio de contrato</th>
                                                                    <th>Termino de contrato</th>
                                                                    <th>Horario</th>
                                                                    <th>Banco nombre</th>
                                                                    <th>Titular de la cuenta</th>
                                                                    <th>No. Cuenta</th>
                                                                    <th>No. Plastico</th>
                                                                    <th>No. Clabe</th>

                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                    $idrow=1; 
                                                                    foreach ($list->result() as $item) { 
                                                                        $url_img=FCPATH.'_files/_coe/rechum/'.$item->rh_foto;
                                                                        $url_img1=base_url().'_files/_coe/rechum/'.$item->rh_foto;
                                                                        if(file_exists($url_img)){
                                                                            $file_img='<img src="'.$url_img1.'" style="width:100px">';
                                                                        }else{
                                                                            $file_img='<div class="file_user_default" data-info="'.$url_img.'"></div>';
                                                                        }
                                                                        ?>
                                                                    <tr>
                                                                        <td><?php echo $idrow;?></td>
                                                                        <td><?php echo $item->suc_nombre;?></td>
                                                                        <td><?php echo $item->rh_empleado;?></td>
                                                                        <td><?php echo $file_img;?></td>
                                                                        <td><?php echo $item->rh_nom.' '.$item->rh_pat.' '.$item->rh_mat;?></td>
                                                                        <td><?php echo $item->rh_depto;?></td>
                                                                        <td><?php echo $item->rh_cargo;?></td>
                                                                        <td><?php echo $item->rh_serviciosa;?></td>
                                                                        <td><?php echo $item->rh_vigenciaid;?></td>
                                                                        <td><?php echo $item->rh_vigenciatia;?></td>
                                                                        <td><?php echo $item->rh_fecnac;?></td>
                                                                        <td><?php echo $item->rh_domicilio;?></td>
                                                                        <td><?php echo $item->rh_contacto;?></td>

                                                                        <td><?php echo $item->rh_identificacion;?></td>
                                                                        <td><?php echo $item->rh_obs;?></td>
                                                                        <td><?php echo $item->rhl_tipo;?></td>
                                                                        <td><?php echo $item->rhl_sueldo;?></td>
                                                                        <td><?php echo $item->rhl_bonos;?></td>
                                                                        <td><?php echo $item->rhl_altaimss;?></td>
                                                                        <td><?php echo $item->rhl_nss;?></td>
                                                                        <td><?php echo $item->rhl_conini;?></td>
                                                                        <td><?php echo $item->rhl_confin;?></td>
                                                                        <td><?php echo $item->rhl_horario;?></td>
                                                                        <td><?php echo $item->rhb_banco;?></td>
                                                                        <td><?php echo $item->rhb_titular;?></td>
                                                                        <td><?php echo $item->rhb_cuenta;?></td>
                                                                        <td><?php echo $item->rhb_plastico;?></td>
                                                                        <td><?php echo $item->rhb_clabe;?></td>

                                                                        <td>
                                                                            <div class="divbuttonstable">
                                                                                <a onclick="editar(<?php echo $item->rid;?>)" 
                                                                                    type="button" class="btn btn-sm btn-success mr-1 mb-1 edit_form_<?php echo $item->rid;?>"
                                                                                    data-sucursal="<?php echo $item->sucursal;?>"
                                                                                    data-rh_empleado="<?php echo $item->rh_empleado;?>"
                                                                                    data-rh_nom="<?php echo $item->rh_nom;?>"
                                                                                    data-rh_pat="<?php echo $item->rh_pat;?>"
                                                                                    data-rh_mat="<?php echo $item->rh_mat;?>"
                                                                                    data-rh_depto="<?php echo $item->rh_depto;?>"
                                                                                    data-rh_cargo="<?php echo $item->rh_cargo;?>"
                                                                                    data-rh_serviciosa="<?php echo $item->rh_serviciosa;?>"
                                                                                    data-rh_vigenciaid="<?php echo $item->rh_vigenciaid;?>"
                                                                                    data-rh_vigenciatia="<?php echo $item->rh_vigenciatia;?>"
                                                                                    data-rh_fecnac="<?php echo $item->rh_fecnac;?>"
                                                                                    data-rh_domicilio="<?php echo $item->rh_domicilio;?>"
                                                                                    data-rh_contacto="<?php echo $item->rh_contacto;?>"
                                                                                    data-rh_identificacion="<?php echo $item->rh_identificacion;?>"
                                                                                    data-rh_obs="<?php echo $item->rh_obs;?>"
                                                                                    data-rh_foto="<?php echo $item->rh_foto;?>"
                                                                                    data-rhl_tipo="<?php echo $item->rhl_tipo;?>"
                                                                                    data-rhl_sueldo="<?php echo $item->rhl_sueldo;?>"
                                                                                    data-rhl_bonos="<?php echo $item->rhl_bonos;?>"
                                                                                    data-rhl_altaimss="<?php echo $item->rhl_altaimss;?>"
                                                                                    data-rhl_nss="<?php echo $item->rhl_nss;?>"
                                                                                    data-rhl_conini="<?php echo $item->rhl_conini;?>"
                                                                                    data-rhl_confin="<?php echo $item->rhl_confin;?>"
                                                                                    data-rhl_horario="<?php echo $item->rhl_horario;?>"
                                                                                    data-rhb_banco="<?php echo $item->rhb_banco;?>"
                                                                                    data-rhb_titular="<?php echo $item->rhb_titular;?>"
                                                                                    data-rhb_cuenta="<?php echo $item->rhb_cuenta;?>"
                                                                                    data-rhb_plastico="<?php echo $item->rhb_plastico;?>"
                                                                                    data-rhb_clabe="<?php echo $item->rhb_clabe;?>"
                                                                                ><i class="fa fa-pencil fa-fw"></i></a>
                                                                                <?php if($viewsuc==1){ ?>
                                                                                <a href="<?php echo base_url().'Suc_rechum/reg_evis/'.$item->sucursal.'/'.$item->rid; ?>" type="button" class="btn btn-sm btn-info mr-1 mb-1"><i class="fa fa-folder-open fa-fw"></i></a>
                                                                                <?php }else{ ?>
                                                                                    <a href="<?php echo base_url().'Coe_rechum/reg_evis/'.$item->sucursal.'/'.$item->rid; ?>" type="button" class="btn btn-sm btn-info mr-1 mb-1"><i class="fa fa-folder-open fa-fw"></i></a>
                                                                                <?php } ?>
                                                                                <?php if($viewsuc==0){ ?>
                                                                                    <a type="button" onclick="deleterh(<?php echo $item->rid;?>)" class="btn btn-sm btn-danger mr-1 mb-1"><i class="fa fa-trash fa-fw"></i></a>
                                                                                <?php }?>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                <?php $idrow++;} ?>
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>

                                
                                
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->

<div id="modalform" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_datos">
            <input type="hidden" id="rid" name="rid" value="0">
            <div class="row">
                <div class="col-md-6 sucursal_view">
                    <label>Estación</label>
                    <select name="sucursal" id="sucursal" class="form-control" required>
                        <?php foreach ($list_suc as $item) { ?>
                            <option value="<?php echo $item->sucursal;?>"><?php echo $item->suc_nombre;?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <label>Número de empleado</label>
                    <input type="text" class="form-control" name="rh_empleado" id="rh_empleado" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label>Nombre (s)</label>
                    <input type="text" class="form-control" placeholder="" name="rh_nom" id="rh_nom" required>                
                </div>
                <div class="col-md-4">
                    <label>Apellido paterno</label>
                    <input type="text" class="form-control" placeholder="" name="rh_pat" id="rh_pat" required>
                </div>
                <div class="col-md-4">
                    <label>Apellido materno</label>
                    <input type="text" class="form-control" placeholder="" name="rh_mat" id="rh_mat" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Departamento</label>
                    <input type="text" class="form-control" placeholder="" name="rh_depto" id="rh_depto" required>                
                </div>
                <div class="col-md-6">
                    <label>Cargo / Puesto</label>
                    <input type="text" class="form-control" placeholder="" name="rh_cargo" id="rh_cargo" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>Brinda servicios a</label>
                    <input type="text" class="form-control" placeholder="" name="rh_serviciosa" id="rh_serviciosa" required>                
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label>Vigencia ID</label>
                    <input type="text" class="form-control" placeholder="" name="rh_vigenciaid" id="rh_vigenciaid" required>                
                </div>
                <div class="col-md-4">
                    <label>Vigencia TIA</label>
                    <input type="text" class="form-control" placeholder="" name="rh_vigenciatia" id="rh_vigenciatia" required>
                </div>
                <div class="col-md-4">
                    <label>Fecha de nacimiento</label>
                    <input type="date" class="form-control" placeholder="" name="rh_fecnac" id="rh_fecnac" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Domicilio</label>
                    <textarea class="form-control" id="rh_domicilio" name="rh_domicilio" ></textarea>               
                </div>
                <div class="col-md-6">
                    <label>Contacto</label>
                    <textarea class="form-control" id="rh_contacto" name="rh_contacto" ></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Identificación</label>
                    <textarea class="form-control" id="rh_identificacion" name="rh_identificacion" ></textarea>               
                </div>
                <div class="col-md-6">
                    <label>Observaciones</label>
                    <textarea class="form-control" id="rh_obs" name="rh_obs" ></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="rh_foto">Foto</label>
                    <input type="file" accept="image/*" name="rh_foto" id="rh_foto">
                </div>
            </div>
            <div class="row view_view">
                <div class="col-md-12">
                    <h3>Laboral</h3>
                </div>
            </div>
            <div class="row view_view">
                <div class="col-md-12">
                    <label for="rhl_tipo">Tipo de empleado</label><input type="text" class="form-control" name="rhl_tipo" id="rhl_tipo">
                </div>
            </div>
            <div class="row view_view">
                <div class="col-md-6">
                    <label for="rhl_sueldo">Sueldo base</label><input type="text" class="form-control" name="rhl_sueldo" id="rhl_sueldo">
                </div>
                <div class="col-md-6">
                    <label for="rhl_bonos">Bonificaciones</label><input type="text" class="form-control" name="rhl_bonos" id="rhl_bonos">
                </div>
            </div>
            <div class="row view_view">
                <div class="col-md-6">
                    <label for="rhl_altaimss">Fecha alta IMSS</label><input type="text" class="form-control hasDatepicker" name="rhl_altaimss" id="rhl_altaimss">
                </div>
                <div class="col-md-6">
                    <label for="rhl_nss">NSS</label><input type="text" class="form-control" name="rhl_nss" id="rhl_nss">
                </div>
            </div>
            <div class="row view_view">
                <div class="col-md-6">
                    <label for="rhl_conini">Fecha de inicio de contrato</label><input type="date" class="form-control hasDatepicker" name="rhl_conini" id="rhl_conini">
                </div>
                <div class="col-md-6">
                    <label for="rhl_confin">Fecha de termino de contrato</label><input type="date" class="form-control hasDatepicker" name="rhl_confin" id="rhl_confin">
                </div>
            </div>
            <div class="row view_view">
                <div class="col-md-12">
                    <label for="rhl_horario">Horario</label><input type="text" class="form-control" name="rhl_horario" id="rhl_horario">
                </div>
            </div>
            <div class="row view_view">
                <div class="col-md-12">
                    <h3>Bancaria</h3>
                </div>
            </div>
            <div class="row view_view">
                <div class="col-md-6">
                    <label for="rhb_banco">Banco</label><input type="text" class="form-control" name="rhb_banco" id="rhb_banco">
                </div>
                <div class="col-md-6">
                    <label for="rhb_titular">Titular</label><input type="text" class="form-control" name="rhb_titular" id="rhb_titular">
                </div>
            </div>
            <div class="row view_view">
                <div class="col-md-6">
                    <label for="rhb_cuenta">No. Cuenta</label><input type="text" class="form-control" name="rhb_cuenta" id="rhb_cuenta">
                </div>
                <div class="col-md-6">
                    <label for="rhb_plastico">No. Plastico</label><input type="text" class="form-control" name="rhb_plastico" id="rhb_plastico">
                </div>
            </div>
            <div class="row view_view">
                <div class="col-md-6">
                    <label for="rhb_clabe">No. Clabe</label><input type="text" class="form-control" name="rhb_clabe" id="rhb_clabe">
                </div>
                <div class="col-md-6">
                    
                </div>
            </div>
           


            
            
            
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary saveform" onclick="saveform()">Actualizar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>