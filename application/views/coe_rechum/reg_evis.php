<style type="text/css">
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
</style>  
<input type="hidden" id="sucursal" value="<?php echo $codigo?>">          
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">DOCUMENTOS IMPORTANTES</div>
                            <p class="content-sub-header mb-1"><?php echo $resultsuc->suc_nombre?> Recursos humanos</p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                        <div class="box-tools pull-right">
                                            <?php if($viewsuc==1){ ?>
                                            <a href="<?php echo base_url();?>Suc_rechum" type="button" class="btn btn-sm btn-light mr-1 mb-1"><i class="fa fa-arrow-left" style="color:black;"></i></a>
                                        <?php }else{ ?>
                                            <a href="<?php echo base_url();?>Coe_rechum" type="button" class="btn btn-sm btn-light mr-1 mb-1"><i class="fa fa-arrow-left" style="color:black;"></i></a>
                                        <?php } ?>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 table-bordered table-striped thead-light tablevc" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>Foto</th>
                                                                    <th>Nombre</th>
                                                                    <th>A. Paterno  </th>
                                                                    <th>A. Materno  </th>
                                                                    <th>Domicilio</th>
                                                                    <th>Contacto</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php 
                                                                    $url_img=FCPATH.'_files/_coe/rechum/'.$resulrh->rh_foto;
                                                                    $url_img1=base_url().'_files/_coe/rechum/'.$resulrh->rh_foto;
                                                                    if(file_exists($url_img)){
                                                                        $file_img='<img src="'.$url_img1.'" style="width:100px">';
                                                                    }else{
                                                                        $file_img='<div class="file_user_default" data-info="'.$url_img.'"></div>';
                                                                    }
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $file_img; ?></td>
                                                                    <td><?php echo $resulrh->rh_nom; ?></td>
                                                                    <td><?php echo $resulrh->rh_pat; ?></td>
                                                                    <td><?php echo $resulrh->rh_mat; ?></td>
                                                                    <td><?php echo $resulrh->rh_domicilio; ?></td>
                                                                    <td><?php echo $resulrh->rh_contacto; ?></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Evidencias</h4>
                                        <div class="box-tools pull-right">
                                            <a onclick="editar(0)" type="button" class="btn btn-success mr-1 mb-1">Agregar evidencia</a>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 table-bordered table-striped thead-light tablevc" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Nombre</th>
                                                                    <th>Archivo</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php $idrow=1; 
                                                                    foreach ($list_evidemcias->result() as $item) { ?>
                                                                    <tr>
                                                                        <td><?php echo $idrow; ?></td>
                                                                        <td><?php echo $item->evi_nombre; ?></td>
                                                                        <td><a href="<?php echo base_url().'_files/_coe/rechum/'.$item->evi_archivo;?>" target="_blank">Ver archivo</a></td>
                                                                        <td>
                                                                            <a onclick="editar(<?php echo $item->rid;?>)" 
                                                                                    type="button" class="btn btn-sm btn-success mr-1 mb-1 edit_form_<?php echo $item->rid;?>"
                                                                                    data-evi_archivo="<?php echo $item->evi_archivo;?>"
                                                                                    data-evi_nombre="<?php echo $item->evi_nombre;?>"
                                                                                ><i class="fa fa-pencil fa-fw"></i></a>
                                                                                
                                                                                <a type="button" onclick="deleterh(<?php echo $item->rid;?>)" class="btn btn-sm btn-danger mr-1 mb-1"><i class="fa fa-trash fa-fw"></i></a>
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                <?php $idrow++;} ?>
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->
<div id="modalform" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_datos">
            <input type="hidden" id="rid" name="rid" value="0">
            <input type="hidden" id="rrid" name="rrid" value="<?php echo $rrid;?>">
            <div class="row">
                <div class="col-md-12">
                    <label>Nombre</label>
                    <input type="text" class="form-control" placeholder="Nombre de la evidencia" name="evi_nombre" id="evi_nombre" required>            
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>Archivo</label>
                    <input type="file" class="form-control"  name="evi_archivo" id="evi_archivo" >            
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary saveform" onclick="saveform()">Actualizar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>