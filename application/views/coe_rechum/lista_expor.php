<?php
header('Content-type: application/excel');
$filename = 'RecursosHumanos_'.date('Y-m-d').'.xls';
header('Content-Disposition: attachment; filename='.$filename);
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>app-assets/css/bootstrap.min.css">

<table class="table m-0 table-striped table-bordered tablevc" id="table_list">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Estación</th>
            <th>Foto</th>
            <th>Nombre</th>
            <th>Domicilio</th>
            <th>Contacto</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $idrow=1; 
            foreach ($list->result() as $item) { 
                $url_img=FCPATH.'_files/_coe/rechum/'.$item->rh_foto;
                $url_img1=base_url().'_files/_coe/rechum/'.$item->rh_foto;
                if(file_exists($url_img)){
                    $file_img='<img src="'.$url_img1.'" style="width:100px">';
                }else{
                    $file_img='<div class="file_user_default" data-info="'.$url_img.'"></div>';
                }
                ?>
            <tr>
                <td><?php echo $idrow;?></td>
                <td><?php echo $item->suc_nombre;?></td>
                <td><?php echo $file_img;?></td>
                <td><?php echo $item->rh_nom.' '.$item->rh_pat.' '.$item->rh_mat;?></td>
                <td><?php echo $item->rh_domicilio;?></td>
                <td><?php echo $item->rh_contacto;?></td>
                <td>Acciones</td>
            </tr>
        <?php $idrow++;} ?>
        
    </tbody>
</table>