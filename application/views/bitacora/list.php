<style type="text/css">
    #table_list th,#table_list td{
        font-size: 12px;
    }
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
</style>            
<!-- BEGIN : Main Content-->
<div class="main-content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="content-header">Bitacora</div>
            </div>
        </div>
        <!--Basic Table Starts-->
        <section id="simple-table">
            <div class="row">
                <div class="col-12" style="padding:0px;">
                    <div class="card">
                        <div class="card-header">
                            <!--<h4 class="card-title">Default Table</h4>-->
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <!--------------->
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Sucursal</label>
                                            <select id="suc" class="form-control" onchange="loadtable()"><option value="0">Todas</option>
                                                <?php 
                                                    foreach ($resultsuc as $item) {
                                                        echo '<option value="'.$item->sucursal.'">'.$item->suc_nombre.'</option>';
                                                        // code...
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-12 table-responsive">
                                            <table class="table m-0 table-bordered" id="table_list">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Usuario</th>
                                                        <th>Tipo</th>
                                                        <th>Descripción</th>
                                                        <th>Reg</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                <!--------------->
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Basic Table Ends-->
    </div>
</div>
<!-- END : End Main Content-->