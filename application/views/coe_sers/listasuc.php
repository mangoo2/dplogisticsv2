<?php 
    $htmCoe='';
    $coecor=0;
    $coecre=0;
    $coeefe=0;
    $coeg=0;
    $coep=0;
    $coek=0;
    $resultcoe=$this->ModeloCoesers->lista_coe($codigo);
    foreach ($resultcoe->result() as $itemcoe) {
        $ser_guia=$itemcoe->ser_guia;
        
        if($itemcoe->ser_fdp=='corporativo'){
            $coecor++;
        }elseif($itemcoe->ser_fdp=='credito'){
            $coecre += $itemcoe->ser_fdpdes;
        }elseif($itemcoe->ser_fdp=='efectivo'){
            $coeefe += $itemcoe->ser_fdpdes;
        }
        if($itemcoe->ser_guia!=''){
            //$tmp = count(explode(',',$ser_guia));
            //$coeg += $tmp; 
            //unset($tmp);
            $coeg++;
        }
        $ser_unis_l='';
        $ser_unis = explode(',',$itemcoe->ser_unis);
        for($x=0;$x<count($ser_unis);$x++){
            $r_pam_equ=$this->ModeloCatalogos->getselectwheren('pam_equ',array('sucursal'=>$codigo,'equipo'=>$ser_unis[$x]));
            foreach ($r_pam_equ->result() as $itempe) {
                $ser_unis_l.=$itempe->equ_nombre.'<br>';
            }
        }
        $coep+=$itemcoe->ser_piezas;
        $coek+=$itemcoe->ser_kilos;
        $htmCoe.='<tr>
                    <td>'.$itemcoe->ser_fol.'</td>
                    <td>'.$itemcoe->ser_fecini.'</td>
                    <td>'.$itemcoe->ser_vuelo.'</td>
                    <td>'.$itemcoe->ser_aeronave.'</td>
                    <td>'.$ser_guia.'</td>
                    <td>'.$itemcoe->ser_origen.'</td>
                    <td>'.$itemcoe->ser_destino.'</td>
                    <td>'.$itemcoe->ser_piezas.'</td>
                    <td>'.$itemcoe->ser_kilos.'</td>
                    <td>'.$itemcoe->ser_clicorp.'</td>
                    <td>'.$itemcoe->ser_clis.'</td>
                    <td>'.$itemcoe->ser_sers.'</td>
                    <td>'.str_replace(',','<br>',$itemcoe->ser_pers).'</td>
                    <td>'.$ser_unis_l.'</td>
                    <td>'.$itemcoe->ser_fdp.' / '.$itemcoe->ser_fdpdes.'</td>
                    <td>'.$itemcoe->ser_obs.'</td>
                    <td>'.$itemcoe->usuario.'</td>
                    <td>'.$itemcoe->ser_enc.'</td>
                    <td>'.$itemcoe->ser_firs.'</td>
                    <td>'.$itemcoe->ser_fecfin.'<br>'.$itemcoe->ser_finusr.'</td>
                </tr>';
    }
?>
<?php 
    $htmCad='';
    $cadg=0;
    $cadp=0;
    $cadk=0;
    $cadc=0;
    $cadcor=0;
    $cadcre=0;
    $cadefe=0;
    $resultcad=$this->ModeloCoesers->lista_cad($codigo);
    foreach ($resultcad->result() as $itemcad) {
        //$htmCad.=jTableTR([$ser_fol,fechaHoraImp($ser_fecini),$ser_origen,$ser_destino,$ser_guia,$ser_piezas,$ser_kilos,$ser_fac,$ser_fri,$ser_cajas,$ser_clis,$ser_sers,$ser_pers,$ser_unis,$ser_fdp.' / '.$ser_fdpdes,$ser_obs,$usuario,$ser_enc,$ser_firs,fechaHoraImp($ser_fecfin).'<br />'.$ser_finusr]);

        //=============================
            $ser_unis_l='';
            $ser_unis = explode(',',$itemcad->ser_unis);
            for($x=0;$x<count($ser_unis);$x++){
                $r_pam_equ=$this->ModeloCatalogos->getselectwheren('pam_equ',array('sucursal'=>$codigo,'equipo'=>$ser_unis[$x]));
                foreach ($r_pam_equ->result() as $itempe) {
                    $ser_unis_l.=$itempe->equ_nombre.'<br>';
                }
            }
        //=============================
            if($itemcad->ser_guia!=''){
                //$tmp = count(explode(',',$ser_guia));
                //$cadg += $tmp; unset($tmp);
                $cadg++;
            }
            $cadp+=$itemcad->ser_piezas;
            $cadk+=$itemcad->ser_kilos;
            $cadc+=$itemcad->ser_cajas;

            if($itemcad->ser_fdp=='corporativo'){
                $cadcor++;
            }elseif($itemcad->ser_fdp=='credito'){
                $cadcre += $itemcad->ser_fdpdes;
            }elseif($itemcad->ser_fdp=='efectivo'){
                $cadefe += $itemcad->ser_fdpdes;
            }

        $htmCad.='<tr>
                    <td>'.$itemcad->ser_fol.'</td>
                    <td>'.$itemcad->ser_fecini.'</td>
                    <td>'.$itemcad->ser_origen.'</td>
                    <td>'.$itemcad->ser_destino.'</td>
                    <td>'.$itemcad->ser_guia.'</td>
                    <td>'.$itemcad->ser_piezas.'</td>
                    <td>'.$itemcad->ser_kilos.'</td>
                    <td>'.$itemcad->ser_fac.'</td>
                    <td>'.$itemcad->ser_fri.'</td>
                    <td>'.$itemcad->ser_cajas.'</td>
                    <td>'.$itemcad->ser_clis.'</td>
                    <td>'.$itemcad->ser_sers.'</td>
                    <td>'.$itemcad->ser_pers.'</td>
                    <td>'.$ser_unis_l.'</td>
                    <td>'.$itemcad->ser_fdp.' / '.$itemcad->ser_fdpdes.'</td>
                    <td>'.$itemcad->ser_obs.'</td>
                    <td>'.$itemcad->usuario.'</td>
                    <td>'.$itemcad->ser_enc.'</td>
                    <td>'.$itemcad->ser_firs.'</td>
                    <td>'.$itemcad->ser_fecfin.'<br>'.$itemcad->ser_finusr.'</td>
                </tr>';
    }
?>
<style type="text/css">
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
</style>            
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header"><?php echo $resultsuc->suc_nombre?></div>
                            <!--<p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>-->
                            <p class="content-sub-header mb-1">Servicios</p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h4>Últimos servicios RAMPA</h4>
                                                    </div>
                                                    <div class="col-md-8" style="text-align:end;">
                                                        <a href="<?php echo base_url();?>Coe_sers/coe_cns/<?php echo $codigo;?>" type="button" class="btn btn-success mr-1 mb-1">Buscar..</a>  
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <table class="table table-bordered tablevc" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>FOLIO</th>
                                                                    <th>FECHA INICIO</th>
                                                                    <th>VUELO</th>
                                                                    <th>AERONAVE</th>
                                                                    <th>NO. GUIA</th>
                                                                    <th>ORIGEN</th>
                                                                    <th>DESTINO</th>
                                                                    <th>NO. PIEZAS</th>
                                                                    <th>KG</th>
                                                                    <th>CLIENTE CORPORATIVO</th>
                                                                    <th>EMPRESA</th>
                                                                    <th style="min-width: 218px;">SERVICIO ESPECÍFICO</th>
                                                                    <th>PERSONAL QUE PRESTA EL SERVICIO</th>
                                                                    <th>UNIDADES O EQUIPO</th>
                                                                    <th>TIPO DE COBRO</th>
                                                                    <th>OBSERVACIONES</th>
                                                                    <th>ELABORA</th>
                                                                    <th>ENCARGADO DE OPERACIÓN</th>
                                                                    <th>SE RECABA FIRMA DE</th>
                                                                    <th style="min-width: 120px;">FECHA FINALIZACIÓN</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php echo $htmCoe;?>
                                                            </tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <th>Folios <?php echo $resultcoe->num_rows();?></th>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <th>Guias: <?php echo $coeg;?></th>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <th>Piezas: <?php echo $coep;?></th>
                                                                    <th>Kilos: <?php echo $coek;?></th>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <th>Corporativo: <?php echo $coecor;?><br>Crédito: <?php echo $coecre;?><br>Efectivo: <?php echo $coeefe;?></th>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td ></td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <hr>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h4>Últimos servicios RyE</h4>
                                                    </div>
                                                    <div class="col-md-8" style="text-align:end;">
                                                        <a href="<?php echo base_url();?>Coe_sers/cad_cns/<?php echo $codigo;?>" type="button" class="btn btn-success mr-1 mb-1">Buscar..</a>  
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table class="table table-bordered tablevc" id="table_list2">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>FOLIO</th>
                                                                    <th>FECHA INICIO</th>
                                                                    <th>ORIGEN</th>
                                                                    <th>DESTINO</th>
                                                                    <th>NO. GUIA</th>
                                                                    <th>NO. PIEZAS</th>
                                                                    <th>KG</th>
                                                                    <th>FACTURA</th>
                                                                    <th>FOLIO REMISION IMR</th>
                                                                    <th>NO. CAJAS</th>
                                                                    <th style="min-width: 100px;">EMPRESA</th>
                                                                    <th style="min-width: 218px;">SERCIVIO ESPECÍFICO</th>
                                                                    <th>PERSONAL QUE PRESTA EL SERVICIO</th>
                                                                    <th>UNIDADES O EQUIPO</th>
                                                                    <th>TIPO DE COBRO</th>
                                                                    <th>OBSERVACIONES</th>
                                                                    <th>ELABORA</th>
                                                                    <th>ENCARGADO DE OPERACIÓN</th>
                                                                    <th>SE RECABA FIRMA DE</th>
                                                                    <th style="min-width: 120px;">FECHA FINALIZACIÓN</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php echo $htmCad;?>
                                                            </tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <th>Folio: <?php echo $resultcad->num_rows();?></th>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <th>Guias: <?php echo $cadg;?></th>
                                                                    <td>Piezas: <?php echo $cadk;?></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td>Cajas: <?php echo $cadc;?></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td>Corporativo: <?php echo $cadcor;?><br />Credito: <?php echo $cadcre;?><br />Efectivo: <?php echo $cadefe;?></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->