<script type="text/javascript">
	$(document).ready(function($) {
		$("#table_list").DataTable({
	        
	        scrollX:true,
	        scrollCollapse:true,
	        fixedColumns:{leftColumns:4},
	        columnDefs: [
    			{ orderable: false, targets: [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19] }
  			],
  			dom: 'Bfrtip',
        	buttons: [
            	'excel', 'pdf', 'print','pageLength'
        	]
	    });
	    $("#table_list2").DataTable({
	        scrollX:true,
	        scrollCollapse:true,
	        fixedColumns:{leftColumns:4},
	        columnDefs: [
    			{ orderable: false, targets: [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19] }
  			],
  			dom: 'Bfrtip',
        	buttons: [
            	'excel', 'pdf', 'print','pageLength'
        	]
	    });

	    $('.dt-buttons').addClass('btn-group');
  		$('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel, .buttons-page-length').addClass('btn btn-primary mb-2');
	});
	
</script>