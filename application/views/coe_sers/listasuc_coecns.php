<?php 
    if( isset($_GET["rep_emp"]) ){
        $rep_emp=$_GET["rep_emp"];
    }else{
        $rep_emp=0;
    }
    if( isset($_GET["rep_ser"]) ){
        $rep_ser=$_GET["rep_ser"];
    }else{
        $rep_ser=0;
    }
    if( isset($_GET["rep_fecini"]) ){
        $rep_fecini=$_GET["rep_fecini"];
    }else{
        $rep_fecini=date('Y-m-d');
    }
    if( isset($_GET["rep_fecfin"]) ){
        $rep_fecfin=$_GET["rep_fecfin"];
    }else{
        $rep_fecfin=date('Y-m-d');
    }
    $htmCoe='';
    $coecor=0;
    $coecre=0;
    $coeefe=0;
    $coeg=0;
    $coep=0;
    $coek=0;
    $resultcoe=$this->ModeloCoesers->lista_coe_filtros($codigo,$rep_emp,$rep_ser,$rep_fecini,$rep_fecfin);
    foreach ($resultcoe->result() as $itemcoe) {
        $ser_guia=$itemcoe->ser_guia;
        
        if($itemcoe->ser_fdp=='corporativo'){
            $coecor++;
        }elseif($itemcoe->ser_fdp=='credito'){
            $coecre += $itemcoe->ser_fdpdes;
        }elseif($itemcoe->ser_fdp=='efectivo'){
            $coeefe += $itemcoe->ser_fdpdes;
        }
        if($itemcoe->ser_guia!=''){
            //$tmp = count(explode(',',$ser_guia));
            //$coeg += $tmp; 
            //unset($tmp);
            $coeg++;
        }
        $ser_unis_l='';
        $ser_unis = explode(',',$itemcoe->ser_unis);
        for($x=0;$x<count($ser_unis);$x++){
            $r_pam_equ=$this->ModeloCatalogos->getselectwheren('pam_equ',array('sucursal'=>$codigo,'equipo'=>$ser_unis[$x]));
            foreach ($r_pam_equ->result() as $itempe) {
                $ser_unis_l.=$itempe->equ_nombre.'<br>';
            }
        }
        $coep+=$itemcoe->ser_piezas;
        $coek+=$itemcoe->ser_kilos;
        $htmCoe.='<tr>
                    <td>'.$itemcoe->ser_fol.'</td>
                    <td>'.$itemcoe->ser_fecini.'</td>
                    <td>'.$itemcoe->ser_vuelo.'</td>
                    <td>'.$itemcoe->ser_aeronave.'</td>
                    <td>'.$ser_guia.'</td>
                    <td>'.$itemcoe->ser_origen.'</td>
                    <td>'.$itemcoe->ser_destino.'</td>
                    <td>'.$itemcoe->ser_piezas.'</td>
                    <td>'.$itemcoe->ser_kilos.'</td>
                    <td>'.$itemcoe->ser_clicorp.'</td>
                    <td>'.$itemcoe->ser_clis.'</td>
                    <td>'.$itemcoe->ser_sers.'</td>
                    <td>'.str_replace(',','<br>',$itemcoe->ser_pers).'</td>
                    <td>'.$ser_unis_l.'</td>
                    <td>'.$itemcoe->ser_fdp.' / '.$itemcoe->ser_fdpdes.'</td>
                    <td>'.$itemcoe->ser_obs.'</td>
                    <td>'.$itemcoe->usuario.'</td>
                    <td>'.$itemcoe->ser_enc.'</td>
                    <td>'.$itemcoe->ser_firs.'</td>
                    <td>'.$itemcoe->ser_fecfin.'<br>'.$itemcoe->ser_finusr.'</td>
                </tr>';
    }
?>
<style type="text/css">
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
</style>            
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header"><?php echo $resultsuc->suc_nombre?></div>
                            <!--<p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>-->
                            <p class="content-sub-header mb-1">Servicios RAMPA</p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                        <div class="box-tools pull-right">
                                            <a href="<?php echo base_url();?>Coe_sers/sers/<?php echo $codigo;?>" type="button" class="btn btn-sm btn-light mr-1 mb-1"><i class="fa fa-arrow-left" style="color:black;"></i></a>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h4>Fecha de consulta</h4>
                                                    </div>
                                                </div>
                                                <form action="<?php echo base_url();?>Coe_sers/coe_cns/<?php echo $codigo;?>" method="get">
                                                    <div class="row">
                                                    <div class="col-md-2 form-group">
                                                        <label>Empresa</label>
                                                        <select class="form-control" name="rep_emp" id="rep_emp" >
                                                            <option value="0">Todos</option>
                                                            <?php foreach ($resulcli->result() as $item) { ?>
                                                                <option value="<?php echo $item->cli;?>" <?php if($item->cli===$rep_emp){ echo 'selected';}?> ><?php echo $item->cli_nom;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2 form-group">
                                                        <label for="rep_ser">SERVICIO</label>
                                                        <select class="form-control" name="rep_ser" id="rep_ser" >
                                                            <option value="0">Todos</option>
                                                            <?php foreach ($resulser->result() as $item) { ?>
                                                            <option value="<?php echo $item->ser;?>" <?php if($item->ser===$rep_ser){ echo 'selected';}?>><?php echo $item->ser_nom;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2 form-group">
                                                        <label for="rep_fecini">Fecha de inicio</label>
                                                        <input type="date" class="form-control" name="rep_fecini" id="rep_fecini" value="<?php echo $rep_fecini; ?>" />
                                                    </div>
                                                    <div class="col-md-2 form-group">
                                                        <label for="rep_fecfin">Fecha de termino</label>
                                                        <input type="date" class="form-control" name="rep_fecfin" id="rep_fecfin" value="<?php echo $rep_fecfin; ?>" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button  type="submit" class="btn btn-success mr-1 mb-1">Buscar..</button> 
                                                    </div>

                                                    </div>
                                                </form>
                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <table class="table table-bordered tablevc" id="table_list">
                                                            <thead>
                                                                <tr>
                                                                    <th>FOLIO</th>
                                                                    <th>FECHA INICIO</th>
                                                                    <th>VUELO</th>
                                                                    <th>AERONAVE</th>
                                                                    <th>NO. GUIA</th>
                                                                    <th>ORIGEN</th>
                                                                    <th>DESTINO</th>
                                                                    <th>NO. PIEZAS</th>
                                                                    <th>KG</th>
                                                                    <th>CLIENTE CORPORATIVO</th>
                                                                    <th>EMPRESA</th>
                                                                    <th style="min-width: 218px;">SERVICIO ESPECÍFICO</th>
                                                                    <th>PERSONAL QUE PRESTA EL SERVICIO</th>
                                                                    <th>UNIDADES O EQUIPO</th>
                                                                    <th>TIPO DE COBRO</th>
                                                                    <th>OBSERVACIONES</th>
                                                                    <th>ELABORA</th>
                                                                    <th>ENCARGADO DE OPERACIÓN</th>
                                                                    <th>SE RECABA FIRMA DE</th>
                                                                    <th style="min-width: 120px;">FECHA FINALIZACIÓN</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php echo $htmCoe;?>
                                                            </tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <th>Folios <?php echo $resultcoe->num_rows();?></th>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <th>Guias: <?php echo $coeg;?></th>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <th>Piezas: <?php echo $coep;?></th>
                                                                    <th>Kilos: <?php echo $coek;?></th>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <th>Corporativo: <?php echo $coecor;?><br>Crédito: <?php echo $coecre;?><br>Efectivo: <?php echo $coeefe;?></th>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td ></td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                                
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->