<!DOCTYPE html>
<html class="loading" lang="en">
<!-- BEGIN : Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="DPLogistics">
    <meta name="keywords" content="DPLogistics">
    <meta name="author" content="PIXINVENT">
    <title>PAQUETERÍA Y LOGÍSTICA AEROPORTUARIO</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>public/img/avion.png">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>public/img/avion.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900%7CMontserrat:300,400,500,600,700,800,900" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>app-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>app-assets/fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>app-assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>app-assets/vendors/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>app-assets/vendors/css/prism.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>app-assets/vendors/css/switchery.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN APEX CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>app-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>app-assets/css/themes/layout-dark.css?v=2">
    <link rel="stylesheet" href="<?php echo base_url();?>app-assets/css/plugins/switchery.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>app-assets/vendors/confirm/jquery-confirm.min.css?v=2">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>app-assets/vendors/css/toastr.css">
    <!-- END APEX CSS-->
    <!-- BEGIN Page Level CSS-->
    <!-- END Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
    <!-- END: Custom CSS-->
</head>
<!-- END : Head-->

<!-- BEGIN : Body-->
<style type="text/css">
    body {
        font-family: Galano, Helvetica, Arial, sans-serif !important;
    }
    .text_red{
        color: red;
    }
</style>

<body class="vertical-layout vertical-menu 2-columns chat-application navbar-sticky" data-menu="vertical-menu" data-col="2-columns">
    
    <?php 
        $demo=1;//0 demo, 1 productivo
        if($demo==0){ ?>
            <style type="text/css">
                .demosis{
                    background-color: #F55252;
                    height: 21px;
                    width: 86%;
                    border-radius: 6px;
                    text-align: center;
                    color: white;
                }
            </style>
            <div class="demosis">Ambiente de pruebas</div>
    <?php } ?>
    <nav class="navbar navbar-expand-lg navbar-light header-navbar navbar-static">
        <div class="container-fluid navbar-wrapper">
            <div class="navbar-header d-flex">
                <div class="navbar-toggle menu-toggle d-xl-none d-block float-left align-items-center justify-content-center" data-toggle="collapse"><i class="ft-menu font-medium-3"></i></div>
                <ul class="navbar-nav">
                    <li class="nav-item mr-2 d-none d-lg-block"><a class="nav-link apptogglefullscreen" id="navbar-fullscreen" href="javascript:;"><i class="ft-maximize font-medium-3"></i></a></li>
                    <!--<li class="nav-item nav-search"><a class="nav-link nav-link-search" href="javascript:"><i class="ft-search font-medium-3"></i></a>
                        <div class="search-input">
                            <div class="search-input-icon"><i class="ft-search font-medium-3"></i></div>
                            <input class="input" type="text" placeholder="Explore Apex..." tabindex="0" data-search="template-search">
                            <div class="search-input-close"><i class="ft-x font-medium-3"></i></div>
                            <ul class="search-list"></ul>
                        </div>
                    </li>-->
                </ul>
            </div>
            <div class="navbar-container">
                <?php
                    $num_notificacion=0; 
                    $RANGO=$this->session->userdata('sess_rango_nda');
                    $sess_usr=$this->sess_usr=$this->session->userdata('sess_usr');
                    if ($RANGO=='ttl' || $RANGO=='adm' || $sess_usr=='monserratsanchez' || $sess_usr=='guadalupemora'){
                        $solcoti=$this->ModeloCatalogos->getselectwheren('doc_cotiza',array('sol_editar'=>1));
                        $num_notificacion=$num_notificacion+$solcoti->num_rows();
                        $solcotidelete=$this->ModeloCatalogos->solcotidelete();
                        $num_notificacion=$num_notificacion+$solcotidelete->num_rows();
                    }
                ?>
                <div class="collapse navbar-collapse d-block" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        
                        <li class="dropdown nav-item"><a class="nav-link dropdown-toggle dropdown-notification p-0 mt-2" id="dropdownBasic1" href="javascript:;" data-toggle="dropdown"><i class="ft-bell font-medium-3"></i><span class="notification badge badge-pill badge-danger"><?php echo $num_notificacion;?></span></a>
                            <ul class="notification-dropdown dropdown-menu dropdown-menu-media dropdown-menu-right m-0 overflow-hidden">
                                <li class="dropdown-menu-header">
                                    <div class="dropdown-header d-flex justify-content-between m-0 px-3 py-2 white bg-primary">
                                        <div class="d-flex"><i class="ft-bell font-medium-3 d-flex align-items-center mr-2"></i><span class="noti-title"><?php echo $num_notificacion;?> New Notification</span></div><span class="text-bold-400 cursor-pointer">Mark all as read</span>
                                    </div>
                                </li>
                                <li class="scrollable-container">
                                    <?php if ($RANGO=='ttl' || $RANGO=='adm' || $sess_usr=='monserratsanchez' || $sess_usr=='guadalupemora'){ ?>
                                        <?php foreach ($solcoti->result() as $item) { ?>                                    
                                            <a class="d-flex justify-content-between edit_cotizacion_<?php echo $item->rid; ?>" data-cotizacion="<?php echo $item->cotiza; ?>" data-suc="<?php echo $item->sucursal; ?>" onclick="aceptacionsolicitud(<?php echo $item->rid; ?>,'<?php echo $item->cotiza; ?>','<?php echo $item->sucursal; ?>')">
                                                <div class="media d-flex align-items-center">
                                                    <div class="media-left">
                                                        <div class="media-left">
                                                            <div class="avatar bg-primary bg-lighten-3 mr-3 p-1"><span class="avatar-content font-medium-2">SE</span></div>
                                                        </div>
                                                    </div>
                                                    <div class="media-body">
                                                        <h6 class="m-0">
                                                            <span>Se solicito la edicion de cotizacion</span>
                                                        </h6>
                                                        <small class="noti-text"><b><?php echo $item->rid; ?></b>(<?php echo $item->cotiza; ?>)</small>
                                                    </div>
                                                </div>
                                            </a>
                                        <?php } ?>
                                        <?php foreach ($solcotidelete->result() as $item) { ?>                                    
                                            <a class="d-flex justify-content-between edit_cotizacion_<?php echo $item->rid; ?>" data-cotizacion="<?php echo $item->cotiza; ?>" data-suc="<?php echo $item->sucursal; ?>" data-motivo="<?php echo $item->motivo; ?>" data-descrip="<?php echo $item->descrip_can; ?>" onclick="deletecotsoladm(<?php echo $item->rid; ?>,'<?php echo $item->cotiza; ?>','<?php echo $item->sucursal; ?>')">
                                                <div class="media d-flex align-items-center">
                                                    <div class="media-left">
                                                        <div class="media-left">
                                                            <div class="avatar bg-primary bg-lighten-3 mr-3 p-1"><span class="avatar-content font-medium-2">SE</span></div>
                                                        </div>
                                                    </div>
                                                    <div class="media-body">
                                                        <h6 class="m-0">
                                                            <span>Se solicito la eliminacion de documentacion</span>
                                                        </h6>
                                                        <small class="noti-text"><b><?php echo $item->rid; ?></b>(<?php echo $item->cotiza; ?>)</small>
                                                    </div>
                                                </div>
                                            </a>
                                        <?php } ?>
                                    <?php } ?>
                                    


                                    
                                </li>
                                <li class="dropdown-menu-footer">
                                    <div class="noti-footer text-center cursor-pointer primary border-top text-bold-400 py-1">Read All Notifications</div>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown nav-item mr-1"><a class="nav-link dropdown-toggle user-dropdown d-flex align-items-end" id="dropdownBasic2" href="javascript:;" data-toggle="dropdown">
                                <div class="user d-md-flex d-none mr-2"><span class="text-right"><?php echo $this->session->userdata('sess_nom');?></span><span class="text-right text-muted font-small-3"><br></span></div>
                                <?php if($this->session->userdata('foto').'asdsadas'!=''){ ?>
                                    <img class="avatar" src="<?php echo base_url();?>_files/usrs/<?php echo $this->session->userdata('foto') ?>" alt="avatar" height="35" width="35">
                                <?php }else{ ?>    
                                    <img class="avatar" src="<?php echo base_url();?>app-assets/img/portrait/small/avatar-s-1.png" alt="avatar" height="35" width="35">
                                <?php } ?>    
                            </a>
                            <div class="dropdown-menu text-left dropdown-menu-right m-0 pb-0" aria-labelledby="dropdownBasic2">
                                <!--<a class="dropdown-item" href="app-chat.html">
                                    <div class="d-flex align-items-center"><i class="ft-message-square mr-2"></i><span>Chat</span></div>
                                </a><a class="dropdown-item" href="page-user-profile.html">
                                    <div class="d-flex align-items-center"><i class="ft-edit mr-2"></i><span>Edit Profile</span></div>
                                </a><a class="dropdown-item" href="app-email.html">
                                    <div class="d-flex align-items-center"><i class="ft-mail mr-2"></i><span>My Inbox</span></div>
                                </a>-->
                                <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="<?php echo base_url();?>Login/cerrar_sesion">
                                     <div class="d-flex align-items-center"><i class="ft-power mr-2"></i><span>Logout</span></div>
                                    </a>
                            </div>
                        </li>
                        <!--<li class="nav-item d-none d-lg-block mr-2 mt-1"><a class="nav-link notification-sidebar-toggle" href="javascript:;"><i class="ft-align-right font-medium-3"></i></a></li>-->
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <style type="text/css">
        .loading-shown{
            z-index: 99999 !important;
        }
    </style>
    
    <!-- Navbar (Header) Ends-->
    <input type="hidden" id="base_url" value="<?php echo base_url();?>">