<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/datatables/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/js/datatable/responsive.dataTables.min.css">

<!-- BEGIN PAGE VENDOR JS-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/js/datatable/buttons.dataTables.min.css">

<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/vfs_fonts.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/js/datatable/fixedColumns.dataTables.min.css">
<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/dataTables.fixedColumns.min.js"></script>

<!-- END PAGE VENDOR JS-->
<style type="text/css">
	.buttonsearch{
    	position: absolute;
    	cursor: pointer;
    	font-size: 20px;
    	top: 5px;
    	right: 1px;
    	color: #b3adad;
	}
	.dataTable td{
		padding-top: 10px;
		padding-bottom: 10px;
		padding-right: 10px;
		font-size: 12px;
	}
	.dataTable th{
		
		font-size: 12px;
	}
</style>