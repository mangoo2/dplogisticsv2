    <?php 
        $RANGO=$this->session->userdata('sess_rango_nda');
    ?>
    <!--
        C:\xampp\htdocs\dplogistics\estilo\JSIEFuncs.php aqui se establecen los permisos
    -->
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <div class="wrapper">


        <!-- main menu-->
        <!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
        <div class="app-sidebar menu-fixed" data-background-color="man-of-steel" data-image="<?php echo base_url();?>public/img/cadipo.jpg" data-scroll-to-active="true">
            <!-- main menu header-->
            <!-- Sidebar Header starts-->
            <div class="sidebar-header">
                <div class="logo clearfix"><a class="logo-text float-left" href="<?php echo base_url();?>Sistema">
                    <div class="logo_menu">
                        <div class="logo-img"><img style="width: 212px;" src="<?php echo base_url();?>public/img/34/LOGCOECSA.webp" alt="Apex Logo" /></div><span class="text"></span>
                    </div>
                    </a><a class="nav-toggle d-none d-lg-none d-xl-block" id="sidebarToggle" href="javascript:;" onclick="img_logo()"><i class="toggle-icon ft-toggle-right" data-toggle="expanded"></i></a><a class="nav-close d-block d-lg-block d-xl-none" id="sidebarClose" href="javascript:;"><i class="ft-x"></i></a></div>
            </div>
            <!-- Sidebar Header Ends-->
            <!-- / main menu header-->
            <!-- main menu content-->
            <div class="sidebar-content main-menu-content">
                <div class="nav-container">
                    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                        <li class=" nav-item"><a href="<?php echo base_url();?>Inicio"><i class="ft-mail"></i><span class="menu-title">Inicio</span></a>
                        <?php if ($this->ModeloPermisos->SECCION('panel')){ ?>
                            <li class=" nav-item">
                                <a href="<?php echo base_url();?>Panel">
                                    <i class="ft-mail"></i><span class="menu-title">Panel</span>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if ($this->ModeloPermisos->SECCION('documentar')){ ?>
                            <li class=" nav-item">
                                <a href="<?php echo base_url();?>Documentar">
                                    <i class="ft-mail"></i><span class="menu-title">Documentar</span>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if ($this->ModeloPermisos->SECCION('ser_rampa')){ ?>
                            <li class=" nav-item">
                                <a href="<?php echo base_url();?>Servicios_RAMPA">
                                    <i class="ft-mail"></i><span class="menu-title">Servicios RAMPA</span>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if ($this->ModeloPermisos->SECCION('ser_rye')){ ?>
                            <li class=" nav-item">
                                <a href="<?php echo base_url();?>Servicios_Rye">
                                    <i class="ft-mail"></i><span class="menu-title">Servicios Rye</span>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if ($this->ModeloPermisos->SECCION('clientes')){ ?>
                            <li class=" nav-item">
                                <a href="<?php echo base_url();?>Clientes">
                                    <i class="ft-users"></i><span class="menu-title">Clientes</span>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if ($this->ModeloPermisos->SECCION('ope_pam') || $RANGO=='ttl' || $RANGO=='adm'){ ?>
                            <li class=" nav-item">
                                <a href="<?php echo base_url();?>Ope_pam">
                                    <i class="ft-mail"></i><span class="menu-title">PAM</span>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if ($this->ModeloPermisos->SECCION('ope_brdu') || $RANGO=='ttl' || $RANGO=='adm'){ ?>
                            <li class=" nav-item">
                                <a href="<?php echo base_url();?>BRDU">
                                    <i class="ft-mail"></i><span class="menu-title">BRDU</span>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if($this->ModeloPermisos->SECCION('coe_facts') || $RANGO=='ttl' || $RANGO=='adm'){ ?>
                        <li class=" nav-item"><a href="<?php echo base_url();?>Facturas"><i class="ft-mail"></i><span class="menu-title">Facturas</span></a>
                        <li class=" nav-item"><a href="<?php echo base_url();?>ComplementosPago"><i class="ft-mail"></i><span class="menu-title">Complementos de Pago</span></a>
                        <?php } ?>
                        <?php if ($RANGO=='ttl' || $RANGO=='adm'){ ?>
                            <li class="treeview">
                                <a href="#"><i class="fa fa-folder"></i> <span>ADMINISTRADOR</span> <span class="pull-right-container"></span></a>
                                <ul class="treeview-menu">
                                    <li><a href="<?php echo base_url();?>Adm_usrs" title="Usuarios"><i class="fa fa-users"></i> Usuarios</a></li>
                                    <li><a href="<?php echo base_url();?>Bitacora" title="Bitacora"><i class="fa fa-users"></i> Bitacora</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                        <?php } ?>

                        <li class="has-sub nav-item">
                            <a href="javascript:;">
                                <i class="ft-home"></i>
                                <span class="menu-title" >ADMINISTRACIÓN</span>
                            </a>
                            <ul class="menu-content">
                                <?php if ($this->ModeloPermisos->SECCION('coe_guias')){ ?>
                                    <li>
                                        <a href="<?php echo base_url();?>Coe_guias">
                                            <i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Guiás</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('coe_sers')){ ?>
                                    <li>
                                        <a href="<?php echo base_url();?>Coe_sers">
                                            <i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Servicios</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('coe_cuenta')){ ?>
                                    <li>
                                        <a href="<?php echo base_url();?>Coe_cuenta">
                                            <i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Cuenta de estación</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('coe_facts')){ ?>
                                    <li>
                                        <a href="<?php echo base_url();?>#">
                                            <i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Facturas</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('coe_creds')){ ?>
                                    <li>
                                        <a href="<?php echo base_url();?>Coe_creds">
                                            <i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Créditos</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('coe_clientes')){ ?>
                                    <li>
                                        <a href="<?php echo base_url();?>Coe_clientes">
                                            <i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Clientes</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('coe_pyg')){ ?>
                                    <li>
                                        <a href="<?php echo base_url();?>Coe_pyg">
                                            <i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Presupuestos y gastos</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('coe_reqs')){ ?>
                                    <li>
                                        <a href="<?php echo base_url();?>Coe_reqs">
                                            <i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Requisiciones</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('coe_rechum')){ ?>
                                    <li>
                                        <a href="<?php echo base_url();?>Coe_rechum">
                                            <i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Recursos Humanos</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('coe_pam')){ ?>
                                    <li>
                                        <a href="<?php echo base_url();?>Coe_pam">
                                            <i class="ft-arrow-right submenu-icon"></i><span class="menu-item">PAM</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('coe_brdu')){ ?>
                                    <li>
                                        <a href="<?php echo base_url();?>Coe_brdu">
                                            <i class="ft-arrow-right submenu-icon"></i><span class="menu-item">BRDU</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                
                            </ul>
                        </li>

                        <li class="has-sub nav-item">
                            <a href="javascript:;">
                                <i class="ft-home"></i>
                                <span class="menu-title" >ESTACIONES</span>
                            </a>
                            <ul class="menu-content">
                                <?php if ($this->ModeloPermisos->SECCION('suc_cors')){ ?>
                                <li><a href="<?php echo base_url();?>Corte_caja"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Corte de caja</span></a></li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('suc_guias')){ ?>
                                <li><a onclick="cargaloginpage('<?php echo base_url();?>Suc_guias')"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Guiás</span></a></li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('suc_serrampa')){ ?>
                                <li><a href="<?php echo base_url();?>#"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Servicios RAMPA</span></a></li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('suc_serrye')){ ?>
                                <li><a href="<?php echo base_url();?>#"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Servicios RyE</span></a></li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('suc_cuenta')){ ?>
                                <li><a href="<?php echo base_url();?>Suc_cuenta"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Cuenta de estación</span></a></li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('suc_clientes')){ ?>
                                <li><a href="<?php echo base_url();?>#"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Clientes</span></a></li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('suc_pyg')){ ?>
                                <li><a href="<?php echo base_url();?>#"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Presupuestos y gastos</span></a></li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('suc_usogas')){ ?>
                                <li><a href="<?php echo base_url();?>#"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Uso de combustible</span></a></li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('suc_eyv')){ ?>
                                <li><a href="<?php echo base_url();?>#"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Entregas y viajes</span></a></li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('suc_usotel')){ ?>
                                <li><a href="<?php echo base_url();?>#"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Uso Telefónico</span></a></li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('suc_reqs')){ ?>
                                <li><a href="<?php echo base_url();?>Suc_reqs"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Requisiciones</span></a></li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('suc_rechum')){ ?>
                                <li><a href="<?php echo base_url();?>Suc_rechum"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Recursos humanos</span></a></li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('suc_pam')){ ?>
                                <li><a href="<?php echo base_url();?>Suc_pam"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">PAM</span></a></li>
                                <?php } ?>
                                <?php if ($this->ModeloPermisos->SECCION('suc_brdu')){ ?>
                                <li><a href="<?php echo base_url();?>Coe_brdu/suc"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">BRDU</span></a></li>
                                <?php } ?>
                                
                            </ul>
                        </li>
                        <?php if($RANGO=='ttl' || $RANGO=='adm'){ ?>
                        <li class="has-sub nav-item">
                            <a href="javascript:;">
                                <i class="ft-home"></i>
                                <span class="menu-title" >CONFIG</span>
                            </a>
                            <ul class="menu-content">

                                <li><a href="<?php echo base_url();?>Conf_emps"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Empresas</span></a></li>
                                <li><a href="<?php echo base_url();?>Conf_aers"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Aerolineas</span></a></li>
                                <li><a href="<?php echo base_url();?>Conf_rut"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Rutas</span></a></li>
                                <li><a href="<?php echo base_url();?>Conf_cla"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Tarifas</span></a></li>
                                <li><a href="<?php echo base_url();?>Conf_suc"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Estaciones</span></a></li>
                                <li><a href="<?php echo base_url();?>Conf_sers"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Servicios</span></a></li>
                                <li><a href="<?php echo base_url();?>Conf_pam"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">PAM</span></a></li>
                                <li><a href="<?php echo base_url();?>#"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">BRDU</span></a></li>
                                <li><a href="<?php echo base_url();?>Config/facturacion"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Facturación</span></a></li>
                                <li><a href="<?php echo base_url();?>Vuelos"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Vuelos</span></a></li>
                                <li><a href="<?php echo base_url();?>Operadores"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Operadores</span></a></li>
                                <li><a href="<?php echo base_url();?>Origen"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Origen</span></a></li>
                                <li><a href="<?php echo base_url();?>Domicilio_origen"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Domicilio origen</span></a></li>
                                <li><a href="<?php echo base_url();?>Destino"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Destino</span></a></li>
                                <li><a href="<?php echo base_url();?>Domicilio_destino"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Domicilio destino</span></a></li>
                                <li><a href="<?php echo base_url();?>Transporte_federal"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Transporte federal</span></a></li>
                                <li><a href="<?php echo base_url();?>Estaciones"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Estaciones</span></a></li>
                                <li><a href="<?php echo base_url();?>Config/sucursales"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Sucursales</span></a></li>
                                
                            </ul>
                        </li>
                        <?php } ?>
                        <li class=" nav-item"><a><i class="ft-arrow-right submenu-icon"></i><span class="menu-title">Documentos Estación</span></a></li>

                        <li class="has-sub nav-item">
                            <a href="javascript:;">
                                <i class="ft-home"></i>
                                <span class="menu-title" >Administrativo</span>
                            </a>
                            <ul class="menu-content">
                                <li><a href="<?php echo base_url();?>#"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Circulares Obligatorias</span></a></li>
                                <li><a href="<?php echo base_url();?>#"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Manuales Coecsa</span></a></li>
                                <li><a href="<?php echo base_url();?>#"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">MSAT REV.1</span></a></li>
                                <li><a href="<?php echo base_url();?>#"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">OAX</span></a></li>
                                <li><a href="<?php echo base_url();?>#"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Agregar nuevo</span></a></li>
                                <li><a href="<?php echo base_url();?>Admin/Reportes"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Reportes</span></a></li>
                                <li><a href="<?php echo base_url();?>Admin/Metas"><i class="ft-arrow-right submenu-icon"></i><span class="menu-item">Reportes Metas</span></a></li>
                                
                                
                            </ul>
                        </li>
                        
                    </ul>
                </div>
            </div>
            <!-- main menu content-->
            <div class="sidebar-background"></div>
            <!-- main menu footer-->
            <!-- include includes/menu-footer-->
            <!-- main menu footer-->
            <!-- / main menu-->
        </div>

        <div class="main-panel">
