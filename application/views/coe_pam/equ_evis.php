<?php 
    $resul_evi=$this->ModeloCatalogos->getselectwheren('pam_equevi',array('eid'=>$eid));
    $resultsuc=$this->ModeloCatalogos->getselectwheren('pam_equ',array('rid'=>$eid));
    
    $equ_numeco='';
    $equipo='';
    foreach ($resultsuc->result() as $item) {
        $equ_numeco=$item->equ_numeco;
        $equipo=$item->equipo;
    }
?>
<style type="text/css">
    #table_list th,#table_list td{font-size: 12px;}
    .table-responsive{padding: 0px;}
    .divbuttonstable{width: 135px;}
    .tablevc th,.tablevc td{font-size: 12px;text-align: center;padding: 8px;}
    .a_bmau{text-decoration: none;color: white;}
</style>   
<input type="hidden" id="sucursal" value="<?php echo $sucu;?>"> 
<input type="hidden" id="equipo" value="<?php echo $equipo;?>"> 
<input type="hidden" id="eid" value="<?php echo $eid;?>">         
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header"><?php echo $equ_numeco ?> </div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">PAM </h4>-->
                                        <div class="row">
                                            <div class="col-md-7"><h3 class="card-title">UNIDAD, EQUIPO E INSTALACIÓN</h3></div>
                                            <div class="col-md-5" style="text-align: end;">
                                                <a onclick="javascript:history.back()" type="button" class="btn btn-light btnRegresar"><span class="fa fa-arrow-left"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 table-bordered tablevc" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr><th>No. Económico</th><th>Nombre</th><th>Marca</th><th>Modelo</th><th>Placa / Serie</th><th>Descripción</th><th>Estatus</th></tr>
                                                            </thead>
                                                            <tbody><?php 
                                                                foreach ($resultsuc->result() as $item) {
                                                                    echo '<tr><th>'.$item->equ_numeco.'</th><th>'.$item->equ_nombre.'</th><th>'.$item->equ_marca.'</th><th>'.$item->equ_modelo.'</th><th>'.$item->equ_plaser.'</th><th>'.$item->equ_descrip.'</th><th>'.$item->equ_estatus.'</th></tr>';
                                                                }
                                                            ?></tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row"><div class="col-12"><hr></div></div>
                                                <div class="row">
                                                    <div class="col-md-7"><h3 class="card-title">Evidencias</h3></div>
                                                    <div class="col-md-5" style="text-align: end;">
                                                        <a  type="button" class="btn btn-success" onclick="edit_equ_evis(0)">Agregar evidencia</a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <table class="table m-0 table-bordered tablevc" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr><th>#</th><th>Nombre</th><th>Archivo</th><th>Acciones</th></tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php 
                                                                    $x=0;
                                                                    foreach ($resul_evi->result() as $item) {
                                                                        $x++;
                                                                        $urlfile=base_url().'_files/_suc/'.$sucu.'/pam/equs/'.$equipo.'/'.$item->evi_archivo;
                                                                        $trse='';
                                                                        $trse.='<tr>';
                                                                            $trse.='<td>'.$x.'</td>';
                                                                            $trse.='<td>'.$item->evi_nombre.'</td>';
                                                                            $trse.='<td><a href="'.$urlfile.'" target="_blank">Ver archivo</a></td>';
                                                                            $trse.='<td>';
                                                                                $trse.='<a class="btn btn-info btn-sm edit_pam_u'.$item->rid.'" onclick="edit_equ_evis('.$item->rid.')"
                                                                                        data-evinombre="'.$item->evi_nombre.'"
                                                                                        data-eviarchivo="'.$item->evi_archivo.'"
                                                                                    ><i class="fa fa-edit"></i></a> ';
                                                                                $trse.='<a class="btn btn-danger btn-sm" onclick="delete_pam_u('.$item->rid.')"><i class="fa fa-trash"></i></a> ';
                                                                            $trse.='</td></tr>';
                        
                                                                        echo $trse;
                                                                    }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->


<div class="modal fade text-left" id="mdlEviNue" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="mdlEviNue_Label">Modificar Equipo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                
                <form id="frmNue_ev" class="form frmMod">
                    <input type="hidden" name="rid" id="rid_eve" value="0">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Nombre</label>
                            <input type="text" name="evi_nombre" id="evi_nombre" class="form-control" required>
                        </div>
                        <div class="col-md-12 form-group">
                            <label><b>Archivo: </b></label>
                            <input type="file" name="evi_archivo" id="evi_archivo" class="form-control">
                            <p class="mdlEviNue_info"></p>
                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="savemdlEviNue()">Guardar</button>
            </div>
        </div>
    </div>
</div>