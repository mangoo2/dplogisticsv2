<?php
    $equ_numeco='';
    foreach ($resultpamequ->result() as $item) {
        $equ_numeco = $item->equ_numeco;
    }
    $resul_evi=$this->ModeloCatalogos->getselectwheren('pam_evi',array('bit'=>$bit));
?>
<style type="text/css">
    #table_list th, #table_list td{
        font-size: 12px;
        padding: 10px 10px;
    }

    .overflow_div{
        overflow: auto;
    }
    .titulosform{
        background-color: #e9ecef;
        border-color: #E0E0E0;
        margin-bottom: 10px;
        -webkit-box-shadow: 0px 11px 14px -15px rgba(0,0,0,0.75);
        -moz-box-shadow: 0px 11px 14px -15px rgba(0,0,0,0.75);
        box-shadow: 0px 11px 14px -15px rgba(0,0,0,0.75);
    }
    .titulosform label{
        font-weight: bold;
    }
</style>
<input type="hidden" id="suc" value="<?php echo $suc;?>">
<input type="hidden" id="equ" value="<?php echo $equ;?>">
<input type="hidden" id="pam" value="<?php echo $pam;?>">
<input type="hidden" id="bit" value="<?php echo $bit;?>">
<!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header"> <?php echo $equ_numeco ?></div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">PAM </h4>-->
                                        <div class="row">
                                            <div class="col-md-7">
                                                <h3 class="card-title">BMAU </h3>
                                            </div>
                                            <div class="col-md-5" style="text-align: end;">
                                                
                                                <a onclick="javascript:history.back()" type="button" class="btn btn-light btn-sm btnRegresar"><span class="fa fa-arrow-left"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                
                                                <div class="row">
                                                    <div class="col-md-12 overflow_div">
                                                        <table class="tabla table table-bordered table-hover" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th rowspan="2">Fecha</th>
                                                                    <th rowspan="2">Lectura odómetro</th>
                                                                    <th rowspan="2">Tipo de Servicio</th>
                                                                    <th rowspan="2">Clase de Servicio</th>
                                                                    <th rowspan="2" style="width:250px;">Descripción Clase de Servicio</th>
                                                                    <th>PREVENTIVO</th>
                                                                    <th>CORRECTIVO</th>
                                                                    <th colspan="3">COMPROBANTE SERVICIOS</th>
                                                                    <th>CAMBIO DE PIEZAS O COMPRA DE ADICIONALES</th>
                                                                    <th colspan="2">COMPROBANTE PIEZAS</th>
                                                                    <th rowspan="2" style="width:150px;">Otros Servicios / Observaciones</th>
                                                                    <th rowspan="2" style="width:100px;">Costo Total</th>
                                                                    <th rowspan="2" style="width:150px;">NOMBRE Y FIRMA RESPONSABLE COECSA</th>
                                                                </tr>
                                                                <tr>
                                                                    <th style="width:200px;">DETALLAR EXACTAMENTE LOS SERVICIOS QUE SE LE HICIERON</th>
                                                                    <th style="width:200px;">DETALLAR QUÉ SE LE HIZO</th>
                                                                    <th style="width:100px;">Facturas No. / Notas No.</th>
                                                                    <th style="width:100px;">¿Anexa Carta Responsiva?</th>
                                                                    <th style="width:100px;">¿Anexa INE?</th>
                                                                    <th style="width:250px;">DETALLAR CUÁLES PIEZAS SE CAMBIARON O QUÉ SE COMPRÓ</th>
                                                                    <th style="width:100px;">Facturas No. / Notas No.</th>
                                                                    <th style="width:100px;">¿Anexa fotos?</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody><?php $resE_strq="SELECT * FROM pam_bit WHERE rid='$bit'";
                                                                        $resE = $this->db->query($resE_strq);
                                                                        $trsp='';
                                                                        foreach ($resE->result() as $item) {
                                                                            $btn='';
                                                                            
                                                                            $trsp.='<tr>
                                                                                <td>'.$item->bit_fecha.'</td><td>'.$item->bit_odometro.'</td><td>'.$item->bit_tipo.'</td><td>'.$item->bit_clase.'</td><td>'.$item->bit_clades.'</td>
                                                                                <td>'.$item->bit_preventivo.'</td><td>'.$item->bit_correctivo.'</td><td>'.$item->bit_comfon.'</td><td>'.$item->bit_comcr.'</td><td>'.$item->bit_comine.'</td><td>'.$item->bit_piecoc.'</td>
                                                                                <td>'.$item->bit_piefon.'</td><td>'.$item->bit_piefot.'</td><td>'.$item->bit_oso.'</td><td>'.$item->bit_costo.'</td><td>'.$item->bit_respon.'</td>';
                                                                                
                                                                            $trsp.='</tr>';
                                                                        }
                                                                echo $trsp;
                                                                ?></tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                
                                                
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">PAM </h4>-->
                                        <div class="row">
                                            <div class="col-md-7">
                                                <h3 class="card-title">Evidencias </h3>
                                            </div>
                                            <div class="col-md-5" style="text-align: end;">
                                                <a class="btn btn-success" onclick="edit_equ_evis(0)">Agregar evidencias</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                
                                                <div class="row">
                                                    <div class="col-md-12 ">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <table class="table m-0 table-bordered tablevc" id="table_list">
                                                                    <thead class="thead-light">
                                                                        <tr><th>#</th><th>Nombre</th><th>Archivo</th><th>Acciones</th></tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php 
                                                                            $x=0;
                                                                            foreach ($resul_evi->result() as $item) {
                                                                                $x++;
                                                                                $urlfile=base_url().'_files/_suc/'.$sucu.'/pam/evis/'.$equ.'/'.$item->evi_archivo;
                                                                                $trse='';
                                                                                $trse.='<tr>';
                                                                                    $trse.='<td>'.$x.'</td>';
                                                                                    $trse.='<td>'.$item->evi_nombre.'</td>';
                                                                                    $trse.='<td><a href="'.$urlfile.'" target="_blank">'.$item->evi_archivo.'</td>';
                                                                                    $trse.='<td>';
                                                                                        $trse.='<a class="btn btn-info btn-sm edit_pam_u'.$item->rid.'" onclick="edit_equ_evis('.$item->rid.')"
                                                                                                data-evinombre="'.$item->evi_nombre.'"
                                                                                                data-eviarchivo="'.$item->evi_archivo.'"
                                                                                            ><i class="fa fa-edit"></i></a> ';
                                                                                        $trse.='<a class="btn btn-danger btn-sm" onclick="delete_pam_u('.$item->rid.')"><i class="fa fa-trash"></i></a> ';
                                                                                    $trse.='</td></tr>';
                                
                                                                                echo $trse;
                                                                            }
                                                                        ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>

            <!-- END : End Main Content-->
<div class="modal fade text-left" id="mdlEviNue" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="mdlEviNue_Label">Modificar Equipo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                
                <form id="frmNue_ev" class="form frmMod">
                    <input type="hidden" name="rid" id="rid_eve" value="0">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Nombre</label>
                            <input type="text" name="evi_nombre" id="evi_nombre" class="form-control" required>
                        </div>
                        <div class="col-md-12 form-group">
                            <label><b>Archivo: </b></label>
                            <input type="file" name="evi_archivo" id="evi_archivo" class="form-control">
                            <p class="mdlEviNue_info"></p>
                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="savemdlEviNue()">Guardar</button>
            </div>
        </div>
    </div>
</div>