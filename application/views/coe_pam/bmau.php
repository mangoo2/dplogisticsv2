<?php
    $equ_numeco='';
    foreach ($resultpamequ->result() as $item) {
        $equ_numeco = $item->equ_numeco;
    }
?>
<style type="text/css">
    #table_list th, #table_list td{
        font-size: 12px;
        padding: 10px 10px;
    }

    .overflow_div{
        overflow: auto;
    }
    .titulosform{
        background-color: #e9ecef;
        border-color: #E0E0E0;
        margin-bottom: 10px;
        -webkit-box-shadow: 0px 11px 14px -15px rgba(0,0,0,0.75);
        -moz-box-shadow: 0px 11px 14px -15px rgba(0,0,0,0.75);
        box-shadow: 0px 11px 14px -15px rgba(0,0,0,0.75);
    }
    .titulosform label{
        font-weight: bold;
    }
</style>
<input type="hidden" id="suc" value="<?php echo $suc;?>">
<input type="hidden" id="equ" value="<?php echo $equ;?>">
<input type="hidden" id="pam" value="<?php echo $pam;?>">
<!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header"> <?php echo $equ_numeco ?></div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">PAM </h4>-->
                                        <div class="row">
                                            <div class="col-md-7">
                                                <h3 class="card-title">BMAU <?php echo $cic ?></h3>
                                            </div>
                                            <div class="col-md-5" style="text-align: end;">
                                                <a class="btn btn-success " onclick="add_bit(0)" >Agregar Servicio</a>
                                                <a class="btn btn-secondary btnexportar_pam" href="<?php echo base_url().'Coe_pam/bma_expxls/'.$cic.'/'.$suc.'/'.$equ.'/'.$pam; ?>" target="_blank">Exportar XLS</a>
                                                <a href="<?php echo base_url().'Coe_pam/pam/'.$cic.'/'.$suc;?>" type="button" class="btn btn-light btn-sm btnRegresar"><span class="fa fa-arrow-left"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                
                                                <div class="row">
                                                    <div class="col-md-12 overflow_div">
                                                        <table class="tabla table table-bordered table-hover" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th rowspan="2">No.</th>
                                                                    <th rowspan="2">Fecha</th>
                                                                    <th rowspan="2">Lectura odómetro</th>
                                                                    <th rowspan="2">Tipo de Servicio</th>
                                                                    <th rowspan="2">Clase de Servicio</th>
                                                                    <th rowspan="2" style="width:250px;">Descripción Clase de Servicio</th>
                                                                    <th>PREVENTIVO</th>
                                                                    <th>CORRECTIVO</th>
                                                                    <th colspan="3">COMPROBANTE SERVICIOS</th>
                                                                    <th>CAMBIO DE PIEZAS O COMPRA DE ADICIONALES</th>
                                                                    <th colspan="2">COMPROBANTE PIEZAS</th>
                                                                    <th rowspan="2" style="width:150px;">Otros Servicios / Observaciones</th>
                                                                    <th rowspan="2" style="width:100px;">Costo Total</th>
                                                                    <th rowspan="2" style="width:150px;">NOMBRE Y FIRMA RESPONSABLE COECSA</th>
                                                                    <th rowspan="2" style="width:94px;">Acciones</th>
                                                                </tr>
                                                                <tr>
                                                                    <th style="width:200px;">DETALLAR EXACTAMENTE LOS SERVICIOS QUE SE LE HICIERON</th>
                                                                    <th style="width:200px;">DETALLAR QUÉ SE LE HIZO</th>
                                                                    <th style="width:100px;">Facturas No. / Notas No.</th>
                                                                    <th style="width:100px;">¿Anexa Carta Responsiva?</th>
                                                                    <th style="width:100px;">¿Anexa INE?</th>
                                                                    <th style="width:250px;">DETALLAR CUÁLES PIEZAS SE CAMBIARON O QUÉ SE COMPRÓ</th>
                                                                    <th style="width:100px;">Facturas No. / Notas No.</th>
                                                                    <th style="width:100px;">¿Anexa fotos?</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody><?php 
                                                                echo $this->ModeloCoepam->reporte_prog_bmau($cic,$suc,$equ,$pam,0);
                                                                ?></tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                
                                                
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>

            <!-- END : End Main Content-->


<div class="modal fade text-left" id="mdlBmaNue" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="mdlBmaNue_Label">Modificar Solicitud de pago</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <form action="coe_pam/bma_agr/03102016223335/27062017191111/696" name="frmNue" id="frmNue" method="post" role="form">
                    <input type="hidden" name="rid" id="rid" value="0">
                    <div class="row">
                        <div class="col-md-12 titulosform">
                            <label>DETALLES</label>
                            
                        </div>
                        <hr>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="bit_fecha">Fecha*</label>
                                <input type="date" class="form-control" name="bit_fecha" id="bit_fecha" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="bit_odometro">Lectura odómetro</label>
                                <input type="text" class="form-control" placeholder="Lectura odómetro" name="bit_odometro" id="bit_odometro">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="bit_tipo">Tipo de Servicio</label>
                                <select class="form-control" name="bit_tipo" id="bit_tipo">
                                    <option value="preventivo">Preventivo</option><option value="correctivo">Correctivo</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group"><label for="bit_clase">Clase de Servicio</label>
                                <select class="form-control" name="bit_clase" id="bit_clase">
                                    <?php echo $this->ModeloCoepam->pam_sercla(''); ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 titulosform">
                            <label>SERVICIO</label>
                        </div><hr>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="bit_fecha">PREVENTIVO</label>
                                <input type="text" class="form-control" placeholder="DETALLAR EXACTAMENTE LOS SERVICIOS QUE SE LE HICIERON" name="bit_preventivo" id="bit_preventivo">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="bit_odometro">CORRECTIVO</label>
                                <input type="text" class="form-control" placeholder="DETALLAR QUÉ SE LE HIZO" name="bit_correctivo" id="bit_correctivo">
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-12 titulosform">
                            <label>COMPROBANTE SERVICIOS</label>
                            
                        </div>
                        <hr>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="bit_comfon">Facturas No. / Notas No.</label>
                                <input type="text" class="form-control" placeholder="" name="bit_comfon" id="bit_comfon">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="bit_comcr">¿Anexa Carta Responsiva?</label>
                                <select class="form-control" name="bit_comcr" id="bit_comcr">
                                    <option value="N/A">N/A</option><option value="SÍ">SÍ</option>            <option value="NO">NO</option>            
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="bit_comine">¿Anexa INE?</label>
                                <select class="form-control" name="bit_comine" id="bit_comine">
                                    <option value="N/A">N/A</option>
                                    <option value="SÍ">SÍ</option>
                                    <option value="NO">NO</option>
                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 titulosform">
                            <label>PIEZAS</label>
                            
                        </div><hr>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="bit_piecoc">CAMBIO DE PIEZAS O COMPRA DE ADICIONALES</label>
                                <input type="text" class="form-control" placeholder="DETALLAR CUÁLES PIEZAS SE CAMBIARON O QUÉ SE COMPRÓ" name="bit_piecoc" id="bit_piecoc">
                            </div>
                        </div>
                        <div class="col-md-12 label label-info">COMPROBANTE PIEZAS</div><br>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="bit_piefon">Facturas No. / Notas No.</label>
                                <input type="text" class="form-control" placeholder="" name="bit_piefon" id="bit_piefon">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="bit_piefot">¿Anexa fotos?</label>
                                <select class="form-control" name="bit_piefot" id="bit_piefot">
                                    <option value="N/A" >N/A</option>
                                    <option value="SÍ">SÍ</option>
                                    <option value="NO">NO</option>
                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="bit_oso">Otros Servicios / Observaciones</label>
                        <input type="text" class="form-control" placeholder="Detallar" name="bit_oso" id="bit_oso">
                    </div>
                    <div class="form-group">
                        <label for="bit_costo">Costo Total</label>
                        <input type="text" class="form-control" placeholder="0.0" name="bit_costo" id="bit_costo">
                    </div>
                    <div class="form-group">
                        <label for="bit_respon">NOMBRE Y FIRMA RESPONSABLE</label>
                        <input type="text" class="form-control" placeholder="" name="bit_respon" id="bit_respon">
                    </div>
                </form>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="savemdlBmaNue()">Guardar</button>
            </div>
        </div>
    </div>
</div>