<style type="text/css">
    #table_list th,#table_list td{
        font-size: 12px;
    }
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
</style>            
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">PAM</div>
                            <!--<p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>-->
                            <p class="content-sub-header mb-1">PAM de las estaciones</p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 table-bordered tablevc" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>Año</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php 
                                                                    $REQUEST_URI=$_SERVER['REQUEST_URI'];
                                                                    $ai = 2017;
                                                                    $aa = date('Y');
                                                                    for($x=$aa;$x>=$ai;$x--){
                                                                        if($REQUEST_URI=='/dplogisticsv2/Suc_pam' or $REQUEST_URI=='/Suc_pam'){
                                                                            $urllink=base_url().'Suc_pam/pam/'.$x.'?est=1'; 
                                                                        }else{
                                                                           $urllink=base_url().'Coe_pam/gsucs/'.$x; 
                                                                        }
                                                                        
                                                                        echo '<tr>
                                                                                <td>'.$x.'</td>
                                                                                <td><a href="'.$urllink.'" type="button" class="btn btn-light mr-1 mb-1" >Ver año</a></td>
                                                                             </tr>';
                                                                        
                                                                    }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->