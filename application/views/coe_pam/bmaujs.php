<script src="<?php echo base_url();?>app-assets/vendors/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>app-assets/css/plugins/form-validation.css">

<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/js/jquery.steps.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>app-assets/css/pages/form-wizard.css">

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css">

<script src="<?php echo base_url();?>public/plugins/fileinput/fileinput.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>public/plugins/fileinput/fileinput.min.css">


<script type="text/javascript" src="<?php echo base_url(); ?>public/js/bmau.js?v=<?php echo date('Ymdgis');?>"></script>