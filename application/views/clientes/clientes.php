<!--
    para esta vista basarse en C:\xampp\htdocs\dplogistics\JSIMods\clientes\ops\cli.php
    -->
<style type="text/css">
    table td{font-size: 12px;padding: 5px !important;}
    table th{font-size: 13px;padding: 8px !important;}
    .fileiframe{width: 100%;border: 0px;}
    #modal_add_subcli .select2-container{
        width: 100% !important;
    }
    .subcli_checked{
        display: none;
    }
</style>
<input type="hidden" name="cot_remite" id="cot_remite" value="<?php echo $numcliente?>">
<input type="hidden" name="rid_cli" id="rid_cli" value="<?php echo $datoscli->rid;?>">
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header"><?php echo $datoscli->cli_nombre.' '.$datoscli->cli_paterno.' '.$datoscli->cli_materno; ?> </div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Estado de cuenta</h4>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body" style="font-size:12px">
                                            <!--------------->
                                                Domicilio: <?php echo $datoscli->cli_calle.' / '.$datoscli->cli_numext.' / '.$datoscli->cli_numint.' :: '.$datoscli->cli_colonia.' '.$datoscli->cli_municipio.' / '.$datoscli->cli_entidad.' :: C.P.: '.$datoscli->cli_cp.' / Tel.: '.$datoscli->cli_telefono; ?>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Crédito 0.00</h4>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <?php $vencidos=0.00; 
                                                            foreach ($resultguias->result() as $item) { 
                                                                $vencidos=$vencidos+$item->importe;
                                                            } 
                                                            echo 'Vencidos: '.number_format($vencidos,2);
                                                        ?> 
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12" style="padding:0px;">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Empresa</th>
                                                                    <th>Folio</th>
                                                                    <th>Guía</th>
                                                                    <th>Importe</th>
                                                                    <th>Fecha guía</th>
                                                                    <th>Fecha pago</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($resultguias->result() as $item) { ?>
                                                                    <tr>
                                                                        <td><?php echo $item->emp;?></td>
                                                                        <td><?php echo $item->folio;?></td>
                                                                        <td><?php echo $item->guia;?></td>
                                                                        <td><?php echo number_format($item->importe,2);?></td>
                                                                        <td><?php echo date("Y-m-d", strtotime($item->fecini) ); ?></td>
                                                                        <td><?php echo date("Y-m-d", strtotime($item->fecfin) ); ?></td>
                                                                        <td><?php 
                                                                            //jBoton(['href'=>SECCION_NAV.'/guiver/'.$cliente.'/'.$gid,'value'=>'Ver guía','class'=>'btnGuiVer'])
                                                                            //Documentar/fdg/19052022111323
                                                                        if($item->ridg!=''){
                                                                            echo '<a href="'.base_url().'Cotizacion/file2/'.$item->ridg.'" class="btn btn-light mr-1 mb-1" Target="_blank" >Ver guía</a>';
                                                                        }

                                                                        ?>
                                                                            
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <?php $pendientes=0.00; 
                                                            foreach ($resultguias2->result() as $item) { 
                                                                $pendientes=$pendientes+$item->importe;
                                                            } 
                                                            echo 'Pendientes: '.number_format($pendientes,2);
                                                        ?> 
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12" style="padding:0px;">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Empresa</th>
                                                                    <th>Folio</th>
                                                                    <th>Guía</th>
                                                                    <th>Importe</th>
                                                                    <th>Fecha guía</th>
                                                                    <th>Fecha pago</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($resultguias2->result() as $item) { ?>
                                                                    <tr>
                                                                        <td><?php echo $item->emp;?></td>
                                                                        <td><?php echo $item->folio;?></td>
                                                                        <td><?php echo $item->guia;?></td>
                                                                        <td><?php echo number_format($item->importe,2);?></td>
                                                                        <td><?php echo date("Y-m-d", strtotime($item->fecini) ); ?></td>
                                                                        <td><?php echo date("Y-m-d", strtotime($item->fecfin) ); ?></td>
                                                                        <td><?php 
                                                                            //jBoton(['href'=>SECCION_NAV.'/guiver/'.$cliente.'/'.$gid,'value'=>'Ver guía','class'=>'btnGuiVer'])
                                                                            if($item->ridg!=''){
                                                                                echo '<a href="'.base_url().'Cotizacion/file2/'.$item->ridg.'" class="btn btn-light mr-1 mb-1" Target="_blank" >Ver guía</a>';
                                                                            }
                                                                        ?>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Consignatarios</h4>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body" style="overflow: auto;">
                                            <div class="row">
                    
                                                <div class="col-md-12" style="text-align: end;">
                                                    <button type="button" class="btn btn-success mr-1 mb-1" onclick="addconsig()">Agregar Consignatario</button>
                                                </div>
                                            </div>
                                            <!--------------->
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Nombre</th>
                                                            <th>Dirección</th>
                                                            <th>Colonia</th>
                                                            <th>Ciudad</th>
                                                            <th>Tel.</th>
                                                            <th>Email</th>
                                                            <th>C.P.</th>
                                                            <th>Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $row=1; 
                                                            foreach ($datosconsig->result() as $item) { ?>
                                                            <tr>
                                                                <td><?php echo $row;?></td>
                                                                <td><?php echo $item->con_nombre.' '.$item->con_paterno.' '.$item->con_materno;?></td>
                                                                <td><?php echo $item->con_calle.' '.$item->con_numext.' '.$item->con_numint;?></td>
                                                                <td><?php echo $item->con_colonia?></td>
                                                                <td><?php echo $item->con_municipio?></td>
                                                                <td><?php echo $item->con_telefono?></td>
                                                                <td><?php echo $item->con_email?></td>
                                                                <td><?php echo $item->con_cp;?></td>
                                                                <td>
                                                                    <button type="button" class="btn btn-secondary mr-1 mb-1" onclick="editarconsig(<?php echo $item->rid;?>)">
                                                                        <i class="fa fa-pencil fa-fw"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                        <?php $row++;
                                                            } ?>
                                                    </tbody>
                                                </table>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">RFC's</h4>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body" style="overflow: auto;" >
                                            <div class="row">
                                                
                                                <div class="col-md-12" style="text-align: end;">
                                                    <button type="button" class="btn btn-success mr-1 mb-1" onclick="addrfc()">Agregar RFC</button>
                                                </div>
                                            </div>
                                            <!--------------->
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Nombre</th>
                                                            <th>R.F.C.</th>
                                                            <th>Dirección</th>
                                                            <th>Teléfono</th>
                                                            <th>C.P.</th>
                                                            <th>Email</th>
                                                            <th>Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $row=1; 
                                                            foreach ($datosfacts->result() as $item) { ?>
                                                            <tr>
                                                                <td><?php echo $row;?></td>
                                                                <td><?php echo $item->fac_nrs;?></td>
                                                                <td><?php echo $item->fac_rfc;?></td>
                                                                <td><?php echo $item->fac_dir?></td>
                                                                <td><?php echo $item->fac_te1?></td>
                                                                <td><?php echo $item->fac_cod; ?></td>
                                                                <td><?php echo $item->fac_email?></td>
                                                                <td>
                                                                    <button type="button" class="btn btn-secondary mr-1 mb-1" onclick="editarrfc(<?php echo $item->rid;?>)">
                                                                        <i class="fa fa-pencil fa-fw"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                        <?php $row++;
                                                            } ?>
                                                    </tbody>
                                                </table>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="card">

                                    <div class="card-header">
                                        <h4 class="card-title">Subclientes</h4>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body" style="overflow: auto;" >
                                            <div class="row">
                                                <?php if($resul_hijo->num_rows()==0){ ?>
                                                    <div class="col-md-12" style="text-align: end;">
                                                        <button type="button" class="btn btn-success mr-1 mb-1" onclick="addclisub()">Agregar</button>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <!--------------->
                                                <?php if($resul_hijo->num_rows()==0){ ?>
                                                <table class="table table-striped" id="table_sc">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Nombre</th>
                                                            
                                                            <th>Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        
                                                    </tbody>
                                                </table>
                                                <?php }else{
                                                    $html='';
                                                    foreach ($resul_hijo->result() as $itemh) {
                                                        $html='';
                                                        $html.='<table class="table table-striped table-bordered">';
                                                                $html.='<tbody>';
                                                                    $html.='<tr><td colspan="3">Cliente Principal</td></tr>';
                                                                    $html.='<tr><td>'.$itemh->rid.'</td><td>'.$itemh->cli_nombre.''.$itemh->cli_paterno.''.$itemh->cli_materno.'</td><td></td></tr>';
                                                                $html.='</tbody>';
                                                        $html.='</table>';

                                                    }
                                                    echo $html;
                                                } ?>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Facturación</h4>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h5>Para generar</h5>
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Empresa</th>
                                                                    <th>Folio</th>
                                                                    <th>Guía</th>
                                                                    <th>Datos</th>
                                                                    <th>Importe</th>
                                                                    <th>Solicitada</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php 
                                                                    foreach ($fac_parafacturar->result() as $item_pf) {
                                                                        $inf_guia = $this->ModeloCatalogos->getselectwheren('guias',array('rid'=>$item_pf->guia,'sucursal'=>$sess_suc));
                                                                        $folio='';
                                                                        $guia='';
                                                                        $importe='';
                                                                        foreach ($inf_guia->result() as $item_pf_g) {
                                                                            $folio=$item_pf_g->folio;
                                                                            $guia=$item_pf_g->guia;
                                                                            $importe=$item_pf_g->importe;
                                                                        }

                                                                        $html_pd='';
                                                                        $html_pd.='<tr>';
                                                                        $html_pd.='<td>'.$item_pf->emp.'</td>';
                                                                        $html_pd.='<td>'.$folio.'</td>';
                                                                        $html_pd.='<td>'.$guia.'</td>';
                                                                        $html_pd.='<td>'.$item_pf->facturainfo.'</td>';
                                                                        $html_pd.='<td>'.$importe.'</td>';
                                                                        $html_pd.='<td>'.$item_pf->fechasol.'</td>';
                                                                        $html_pd.='<td></td>';
                                                                        $html_pd.='</tr>';
                                                                        echo $html_pd;
                                                                    }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h5>Pendiente de pago</h5>
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Empresa</th>
                                                                    <th>Folio</th>
                                                                    <th>Guía</th>
                                                                    <th>Datos</th>
                                                                    <th>Importe</th>
                                                                    <th>Solicitada</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php 
                                                                    foreach ($fac_pendietepago->result() as $item_pf) {
                                                                        $inf_guia = $this->ModeloCatalogos->getselectwheren('guias',array('rid'=>$item_pf->guia,'sucursal'=>$sess_suc));
                                                                        $folio='';
                                                                        $guia='';
                                                                        $importe='';
                                                                        foreach ($inf_guia->result() as $item_pf_g) {
                                                                            $folio=$item_pf_g->folio;
                                                                            $guia=$item_pf_g->guia;
                                                                            $importe=$item_pf_g->importe;
                                                                        }

                                                                        $html_pd='';
                                                                        $html_pd.='<tr>';
                                                                        $html_pd.='<td>'.$item_pf->emp.'</td>';
                                                                        $html_pd.='<td>'.$folio.'</td>';
                                                                        $html_pd.='<td>'.$guia.'</td>';
                                                                        $html_pd.='<td>'.$item_pf->facturainfo.'</td>';
                                                                        $html_pd.='<td>'.$importe.'</td>';
                                                                        $html_pd.='<td>'.$item_pf->fechasol.'</td>';
                                                                        $html_pd.='<td></td>';
                                                                        $html_pd.='</tr>';
                                                                        echo $html_pd;
                                                                    }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Ultimas guías</h4>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body" style="overflow:auto;">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>Fecha</label>
                                                    <input type="date" class="form-control" id="fechaini" onchange="loadtable()">
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Fecha</label>
                                                    <input type="date" class="form-control" id="fechafin" onchange="loadtable()">
                                                </div>
                                                <div class="col-md-2">
                                                    <button type="button" class="btn btn-success mr-1 mb-1" onclick="exportar()">Exportar</button>
                                                </div>
                                            </div>
                                            <!--------------->
                                                <table class="table table-striped" id="table_ultimas_guias">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Empresa</th>
                                                            <th>Folio</th>
                                                            <th>Guía</th>
                                                            <th>Origen</th>
                                                            <th>Destino</th>
                                                            <th>Remitente</th>
                                                            <th>Consignatario</th>
                                                            <th>Paquetes</th>
                                                            <th>Descripción</th>
                                                            <th>Peso</th>
                                                            <th>Volumen</th>
                                                            <th>Tarifa</th>
                                                            <th>Importe</th>
                                                            <th>Fecha</th>
                                                            <th>Admin</th>
                                                            <th>Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                            /*
                                                            $x=0;
                                                            $sql="SELECT gur.emp,gur.guia,gur.con,gur.con_dir,gur.con_col,gur.con_ciu,gur.con_tel,gur.con_cp,
                                                                         gui.folio,gui.guia,gui.origen,gui.destino,gui.paqs,gui.paq_des,gui.peso,gui.volumen,gui.tarifa,gui.importe,gui.fecha,gui.admin
                                                                FROM guia_rycs  as gur
                                                                inner join guias as gui on gui.rid=gur.guia and gui.sucursal='$sess_suc'
                                                                WHERE gur.cliente='$numcliente' 
                                                                ORDER BY gur.rid DESC LIMIT 6";
                                                            $sql="SELECT gur.emp,gur.guia,gur.con,gur.con_dir,gur.con_col,gur.con_ciu,gur.con_tel,gur.con_cp,
                                                                         gui.folio,gui.guia,gui.origen,gui.destino,gui.paqs,gui.paq_des,gui.peso,gui.volumen,gui.tarifa,gui.importe,gui.fecha,gui.admin
                                                                FROM guia_rycs  as gur
                                                                inner join guias as gui on gui.rid=gur.guia
                                                                WHERE gur.cliente='$numcliente' 
                                                                ORDER BY gur.rid DESC ";
                                                            $result_h=$this->db->query($sql);
                                                            $html='';
                                                            foreach ($result_h->result() as $item) {
                                                                
                                                                
                                                                $x++;
                                                                $html.='<tr>
                                                                        <td>'.$x.'</td>
                                                                        <td>'.$item->emp.'</td>
                                                                        <td>'.$item->folio.'</td>
                                                                        <td>'.$item->guia.'</td>
                                                                        <td>'.$item->origen.'</td>
                                                                        <td>'.$item->destino.'</td>
                                                                        <td>'.$item->con.'</td>
                                                                        <td>'.$item->con_dir.' '.$item->con_col.' '.$item->con_ciu.' '.$item->con_cp.' '.$item->con_tel.'</td>
                                                                        <td>'.$item->paqs.'</td>
                                                                        <td>'.$item->paq_des.'</td>
                                                                        <td>'.$item->peso.'</td>
                                                                        <td>'.$item->volumen.'</td>
                                                                        <td>'.$item->tarifa.'</td>
                                                                        <td>'.$item->importe.'</td>
                                                                        <td>'.$item->fecha.'</td>
                                                                        <td>'.$item->admin.'</td>
                                                                        <th></th>
                                                                    </tr>';
                                                            }
                                                            echo $html;
                                                            */
                                                        ?>
                                                        <?php 
                                                        /*
                                                            $x=0;
                                                            $sql="SELECT emp,guia,con,con_dir,con_col,con_ciu,con_tel,con_cp FROM guia_rycs WHERE cliente='$numcliente' ORDER BY rid DESC LIMIT 6";
                                                            $result_h=$this->db->query($sql);
                                                            $html='';
                                                            foreach ($result_h->result() as $item) {
                                                                
                                                                $sql2="SELECT folio,guia,origen,destino,paqs,paq_des,peso,volumen,tarifa,importe,fecha,admin FROM guias WHERE rid='$item->guia' AND sucursal='$sess_suc' LIMIT 1";
                                                                $result_h2=$this->db->query($sql2);
                                                                $folio='';
                                                                $guia='';
                                                                $origen='';
                                                                $destino='';
                                                                $paqs='';
                                                                $paq_des='';
                                                                $peso='';
                                                                $volumen='';
                                                                $tarifa='';
                                                                $importe='';
                                                                $fecha='';
                                                                $admin='';
                                                                foreach ($result_h2->result() as $item2) {
                                                                    $folio=$item2->folio;
                                                                    $guia=$item2->guia;
                                                                    $origen=$item2->origen;
                                                                    $destino=$item2->destino;
                                                                    $paqs=$item2->paqs;
                                                                    $paq_des=$item2->paq_des;
                                                                    $peso=$item2->peso;
                                                                    $volumen=$item2->volumen;
                                                                    $tarifa=$item2->tarifa;
                                                                    $importe=$item2->importe;
                                                                    $fecha=$item2->fecha;
                                                                    $admin=$item2->admin;
                                                                }
                                                                $x++;
                                                                $html.='<tr>
                                                                        <td>'.$x.'</td>
                                                                        <td>'.$item->emp.'</td>
                                                                        <td>'.$folio.'</td>
                                                                        <td>'.$guia.'</td>
                                                                        <td>'.$origen.'</td>
                                                                        <td>'.$destino.'</td>
                                                                        <td>'.$item->con.'</td>
                                                                        <td>'.$item->con_dir.' '.$item->con_col.' '.$item->con_ciu.' '.$item->con_cp.' '.$item->con_tel.'</td>
                                                                        <td>'.$paqs.'</td>
                                                                        <td>'.$paq_des.'</td>
                                                                        <td>'.$peso.'</td>
                                                                        <td>'.$volumen.'</td>
                                                                        <td>'.$tarifa.'</td>
                                                                        <td>'.$importe.'</td>
                                                                        <td>'.$fecha.'</td>
                                                                        <td>'.$admin.'</td>
                                                                        <th></th>
                                                                    </tr>';
                                                            }
                                                            echo $html;
                                                            */
                                                        ?>
                                                        
                                                    </tbody>
                                                </table>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->





<div class="modal fade text-left" id="modal_add_subcli" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel2">Agregar Sub Clientes</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                
                    
                <div class="row">
                        <div class="col-md-6">
                            <select id="search_cli"></select>
                        </div>
                        <div class="col-md-4" style="text-align: end;">
                            <button type="button" class="btn btn-primary addtrsubc" onclick="addtrsubc()">Agregar</button>
                        </div>
                        <div class="col-md-2">
                            <a onclick="addcliente()" class="btn btn-success mr-1 mb-1"><i class="ft-plus-square"></i></a>
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" id="table_cli_selected_add">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Cliente</th>
                                    <th>Sucursal</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class="tbody_sc_add">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                        
                


            </div>
            <div class="modal-footer" style="display:block;">
                <div class="row">
                    <div class="col-md-7">
                        
                    </div>
                    <div class="col-md-5">
                        <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" onclick="saveformsubcli()">Guardar</button>
                    </div>
                </div>
                
                
            </div>
        </div>
    </div>
</div>