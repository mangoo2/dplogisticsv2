<script src="<?php echo base_url();?>app-assets/vendors/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>app-assets/css/plugins/form-validation.css">

<script src="<?php echo base_url();?>public/plugins/fileinput/fileinput.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>public/plugins/fileinput/fileinput.min.css">

<script src="<?php echo base_url();?>public/js/clienteform.js?v=<?php echo date('Ymdgis')?>"></script>

<script type="text/javascript">
	$(document).ready(function($) {
		<?php if($rid>0){ ?>
			obtenerdatoscliente(<?php echo $rid;?>);	
		<?php } ?>
	});
</script>