<style type="text/css">
    .file-preview-thumbnails img{
        width: 100% !important;
        max-width: 439px;
    }
</style>
<input type="hidden" id="viewmodal" value="<?php if(isset($_GET['modal'])){ echo 1;}else{ echo 0;} ?>">
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header"><?php echo $title;?> CLIENTE</div>
                            <!--<p class="content-sub-header mb-1">All table styles are inherited in Bootstrap 4.3.1, meaning any nested tables will be styled in the same manner as the parent.</p>-->
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                        <form id="form_cliente" class="col-md-8">
                                                            <?php if(isset($_GET['suc'])){ ?>
                                                                <input type="hidden" name="sucursal" id="sucursal" value="<?php echo $_GET['suc'];?>">
                                                            <?php } ?>
                                                            <input type="hidden" name="rid" id="rid" value="<?php echo $rid;?>">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h5 class="box-title">Nombre</h5>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                    <div class="col-md-6 form-group">
                                                                        <label>Nombre(s)</label>
                                                                        <input type="text" class="form-control" name="cli_nombre" id="cli_nombre" required>
                                                                    </div>
                                                                    <div class="col-md-6 form-group">
                                                                        <label>Ape. paterno</label>
                                                                        <input type="text" class="form-control" name="cli_paterno" id="cli_paterno" required>
                                                                    </div>
                                                                    <div class="col-md-6 form-group">
                                                                        <label>Ape. materno</label>
                                                                        <input type="text" class="form-control validadexistencia" name="cli_materno" id="cli_materno" required>
                                                                    </div>
                                                                    <div class="col-md-6 form-group">
                                                                        <label>Identificación</label>
                                                                        <input type="text" class="form-control validadexistencia" name="cli_io" id="cli_io" >
                                                                    </div>
                                                                    <div class="col-md-6 form-group">
                                                                        <label>Cumpleaños dd/mm</label>
                                                                        <input type="date" class="form-control validadexistencia" name="cli_cum" id="cli_cum" required>
                                                                    </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h5 class="box-title">Domicilio</h5>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6 form-group">
                                                                    <label>Calle</label>
                                                                    <input type="text" class="form-control validadexistencia" name="cli_calle" id="cli_calle" required>
                                                                </div>
                                                                <div class="col-md-3 form-group">
                                                                    <label>Num ext</label>
                                                                    <input type="text" class="form-control" name="cli_numext" id="cli_numext" required>
                                                                </div>
                                                                <div class="col-md-3 form-group">
                                                                    <label>Num int</label>
                                                                    <input type="text" class="form-control" name="cli_numint" id="cli_numint">
                                                                </div>
                                                                <div class="col-md-4 form-group">
                                                                    <label>Entre Calles</label>
                                                                    <input type="text" class="form-control" name="cli_calles" id="cli_calles" >
                                                                </div>
                                                                <div class="col-md-4 form-group">
                                                                    <label>Colonia</label>
                                                                    <input type="text" class="form-control" name="cli_colonia" id="cli_colonia" required>
                                                                </div>
                                                                <div class="col-md-4 form-group">
                                                                    <label>C.P.</label>
                                                                    <input type="text" class="form-control" name="cli_cp" id="cli_cp" required>
                                                                </div>
                                                                <div class="col-md-4 form-group">
                                                                    <label>Municipio</label>
                                                                    <input type="text" class="form-control" name="cli_municipio" id="cli_municipio" required>
                                                                </div>
                                                                <div class="col-md-4 form-group">
                                                                    <label>Estado</label>
                                                                    <input type="text" class="form-control" name="cli_entidad" id="cli_entidad" required>
                                                                </div>
                                                                <div class="col-md-4 form-group">
                                                                    <label>Teléfono</label>
                                                                    <input type="tel" class="form-control" name="cli_telefono" id="cli_telefono" required>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h5 class="box-title">Contacto</h5>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-4 form-group">
                                                                    <label>Celular</label>
                                                                    <input type="tel" class="form-control" name="cli_celular" id="cli_celular">
                                                                </div>
                                                                <div class="col-md-4 form-group">
                                                                    <label>Email</label>
                                                                    <input type="email" class="form-control" name="cli_email" id="cli_email">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h5 class="box-title">Comentarios</h5>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 form-group">
                                                                    <label>Comentarios</label>
                                                                    <textarea class="form-control" name="cli_com" id="cli_com"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h5 class="box-title">Descuentos</h5>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-3 form-group">
                                                                    <label>Descuento</label>
                                                                    <input type="number" class="form-control" name="cli_des" id="cli_des" max="100">
                                                                </div>
                                                                <div class="col-md-5 form-group">
                                                                    <label>TARIFARIO COECSA LA MÁS BAJA (TARIFARIO VOI)</label>
                                                                    <input type="number" class="form-control" name="tarifa_baja" id="tarifa_baja">
                                                                </div>
                                                                <div class="col-md-4 form-group">
                                                                    <label>TARIFA ESPECIAL COECSA (DPL)</label>
                                                                    <input type="number" class="form-control" name="tarifa_especial" id="tarifa_especial">
                                                                </div>
                                                            </div>
                                                        </form>
                                                        <div class="col-md-4">
                                                                    <label>Adjuntar identificación</label>
                                                                    <input type="file" class="form-control" id="cliioimg" name="cliioimg">
                                                                </div>
                                                    
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a onclick="javascript:history.back()" class="btn btn-danger">Regresar</a>
                                                        <button id="okform" class="btn btn-primary" style="float: right;"><?php echo $formbtn;?></button>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->