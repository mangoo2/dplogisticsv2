<style type="text/css">
    #table_list th,#table_list td{
        font-size: 12px;
    }
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
</style>            
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Servicios</div>
                            <!--<p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>-->
                            <p class="content-sub-header mb-1">Config</p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 tablevc table-bordered" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>Clave</th>
                                                                    <th>Empresa</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php 
                                                                foreach ($resultlist->result() as $item) { 
                                                                    $resultsucu=$this->ModeloCatalogos->getselectwheren('coe_sucs',array('rid >'=>0));
                                                                    foreach ($resultsucu->result() as $item2) {
                                                                        $resultsucuf=$this->ModeloCatalogos->getselectwheren('ser_fols',array('sucursal'=>$item2->sucursal,'emp'=>$item->emp));
                                                                        if($resultsucuf->num_rows()==0){
                                                                            $this->ModeloCatalogos->Insert('ser_fols',array('sucursal'=>$item2->sucursal,'emp'=>$item->emp));
                                                                        }
                                                                    }

                                                                ?>
                                                                    <tr>
                                                                        <td><?php echo $item->emp;?></td>
                                                                        <td><?php echo $item->emp_nom;?></td>
                                                                        <td><a href="<?php echo base_url();?>Conf_sers/emp/<?php echo $item->emp;?>" type="button" class="btn-sm btn-info mr-1 mb-1"><i class="fa fa-folder-o"></i></a></td>
                                                                    </tr>
                                                                <?php 
                                                                } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->