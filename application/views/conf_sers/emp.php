<style type="text/css">
    #table_list th,#table_list td{
        font-size: 12px;
    }
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
</style>            
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Servicios <?php echo $resultrow->emp_nom;?></div>
                            <!--<p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>-->
                            <p class="content-sub-header mb-1">Config</p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-6" >
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-7">
                                                        <h4>Clientes</h4>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <a type="button" class="btn btn-success mr-1 mb-1" onclick="editarc(0)">Cliente Nuevo</a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 tablevc table-bordered" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>Pos</th>
                                                                    <th>Cliente</th>
                                                                    <th>Nombre</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($list_clis->result() as $item) { ?>
                                                                    <tr>
                                                                        <td><?php echo $item->cli_pos; ?></td>
                                                                        <td><?php echo $item->cli; ?></td>
                                                                        <td><?php echo $item->cli_nom; ?></td>
                                                                        <td><a type="button" 
                                                                                class="btn btn-sm btn-success mr-1 mb-1 edit_formc_<?php echo $item->rid;?>"
                                                                                data-cli_pos="<?php echo $item->cli_pos;?>"
                                                                                data-cli="<?php echo $item->cli;?>"
                                                                                data-cli_nom="<?php echo $item->cli_nom;?>"  
                                                                                onclick="editarc(<?php echo $item->rid;?>)"><i class="fa fa-pencil fa-fw"></i></a>
                                                                            <a type="button" onclick="deletec(<?php echo $item->rid;?>,1)" class="btn btn-sm btn-danger mr-1 mb-1"><i class="fa fa-trash fa-fw"></i></a></td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6" >
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-7">
                                                        <h4>Servicios</h4>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <a type="button" class="btn btn-success mr-1 mb-1" onclick="editars(0)">Servicio Nuevo</a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 tablevc table-bordered" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>Pos</th>
                                                                    <th>Cliente</th>
                                                                    <th>Nombre</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($list_sers->result() as $item) { ?>
                                                                    <tr>
                                                                        <td><?php echo $item->ser_pos; ?></td>
                                                                        <td><?php echo $item->ser; ?></td>
                                                                        <td><?php echo $item->ser_nom; ?></td>
                                                                        <td>
                                                                            <a type="button" 
                                                                                class="btn btn-sm btn-success mr-1 mb-1 edit_forms_<?php echo $item->rid;?>"
                                                                                data-ser_pos="<?php echo $item->ser_pos;?>"
                                                                                data-ser="<?php echo $item->ser;?>"
                                                                                data-ser_nom="<?php echo $item->ser_nom;?>"  
                                                                                onclick="editars(<?php echo $item->rid;?>)"><i class="fa fa-pencil fa-fw"></i></a>
                                                                            <a type="button" onclick="deletec(<?php echo $item->rid;?>,2)" class="btn btn-sm btn-danger mr-1 mb-1"><i class="fa fa-trash fa-fw"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->


<div id="modalformc" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Agregar cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_datosc">
            <input type="hidden" id="rid" name="rid" value="0">
            <input type="hidden" name="emp" id="emp" class="form-control" value="<?php echo $empresa?>" readonly>
            <div class="row">
                <div class="col-md-12">
                    <label>Empresa <?php echo $resultrow->emp_nom;?></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>* Clave</label>
                    <input type="text" name="cli" id="cli" class="form-control" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>* Nombre</label>
                    <input type="text" name="cli_nom" id="cli_nom" class="form-control" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>* Posición</label>
                    <input type="text" name="cli_pos" id="cli_pos" class="form-control" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>* Campos importantes</label>
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary saveformc" onclick="saveformc()">Guardar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div id="modalforms" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Agregar cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_datoss">
            <input type="hidden" id="rid" name="rid" value="0">
            <input type="hidden" name="emp" id="emp" class="form-control" value="<?php echo $empresa?>" readonly>
            <div class="row">
                <div class="col-md-12">
                    <label>Empresa <?php echo $resultrow->emp_nom;?></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>* Clave</label>
                    <input type="text" name="ser" id="ser" class="form-control" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>* Nombre</label>
                    <input type="text" name="ser_nom" id="ser_nom" class="form-control" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>* Posición</label>
                    <input type="text" name="ser_pos" id="ser_pos" class="form-control" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>* Campos importantes</label>
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary saveforms" onclick="saveforms()">Guardar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<input type="hidden" name="empsss" id="empsss" class="form-control" value="<?php echo $empresa?>" readonly>