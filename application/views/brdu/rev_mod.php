            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header"><?php echo $codigo;?></div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->

                                                <div class="row">
                                                    <div class="col-md-4">
                                                        Revisión: <b><?php echo date("d/m/Y", strtotime($result_revs->rev_fecha)).' '.date("h:i:s A", strtotime($result_revs->rev_fecha));?></b>
                                                    </div>
                                                    <div class="col-md-8" style="text-align: end;">
                                                        <a href="<?php echo base_url();?>BRDU" class="btn btn-danger">Cerrar</a>
                                                        <button class="btn btn-success saveform" onclick="saveform(0,<?php echo $rrevs_rid;?>)">Guardar</button>
                                                        <button class="btn btn-info saveform" onclick="saveform(1,<?php echo $rrevs_rid;?>)">Finalizar</button>
                                                    </div>
                                                </div>
                                                <style type="text/css">
                                                    .tdthcenter{
                                                        text-align: center;
                                                    }
                                                    .tablerm th,.tablerm td{
                                                        padding: 10px;
                                                    }
                                                    .tablerm{
                                                        font-size: 12px;
                                                    }

                                                </style>
                                                <form id="form_datos">
                                                    <table class="table table-bordered tablerm">
                                                        <thead>
                                                            <tr>
                                                                <th rowspan="2">#</th>
                                                                <th rowspan="2" class="tdthcenter">Actividad</th>
                                                                <?php foreach ($result_runis->result() as $itemru) { ?>
                                                                    <th colspan="2" class="tdthcenter"><?php echo $itemru->equ_numeco;?><br><?php echo $itemru->equ_nombre;?></th>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <?php foreach ($result_runis->result() as $itemru) { ?>
                                                                    <th class="tdthcenter">Revisión</th>
                                                                    <th class="tdthcenter">Observaciones</th>
                                                                <?php } ?>
                                                                
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                                foreach ($result_rpress->result() as $item) { 
                                                                    
                                                                    ?>
                                                                        <tr>
                                                                            <td><?php echo $item->pre_pos;?></td>
                                                                            <td><?php echo $item->pre_tit;?></td>
                                                                            <?php 
                                                                                foreach ($result_runis->result() as $itemru) {
                                                                                $result_ress=$this->ModeloCatalogos->getselectwherenbd2('rev_ress',array('rev'=>$rrevs_rid,'uid'=>$itemru->rid,'pid'=>$item->rid));
                                                                                $result_ress=$result_ress->row();
                                                                                    ?>
                                                                                        <td>
                                                                                            <select class="form-control" name="rr_<?php echo $result_ress->rid?>">
                                                                                                <option value="OK" <?php if($result_ress->res=='OK'){ echo 'selected';}?>>OK</option>
                                                                                                <option value="F" <?php if($result_ress->res=='F'){ echo 'selected';}?>>FALLANDO</option>
                                                                                                <option value="NA" <?php if($result_ress->res=='NA'){ echo 'selected';}?>>NA</option>
                                                                                            </select>
                                                                                        </td>
                                                                                        <td><input type="text" class="form-control" name="ro_<?php echo $result_ress->rid;?>" value="<?php echo $result_ress->res_obs; ?>" /></td>
                                                                                    <?php
                                                                                }
                                                                            ?>
                                                                        </tr>

                                                                    <?php
                                                                }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </form>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->