            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Nueva revision</div>
                            <!--<p class="content-sub-header mb-1">All table styles are inherited in Bootstrap 4.3.1, meaning any nested tables will be styled in the same manner as the parent.</p>-->
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a onclick="javascript:history.back()" class="btn btn-danger">Cancelar</a>
                                                        <button id="saveform" class="btn btn-success" style="float: right;">Agregar</button>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <form id="form_datos">
                                                           <h4>DETALLES</h4><br>
                                                            <label>Fecha / Hora</label>
                                                            <input type="datetime-local" name="rev_fecha" id="rev_fecha" class="form-control"> 
                                                        </form>
                                                        
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h4>UNIDADES O EQUIPO</h4><br>
                                                        <table id="table_unidades_equipo">
                                                            <tbody>
                                                            <?php foreach ($result_pam_equ->result() as $item) { ?>
                                                                <tr>
                                                                    <td>
                                                                        <label><input type="checkbox" id="rev_unis" name="rev_unis" value="<?php echo $item->equipo?>" >
                                                                        <?php echo $item->equ_numeco.' > '.$item->equ_nombre;?></label>
                                                                    </td>
                                                                </tr>
                                                            <?php }?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->