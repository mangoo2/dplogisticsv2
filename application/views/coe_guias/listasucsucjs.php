<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/plugins/loader/jquery.loader.css">
<script src="<?php echo base_url();?>public/plugins/loader/jquery.loader.js"></script>
<script type="text/javascript">
	$(document).ready(function($) {
		$('.loaderbtn').click(function(event) {
			$('body').loader('show');
		});	
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
	    /*
	    $("#table_list2").DataTable({
	        "ordering": false,
	        scrollX:true,
	        scrollCollapse:true,
	        fixedColumns:{leftColumns:8},
	        dom: 'Bfrtip',
        	buttons: [
            	'excel', 'pdf', 'print'
        	]
	    });
	    */
	    $("#table_list2").DataTable({
	        "ordering": false,
	        scrollX:true,
	        scrollCollapse:true,
	        fixedColumns:{leftColumns:8},
	        
	    });
	    $('.dt-buttons').addClass('btn-group');
  		$('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mb-2');
	} );
</script>