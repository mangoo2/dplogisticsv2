<?php 
    if( isset($_GET["rep_fecini"]) ){
        $rep_fecini=$_GET["rep_fecini"];
    }else{
        $rep_fecini=date('Y-m-d');
    }
    if( isset($_GET["rep_fecfin"]) ){
        $rep_fecfin=$_GET["rep_fecfin"];
    }else{
        $rep_fecfin=date('Y-m-d');
    }
    $trs='';$x=0;
    $trsSC='';$xSC=0;
    $efeTot=0;$creTot=0;
    $efeTotSC=0;$creTotSC=0;
    $tarTotSC=0;

    $efetarTotSC=0;$tarTot=0;$efetarTot=0;
    //$resG=$this->ModeloCatalogos->getselectwherenlimirorderby('guias',array('sucursal'=>$codigo),'rid','DESC',100);
    //$resG=$this->ModeloCatalogos->list_coe_guias($codigo);
    $resG=$this->ModeloCatalogos->list_coe_guias_fechas($codigo,$rep_fecini,$rep_fecfin);
    foreach ($resG->result() as $itemrg) {
        $fecha=date("d/m/Y", strtotime($itemrg->fecha)).' <br> '.date("h:i:s A", strtotime($itemrg->fecha));
        //================================================ cuando se solucione list_coe_guias esta se quitara
        //$resG_rem_con=$this->ModeloCatalogos->getselectwheren('guia_rycs',array('guia'=>$itemrg->rid));
        //$resG_rem_con=$resG_rem_con->row();
        $rem = $itemrg->rem.'<br>Dir.: '.$itemrg->rem_dir.' Col.: '.$itemrg->rem_col.' Ciu.: '.$itemrg->rem_ciu.' Tel.: '.$itemrg->rem_tel.' C.P.: '.$itemrg->rem_cp;
        $con = $itemrg->con.'<br>Dir.: '.$itemrg->con_dir.' Col.: '.$itemrg->con_col.' Ciu.: '.$itemrg->con_ciu.' Tel.: '.$itemrg->con_tel.' C.P.: '.$itemrg->con_cp;
        if($itemrg->tar_ce>0){ $tarce=$itemrg->tar_cen.'<br />'.$itemrg->tar_ce;}else{ $tarce=0.0;}
        //=====================================

        if($itemrg->corte!=''){
            //==============================
                if($itemrg->formaf=='efe'){
                    if($itemrg->importe2>0){
                        $efeTot+=$itemrg->importe1;
                    }else{
                        $efeTot+=$itemrg->importe;
                    }
                    
                    $formal='Efectivo';
                }elseif($itemrg->formaf=='cre'){
                    $creTot+=$itemrg->importe;
                    $formal='Crédito';
                }elseif($itemrg->formaf=='tar'){
                    
                       $tarTot+=$itemrg->importe; 
                    
                    
                    $formal='Tarjeta';
                }elseif($itemrg->formaf=='efetar'){
                    $efetarTot+=$itemrg->importe;
                    $formal='Efectivo Tarjeta';
                }else{
                    $formal='';
                }
                if($itemrg->importe2>0){
                    $formal.=' Tarjeta';
                    $tarTot+=$itemrg->importe2;
                }
            //==============================
            $x++;
            $trs.='<tr>
                        <td>'.$x.'<!--('.$itemrg->rid.')--></td><td>'.$fecha.'</td><td>'.$itemrg->emp_as.'</td><td>'.$itemrg->folio.'</td><td>'.$itemrg->guia_as.'</td>
                        <td>'.$itemrg->origen.'</td><td>'.$itemrg->destino.'</td><td>'.str_replace(',',', ',$itemrg->paq_des).'</td><td>'.$rem.'</td><td>'.$con.'</td>
                        <td>'.$itemrg->paqs.'</td><td>'.$itemrg->peso.'</td><td>'.$itemrg->volumen.'</td>
                        <td>'.$itemrg->paquetes.'</td>
                        <td>'.$itemrg->clasif.'</td>

                        <td>'.$itemrg->tarifa.'</td>
                        <td class="ce">'.$tarce.'</td><td class="cf">'.$itemrg->tar_cf.'</td>
                        <td>'.$itemrg->tar_pov.'</td><td>'.$itemrg->tar_fer.'</td><td>'.$itemrg->tar_cpc.'</td><td>'.$itemrg->tar_sub.'</td><td>'.$itemrg->tar_iva.'</td><td>'.number_format(floatval($itemrg->tar_tot),2).'</td><td>'.$formal.'</td>
                        <td>'.$itemrg->facturainfo.'</td>
                        <td>'.$itemrg->fechasol.'</td>
                        <td>'.$itemrg->fechafin.'</td>
                        <td>'.$itemrg->admin.'</td>
                        <td>'.$itemrg->cor_fecha.'</td>
                    </tr>';
        }else{
            //==============================
                if($itemrg->formaf=='efe'){
                    $efeTotSC+=$itemrg->importe;
                    $formal='Efectivo';
                }elseif($itemrg->formaf=='cre'){
                    $creTotSC+=$itemrg->importe;
                    $formal='Crédito';
                }elseif($itemrg->formaf=='tar'){
                    $tarTotSC+=$itemrg->importe;
                    $formal='Tarjeta';
                }elseif($itemrg->formaf=='efetar'){
                    $efetarTotSC+=$itemrg->importe;
                    $formal='Efectivo Tarjeta';
                }else{
                    $formal='';
                }
            //==============================
            $xSC++;
            $trsSC.='<tr>
                        <td>'.$xSC.'<!--('.$itemrg->rid.')--></td><td>'.$fecha.'</td><td>'.$itemrg->emp_as.'</td><td>'.$itemrg->folio.'</td><td>'.$itemrg->guia_as.'</td>
                        <td>'.$itemrg->origen.'</td><td>'.$itemrg->destino.'</td><td>'.str_replace(',',', ',$itemrg->paq_des).'</td><td>'.$rem.'</td><td>'.$con.'</td>
                        <td>'.$itemrg->paqs.'</td><td>'.$itemrg->peso.'</td><td>'.$itemrg->volumen.'</td>
                        <td>'.$itemrg->paquetes.'</td><td>'.$itemrg->clasif.'</td><td>'.$itemrg->tarifa.'</td>
                        <td class="ce">'.$tarce.'</td><td class="cf">'.$itemrg->tar_cf.'</td>
                        <td>'.$itemrg->tar_pov.'</td><td>'.$itemrg->tar_fer.'</td><td>'.$itemrg->tar_cpc.'</td><td>'.$itemrg->tar_sub.'</td><td>'.$itemrg->tar_iva.'</td><td>'.number_format($itemrg->tar_tot,2).'</td><td>'.$formal.'</td>
                        <td>'.$itemrg->facturainfo.'</td>
                        <td>'.$itemrg->fechasol.'</td>
                        <td>'.$itemrg->fechafin.'</td>
                        <td>'.$itemrg->admin.'</td>
                        <td>'.$itemrg->cor_fecha.'</td>
                    </tr>';
        }
    }
?>
<style type="text/css">
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
</style>            
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header"><?php echo $resultsuc->suc_nombre?></div>
                            <!--<p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>-->
                            <p class="content-sub-header mb-1">Guias utilizadas</p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                        <div class="box-tools pull-right">
                                            <?php 
                                                        if($botonatras==1){
                                                    ?>
                                            <a href="<?php echo base_url();?>Coe_guias/suc/<?php echo $codigo;?>" type="button" class="btn btn-sm btn-light mr-1 mb-1"><i class="fa fa-arrow-left" style="color:black;"></i></a>
                                            <?php }else{ ?>
                                                <a onclick="cargaloginpage('<?php echo base_url();?>Suc_guias')" type="button" class="btn btn-sm btn-light mr-1 mb-1"><i class="fa fa-arrow-left" style="color:black;"></i></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h4>Fecha de consulta</h4>
                                                    </div>
                                                    <div class="col-md-8" style="text-align:right;">
                                                        <a class="btn btn-success mr-1 mb-1" href="<?php echo base_url().'Coe_guias/cons_reporte/'.$codigo.'?rep_fecini='.$rep_fecini.'&rep_fecfin='.$rep_fecfin; ?>"  target="_blank">Excel</a>
                                                    </div>
                                                </div>

                                                <form action="<?php echo base_url();?>Coe_guias/cons/<?php echo $codigo;?>" method="get">
                                                    <div class="row">
                                                        
                                                        <div class="col-md-2 form-group">
                                                            <label >Fecha Inicio</label>
                                                            <input type="date" name="rep_fecini" value="<?php echo $rep_fecini;?>" class="form-control">
                                                        </div>
                                                        
                                                        <div class="col-md-2 form-group">
                                                            <label >Fecha Termino</label>
                                                            <input type="date" name="rep_fecfin" value="<?php echo $rep_fecfin;?>" class="form-control">
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button  type="submit" class="btn btn-success mr-1 mb-1 loaderbtn">Buscar..</button> 
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h4>Registros de la consulta</h4>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <table class="table table-bordered tablevc" id="tabla_general">
                                                            <thead>
                                                                <tr style="font-weight: bold;">
                                                                    <th></th>
                                                                    <th>EFECTIVO</th>
                                                                    <th>CRÉDITO</th>
                                                                    <th>TARJETA</th>
                                                                    <th>TOTAL</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr style="text-align: right;">
                                                                    <td>Guías finalizadas</td>
                                                                    <td>$ <?php echo number_format($efeTot,2); ?></td>
                                                                    <td>$ <?php echo number_format($creTot,2); ?></td>
                                                                    <td>$ <?php echo number_format($tarTot,2); ?></td>
                                                                    <td>$ <?php echo number_format($efeTot+$creTot+$tarTot,2); ?></td>
                                                                </tr>
                                                                <tr style="text-align: right;">
                                                                    <th >Guías sin corte de caja</th>
                                                                    <td>$ <?php echo number_format($efeTotSC,2); ?></td>
                                                                    <td>$ <?php echo number_format($creTotSC,2); ?></td>
                                                                    <td>$ <?php echo number_format($tarTotSC,2); ?></td>
                                                                    <td>$ <?php echo number_format($efeTotSC+$creTotSC+$tarTotSC,2); ?></td>
                                                                </tr>
                                                                <tr style="text-align: right;">
                                                                    <th>Total</th>
                                                                    <td>$ <?php echo number_format($efeTot+$efeTotSC,2); ?></td>
                                                                    <td>$ <?php echo number_format($creTot+$creTotSC,2); ?></td>
                                                                    <td>$ <?php echo number_format($tarTot+$tarTotSC,2); ?></td>
                                                                    <td>$ <?php echo number_format(($efeTot+$creTot)+($efeTotSC+$creTotSC)+($tarTot+$tarTotSC),2); ?></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 table-bordered tablevc" id="table_list2">
                                                            <thead>
                                                                <tr>
                                                                    <th rowspan="2">#</th>
                                                                    <th colspan="7">DETALLES</th>
                                                                    <th colspan="2">CLIENTES</th>
                                                                    <th colspan="4">PAQUETES</th>
                                                                    <th colspan="11">TARIFA</th>
                                                                    <th colspan="3">FACTURA</th>
                                                                    <th rowspan="2">ADMIN</th>
                                                                    <th rowspan="2">CORTE</th>
                                                                </tr>
                                                                <tr>
                                                                    <th>Fecha</th>
                                                                    <th>Empresa</th>
                                                                    <th>Folio</th>
                                                                    <th>Guía</th>
                                                                    <th>Origen</th>
                                                                    <th>Destino</th>
                                                                    <th>Descripción</th>
                                                                    <th>Remitente</th>
                                                                    <th>Consignatario</th>
                                                                    <th>Paquetes</th>
                                                                    <th>Peso</th>
                                                                    <th>Volumen</th>
                                                                    <th>Detalles</th>
                                                                    <th>Product_type</th>
                                                                    <th>Ck/U</th>
                                                                    <th>CE</th>
                                                                    <th>CF</th>
                                                                    <th>Pov</th>
                                                                    <th>FER</th>
                                                                    <th>CpC</th>
                                                                    <th>SUB</th>
                                                                    <th>IVA</th>
                                                                    <th>TOTAL</th>
                                                                    <th>FDP</th>
                                                                    <th>RFC</th>
                                                                    <th>Solicitada</th>
                                                                    <th>Procesada</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php echo $trs; ?>
                                                                
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->