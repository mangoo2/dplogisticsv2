    <?php 
        $RANGO=$this->session->userdata('sess_rango_nda');
    ?>
<style type="text/css">
    .form-control2{
        border-color: rgb(91 92 92) !important;
        color: #333333 !important;
    }
    .form-control2 option{
        background-color: #ffffff !important;
    }
    .btns-factura{
        min-width: 147px;
    }
    .btns-factura .btn{
        padding: 6px 11px;
    }
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
    .iframepdf iframe{
        width: 100%;
        border: 0;
        height: 305px;
    }
</style>
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Complementos de Pago</div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <a type="button" class="btn btn-success mr-1 mb-1 btn_ex1" onclick="excel()">Excel</a>
                                                        <a type="button" class="btn btn-success mr-1 mb-1 btn_ex2" style="display: none" onclick="excel_exportal()">Exportar</a>
                                                        <a type="button" class="btn btn-success mr-1 mb-1 btn_ex2" style="display: none" onclick="excel_exportalindi()">Exportar individual</a>
                                                    </div>
                                                    <div class="col-md-3">
                                                        
                                                    </div>
                                                    <div class="col-md-3">
                                                        
                                                    </div>    
                                                    <div class="col-md-4" style="text-align: right;">
                                                        <!--<a href="<?php echo base_url();?>Facturas/add" type="button" class="btn btn-success mr-1 mb-1">Agregar</a>-->
                                                        <?php if ($RANGO=='ttl' || $RANGO=='adm'){ ?>
                                                        <a class=" pull-right btn btn-danger shadow-z-1 white" onclick="cancelarfacturas()">Cancelar</a><br>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="">
                                                            <label>Fecha inicio</label>
                                                            <input type="date" class="form-control" id="finicial" onchange="loadtable()">
                                                        </div>
                                                        <div class="">
                                                            <label>Fecha fin</label>
                                                            <input type="date" class="form-control" id="ffin" onchange="loadtable()">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="">
                                                            <label>Razon social</label>
                                                            <select id="idcliente" name="idcliente" class="form-control" onchange="loadtable()">
                                                              
                                                            </select>
                                                        </div>
                                                        <div class="">
                                                            <label>Empresa</label>
                                                            <select id="tipoempresa" name="tipoempresa" class="form-control" onchange="loadtable()">
                                                              <option value="1" <?php if(isset($_GET['emp'])){ if($_GET['emp']==1){ echo 'selected';} }?>>COECSA</option>
                                                              <option value="2" <?php if(isset($_GET['emp'])){ if($_GET['emp']==2){ echo 'selected';} }?>>CADIPA</option>
                                                            </select>
                                                        </div>
                                                        <div class=" sucursales_view">
                                                          <label>Sucursal</label>
                                                          <select name="sucursal" id="sucursal" class="form-control" onchange="loadtable()">
                                                            <option value="0">Todas</option>
                                                            <?php  foreach ($sucursal_row->result() as $item) { ?>
                                                                <option value="<?php echo $item->id; ?>"><?php echo $item->nombre; ?></option>
                                                              <?php } ?>
                                                          </select>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div>
                                                            <label>Tipo de CFDI</label>
                                                            <select name="t_cfdi" id="t_cfdi" class="form-control" onchange="loadtable()">
                                                                <option value="0">Todas</option>
                                                                <?php foreach ($cfdi as $key) { ?>
                                                                    <option value="<?php echo $key->uso_cfdi; ?>" ><?php echo $key->uso_cfdi_text; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <label>Estado del comprobante</label>
                                                        <select name="e_comp" id="e_comp" class="form-control" onchange="loadtable()">
                                                            <option value="0">Todas</option>
                                                            <option value="1">Timbrado</option>
                                                            <option value="2">Cancelado</option>
                                                            <option value="3">Sin Timbrar</option>
                                                            
                                                        </select>
                                                    </div>




                                                    
                                                    
                                                    
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 table-bordered tablevc" id="tabla_facturacion">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Serie</th>
                                                                    <th>Folio</th>
                                                                    <th>Cliente</th>
                                                                    <th>RFC</th>
                                                                    <th>Monto</th>
                                                                    <th>Estatus</th>
                                                                    <th>Fecha</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row" style="display:none;">
                                                    <div class="col-md-12">
                                                        <select id="mot_can">
                                                            <?php foreach ($mot_can->result() as $item) { ?>
                                                                <option value="<?php echo $item->id;?>"><?php echo $item->motivo;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->



<div class="modal fade text-left" id="modalenviofact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600" id="myModalLabel33">Envió de Documentos</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Email: </label>
                            <div class="form-group">
                                <input type="email" placeholder="Email Address" class="form-control" id="correocliente">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Asunto: </label>
                            <div class="form-group">
                                <input type="text" placeholder="Asunto del correo" class="form-control" id="correoasunto">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Mensaje: </label>
                            <div class="form-group">
                                <textarea type="text" placeholder="Mensaje del correo" class="form-control" id="correomensaje"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row iframepdf">
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary enviocorreomail">Enviar</button> 
                </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="modalcomplementos" tabindex="-1" aria-labelledby="exampleModalSizeXl" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Complementos de pago</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <a class="btn btn-primary" onclick="addcomplemento()" style="background: #12264b;color: white;">Nuevo</a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 listadocomplementos">
                  </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal" style="background: #fcb520;color: white;">Cerrar</button>
            </div>
        </div>
    </div>
</div>