<?php



header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=reporte_complementos.xls");

?>

<table  border="1">
    <thead>
        <tr>
            <th style="background-color: #ccecff;">Serie</th>
            <th style="background-color: #ccecff;">Folio</th>
            <th style="background-color: #ccecff;">cfiUUDID</th>
            <th style="background-color: #ccecff;">Tipo</th>
            <th style="background-color: #ccecff;">Fecha</th>
            <th style="background-color: #ccecff;">RFC</th>
            <th style="background-color: #ccecff;">Receptor</th>
            <th style="background-color: #ccecff;">Estado</th>
            <th style="background-color: #ccecff;">Forma de pago</th>
            <th style="background-color: #ccecff;">Monto</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($result as $r){ ?>
        <tr>
            <td><?php echo $r->Serie ?></td>
            <td><?php echo $r->Folio ?></td>
            <td><?php echo $r->uuid ?></td>
            <td><?php echo 'Complemento' ?></td>
            <td><?php echo $r->fechatimbre ?></td>
            <td><?php echo $r->R_rfc ?></td>
            <td><?php echo utf8_decode($r->R_nombre) ?></td>
            <td><?php 
                $valor=''; 
                if($r->Estado==0){
                    $valor='Cancelada'; 
                }else if($r->Estado==1){
                    $valor='Timbrada'; 
                }else if($r->Estado==2){
                    $valor='error'; 
                } 
                echo $valor;
                ?></td>
            <td><?php echo $r->formapago_text ?></td>
            <td><?php echo $r->Monto ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>