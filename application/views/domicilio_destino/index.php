<style type="text/css">
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
    .fc-header-toolbar{
        width: 100%;
    }
</style>  
<!-- BEGIN : Main Content-->
<div class="main-content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-6">
                <div class="content-header">Listado de Domicilio destino</div>
            </div>
            <div class="col-md-6" style="text-align: end;">
                <a href="<?php echo base_url() ?>Domicilio_destino/registro" type="button" class="btn btn-success mr-1 mb-1">Agregar</a>
            </div>
        </div>
        <!--Basic Table Starts-->
        <section id="simple-table">
            <div class="row">
                <div class="col-12" style="padding:0px;">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <!--------------->
                                <div class="row">
                                    <div class="col-md-12 table-responsive">
                                        <table class="table m-0 table-bordered" id="table_list">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>#</th>
                                                    <th>DPL</th>
                                                    <th>CALLE</th>
                                                    <th>NUM. EXT.</th>
                                                    <th>NUM. INT</th>
                                                    <th>COLONIA</th>
                                                    <th>LOCALIDAD</th>
                                                    <th>REFERENCIA</th>
                                                    <th>MUNICIPIO</th>
                                                    <th>ESTADO</th>
                                                    <th>PAÍS</th>
                                                    <th>CÓDIGO POSTAL</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!--------------->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Basic Table Ends-->
    </div>
</div>