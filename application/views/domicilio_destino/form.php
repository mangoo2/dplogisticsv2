<input type="hidden" id="c_Pais" value="<?php echo $c_Pais ?>">
<input type="hidden" id="estado_aux" value="<?php echo $estado ?>">
<input type="hidden" id="c_Estado" value="<?php echo $c_Estado ?>">

<input type="hidden" id="localidad_aux" value="<?php echo $localidad ?>">
<input type="hidden" id="municipio_aux" value="<?php echo $municipio ?>">
<!-- BEGIN : Main Content-->
<div class="main-content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="content-header"><?php echo $title;?> DOMICILIO DESTINO</div>
                <!--<p class="content-sub-header mb-1">All table styles are inherited in Bootstrap 4.3.1, meaning any nested tables will be styled in the same manner as the parent.</p>-->
            </div>
        </div>
        <!--Basic Table Starts-->
        <section id="simple-table">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <!--<h4 class="card-title">Default Table</h4>-->
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <!--------------->
                                <form id="form_registro" method="POST">
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="hidden" name="rid" id="rid" value="<?php echo $id;?>">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5 class="box-title">Datos generales</h5>
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 form-group">
                                                    <label>DPL</label>
                                                    <select class="form-control" name="dpl" id="dpl" required style="width: 100%">   
                                                        <?php if($dpl!=''){?> 
                                                            <option value="<?php echo $dpl ?>"><?php echo $dpltxt ?></option>
                                                        <?php } ?>  
                                                    </select>
                                                </div>
                                                <div class="col-md-9 form-group">
                                                    <label>CALLE</label>
                                                    <input type="text" class="form-control" name="calle" value="<?php echo $calle;?>" required>
                                                </div>
                                            </div>    
                                            <div class="row">  
                                                <div class="col-md-4 form-group">
                                                    <label>NUM. EXT.</label>
                                                    <input type="text" class="form-control" name="num_ext" value="<?php echo $num_ext;?>" required>
                                                </div>  
                                                <div class="col-md-4 form-group">
                                                    <label>NUM. INT</label>
                                                    <input type="text" class="form-control" name="num_int" value="<?php echo $num_int;?>" required>
                                                </div>
                                                <div class="col-md-4 form-group">
                                                    <label>CÓDIGO POSTAL</label>
                                                    <input type="text" class="form-control" name="codigo_postal" id="codigo_postal" value="<?php echo $codigo_postal;?>">
                                                </div>
                                            </div>    
                                            <div class="row">
                                                <div class="col-md-4 form-group">
                                                    <label>PAÍS</label>
                                                    <select class="form-control" name="pais" id="pais">
                                                        <?php if($pais!=''){?> 
                                                            <option value="<?php echo $pais ?>"><?php echo $paistxt ?></option>
                                                        <?php } ?>     
                                                    </select>
                                                </div>
                                                <div class="col-md-4 form-group">
                                                    <label>ESTADO</label>
                                                    <select class="form-control" name="estado" id="estado">
                                                        <?php if($estado!=''){?> 
                                                            <option value="<?php echo $estado ?>"><?php echo $estadotxt ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4 form-group">
                                                    <label>COLONIA</label>
                                                    <select class="form-control" name="colonia" id="colonia">
                                                        <?php if($colonia!=''){?> 
                                                            <option value="<?php echo $colonia ?>"><?php echo $coloniatxt ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>    
                                            <div class="row">
                                                <div class="col-md-4 form-group">
                                                    <label>REFERENCIA</label>
                                                    <input type="text" class="form-control" name="referencia" value="<?php echo $referencia;?>">
                                                </div>
                                                <div class="col-md-4 form-group">
                                                    <label>LOCALIDAD</label>
                                                    <select class="form-control" name="localidad" id="localidad">
                                                        <?php if($localidad!=''){?> 
                                                            <option value="<?php echo $localidad ?>"><?php echo $localidadtxt ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4 form-group">
                                                    <label>MUNICIPIO</label>
                                                    <select class="form-control" name="municipio" id="municipio">
                                                        <?php if($municipio!=''){?> 
                                                            <option value="<?php echo $municipio ?>"><?php echo $municipiotxt ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-primary btn_registro" onclick="guardar_registro()">Guardar</button>
                                            <a onclick="javascript:history.back()" class="btn btn-danger">Regresar</a>
                                        </div>
                                    </div>
                            </div>            
                        </div>
                    </div>
                </div>            
            </div>
        </section>

    </div>
</div>            