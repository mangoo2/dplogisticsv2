<?php  
    $equ_nombre='';$equ_marca='';$equ_modelo='';$equ_numeco='';
    $eid=0;
    foreach ($result_equ->result() as $item) {
        $eid = $item->rid;
        $equ_nombre=$item->equ_nombre;

        $equ_marca=$item->equ_marca;
        $equ_modelo=$item->equ_modelo;
        $equ_numeco=$item->equ_numeco;
    }
    $ioimg='Sin evidencia';
    $result_pam_equevi=$this->ModeloCatalogos->getselectwheren('pam_equevi',array('eid'=>$eid));
    $equ_foto='';
    foreach ($result_pam_equevi->result() as $itemeqevi) {
        $equ_foto=$itemeqevi->evi_archivo;
    }
    if($equ_foto!=''){
        $url_file=FCPATH.'_files/_suc/'.$sucu.'/pam/equs/'.$equ.'/'.$equ_foto;
        $url_file_b=base_url().'_files/_suc/'.$sucu.'/pam/equs/'.$equ.'/'.$equ_foto;
        if(file_exists($url_file)){
            $img = @getimagesize($url_file) ? true : false;
            if($img){
                $ioimg='<img class="img-circle" src="'.$url_file_b.'" alt="" />';}
            else{
                $ioimg='<a href="'.$url_file_b.'" target="_blank">Ver archivo</a>';
            }
        }
    }
    if( isset($_POST["rep_fecini"]) ){$rep_fecini=$_POST["rep_fecini"];}else{$rep_fecini=date('Y-m-d');}
    if( isset($_POST["rep_fecfin"]) ){$rep_fecfin=$_POST["rep_fecfin"];}else{$rep_fecfin=date('Y-m-d');}

    $doc='';$htm='';
    $x_row=1;
    $resR = $this->ModeloCatalogos->consult_bd2_rev_unis($equ,$rep_fecini,$rep_fecfin);
    foreach ($resR->result() as $itemR) {
        $res_rev = $this->ModeloCatalogos->consult_bd2_BRDU_REV($itemR->rev);
        foreach ($res_rev->result() as $itemrev) {
            $doc=$itemrev->rev;
        }
        $btn='<a data-sucu="'.$sucu.'" data-equ="'.$equ.'" data-rev="'.$itemR->rev.'"  onclick="ver('.$itemR->rev.')" class="ver_'.$itemR->rev.' btn btn-primary btnRevVer" ><span class="fa fa-file-text-o"></span></a>';
        $htm.='<tr><td>'.$x_row.'</td><td>'.$itemR->rev_fec.'</td><td>'.$doc.'</td><td>'.$btn.'</td></tr>';
        $x_row++;
    }
?>
<style type="text/css">
    #table_list th,#table_list td{font-size: 12px;}
    .table-responsive{padding: 0px;}
    .divbuttonstable{width: 135px;}
    .tablevc th,.tablevc td{font-size: 12px;text-align: center;padding: 8px;}
    .imgloader{
            width: 50%;
            /* text-align: center; */
            margin-left: 20%;
    }
    #tabRegs td{
        padding: 10px 10px;
        font-size: 12px;
    }
</style>            
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">BRDU</div>
                            <p class="content-sub-header mb-1"><?php echo $suc_name;?></p>
                            <!--<p class="content-sub-header mb-1">Guías de las estaciones</p>-->
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card_xxx">
                                    <div class="card-header_xxx">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                        <div class="row">
                                            <div class="col-md-7"></div>
                                            <div class="col-md-5" style="text-align: end;">
                                                <a href="<?php echo base_url();?>Coe_brdu/suc/<?php echo $sucu;?>" type="button" class="btn btn-light btnRegresar btn-sm"><span class="fa fa-arrow-left"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class=" card col-xs-12 col-md-4">
                                                        <div class="card gradient-purple-bliss">
                                                            <div class="card-content">
                                                                <div class="card-body">
                                                                    <h3 class=" white" style="text-align: center;"><?php echo $equ_nombre;?></h3>
                                                                    <h5 class=" white"><?php echo $suc_name;?></h5>
                                                                    <div class="p-2 text-center">
                                                                        <?php echo $ioimg;?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-4 border-right">
                                                                <div class="description-block">
                                                                    <span class="description-text">MARCA</span><h5 class="description-header"><?php echo $equ_marca; ?></h5>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4 border-right">
                                                                <div class="description-block">
                                                                    <span class="description-text">MODELO</span><h5 class="description-header"><?php echo $equ_modelo; ?></h5>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="description-block">
                                                                    <span class="description-text">No. Económico</span><h5 class="description-header"><?php echo $equ_numeco; ?></h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-4">
                                                        <div class="card row">
                                                            <div class="col-md-12"><h3 class="box-title" style="font-size: 22px;margin-top: 15px;">Estado</h3></div>

                                                            <div class="col-md-12">
                                                                <table class="table table-bordered table-stripped">
                                                                    <tbody>
                                                                        <tr><td>OK</td><td>Correcto</td></tr>
                                                                        <tr><td>N/A</td><td>No Aplica</td></tr>
                                                                        <tr><td>F</td><td>Fallando</td></tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-4">
                                                        <div class="card row">
                                                            <div class="col-md-12"><h3 class="box-title" style="font-size: 22px;margin-top: 15px;">Fecha de consulta</h3></div>
                                                            <div class="col-md-12">
                                                                <form name="frmBus" id="frmBus" action="<?php echo base_url().'Coe_brdu/equ/'.$sucu.'/'.$equ; ?>" method="post">
                                                                    <table>
                                                                        <tr>
                                                                            <td><label for="rep_fecini" class="control-label">Fecha inicio</label> </td>
                                                                            <td><input type="date" class="form-control pull-right" id="rep_fecini" name="rep_fecini" value="<?php echo $rep_fecini; ?>" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><label for="rep_fecfin" class="control-label">Fecha termino</label> </td>
                                                                            <td><input type="date" class="form-control pull-right" id="rep_fecfin" name="rep_fecfin" value="<?php echo $rep_fecfin; ?>" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><button type="submit" class="btn btn-success" id="btnCons">Buscar...</button></td>
                                                                            <td></td>
                                                                        </tr>
                                                                    </table>
                                                                    
                                                                    
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                        <div class="row">
                                            <div class="col-md-7">Revisiones</div>
                                            <div class="col-md-5" style="text-align: end;">
                                                <!--<a onclick="javascript:history.back()" type="button" class="btn btn-light btnRegresar btn-sm"><span class="fa fa-arrow-left"></span></a>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <table class="table table-bordered" id="table_info">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Fecha</th>
                                                            <th>Documento</th>
                                                            <th>Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody><?php echo $htm;?>
                                                    </tbody>
                                                </table>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->





<?php $modalid='mdlRevVer'; ?>
<div class="modal fade text-left" id="<?php echo $modalid;?>" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="<?php echo $modalid;?>_Label">documento</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body body_<?php echo $modalid;?>">
                
                

            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
                <!--<button type="button" class="btn btn-primary" onclick="saveEquMod()">Guardar</button>-->
            </div>
        </div>
    </div>
</div>