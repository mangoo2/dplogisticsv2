<style type="text/css">
    #table_list th,#table_list td{
        font-size: 12px;
    }
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
</style>            
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">BRDU</div>
                            <!--<p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>-->
                            <p class="content-sub-header mb-1">Estaciones</p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 table-bordered tablevc" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Estación</th>
                                                                    <th>Ubicación</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php 
                                                                    $rowtr=1;
                                                                foreach ($resultsuc as $item) { ?>
                                                                    <tr>
                                                                        <td><?php echo $rowtr;?></td>
                                                                        <td><?php echo $item->suc_nombre;?></td>
                                                                        <td><?php echo $item->suc_ubicacion;?></td>
                                                                        <td><a href="<?php echo base_url();?>Coe_brdu/suc/<?php echo $item->sucursal;?>" type="button" class="btn-sm btn-info mr-1 mb-1 loaderbtn"><i class="fa fa-folder-open-o"></i></a></td>
                                                                    </tr>
                                                                <?php 
                                                                    $rowtr++;
                                                                } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->