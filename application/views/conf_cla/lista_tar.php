<style type="text/css">
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
</style>  
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Tarifas <?php echo $resultaero->aer_nom;?></div>
                            <p class="content-sub-header mb-1">Config</p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        Clasificaciones
                                                    </div>
                                                    <div class="col-md-10">
                                                        <a onclick="editar(0)" type="button" class="btn btn-success mr-1 mb-1">Agregar</a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 table-bordered table-striped thead-light tablevc" id="table_list">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                
                                                                    <th>Clasificación</th>
                                                                    <th>Descripción</th>
                                                                    <th>Tipo Costo</th>
                                                                    <th>Costo x kilo</th>
                                                                    <th>Kilos mínimos</th>
                                                                    <th>Costo mínimo</th>
                                                                    <th>Cargo fijo</th>
                                                                    <th>Cargo x combustible</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($resultlist->result() as $item) { ?>
                                                                    <tr>
                                                                        <td><?php echo $item->cla_nom;?></td>
                                                                        <td><?php echo $item->cla_des;?></td>
                                                                        <td><?php if($item->cla_tip=='uni'){ echo 'Costo Unico';}
                                                                                  if($item->cla_tip=='kil'){ echo 'Costo por kilo';}?></td>
                                                                        <td>$ <?php echo $item->cla_ck;?></td>
                                                                        <td><?php echo $item->cla_km;?></td>
                                                                        <td>$ <?php echo $item->cla_cm;?></td>
                                                                        <td>$ <?php echo $item->cla_cf;?></td>
                                                                        <td><?php echo $item->cla_cpc;?></td>
                                                                        <td>
                                                                            <a type="button" 
                                                                                class="btn btn-sm btn-success mr-1 mb-1 edit_form_<?php echo $item->rid;?>"
                                                                                data-cla_nom="<?php echo $item->cla_nom;?>"
                                                                                data-cla_des="<?php echo $item->cla_des;?>"
                                                                                
                                                                                data-cla_ck="<?php echo $item->cla_ck;?>" 
                                                                                
                                                                                data-cla_km="<?php echo $item->cla_km;?>"
                                                                                data-cla_cm="<?php echo $item->cla_cm;?>"
                                                                                
                                                                                data-cla_ce="<?php echo $item->cla_ce;?>"
                                                                                data-cla_cen="<?php echo $item->cla_cen;?>"

                                                                                data-cla_cf="<?php echo $item->cla_cf;?>"
                                                                                data-cla_cfn="<?php echo $item->cla_cfn;?>"
                                                                                
                                                                                data-cla_fer="<?php echo $item->cla_fer;?>"
                                                                                data-cla_fern="<?php echo $item->cla_fern;?>"

                                                                                data-cla_cpc="<?php echo $item->cla_cpc;?>"
                                                                                data-cla_cpcn="<?php echo $item->cla_cpcn;?>"
                                                                                data-cla_tip="<?php echo $item->cla_tip;?>"
                                                                                onclick="editar(<?php echo $item->rid;?>)"
                                                                            ><i class="fa fa-pencil fa-fw"></i></a>
                                                                            <a type="button" onclick="deleteaer(<?php echo $item->rid;?>)" class="btn btn-sm btn-danger mr-1 mb-1"><i class="fa fa-trash fa-fw"></i></a>
                                                                            
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->

<div id="modalform" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Modificar empresa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_datos">
            <input type="hidden" id="rid" name="rid" value="0">
            <input type="hidden" name="aero" id="aero" value="<?php echo $codigo?>">
            <div class="row">
                <div class="col-md-12">
                    <?php echo $resultaero->aer_nom;?>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-6">
                    <label>Clasificación</label>
                    <input type="text" name="cla_nom" id="cla_nom" class="form-control" required>
                </div>
                <div class="col-md-6">
                    <label>Descripción</label>
                    <input type="text" name="cla_des" id="cla_des" class="form-control" required>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    <label>Tipo de costo</label>
                    <select class="form-control" name="cla_tip" id="cla_tip">
                        <option value="kil" selected>Costo por kilo</option>
                        <option value="uni">Costo unico</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label>Kilos minimos</label>
                    <input type="number" name="cla_km" id="cla_km" class="form-control">
                </div>
                <div class="col-md-4">
                    <label>Costo minimo</label>
                    <input type="number" name="cla_cm" id="cla_cm" class="form-control">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-6">
                    <label>Cargos adicionales</label>
                </div>
                <div class="col-md-6">
                    <label>Descripción</label>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-6">
                    <input type="number" name="cla_ce" id="cla_ce" placeholder="0.0" class="form-control">
                </div>
                <div class="col-md-6">
                    <input type="text" name="cla_cen" id="cla_cen" placeholder="" class="form-control">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-6">
                    <input type="number" name="cla_cf" id="cla_cf" placeholder="0.0" class="form-control">
                </div>
                <div class="col-md-6">
                    <input type="text" name="cla_cfn" id="cla_cfn" placeholder="" class="form-control">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-6">
                    <input type="number" name="cla_fer" id="cla_fer" placeholder="0.0" class="form-control">
                </div>
                <div class="col-md-6">
                    <input type="text" name="cla_fern" id="cla_fern" placeholder="" class="form-control">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-6">
                    <input type="number" name="cla_cpc" id="cla_cpc" placeholder="0.0" class="form-control">
                </div>
                <div class="col-md-6">
                    <input type="text" name="cla_cpcn" id="cla_cpcn" placeholder="" class="form-control">
                </div>
            </div>






            
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary saveform" onclick="saveform()">Actualizar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
