<style type="text/css">
    .table_cont_env td{
        padding-top: 0px;
        padding-bottom: 0px; 
    }
    .table_cont_env .input-group{
        margin-bottom: 3px;
    }
    .inputs_fijos{
        border: 0px;
        background-color: transparent !important;
    }
    .paddingtd{
        padding-left: 5px !important;
        padding-right: 5px !important;
    }
</style>
<input type="hidden" name="cotizacion" id="cotizacion" value="<?php echo $cotizacion;?>">
<input type="hidden" name="sucursal_s" id="sucursal_s" value="<?php echo $sucursal;?>">
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Documentación</div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="icons-tab-steps">
                                                <!-- Step 1 Doc-->
                                                <h6>Documentación</h6>
                                                <fieldset>
                                                    <form id="form_1">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <label class="col-form-label">Seleccionar aerolínea</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <select class="form-control" name="aero" id="aero" onchange="obtenerinfo()" required>
                                                                    <option value=""></option>
                                                                    <?php foreach ($result_aereo->result() as $item) { ?>
                                                                        <option value="<?php echo $item->aero;?>"><?php echo $item->aer_nom;?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-md-2">
                                                                <label class="col-form-label">Tipo Servicio</label>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select class="form-control" name="tiposervicio" id="tiposervicio" required>
                                                                
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <h5>1. Detalles</h5>
                                                                <hr>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-md-2">
                                                                <label class="col-form-label">Ruta Origen</label>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select class="form-control" name="cot_rutori" id="cot_rutori" onchange="s_seletod()" required>
                                                                    <?php foreach ($result_ruta_origen->result() as $item) { ?>
                                                                        <option value="<?php echo $item->suc_ruta;?>" data-vorigen="<?php echo $item->rut_cla;?>"><?php echo $item->rut_cla;?> -> <?php echo $item->rut_nom;?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-2">
                                                                <label class="col-form-label">Ruta Destino</label>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select class="form-control" name="cot_rutdes" id="cot_rutdes" onchange="s_seletod()" required>
                                                                    <?php foreach ($result_ruta_destino->result() as $item) { ?>
                                                                        <option value="<?php echo $item->ruta;?>" data-vdestino="<?php echo $item->rut_cla;?>"><?php echo $item->rut_cla;?> -> <?php echo $item->rut_nom;?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1"></div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-md-2">
                                                                <label class="col-form-label">PRODUCT TYPE</label>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <select class="form-control" name="cot_clasif" id="cot_clasif" onchange="ontenerpreciosporclasificacion()" required>
                                                                    
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2" style="padding-right: 0;">
                                                                <label class="col-form-label">COSTO KILO / UNICO</label>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input type="number" class="form-control" name="cot_cos" id="cot_cos" step="any" onchange="recalculartotales()" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-md-2">
                                                                <label class="col-form-label">Descripción</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <textarea class="form-control" id="cot_descrip" name="cot_descrip" required></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-md-2">
                                                                <label class="col-form-label">Remitente</label>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <select class="form-control" name="cot_remite" id="cot_remite" required>
                                                                    
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <a onclick="addcliente()"  class="btn btn-success mr-1 mb-1"><i class="ft-plus-square"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="infocliente row">
                                                            
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12" style="margin-top:20px">
                                                                <b>Vuelo</b>
                                                                <hr style="margin-top:0px;">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label>Origen</label>
                                                                <select class="form-control v_origen" id="v_origen" name="v_origen">
                                                                    <option></option>
                                                                    <?php 
                                                                    $r_d_o=$this->Modelovuelos->doc_group_origen();
                                                                    foreach ($r_d_o->result() as $itemo) {
                                                                        $clave1=explode('(',$itemo->origen);
                                                                        $clave2=explode(')',$clave1[1]);
                                                                        $clave= $clave2[0];

                                                                        echo '<option value="'.$itemo->origen.'" data-ivo="'.$clave.'" class="'.$clave.'">'.$itemo->origen.'</option>';
                                                                    } 

                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label>Destino</label>
                                                                <select class="form-control v_destino" id="v_destino" name="v_destino">
                                                                    <option></option>
                                                                    <?php 
                                                                    $r_d_d=$this->Modelovuelos->doc_group_destino();
                                                                    foreach ($r_d_d->result() as $itemd) {
                                                                        $clave1=explode('(',$itemd->destino);
                                                                        $clave2=explode(')',$clave1[1]);
                                                                        $clave= $clave2[0];

                                                                        echo '<option value="'.$itemd->destino.'" data-ivo="'.$clave.'" class="'.$clave.'">'.$itemd->destino.'</option>';
                                                                    } 
                                                                    ?>

                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label>Fecha</label>
                                                                <input type="date" class="form-control" id="vfecha" name="vfecha" min="<?php echo date('Y-m-d');?>" onchange="select_o_d()">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label>Vuelo</label>    
                                                                <select class="form-control" id="novuelo" name="novuelo">
                                                                    <option></option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <br>
                                                            </div>
                                                            
                                                        </div>

                                                    </form>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5>2. Paquetes</h5>
                                                            <hr>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12" style="text-align: right;">
                                                            <a onclick="addpaquete()" type="button" class="btn btn-success mr-1 mb-1">Agregar Paquete</a>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <table class="table" id="table_paquetes">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Cantidad</th>
                                                                        <th>Largo</th>
                                                                        <th>Ancho</th>
                                                                        <th>Altura</th>
                                                                        <th>Peso</th>
                                                                        <th>Volumen</th>
                                                                        <th></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody class="tbody_paquetes">
                                                                    
                                                                </tbody>

                                                            </table>
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Paquetes</th>
                                                                        <th>Peso</th>
                                                                        <th>Volumen</th>
                                                                        <th>Tarifa x kilo</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody >
                                                                    <tr>
                                                                        <td class="totalpaquetes">0</td>
                                                                        <td><span class="totalpeso">0</span> Kg</td>
                                                                        <td><span class="totalvolumen">0</span> m<sup>3</sup></td>
                                                                        <td>$<span class="totalpreciokilo">0</span></td>
                                                                    </tr>
                                                                </tbody>
                                                                
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5>3. Cotización del envío</h5>
                                                            <hr>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <form class="table" id="form_doc_tars">
                                                                <table class="table table-striped table_cont_env">
                                                                    <tr>
                                                                        <td width="10%">0</td>
                                                                        <td width="50%" class="paddingtd">
                                                                            <input type="text"  class="form-control" name="tar_cen" id='tar_cen' value="Cargo Extra">
                                                                        </td>
                                                                        <td width="40%">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text" id="basic-addon1">$</span>
                                                                                </div>
                                                                                <input type="text" class="form-control" placeholder="0.00" value="0" id="tar_ce" name="tar_ce" aria-describedby="basic-addon1" onchange="calculartotalgeneral()">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>1</td>
                                                                        <td class="paddingtd"><input type="text"  class="form-control inputs_fijos "  id="tar_cfn" name="tar_cfn" value="Cargo Fijo" readonly></td>
                                                                        <td>
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text" id="basic-addon1">$</span>
                                                                                </div>
                                                                                <input type="text" class="form-control inputs_fijos" placeholder="0.00" id="tar_cf" name="tar_cf" value="0" aria-describedby="basic-addon1" readonly>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>2</td>
                                                                        <td class="paddingtd"><input type="text"  class="form-control inputs_fijos"  id="tar_povn" name="tar_povn" value="Peso ó Volumen" readonly></td>
                                                                        <td>
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text" id="basic-addon1">$</span>
                                                                                </div>
                                                                                <input type="text" class="form-control inputs_fijos" placeholder="0.00" id="tar_pov" name="tar_pov" value="0" aria-describedby="basic-addon1" readonly>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>3</td>
                                                                        <td class="paddingtd"><input type="text"  class="form-control inputs_fijos"  id="tar_fern" name="tar_fern" value="Flete / Empaque - Recolección" readonly></td>
                                                                        <td>
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text" id="basic-addon1">$</span>
                                                                                </div>
                                                                                <input type="text" class="form-control" placeholder="0.00" value="0" id="tar_fer" name="tar_fer" aria-describedby="basic-addon1" onchange="calculartotalgeneral()">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>4</td>
                                                                        <td class="paddingtd"><input type="text"  class="form-control inputs_fijos"  id="tar_cpcn" name="tar_cpcn" value="Cargo por Combustible" readonly></td>
                                                                        <td>
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text" id="basic-addon1">$</span>
                                                                                </div>
                                                                                <input type="text" class="form-control inputs_fijos" placeholder="0.00" id="tar_cpc" name="tar_cpc" value="0" aria-describedby="basic-addon1" readonly>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>5</td>
                                                                        <td class="paddingtd"><input type="text"  class="form-control inputs_fijos"  id="tar_subn" name="tar_subn" value="Sub-total (Base IVA)" readonly></td>
                                                                        <td>
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text" id="basic-addon1">$</span>
                                                                                </div>
                                                                                <input type="text" class="form-control inputs_fijos" placeholder="0.00" id="tar_sub" name="tar_sub" value="0" aria-describedby="basic-addon1" readonly>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>6</td>
                                                                        <td class="paddingtd"><input type="text"  class="form-control inputs_fijos"  id="tar_ivan" name="tar_ivan" value="Importe IVA" readonly></td>
                                                                        <td>
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text" id="basic-addon1">$</span>
                                                                                </div>
                                                                                <input type="text" class="form-control inputs_fijos" placeholder="0.00" id="tar_iva" name="tar_iva" value="0"aria-describedby="basic-addon1" readonly>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>7</td>
                                                                        <td class="paddingtd"><input type="text"  class="form-control inputs_fijos"  id="tar_totn" name="tar_totn" value="Total" readonly ></td>
                                                                        <td>
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text" id="basic-addon1">$</span>
                                                                                </div>
                                                                                <input type="text" class="form-control inputs_fijos" placeholder="0.00" id="tar_tot" name="tar_tot" value="0"aria-describedby="basic-addon1" readonly>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </form>
                                                        </div>
                                                    </div>

                                                </fieldset>
                                                <!-- Step 2  CYF-->
                                                <h6>Consignatario y factura</h6>
                                                <fieldset>
                                                    <form id="form_2_cyf">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <h5>4. Consignatario</h5>
                                                                <hr>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                                <div class="col-md-2">
                                                                    <label class="col-form-label">Consignatario</label>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <select class="form-control" name="cot_consign" id="cot_consign"  required>
                                                                        
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <a type="button" class="btn btn-info mr-1 mb-1" onclick="modal_consignatario()"><i class="ft-plus-square"></i></a>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <a type="button" class="btn btn-success mr-1 mb-1" onclick="asignar_consig()">Asignar</a>
                                                                </div>
                                                        </div>
                                                        <div class="row infoconsignatario">
                                                            
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <h5>5. Factura</h5>
                                                                <hr>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                                <div class="col-md-2">
                                                                    <label class="col-form-label">RFC's</label>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <select class="form-control" name="cot_factura" id="cot_factura"  required>
                                                                        
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <a type="button" class="btn btn-info mr-1 mb-1" onclick="modalrfc()" ><i class="ft-plus-square"></i></a>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <a type="button" class="btn btn-success mr-1 mb-1" onclick="asignar_factura()">Asignar</a>
                                                                </div>
                                                        </div>
                                                        <div class="row infofacturas">
                                                            
                                                        </div>
                                                        <div class="row">
                                                            <div class="custom-switch custom-control-inline mb-1 mb-xl-0">
                                                                <input type="checkbox" class="custom-control-input" id="facturar" name="facturar" value="1" onchange="infofacturar()">
                                                                <label class="custom-control-label mr-1" for="facturar">
                                                                    <span>Facturar</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h5>Detalles</h5>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 ">
                                                                    Ruta origen
                                                                </div>
                                                                <div class="col-md-7 infohtml_rorigen">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Ruta destino
                                                                </div>
                                                                <div class="col-md-7 infohtml_rdestino">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Paquetes
                                                                </div>
                                                                <div class="col-md-7 totalpaquetes">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Peso
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <span class="totalpeso">0</span> Kg
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Volumen
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <span class="totalvolumen">0</span> m<sup>3</sup>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Tarifa x kilo
                                                                </div>
                                                                <div class="col-md-7">
                                                                    $<span class="totalpreciokilo">0</span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Clasificación
                                                                </div>
                                                                <div class="col-md-7 infohtml_rclass">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Descripción
                                                                </div>
                                                                <div class="col-md-7 infohtml_rdescripcion">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h5>Remitente</h5>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    REMITENTE
                                                                </div>
                                                                <div class="col-md-7 infohtml_remitente">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    DIRECCIÓN
                                                                </div>
                                                                <div class="col-md-7 infohtml_direccion">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row ">
                                                                <div class="col-md-5">
                                                                    COLONIA
                                                                </div>
                                                                <div class="col-md-7 infohtml_colonia">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    CIUDAD
                                                                </div>
                                                                <div class="col-md-7 infohtml_ciudad">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    TEL
                                                                </div>
                                                                <div class="col-md-7 infohtml_tel">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    C.P.
                                                                </div>
                                                                <div class="col-md-7 infohtml_cp">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h5>Tarifa</h5>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <table class="table table-striped table_cont_env">
                                                                        <tr>
                                                                            <td width="10%" class="paddingtd">0</td>
                                                                            <td width="50%" class="paddingtd tar_cen">Cargo Extra</td>
                                                                            <td width="40%" class="paddingtd tar_ce">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">1</td>
                                                                            <td class="paddingtd tar_cfn">Cargo Fijo</td>
                                                                            <td class="paddingtd tar_cf">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">2</td>
                                                                            <td class="paddingtd tar_povn">Peso ó Volumen</td>
                                                                            <td class="paddingtd tar_pov">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">3</td>
                                                                            <td class="paddingtd tar_fern">Flete / Empaque - Recolección</td>
                                                                            <td class="paddingtd tar_fer">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">4</td>
                                                                            <td class="paddingtd tar_cpcn">Cargo por Combustible</td>
                                                                            <td class="paddingtd tar_cpc">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">5</td>
                                                                            <td class="paddingtd tar_subn">Sub-total (Base IVA)</td>
                                                                            <td class="paddingtd tar_sub">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">6</td>
                                                                            <td class="paddingtd tar_ivan">Importe IVA</td>
                                                                            <td class="paddingtd tar_iva">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">7</td>
                                                                            <td class="paddingtd tar_totn">Total</td>
                                                                            <td class="paddingtd tar_tot">0</td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <!-- Step 3 fyg -->
                                                <h6>Folio y Guía</h6>
                                                <fieldset>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h5>06. Folio y Guía</h5>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <label>Folio:</label>
                                                                    <span class="badge badge-success mb-1 mr-1"><h5 class="cot_folio"></h5></span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <form class="form" id="form_fyg">
                                                                <div class="col-md-12">
                                                                    <label>Numero de guía</label>
                                                                    <input type="text" name="cot_guia" id="cot_guia" class="form-control" required>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="alert alert-danger mb-2" role="alert" id="singuia">Sin número de guía.</div>
                                                                </div>
                                                                </form>
                                                            </div>
                                                            
                                                            
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h5>Remitente</h5>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    REMITENTE
                                                                </div>
                                                                <div class="col-md-7 infohtml_remitente">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    DIRECCIÓN
                                                                </div>
                                                                <div class="col-md-7 infohtml_direccion">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    COLONIA
                                                                </div>
                                                                <div class="col-md-7 infohtml_colonia">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    CIUDAD
                                                                </div>
                                                                <div class="col-md-7 infohtml_ciudad">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    TEL
                                                                </div>
                                                                <div class="col-md-7 infohtml_tel">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    C.P.
                                                                </div>
                                                                <div class="col-md-7 infohtml_cp">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row col-md-4 infoconsignatario2">
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h5>Detalles</h5>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 ">
                                                                    Ruta origen
                                                                </div>
                                                                <div class="col-md-7 infohtml_rorigen">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Ruta destino
                                                                </div>
                                                                <div class="col-md-7 infohtml_rdestino">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Paquetes
                                                                </div>
                                                                <div class="col-md-7 totalpaquetes">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Peso
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <span class="totalpeso">0</span> Kg
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Volumen
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <span class="totalvolumen">0</span> m<sup>3</sup>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Tarifa x kilo
                                                                </div>
                                                                <div class="col-md-7">
                                                                    $<span class="totalpreciokilo">0</span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Clasificación
                                                                </div>
                                                                <div class="col-md-7 infohtml_rclass">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Descripción
                                                                </div>
                                                                <div class="col-md-7 infohtml_rdescripcion">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h5>Tarifa</h5>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <table class="table table-striped table_cont_env">
                                                                        <tr>
                                                                            <td width="10%" class="paddingtd">0</td>
                                                                            <td width="50%" class="paddingtd tar_cen">Cargo Extra</td>
                                                                            <td width="40%" class="paddingtd tar_ce">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">1</td>
                                                                            <td class="paddingtd tar_cfn">Cargo Fijo</td>
                                                                            <td class="paddingtd tar_cf">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">2</td>
                                                                            <td class="paddingtd tar_povn">Peso ó Volumen</td>
                                                                            <td class="paddingtd tar_pov">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">3</td>
                                                                            <td class="paddingtd tar_fern">Flete / Empaque - Recolección</td>
                                                                            <td class="paddingtd tar_fer">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">4</td>
                                                                            <td class="paddingtd tar_cpcn">Cargo por Combustible</td>
                                                                            <td class="paddingtd tar_cpc">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">5</td>
                                                                            <td class="paddingtd tar_subn">Sub-total (Base IVA)</td>
                                                                            <td class="paddingtd tar_sub">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">6</td>
                                                                            <td class="paddingtd tar_ivan">Importe IVA</td>
                                                                            <td class="paddingtd tar_iva">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">7</td>
                                                                            <td class="paddingtd tar_totn">Total</td>
                                                                            <td class="paddingtd tar_tot">0</td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row col-md-4 infofacturas2">
                                                            
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <!-- Step 4 pdg-->
                                                <h6>Pago</h6>
                                                <fieldset>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h5>07. Forma de pago</h5>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12" style=" color: red;">
                                                                    <label>Folio: <span class="cot_folio"></span></label>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <label>Guía: <span class="cot_guia"></span></label>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <label>Tipo Pago:</label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <select class="form-control" id="cot_fdp" name="cot_fdp" onchange="cot_fdp()">
                                                                        <option value=""></option>
                                                                        <option value="efe">EFECTIVO</option>
                                                                        <option value="cre" class="optioncre">CRÉDITO</option>
                                                                        <option value="tar">TARJETA</option>
                                                                        <option value="efetar">EFECTIVO / TARJETA</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <form class="form" id="form_fdp_efe">
                                                                <div class="row cot_fdp_efe"   style="border: 2px solid #149078;border-radius: 6px; display: none;">
                                                                    
                                                                    <div class="col-md-12" style="background:#00a65aa8;padding: 7px; ">
                                                                        <b>PAGO EN EFECTIVO</b>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>Importe total</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <input type="number" name="cot_importe" id="cot_importe" class="form-control " value="">
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>Efectivo recibido</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <input type="number" class="form-control" id="cot_cantidad" name="cot_cantidad" value="" placeholder="$ 00.00" onchange="cam_cot_cantidad()" required>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>Cambio</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <input type="number" class="form-control" id="cot_cambio" name="cot_cambio" value="">
                                                                    </div>
                                                                    
                                                                </div>
                                                        </form>
                                                        <form class="form" id="form_fdp_tar">
                                                            <div class="row cot_fdp_tar"  style="border: 2px solid #d38510a8;border-radius: 6px; margin-top: 5px;display: none;">
                                                                
                                                                    <div class="col-md-12" style="background:#d38510a8;padding: 7px; ">
                                                                        <b>PAGO CON TARJETA</b>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>Importe total</label>
                                                                    </div>
                                                                    <div class="col-md-6 tar_tot">
                                                                        
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>DÉBITO</label>
                                                                    </div>
                                                                    <div class="col-md-6 form-group">
                                                                        <input type="number" class="form-control" id="banco_efectivo" name="banco_efectivo" placeholder="$ 00.00" value="">
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>CRÉDITO</label>
                                                                    </div>
                                                                    <div class="col-md-6 form-group">
                                                                        <input type="number" class="form-control" id="banco_credito" name="banco_credito" placeholder="$ 00.00" value="">
                                                                    </div>
                                                                
                                                                     
                                                            </div>
                                                        </form>
                                                        <form class="form" id="form_fdp_cre">
                                                            <div class="row cot_fdp_cre"  style="border: 2px solid #10d3d3a8;border-radius: 6px; margin-top: 5px;display: none;">
                                                                
                                                                    <div class="col-md-12" style="background:#10d3d3a8;padding: 7px; ">
                                                                        <b>PAGO EN CRÉDITO</b>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>Importe total</label>
                                                                    </div>
                                                                    <div class="col-md-6 tar_tot">
                                                                        
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>TIPO</label>
                                                                    </div>
                                                                    <div class="col-md-6 form-group">
                                                                        <select class="form-control" id="tipo_pago_credito" name="tipo_pago_credito" required>
                                                                            <option value=""></option>
                                                                            <option value="1">DEPOSITO BANCARIO</option>
                                                                            <option value="2">TRANSFERENCIA</option>
                                                                            <option value="3">EFECTIVO</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>Indique la fecha de pago</label>
                                                                    </div>
                                                                    <div class="col-md-6 form-group">
                                                                        <input type="date" class="form-control" id="cot_fecpag" name="cot_fecpag" min="<?php echo date('Y-m-d')?>" value="<?php echo date('Y-m-d')?>">
                                                                    </div>
                                                                
                                                                
                                                                     
                                                            </div>
                                                        </form>
                                                            
                                                            
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h5>Remitente</h5>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    REMITENTE
                                                                </div>
                                                                <div class="col-md-7 infohtml_remitente">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    DIRECCIÓN
                                                                </div>
                                                                <div class="col-md-7 infohtml_direccion">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    COLONIA
                                                                </div>
                                                                <div class="col-md-7 infohtml_colonia">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    CIUDAD
                                                                </div>
                                                                <div class="col-md-7 infohtml_ciudad">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    TEL
                                                                </div>
                                                                <div class="col-md-7 infohtml_tel">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    C.P.
                                                                </div>
                                                                <div class="col-md-7 infohtml_cp">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row col-md-4 infoconsignatario2">
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h5>Detalles</h5>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 ">
                                                                    Ruta origen
                                                                </div>
                                                                <div class="col-md-7 infohtml_rorigen">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Ruta destino
                                                                </div>
                                                                <div class="col-md-7 infohtml_rdestino">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Paquetes
                                                                </div>
                                                                <div class="col-md-7 totalpaquetes">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Peso
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <span class="totalpeso">0</span> Kg
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Volumen
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <span class="totalvolumen">0</span> m<sup>3</sup>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Tarifa x kilo
                                                                </div>
                                                                <div class="col-md-7">
                                                                    $<span class="totalpreciokilo">0</span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Clasificación
                                                                </div>
                                                                <div class="col-md-7 infohtml_rclass">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    Descripción
                                                                </div>
                                                                <div class="col-md-7 infohtml_rdescripcion">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h5>Tarifa</h5>
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <table class="table table-striped table_cont_env">
                                                                        <tr>
                                                                            <td width="10%" class="paddingtd">0</td>
                                                                            <td width="50%" class="paddingtd tar_cen">Cargo Extra</td>
                                                                            <td width="40%" class="paddingtd tar_ce">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">1</td>
                                                                            <td class="paddingtd tar_cfn">Cargo Fijo</td>
                                                                            <td class="paddingtd tar_cf">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">2</td>
                                                                            <td class="paddingtd tar_povn">Peso ó Volumen</td>
                                                                            <td class="paddingtd tar_pov">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">3</td>
                                                                            <td class="paddingtd tar_fern">Flete / Empaque - Recolección</td>
                                                                            <td class="paddingtd tar_fer">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">4</td>
                                                                            <td class="paddingtd tar_cpcn">Cargo por Combustible</td>
                                                                            <td class="paddingtd tar_cpc">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">5</td>
                                                                            <td class="paddingtd tar_subn">Sub-total (Base IVA)</td>
                                                                            <td class="paddingtd tar_sub">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">6</td>
                                                                            <td class="paddingtd tar_ivan">Importe IVA</td>
                                                                            <td class="paddingtd tar_iva">0</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="paddingtd">7</td>
                                                                            <td class="paddingtd tar_totn">Total</td>
                                                                            <td class="paddingtd tar_tot">0</td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row col-md-4 infofacturas2">
                                                            
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>





                                                
                                                    
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->













