<div class="modal fade text-left" id="modal_consignatario" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Agregar consignatario</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                
                <form id="form_consignatario" class="form">
                    <input type="hidden" name="rid" id="rid"  class="rid_config" value="0">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Nombre</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label>Nombre(s)</label>
                            <input type="text" name="con_nombre" class="form-control" id="con_nombre" placeholder="Nombre o nombres" required>
                        </div>
                        <div class="col-md-4 form-group">
                            <label>Ape. paterno</label>
                            <input type="text" name="con_paterno" class="form-control" id="con_paterno" placeholder="Apellido paterno" required>
                        </div>
                        <div class="col-md-4 form-group">
                            <label>Ape. materno</label>
                            <input type="text" name="con_materno" class="form-control" id="con_materno" placeholder="Apellido materno" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Domicilio</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label for="con_calle">Calle</label>
                            <input type="text" class="form-control" name="con_calle" id="con_calle" required>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="con_numext">Num ext</label>
                            <input type="text" class="form-control" name="con_numext" id="con_numext" required>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="con_numint">Num int</label>
                            <input type="text" class="form-control" name="con_numint" id="con_numint">
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="con_calles">Entre las calles de</label>
                            <input type="text" class="form-control" name="con_calles" id="con_calles" required>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="con_colonia">Colonia</label>
                            <input type="text" class="form-control" name="con_colonia" id="con_colonia" required>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="con_cp">C.P.</label>
                            <input type="text" class="form-control" name="con_cp" id="con_cp" required>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="con_municipio">Municipio</label>
                            <input type="text" class="form-control" name="con_municipio" id="con_municipio" required>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="con_entidad">Estado</label>
                            <input type="text" class="form-control" name="con_entidad" id="con_entidad" required>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="con_telefono">Teléfono</label>
                            <input type="text" class="form-control" name="con_telefono" id="con_telefono">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Contacto</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label>Celular</label>
                            <input type="text" name="con_celular" class="form-control" id="con_celular" placeholder="Nombre o nombres">
                        </div>
                        <div class="col-md-4 form-group">
                            <label>Email</label>
                            <input type="text" name="con_email" class="form-control" id="con_email" placeholder="Apellido paterno">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Comentarios</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Comentarios</label>
                            <textarea class="form-control" placeholder="Información adicional" rows="6" name="con_com" id="con_com"></textarea>
                        </div>
                        
                    </div>
                </form>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="saveformconsignatario()">Guardar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade text-left" id="modal_datosfiscales" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel2">Agregar RFC</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                
                    <form id="form_datosfiscales" class="form">
                        <input type="hidden" name="rid" id="rid" class="rid_df" value="0">
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label for="fac_rfc">R.F.C.</label>
                                <input type="text" class="form-control" name="fac_rfc" id="fac_rfc" required>
                            </div>
                            <div class="col-md-8 form-group">
                                <label for="fac_nrs">Nombre ó Razón Social</label>
                                <input type="text" class="form-control" name="fac_nrs" id="fac_nrs" required>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="fac_dir">Domicilio Fiscal</label>
                                <input type="text" class="form-control" placeholder="Calle y número" name="fac_dir" id="fac_dir" required>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="fac_cod">Código Postal</label>
                                <input type="number" class="form-control" name="fac_cod" id="fac_cod" minlength="5" required>
                            </div>
                            <div class="col-md-8 form-group">
                                <label for="fac_email">CORREO ELECTRÓNICO:</label>
                                <input type="email" class="form-control" name="fac_email" id="fac_email">
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="fac_te1">Teléfono</label>
                                <input type="text" class="form-control" name="fac_te1" id="fac_te1">
                            </div>
                            <div class="col-md-8 form-group">
                                <label>Régimen Fiscal</label>
                                <select id="RegimenFiscalReceptor" name="RegimenFiscalReceptor" class="form-control" required>
                                  <option value="" ></option>
                                  <option value="601" >601 General de Ley Personas Morales</option>
                                  <option value="603" >603 Personas Morales con Fines no Lucrativos</option>
                                  <option value="605" >605 Sueldos y Salarios e Ingresos Asimilados a Salarios</option>
                                  <option value="606" >606 Arrendamiento</option>
                                  <option value="607" >607 Régimen de Enajenación o Adquisición de Bienes</option>
                                  <option value="608" >608 Demás ingresos</option>
                                  <option value="609" >609 Consolidación</option>
                                  <option value="610" >610 Residentes en el Extranjero sin Establecimiento Permanente en México</option>
                                  <option value="611" >611 Ingresos por Dividendos (socios y accionistas)</option>
                                  <option value="612" >612 Personas Físicas con Actividades Empresariales y Profesionales</option>
                                  <option value="614" >614 Ingresos por intereses</option>
                                  <option value="615" >615 Régimen de los ingresos por obtención de premios</option>
                                  <option value="616" >616 Sin obligaciones fiscales</option>
                                  <option value="620" >620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos</option>
                                  <option value="621" >621 Incorporación Fiscal</option>
                                  <option value="622" >622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras</option>
                                  <option value="623" >623 Opcional para Grupos de Sociedades</option>
                                  <option value="624" >624 Coordinados</option>
                                  <option value="625" >625 Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas</option>
                                  <option value="626" >626 Régimen Simplificado de Confianza</option>
                                  <option value="628" >628 Hidrocarburos</option>
                                  <option value="629" >629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales</option>
                                  <option value="630" >630 Enajenación de acciones en bolsa de valores</option>
                                </select>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Forma de pago</label>
                                <select id="forma_pago" name="forma_pago" class="form-control" required>
                                  <option></option>
                                  <?php foreach ($forma as $item) { ?>
                                      <option value="<?php echo $item->clave; ?>"><?php echo $item->formapago_text; ?></option>
                                  <?php } ?>
                                </select>
                            </div> 
                            <div class="col-md-4 form-group">
                                <label>Método de pago</label>
                                <select id="metodo_pago" name="metodo_pago" class="form-control" required>
                                  <option></option>
                                  <?php foreach ($metodo as $item) { ?>
                                      <option value="<?php echo $item->metodopago; ?>"><?php echo $item->metodopago_text; ?></option>
                                  <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Uso CFDI</label>
                                <select id="uso_cfdi" name="uso_cfdi" class="form-control" required>
                                  <option></option>
                                  <?php foreach ($cfdi as $item) { ?>
                                      <option value="<?php echo $item->uso_cfdi; ?>"><?php echo $item->uso_cfdi_text; ?></option>
                                  <?php } ?>
                                </select>
                            </div>
                        </div>
                    </form>
                <div class="row">
                        <div class="col-md-6">
                            <input type="file" class="form-control" name="escaneofile" id="escaneofile">
                        </div>
                        <div class="col-md-6">
                            <input type="file" class="form-control" name="situacion_fiscal_file" id="situacion_fiscal_file">
                        </div>
                        <div class="col-md-6 divescalfile">
                        </div>
                        <div class="col-md-6 divsituacionf">
                        </div>
                </div>
                        
                


            </div>
            <div class="modal-footer" style="display:block;">
                <div class="row">
                    <div class="col-md-7">
                        <button type="button" class="btn btn-primary" onclick="printformdatosfiscales()">Imprimir Datos</button>
                    </div>
                    <div class="col-md-5">
                        <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" onclick="saveformdatosfiscales()">Agregar RFC</button>
                    </div>
                </div>
                
                
            </div>
        </div>
    </div>
</div>