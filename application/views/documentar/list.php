<style type="text/css">
#table_list th{padding-left: 10px;}
#table_list td{padding-left: 10px;}
</style>
<!-- BEGIN : Main Content-->
<div class="main-content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="content-header">Documentación</div>
                <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
            </div>
        </div>
        <!--Basic Table Starts-->
        <section id="simple-table">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <!--<h4 class="card-title">Default Table</h4>-->
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <!--------------->
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="<?php echo base_url();?>Documentar/addi" type="button" class="btn btn-success mr-1 mb-1">Agregar</a>
                                    </div>
                                    <?php if ($this->ModeloPermisos->SECCION('coe_bit_guias') || $this->ModeloPermisos->SECCION('suc_bit_guias')){ ?>
                                    <div class="col-md-6" style="text-align: right;">
                                        <a href="<?php echo base_url();?>Documentar/bitacora" type="button" class="btn btn-info mr-1 mb-1">Bitácora</a>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-bordered m-0" id="table_list">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>#</th>
                                                    <th style="width:90px;">Fecha/Hora</th>
                                                    <th>Cotiza</th>
                                                    <th>Aerolíneas</th>
                                                    <th>Folio/Guía</th>
                                                    <th>Clasif</th>
                                                    <th>Descrip</th>
                                                    <th>Ruta origen</th>
                                                    <th>Ruta destino</th>
                                                    <th>Cliente</th>
                                                    <th>Estatus</th>
                                                    <th>Acciones</th>
                                                </tr></thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!--------------->
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--Basic Table Ends-->
        </div>
    </div>
    <!-- END : End Main Content-->
    <div class="row" style="display:none;">
        <div class="col-md-12">
            <select id="mot_can">
                <?php foreach ($mot_can->result() as $item) { ?>
                <option value="<?php echo $item->id;?>"><?php echo $item->motivo;?></option>
                <?php } ?>
            </select>
        </div>
    </div>