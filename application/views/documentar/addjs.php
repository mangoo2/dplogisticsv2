<script src="<?php echo base_url();?>app-assets/vendors/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>app-assets/css/plugins/form-validation.css">

<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/js/jquery.steps.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>app-assets/css/pages/form-wizard.css">

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css">

<script src="<?php echo base_url();?>public/plugins/fileinput/fileinput.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>public/plugins/fileinput/fileinput.min.css">

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/documentar.js?v=<?php echo date('Ymdgis');?>"></script>
<script type="text/javascript">
	$(document).ready(function($) {
		setTimeout(function(){ 
		<?php 
			$doc_cotiza=$doc_cotiza->result();
			$doc_cotiza=$doc_cotiza[0];
			if(isset($_GET['status'])){
				if($_GET['status']=='doc'){
					?>$("#steps-uid-0-t-0").click();<?php
				}
				if($_GET['status']=='cyf'){
					?>$("#steps-uid-0-t-1").click();<?php
				}
				if($_GET['status']=='fyg'){
					?>$("#steps-uid-0-t-2").click();<?php
				}
				if($_GET['status']=='pdg'){
					?>$("#steps-uid-0-t-3").click();<?php
				}
				if($_GET['status']=='fdg'){
					
            		
            		if($doc_cotiza->sol_editar==0 or $doc_cotiza->sol_editar==1){
						?>
							var cotizacion=$('#cotizacion').val();
							var base_url = $('#base_url').val();
							window.location.href = base_url+"Documentar/fdg/"+cotizacion;
						<?php
					}
				}
			}
			//echo var_dump($doc_cotiza);
			//echo $doc_cotiza->cot_estatus;
			if($doc_cotiza->cot_estatus=='fdg'){
				if($doc_cotiza->sol_editar==0 or $doc_cotiza->sol_editar==1){
					
					redirect('Documentar/fdg/'.$doc_cotiza->cotiza); 
				}
			}
		?>
		}, 1500);
		
	});
</script>