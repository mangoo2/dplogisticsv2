<style type="text/css">
    .iframefile{
        width: 100%;
        border: 0px;
        min-height: 576px;
    }
</style>
<input type="hidden" name="cotizacion" id="cotizacion" value="<?php echo $cotizacion;?>">
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Documentación</div>
                            <p class="content-sub-header mb-1">PBC</p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4 class="card-title">Cotización: <?php echo $cotizacion?></h4>
                                            </div>
                                            <div class="col-md-6" style="text-align: right;">
                                                <a href="<?php echo base_url();?>Documentar" type="button" class="btn btn-secondary mr-1 mb-1">Regresar</a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <iframe src="<?php echo base_url().'Cotizacion/file/'.$cotizacion;?>" class="iframefile"></iframe>
                                            
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->















