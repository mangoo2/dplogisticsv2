<?php 
    if ($this->ModeloPermisos->SECCION('coe_bit_guias') || $this->ModeloPermisos->SECCION('suc_bit_guias')){ }else{
        redirect('Sistema'); 
    }
?>
<style type="text/css">
#table_list td, #table_list th{padding-left: 10px; font-size: 11px;}
</style>
<!-- BEGIN : Main Content-->
<div class="main-content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="content-header">Documentación bitacora</div>
                <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
            </div>
        </div>
        <!--Basic Table Starts-->
        <section id="simple-table">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <!--<h4 class="card-title">Default Table</h4>-->
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <!--------------->
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Tipo</label>
                                        <select class="form-control" id="tipo_ed_de" onchange="loadtable()">
                                            <option value="all">Todos</option>
                                            <option value="1">Editadas</option>
                                            <option value="0">Eliminadas</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-bordered m-0" id="table_list">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>#</th>
                                                    <th style="width:90px;">Fecha/Hora</th>
                                                    <th>Cotiza</th>
                                                    <th>Aerolíneas</th>
                                                    <th>Folio/Guía</th>
                                                    <th>Clasif</th>
                                                    <th>Descrip</th>
                                                    <th>Ruta origen</th>
                                                    <th>Ruta destino</th>
                                                    <th>Cliente</th>
                                                    <th>Estatus</th>
                                                    <th>TIPO</th>
                                                    <th>SOLICITANTE</th>
                                                    <th>AUTORIZACION</th>
                                                    <th>Acciones</th>
                                                </tr></thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!--------------->
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--Basic Table Ends-->
        </div>
    </div>
    <!-- END : End Main Content-->
