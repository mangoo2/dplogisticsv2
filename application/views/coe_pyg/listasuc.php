<?php 
    //======================Varios=========================
        $trsV='';$var_total_v=0;$var_total_g=0;$x=0;
        $result_trsV=$this->ModeloCatalogos->getselectwherenlimirorderby('suc_pres',array('sucursal'=>$codigo,'pre_tipo'=>'vrs'),'pre_fecha','DESC',30);
        foreach ($result_trsV->result() as $itemtv) {
            $x++;
            if($itemtv->pre_verifico==''){
                $var_verifico='Sin verificar...';
            }else{
                $var_verifico=$itemtv->pre_verifico;
            }
            if($itemtv->pre_saldo==$itemtv->pre_importe && $itemtv->pre_verifico==''){
                        /*
                        $accv=jBotones(
                            jBoton(['value'=>jIcono('edit'),'href'=>SECCION_NAV.'/var_mod/'.$suc.'/'.$vid,'class'=>'btnVarMod']).
                            jBoton(['value'=>jIcono('trash'),'href'=>SECCION_NAV.'/var_bor/'.$suc.'/'.$vid,'class'=>'btnVarBor'])
                        );
                        */
                        $accv='<a class="btn btn-info btn-sm edit_'.$itemtv->rid.'" onclick="edit_pres('.$itemtv->rid.')"
                            data-preimporte="'.$itemtv->pre_importe.'"
                            data-preforma="'.$itemtv->pre_forma.'"
                            data-preconcepto="'.$itemtv->pre_concepto.'"
                            data-prefecha="'.$itemtv->pre_fecha.'"
                            data-preasigno="'.$itemtv->pre_asigno.'"
                            data-pretipo="'.$itemtv->pre_tipo.'"
                        ><i class="fa fa-edit"></i></a> ';
                        $accv.='<a class="btn btn-danger btn-sm" onclick="delete_pres('.$itemtv->rid.')"><i class="fa fa-trash"></i></a> ';
            }else{$accv='';}
            $trsV.='<tr>
                        <td>'.$x.'</td>
                        <td>$ '.number_format($itemtv->pre_importe,2).'</td>
                        <td>$ '.number_format($itemtv->pre_saldo,2).'</td>
                        <td>'.$itemtv->pre_forma.'</td>
                        <td>'.$itemtv->pre_concepto.'</td>
                        <td>'.$itemtv->pre_fecha.'</td>
                        <td>'.$itemtv->pre_asigno.'</td>
                        <td>'.$var_verifico.'</td>
                        <td><a href="'.base_url().'_files/_suc/'.$itemtv->sucursal.'/pre/'.$itemtv->pre_comprobante.'" target="_blank">Ver comprobante</a></td>
                        <td>'.$accv.'</td>
                        <td>'.$itemtv->admin.'</td><td>'.$itemtv->pre_obs.'</td></tr>';
            $var_total_v=$var_total_v+$itemtv->pre_saldo;
        }

    //===============================================
    //======================Gasolina=========================
        $result_trsG=$this->ModeloCatalogos->getselectwherenlimirorderby('suc_pres',array('sucursal'=>$codigo,'pre_tipo'=>'gas'),'pre_fecha','DESC',30);
        $trsG='';$x=0;
        foreach ($result_trsG->result() as $itemtv) {
            $x++;
            if($itemtv->pre_verifico==''){
                $var_verifico='Sin verificar...';
            }else{
                $var_verifico=$itemtv->pre_verifico;
            }
            if($itemtv->pre_saldo==$itemtv->pre_importe && $itemtv->pre_verifico==''){
                        /*
                        $accv=jBotones(
                            jBoton(['value'=>jIcono('edit'),'href'=>SECCION_NAV.'/var_mod/'.$suc.'/'.$vid,'class'=>'btnVarMod']).
                            jBoton(['value'=>jIcono('trash'),'href'=>SECCION_NAV.'/var_bor/'.$suc.'/'.$vid,'class'=>'btnVarBor'])
                        );
                        */
                        $accv='<a class="btn btn-info btn-sm edit_'.$itemtv->rid.'" onclick="edit_pres('.$itemtv->rid.')"
                            data-preimporte="'.$itemtv->pre_importe.'"
                            data-preforma="'.$itemtv->pre_forma.'"
                            data-preconcepto="'.$itemtv->pre_concepto.'"
                            data-prefecha="'.$itemtv->pre_fecha.'"
                            data-preasigno="'.$itemtv->pre_asigno.'"
                            data-pretipo="'.$itemtv->pre_tipo.'"
                        ><i class="fa fa-edit"></i></a> ';
                        $accv.='<a class="btn btn-danger btn-sm" onclick="delete_pres('.$itemtv->rid.')"><i class="fa fa-trash"></i></a> ';
            }else{$accv='';}
            $trsG.='<tr>
                        <td>'.$x.'</td>
                        <td>$ '.number_format($itemtv->pre_importe,2).'</td>
                        <td>$ '.number_format($itemtv->pre_saldo,2).'</td>
                        <td>'.$itemtv->pre_forma.'</td>
                        <td>'.$itemtv->pre_concepto.'</td>
                        <td>'.$itemtv->pre_fecha.'</td>
                        <td>'.$itemtv->pre_asigno.'</td>
                        <td>'.$var_verifico.'</td>
                        <td><a href="'.base_url().'_files/_suc/'.$itemtv->sucursal.'/pre/'.$itemtv->pre_comprobante.'" target="_blank">Ver comprobante</a></td>
                        <td>'.$accv.'</td>
                        <td>'.$itemtv->admin.'</td><td>'.$itemtv->pre_obs.'</td></tr>';
            $var_total_g=$var_total_g+$itemtv->pre_saldo;
        }
    //====================================================
    //==============pagos solicitado======================
        $result_trsP=$this->ModeloCatalogos->getselectwherenlimirorderby('suc_egrb',array('sucursal'=>$codigo),'egr_fecini','DESC',30);
        $trsP='';$x=0;
        foreach ($result_trsP->result() as $itemtsp) {
            $egrfecini = date("d/m/Y h:i:s A", strtotime($itemtsp->egr_fecini));
            $x++;
            if($itemtsp->egr_compini!=''){
                $egr_compini='<a href="'.base_url().'_files/_suc/'.$itemtsp->sucursal.'/pag/'.$itemtsp->egr_compini.'" target="_blank">Comprobante</a>'; 
            }else{
                $egr_compini='';
            }
            if($itemtsp->egr_compfin!=''){
                $egr_compfin='<a href="'.base_url().'_files/_suc/'.$itemtsp->sucursal.'/pag/'.$itemtsp->egr_compfin.'" target="_blank">Comprobante</a>'; 
            }else{
                $egr_compfin='';
            }
            if($itemtsp->egr_est=='Finalizado' || $itemtsp->egr_est=='Cancelado'){
                $fecfin=$itemtsp->egr_fecfin;
                $accp='';
            }else{
                $fecfin='';
                //$accp=jBoton(['value'=>jIcono('edit'),'href'=>SECCION_NAV.'/pag_mod/'.$suc.'/'.$rid,'class'=>'btnPagMod']);

                $accp='<a class="btn btn-info btn-sm edit_ccp_'.$itemtsp->rid.'" onclick="edit_ccp('.$itemtsp->rid.')"
                            data-egrfecini="'.$egrfecini.'"
                            data-egrimporte="$ '.number_format($itemtsp->egr_importe,2).'"
                            data-egrconcepto="'.$itemtsp->egr_concepto.'"
                            data-egrobs="'.$itemtsp->egr_obs.'"
                            data-egrcompini="'.$itemtsp->egr_compini.'"
                            data-egrest="'.$itemtsp->egr_est.'"
                            data-suc="'.$itemtsp->sucursal.'"
                        ><i class="fa fa-edit"></i></a> ';
            }

            $trsP.='<tr>
                        <td>'.$x.'</td>
                        <td>'.$egrfecini.'</td>
                        <td>$ '.number_format($itemtsp->egr_importe,2).'</td>
                        <td>'.$itemtsp->egr_concepto.'</td>
                        <td>'.$egr_compini.'</td>
                        <td>'.$egr_compfin.'</td>
                        <td>'.$itemtsp->egr_obs.'</td>
                        <td>'.$itemtsp->admin.'</td>
                        <td>'.$itemtsp->egr_est.'</td>
                        <td>'.$accp.'</td>
                    </tr>';
        }
        
    //====================================================
    //==============Gastos varios======================
        $result_resGV=$this->ModeloCatalogos->getselectwherenlimirorderby('suc_egrs',array('sucursal'=>$codigo),'egr_fecha','DESC',50);
        $trsGV='';$xi=0;
         foreach ($result_resGV->result() as $itemgv) {
            $xi++;
            $egr_fecha = date("d/m/Y h:i:s A", strtotime($itemgv->egr_fecha));
            if($itemgv->egr_compfis!=''){
                $egr_compfis='<a href="'.base_url().'_files/_suc/'.$itemgv->sucursal.'/egr/'.$itemgv->egr_compfis.'" target="_blank">Comprobante</a>'; 
            }else{$egr_compfis='';}

            $pres='';
            $egr_pres = explode('|',$itemgv->egr_pres);
            $np=count($egr_pres);
            for($x=0;$x<$np;$x++){
                $result_pre_con=$this->ModeloCatalogos->getselectwheren('suc_pres',array('presupuesto'=>$egr_pres[$x]));
                foreach ($result_pre_con->result() as $itempc) {
                    $pres .= $itempc->pre_concepto.'<br>';
                }
                
            }
            $trsGV.='<tr>
                        <td>'.$xi.'</td>
                        <td>'.$egr_fecha.'</td>
                        <td>$ '.number_format($itemgv->egr_importe,2).'</td>
                        <td>'.$itemgv->egr_concepto.'</td>
                        <td>'.$itemgv->egr_compnum.'</td>
                        <td>'.$itemgv->egr_forma.'</td>
                        <td>'.$egr_compfis.'</td>
                        <td>'.$pres.'</td>
                        <td>'.$itemgv->egr_obs.'</td>
                        <td>'.$itemgv->admin.'</td>
                        
                    </tr>';
         }
    //====================================================
    //====================Gastos gasolina================================
        $resGG=$this->ModeloCatalogos->getselectwherenlimirorderby('suc_usogas',array('sucursal'=>$codigo),'gas_fecha','DESC',50);
        $trsGG='';$x=0;
        foreach ($resGG->result() as $itemgg) {
            $x++;
            $gas_fecha = date("d/m/Y h:i:s A", strtotime($itemgg->gas_fecha));
            $equ_numeco='';$pre_concepto='';
            //===================
                $r_p_eq=$this->ModeloCatalogos->getselectwheren('pam_equ',array('equipo'=>$itemgg->equipo));
                foreach ($r_p_eq->result() as $itemrqeq) {
                    $equ_numeco.=$itemrqeq->equ_numeco;
                }
                $gas_pres = explode('|',$itemgg->gas_pres);
                $np=count($gas_pres);
                for($x=0;$x<$np;$x++){
                    $r_p_pre=$this->ModeloCatalogos->getselectwheren('suc_pres',array('presupuesto'=>$gas_pres[$x]));
                    foreach ($r_p_pre->result() as $itemrppre) {
                        $pre_concepto.=$itemrppre->pre_concepto.'<br>';
                    }

                }
                if($itemgg->gas_ticket!=''){
                    $gas_ticket='<a href="'.base_url().'_files/_suc/'.$itemgg->sucursal.'/gas/'.$itemgg->gas_ticket.'" target="_blank">Comprobante</a>'; 
                }else{
                    $gas_ticket='';
                }
            //===================
            $trsGG.='<tr>
                        <td>'.$x.'</td>
                        <td>'.$equ_numeco.'</td>
                        <td>'.$gas_fecha.'</td>
                        <td>$ '.number_format($itemgg->gas_importe,2).'</td>
                        <td>'.$itemgg->gas_agente.'</td>
                        <td>'.$pre_concepto.'</td>
                        <td>'.$gas_ticket.'</td>
                        <td>'.$itemgg->gas_obs.'</td>
                        <td>'.$itemgg->admin.'</td>
                    </tr>';
        }
    //===============================================
?>
<style type="text/css">
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 3px;
    }
    .kv-file-upload{
        display: none;
    }
    .tablevc td{
        font-size: 11px;
    }
</style>
<input type="hidden" id="sucursal" value="<?php echo $codigo;?>">
<!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="content-header"><?php echo $resultsuc->suc_nombre?></div>
                    <!--<p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>-->
                    <p class="content-sub-header mb-1">Cuenta de estación</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-12">
                    <div class="card card-inverse bg-warning">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media" style="padding:0px">
                                    <div class="media-left align-self-center">
                                        <i class="fa fa-shopping-cart fa-fw fa-fw font-large-2 float-left"></i>
                                    </div>
                                    <div class="media-body text-right">
                                        <span>VARIOS</span>
                                        <h3 class="card-text" style="font-size: 20px;">$ <?php echo number_format($var_total_v,2);?></h3>
                                        <span>Saldo total de los depositos</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-12">
                    <div class="card card-inverse bg-info">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media" style="padding:0px">
                                    <div class="media-left align-self-center">
                                        <i class="fa fa-dashboard font-large-2 float-left"></i>
                                    </div>
                                    <div class="media-body text-right">
                                        <span>GASOLINA</span>
                                        <h3 class="card-text" style="font-size: 20px;">$ <?php echo number_format($var_total_g,2);?></h3>
                                        <span>Saldo total de los depositos</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="box-tools pull-right">
                        <a href="<?php echo base_url();?>Coe_pyg" type="button" class="btn btn-sm btn-light mr-1 mb-1"><i class="fa fa-arrow-left" style="color:black;"></i></a>
                    </div>
                </div>
            </div>
            <!--Basic Table Starts-->
            <section>
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Presupuestos varios</h4>
                                <div class="box-tools pull-right">
                                    <a type="button" class="btn btn-success mr-1 mb-1" onclick="modaladdpresupuesto(0,'vrs')">Agregar deposito varios</a>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <!--------------->
                                        <div class="row">
                                            <div class="col-md-12" style="overflow:auto;">
                                                <table class="table table-bordered tablevc">
                                                    <thead class="thead-light">
                                                        <tr>
                                                            <th>#</th><th>Importe</th><th>Saldo</th><th>Forma</th><th>Concepto</th><th>Fecha</th><th>Asigno</th><th>Verifico</th><th>Comprobante</th><th>Acciones</th><th>Admin</th><th>Estación</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php echo $trsV;?>        
                                                    </tbody>
                                                    
                                                </table>
                                            </div>
                                        </div>
                                    <!--------------->
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Presupuestos gasolina</h4>
                                <div class="box-tools pull-right">
                                    <a type="button" class="btn btn-success mr-1 mb-1" onclick="modaladdpresupuesto(0,'gas')">Agregar deposito gasolina</a>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <!--------------->
                                        <div class="row">
                                            <div class="col-md-12" style="overflow:auto;">
                                                <table class="table table-bordered tablevc">
                                                    <thead class="thead-light">
                                                        <tr><th>#</th><th>Importe</th><th>Saldo</th><th>Forma</th><th>Concepto</th><th>Fecha</th><th>Asigno</th><th>Verifico</th><th>Comprobante</th><th>Acciones</th><th>Admin</th><th>Estación</th></tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php echo $trsG;?> 
                                                    </tbody>
                                                    
                                                </table>
                                            </div>
                                        </div>
                                    <!--------------->
                                    
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Pagos solicitados</h4>
                                <div class="box-tools pull-right">
                                    <!--<a type="button" class="btn btn-success mr-1 mb-1" onclick="addvarios()">Agregar deposito gasolina</a>-->
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <!--------------->
                                        <div class="row">
                                            <div class="col-md-12" style="overflow:auto;">
                                                <table class="table table-bordered tablevc">
                                                    <thead class="thead-light">
                                                        <tr><th>#</th><th>Fecha</th><th>Importe</th><th>Concepto</th><th>Solicitud</th><th>Pago</th><th>Observaciones</th><th>Admin estación</th><th>Estatus</th><th>Acciones</th></tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php echo $trsP;?>
                                                    </tbody>
                                                    
                                                </table>
                                            </div>
                                        </div>
                                    <!--------------->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Gastos varios</h4>
                                <div class="box-tools pull-right">
                                    <!--<a type="button" class="btn btn-success mr-1 mb-1" onclick="addvarios()">Agregar deposito gasolina</a>-->
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <!--------------->
                                        <div class="row">
                                            <div class="col-md-12" style="overflow:auto;">
                                                <table class="table table-bordered tablevc">
                                                    <thead class="thead-light">
                                                        <tr><th>#</th><th>Fecha</th><th>Importe</th><th>Concepto</th><th>Factura o Nota número</th><th>Forma</th><th>Comprobante</th><th>Presupuesto</th><th>Observaciones</th><th>Admin</th></tr>
                                                    </thead>
                                                    <tbody><?php echo $trsGV;?></tbody>
                                                    
                                                </table>
                                            </div>
                                        </div>
                                    <!--------------->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Gastos gasolina</h4>
                                <div class="box-tools pull-right">
                                    <!--<a type="button" class="btn btn-success mr-1 mb-1" onclick="addvarios()">Agregar deposito gasolina</a>-->
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <!--------------->
                                        <div class="row">
                                            <div class="col-md-12" style="overflow:auto;">
                                                <table class="table table-bordered tablevc">
                                                    <thead class="thead-light">
                                                        <tr><th>#</th><th>Unidad</th><th>Fecha</th><th>Importe</th><th>Agente que cargo</th><th>Deposito de pago</th><th>Comprobante</th><th>Observaciones</th><th>Admin</th></tr>
                                                    </thead>
                                                    <tbody><?php echo $trsGG;?></tbody>
                                                    
                                                </table>
                                            </div>
                                        </div>
                                    <!--------------->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--Basic Table Ends-->
        </div>
    </div>
<!-- END : End Main Content-->




<div class="modal fade text-left" id="modal_presupesto" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalpresupuestoLabel">Agregar consignatario</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                
                <form id="form_presupuesto" class="form">
                    <input type="hidden" name="rid" id="rid"  class="rid_config" value="0">
                    
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="pre_importe">Importe *</label>
                            <input type="number" name="pre_importe" class="form-control" id="pre_importe" placeholder="0.00" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Forma *</label>
                            <input type="text" name="pre_forma" class="form-control" id="pre_forma" placeholder="Forma de asignacion" required>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Concepto *</label>
                            <input type="text" name="pre_concepto" class="form-control" id="pre_concepto" placeholder="" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Fecha/hora *</label>
                            <input type="datetime-local" name="pre_fecha" class="form-control" id="pre_fecha" value="<?php echo date('Y-m-d g:i:s')?>" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Asigno *</label>
                            <input type="text" name="pre_asigno" class="form-control" id="pre_asigno" placeholder="Nombre de quien asigno" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="file" name="pre_comprobante" id="pre_comprobante" class="form-control" >
                        </div>
                    </div>

                    
                    
                    
                </form>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="saveformpresupuesto()">Guardar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade text-left" id="modal_solicitudpago" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalpresupuestoLabel">Modificar Solicitud de pago</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                
                <form id="form_solicitudpago" class="form">
                    <input type="hidden" name="sprid" id="sprid"  class="rid_config" value="0">
                    
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label><b>Fecha: </b><span class="egrfecini"></span></label>
                        </div>
                        <div class="col-md-12 form-group">
                            <label><b>Importe: </b><span class="egrimporte"></span></label>
                        </div>
                        <div class="col-md-12 form-group">
                            <label><b>Concepto: </b><span class="egrconcepto"></span></label>
                        </div>
                        <div class="col-md-12 form-group">
                            <label><b>Observaciones: </b><span class="egrobs"></span></label>
                        </div>
                        <div class="col-md-12 form-group">
                            <label><b>Comprobante de solicitud: </b><span class="egrcompini"></span></label>
                        </div>
                        <div class="col-md-12 form-group">
                            <label><b>Comprobante de pago: </b></label>
                            <input type="file" name="egr_compfin" id="egr_compfin" class="form-control" required>
                        </div>
                        <div class="col-md-12 form-group">
                            <label><b>Estatus</b></label>
                            <select class="form-control" name="egrest" id="egrest" required>
            <option value="Solicitado">Solicitado</option><option value="Proceso">En Proceso</option><option value="Pendiente">Pendiente</option><option value="Cancelado">Cancelado</option><option value="Finalizado">Finalizar</option>
        </select>
                        </div>
                    </div>




                    
                    
                    
                    
                </form>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="saveformsolpago()">Guardar</button>
            </div>
        </div>
    </div>
</div>
