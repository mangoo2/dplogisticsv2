<?php 
 $corTrs='';
 $x=0;
 $pens=0;
 foreach ($resC->result() as $item) {
    $x++;
    $cor_btndep = '';
    
    if($item->cor_est=='pen'){
        $pens+=$item->cor_efectivo;
        $cor_est='Por depositar';
        
        //$cor_btndep = jBoton(['value'=>'Finalizar','href'=>SECCION_NAV.'/cor_dep/'.$suc.'/'.$cid,'class'=>'btnCorDep']);
        if($botonatras==1){
            $cor_btndep='<button type="button" class="btn btn-light mr-1 mb-1" onclick="finalizar('.$item->rid.')">Finalizar</button>';
        }else{
            $cor_btndep='<a  href="'.base_url().'Corte_caja/cor_ver/'.$item->corte.'" type="button" class="btn btn-light mr-1 mb-1 vercorte" >Ver Corte</a>';
        }
            
    }elseif($item->cor_est='fin'){
        $cor_est='Depositado';
    }else{
        $cor_est='Error...';
    }
                   
    $corTrs.='<tr>
                <td>'.$x.'</td>
                <td>'.$item->cor_fecha.'</td>
                <td>'.number_format($item->cor_efectivo,2).'</td>
                <td>'.number_format($item->cor_credito,2).'</td>
                <td>'.$item->cor_com.'</td>
                <td>'.$item->cor_admnom.'</td>
                <td>'.$cor_est.'</td>
                <td>'.$cor_btndep.'</td>
            </tr>';
 }
?>
<?php 
    $depTrs='';$dep_total=0;$x=0;$deps=0;$sals=0;
    foreach ($resD->result() as $item) {
        $fileimagenexit2='';
        $urlcomplobante=base_url().'_files/_suc/'.$codigo.'/dep/'.$item->dep_comprobante;
        $fileimagenexit1=$item->dep_comprobante;
        //===============================
            if($item->relacionguias>0){
                $id_relacionguias=$item->relacionguias;
                $result=$this->ModeloCatalogos->getselectwheren_o2('guias',array('rid'=>$id_relacionguias));
                foreach ($result->result() as $itemg) {
                    $urlcomplobante0=base_url().'comprobante/'.$itemg->file_comprobante;
                    $fileimagenexit2=$itemg->file_comprobante;
                }
                if($item->dep_comprobante==''){
                    $urlcomplobante=$urlcomplobante0;
                }

            }
        //===============================
        $x++;
        if($item->dep_est==='dep'){
            $deps += $item->dep_importe;
            $dep_est='Por verificar';
        }elseif($item->dep_est==='vrf'){
            $sals += $item->dep_saldo;
            $dep_est='Verificado';
        }elseif($item->dep_est==='can'){
            $dep_est='Cancelado';
        }else{
            $dep_est='Error...';
        }
        
        if($item->dep_est != 'Cancelado'){
            $dep_total+=$item->dep_saldo;
        }
        if($item->dep_verifico==''){
            $dep_verifico='Sin verificar...';
        }

        if($item->dep_saldo==$item->dep_importe && $item->dep_verifico=='' && $item->dep_est!='Cancelado'){
             
            /*
            $accd=jBotones(
                    jBoton(['value'=>'Verificar','href'=>SECCION_NAV.'/dep_ver/'.$suc.'/'.$did,'class'=>'btnDepVer','type'=>'primary']).
                    jBoton(['value'=>'Cancelar','href'=>SECCION_NAV.'/dep_can/'.$suc.'/'.$did,'class'=>'btnDepCan'])
                );
            */
            if($botonatras==1){ 
                if($item->dep_est!='vrf'){

                
                    $accd='<button type="button" class="btn btn-info mr-1 mb-1 verificar_'.$item->rid.'" 
                            onclick="verificar('.$item->rid.')"
                            data-fecha="'.$item->dep_fecha.'"
                            data-importe="'.$item->dep_importe.'"
                            data-saldo="'.$item->dep_saldo.'"
                            data-forma="'.$item->dep_forma.'"
                            data-concepto="'.$item->dep_concepto.'"
                            data-deposito="'.$item->dep_deposito.'"
                            data-comprobante="'.$urlcomplobante.'"
                            data-usuario="'.$item->admin.'"

                            >Verificar</button>';
                    $accd.='<button type="button" class="btn btn-light mr-1 mb-1" onclick="cancelar('.$item->rid.')">Cancelar</button>';
                    $accd.='<!-- $item->dep_saldo: ('.$item->dep_saldo.')==item->dep_importe:('.$item->dep_importe.')  item->dep_verifico('.$item->dep_verifico.')==""  dep_est: ('.$item->dep_est.')-->';
                }
            }else{
                if($item->dep_est!='vrf'){
                    $accd='<a 
                                onclick="adddeposito('.$item->rid.')" 
                                type="button" class="btn btn-sm btn-success mr-1 mb-1 deposito_'.$item->rid.'"
                                data-sucursal="'.$item->sucursal.'"
                                data-importe="'.$item->dep_importe.'"
                                data-forma="'.$item->dep_forma.'"
                                data-concepto="'.$item->dep_concepto.'"
                                data-fecha="'.$item->dep_fecha.'"
                                data-deposito="'.$item->dep_deposito.'"

                            ><i class="fa fa-pencil fa-fw"></i></a>';
                    $accd.='<a type="button" onclick="deletedeposito('.$item->rid.')" class="btn btn-sm btn-danger mr-1 mb-1"><i class="fa fa-trash fa-fw"></i></a>';
                }
            }
            
        }else{
            $accd='';
        }
                    
        
        /*
        $depTrs.=jTableTr([$x,formatoMoneda($dep_importe,'pesos'),formatoMoneda($dep_saldo, 'pesos'),$dep_forma,$dep_concepto,fechaHoraImp($dep_fecha,12),$dep_deposito,$dep_verifico,jLink(['value'=>'Ver comprobante','href'=>DIR_SUC.SUC_CLA.'/dep/'.$dep_comprobante,'target'=>'_blank']),$dep_est,jBotones($accd)]);
        */
        //$codigo o $this->sess_suc
        $urlimegenurl='<a href="'.$urlcomplobante.'" target="_blank">Ver Comprobante</a>';
        if($fileimagenexit1=='' and $fileimagenexit2==''){
            $urlimegen='<!--'.$urlimegenurl.'-->';
        }else{
            $urlimegen=$urlimegenurl;
        }
        $depTrs.='<tr>
                    <td>'.$x.'</td>
                    <td>'.$item->dep_importe.'</td>
                    <td>'.$item->dep_saldo.'</td>
                    <td>'.$item->dep_forma.'</td>
                    <td>'.$item->dep_concepto.'</td>
                    <td>'.$item->dep_fecha.'</td>
                    <td>'.$item->dep_deposito.'</td>
                    <td>'.$item->dep_verifico.'</td>
                    <td>'.$urlimegen.'</td>
                    <td>'.$dep_est.'</td>
                    <td>'.$accd.'</td>
                </tr>';
    }
?>
<style type="text/css">
    .tablevc th,.tablevc td{
        font-size: 11px;
        text-align: center;
        padding: 3px;
    }
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
    .vercorte{
        color: black !important;
    }
    .kv-file-upload{
        display: none;
    }
    /*-----------*
        .modal-footer{
            border-top: 1px solid rgb(2 2 2 / 20%) !important;
        }
        .modal,.modal-title{
            color: black !important;
        }
        .modal-content{
            background: white !important;
        }
    /*--------*/
</style>            
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header"><?php echo $resultsuc->suc_nombre?></div>
                            <!--<p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>-->
                            <p class="content-sub-header mb-1">Cuenta de estación</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-3 col-lg-6 col-12">
                            <div class="card card-inverse bg-warning">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-left align-self-center">
                                                <i class="ft-rotate-cw font-large-2 float-left"></i>
                                            </div>
                                            <div class="media-body text-right">
                                                <h3 class="card-text" style="font-size: 20px;">$ <?php echo number_format($pens,2); ?></h3>
                                                <span>EFECTIVO EN ESTACIÓN</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12">
                            <div class="card card-inverse bg-warning">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-left align-self-center">
                                                <i class="ft-check font-large-2 float-left"></i>
                                            </div>
                                            <div class="media-body text-right">
                                                <h3 class="card-text" style="font-size: 20px;">$ <?php echo number_format($pens-($sals+$deps),2); ?>
                                                <!--<?php echo $pens.'-('.$sals.'+'.$deps.')'; ?>--></h3>
                                                <span>EFECTIVO POR DEPOSITAR</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12">
                            <div class="card card-inverse bg-info">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-left align-self-center">
                                                <i class="ft-check-square font-large-2 float-left"></i>
                                            </div>
                                            <div class="media-body text-right">
                                                <h3 class="card-text" style="font-size: 20px;">$ <?php echo number_format($deps,2); ?></h3>
                                                <span>DEPOSITOS POR VERIFICAR</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12">
                            <div class="card card-inverse bg-success">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-left align-self-center">
                                                <i class="ft-shopping-bag font-large-2 float-left"></i>
                                            </div>
                                            <div class="media-body text-right">
                                                <h3 class="card-text" style="font-size: 20px;">$ <?php echo number_format($sals,2); ?></h3>
                                                <span>SALDO ACTUAL EN LA CUENTA</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                        <input type="hidden" name="cod_sucursal" id="cod_sucursal" value="<?php echo $codigo?>">
                                        <div class="box-tools pull-right">
                                            <?php if($botonatras==1){ ?>
                                            <a href="<?php echo base_url();?>Coe_cuenta" type="button" class="btn btn-sm btn-light mr-1 mb-1"><i class="fa fa-arrow-left" style="color:black;"></i></a>
                                        <?php }else{ ?>
                                            <button type="button" class="btn btn-success mr-1 mb-1" onclick="adddeposito(0)">Agregar deposito</button>
                                        <?php } ?>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-6" style="overflow: auto;">
                                                        <div>
                                                            <h5>Cortes de caja sin finalizar</h5>
                                                        </div>
                                                        <table class="table table-bordered tablevc" id="table_list1">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Fecha</th>
                                                                    <th>Efectivo</th>
                                                                    <th>Credito</th>
                                                                    <th>Comentario</th>
                                                                    <th>Usuario</th>
                                                                    <th>Estatus</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php echo $corTrs;?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-6" style="overflow: auto;padding: 0px;">
                                                        <div>
                                                            <h5>Últimos depositos</h5>
                                                        </div>
                                                        <table class="table table-bordered tablevc" id="table_list2">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Importe</th>
                                                                    <th>Saldo</th>
                                                                    <th>Forma</th>
                                                                    <th>Concepto</th>
                                                                    <th>Fecha</th>
                                                                    <th>Deposito</th>
                                                                    <th>Verifico</th>
                                                                    <th>Comprobante</th>
                                                                    <th>Estatus</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php echo $depTrs;?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->


<div class="modal fade text-left" id="modal_validar" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Agregar consignatario</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                
                <div class="row">
                    <div class="col-md-12">
                        <p><b>Fecha</b> <span class="m_fecha"></span></p>
                        <p><b>Importe</b> <span class="m_importe"></span></p>
                        <p><b>Saldo</b> <span class="m_saldo"></span></p>
                        <p><b>Forma</b> <span class="m_forma"></span></p>
                        <p><b>Concepto</b> <span class="m_concepto"></span></p>
                        <p><b>Deposito</b> <span class="m_deposito"></span></p>
                        <p><b>Comprobante</b> <span class="m_comprobante"></span></p>
                        <p><b>Usuario activo</b> <span class="m_usuario"></span></p>
                    </div>
                    <div class="col-md-12">
                        <label>Nombre de quién verifica</label>
                        <input type="text" name="dep_verifico" placeholder="Nombre de quién verifica" id="dep_verifico" class="form-control">
                    </div>
                    <div class="col-md-12">
                        <label>Observaciones</label>
                        <input type="text" name="dep_obs" placeholder="Comentarios" id="dep_obs" class="form-control">
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="saveverificar()">Verificar</button>
            </div>
        </div>
    </div>
</div>

<div id="modalform" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Agregar deposito</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_datos">
            <input type="hidden" id="rid" name="rid" value="0">
            <input type="hidden" id="sucursal" name="sucursal" value="<?php echo $codigo?>">
            <div class="row">
                <div class="col-md-12">
                    <label>Importe</label>
                    <input type="number" name="dep_importe" id="dep_importe" class="form-control" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Forma</label>
                    <input type="text" name="dep_forma" id="dep_forma" class="form-control" placeholder="Deposito, transferencia,etc" required>
                </div>
                <div class="col-md-6">
                    <label>Concepto</label>
                    <input type="text" name="dep_concepto" id="dep_concepto" class="form-control" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>Fecha</label>
                    <input type="datetime-local" name="dep_fecha" id="dep_fecha" class="form-control"  value="<?php echo date('Y-m-d H:i:s');?>" required>
                </div>
            </div>
            <div class="row" style=" display: none;"><!-- ya no se ocupara sera interno session-->
                <div class="col-md-12">
                    <label>Quien realizo el proceso</label>
                    <input type="text" name="dep_deposito" id="dep_deposito" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 file_dep_comprobante">
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>Comprobante</label>
                    <input type="file" name="dep_comprobante" id="dep_comprobante" class="form-control">
                </div>
            </div>
            
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary saveform" onclick="saveform()">Actualizar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>