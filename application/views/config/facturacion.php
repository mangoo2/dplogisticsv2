<style type="text/css">
    html body.layout-dark.layout-transparent .nav-tabs .nav-link.active{
        border: 1px solid white;
        border-radius: 11px 11px 0px 0px;
    }
</style>
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Facturación</div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Razon</label>
                            <select class="form-control" id="tiporason" onchange="tiporason()">
                                <option value="1" <?php if($ConfiguracionesId==1){ echo 'selected';}?>>COECSA</option>
                                <option value="2" <?php if($ConfiguracionesId==2){ echo 'selected';}?>>CADIPA</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        
                        <div class="col-xl-3 col-lg-6 col-12">
                            <div class="card card-inverse bg-primary">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-body text-left">
                                                <h3 class="card-text"><?php echo number_format($timbresvigentes,0,'.',',');?></h3>
                                                <span>Timbres</span>
                                            </div>
                                            <div class="media-right align-self-center">
                                                <i class="ft-file font-large-2 float-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12">
                            <div class="card card-inverse bg-success">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-body text-left">
                                                <h3 class="card-text"><?php echo number_format($facturastimbradas,0,'.',',');?></h3>
                                                <span>Facturadas</span>
                                            </div>
                                            <div class="media-right align-self-center">
                                                <i class="ft-file-text font-large-2 float-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12">
                            <div class="card card-inverse bg-danger">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-body text-left">
                                                <h3 class="card-text"><?php echo number_format($facturascanceladas,0,'.',',');?></h3>
                                                <span>Canceladas</span>
                                            </div>
                                            <div class="media-right align-self-center">
                                                <i class="ft-file-minus font-large-2 float-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12">
                            <div class="card card-inverse bg-success">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-body text-left">
                                                <h3 class="card-text"><?php echo number_format($complementotimbradas,0,'.',',');?></h3>
                                                <span>Complementos</span>
                                            </div>
                                            <div class="media-right align-self-center">
                                                <i class="ft-file-text font-large-2 float-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12">
                            <div class="card card-inverse bg-primary">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-body text-left">
                                                <h3 class="card-text" ><?php 
                              $consumidostimbre=$timbresvigentes-$facturasdisponibles;
                            echo number_format($consumidostimbre,0,'.',',');?></h3>
                                                <span>Consumidos</span>
                                            </div>
                                            <div class="media-right align-self-center">
                                                <i class="ft-file-text font-large-2 float-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12">
                            <div class="card card-inverse bg-primary">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-body text-left">
                                                <h3 class="card-text"><?php echo number_format($facturasdisponibles,0,'.',',');?></h3>
                                                <span>Disponibles</span>
                                            </div>
                                            <div class="media-right align-self-center">
                                                <i class="ft-file-text font-large-2 float-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12">
                            <div class="card card-inverse bg-primary">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-body text-left">
                                                <h3 class="card-text" style="font-size: 25px;"><?php echo $fechafin;?></h3>
                                                <span>Fin de vigencia</span>
                                            </div>
                                            <div class="media-right align-self-center">
                                                <i class="ft-calendar font-large-2 float-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul class="nav nav-tabs">
                                                        <li class="nav-item">
                                                            <a class="nav-link active" id="base-tab11" data-toggle="tab" aria-controls="tab1" href="#tab1" aria-expanded="true">Datos Generales</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" id="base-tab12" data-toggle="tab" aria-controls="tab2" href="#tab2" aria-expanded="false" onclick="loadtables()">Servicios</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" id="base-tab13" data-toggle="tab" aria-controls="tab3" href="#tab3" aria-expanded="false" onclick="loadtableu()">Unidades</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" id="base-tab14" data-toggle="tab" aria-controls="tab4" href="#tab4" aria-expanded="false">Archivos</a>
                                                        </li>
                                                        
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true" aria-labelledby="base-tab11">
                                                            <!----------------------->
                                                                <form class="row" id="formdatosgenerales"  >
                                                                  <input type="hidden" name="ConfiguracionesId"  value="<?php echo $ConfiguracionesId;?>">
                                                                  <div class="row">
                                                                    <div class="col-md-3 form-group">
                                                                      <label for="Nombre" class="active">RAZÓN SOCIAL</label>
                                                                      <input type="text" id="Nombre" name="Nombre" class="form-control" value="<?php echo $Nombre;?>">          
                                                                    </div>
                                                                    <div class="col-md-3 form-group">
                                                                      <label for="Curp" class="active">CURP</label>
                                                                      <input type="text" id="Curp" name="Curp" class="form-control" value="<?php echo $Curp;?>">          
                                                                    </div>
                                                                    <div class="col-md-3 form-group">
                                                                      <label for="Rfc" class="active">RFC</label>
                                                                      <input type="text" id="Rfc" name="Rfc" class="form-control" value="<?php echo $Rfc;?>" required>          
                                                                    </div>
                                                                    <div class="col-md-3 form-group">
                                                                      <label for="Rfc" class="active">RÉGIMEN FISCAL</label>
                                                                      <select id="Regimen" name="Regimen" class="form-control">';
                                                                        <option value="601" <?php if($Regimen==601){ echo 'selected';} ?> >601 General de Ley Personas Morales</option>
                                                                        <option value="603" <?php if($Regimen==603){ echo 'selected';} ?> >603 Personas Morales con Fines no Lucrativos</option>
                                                                        <option value="605" <?php if($Regimen==605){ echo 'selected';} ?> >605 Sueldos y Salarios e Ingresos Asimilados a Salarios</option>
                                                                        <option value="606" <?php if($Regimen==606){ echo 'selected';} ?> >606 Arrendamiento</option>
                                                                        <option value="607" <?php if($Regimen==607){ echo 'selected';} ?> >607 Régimen de Enajenación o Adquisición de Bienes</option>
                                                                        <option value="608" <?php if($Regimen==608){ echo 'selected';} ?> >608 Demás ingresos</option>
                                                                        <option value="609" <?php if($Regimen==609){ echo 'selected';} ?> >609 Consolidación</option>
                                                                        <option value="610" <?php if($Regimen==610){ echo 'selected';} ?> >610 Residentes en el Extranjero sin Establecimiento Permanente en México</option>
                                                                        <option value="611" <?php if($Regimen==611){ echo 'selected';} ?> >611 Ingresos por Dividendos (socios y accionistas)</option>
                                                                        <option value="612" <?php if($Regimen==612){ echo 'selected';} ?> >612 Personas Físicas con Actividades Empresariales y Profesionales</option>
                                                                        <option value="614" <?php if($Regimen==614){ echo 'selected';} ?> >614 Ingresos por intereses</option>
                                                                        <option value="615" <?php if($Regimen==615){ echo 'selected';} ?> >615 Régimen de los ingresos por obtención de premios</option>
                                                                        <option value="616" <?php if($Regimen==616){ echo 'selected';} ?> >616 Sin obligaciones fiscales</option>
                                                                        <option value="620" <?php if($Regimen==620){ echo 'selected';} ?> >620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos</option>
                                                                        <option value="621" <?php if($Regimen==621){ echo 'selected';} ?> >621 Incorporación Fiscal</option>
                                                                        <option value="622" <?php if($Regimen==622){ echo 'selected';} ?> >622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras</option>
                                                                        <option value="623" <?php if($Regimen==623){ echo 'selected';} ?> >623 Opcional para Grupos de Sociedades</option>
                                                                        <option value="624" <?php if($Regimen==624){ echo 'selected';} ?> >624 Coordinados</option>
                                                                        <option value="628" <?php if($Regimen==628){ echo 'selected';} ?> >628 Hidrocarburos</option>
                                                                        <option value="629" <?php if($Regimen==629){ echo 'selected';} ?> >629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales</option>
                                                                        <option value="630" <?php if($Regimen==630){ echo 'selected';} ?> >630 Enajenación de acciones en bolsa de valores</option>
                                                                                     
                                                                      </select>         
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-3 form-group">
                                                                      <label for="Calle" class="active">CALLE</label>
                                                                      <input type="text" id="Calle" name="Calle" class="form-control" value="<?php echo $Calle;?>">          
                                                                    </div>
                                                                    <div class="col-md-3 form-group">
                                                                      <label for="localidad" class="active">LOCALIDAD</label>
                                                                      <input type="text" id="localidad" name="localidad" class="form-control" value="<?php echo $localidad;?>">          
                                                                    </div>
                                                                    <div class="col-md-3 form-group">
                                                                      <label for="Noexterior" class="active">N° EXT.</label>
                                                                      <input type="text" id="Noexterior" name="Noexterior" class="form-control" value="<?php echo $Noexterior;?>">          
                                                                    </div>
                                                                    <div class="col-md-3 form-group">
                                                                      <label for="Nointerior" class="active">N° INT.</label>
                                                                      <input type="text" id="Nointerior" name="Nointerior" class="form-control" value="<?php echo $Nointerior;?>">          
                                                                    </div>
                                                                    <div class="col-md-3 form-group">
                                                                      <label for="Municipio" class="active">MUNICIPIO</label>
                                                                      <input type="text" id="Municipio" name="Municipio" class="form-control" value="<?php echo $Municipio;?>">          
                                                                    </div>
                                                                    <div class="col-md-3 form-group">
                                                                      <label for="Colonia" class="active">COLONIA</label>
                                                                      <input type="text" id="Colonia" name="Colonia" class="form-control" value="<?php echo $Colonia;?>">          
                                                                    </div>
                                                                    <div class="col-md-3 form-group">
                                                                      <label for="Estado" class="active">ESTADO</label>
                                                                      <input type="text" id="Estado" name="Estado" class="form-control" value="<?php echo $Estado;?>">          
                                                                    </div>
                                                                    <div class="col-md-3 form-group">
                                                                      <label for="CodigoPostal" class="active">CÓDIGO POSTAL</label>
                                                                      <input type="text" id="CodigoPostal" name="CodigoPostal" class="form-control" value="<?php echo $CodigoPostal;?>">          
                                                                    </div>
                                                                    <div class="col-md-3 form-group">
                                                                      <label for="PaisExpedicion" class="active">PAÍS</label>
                                                                      <input type="text" id="PaisExpedicion" name="PaisExpedicion" class="form-control" value="<?php echo $PaisExpedicion;?>">          
                                                                    </div>



                                                                  </div>
                                                                </form>
                                                                <div class="row">
                                                                  <div class="col s12">
                                                                    <button  type="button" class="btn btn-primary mr-1 mb-1" onclick="actualizardatos()" style="float: right;">Actualizar</button>
                                                                  </div>
                                                                </div>
                                                            <!----------------------->
                                                        </div>
                                                        <div class="tab-pane" id="tab2" aria-labelledby="base-tab12">
                                                            <table id="tabla_servicio"  class="table display" cellspacing="0">
                                                              <thead>
                                                                <tr>
                                                                  <th>Clave</th>
                                                                  <th>Concepto</th>
                                                                  <th></th>
                                                                </tr>
                                                              </thead>
                                                              <tbody>
                                                              </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="tab-pane" id="tab3" aria-labelledby="base-tab13">
                                                            <table id="tabla_unidades"  class="table display" cellspacing="0" style="width: 100%">
                                                              <thead>
                                                                <tr>
                                                                  <th>Clave</th>
                                                                  <th>Concepto</th>
                                                                  <th></th>
                                                                </tr>
                                                              </thead>
                                                              <tbody>
                                                              </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="tab-pane" id="tab4" aria-labelledby="base-tab13">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <input type="file" id="cerdigital" name="cerdigital" class="form-control" required >
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <input type="file" id="claveprivada" name="claveprivada" class="form-control" required>
                                                                    <div class="row" style="margin-top: 5px;">
                                                                      <label for="passprivada" class="col-md-4" style="color: black; font-size: 14px;">Contraseña de clave privada:</label>
                                                                      <div class="col-md-8">
                                                                        <input type="password" id="passprivada" name="passprivada" class="form-control" autocomplete="new-password" required>          
                                                                      </div>
                                                                    </div>
                                                                </div>
                                                                  <div class="col s12">
                                                                    <a  class="btn btn-primary mr-1 mb-1" onclick="instalararchivos()" >Instalar</a>
                                                                  </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->