    <?php 
        $RANGO=$this->session->userdata('sess_rango_nda');
    ?>
<style type="text/css">
    .form-control2{
        border-color: rgb(91 92 92) !important;
        color: #333333 !important;
    }
    .form-control2 option{
        background-color: #ffffff !important;
    }
    .btns-factura{
        min-width: 147px;
    }
    .btns-factura .btn{
        padding: 6px 11px;
    }
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
    .iframepdf iframe{
        width: 100%;
        border: 0;
        height: 305px;
    }
</style>
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Sucursales</div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a onclick="addsuc(0)" type="button" class="btn btn-success mr-1 mb-1">Agregar</a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    




                                                    
                                                    
                                                    
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 table-bordered tablevc" id="tabla_facturacion">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Sucursal</th>
                                                                    <th>Con serie</th>
                                                                    <th>Serie</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->


<div class="modal fade" id="modalsucursal" tabindex="-1" aria-labelledby="exampleModalSizeXl" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Sucursal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_datos">
                    <input type="hidden" name="id" id="id">
                    <div class="row">
                      <div class="col-md-12">
                        <label>Sucursal</label>
                        <input type="text" name="nombre" id="nombre" class="form-control" required>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label for="conserie">Con Serie</label>
                        <input type="checkbox" name="conserie" id="conserie" value="1" onchange="vconserie()">
                      </div>
                    </div> 
                    <div class="row">
                      <div class="col-md-12">
                        <label>Serie</label>
                        <input type="text" name="serie" id="serie" class="form-control" readonly>
                      </div>
                    </div>    
                </form>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal" style="background: #fcb520;color: white;">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn_save" style="background: #12264b;color: white;">Save</button>
            </div>
        </div>
    </div>
</div>