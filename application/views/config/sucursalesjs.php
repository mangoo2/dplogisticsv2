<script src="<?php echo base_url();?>app-assets/vendors/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>app-assets/css/plugins/form-validation.css">

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css">

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">

<script src="<?php echo base_url();?>public/js/config_suc.js?v=<?php echo date('YmdGis') ?>"></script>