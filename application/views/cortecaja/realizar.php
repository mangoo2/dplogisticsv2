<input type="hidden" id="num_docd" value="<?php echo $num_docd; ?>">
<div class="row cot_faltantes" style="display:none">
    <?php 
        foreach ($result_doc->result() as $itemf) {
            echo $itemf->rid.', ';
        }
    ?>
</div>
<style type="text/css">
    .tableinfo th, .tableinfo td{
        padding: 5px;
        font-size: 13px;
    }
</style>

            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Cortes de caja</div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Folios en efectivo</h4>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive" style="padding:0px;">
                                                        <table class="table m-0 table-bordered table-striped tableinfo">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Aeronilea</th>
                                                                    <th>Folio</th>
                                                                    <th>Guía</th>
                                                                    <th>cliente</th>
                                                                    <th>Clasificación</th>
                                                                    <th>Importe</th>
                                                                    <th>Descripción</th>
                                                                    <th>Ruta Origen</th>
                                                                    <th>Ruta Destino</th>
                                                                    <th>Estatus</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                    $row=1; 
                                                                    $total=0;
                                                                    foreach ($list_efe->result() as $item){ 
                                                                        $total=$total+$item->cot_importe; ?>
                                                                        <tr>
                                                                            <td><?php echo $row;?></td>
                                                                            <td><?php echo $item->aer_nom;?></td>
                                                                            <td><?php echo $item->cot_folio;?></td>
                                                                            <td><?php echo $item->cot_guia;?></td>
                                                                            <td><?php echo $item->cliente;?></td>
                                                                            <td><?php echo $item->clasifi;?></td>
                                                                            <td><?php echo $item->cot_importe;?></td>
                                                                            <td><?php echo $item->cot_descrip;?></td>
                                                                            <td><?php echo $item->rutao;?></td>
                                                                            <td><?php echo $item->rutad;?></td>
                                                                            <td>OK</td>
                                                                        </tr>
                                                                        <?php 
                                                                        $row++;
                                                                    }
                                                                    $rowefe=0;
                                                                    foreach ($list_efetar->result() as $item){ 
                                                                        if($item->cot_cantidad>0){
                                                                            $total=$total+$item->cot_cantidad; ?>
                                                                            <tr>
                                                                                <td><?php echo $row;?></td>
                                                                                <td><?php echo $item->aer_nom;?></td>
                                                                                <td><?php echo $item->cot_folio;?></td>
                                                                                <td><?php echo $item->cot_guia;?></td>
                                                                                <td><?php echo $item->cliente;?></td>
                                                                                <td><?php echo $item->clasifi;?></td>
                                                                                <td><?php echo $item->cot_cantidad;?></td>
                                                                                <td><?php echo $item->cot_descrip;?></td>
                                                                                <td><?php echo $item->rutao;?></td>
                                                                                <td><?php echo $item->rutad;?></td>
                                                                                <td>OK</td>
                                                                            </tr>
                                                                            <?php 
                                                                            $row++;
                                                                            $rowefe++;
                                                                        }
                                                                    }
                                                                     ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label>Folios: <?php echo $list_efe->num_rows()+$rowefe; ?></label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label>Importe total: <?php echo $total; ?> </label>
                                                    </div>
                                                </div>
                                               
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Folios en crédito</h4>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive" style="padding:0px;">
                                                        <table class="table m-0 table-bordered table-striped tableinfo">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Aeronilea</th>
                                                                    <th>Folio</th>
                                                                    <th>Guía</th>
                                                                    <th>cliente</th>
                                                                    <th>Clasificación</th>
                                                                    <th>Importe</th>
                                                                    <th>Descripción</th>
                                                                    <th>Ruta Origen</th>
                                                                    <th>Ruta Destino</th>
                                                                    <th>Estatus</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                    $row=1; 
                                                                    $total=0;
                                                                    foreach ($list_cre->result() as $item){ 
                                                                    $total=$total+$item->tar_tot;
                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $row;?></td>
                                                                        <td><?php echo $item->aer_nom;?></td>
                                                                        <td><?php echo $item->cot_folio;?></td>
                                                                        <td><?php echo $item->cot_guia;?></td>
                                                                        <td><?php echo $item->cliente;?></td>
                                                                        <td><?php echo $item->clasifi;?></td>
                                                                        <td><?php echo $item->tar_tot;?></td>
                                                                        <td><?php echo $item->cot_descrip;?></td>
                                                                        <td><?php echo $item->rutao;?></td>
                                                                        <td><?php echo $item->rutad;?></td>
                                                                        <td>OK</td>
                                                                    </tr>
                                                                <?php 
                                                                     $row++;
                                                                    } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label>Folios: <?php echo $list_cre->num_rows(); ?></label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label>Importe total: <?php echo $total; ?> </label>
                                                    </div>
                                                </div>
                                               
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Folios en tarjeta</h4>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive" style="padding:0px;">
                                                        <table class="table m-0 table-bordered table-striped tableinfo">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Aeronilea</th>
                                                                    <th>Folio</th>
                                                                    <th>Guía</th>
                                                                    <th>cliente</th>
                                                                    <th>Clasificación</th>
                                                                    <th>Importe</th>
                                                                    <th>Descripción</th>
                                                                    <th>Ruta Origen</th>
                                                                    <th>Ruta Destino</th>
                                                                    <th>Estatus</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                    $row=1; 
                                                                    $total=0;
                                                                    foreach ($list_tar->result() as $item){ 
                                                                        $total=$total+$item->tar_tot;
                                                                        ?>
                                                                        <tr>
                                                                            <td><?php echo $row;?></td>
                                                                            <td><?php echo $item->aer_nom;?></td>
                                                                            <td><?php echo $item->cot_folio;?></td>
                                                                            <td><?php echo $item->cot_guia;?></td>
                                                                            <td><?php echo $item->cliente;?></td>
                                                                            <td><?php echo $item->clasifi;?></td>
                                                                            <td><?php echo $item->tar_tot;?></td>
                                                                            <td><?php echo $item->cot_descrip;?></td>
                                                                            <td><?php echo $item->rutao;?></td>
                                                                            <td><?php echo $item->rutad;?></td>
                                                                            <td>OK</td>
                                                                        </tr>
                                                                        <?php 
                                                                     $row++;
                                                                    } 
                                                                    $rowtar=0;
                                                                    foreach ($list_efetar->result() as $item){ 
                                                                        if($item->banco_efectivo>0 or $item->banco_credito>0){
                                                                            $montoimporte=0;
                                                                            if($item->banco_efectivo>0){
                                                                                $montoimporte=$montoimporte+$item->banco_efectivo;
                                                                            }
                                                                            if($item->banco_credito>0){
                                                                                $montoimporte=$montoimporte+$item->banco_credito;
                                                                            }
                                                                            $total=$total+$montoimporte; ?>
                                                                            <tr>
                                                                                <td><?php echo $row;?></td>
                                                                                <td><?php echo $item->aer_nom;?></td>
                                                                                <td><?php echo $item->cot_folio;?></td>
                                                                                <td><?php echo $item->cot_guia;?></td>
                                                                                <td><?php echo $item->cliente;?></td>
                                                                                <td><?php echo $item->clasifi;?></td>
                                                                                <td><?php echo $montoimporte;?></td>
                                                                                <td><?php echo $item->cot_descrip;?></td>
                                                                                <td><?php echo $item->rutao;?></td>
                                                                                <td><?php echo $item->rutad;?></td>
                                                                                <td>OK</td>
                                                                            </tr>
                                                                            <?php 
                                                                            $row++;
                                                                            $rowtar++;
                                                                        }
                                                                    } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label>Folios: <?php echo $list_tar->num_rows()+$rowtar; ?></label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label>Importe total: <?php echo $total; ?> </label>
                                                    </div>
                                                </div>
                                               
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Notificación</h4>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive" style="padding:0px;">
                                                        <table class="table m-0 table-bordered table-striped tableinfo">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Aeronilea</th>
                                                                    <th>Folio</th>
                                                                    <th>Guía</th>
                                                                    <th>cliente</th>
                                                                    <th>Clasificación</th>
                                                                    <th>Importe</th>
                                                                    <th>Descripción</th>
                                                                    <th>Ruta Origen</th>
                                                                    <th>Ruta Destino</th>
                                                                    <th>Estatus</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                    $row=1; 
                                                                    $total=0;
                                                                    foreach ($list_nor->result() as $item){ 
                                                                    $total=$total+$item->tar_tot;
                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $row;?></td>
                                                                        <td><?php echo $item->aer_nom;?></td>
                                                                        <td><?php echo $item->cot_folio;?></td>
                                                                        <td><?php echo $item->cot_guia;?></td>
                                                                        <td><?php echo $item->cliente;?></td>
                                                                        <td><?php echo $item->clasifi;?></td>
                                                                        <td><?php echo $item->tar_tot;?></td>
                                                                        <td><?php echo $item->cot_descrip;?></td>
                                                                        <td><?php echo $item->rutao;?></td>
                                                                        <td><?php echo $item->rutad;?></td>
                                                                        <td>Inconcluso, será eliminado.</td>
                                                                    </tr>
                                                                <?php 
                                                                     $row++;
                                                                    } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label>Folios: <?php echo $list_nor->num_rows(); ?></label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label>Importe total: <?php echo $total; ?> </label>
                                                    </div>
                                                </div>
                                               
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Finalizar Corte</h4>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <label class="col-md-3">Observaciones:</label>
                                                    <div class="col-md-3">
                                                        <textarea id="cor_com" name="cor_com" class="form-control"></textarea>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <button type="button" class="btn btn-success mr-1 mb-1 cerrarcorte" onclick="cerrarcorte()">Cerrar Corte</button>
                                                    </div>
                                                </div>
                                               
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                   
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->