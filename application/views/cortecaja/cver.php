<style type="text/css">
    table td{
        font-size: 13px;
        padding: 5px !important;
    }
    #table_list2 th{
        font-size: 14px;
        padding: 10px !important;
    }
</style>
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Cortes de caja</div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Corte de caja</h4>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0 table-bordered" id="table_list">
                                                            <thead>
                                                                <tr>
                                                                    <th>Fecha</th>
                                                                    <th>Efectivo</th>
                                                                    <th>Crédito</th>
                                                                    <th>Tarjeta</th>
                                                                    <th>Comentario</th>
                                                                    <th>Admin</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($resul_cors->result() as $item) { ?>
                                                                <tr>
                                                                    <td><?php echo date("d/m/Y h:i:s A",strtotime($item->cor_fecha)); ?></td>
                                                                    <td><?php echo number_format($item->cor_efectivo,2,'.',','); ?></td>
                                                                    <td><?php echo number_format($item->cor_credito,2,'.',','); ?></td>
                                                                    <td><?php echo number_format($item->cor_tarjeta,2,'.',','); ?></td>
                                                                    <td><?php echo $item->cor_com; ?></td>
                                                                    <td><?php echo $item->cor_adm; ?></td>
                                                                </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Guías en el corte</h4>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-12" style="text-align: right;">
                                                        <a onclick="adjuntarcomprobantes()" type="button" class="btn btn-success mr-1 mb-1">Adjuntar comprobante</a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive" style="padding:0px;">
                                                        <table class="table m-0 table-bordered table-striped" id="table_list2">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Fecha</th>
                                                                    <th>Folio</th>
                                                                    <th>Guía</th>
                                                                    <th>Origen</th>
                                                                    <th>Destino</th>
                                                                    <th>Paqs</th>
                                                                    <th>Descripción</th>
                                                                    <th>Peso</th>
                                                                    <th>Volumen</th>
                                                                    <th>Tarifa</th>
                                                                    <th>Importe</th>
                                                                    <th>Admin</th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                    $row=1; 
                                                                    foreach ($resul_cors_guias->result() as $item) { ?>
                                                                    <tr>
                                                                        <td><?php echo $row;?>
                                                                            <input type="checkbox" name="id_guia" id="id_guia" value="<?php echo $item->rid?>" data-monto="<?php echo $item->importe;?>">
                                                                        </td>
                                                                        <td><?php echo date("d/m/Y h:i:s A",strtotime($item->fecha)); ?></td>
                                                                        <td><?php echo $item->folio; ?></td>
                                                                        <td><?php echo $item->guia; ?></td>
                                                                        <td><?php echo $item->origen; ?></td>
                                                                        <td><?php echo $item->destino; ?></td>
                                                                        <td><?php echo $item->paqs; ?></td>
                                                                        <td><?php echo $item->paq_des; ?></td>
                                                                        <td><?php echo $item->peso; ?></td>
                                                                        <td><?php echo $item->volumen; ?></td>
                                                                        <td><?php echo number_format($item->tarifa,2,'.',','); ?></td>
                                                                        <td><?php echo number_format($item->importe,2,'.',','); ?></td>
                                                                        <td><?php echo $item->admin; ?></td>
                                                                        <td><?php if($item->file_comprobante!=''){ ?>
                                                                                <a href="<?php echo base_url()?>comprobante/<?php echo $item->file_comprobante;?>" type="button" class="btn btn-info mr-1 mb-1" target="_blank"><i class="fa fa-file-o"></i></a>
                                                                            <?php } ?>
                                                                         </td>
                                                                    </tr>
                                                                <?php 
                                                                        $row++;
                                                                    } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->