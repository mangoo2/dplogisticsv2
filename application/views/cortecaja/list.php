            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">Cortes de caja</div>
                            <p class="content-sub-header mb-1"><?php echo $this->session->userdata('sess_suc_nombre');?></p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <!--<h4 class="card-title">Default Table</h4>-->
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <div class="row">
                                                    <div class="col-md-12" style="text-align: right;">
                                                        <a href="<?php echo base_url();?>Corte_caja/realizar" type="button" class="btn btn-success mr-1 mb-1">Realizar Corte</a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 form-group">
                                                        <label>Fecha inicio</label>
                                                        <input type="date" id="fecha_i" class="form-control" onchange="loadtable()">
                                                    </div>
                                                    <div class="col-md-4 form-group">
                                                        <label>Fecha termino</label>
                                                        <input type="date" id="fecha_t" class="form-control" onchange="loadtable()">
                                                    </div>
                                                    <div class="col-md-4" style="text-align: end;">
                                                        <button  type="button" class="btn btn-secondary mr-1 mb-1" onclick="agregarcomprobanteall()">Agregar Comprobante</button>
                                                    </div>
                                                    <div class="col-md-12 table-responsive">
                                                        <table class="table m-0" id="table_list">
                                                            <thead>
                                                                <tr>
                                                                    <th></th>
                                                                    <th>#</th>
                                                                    <th>Fecha</th>
                                                                    <th>Efectivo</th>
                                                                    <th>Crédito</th>
                                                                    <th>Comentario</th>
                                                                    <th>Admin</th>
                                                                    <th></th>
                                                                </tr></thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->

<style type="text/css">
    .simple-lightbox{
        z-index: 1000002 !important;
    }
    .comprobantesimg{
        width: 200px;
    }
    .comprobantespdf{
        width: 150px;
    }
</style>

<div class="modal fade text-left" id="modal_comprobante" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Comprobante</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
               <div class="row">
                   <div class="col-md-12 datasimages">
                       
                   </div>
               </div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" id="idcorte">
                        <input type="file" name="comprobante" id="comprobante">
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade text-left" id="modal_comprobante_all" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Comprobante</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="file" name="comprobanteall" id="comprobanteall">
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>