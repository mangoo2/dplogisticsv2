<script src="<?php echo base_url();?>public/plugins/simplelightbox-master/dist/simple-lightbox.js?v2.10.3"></script>
<link rel="stylesheet" href="<?php echo base_url();?>public/plugins/simplelightbox-master/dist/simple-lightbox.css?v2.10.3">


<script src="<?php echo base_url();?>public/plugins/fileinput/fileinput.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>public/plugins/fileinput/fileinput.min.css">
<script type="text/javascript">
	var base_url = $('#base_url').val();
	function adjuntarcomprobantes() {
		//
		var guiaslis = $("#table_list2 tbody > tr");
    	var DATAa  = [];
    	var num_guias_selected=0;
        guiaslis.each(function(){  
        	if ($(this).find("input[id*='id_guia']").is(':checked')) {
        		num_guias_selected++;
        		item = {};                    
            	item ["guiasIds"]  = $(this).find("input[id*='id_guia']").val();
            	item ["monto"]  = $(this).find("input[id*='id_guia']").data('monto');
                
            	DATAa.push(item);
        	}       
            
        });
        aInfoa   = JSON.stringify(DATAa);
        if(num_guias_selected>0){
        	$('#modal_comprobante').modal();
        	$("#comprobante").fileinput({
	            showCaption: false,
	            
	            //rtl: true,
	            allowedFileExtensions: ["png","jpg","jpeg","bmp","pdf"],
	            browseLabel: 'Seleccionar Imagen',
	            uploadUrl: base_url+'Corte_caja/comprobantei',
	            
	            //initialPreview: [DATAimeges],
	            //initialPreviewAsData: true,
	            
	            maxFilePreviewSize: 5000,
	            previewFileIconSettings: {
	                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
	                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
	                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
	                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
	                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
	                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
	            },
	            uploadExtraData: function (previewId, index) {
		            var info = {
		            			guias:aInfoa

		            		};
		            return info;
		        }
	    	}).on('filebatchuploadcomplete', function(event, files, extra) {
	          //location.reload();
	    		//toastr.success('Se cargo Correctamente','Hecho!');
	    		setTimeout(function(){ 
                 	location.reload();
            	}, 2000);
	        }).on('filebatchuploadsuccess', function(event, files, extra) {
	          //location.reload();
	        	toastr.success('Se cargo Correctamente','Hecho!');
	        	setTimeout(function(){ 
                 	location.reload();
            	}, 2000);
	        });
        }else{
        	toastr["error"]("Selecciona una o mas guias del listado", "Advertencia")
        }
		
	}
</script>
<style type="text/css">
    .simple-lightbox{
        z-index: 1000002 !important;
    }
    .comprobantesimg{
        width: 200px;
    }
    .comprobantespdf{
        width: 150px;
    }
</style>
<div class="modal fade text-left" id="modal_comprobante" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Comprobante</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
            	<div class="row datasimages">
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="file" name="comprobante" id="comprobante">
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>