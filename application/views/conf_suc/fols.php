<style type="text/css">
    .table-responsive{
        padding: 0px;
    }
    .divbuttonstable{
        width: 135px;
    }
    .tablevc th,.tablevc td{
        font-size: 12px;
        text-align: center;
        padding: 8px;
    }
</style>  
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-header">COECSA</div>
                            <p class="content-sub-header mb-1">Sucursales</p>
                        </div>
                    </div>
                    <!--Basic Table Starts-->
                    <section id="simple-table">
                        <div class="row">
                            <div class="col-12" style="padding:0px;">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!--------------->
                                                <form id="form_datos" method="post" class="form" action="<?php echo base_url()?>Conf_suc/fols_add">
                                                    <input type="hidden" name="sucursal" value="<?php echo $codigo?>">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            Agregar Folio
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="basic-addon1"><?php echo $resultsuc->suc_nombre?></span>
                                                                </div>
                                                                <input type="text" class="form-control" placeholder="Addon to Left" aria-describedby="basic-addon1" value="<?php echo $resultsuc->suc_ubicacion?>" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="input-group-text" id="basic-addon1">Agregar Folio</span>
                                                                <input type="text" class="form-control" name="folio" id="folio" placeholder="Folio" required>
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <a class="btn btn-success" onclick="saveform()">Agregar</a>
                                                            
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>Folio</th>
                                                                    <th>Número</th>
                                                                    <th>Estatus</th>
                                                                </tr>
                                                                <tbody>
                                                                    <?php foreach ($lisfolis->result() as $item) { ?>
                                                                        <tr>
                                                                            <td><?php echo $item->folio;?></td>
                                                                            <td><?php echo $item->fol_num;?></td>
                                                                            <td><?php 
                                                                                if($item->fol_est=='act'){
                                                                                    echo '<span class="badge badge-success mb-1 mr-2">Folio Activo</span>';
                                                                                }else{
                                                                                    echo '<span class="badge badge-info mb-1 mr-2">Folio Cerrado</span>';
                                                                                }
                                                                            ?></td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                </div>
                                                
                                            <!--------------->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--Basic Table Ends-->
                </div>
            </div>
            <!-- END : End Main Content-->



