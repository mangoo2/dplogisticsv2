var base_url = $('#base_url').val();
var table;
$(document).ready(function($) {
	table = $('#tabla_facturacion').DataTable();
	loadtable();
    $('#btn_save').click(function(event) {
        var formulario =$('#form_datos');
        var validform=formulario.valid();
        if(validform){
            var datos = formulario.serialize();
            $.ajax({
                type:'POST',
                url: base_url+'index.php/Config/insertaactualizasucursal',
                data: datos,
                success:function(data){
                    toastr["success"]("Información Guardada", "Hecho!");
                    loadtable();
                    $('#modalsucursal').modal('hide');
                },
                error: function(response){
                    toastr["error"]("No se pudo procesar", "Error"); 
                         
                }
              });
        }
    });
});
function loadtable(){
	table.destroy();
	table = $('#tabla_facturacion').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Config/getlistsucursales",
            type: "post",
            "data": {
                'cliente':0
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "nombre"},
            {"data": "conserie",
                render:function(data,type,row){
                    var html='';
                        if(row.conserie>0){
                            html=row.conserie;
                        }
                    return html;
                }
            },
            {"data": "serie"},
            {"data": null,
                 render:function(data,type,row){

                    $('.tooltipped').tooltip();
                    var html='';
                    	html+='<a onclick="addsuc('+row.id+')"  class="btn btn-info mr-1 mb-1 edit_'+row.id+'" style="padding: 6px 15px;" data-nombre="'+row.nombre+'" data-conserie="'+row.conserie+'" data-serie="'+row.serie+'" title="Solicitar Edicion" type="button"><i class="fa fa-edit"></i></a>';
                    	html+='<button type="button" class="btn btn-danger mr-1 mb-1 idcotiza_51033" data-idcotiza="09012023165701" onclick="deletesuc('+row.id+')"><i class="fa fa-trash-o"></i></button>';
                    
                    return html;
                }
            }
            
          
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]]
        
    }).on('draw',function(){
       
    });
}
function deletesuc(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la eliminacion de la sucursal?',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function (){
                
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Config/sucursalesdelete",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        toastr["success"]("sucursal eliminado", "Hecho"); 
                        loadtable();
                    },
                    error: function(response){
                        toastr["error"]("No se pudo procesar", "Error"); 
                         
                    }
                });
                
            },
            cancelar: function () {
                
            }
        }
    });
}
function addsuc(id){
    $('#modalsucursal').modal();
    if(id>0){
        $('#btn_save').html('Actualizar');
        $('#id').val(id);
        var sucu=$('.edit_'+id).data('nombre');
        $('#nombre').val(sucu);
        var sc=$('.edit_'+id).data('conserie');
        var ser=$('.edit_'+id).data('serie');
        if(sc==1){
            $( "#conserie" ).prop( "checked", true ); 
            $( "#serie" ).prop( "readonly", false); 
            $( "#serie" ).val(ser);
        }else{
            $( "#conserie" ).prop( "checked", false);
            $( "#serie" ).prop( "readonly", true); 
            $( "#serie" ).val('');
        }

    }else{
        $('#id').val(0);
        $('#nombre').val('');
        $( "#conserie" ).prop( "checked", false ); 
        $( "#serie" ).prop( "readonly", true ); 
        $('#btn_save').html('Guardar');
        $( "#serie" ).val('');
    }

}
function vconserie(){
    setTimeout(function(){ 
        if($( "#conserie" ).is(':checked')){
            $( "#serie" ).prop( "readonly", false); 
        }else{
            $( "#serie" ).prop( "readonly", true); 
            $( "#serie" ).val('');
        }
    }, 1000);
}