var base_url = $('#base_url').val();
var c_Pais = $('#c_Pais').val();
var c_Estado = $('#c_Estado').val();
var estado_aux = $('#estado_aux').val();
var localidad_aux = $('#localidad_aux').val();
var municipio_aux = $('#municipio_aux').val();


var c_Estado = $('#c_Estado').val();

var rid;
$(document).ready(function($){
	get_pais();
	get_colonia();
    if(estado_aux!=''){
        get_estado(c_Pais);
    }

    if(localidad_aux!=''){
        get_localidad(c_Estado);
    }

    if(municipio_aux!=''){
        get_municipio(c_Estado);
    }
    
    get_dpl();
});

function guardar_registro(){
	var formulario =$('#form_registro');
	var validform=formulario.valid();
	if(validform){
		$('.btn_registro').attr('disabled',true);
		var datos = formulario.serialize();
		$.ajax({
            type:'POST',
            url: base_url+'index.php/Domicilio_origen/insert_registro',
            data: datos,
            success:function(data){
                rid=parseInt(data);
                if(data>=1){
                    Swal.fire(
					  'Hecho!',
					  'Guardada Correctamente',
					  'success'
					);
                    setTimeout(function(){ 
                      javascript:history.back();
                    }, 3000);
                }else{
                    Swal.fire(
		              '¡Atención!',
		              'No se pudo guardar la información',
		              'error'
		            ); 
                }
            }
        });
	}
}

function get_pais(){
	$('#pais').select2({
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar:',
        ajax: {
            url: base_url+'Domicilio_origen/search_pais',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.descripcion,
                        c_Pais: element.c_Pais,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        get_estado(data.c_Pais);
    });
}

function get_estado(pais){
	$('#estado').select2({
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar:',
        ajax: {
            url: base_url+'Domicilio_origen/search_estado',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    pais:pais, 
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.descripcion,
                        c_Estado: element.c_Estado,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        get_localidad(data.c_Estado);
        get_municipio(data.c_Estado);
    });
}

function get_colonia(){
	$('#colonia').select2({
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar:',
        ajax: {
            url: base_url+'Domicilio_origen/search_colonia',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: '('+element.c_CodigoPostal+')'+element.nombre,
                        cp:element.c_CodigoPostal
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
            var data = e.params.data;
            $('#codigo_postal').val(data.cp);
        });
}



function get_localidad(estado){
	$('#localidad').select2({
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar:',
        ajax: {
            url: base_url+'Domicilio_origen/search_localidad',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    estado:estado,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.descripcion+'('+element.c_Estado+')',
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}

function get_municipio(estado){
	$('#municipio').select2({
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar:',
        ajax: {
            url: base_url+'Domicilio_origen/search_municipio',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    estado:estado,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.descripcion+'('+element.c_Estado+')',
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}

function get_dpl(){
    $('#dpl').select2({
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar:',
        ajax: {
            url: base_url+'Origen/search_dpl',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.ruta,
                        text: element.rut_cla+' -> '+element.rut_nom,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}
