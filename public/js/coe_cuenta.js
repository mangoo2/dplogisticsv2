var base_url =$('#base_url').val();
var v_rid;
var v_sucursal=$('#sucursal').val();
var v_dep_importe;
var v_dep_forma;
var v_dep_concepto;
var v_dep_fecha;
var v_dep_deposito;

$(document).ready(function($) {
    

    $("#dep_comprobante").fileinput({
            showCaption: false,
            dropZoneEnabled: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["png","jpg","jpeg","bmp","pdf"],
            browseLabel: 'Seleccionar documento',
            uploadUrl: base_url+'Coe_cuenta/inserupdate',
            inputGroupClass: "input-group-sm",
            maxFilePreviewSize: 5000,
            uploadExtraData: function (previewId, index) {
                var info = {
                            rid:v_rid,
                            sucursall:v_sucursal,
                            dep_importe:v_dep_importe,
                            dep_forma:v_dep_forma,
                            dep_concepto:v_dep_concepto,
                            dep_fecha:v_dep_fecha,
                            dep_deposito:v_dep_deposito

                        };
                return info;
            }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
        }).on('filebatchuploadsuccess', function(event, files, extra) {
          location.reload();
          //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        });  
});
function finalizar(rid){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma de finalizar',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Coe_cuenta/cor_dep",
                        data: {
                            rid:rid
                        },
                        success: function (response){
                          location.reload();

                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
var verificar_rid;
function verificar(rid){
	verificar_rid=rid;
	$('#modal_validar').modal({backdrop: 'static', keyboard: false});
    var fech=$('.verificar_'+rid).data('fecha');
    $('.m_fecha').html(fech);
    var importe=$('.verificar_'+rid).data('importe');
    $('.m_importe').html(importe);
    var saldo=$('.verificar_'+rid).data('saldo');
    $('.m_saldo').html(saldo);
    var forma=$('.verificar_'+rid).data('forma');
    $('.m_forma').html(forma);
    var concepto=$('.verificar_'+rid).data('concepto');
    $('.m_concepto').html(concepto);
    var deposito=$('.verificar_'+rid).data('deposito');
    $('.m_deposito').html(deposito);
    var comprobante=$('.verificar_'+rid).data('comprobante');
    $('.m_comprobante').html('<a href="'+comprobante+'" target="_blank">Ver Comprobante</a>');
    var usuario=$('.verificar_'+rid).data('usuario');
    $('.m_usuario').html(usuario);
}
function saveverificar(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Coe_cuenta/dep_gua",
        data: {
            rid:verificar_rid,
            dep_verifico:$('#dep_verifico').val(),
            dep_obs:$('#dep_obs').val()
        },
        success: function (response){
            $('#modal_validar').modal('hide');
            toastr["success"]("Se registro la verificacion", "Hecho"); 
            setTimeout(function(){ 
                location.reload(); 
            }, 3000);

        },
        error: function(response){
            toastr["error"]("No se pudo procesar", "Error"); 
             
        }
    });
}
function finalizar(rid){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma la cancelación de deposito.?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Coe_cuenta/dep_can",
                        data: {
                            rid:rid
                        },
                        success: function (response){
                            toastr["success"]("Se realizo la cancelación correctamente", "Hecho"); 
                            setTimeout(function(){ 
                                location.reload(); 
                            }, 3000);

                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function adddeposito(id){
    var codigo=$('#cod_sucursal').val()
    $('#modalform').modal({backdrop: 'static', keyboard: false});

    if(id==0){
        $('#sucursal').val(codigo);    
        $('#rid').val(id);
        $('.saveform').html('Agregar');
        $('#sucursal').val(codigo);
        $('#dep_importe').val('');
        $('#dep_forma').val('');
        $('#dep_concepto').val('');
        $('#dep_deposito').val('');
        $('#dep_comprobante').fileinput('clear');

    }else{
        $('.saveform').html('Actualizar');
        $('#rid').val(id);
        var data_suc=$('.deposito_'+id).data('sucursal');
        var data_imp=$('.deposito_'+id).data('importe');
        var data_form=$('.deposito_'+id).data('forma');
        var data_conc=$('.deposito_'+id).data('concepto');
        var data_fech=$('.deposito_'+id).data('fecha');
        var data_dep=$('.deposito_'+id).data('deposito');
        $('#sucursal').val(data_suc);
        $('#dep_importe').val(data_imp);
        $('#dep_forma').val(data_form);
        $('#dep_concepto').val(data_conc);
        $('#dep_fecha').val(data_fech);
        $('#dep_deposito').val(data_dep);
        $('#sucursal').val(codigo);
    }
    
}
function saveform() {
        v_rid =$('#rid').val();
        v_sucursal =$('#sucursal').val();
        v_dep_importe =$('#dep_importe').val();
        v_dep_forma =$('#dep_forma').val();
        v_dep_concepto =$('#dep_concepto').val();
        v_dep_fecha =$('#dep_fecha').val();
        v_dep_deposito =$('#dep_deposito').val();

        var varform=$('#form_datos');
        if(varform.valid()){
             $('#dep_comprobante').fileinput('upload'); 
              
        }
}
function deletedeposito(rid){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma la Eliminación del deposito?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Coe_cuenta/deletedeposito",
                        data: {
                            rid:rid
                        },
                        success: function (response){
                            toastr["success"]("Se realizo la Eliminación correctamente", "Hecho"); 
                            setTimeout(function(){ 
                                location.reload(); 
                            }, 3000);

                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}