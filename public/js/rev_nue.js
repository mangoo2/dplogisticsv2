var base_url = $('#base_url').val();
var rid;
$(document).ready(function($) {
	//obtenerinfo();
	$('#saveform').click(function(event) {
		$( "#saveform" ).prop( "disabled", true );
        setTimeout(function(){ 
             $("#saveform" ).prop( "disabled", false );
        }, 5000);

		var formulario =$('#form_datos');
		var validform=formulario.valid();
		if(validform){
				var table_uni_equi = $("#table_unidades_equipo tbody > tr");
            	var DATAtsu  = [];
		        table_uni_equi.each(function(){  
		        	if ($(this).find("input[id*='rev_unis']").is(':checked')) {
		        		item = {};                    
		            	item  = $(this).find("input[id*='rev_unis']").val();
		            	DATAtsu.push(item);
		        	}       
		        });
		        aInfotsu   = JSON.stringify(DATAtsu);
			//==================================
			//==================================
			//==================================
			var datos = formulario.serialize()+'&uni_equi_a='+aInfotsu;
			$.ajax({
                  type:'POST',
                  url: base_url+'index.php/BRDU/rev_gua',
                  data: datos,
                  success:function(data){
                      
                    toastr["success"]("Información Guardada", "Hecho!")      
                    setTimeout(function(){ 
                    		var codigo=data;
                        window.location.href = base_url+"index.php/BRDU/rev_mod/"+codigo; 
                        //javascript:history.back();
                    }, 3000);
                      
                      
                  },
                  error: function(response){
                  	console.log(response);
                  	toastr["error"]("No se pudo guardar la información", "Error");
                  }

              });
		}
	});

});