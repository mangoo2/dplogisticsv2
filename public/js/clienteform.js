var base_url = $('#base_url').val();
var rid;
var rid_cli=$('#rid').val();
$(document).ready(function($) {
	$('#okform').click(function(event) {
		var formulario =$('#form_cliente');
		var validform=formulario.valid();
		if(validform){
			var datos = formulario.serialize();
			$.ajax({
                  type:'POST',
                  url: base_url+'index.php/Clientes/insertaActualizaClientes',
                  data: datos,
                  success:function(data){
                      // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                      rid=parseInt(data);
                      $('#rid').val(rid);
                      rid_cli=rid;
                      if(data>=1){
                          toastr["success"]("Información Guardada", "Hecho!");
                          var imges=$('#cliioimg').val();
                          if(imges==''){
                            setTimeout(function(){ 
                              if($('#viewmodal').val()==1){
                                window.close();
                              }
                            }, 2000);
                          }else{
                            setTimeout(function(){ 
                            $('#cliioimg').fileinput('upload');
                              
                            }, 1000);
                            setTimeout(function(){ 
                              if($('#viewmodal').val()==1){
                                window.close();
                              }
                            }, 2000);
                          }
                          
                          setTimeout(function(){ 
                              //window.location.href = base_url+"index.php/clientes"; 
                              javascript:history.back();
                          }, 3000);
                      }
                      // En caso contrario, se notifica
                      else{
                          toastr["error"]("No se pudo guardar la información", "Error")

                      }
                  }
              });
		}
	});
	$("#cliioimg").fileinput({
            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["png","jpg","jpeg","bmp"],
            browseLabel: 'Seleccionar Imagen',
            uploadUrl: base_url+'Clientes/imagencli',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            },
            uploadExtraData: function (previewId, index) {
	            var info = {
	            			id:rid_cli

	            		};
	            return info;
	        }
    })
  $('.validadexistencia').change(function(event) {
     var nombre=$('#cli_nombre').val();
     var cli_paterno = $('#cli_paterno').val();
     var cli_materno = $('#cli_materno').val();
     var rid = $('#rid').val();
     if(rid>0){

     }else{
        $.ajax({
            type:'POST',
            url: base_url+'index.php/Clientes/validadexistencia',
            data: {
              nombre:nombre,
              cli_paterno:cli_paterno,
              cli_materno:cli_materno,
            },
            success:function(data){
              var numrows=parseInt(data);
              if(numrows>0){
                toastr["warning"]("Ya existe un cliente con este nombre",)
              }
                
            }
        });
     }
     
  });
});
function obtenerdatoscliente(id){
  $.ajax({
        type:'POST',
        url: base_url+'index.php/Clientes/datoscli',
        data: {
          idcl:id
        },
        success:function(data){
            var array = $.parseJSON(data);
            console.log(array);
            if(array.length==0){
              $('#rid').val(0);              
              
            }else{
              
              $('#RegimenFiscalReceptor').val(array.RegimenFiscalReceptor);
              $('#cli_calle').val(array.cli_calle);
              $('#cli_calles').val(array.cli_calles);
              $('#cli_celular').val(array.cli_celular);
              $('#cli_colonia').val(array.cli_colonia);
              $('#cli_com').val(array.cli_com);
              $('#cli_cp').val(array.cli_cp);
              $('#cli_cum').val(array.cli_cum);
              $('#cli_des').val(array.cli_des);
              $('#cli_email').val(array.cli_email);
              $('#cli_entidad').val(array.cli_entidad);
              $('#cli_io').val(array.cli_io);
              if(array.cli_ioimg!=''){
                $('.file-drop-zone-title').hide();
                $('.file-preview-thumbnails').html('<img src="'+base_url+'_files/_clis/'+array.sucursal+'/'+array.cliente+'/'+array.cli_ioimg+'" style="width: 169px;">')
              }
              $('#cli_ioimg').val(array.cli_ioimg);
              $('#cli_materno').val(array.cli_materno);
              $('#cli_municipio').val(array.cli_municipio);
              $('#cli_nombre').val(array.cli_nombre);
              $('#cli_numext').val(array.cli_numext);
              $('#cli_numint').val(array.cli_numint);
              $('#cli_paterno').val(array.cli_paterno);
              $('#cli_rfc').val(array.cli_rfc);
              $('#cli_telefono').val(array.cli_telefono);
              $('#nombrefiscal').val(array.nombrefiscal);
              $('#rid').val(array.rid);

              $('#tarifa_baja').val(array.tarifa_baja);
              $('#tarifa_especial').val(array.tarifa_especial);
            }
        }
    });
}