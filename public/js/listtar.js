var base_url=$('#base_url').val();
$(document).ready(function($) {
	
});
function editar(id){
        if(id>0){
        $('#exampleModalLiveLabel').html('Editar clasificación');
        $('.saveform').html('Actualizar');
        $('#modalform').modal({backdrop: 'static', keyboard: false});

        $('#rid').val(id);

        var rut_cla = $('.edit_form_'+id).data('rut_cla');
        $('#rut_cla').val(rut_cla);

        var rut_nom = $('.edit_form_'+id).data('rut_nom');
        $('#rut_nom').val(rut_nom);



        var cla_nom = $('.edit_form_'+id).data('cla_nom');
        $('#cla_nom').val(cla_nom);
		var cla_des = $('.edit_form_'+id).data('cla_des');
		$('#cla_des').val(cla_des);
		var cla_km = $('.edit_form_'+id).data('cla_km');
		$('#cla_km').val(cla_km);
		var cla_cm = $('.edit_form_'+id).data('cla_cm');
		$('#cla_cm').val(cla_cm);
		var cla_ce = $('.edit_form_'+id).data('cla_ce');
		$('#cla_ce').val(cla_ce);
		var cla_cen = $('.edit_form_'+id).data('cla_cen');
		$('#cla_cen').val(cla_cen);
		var cla_cf = $('.edit_form_'+id).data('cla_cf');
		$('#cla_cf').val(cla_cf);
		var cla_cfn = $('.edit_form_'+id).data('cla_cfn');
		$('#cla_cfn').val(cla_cfn);
		var cla_fer = $('.edit_form_'+id).data('cla_fer');
		$('#cla_fer').val(cla_fer);
		var cla_fern = $('.edit_form_'+id).data('cla_fern');
		$('#cla_fern').val(cla_fern);
		var cla_cpc = $('.edit_form_'+id).data('cla_cpc');
		$('#cla_cpc').val(cla_cpc);
		var cla_cpcn = $('.edit_form_'+id).data('cla_cpcn');
		$('#cla_cpcn').val(cla_cpcn);
        var cla_tip = $('.edit_form_'+id).data('cla_tip');
        $('#cla_tip').val(cla_tip);

        }else{
            $('#exampleModalLiveLabel').html('Agregar clasificación');
            $('.saveform').html('Agregar');
            $('#modalform').modal({backdrop: 'static', keyboard: false});
            $('#form_datos')[0].reset();

            $('#cla_cen').val('Cargo extra');
			$('#cla_cfn').val('Cargo Fijo');
			$('#cla_fern').val('Flete / Empaque - Recolección');
			$('#cla_cpcn').val('Cargo por Combustible');
        }
}
function saveform() {
        var varform=$('#form_datos');
        if(varform.valid()){
            var datos = varform.serialize();
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Conf_cla/inserupdate",
                data: datos,
                success: function (response){
                  $('#modalform').modal('hide');
                  
                  toastr["success"]("Informacion Actualizada", "Hecho"); 
                  
                  setTimeout(function(){
                    location.reload();
                  }, 2000);

                },
                error: function(response){
                    toastr["error"]("Algo salio mal", "Error"); 
                     
                }
              });                
        }
}
function deleteaer(id){
        $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma borrar la Clasificación',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Conf_cla/delete",
                        data: {
                            rid:id
                        },
                        success: function (response){
                            toastr["success"]("Aerolinea eliminada", "Hecho"); 
                            location.reload();                         
                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
    }