var base_url=$('#base_url').val();
var sucursal=$('#sucursal').val();
var eid = $('#eid').val();
var equipo=$('#equipo').val();
var rid;
var evi_nombre;
$(document).ready(function($) {
	$("#evi_archivo").fileinput({
            showCaption: false,
            dropZoneEnabled: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["png","jpg","jpeg","bmp","pdf"],
            browseLabel: 'Seleccionar documento',
            uploadUrl: base_url+'Coe_pam/equ_eviagr',
            inputGroupClass: "input-group-sm",
            maxFilePreviewSize: 5000,
            uploadExtraData: function (previewId, index) {
                var info = {
                            eid:eid,
                            rid:rid,
							evi_nombre:evi_nombre,
							sucursal:sucursal,
							equipo:equipo
                        };
                return info;
            }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
    });
});
function edit_equ_evis(id){
	rid=id;
	$('#mdlEviNue').modal({backdrop: 'static', keyboard: false});
	if(id>0){

		$('#mdlEviNue_Label').html('Modificar evidencia');


		var evinombre = $('.edit_pam_u'+id).data('evinombre');
		var eviarchivo = $('.edit_pam_u'+id).data('eviarchivo');

		$('.mdlEviNue_info').html('Archivo de la evidencia. Al subir otro archivo se reemplazará la actual: '+eviarchivo);

		$('#rid_eve').val(id);
		$('#evi_nombre').val(evinombre);

		$('#evi_archivo').prop('required', false);
	}else{
		$('#mdlEviNue_Label').html('Agregar evidencia');
		$('.mdlEviNue_info').html('Archivo de la evidencia');
		$('#evi_archivo').prop('required', true);
		$("#frmNue_ev")[0].reset();
	}
}
function savemdlEviNue(){
	var form = $('#frmNue_ev');
	if(form.valid()){
		evi_nombre=$('#evi_nombre').val();

		$('#evi_archivo').fileinput('upload'); 
	}else{
		toastr.error('Es necesario completar los campos requeridos');
	}
}
function delete_pam_u(id){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma la eliminación del registro',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Coe_pam/equ_evibor",
                        data: {
                            rid:id
                        },
                        success: function (response){
                            toastr.success('Registro eliminado');
                            setTimeout(function(){ 
                                location.reload();
                            }, 1000);

                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}