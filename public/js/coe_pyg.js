var base_url=$('#base_url').val();
var rid=0;
var pre_importe=0;
var pre_forma='';
var pre_concepto='';
var pre_fecha='';
var pre_asigno='';
var sucursal=$('#sucursal').val();
var pre_tipo_v;
var sprid=0;
var egrest='';
$(document).ready(function($) {
	$("#pre_comprobante").fileinput({
            showCaption: false,
            dropZoneEnabled: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["png","jpg","jpeg","bmp","pdf"],
            browseLabel: 'Seleccionar documento',
            uploadUrl: base_url+'Coe_pyg/inserupdate_comprobante',
            inputGroupClass: "input-group-sm",
            maxFilePreviewSize: 5000,
            uploadExtraData: function (previewId, index) {
                var info = {
                            rid:rid,
							pre_importe:pre_importe,
							pre_forma:pre_forma,
							pre_concepto:pre_concepto,
							pre_fecha:pre_fecha,
							pre_asigno:pre_asigno,
							sucursal:sucursal,
							pre_tipo:pre_tipo_v

                        };
                return info;
            }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
        }).on('filebatchuploadsuccess', function(event, files, extra) {
          location.reload();
          //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        }); 

    $("#egr_compfin").fileinput({
            showCaption: false,
            dropZoneEnabled: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["png","jpg","jpeg","bmp","pdf","zip","rar"],
            browseLabel: 'Seleccionar documento',
            uploadUrl: base_url+'Coe_pyg/inserupdate_compfin',
            inputGroupClass: "input-group-sm",
            maxFilePreviewSize: 5000,
            uploadExtraData: function (previewId, index) {
                var info = {
                            sprid:sprid,
							egrest:egrest,
							sucursal:sucursal,

                        };
                return info;
            }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
        }).on('filebatchuploadsuccess', function(event, files, extra) {
          	setTimeout(function(){ 
          		location.reload();
      		}, 1000);
          //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        }); 
});
function modaladdpresupuesto(ti,pre_tipo){
	$('#modal_presupesto').modal({backdrop: 'static', keyboard: false});
	var modaltitle='';
	if(tipo>0){
		modaltitle='Modificar ';
	}else{
		modaltitle='Agregar ';
	}
	if(pre_tipo=='vrs'){
		modaltitle+=' Presupuesto varios';
	}
	if(pre_tipo=='gas'){
		modaltitle+=' Presupuesto gasolina';
	}
	pre_tipo_v=pre_tipo;
	if(ti>0){
		$('#pre_comprobante').prop('required', false);
	}else{
		$('#pre_comprobante').prop('required', true);
		$("#form_presupuesto")[0].reset();
	}
	$('#myModalpresupuestoLabel').html(modaltitle);
}
function saveformpresupuesto(){
	var form = $('#form_presupuesto');
	if(form.valid()){
		rid = $('#rid').val();
		pre_importe = $('#pre_importe').val();
		pre_forma = $('#pre_forma').val();
		pre_concepto = $('#pre_concepto').val();
		pre_fecha = $('#pre_fecha').val();
		pre_asigno = $('#pre_asigno').val();
		$('#pre_comprobante').fileinput('upload'); 
	}else{
		toastr.error('Es necesario completar los campos requeridos');
	}
}
function edit_pres(id){
	var preimporte = $('.edit_'+id).data('preimporte');
	var preforma = $('.edit_'+id).data('preforma');
	var preconcepto = $('.edit_'+id).data('preconcepto');
	var prefecha = $('.edit_'+id).data('prefecha');
	var preasigno = $('.edit_'+id).data('preasigno');
	var pretipo = $('.edit_'+id).data('pretipo');

	modaladdpresupuesto(id,pretipo);

	$('#rid').val(id);
	$('#pre_importe').val(preimporte);
	$('#pre_forma').val(preforma);
	$('#pre_concepto').val(preconcepto);
	$('#pre_fecha').val(prefecha);
	$('#pre_asigno').val(preasigno);
}
function edit_ccp(id){
	$('#modal_solicitudpago').modal({backdrop: 'static', keyboard: false});
	$('#sprid').val(id);
	var egrfecini = $('.edit_ccp_'+id).data('egrfecini');
	var egrimporte = $('.edit_ccp_'+id).data('egrimporte');
	var egrconcepto = $('.edit_ccp_'+id).data('egrconcepto');
	var egrobs = $('.edit_ccp_'+id).data('egrobs');
	var egrcompini = $('.edit_ccp_'+id).data('egrcompini');
	var egrest = $('.edit_ccp_'+id).data('egrest');
	var suc = $('.edit_ccp_'+id).data('suc');

	$('.egrfecini').html(egrfecini);
	$('.egrimporte').html(egrimporte);
	$('.egrconcepto').html(egrconcepto);
	$('.egrobs').html(egrobs);
	if(egrcompini==''){
		var egrcompini_file='';
	}else{
		var egrcompini_file='<a href="'+base_url+'_files/_suc/'+suc+'/pag/'+egrcompini+'" target="_blank">Comprobante</a>';
	}

	$('.egrcompini').html(egrcompini_file);
}
function saveformsolpago(){
	var form = $('#form_solicitudpago');
	if(form.valid()){
		sprid = $('#sprid').val();
		egrest = $('#egrest option:selected').val();
		$('#egr_compfin').fileinput('upload'); 
	}else{
		toastr.error('Es necesario completar los campos requeridos');
	}
}
function delete_pres(id){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma la eliminación del registro',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Coe_pyg/delete_pres",
                        data: {
                            rid:id
                        },
                        success: function (response){
                        	toastr.success('Registro eliminado');
                          	setTimeout(function(){ 
				          		location.reload();
				      		}, 1000);

                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}