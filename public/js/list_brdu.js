var base_url =$('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table_list').DataTable();
	loadtable();
});
function loadtable(){
	table.destroy();
	table=$("#table_list").DataTable({
    		//stateSave: true,
    		//responsive: !0,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		       "url": base_url+"BRDU/getlista",
		       type: "post",
		       "data": function(d){
		        // d.bodegaId = $('#bodegaId option:selected').val()
		        },
		        error: function(){
		           $("#table").css("display","none");
		        }
		    },
		    "columns": [
		        {"data": "rid","width": "10%","orderable":false},
		        {"data": "rev_cap"},
				{"data": "usuario"},
				{"data": "rev"},
				{"data": "equipo","width": "20%","orderable":false},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		            	var html="";
		            	if(row.rev_est=='fin'){
		            		html+='<a href="'+base_url+'BRDU/rev_ver/'+row.rev+'" type="button" class="btn-sm btn-info mr-1 mb-1"><i class="fa fa-folder fa-fw"></i></a>';
		            	}else{
		            		html+='<a href="'+base_url+'BRDU/rev_mod/'+row.rev+'" type="button" class="btn-sm btn-success mr-1 mb-1"><i class="fa fa-pencil fa-fw"></i></a>';
		            		html+='<a onclick="deleteservicio('+row.rid+')" type="button" class="btn-sm btn-danger mr-1 mb-1"><i class="fa fa-trash fa-fw"></i></a>';
		            	}
		            	return html;
		            }
		        },
		    ],
    		"order": [[ 1, "DESC" ]],
    		"lengthMenu": [[10,20,30, 50, 100], [10,20,30, 50, 100]],
    		
			createdRow: function (row, data, index) {
				//$( row ).find('td:eq(0)').addClass('td_colum_0');
				//$( row ).find('td:eq(9)').addClass('td_colum_9');
			}
  	}).on('draw',function(row){
  		
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
   	});
}
/*
function searchtable_v(){
    var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}
*/

function deleteservicio(codigo){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma Eliminación de la revisión',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/BRDU/delete",
                        data: {
                            codigo:codigo
                        },
                        success: function (response){
                                toastr["success"]("Se realizo la Eliminación", "Hecho!");
                                loadtable();
                                //location.reload();
                        },
                        error: function(response){
                            toastr["error"]("No se pudo realizar la Eliminación", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}