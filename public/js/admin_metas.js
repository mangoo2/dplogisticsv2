var base_url = $('#base_url').val();

$(document).ready(function($) {
	$('#anio').change();
});
function cargarmetas(){
	var anio=$('#anio option:selected').val();
	$.ajax({
            type:'POST',
            url: base_url+'index.php/Admin/cargarmetas',
            data: {
                anio:anio
            },
            success:function(data){
                $('.load_metas').html(data);
            }
        });
}
function actualizar_metas(){
    var datametas = $(".tables_metas tbody > tr");
    //==============================================
        var DATAa  = [];
        datametas.each(function(){         
            item = {};
            item["id"] = $(this).find("input[id*='id']").val();
            item["sucid"] = $(this).find("input[id*='sucid']").val();
            item["anio"] = $(this).find("input[id*='anio']").val();
            item["mes"] = $(this).find("input[id*='mes']").val();
            item["fecha_ini"] = $(this).find("input[id*='fecha_ini']").val();
            item["fecha_fin"] = $(this).find("input[id*='fecha_fin']").val();
            item["meta_kg"] = $(this).find("input[id*='meta_kg']").val();
            item["meta_venta"] = $(this).find("input[id*='meta_venta']").val();
            DATAa.push(item);
        });
        INFOa  = new FormData();
        aInfoa   = JSON.stringify(DATAa);
        var datos='metas='+aInfoa;
    //==================================================
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Admin/insertupdatemetas',
        data: datos,
        async: false,
        statusCode:{
            404: function(data){
                toastr["error"]("No se pudo procesar", "Error"); 
            },
            500: function(){
                toastr["error"]("No se pudo procesar", "Error"); 
            }
        },
        success:function(data){
            toastr["success"]("Datos registrados"); 
        }
    });
}
function generar_reporte(){
    $('.load_metas_reporte').html('');
    $('.load_metas_reporte_char').html('');
    var anio2=$('#anio2 option:selected').val();
    $('body').loader('show');
    setTimeout(function(){ 
        $.ajax({
            type:'POST',
            url: base_url+'index.php/Admin/generar_reportemetas',
            data: {
                    anio:anio2
                },
            async: false,
            statusCode:{
                404: function(data){
                    toastr["error"]("No se pudo procesar", "Error"); 
                    $('body').loader('hide');
                },
                500: function(){
                    toastr["error"]("No se pudo procesar", "Error"); 
                    $('body').loader('hide');
                }
            },
            success:function(data){
                $('.load_metas_reporte').html(data);
                $('body').loader('hide');
                agregarcanvas();
                generarcharts();
            }
        });
    }, 1000);
}
