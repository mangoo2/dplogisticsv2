var base_url=$('#base_url').val();
var sucursal=$('#sucursal').val();
$(document).ready(function($) {
	
});
function modalpam(rid){
	$('#mdlPamMod').modal({backdrop: 'static', keyboard: false});
	$('#rid').val(rid);
	$.ajax({
            type:'POST',
            url: base_url+"index.php/Coe_pam/pam_pammod",
            data: {
                rid:rid,
                suc:sucursal
            },
            success: function (response){
               $('.row_frmMod').html(response);                
            },
            error: function(response){
                toastr["error"]("No se pudo procesar", "Error"); 
                 
            }
        });
}
function savePamMod(){
	var form=$('#frmMod');
	var datos=form.serialize()
	$.ajax({
            type:'POST',
            url: base_url+"index.php/Coe_pam/pam_pamgua",
            data: datos,
            success: function (response){
               //$('.row_frmMod').html(response); 
               toastr.success('Se a reistrado correctamente');
               	setTimeout(function(){ 
          			location.reload();
      			}, 1000);           
            },
            error: function(response){
                toastr["error"]("No se pudo procesar", "Error"); 
                 
            }
    });
}
function edit_pam_u(id){
    $('#mdlEquMod').modal({backdrop: 'static', keyboard: false});
    if(id>0){
        var equnumeco = $('.edit_pam_u'+id).data('equnumeco');
        var equnombre = $('.edit_pam_u'+id).data('equnombre');
        var equmarca = $('.edit_pam_u'+id).data('equmarca');
        var equmodelo = $('.edit_pam_u'+id).data('equmodelo');
        var equplaser = $('.edit_pam_u'+id).data('equplaser');
        var equdescrip = $('.edit_pam_u'+id).data('equdescrip');
        var equestatus = $('.edit_pam_u'+id).data('equestatus');
        
        $('#edeqrid').val(id);
        $('#equ_numeco').val(equnumeco);
        $('#equ_nombre').val(equnombre);
        $('#equ_marca').val(equmarca);
        $('#equ_modelo').val(equmodelo);
        $('#equ_plaser').val(equplaser);
        $('#equ_descrip').val(equdescrip);
        $('#equ_estatus').val(equestatus);
    }else{
        $("#frmMod_equipo")[0].reset();
    }
}
function saveEquMod(){
    var form = $('#frmMod_equipo');
    if(form.valid()){
        var datos=form.serialize()
        $.ajax({
                type:'POST',
                url: base_url+"index.php/Coe_pam/pam_equgua",
                data: datos,
                success: function (response){
                   //$('.row_frmMod').html(response); 
                   toastr.success('Se a reistrado correctamente');
                    setTimeout(function(){ 
                        location.reload();
                    }, 1000);           
                },
                error: function(response){
                    toastr["error"]("No se pudo procesar", "Error"); 
                     
                }
        });
    }else{
        toastr.error('complete los campos requeridos');
    }
}
function delete_pam_u(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma la eliminación del registro',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Coe_pam/pam_equbor",
                        data: {
                            rid:id
                        },
                        success: function (response){
                            toastr.success('Registro eliminado');
                            setTimeout(function(){ 
                                location.reload();
                            }, 1000);

                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
