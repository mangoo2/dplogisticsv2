var base_url = $('#base_url').val();
$(document).ready(function($) {
	$('#saveform').click(function(event) {
		$( "#saveform" ).prop( "disabled", true );
        setTimeout(function(){ 
             $("#saveform" ).prop( "disabled", false );
        }, 5000);

		var formulario =$('#form_datos');
		var validform=formulario.valid();
		if(validform){
			
			var datos = formulario.serialize();
			$.ajax({
                  type:'POST',
                  url: base_url+'index.php/Servicios_RAMPA/updateinser_ser_coefin',
                  data: datos,
                  success:function(data){
                      
                    toastr["success"]("Información Guardada", "Hecho!")      
                    setTimeout(function(){ 
                    		var codigo=$('#ser').val();
                        window.location.href = base_url+"index.php/Servicios_RAMPA/"; 
                        //javascript:history.back();
                    }, 3000);
                      
                      
                  },
                  error: function(response){
                  	console.log(response);
                  	toastr["error"]("No se pudo guardar la información", "Error");
                  }

              });
		}
	});

});