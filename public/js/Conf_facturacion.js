var base_url = $('#base_url').val();
$(document).ready(function($) {
    $("#cerdigital").fileinput({
        showCaption: false,
        showUpload: false,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["cer"],
        browseLabel: 'Seleccionar Certificado',
        uploadUrl: base_url+'Config/cargacertificado',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            'cer': '<i class="fas fa-file-invoice"></i>',
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    id:$('#tiporason option:selected').val()
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      toastr.success('Se cargo el Certificado Correctamente','Hecho!');
    });
    $("#claveprivada").fileinput({
        showCaption: false,
        showUpload: false,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["key"],
        browseLabel: 'Seleccionar clave privada',
        uploadUrl: base_url+'Config/cargakey',
        preferIconicZoomPreview: false,
        previewFileIcon: '<i class="fas fa-passport"></i>',
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            'key': '<i class="fas fa-passport"></i>',
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    pass:$('#passprivada').val(),
                    id:$('#tiporason option:selected').val()
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      toastr.success('Se cargo la claveprivada Correctamente','Hecho!');
    });
});
function actualizardatos(){
    var form = $('#formdatosgenerales');
    var datos = form.serialize();
    if (form.valid()) {
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Config/datosgenerales",
            data: datos,
            statusCode:{
                404: function(data){
                    toastr.error('404','Error!');
                },
                500: function(){ 
                    toastr.error('500','Error!');
                }
            },
            success:function(data){  
                toastr.success('Se actualizo Correctamente','Hecho!');
            }
        });
    }
}
function tiporason(){
    var tipo=$('#tiporason option:selected').val();
    window.location.href = base_url+"Config/facturacion/"+tipo;
}
function instalararchivos(){
    $('#cerdigital').fileinput('upload');
    $('#claveprivada').fileinput('upload');
    setTimeout(function(){
        instalararchivosprocesar();
    }, 3000);
}
function instalararchivosprocesar(){
    var id=$('#tiporason option:selected').val()
    if(id==1){
        var idurl='';
    }else{
        var idurl='2';
    }
    $.ajax({
        type:'POST',
        url: base_url+"files_sat/elementos"+idurl+"/procesarfiles.php",
        success: function (data){
            
        }
    });
}