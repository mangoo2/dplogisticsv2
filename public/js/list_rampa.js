var base_url =$('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table_list').DataTable();
	loadtable();
});
function loadtable(){
	table.destroy();
	table=$("#table_list").DataTable({
    		stateSave: true,
    		//responsive: !0,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		       "url": base_url+"Servicios_RAMPA/getlista",
		       type: "post",
		       "data": function(d){
		        // d.bodegaId = $('#bodegaId option:selected').val()
		        },
		        error: function(){
		           $("#table").css("display","none");
		        }
		    },
		    "columns": [
		        {"data": "ser_fol"},
		        {"data": "ser_fecini"},
		        {"data":"ser_vuelo"},
				{"data":"ser_aeronave"},
				{"data":"ser_origen"},
				{"data":"ser_destino"},
				{"data":"ser_clis"},
				{"data":"ser_sers"},
				{"data":"ser_obs",
					"render": function ( data, type, row, meta ) {
						var html="";
						html+=row.ser_obs+'<br>'+row.ser_finobs;
						return html;
					}
				},
				{"data":"ser_est","width": "90px",
					"render": function ( data, type, row, meta ) {
						var html="";

							if(row.ser_est=='fin'){
								html = 'Finalizado';
							}else{
								html = '<span class="badge badge-warning mb-1 mr-2">Sin finalizar</span>';
							}
						return html;
					}
				},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="";
	                	if(row.ser_est=='ini'){
	                		//$btnCoeVer = jBoton(['value'=>jIcono('edit'),'href'=>SECCION_NAV.'/coe_ini/'.$ser,'class'=>'btnCoeIni']);
	                		html+='<a href="'+base_url+'Servicios_RAMPA/coe_ini/'+row.ser+'" type="button" class="btn-sm btn-success mr-1 mb-1"><i class="fa fa-pencil fa-fw"></i></a>';
            			}else if(row.ser_est=='ffi'){
            				//$btnCoeVer = jBoton(['value'=>jIcono('edit'),'href'=>SECCION_NAV.'/coe_fin/'.$ser,'class'=>'btnCoeFin']);
            				html+='<a href="'+base_url+'Servicios_RAMPA/coe_fin/'+row.ser+'" type="button" class="btn-sm btn-success mr-1 mb-1"><i class="fa fa-pencil fa-fw"></i></a>';
            			}else{
            				//$btnCoeVer = jBoton(['value'=>jIcono('edit'),'href'=>SECCION_NAV.'/coe_ver/'.$ser,'class'=>'btnCoeVer']);
            				html+='<a href="'+base_url+'Servicios_RAMPA/coe_ver/'+row.ser+'" type="button" class="btn-sm btn-success mr-1 mb-1"><i class="fa fa-pencil fa-fw"></i></a>';
            			}
            			if(row.ser_fol<=0){
            				//$btnCoeBor=jBoton(['value'=>jIcono('trash'),'href'=>SECCION_NAV.'/coe_bor/'.$rid,'class'=>'btnCoeBor']);
            				html+='<a onclick="deleteservicio('+row.rid+')" type="button" class="btn-sm btn-danger mr-1 mb-1"><i class="fa fa-trash fa-fw"></i></a>';
            			}else{
            				//$btnCoeImp = jBoton(['value'=>jIcono('print'),'href'=>SECCION_NAV.'/coe_ver/'.$ser,'class'=>'btnCoeVer']);
            				html+='<a href="'+base_url+'Servicios_RAMPA/coe_ver/'+row.ser+'" type="button" class="btn-sm btn-success mr-1 mb-1"><i class="fa fa-print fa-fw"></i></a>';
            			}
		            return html;
		            }
		        },
		    ],
    		"order": [[ 1, "DESC" ]],
    		"lengthMenu": [[30, 50, 100], [30, 50, 100]],
    		'columnDefs': [ {
    			'targets': [0,1,2,3,4,5,6,7,8,9,10], /* table column index */
    			'orderable': false, /* here set the true or false */
				}],
			createdRow: function (row, data, index) {
				//$( row ).find('td:eq(0)').addClass('td_colum_0');
				//$( row ).find('td:eq(9)').addClass('td_colum_9');
			}
  	}).on('draw',function(row){
  		
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
   	});
}
/*
function searchtable_v(){
    var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}
*/
function checkboxcredito(id){
	var checked = $('#clientecheck_'+id).is(':checked')==true?1:0;
	$.ajax({
      	type:'POST',
      	url: base_url+'index.php/Clientes/checkboxcredito',
      	data: {
      		rid:id,
      		credito:checked
      	},
      	success:function(data){
          
      	}
  	});
}
function deleteservicio(codigo){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma Eliminación de servicio',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Servicios_RAMPA/delete",
                        data: {
                            codigo:codigo
                        },
                        success: function (response){
                                toastr["success"]("Se realizo la solicitud", "Hecho!");
                                loadtable();
                                //location.reload();
                        },
                        error: function(response){
                            toastr["error"]("No se pudo realizar la solicitud", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}