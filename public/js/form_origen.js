var base_url = $('#base_url').val();
var rid;
$(document).ready(function($) {
	get_dpl();
    get_estaciones();
    $(".mascarainput").inputmask();
});
function guardar_registro(){
	var formulario =$('#form_registro');
	var validform=formulario.valid();
	if(validform){
		$('.btn_registro').attr('disabled',true);
		var datos = formulario.serialize();
		$.ajax({
            type:'POST',
            url: base_url+'index.php/Origen/insert_registro',
            data: datos,
            success:function(data){
                rid=parseInt(data);
                if(data>=1){
                    Swal.fire(
					  'Hecho!',
					  'Guardada Correctamente',
					  'success'
					);
                    setTimeout(function(){ 
                      javascript:history.back();
                    }, 3000);
                }else{
                    Swal.fire(
		              '¡Atención!',
		              'No se pudo guardar la información',
		              'error'
		            ); 
                }
            }
        });
	}
}

function get_dpl(){
	$('#dpl').select2({
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar:',
        ajax: {
            url: base_url+'Origen/search_dpl',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.ruta,
                        text: element.rut_cla+' -> '+element.rut_nom,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}

function get_estaciones(){
    $('#num_estacion').select2({
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar:',
        ajax: {
            url: base_url+'Origen/search_estaciones',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave_identificacion,
                        text: element.descripcion+'('+element.clave_identificacion+')',
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}

