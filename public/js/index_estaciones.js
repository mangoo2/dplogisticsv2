var base_url =$('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table_list').DataTable();
	loadtable();
});
function loadtable(){
	table.destroy();
	table=$("#table_list").DataTable({
    		stateSave: true,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		        "url": base_url+"Estaciones/get_listado",
		        type: "post",
		        error: function(){
		           $("#table").css("display","none");
		        }
		    },
		    "columns": [
		        {"data":"id"},
		        {"data":"clave_identificacion"},
		        {"data":"descripcion"},
				{"data":"clave_transporte"},
				{"data":"nacionalidad"},
				{"data":"iata"},
				{"data":"linea_ferrea"},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		            	var checke = '';
		            	if(row.activo==1){
                            checke = 'checked';
		            	}else{
                            checke = ''; 
		            	}
		                var html="";
		                	html+='<div class="divbuttonstable">';
		                    html+='<div class="custom-control custom-checkbox">\
                                <input type="checkbox" class="custom-control-input"  id="check_'+row.id+'" onclick="validar('+row.id+')" '+checke+'>\
                                <label class="custom-control-label" for="check_'+row.id+'"><span></span></label>\
                            </div>';
		                    html+='</div>';
		            return html;
		            }
		        },
		    ],
    		"order": [[ 1, "asc" ]],
    		"lengthMenu": [[10,25, 50, 100], [10,25, 50, 100]],
  	}).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
   	});
}


function validar(id){
	var id_aux=0; 
	if ($('#check_'+id).is(':checked')){
        id_aux=1; 
	}else{
        id_aux=0; 
	}
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Estaciones/estatus_reg",
        data: {
            id:id,
            estatus:id_aux
        },
        success: function (response){         
        },
        error: function(response){
            toastr["error"]("No se pudo procesar", "Error");  
        }
    });
}
