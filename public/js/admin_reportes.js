var base_url = $('#base_url').val();
var tableg;
var myChart;
var rowchart=0;

var myChart2;
var rowchart2=0;
$(document).ready(function($) {
    divtableg();
    divtablegindivi();
	tableg=$('#tableg').DataTable();
    select2_cliente();
	
    
});
function select2_cliente(){
    $('#Cliente').html('');
    $('#Cliente').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Documentar/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.cliente,
                        text: element.cli_nombre+' '+element.cli_paterno+' '+element.cli_materno,
                        credito: element.credito,

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
      //generarclig();
    }); 
}

function generarclig(){
	//generarclig_datatable();
	generarclig_chart();
}
function generarclig_datatable(){
    var cliente=$('#Cliente option:selected').val()==undefined?0:$('#Cliente option:selected').val();
	var anio=$('#anio').val();
    anio   = JSON.stringify(anio);
    var top=$('#top option:selected').val();
	var sucursal = $('#sucursal option:selected').val();

	tableg.destroy();
	tableg = $('#tableg').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Admin/getlistacli",
            type: "post",
            "data": {
                'cliente':cliente,
                'anio':anio,
                'sucursals':sucursal,
                'top':top
            },
        },
        "columns": [
            {"data":"cliente"},
            {"data": "paq_pes"},
		    {"data":"tar_tot"},
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[20,25, 50, 100,200,400,-1], [20,25, 50,100,200,400,'todo']],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        //columnDefs: [ { orderable: false, targets: [1,2] }],
        
    }).on('draw',function(){
        
    });
}

function generarclig_chart(){
    
    $('#Cliente').html('');
    divtableg();
	//$('.classmybarChart').html('<canvas id="mybarChart"></canvas>');
	var cliente=$('#Cliente option:selected').val()==undefined?0:$('#Cliente option:selected').val();
	//var anio=$('#anio').val();
    //anio   = JSON.stringify(anio);
    var f_inicial=$('#f_inicial').val();
    var f_final=$('#f_final').val();
	var sucursal = $('#sucursal option:selected').val();
    var top=$('#top option:selected').val();
	if(sucursal>0){
		var sucursaltext = $('#sucursal option:selected').text();
	}else{
		var sucursaltext='';
	}
	
	if(f_inicial!='' && f_final!=''){
        $('body').loader('show');
    	if (rowchart>0) {
    	  myChart.destroy();
    	        //(or)
    	 // chartStatus.clear();
    	}

    	$.ajax({
            type:'POST',
            url: base_url+'index.php/Admin/getlistaclichart',
            data: {
              	cliente:cliente,
                fini:f_inicial,
                ffin:f_final,
    			sucursal:sucursal,
                top:top
            },
            success:function(data){
                
                var array = $.parseJSON(data);
                console.log(array.datos);
                console.log(array.values);
                var valuesarray=[];
                var valoresarray=[];
                $.each(array.datos, function(index, item) {
                    //console.log(item);
                    if(item.anio>0){
                        $('.tableg_tbody').append('<tr><td class="clienteg" data-clienteid="'+item.clienteid+'" data-cli="'+item.cliente+'" data-paqpes="'+item.paq_pes+'" data-tartot="'+item.tar_tot+'" data-folios="'+item.folios+'" data-tarifabaja="'+item.tarifa_baja+'" data-tarifaespecial="'+item.tarifa_especial+'" >'+item.anio+'</td><td>'+item.cliente+'</td><td>'+item.paq_pes+'</td><td>'+item.tar_tot+'</td>/tr>');
                        valuesarray.push(item.cliente);
                        valoresarray.push(item.paq_pes);
                        $('#Cliente').append('<option value="'+item.clienteid+'">'+item.cliente+'</option>');
                    }
                    
                });
                console.log(valuesarray);

                var ctx = document.getElementById('mybarChart').getContext('2d');
                   myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: valuesarray,
                        datasets: [{
                            label: 'Clientes '+sucursaltext,
                            data: valoresarray,
                            
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });
                myChart.update();
                

                
                tableg = $('#tableg').DataTable({
      "lengthMenu": [ 50, 75, 100 ]
    })
    			$('body').loader('hide');

            }
        });
        rowchart++;
        $('#Cliente').select2();
    }else{
        toastr["error"]("Se debe de ingresar fecha inicial y final", "Advertencia"); 
    }
}
/*
function tablegindivi(){
	var cliente=$('#Cliente option:selected').val()==undefined?0:$('#Cliente option:selected').val();
	
	if(cliente>0){
        var datos=$('#formfintros').serialize();
        console.log(datos);
		$.ajax({
	        type:'POST',
	        url: base_url+'index.php/Admin/getlistacliindivii',
	        data: {
	          	cliente:cliente,
				anio:anio,
				sucursals:sucursal,
	        },
	        success:function(data){
	        	console.log('datos');
                console.log(data);
                var array = $.parseJSON(data);
                $('#tablegindivi tbody').html('');
                var arrayvalues=[];
                var arrayvalores=[];
                $.each(array, function(index, item) {
                    arrayvalues.push(item.periodo);
                    arrayvalores.push(item.kilos);
                    var html='<tr>\
                                    <td>'+item.mes+'</td>\
                                    <td>'+item.semana+'</td>\
                                    <td>'+item.periodo+'</td>\
                                    <td>'+item.kilos+'</td>\
                                    <td>'+item.pesos+'</td>\
                                    <td>'+item.promedio+'</td>\
                                </tr>';
                    $('#tablegindivi tbody').append(html);
                });
				console.log(arrayvalues);
                //========================================
                    if (rowchart2>0) {
                        myChart2.destroy();
                            //(or)
                        // chartStatus.clear();
                    }
                    var ctx2 = document.getElementById('mybarChartind').getContext('2d');
                        myChart2 = new Chart(ctx2, {
                            type: 'bar',
                            data: {
                                labels: arrayvalues,
                                datasets: [{
                                    label: 'Clientes ',
                                    data: arrayvalores,
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                        myChart2.update();
                //========================================

	        }
	    });
	}else{
		toastr["error"]("Seleccione un cliente", "Advertencia"); 
	}
}
*/
function tablegindividetalles(){
    divtablegindivi();
    $('body').loader('show');
    //$('.classmybarChart').html('<canvas id="mybarChart"></canvas>');
    var cliente=$('#Cliente option:selected').val()==undefined?0:$('#Cliente option:selected').val();
    var f_inicial=$('#f_inicial').val();
    var f_final=$('#f_final').val();

    //var anio=$('#anio').val();
    //anio   = JSON.stringify(anio);
    var sucursal = $('#sucursal option:selected').val();
    var top=$('#top option:selected').val();
    if(sucursal>0){
        var sucursaltext = $('#sucursal option:selected').text();
    }else{
        var sucursaltext='';
    }
    
    
    if(f_inicial!='' && f_final!=''){
    var kilostotal=0;
        $.ajax({
            type:'POST',
            url: base_url+'index.php/Admin/tablegindividetalles',
            data: {
                cliente:cliente,
                fini:f_inicial,
                ffin:f_final,
                sucursals:sucursal,
                top:top
            },
            success:function(data){
                console.log('datos');
                    console.log(data);
                    var array = $.parseJSON(data);
                    $('#tablegindivi tbody').html('');
                    var arrayvalues=[];
                    var arrayvalores=[];
                    $.each(array, function(index, item) {
                        arrayvalues.push(item.periodo);
                        arrayvalores.push(item.kilos);
                        kilostotal=parseFloat(kilostotal)+parseFloat(item.kilos);
                        var html='<tr>\
                                        <td>'+item.anio+'</td>\
                                        <td>'+item.mes+'</td>\
                                        <td>'+item.semana+'</td>\
                                        <td>'+item.periodo+'</td>\
                                        <td>'+item.kilos+'</td>\
                                        <td>'+item.pesos+'</td>\
                                        <td>'+item.promedio+'</td>\
                                    </tr>';
                        $('#tablegindivi tbody').append(html);
                    });
                    console.log(arrayvalues);
                    //========================================
                        if (rowchart2>0) {
                            myChart2.destroy();
                                //(or)
                            // chartStatus.clear();
                        }
                        var ctx2 = document.getElementById('mybarChartind').getContext('2d');
                            myChart2 = new Chart(ctx2, {
                                type: 'bar',
                                data: {
                                    labels: arrayvalues,
                                    datasets: [{
                                        label: 'Clientes ',
                                        data: arrayvalores,
                                        borderWidth: 1
                                    }]
                                },
                                options: {
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero: true
                                            }
                                        }]
                                    },
                                    plugins: {
                                      datalabels: {
                                        formatter: (value) => {
                                          //return value + '%';
                                            return '';
                                        },
                                        color: '#fff',
                                      },
                                    },
                                }
                            });
                            myChart2.update();
                    //========================================
                
                $('body').loader('hide');
                var html='<tr>\
                                        <td></td>\
                                        <td></td>\
                                        <td></td>\
                                        <td>TOTAL</td>\
                                        <td>'+kilostotal+'</td>\
                                        <td></td>\
                                        <td></td>\
                                    </tr>';
                $('#tablegindivi tbody').append(html);
            }
        });
    }else{
        toastr["error"]("Se debe de ingresar fecha inicial y final", "Advertencia"); 
    }
}
function divtableg(){
    $('.divtableg').html('<table id="tableg" class="table"><thead><tr><th>AÑO</th><th>NOMBRE DEL CLIENTE</th><th>TOTAL KG AÑO<br>VIGENTE</th><th>TOTAL $ AÑO<br>VIGENTE</th></tr></thead><tbody class="tableg_tbody"></tbody></table>');
}
function divtablegindivi(){
    $('.div_tablegindivi').html('<table id="tablegindivi" class="table"><thead><tr><th>AÑO</th><th>MES</th><th>SEMANA</th><th>PERIODO</th><th>KILOS</th><th>PESOS</th><th>Prom SEM</th></tr></thead><tbody></tbody></table>');
    $('.div_mybarChartind').html('<canvas id="mybarChartind"></canvas>');
}
function generar_v_c(){
    var clientesg = $("#tableg tbody > tr");
    //==============================================
        var DATAa  = [];
        clientesg.each(function(){         
            item = {};                    
            item ["cliid"]   = $(this).find("td[class*='clienteg']").data('clienteid');
            item ["cli"]   = $(this).find("td[class*='clienteg']").data('cli');
            item ["paqpes"]   = $(this).find("td[class*='clienteg']").data('paqpes');
            item ["tartot"]   = $(this).find("td[class*='clienteg']").data('tartot');
            item ["folios"]   = $(this).find("td[class*='clienteg']").data('folios');

            item ["tarifabaja"]   = $(this).find("td[class*='clienteg']").data('tarifabaja');
            item ["tarifaespecial"]   = $(this).find("td[class*='clienteg']").data('tarifaespecial');
            
            
            
            DATAa.push(item);
        });
        INFOa  = new FormData();
        aInfoa   = JSON.stringify(DATAa);
    //========================================
    if(clientesg.length>1){
        var datos='clientes='+aInfoa;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Admin/generar_v_c",
            data: datos,
            success: function (response){
                console.log(response);
                $('.tbody_valor_cli').html(response);
                $('.grafica1').html('<canvas id="grafica1"></canvas>');
                $('.grafica2').html('<canvas id="grafica2"></canvas>');
                $('.grafica3').html('<canvas id="grafica3"></canvas>');
                $('.grafica4').html('<canvas id="grafica4"></canvas>');
                $('.grafica5').html('<canvas id="grafica5"></canvas>');
                setTimeout(function(){ 
                    calculardatos(); 
                    grafica1();
                    grafica2();
                    grafica4();
                    grafica5();
                }, 1000);
                setTimeout(function(){ 
                    grafica3();
                }, 2000);
            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                
            }
        });
    }else{
        toastr["error"]("Generar antes la pestaña general");
    }
}
function calculardatos(){
    var g_ingreso_v_g_a = $('.g_ingreso_v_g_a').data('gingreso');
    var total_por=0;
    $(".ingreso_v_g_a").each(function() {
        var row = $(this).data('row');
        var ingre = $(this).data('ingre');
        var por =(parseFloat(ingre)*1)/parseFloat(g_ingreso_v_g_a);
        total_por=parseFloat(total_por)+parseFloat(por);

        var gpor=parseFloat(por)*100;
            gpor=gpor.toFixed(2);
            $('.ingreso_v_g_a_'+row).html('% '+gpor);
            $('.datasclass_'+row).data( "participacion", gpor);
    });
    var total_por=parseFloat(total_por)*100;
        total_por=total_por.toFixed(2);
        $('.t_ingreso_v_g_a').html('% '+total_por);
}
var barColors = [
  "rgba(185,29,71)",
  "rgba(0,171,169)",
  "rgba(43,87,151)",
  "rgba(232,195,185)",
  "rgb(30 113 69)",
  "rgb(4, 170, 109)",
  "rgb(151, 99, 246)",
  "rgb(165, 42, 42)",
  "rgb(255, 165, 0)",
  "rgb(0, 0, 255)",
  "rgb(255, 0, 0)",
  "rgb(0, 128, 0)",
  "rgb(217, 238, 225)",
];
function grafica1(){
    var arraylabels=[];
    var arrayvalores=[];
    var clientesg = $("#tableg tbody > tr");
    //==============================================
        var DATAa  = [];
        $("#table_valor_cli tbody > tr").each(function(){
            var al   = $(this).find("td[class*='datasclass']").data('cli');
            var av   = $(this).find("td[class*='datasclass']").data('valorxkilo');
            console.log(av);
            if(av>0){
                arraylabels.push(al);
                arrayvalores.push(av);
            }
            
        });
        
    //========================================
        new Chart("grafica1", {
          type: "pie",
          data: {
            labels: arraylabels,
            datasets: [{
              backgroundColor: barColors,
              data: arrayvalores
            }]
          },
          options: {
            legend: {
                labels: {
                    //fontColor: "blue",//color del label
                    fontSize: 10
                }
            },
            title: {
              display: true,
              text: "Valor % kilos"
            },
            plugins: {
              datalabels: {
                formatter: (value) => {
                  return value + '%';
                },
                color: '#fff',
              },
            },
          }
        });
}
function grafica2(){
    var arraylabels=[];
    var arrayvalores=[];
    var clientesg = $("#tableg tbody > tr");
    //==============================================
        var DATAa  = [];
        $("#table_valor_cli tbody > tr").each(function(){
            var al   = $(this).find("td[class*='datasclass']").data('cli');
            var av   = $(this).find("td[class*='datasclass']").data('valorvidacli');
            console.log(av);
            if(av>0){
                arraylabels.push(al);
                arrayvalores.push(av);
            }
            
        });
        
    //========================================
        new Chart("grafica2", {
          type: "pie",
          data: {
            labels: arraylabels,
            datasets: [{
              backgroundColor: barColors,
              data: arrayvalores
            }]
          },
          options: {
            legend: {
                labels: {
                    //fontColor: "blue",//color del label
                    fontSize: 10
                }
            },
            title: {
              display: true,
              text: "Valor de vida del cliente"
            },
            plugins: {
              datalabels: {
                formatter: (value) => {
                  return new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(value);
                },
                color: '#fff',
              },
            },
          }
          
        });
}
function grafica3(){
    var arraylabels=[];
    var arrayvalores=[];
    var clientesg = $("#tableg tbody > tr");
    //==============================================
        var DATAa  = [];
        $("#table_valor_cli tbody > tr").each(function(){
            var al   = $(this).find("td[class*='datasclass']").data('cli');
            var av   = $(this).find("td[class*='datasclass']").data('participacion');
            console.log(av);
            if(av>0){
                arraylabels.push(al);
                arrayvalores.push(av);
            }
            
        });
        
    //========================================
        new Chart("grafica3", {
          type: "pie",
          data: {
            labels: arraylabels,
            datasets: [{
              backgroundColor: barColors,
              data: arrayvalores
            }]
          },
          options: {
            legend: {
                labels: {
                    //fontColor: "blue",//color del label
                    fontSize: 10
                }
            },
            title: {
              display: true,
              text: "participacion del cliente"
            },
            plugins: {
              datalabels: {
                formatter: (value) => {
                  return value+' %';
                },
                color: '#fff',
              },
            },
          }
        });
}
function grafica4(){
    var arraylabels=[];
    var arrayvalores=[];
    var clientesg = $("#tableg tbody > tr");
    //==============================================
        var DATAa  = [];
        $("#table_valor_cli tbody > tr").each(function(){
            var al   = $(this).find("td[class*='datasclass']").data('cli');
            var av   = $(this).find("td[class*='datasclass']").data('valortartot');
            console.log(av);
            if(av>0){
                arraylabels.push(al);
                arrayvalores.push(av);
            }
            
        });
        
    //========================================
        new Chart("grafica4", {
          type: "pie",
          data: {
            labels: arraylabels,
            datasets: [{
              backgroundColor: barColors,
              data: arrayvalores
            }]
          },
          options: {
            legend: {
                labels: {
                    //fontColor: "blue",//color del label
                    fontSize: 10
                }
            },
            title: {
              display: true,
              text: "Valor % pesos"
            },
            plugins: {
              datalabels: {
                formatter: (value) => {
                  return value+' %';
                },
                color: '#fff',
              },
            },
          }
        });
}
function grafica5(){
    var arraylabels=[];
    var arrayvalores=[];
    var clientesg = $("#tableg tbody > tr");
    //==============================================
        var DATAa  = [];
        $("#table_valor_cli tbody > tr").each(function(){
            var al   = $(this).find("td[class*='datasclass']").data('cli');
            var av   = $(this).find("td[class*='datasclass']").data('ticketprom');
            console.log(av);
            if(av>0){
                arraylabels.push(al);
                arrayvalores.push(av);
            }
            
        });
        
    //========================================
        new Chart("grafica5", {
          type: "pie",
          data: {
            labels: arraylabels,
            datasets: [{
              backgroundColor: barColors,
              data: arrayvalores
            }]
          },
          options: {
            legend: {
                labels: {
                    //fontColor: "blue",//color del label
                    fontSize: 10
                }
            },
            title: {
              display: true,
              text: "Ticket promedio de compra"
            },
            plugins: {
              datalabels: {
                formatter: (value) => {
                  return new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(value);
                },
                color: '#fff',
              },
            },
          }
        });
}