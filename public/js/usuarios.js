var base_url =$('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table_list').DataTable();
	loadtable();
});
function loadtable(){
	table.destroy();
	table=$("#table_list").DataTable({
    		stateSave: true,
    		responsive: !0,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		       "url": base_url+"Adm_usrs/getlista",
		       type: "post",
		       "data": function(d){
		        // d.bodegaId = $('#bodegaId option:selected').val()
		        },
		        error: function(){
		           $("#table").css("display","none");
		        }
		    },
		    "columns": [
		        {"data":null},
		        {"data":"usuario"},
		        {"data":"nda"},
				{"data":"nombre"},
				{"data":"correo"},
				{"data":"sexo"},
				{"data":"suc_nombre"},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="";
		                	var idrow="'"+row.usuario+"'";
		                    html+='<a href="'+base_url+'Adm_usrs/add/'+row.usuario+'" type="button" class="btn btn-success mr-1 mb-1" style="padding: 6px;"><i class="fa fa-pencil fa-fw"></i></a>';
		                    html+='<button type="button" class="btn btn-danger mr-1 mb-1" style="padding: 6px;" onclick="deleteusuario('+idrow+')"><i class="fa fa-trash fa-fw"></i></button>';
		            return html;
		            }
		        },
		    ],
    		"order": [[ 1, "asc" ]],
    		"lengthMenu": [[10,25, 50, 100], [10,25, 50, 100]],
    		createdRow:function(row,data,dataIndex){
    			var usuario=data.usuario;
    			var usuariorow = usuario.replace('.', '_');
	            $(row).find('td:eq(0)')
	                .addClass('usuarioimg usuarioimg_'+usuariorow)
	                .attr('data-usuario',usuario).attr('data-usuariorow',usuariorow);
	        }
  	}).on('draw',function(){
  		var timedraw=1000;
  		$(".usuarioimg").each(function() {
  			var usuario = $(this).data('usuario');
  			var usuariorow = $(this).data('usuariorow');
        	setTimeout(function(){ 
                obtenerimagenes(usuario,usuariorow);
        	}, timedraw);
        	timedraw=timedraw+300;
        });
   	});
}
function obtenerimagenes(usuario,usuariorow){
	console.log(usuario)
	$.ajax({
        type:'POST',
        url: base_url+"Adm_usrs/verificarimg",
        data: {
            usuario: usuario
        },
        success: function (data){
        	console.log(data);
            $('.usuarioimg_'+usuariorow).html(data);
        }
    });
}
function deleteusuario(usuario){
	console.log(usuario);
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma borrar el registro...',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Adm_usrs/delete",
                        data: {
                            usuario:usuario
                        },
                        success: function (response){
                                toastr["success"]("Información Eliminada", "Hecho!");
                                loadtable();
                        },
                        error: function(response){
                            toastr["error"]("No se pudo guardar la información", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}