var base_url = $('#base_url').val();
$(document).ready(function($) {
        $(".mascarainput").inputmask();
        //$('#Dimensiones_m').inputmask("9{1,4}/9{1,4}/9{1,4}a{1,3}");
        $('#TranspInternac').change(function(event) {
            var selected=$('#TranspInternac option:selected').val();
            if(selected=='No'){
                $('.divViaEntradaSalida').hide('show');
                $('#ViaEntradaSalida').val('');
                $('.divEntradaSalidaMerc').hide('show');
                $('#EntradaSalidaMerc').val('');
            }else{
                $('.divViaEntradaSalida').show('show');
                $('.divEntradaSalidaMerc').show('show');
            }
        });
        $('#CveTransporte').change(function(event) {
            var selected=$('#CveTransporte option:selected').val();
            //01 transporte
            //03 aereo
            if(selected=='03'){
                $('.divTotalDistRec').hide('show');
                $('#TotalDistRec').val('');

                $('.add_table_transporte').html('');
                $('.divNumLicencia_m').hide('show');
                $('#NumLicencia_m').val('');
            }else{

                $('.divTotalDistRec').show('show');
                $('.add_table_aereo').html('');
                $('.divNumLicencia_m').show('show');
            }
        });
    //==================================================
        $('#NombreRFC_0').select2({
            //width: 'resolve',
            minimumInputLength: 3,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'Facturas/searchcli',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.fac_nrs,
                            text: element.fac_nrs,
                            rfc: element.fac_rfc

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
        }).on('select2:select', function (e) {
            var data = e.params.data;
            $('#RFCRemitenteDestinatario_0').val(data.rfc);
        });
        $('#NumEstacion_0').select2({
            //width: 'resolve',
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/NumEstacion',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.clave_identificacion,
                            text: element.clave_identificacion+' '+element.iata,
                            descrip:element.descripcion

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
        }).on('select2:select', function (e) {
            var data = e.params.data;
            $('#NombreEstacion_0').val(data.descrip);
            obteneridod(data.id,0);
        });
        $('#Colonia_0').select2({
            //width: 'resolve',
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Colonia',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Colonia,
                            text: element.c_Colonia+' ('+element.nombre+') (C.P:'+element.c_CodigoPostal+')',
                            cp:element.c_CodigoPostal

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
        }).on('select2:select', function (e) {
            var data = e.params.data;
            $('#CodigoPostal_0').val(data.cp);
        });
        $('#Localidad_0').select2({
            //width: 'resolve',
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Localidad',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Localidad,
                            text: element.c_Localidad+' ('+element.descripcion+')('+element.c_Estado+')',
                            estado:element.c_Estado

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
        });
        $('#Municipio_0').select2({
            //width: 'resolve',
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Municipio',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Municipio,
                            text: element.c_Municipio+' ('+element.descripcion+')('+element.c_Estado+')',
                            estado:element.c_Estado

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
        });
        $('#Estado_0').select2({
            //width: 'resolve',
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Estado',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Estado,
                            text: element.c_Estado+' ('+element.descripcion+')',
                            estado:element.c_Estado

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
        });
        $('#Pais_0').select2({
            //width: 'resolve',
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Pais',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Pais,
                            text: element.c_Pais+' ('+element.descripcion+')',
                            estado:element.c_Pais

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
        });
    //===================================================
        $('#NombreRFC_1').select2({
            //width: 'resolve',
            minimumInputLength: 3,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'Facturas/searchcli',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.fac_nrs,
                            text: element.fac_nrs,
                            rfc: element.fac_rfc

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
        }).on('select2:select', function (e) {
            var data = e.params.data;
            $('#RFCRemitenteDestinatario_1').val(data.rfc);
        });
        $('#NumEstacion_1').select2({
            //width: 'resolve',
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/NumEstacion',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.clave_identificacion,
                            text: element.clave_identificacion+' '+element.iata,
                            descrip:element.descripcion

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
        }).on('select2:select', function (e) {
            var data = e.params.data;
            $('#NombreEstacion_1').val(data.descrip);
            obteneridod(data.id,1);
        });
        $('#Colonia_1').select2({
            //width: 'resolve',
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Colonia',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Colonia,
                            text: element.c_Colonia+' ('+element.nombre+') (C.P:'+element.c_CodigoPostal+')',
                            cp:element.c_CodigoPostal

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
        }).on('select2:select', function (e) {
            var data = e.params.data;
            $('#CodigoPostal_1').val(data.cp);
        });
        $('#Localidad_1').select2({
            //width: 'resolve',
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Localidad',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Localidad,
                            text: element.c_Localidad+' ('+element.descripcion+')('+element.c_Estado+')',
                            estado:element.c_Estado

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
        });
        $('#Municipio_1').select2({
            //width: 'resolve',
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Municipio',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Municipio,
                            text: element.c_Municipio+' ('+element.descripcion+')('+element.c_Estado+')',
                            estado:element.c_Estado

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
        });
        $('#Estado_1').select2({
            //width: 'resolve',
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Estado',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Estado,
                            text: element.c_Estado+' ('+element.descripcion+')',
                            estado:element.c_Estado

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
        });
        $('#Pais_1').select2({
            //width: 'resolve',
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Pais',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Pais,
                            text: element.c_Pais+' ('+element.descripcion+')',
                            estado:element.c_Pais

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
        });
        $('#BienesTransp_m').select2({
            dropdownParent: $('#modal_addmodalmercancias'),
            width: 'resolve',
            minimumInputLength: 3,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar un Consepto',
              ajax: {
                url: base_url+'General/BienesTransp',
                dataType: "json",
                data: function (params) {
                  var query = {
                    search: params.term,
                    type: 'public'
                  }
                  return query;
              },
              processResults: function(data){
                var clientes=data;
                var itemscli = [];
                data.forEach(function(element) {
                        itemscli.push({
                            id: element.Clave,
                            text: element.nombre
                        });
                    });
                    return {
                        results: itemscli
                    };        
              },  
            }
        });
        $('#Embalaje_m').select2({
            dropdownParent: $('#modal_addmodalmercancias'),
            width: 'resolve',
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar un Consepto',
              ajax: {
                url: base_url+'General/Embalaje',
                dataType: "json",
                data: function (params) {
                  var query = {
                    search: params.term,
                    type: 'public'
                  }
                  return query;
              },
              processResults: function(data){
                var clientes=data;
                var itemscli = [];
                data.forEach(function(element) {
                        itemscli.push({
                            id: element.clave,
                            text: element.descripcion
                        });
                    });
                    return {
                        results: itemscli
                    };        
              },  
            }
        });

        $('#PermSCT_m').select2({
        dropdownParent: $('#modal_addmodaltransporte'),
        width: 'resolve',
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Consepto',
          ajax: {
            url: base_url+'General/PermSCT',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.descripcion
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
    $('#PermSCT2_m').select2({
        dropdownParent: $('#modal_addmodalaereo'),
        width: 'resolve',
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Consepto',
          ajax: {
            url: base_url+'General/PermSCT',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.descripcion
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
    $('#ConfigVehicular_m').select2({
        dropdownParent: $('#modal_addmodaltransporte'),
        width: 'resolve',
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Consepto',
          ajax: {
            url: base_url+'General/ConfigVehicular',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.descripcion
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
    $('#ConfigVehicular2_m').select2({
        dropdownParent: $('#modal_addmodalaereo'),
        width: 'resolve',
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Consepto',
          ajax: {
            url: base_url+'General/ConfigVehicular',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.descripcion
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
    $('#CodigoTransportista_m').select2({
        dropdownParent: $('#modal_addmodalaereo'),
        width: 'resolve',
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Consepto',
          ajax: {
            url: base_url+'General/CodigoTransportista',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.clave+' '+element.descripcion
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
    $('#ResidenciaFiscalOperador_m').select2({
            dropdownParent: $('#modal_addmodaloperadores'),
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Pais',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Pais,
                            text: element.c_Pais+' ('+element.descripcion+')',
                            estado:element.c_Pais

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
    });
    $('#Colonia_m').select2({
            dropdownParent: $('#modal_addmodaloperadores'),
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Colonia',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Colonia,
                            text: element.c_Colonia+' ('+element.nombre+')',
                            cp:element.c_CodigoPostal

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
    }).on('select2:select', function (e) {
            var data = e.params.data;
            $('#CodigoPostal_m').val(data.cp);
    });
    $('#Localidad_m').select2({
            dropdownParent: $('#modal_addmodaloperadores'),
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Localidad',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Localidad,
                            text: element.c_Localidad+' ('+element.descripcion+')',
                            estado:element.c_Estado

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
    });
    $('#Municipio_m').select2({
            dropdownParent: $('#modal_addmodaloperadores'),
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Municipio',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Municipio,
                            text: element.c_Municipio+' ('+element.descripcion+')',
                            estado:element.c_Estado

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
    });
    $('#Estado_m').select2({
        dropdownParent: $('#modal_addmodaloperadores'),
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'General/Estado',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                console.log(data);
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.c_Estado,
                        text: element.c_Estado+' ('+element.descripcion+')',
                        estado:element.c_Estado

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    $('#Pais_m').select2({
        dropdownParent: $('#modal_addmodaloperadores'),
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'General/Pais',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                console.log(data);
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.c_Pais,
                        text: element.c_Pais+' ('+element.descripcion+')',
                        estado:element.c_Pais

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });

    
    $('#ResidenciaFiscalPropietario_m').select2({
            dropdownParent: $('#modal_addmodalpropietario'),
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Pais',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Pais,
                            text: element.c_Pais+' ('+element.descripcion+')',
                            estado:element.c_Pais

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
    });
    $('#Colonia_m_p').select2({
            dropdownParent: $('#modal_addmodalpropietario'),
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Colonia',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Colonia,
                            text: element.c_Colonia+' ('+element.nombre+')',
                            cp:element.c_CodigoPostal

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
    }).on('select2:select', function (e) {
            var data = e.params.data;
            $('#CodigoPostal_m_p').val(data.cp);
    });
    $('#Localidad_m_p').select2({
            dropdownParent: $('#modal_addmodalpropietario'),
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Localidad',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Localidad,
                            text: element.c_Localidad+' ('+element.descripcion+')',
                            estado:element.c_Estado

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
    });
    $('#Municipio_m_p').select2({
            dropdownParent: $('#modal_addmodalpropietario'),
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Municipio',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Municipio,
                            text: element.c_Municipio+' ('+element.descripcion+')',
                            estado:element.c_Estado

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
    });
    $('#Estado_m_p').select2({
        dropdownParent: $('#modal_addmodalpropietario'),
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'General/Estado',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                console.log(data);
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.c_Estado,
                        text: element.c_Estado+' ('+element.descripcion+')',
                        estado:element.c_Estado

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    $('#Pais_m_p').select2({
        dropdownParent: $('#modal_addmodalpropietario'),
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'General/Pais',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                console.log(data);
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.c_Pais,
                        text: element.c_Pais+' ('+element.descripcion+')',
                        estado:element.c_Pais

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });


    $('#ResidenciaFiscalArrendatario_m').select2({
            dropdownParent: $('#modal_addmodalarrendatario'),
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Pais',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Pais,
                            text: element.c_Pais+' ('+element.descripcion+')',
                            estado:element.c_Pais

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
    });
    $('#Colonia_m_a').select2({
            dropdownParent: $('#modal_addmodalarrendatario'),
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Colonia',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Colonia,
                            text: element.c_Colonia+' ('+element.nombre+')',
                            cp:element.c_CodigoPostal

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
    }).on('select2:select', function (e) {
            var data = e.params.data;
            $('#CodigoPostal_m_a').val(data.cp);
    });
    $('#Localidad_m_a').select2({
            dropdownParent: $('#modal_addmodalarrendatario'),
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Localidad',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Localidad,
                            text: element.c_Localidad+' ('+element.descripcion+')',
                            estado:element.c_Estado

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
    });
    $('#Municipio_m_a').select2({
            dropdownParent: $('#modal_addmodalarrendatario'),
            minimumInputLength: 2,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una opción',
            allowClear: true,
            ajax: {
                url: base_url+'General/Municipio',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    console.log(data);
                    //var productos=data;
                    var itemscli = [];
                    //console.log(data);
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.c_Municipio,
                            text: element.c_Municipio+' ('+element.descripcion+')',
                            estado:element.c_Estado

                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
    });
    $('#Estado_m_a').select2({
        dropdownParent: $('#modal_addmodalarrendatario'),
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'General/Estado',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                console.log(data);
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.c_Estado,
                        text: element.c_Estado+' ('+element.descripcion+')',
                        estado:element.c_Estado

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    $('#Pais_m_a').select2({
        dropdownParent: $('#modal_addmodalarrendatario'),
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'General/Pais',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                console.log(data);
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.c_Pais,
                        text: element.c_Pais+' ('+element.descripcion+')',
                        estado:element.c_Pais

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });

    get_operador();
    get_transporte();
});




function formatcant(pesos){
    console.log('pesos: '+pesos);
    return new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(pesos);
}
function deleteconcepto(row){
    $('.rowcobrar_'+row).remove();
    calculartotales();
    //removeventaidtable();
}
function calculartotales(){
    var totales = 0;
    /*
    $(".preciorow").each(function() {
        var vtotales = parseFloat($(this).val());
        totales += Number(vtotales);
    });
    */
    var TABLApr   = $("#table_conceptos tbody > tr");
        TABLApr.each(function(){ 
            var vtotales = parseFloat($(this).find("input[id*='cantidad']").val())*parseFloat($(this).find("input[id*='precio']").val());
                vtotales = vtotales.toFixed(2);
                totales += Number(vtotales);
        });
    //=================================================
    var ivas = 0;
    $(".siva").each(function() {
        var vivas = parseFloat($(this).val());
        ivas += Number(vivas);
    });
    var dest = 0;
    $(".cdescuento").each(function() {
        var destc = parseFloat($(this).val());
        dest += Number(destc);
    });


    var subtotalc = parseFloat(totales);
        subtotalc = subtotalc.toFixed(2);
        ivas=ivas.toFixed(2);

        totales=totales.toFixed(2);
        ivas = parseFloat(ivas);
        ivas =ivas.toFixed(2);

    var Subtotalinfo=parseFloat(subtotalc);
    var subtotalc=parseFloat(subtotalc);
    
    if ($('#risr').is(':checked')) {
        var v_isr=Subtotalinfo*0.1;
            v_isr =v_isr.toFixed(2);
            $('#isr').val(v_isr);
            totales=totales-v_isr;
            totales=totales.toFixed(2);
    }else{
        var v_isr=0;
        $('#isr').val(0.00);
    }
    if ($('#riva').is(':checked')) {
        var v_riva=Subtotalinfo*0.106666;
            v_riva =v_riva.toFixed(2);
            $('#ivaretenido').val(v_riva);
            totales=totales-v_riva;
            totales=totales.toFixed(2);
    }else{
        $('#ivaretenido').val(0.00);
    }
    if ($('#5almillar').is(':checked')) {
        var v_5millar=(Subtotalinfo/1000)*5;
            v_5millar =v_5millar.toFixed(2);
            $('#5almillarval').val(v_5millar);
            totales=totales-v_5millar;
            totales=totales.toFixed(2);
    }else{
        $('#5almillarval').val(0.00);
    }
    if ($('#aplicaout').is(':checked')) {
        var aplicaout = Subtotalinfo*0.06;
            aplicaout = aplicaout.toFixed(2);
        
            $('#outsourcing').val(aplicaout);
            totales=totales-aplicaout;
            totales=totales.toFixed(2);
    }else{
        $('#outsourcing').val(0.00);
    }

    totales=totales-parseFloat(dest)+parseFloat(ivas);
    $('#Subtotalinfo').val(Subtotalinfo);
    $('#Subtotal').val(subtotalc);
    $('#descuentof').val(dest);
    $('#iva').val(ivas);
    $('#total').val(totales.toFixed(2));

}
function obtenerrfc(rfc){
    var data='<option value="'+rfc+'">'+rfc+'</option>';
            $('#rfc').html(data); 
            $('#rfc').select2({width: 'resolve'});
}
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
function funcionconceptos(){
    $('.unidadsat').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Unidad',
          ajax: {
            url: base_url+'Configuracionrentas/searchunidadsat',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.Clave,
                        text: element.Clave+' / '+element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
    $('.conseptosat').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Concepto',
          ajax: {
            url: base_url+'Configuracionrentas/searchconceptosat',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.Clave,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
}
/*function retimbrar(idfactura,contrato){
    $.ajax({
        type:'POST',
        url: base_url+"Timbrar.php",
        data: {
            action:'retimbrar',
            factura:idfactura
        },
        success:function(response){  
            console.log(response);
            var array = $.parseJSON(response);
            if (array.resultado=='error') {
                swal({    title: "Error "+array.CodigoRespuesta+"!",
                              text: array.MensajeError,
                              type: "warning",
                              showCancelButton: false
                 },
                    function(){
                        //retimbrar(idfactura,0);
                    });
                //$('body').loading('stop');
            }else{
                swal({
                    title:"Éxito!", 
                    text:"Se ha creado la factura", 
                    type:"success"
                    },
                    function(){
                        //$(location).attr('href',base_url+'Facturaslis');
                        $(location).attr('href',base_url+'index.php#coe_facts/facturaslist');
                    });
            }

        }
    });       
}*/
function activardescuento(idrow){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    $('.cdescuento_'+idrow).attr({'readonly':false});
                                    $('.cdescuento_'+idrow).removeAttr("onclick");
                                    //$('.cdescuento_'+idrow).removeAttr("onclick");
                                    $('.cdescuento_'+idrow).attr('onchange', 'calculardescuento('+idrow+');');
                                }else{
                                    $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Advertencia!',
                                    content: 'No tiene permisos'}); 
                                }
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                             
                        }
                    });
                    
                }else{
                    $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Advertencia!',
                                    content: 'Ingrese una contraseña'}); 
                }
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function calculardescuento(idrow){
    var costo =  parseFloat($('.cantidad_row_'+idrow).val())*parseFloat($('.precio_'+idrow).val());
                costo.toFixed(2);
    var descuento = $('.cdescuento_'+idrow).val();
    var rowtotal =parseFloat(costo)-parseFloat(descuento);
    var ivaif = $('.cdescuento_'+idrow).data('diva');
    if (ivaif==1) {
        var siva =rowtotal*0.16;
        //siva = siva.toFixed(2);
        if(trunquearredondear==0){
            siva = siva.toFixed(4);
        }else{
            siva=Math.floor(siva * 100) / 100;
        }
    }else{
        var siva =0.00;
    }
    $('.tiva_'+idrow).val(siva);
    var totalg = parseFloat(rowtotal)+parseFloat(siva);
    $('.csubtotal_'+idrow).val(totalg);
    calculartotales();
}
function calculartotales_set(tiempo){
    setTimeout(function(){ 
        calculartotales();
    }, tiempo);
}
/*function registrofac_preview(){
   
    blockearfact=1;
        var form =$('#validateSubmitForm');
        var valid =form.valid();
        if (valid) {
            //$( ".registrofac" ).prop( "disabled", true );
            var datos = form.serialize();
            var productos = $("#table_conceptos tbody > tr");
            //==============================================
                var DATAa  = [];
                productos.each(function(){         
                    item = {};                    
                    item ["Cantidad"]   = $(this).find("input[id*='cantidad']").val();
                    item ["Unidad"]  = $(this).find("select[id*='unidadsat'] option:selected").val();
                    console.log($(this).find("select[id*='unidadsat']").val());
                    if ($(this).find("select[id*='unidadsat']").val()==null || $(this).find("select[id*='unidadsat']").val()=='') {
                        blockearfact=0;
                    }
                    item ["servicioId"]  = $(this).find("select[id*='conseptosat'] option:selected").val();
                    console.log($(this).find("select[id*='conseptosat']").val());
                    if ($(this).find("select[id*='conseptosat']").val()==null || $(this).find("select[id*='conseptosat']").val()=='') {
                        blockearfact=0;
                    }
                    //item ["Descripcion"]  = $(this).find("select[id*='conseptosat'] option:selected").text();
                    item ["Descripcion2"]  = $(this).find("input[id*='descripcion']").val();
                    item ["Cu"]  = $(this).find("input[id*='precio']").val();
                    item ["descuento"]  = $(this).find("input[id*='descuento']").val();
                    item ["Importe"]  = $(this).find("input[id*='subtotal']").val();
                    item ["iva"]  = $(this).find("input[id*='tiva']").val();
                    DATAa.push(item);
                });
                INFOa  = new FormData();
                aInfoa   = JSON.stringify(DATAa);
                
            //========================================
            var productoslength=productos.length;
            if (blockearfact==0) {
                productoslength=0;
            }
            
            if (productoslength>0) {
                
                //$('body').loading({theme: 'dark',message: 'Timbrando factura...'});

                var tiporelacion = $('input:radio[name=tiporelacion]:checked').val();
                var ventaviculada = $('#ventaviculada').val();
                var ventaviculadatipo = $('#ventaviculadatipo').val();
                var Subtotal=$('#Subtotal').val();
                var iva=$('#iva').val();
                var visr= $('#isr').val();
                var vriva= $('#ivaretenido').val();
                var v5millar= $('#5almillarval').val();
                var outsourcing= $('#outsourcing').val();

                var total=$('#total').val();  
                            
                datos=datos+'&subtotal='+Subtotal+'&iva='+iva+'&visr='+visr+'&vriva='+vriva+'&v5millar='+v5millar+'&outsourcing='+outsourcing+'&total='+total+'&conceptos='+aInfoa+'&tiporelacion='+tiporelacion+'&ventaviculada='+ventaviculada+'&ventaviculadatipo='+ventaviculadatipo;
                
                //===
                $('#modal_previefactura').modal({
                    dismissible: false
                });
                
                setTimeout(function(){ 
                    console.log('entra iframe');
                    var urlfac=base_url+"preview.php?"+datos;
                    var htmliframe="<iframe src='"+urlfac+"' title='description' class='ifrafac'>";
                    $('.preview_iframe').html(htmliframe);
                    //window.location.href = base_url+"index.php/Preview/factura?"+datos; 
                }, 1000);
            }else{
                //alertfunction('Atención!','Agregar por lo menos un concepto');
            }
        }else{
            //alertfunction('Atención!','Faltan campos obligatorios');
        }
}*/
const unique = (value, index, self) => {
  return self.indexOf(value) === index
}
function permisoparafacturaabierta(){
    agregarconcepto();                            
}

function addmodalmercancias(){
    $('#modal_addmodalmercancias').modal({backdrop: 'static', keyboard: false});
}
var rowaddmercancia=0;
function addmercancia(){
    var BienesTransp_m = $('#BienesTransp_m').val();
    var BienesTransp_m_t = $('#BienesTransp_m').text().replace(/(\r\n|\n|\r|  )/gm, "");
    var Descripcion_m = $('#Descripcion_m').val();
    var Cantidad_m = $('#Cantidad_m').val();
    var ClaveUnidad_m = $('#ClaveUnidad_m').val();
    var Unidad_m = $('#ClaveUnidad_m option:selected').text().replace(/(\r\n|\n|\r|  )/gm, "");
    var Dimensiones_m = $('#Dimensiones_m').val();
    var Embalaje_m = $('#Embalaje_m').val();
    var DescripEmbalaje_m = $('#Embalaje_m').text().replace(/(\r\n|\n|\r|  )/gm, "");
    var PesoEnKg_m = $('#PesoEnKg_m').val();
    var ValorMercancia_m = $('#ValorMercancia_m').val();
    var Moneda_m = $('#Moneda_m option:selected').val();
    var Moneda_m_t = $('#Moneda_m option:selected').text().replace(/(\r\n|\n|\r|  )/gm, "");
    var html='<tr class="addmercancias_'+rowaddmercancia+'">\
                    <td >\
                        <input type="hidden" id="BienesTransp" value="'+BienesTransp_m+'">\
                        <input type="hidden" id="Descripcion" value="'+Descripcion_m+'">\
                        <input type="hidden" id="Cantidad" value="'+Cantidad_m+'">\
                        <input type="hidden" id="ClaveUnidad" value="'+ClaveUnidad_m+'">\
                        <input type="hidden" id="Unidad" value="'+Unidad_m+'">\
                        <input type="hidden" id="Dimensiones" value="'+Dimensiones_m+'">\
                        <input type="hidden" id="Embalaje" value="'+Embalaje_m+'">\
                        <input type="hidden" id="DescripEmbalaje" value="'+DescripEmbalaje_m+'">\
                        <input type="hidden" id="PesoEnKg" class="PesoEnKg" value="'+PesoEnKg_m+'">\
                        <input type="hidden" id="ValorMercancia" value="'+ValorMercancia_m+'">\
                        <input type="hidden" id="Moneda" value="'+Moneda_m+'">\
                        <div class="row">\
                                          <div class="col-md-3">\
                                            <label>Bienes transportados</label>\
                                            <br><div class="detallemercancia">'+Descripcion_m+'</div>\
                                          </div>\
                                          <div class="col-md-2">\
                                            <label>Cantidad</label>\
                                            <br><div class="detallemercancia">'+Cantidad_m+'</div>\
                                          </div>\
                                          <div class="col-md-2">\
                                            <label>Unidad</label>\
                                            <br><div class="detallemercancia">'+Unidad_m+'</div>\
                                          </div>\
                                          <div class="col-md-2">\
                                            <label>Dimensiones</label>\
                                            <br><div class="detallemercancia">'+Dimensiones_m+'</div>\
                                          </div>\
                                          <div class="col-md-3">\
                                            <label>Embalaje</label>\
                                            <br><div class="detallemercancia">'+DescripEmbalaje_m+'</div>\
                                          </div>\
                            </div><div class="row">\
                                          <div class="col-md-2">\
                                            <label>Peso en Kg</label>\
                                            <br><div class="detallemercancia">'+PesoEnKg_m+'</div>\
                                          </div>\
                                          <div class="col-md-2">\
                                            <label>Valor mercancía</label>\
                                            <br><div class="detallemercancia">'+ValorMercancia_m+'</div>\
                                          </div>\
                                          <div class="col-md-3">\
                                            <label>Moneda</label>\
                                            <br><div class="detallemercancia">'+Moneda_m_t+'</div>\
                                          </div>\
                            </div>\
                    </td>\
                    <td>\
                        <button type="button" class="btn btn-danger" onclick="deletemercancia('+rowaddmercancia+')"><i class="fa fa-trash"></i></button>\
                    </td>\
              </tr>';

    $('.add_table_mercancia').append(html);
    rowaddmercancia++;
    calcularmercancias();
}
function deletemercancia(row){
    $('.addmercancias_'+row).remove();
    calcularmercancias();
}
function calcularmercancias(){
    var caltidadm=$('#table_mercancias tbody > tr').length;
    $('#NumTotalMercancias').val(caltidadm);

    var PesoEnKg = 0;
    $(".PesoEnKg").each(function() {
        var vstotal = $(this).val();
        PesoEnKg += Number(vstotal);
    });
    $('#PesoBrutoTotal').val(PesoEnKg);
}
function addmodaltransporte(){
    $('#modal_addmodaltransporte').modal({backdrop: 'static', keyboard: false});
}
function addtrasporte(){
    var PermSCT_m = $('#PermSCT_m').val();
    var PermSCT_m_t = $('#PermSCT_m').text();
    var NumPermisoSCT_m = $('#NumPermisoSCT_m').val();
    var NombreAseg_m = $('#NombreAseg_m').val();
    var NumPolizaSeguro_m = $('#NumPolizaSeguro_m').val();
    var ConfigVehicular_m = $('#ConfigVehicular_m').val();
    var ConfigVehicular_m_t = $('#ConfigVehicular_m').text();
    var PlacaVM_m = $('#PlacaVM_m').val();
    var AnioModeloVM_m = $('#AnioModeloVM_m').val();
    var SubTipoRem_m=$('#SubTipoRem_m option:selected').val();
    var SubTipoRem_m_t=$('#SubTipoRem_m option:selected').text();
    var Placa_m=$('#Placa_m').val();

    var html='<tr>\
                <td>\
                    <input id="PermSCT" value="'+PermSCT_m+'" type="hidden">\
                    <input id="NumPermisoSCT" value="'+NumPermisoSCT_m+'" type="hidden">\
                    <input id="NombreAseg" value="'+NombreAseg_m+'" type="hidden">\
                    <input id="NumPolizaSeguro" value="'+NumPolizaSeguro_m+'" type="hidden">\
                    <input id="ConfigVehicular" value="'+ConfigVehicular_m+'" type="hidden">\
                    <input id="PlacaVM" value="'+PlacaVM_m+'" type="hidden">\
                    <input id="AnioModeloVM" value="'+AnioModeloVM_m+'" type="hidden">\
                    <input id="SubTipoRem" value="'+SubTipoRem_m+'" type="hidden">\
                    <input id="Placa" value="'+Placa_m+'" type="hidden">\
                    <div class="row">\
                                          <div class="col-md-3">\
                                            <label>Tipo permiso SCT</label>\
                                            <br><div class="detallemercancia">'+PermSCT_m_t+'</div>\
                                          </div>\
                                          <div class="col-md-3">\
                                            <label>Número de permiso SCT</label>\
                                            <br><div class="detallemercancia">'+NumPermisoSCT_m+'</div>\
                                          </div>\
                                          <div class="col-md-3">\
                                            <label>Nombre aseguradora</label>\
                                            <br><div class="detallemercancia">'+NombreAseg_m+'</div>\
                                          </div>\
                                          <div class="col-md-3">\
                                            <label>Número póliza seguro</label>\
                                            <br><div class="detallemercancia">'+NumPolizaSeguro_m+'</div>\
                                          </div>\
                                          <div class="col-md-3">\
                                            <label>Config. Vehicular</label>\
                                            <br><div class="detallemercancia">'+ConfigVehicular_m+'</div>\
                                          </div>\
                                          <div class="col-md-3">\
                                            <label>Placa Vehículo Motor</label>\
                                            <br><div class="detallemercancia">'+PlacaVM_m+'</div>\
                                          </div>\
                                          <div class="col-md-3">\
                                            <label>Año modelo Vehículo Motor</label>\
                                            <br><div class="detallemercancia">'+AnioModeloVM_m+'</div>\
                                          </div>\
                                          <div class="col-md-3">\
                                            <label>Subtipo de remolque </label>\
                                            <br><div class="detallemercancia">'+SubTipoRem_m_t+'</div>\
                                          </div>\
                                          <div class="col-md-3">\
                                            <label>Placa</label>\
                                            <br><div class="detallemercancia">'+Placa_m+'</div>\
                                          </div>\
                                        </div>\
                </td>\
                <td>\
                    <button type="button" class="btn btn-danger" onclick="deletetrasporte()"><i class="fa fa-trash"></i></button>\
                </td></tr>';
    $('.add_table_transporte').html(html);
}
function deletetrasporte(){
    $('.add_table_transporte').html('');
}
function addmodalaereo(){
    $('#modal_addmodalaereo').modal({backdrop: 'static', keyboard: false});
}
function addtrasporteaereo(){
    var PermSCT2_m = $('#PermSCT2_m').val();
    var PermSCT2_m_t = $('#PermSCT2_m').text();
    var NumPermisoSCT2_m = $('#NumPermisoSCT2_m').val();
    var MatriculaAeronave_m = $('#MatriculaAeronave_m').val().toUpperCase();
    var NombreAseg2_m = $('#NombreAseg2_m').val();
    var NumPolizaSeguro2_m = $('#NumPolizaSeguro2_m').val();
    var NumeroGuia_m = $('#NumeroGuia_m').val();
    var LugarContrato_m = $('#LugarContrato_m').val();
    var RFCTransportista_m = $('#RFCTransportista_m').val();
    var CodigoTransportista_m = $('#CodigoTransportista_m').val();
    var CodigoTransportista_m_t = $('#CodigoTransportista_m').text();
    var NumRegIdTribTranspora_m = $('#NumRegIdTribTranspora_m').val();
    var ResidenciaFiscalTranspor_m = $('#ResidenciaFiscalTranspor_m').val();
    var NombreTransportista_m = $('#NombreTransportista_m').val();
    var RFCEmbarcador_m = $('#RFCEmbarcador_m').val().toUpperCase();
    var NumRegIdTribEmbarc_m = $('#NumRegIdTribEmbarc_m').val();
    var ResidenciaFiscalEmbarc_m = $('#ResidenciaFiscalEmbarc_m').val().toUpperCase();
    var NombreEmbarcador_m = $('#NombreEmbarcador_m').val().toUpperCase();
    var html='<tr>\
                <td>\
                    <input type="hidden" id="PermSCT" value="'+PermSCT2_m+'">\
                    <input type="hidden" id="NumPermisoSCT" value="'+NumPermisoSCT2_m+'">\
                    <input type="hidden" id="MatriculaAeronave" value="'+MatriculaAeronave_m+'">\
                    <input type="hidden" id="NombreAseg" value="'+NombreAseg2_m+'">\
                    <input type="hidden" id="NumPolizaSeguro" value="'+NumPolizaSeguro2_m+'">\
                    <input type="hidden" id="NumeroGuia" value="'+NumeroGuia_m+'">\
                    <input type="hidden" id="LugarContrato" value="'+LugarContrato_m+'">\
                    <input type="hidden" id="RFCTransportista" value="'+RFCTransportista_m+'">\
                    <input type="hidden" id="CodigoTransportista" value="'+CodigoTransportista_m+'">\
                    <input type="hidden" id="NumRegIdTribTranspora" value="'+NumRegIdTribTranspora_m+'">\
                    <input type="hidden" id="ResidenciaFiscalTranspor" value="'+ResidenciaFiscalTranspor_m+'">\
                    <input type="hidden" id="NombreTransportista" value="'+NombreTransportista_m+'">\
                    <input type="hidden" id="RFCEmbarcador" value="'+RFCEmbarcador_m+'">\
                    <input type="hidden" id="NumRegIdTribEmbarc" value="'+NumRegIdTribEmbarc_m+'">\
                    <input type="hidden" id="ResidenciaFiscalEmbarc" value="'+ResidenciaFiscalEmbarc_m+'">\
                    <input type="hidden" id="NombreEmbarcador" value="'+NombreEmbarcador_m+'">\
                    <div class="row">\
                          <div class="col-md-3">\
                                <label>Tipo permiso SCT*</label>\
                                <br><div class="detallemercancia">'+PermSCT2_m_t+'</div>\
                          </div>\
                          <div class="col-md-3">\
                                <label>Número de permiso SCT*</label>\
                                <br><div class="detallemercancia">'+NumPermisoSCT2_m+'</div>\
                          </div>\
                          <div class="col-md-3">\
                                <label>Matrícula de Aeronave*</label>\
                                <br><div class="detallemercancia">'+MatriculaAeronave_m+'</div>\
                          </div>\
                          <div class="col-md-3">\
                                <label>Nombre aseguradora</label>\
                                <br><div class="detallemercancia">'+NombreAseg2_m+'</div>\
                          </div>\
                    </div>\
                    <div class="row">\
                          <div class="col-md-3">\
                                <label>Número póliza seguro</label>\
                                <br><div class="detallemercancia">'+NumPolizaSeguro2_m+'</div>\
                          </div>\
                          <div class="col-md-3">\
                                <label>Número de guía</label>\
                                <br><div class="detallemercancia">'+NumeroGuia_m+'</div>\
                          </div>\
                          <div class="col-md-3">\
                                <label>Lugar del contrato</label>\
                                <br><div class="detallemercancia">'+LugarContrato_m+'</div>\
                          </div>\
                          <div class="col-md-3">\
                                <label>RFC del transportista</label>\
                                <br><div class="detallemercancia">'+RFCTransportista_m+'</div>\
                          </div>\
                    </div>\
                    <div class="row">\
                          <div class="col-md-3">\
                                <label>Código de Transportista*</label>\
                                <br><div class="detallemercancia">'+CodigoTransportista_m_t+'</div>\
                          </div>\
                          <div class="col-md-3">\
                                <label>Número de identificación o registro fiscal del transportista</label>\
                                <br><div class="detallemercancia">'+NumRegIdTribTranspora_m+'</div>\
                          </div>\
                          <div class="col-md-3" style="padding-right: 9px; padding-left: 9px; display: none;">\
                                <label>Residencia fiscal transportista</label>\
                                <br><div class="detallemercancia">'+ResidenciaFiscalTranspor_m+'</div>\
                          </div>\
                          <div class="col-md-3">\
                                <label>Nombre del transportista</label>\
                                <br><div class="detallemercancia">'+NombreTransportista_m+'</div>\
                          </div>\
                    </div>\
                    <div class="row">\
                          <div class="col-md-3">\
                                <label>RFC del embarcador</label>\
                                <br><div class="detallemercancia">'+RFCEmbarcador_m+'</div>\
                          </div>\
                          <div class="col-md-3">\
                                <label>Número de identificación o registro fiscal del embarcador</label>\
                                <br><div class="detallemercancia">'+NumRegIdTribEmbarc_m+'</div>\
                          </div>\
                          <div class="col-md-3">\
                                <label>Residencia fiscal embarcador</label>\
                                <br><div class="detallemercancia">'+ResidenciaFiscalEmbarc_m+'</div>\
                          </div>\
                          <div class="col-md-3">\
                                <label>Nombre del embarcador</label>\
                                <br><div class="detallemercancia">'+NombreEmbarcador_m+'</div>\
                          </div>\
                    </div>\
                </td>\
                <td>\
                    <button type="button" class="btn btn-danger" onclick="deletetrasporteaereo()"><i class="fa fa-trash"></i></button>\
                </td>\
            </tr>';

    $('.add_table_aereo').html(html);
}
function deletetrasporteaereo(){
    $('.add_table_aereo').html('');
}
function addmodaloperadores(){
    $('#modal_addmodaloperadores').modal({backdrop: 'static', keyboard: false});
}
function adddomicilio_m(){
    if($('#adddomicilio_m').is(':checked')){
        $('.adddomicilio_m').show('show');
    }else{
        $('.adddomicilio_m').hide('show');
    }
}
var rowoperadores=0;
function addoperadores(){
    var RFCOperador_m = $('#RFCOperador_m').text();
    var NumLicencia_m = $('#NumLicencia_m').val();
    var NombreOperador_m = $('#NombreOperador_m').val();
    var NumRegIdTribOperador_m = $('#NumRegIdTribOperador_m').val();
    var ResidenciaFiscalOperador_m = $('#ResidenciaFiscalOperador_m').val();
    var ResidenciaFiscalOperador_m_t = $('#ResidenciaFiscalOperador_m').text();
    var adddomicilio_m = $('#adddomicilio_m').is(':checked')==true?1:0;
    var Calle_m = $('#Calle_m').val();
    var NumeroExterior_m = $('#NumeroExterior_m').val();
    var NumeroInterior_m = $('#NumeroInterior_m').val();
    var Colonia_m = $('#Colonia_m').val();
    var Colonia_m_t = $('#Colonia_m').text();
    var Localidad_m = $('#Localidad_m').val();
    var Localidad_m_t = $('#Localidad_m').text();
    var Referencia_m = $('#Referencia_m').val();
    var Municipio_m = $('#Municipio_m').val();
    var Municipio_m_t = $('#Municipio_m').text();
    var Estado_m = $('#Estado_m').val();
    var Estado_m_t = $('#Estado_m').text();
    var Pais_m = $('#Pais_m').val();
    var Pais_m_t = $('#Pais_m').text();
    var CodigoPostal_m = $('#CodigoPostal_m').val();
    //<br><div class="detallemercancia">'+PermSCT_m_t+'</div>\
    var html='<tr class="rowoperadores_'+rowoperadores+'">\
                    <td>\
                        <input type="hidden" id="RFCOperador" value="'+RFCOperador_m+'">\
                        <input type="hidden" id="NumLicencia" value="'+NumLicencia_m+'">\
                        <input type="hidden" id="NombreOperador" value="'+NombreOperador_m+'">\
                        <input type="hidden" id="NumRegIdTribOperador" value="'+NumRegIdTribOperador_m+'">\
                        <input type="hidden" id="ResidenciaFiscalOperador" value="'+ResidenciaFiscalOperador_m+'">\
                        <input type="hidden" id="adddomicilio" value="'+adddomicilio_m+'">\
                        <input type="hidden" id="Calle" value="'+Calle_m+'">\
                        <input type="hidden" id="NumeroExterior" value="'+NumeroExterior_m+'">\
                        <input type="hidden" id="NumeroInterior" value="'+NumeroInterior_m+'">\
                        <input type="hidden" id="Colonia" value="'+Colonia_m+'">\
                        <input type="hidden" id="Localidad" value="'+Localidad_m+'">\
                        <input type="hidden" id="Referencia" value="'+Referencia_m+'">\
                        <input type="hidden" id="Municipio" value="'+Municipio_m+'">\
                        <input type="hidden" id="Estado" value="'+Estado_m+'">\
                        <input type="hidden" id="Pais" value="'+Pais_m+'">\
                        <input type="hidden" id="CodigoPostal" value="'+CodigoPostal_m+'">\
                        <div class="row">\
                          <div class="col-md-3">\
                            <label>RFC del operador</label>\
                            <br><div class="detallemercancia">'+RFCOperador_m+'</div>\
                          </div>\
                          <div class="col-md-3">\
                            <label>Número de licencia</label>\
                            <br><div class="detallemercancia">'+NumLicencia_m+'</div>\
                          </div>\
                          <div class="col-md-6">\
                            <label>Nombre del Operador</label>\
                            <br><div class="detallemercancia">'+NombreOperador_m+'</div>\
                          </div>\
                        </div>\
                        <div class="row">\
                          <div class="col-md-6">\
                            <label>Número de identificación o registro fiscal del Operador</label>\
                            <br><div class="detallemercancia">'+NumRegIdTribOperador_m+'</div>\
                          </div>\
                          <div class="col-md-6">\
                            <label>Residencia fiscal operador</label>\
                            <br><div class="detallemercancia">'+ResidenciaFiscalOperador_m_t+'</div>\
                          </div>\
                        </div>';
                        if(adddomicilio_m==1){
                            html+='<div class="row ">\
                              <div class="col-md-3">\
                                <label>Calle*</label>\
                                <br><div class="detallemercancia">'+Calle_m+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Número exterior</label>\
                                <br><div class="detallemercancia">'+NumeroExterior_m+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Número interior</label>\
                                <br><div class="detallemercancia">'+NumeroInterior_m+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Colonia</label>\
                                <br><div class="detallemercancia">'+Colonia_m_t+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Localidad</label>\
                                <br><div class="detallemercancia">'+Localidad_m_t+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Referencia</label>\
                                <br><div class="detallemercancia">'+Referencia_m+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Municipio</label>\
                                <br><div class="detallemercancia">'+Municipio_m_t+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Estado</label>\
                                <br><div class="detallemercancia">'+Estado_m_t+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Pais</label>\
                                <br><div class="detallemercancia">'+Pais_m_t+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Código Postal</label>\
                                <br><div class="detallemercancia">'+CodigoPostal_m+'</div>\
                              </div>\
                            </div>';
                        }
                    html+='</td>\
                    <td>\
                        <button type="button" class="btn btn-danger" onclick="deleteoperadores('+rowoperadores+')"><i class="fa fa-trash"></i></button>\
                    </td>\
                    </tr>';
    $('.add_table_operadores').append(html);
    rowoperadores++;
}
function deleteoperadores(row){
    $('.rowoperadores_'+row).remove();
}

function addmodalpropietario(){
    $('#modal_addmodalpropietario').modal({backdrop: 'static', keyboard: false});
}
function adddomiciliop_m(){
    console.log('adddomiciliop_m');
    if($('#adddomiciliop_m').is(':checked')){
        $('.adddomiciliop_m').show('show');
    }else{
        $('.adddomiciliop_m').hide('show');
    }
}
var rowpropietario=0;
function addpropietario(){
    var RFCPropietario_m = $('#RFCPropietario_m').val();
    var NombrePropietario_m = $('#NombrePropietario_m').val();
    var NumRegIdTribPropietario_m = $('#NumRegIdTribPropietario_m').val();
    var ResidenciaFiscalPropietario_m = $('#ResidenciaFiscalPropietario_m').val();
    var ResidenciaFiscalPropietario_m_t = $('#ResidenciaFiscalPropietario_m').text();
    var adddomiciliop_m = $('#adddomiciliop_m').is(':checked')==true?1:0;
    var Calle_m = $('#Calle_m_p').val();
    var NumeroExterior_m = $('#NumeroExterior_m_p').val();
    var NumeroInterior_m = $('#NumeroInterior_m_p').val();
    var Colonia_m = $('#Colonia_m_p').val();
    var Colonia_m_t = $('#Colonia_m_p').text();
    var Localidad_m = $('#Localidad_m_p').val();
    var Localidad_m_t = $('#Localidad_m_p').text();
    var Referencia_m = $('#Referencia_m_p').val();
    var Municipio_m = $('#Municipio_m_p').val();
    var Municipio_m_t = $('#Municipio_m_p').text();
    var Estado_m = $('#Estado_m_p').val();
    var Estado_m_t = $('#Estado_m_p').text();
    var Pais_m = $('#Pais_m_p').val();
    var Pais_m_t = $('#Pais_m_p').text();
    var CodigoPostal_m = $('#CodigoPostal_m_p').val();
    //<br><div class="detallemercancia">'+PermSCT_m_t+'</div>\
    var html='<tr class="rowpropietario_'+rowpropietario+'">\
                    <td>\
                        <input type="hidden" id="RFCPropietario" value="'+RFCPropietario_m+'">\
                        <input type="hidden" id="NombrePropietario" value="'+NombrePropietario_m+'">\
                        <input type="hidden" id="NumRegIdTribPropietario" value="'+NumRegIdTribPropietario_m+'">\
                        <input type="hidden" id="ResidenciaFiscalPropietario" value="'+ResidenciaFiscalPropietario_m+'">\
                        <input type="hidden" id="adddomiciliop" value="'+adddomiciliop_m+'">\
                        <input type="hidden" id="Calle" value="'+Calle_m+'">\
                        <input type="hidden" id="NumeroExterior" value="'+NumeroExterior_m+'">\
                        <input type="hidden" id="NumeroInterior" value="'+NumeroInterior_m+'">\
                        <input type="hidden" id="Colonia" value="'+Colonia_m+'">\
                        <input type="hidden" id="Localidad" value="'+Localidad_m+'">\
                        <input type="hidden" id="Referencia" value="'+Referencia_m+'">\
                        <input type="hidden" id="Municipio" value="'+Municipio_m+'">\
                        <input type="hidden" id="Estado" value="'+Estado_m+'">\
                        <input type="hidden" id="Pais" value="'+Pais_m+'">\
                        <input type="hidden" id="CodigoPostal" value="'+CodigoPostal_m+'">\
                        <div class="row">\
                          <div class="col-md-3">\
                            <label>RFC del Propietario</label>\
                            <br><div class="detallemercancia">'+RFCPropietario_m+'</div>\
                          </div>\
                          <div class="col-md-6">\
                            <label>Nombre del Propietario</label>\
                            <br><div class="detallemercancia">'+NombrePropietario_m+'</div>\
                          </div>\
                        </div>\
                        <div class="row">\
                          <div class="col-md-6">\
                            <label>Número de identificación o registro fiscal del Propietario</label>\
                            <br><div class="detallemercancia">'+NumRegIdTribPropietario_m+'</div>\
                          </div>\
                          <div class="col-md-6">\
                            <label>Residencia fiscal Propietario</label>\
                            <br><div class="detallemercancia">'+ResidenciaFiscalPropietario_m_t+'</div>\
                          </div>\
                        </div>';
                        if(adddomiciliop_m==1){
                            html+='<div class="row ">\
                              <div class="col-md-3">\
                                <label>Calle*</label>\
                                <br><div class="detallemercancia">'+Calle_m+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Número exterior</label>\
                                <br><div class="detallemercancia">'+NumeroExterior_m+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Número interior</label>\
                                <br><div class="detallemercancia">'+NumeroInterior_m+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Colonia</label>\
                                <br><div class="detallemercancia">'+Colonia_m_t+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Localidad</label>\
                                <br><div class="detallemercancia">'+Localidad_m_t+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Referencia</label>\
                                <br><div class="detallemercancia">'+Referencia_m+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Municipio</label>\
                                <br><div class="detallemercancia">'+Municipio_m_t+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Estado</label>\
                                <br><div class="detallemercancia">'+Estado_m_t+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Pais</label>\
                                <br><div class="detallemercancia">'+Pais_m_t+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Código Postal</label>\
                                <br><div class="detallemercancia">'+CodigoPostal_m+'</div>\
                              </div>\
                            </div>';
                        }
                    html+='</td>\
                    <td>\
                        <button type="button" class="btn btn-danger" onclick="deletepropietario('+rowpropietario+')"><i class="fa fa-trash"></i></button>\
                    </td>\
                    </tr>';
    $('.add_table_propietario').append(html);
    rowoperadores++;
}
function deletepropietario(row){
    $('.rowpropietario_'+row).remove();
}
//=====================================================
function addmodalarrendatario(){
    $('#modal_addmodalarrendatario').modal({backdrop: 'static', keyboard: false});
}
function adddomicilioa_m(){
    console.log('adddomicilioa_m');
    if($('#adddomicilioa_m').is(':checked')){
        $('.adddomicilioa_m').show('show');
    }else{
        $('.adddomicilioa_m').hide('show');
    }
}
var rowarrendatario=0;
function addarrendatario(){
    var RFCPropietario_m = $('#RFCArrendatario_m').val();
    var NombrePropietario_m = $('#NombreArrendatario_m').val();
    var NumRegIdTribPropietario_m = $('#NumRegIdTribArrendatario_m').val();
    var ResidenciaFiscalPropietario_m = $('#ResidenciaFiscalArrendatario_m').val();
    var ResidenciaFiscalPropietario_m_t = $('#ResidenciaFiscalArrendatario_m').text();
    var adddomiciliop_m = $('#adddomicilioa_m').is(':checked')==true?1:0;
    var Calle_m = $('#Calle_m_a').val();
    var NumeroExterior_m = $('#NumeroExterior_m_a').val();
    var NumeroInterior_m = $('#NumeroInterior_m_a').val();
    var Colonia_m = $('#Colonia_m_a').val();
    var Colonia_m_t = $('#Colonia_m_a').text();
    var Localidad_m = $('#Localidad_m_a').val();
    var Localidad_m_t = $('#Localidad_m_a').text();
    var Referencia_m = $('#Referencia_m_a').val();
    var Municipio_m = $('#Municipio_m_a').val();
    var Municipio_m_t = $('#Municipio_m_a').text();
    var Estado_m = $('#Estado_m_a').val();
    var Estado_m_t = $('#Estado_m_a').text();
    var Pais_m = $('#Pais_m_a').val();
    var Pais_m_t = $('#Pais_m_a').text();
    var CodigoPostal_m = $('#CodigoPostal_m_a').val();
    //<br><div class="detallemercancia">'+PermSCT_m_t+'</div>\
    var html='<tr class="rowarrendatario_'+rowarrendatario+'">\
                    <td>\
                        <input type="hidden" id="RFCArrendatario" value="'+RFCPropietario_m+'">\
                        <input type="hidden" id="NombreArrendatario" value="'+NombrePropietario_m+'">\
                        <input type="hidden" id="NumRegIdTribArrendatario" value="'+NumRegIdTribPropietario_m+'">\
                        <input type="hidden" id="ResidenciaFiscalArrendatario" value="'+ResidenciaFiscalPropietario_m+'">\
                        <input type="hidden" id="adddomicilioa" value="'+adddomiciliop_m+'">\
                        <input type="hidden" id="Calle" value="'+Calle_m+'">\
                        <input type="hidden" id="NumeroExterior" value="'+NumeroExterior_m+'">\
                        <input type="hidden" id="NumeroInterior" value="'+NumeroInterior_m+'">\
                        <input type="hidden" id="Colonia" value="'+Colonia_m+'">\
                        <input type="hidden" id="Localidad" value="'+Localidad_m+'">\
                        <input type="hidden" id="Referencia" value="'+Referencia_m+'">\
                        <input type="hidden" id="Municipio" value="'+Municipio_m+'">\
                        <input type="hidden" id="Estado" value="'+Estado_m+'">\
                        <input type="hidden" id="Pais" value="'+Pais_m+'">\
                        <input type="hidden" id="CodigoPostal" value="'+CodigoPostal_m+'">\
                        <div class="row">\
                          <div class="col-md-3">\
                            <label>RFC del Arrendatario</label>\
                            <br><div class="detallemercancia">'+RFCPropietario_m+'</div>\
                          </div>\
                          <div class="col-md-6">\
                            <label>Nombre del Arrendatario</label>\
                            <br><div class="detallemercancia">'+NombrePropietario_m+'</div>\
                          </div>\
                        </div>\
                        <div class="row">\
                          <div class="col-md-6">\
                            <label>Número de identificación o registro fiscal del Arrendatario</label>\
                            <br><div class="detallemercancia">'+NumRegIdTribPropietario_m+'</div>\
                          </div>\
                          <div class="col-md-6">\
                            <label>Residencia fiscal Arrendatario</label>\
                            <br><div class="detallemercancia">'+ResidenciaFiscalPropietario_m_t+'</div>\
                          </div>\
                        </div>';
                        if(adddomiciliop_m==1){
                            html+='<div class="row ">\
                              <div class="col-md-3">\
                                <label>Calle*</label>\
                                <br><div class="detallemercancia">'+Calle_m+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Número exterior</label>\
                                <br><div class="detallemercancia">'+NumeroExterior_m+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Número interior</label>\
                                <br><div class="detallemercancia">'+NumeroInterior_m+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Colonia</label>\
                                <br><div class="detallemercancia">'+Colonia_m_t+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Localidad</label>\
                                <br><div class="detallemercancia">'+Localidad_m_t+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Referencia</label>\
                                <br><div class="detallemercancia">'+Referencia_m+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Municipio</label>\
                                <br><div class="detallemercancia">'+Municipio_m_t+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Estado</label>\
                                <br><div class="detallemercancia">'+Estado_m_t+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Pais</label>\
                                <br><div class="detallemercancia">'+Pais_m_t+'</div>\
                              </div>\
                              <div class="col-md-3">\
                                <label>Código Postal</label>\
                                <br><div class="detallemercancia">'+CodigoPostal_m+'</div>\
                              </div>\
                            </div>';
                        }
                    html+='</td>\
                    <td>\
                        <button type="button" class="btn btn-danger" onclick="deletearrendatario('+rowarrendatario+')"><i class="fa fa-trash"></i></button>\
                    </td>\
                    </tr>';
    $('.add_table_arrendatario').append(html);
    rowarrendatario++;
}
function deletearrendatario(row){
    $('.rowarrendatario_'+row).remove();
}

function get_operador(){
    $('#RFCOperador_m').select2({
        width: 'resolve',
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar:',
        ajax: {
            url: base_url+'Facturas/search_operador',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.rfc_del_operador,
                        text: element.rfc_del_operador,
                        no_licencia: element.no_licencia,
                        operador: element.operador,
                        num_identificacion: element.num_identificacion,
                        residencia_fiscal: element.residencia_fiscal,                        
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        //get_estado(data.c_Pais);
        $('#NumLicencia_m').val(data.no_licencia);
        $('#NombreOperador_m').val(data.operador);
        $('#NumRegIdTribOperador_m').val(data.num_identificacion);
        $('#ResidenciaFiscalOperador_m').html('').select2({data: [{id: '', text: ''}]});
        var datax = {
            id: data.residencia_fiscal,
            text: data.residencia_fiscal
        };
        var id_rf=data.residencia_fiscal;
        var newOption = new Option(datax.text, datax.id, false, false);
        $('#ResidenciaFiscalOperador_m').append(newOption).trigger('change');
        setTimeout(function(){   
        $('#ResidenciaFiscalOperador_m').val(id_rf).trigger('change.select2');
        }, 400);
    });       
}

function get_transporte(){
    $('#transportef').select2({
        width: 'resolve',
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar:',
        ajax: {
            url: base_url+'Facturas/search_transporte',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.dpl+' '+element.nombre_aseguradora,  
                        tipo_permiso_sct: element.tipo_permiso_sct,
                        tipo_permiso_scttxt: element.tipo_permiso_scttxt,
                        num_permiso_sct: element.num_permiso_sct,
                        configuracion_vhicular: element.configuracion_vhicular,
                        configuracion_vhiculartxt: element.configuracion_vhiculartxt,      
                        placa_vehiculo_motor: element.placa_vehiculo_motor,
                        anio_modelo_vihiculo_motor: element.anio_modelo_vihiculo_motor,
                        subtipo_remolque: element.subtipo_remolque,
                        placa_remolque: element.placa_remolque,
                        nombre_aseguradora: element.nombre_aseguradora,
                        num_poliza_seguro: element.num_poliza_seguro,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        
        $('#NumPermisoSCT_m').val(data.num_permiso_sct);
        $('#NombreAseg_m').val(data.nombre_aseguradora);      
        $('#NumPolizaSeguro_m').val(data.num_poliza_seguro);
        //$('#ConfigVehicular_m').val(data.configuracion_vhicular);
        $('#PlacaVM_m').val(data.placa_vehiculo_motor);
        $('#AnioModeloVM_m').val(data.anio_modelo_vihiculo_motor);
        //$('#SubTipoRem_m').val(data.subtipo_remolque);
        $('#Placa_m').val(data.placa_remolque);
        $('#SubTipoRem_m option:selected').remove();
        $("#SubTipoRem_m option[value="+data.subtipo_remolque+"]").attr("selected",true);
        var datax = {
            id: data.tipo_permiso_sct,
            text: data.tipo_permiso_scttxt
        };
        $('#PermSCT_m').html('').select2({data: [{id: '', text: ''}]});
        var id_rf=data.tipo_permiso_sct;
        var newOption = new Option(datax.text, datax.id, false, false);
        $('#PermSCT_m').append(newOption).trigger('change');
        setTimeout(function(){   
        $('#PermSCT_m').val(id_rf).trigger('change.select2');
        }, 400);
        
        var datay = {
            id: data.configuracion_vhicular,
            text: data.configuracion_vhiculartxt
        };
        $('#ConfigVehicular_m').html('').select2({data: [{id: '', text: ''}]});
        var id_cv=data.configuracion_vhicular;
        var newOption = new Option(datay.text, datay.id, false, false);
        $('#ConfigVehicular_m').append(newOption).trigger('change');
        setTimeout(function(){   
        $('#ConfigVehicular_m').val(id_cv).trigger('change.select2');
        }, 400);    
    }); 
        
}
function obteneridod(id,tipo){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/General/obteneridod",
        data: {
            id:id,
            tipo:tipo
        },
        success: function (response){
           var array = $.parseJSON(response);
           array.forEach(function(element) {
            console.log(element.idorigen);
            $('#IDUbicacion_'+tipo).val(element.idorigen);
           });
        },
        error: function(response){
             
             
        }
    });
}