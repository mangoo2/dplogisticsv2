var base_url=$('#base_url').val();
var numberrow=0;
var validadreturn=false;
var iddatosfiscales;
var sucursal_s=$('#sucursal_s').val();
var bolavar=0;
$(document).ready(function () {
  wizard=$(".icons-tab-steps").steps({
    headerTag: "h6",
    bodyTag: "fieldset",
    transitionEffect: "fade",
    titleTemplate: '<span class="step">#index#</span> #title#',
    enableAllSteps: true,
    labels: {
      next: 'Siguiente',
      previous: 'Anterior',
      finish: 'Finalizar'
    },
    onStepChanging: function (event, currentIndex, newIndex) {
      console.log('newIndex: '+newIndex);
      console.log('currentIndex: '+currentIndex);
      bolavar=newIndex;
      var validate=false;
      if(currentIndex==0){
          console.log('formsaveupdate2');
          return formsaveupdate1();
      }
      if(currentIndex==1){
          console.log('formsaveupdate2');
          return formsaveupdate2();
      }
      if(currentIndex==2){
          console.log('formsaveupdate3');
          return formsaveupdate3();
      }
      if(currentIndex==3){
          return formsaveupdate4();
      }
      
      /*
      if (!validate()) {
            // alert and stop when some validation failed
            alert("Error");
            return false;
        } else {
            // proceed
            return true; 
        }
        */
    },
    onFinished: function (event, currentIndex,newIndex) {
      formsaveupdate4();
      return false;
    }
  });

  // To select event date

  // add btn class
  //$('.wizard .actions a[role="menuitem"]').addClass("btn btn-primary");
  $('.wizard .actions a[href="#previous"]').addClass("btn btn-secondary");
  $('.wizard .actions a[href="#next"]').addClass("btn btn-success");
  $('.wizard .actions a[href="#finish"]').addClass("btn btn-success");

  $('#cot_remite').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Documentar/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.cliente,
                        text: element.cli_nombre+' '+element.cli_paterno+' '+element.cli_materno+' ('+element.rid+')',
                        credito: element.credito,

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
      var data = e.params.data;
        obtenerinfocliente(data.id);
        if(data.credito==0){
          $('.optioncre').prop('disabled',true);
        }
    });
  ontenerinformacion();
  $("#escaneofile").fileinput({
            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["png","jpg","jpeg","bmp","pdf"],
            browseLabel: 'Seleccionar escaneo',
            uploadUrl: base_url+'Documentar/imagenrfc_escaneo',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            },
            uploadExtraData: function (previewId, index) {
              var info = {
                    id:iddatosfiscales

                  };
              return info;
          }
    });
  $("#situacion_fiscal_file").fileinput({
            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["png","jpg","jpeg","bmp","pdf"],
            browseLabel: 'Seleccionar Constancia',
            uploadUrl: base_url+'Documentar/imagenrfc_situacionf',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            },
            uploadExtraData: function (previewId, index) {
              var info = {
                    id:iddatosfiscales

                  };
              return info;
          }
    });
  $('#cot_guia').change(function(event) {
    if($('#cot_guia').val().length>0){
      $('#singuia').hide('show');
      validarcot_guia();
    }else{
      $('#singuia').show('show');
    }
  });

});
function obtenerinfo(){
  var aeroniliea=$('#aero option:selected').val();
  $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Documentar/ob_cotclasif",
                        data: {
                            aereo:aeroniliea
                        },
                        success: function (response){
                          var array = $.parseJSON(response);
                          $('#cot_clasif').html(array.clasificacion);
                          $('#tiposervicio').html(array.tiposervicio);
                        },
                        error: function(response){
                            toastr["error"]("Algo salio mal", "Error"); 
                             
                        }
                    });
}
function obtenerinfocliente(codigo){
  $.ajax({
      type:'POST',
      url: base_url+"index.php/Documentar/ob_datoscli",
      data: {
          codigo:codigo
      },
      success: function (response){
        var array = $.parseJSON(response);
        if(array.num==1){
          $('.infohtml_direccion').html(array.datos.cli_calle+' / '+array.datos.cli_numext+' / '+array.datos.cli_numint);
          $('.infohtml_colonia').html(array.datos.cli_colonia);
          $('.infohtml_ciudad').html(array.datos.cli_municipio+' '+array.datos.cli_entidad);
          $('.infohtml_tel').html(array.datos.cli_celular);
          $('.infohtml_cp').html(array.datos.cli_cp);
          var telefono='';
          if(array.datos.cli_telefono==''){
            var telefono=array.datos.cli_celular;
          }else{
            var telefono=array.datos.cli_telefono;
          }
          var html='<div class="col-md-2">\
                        <label class="col-form-label">Dirección</label>\
                    </div>\
                    <div class="col-md-10">\
                        '+array.datos.cli_calle+' / '+array.datos.cli_numext+' / '+array.datos.cli_numint+' '+array.datos.cli_colonia+' '+array.datos.cli_municipio+' '+array.datos.cli_entidad+'\
                    </div>\
                    <div class="col-md-2">\
                        <label class="col-form-label">C.P.:</label>\
                    </div>\
                    <div class="col-md-2">\
                        '+array.datos.cli_cp+'\
                    </div>\
                    <div class="col-md-2">\
                        <label class="col-form-label">Teléfono:</label>\
                    </div>\
                    <div class="col-md-2">\
                        '+telefono+'\
                    </div>\
                    <div class="col-md-2">\
                        <label class="col-form-label">Descuento:</label>\
                    </div>\
                    <div class="col-md-1">\
                        '+array.datos.cli_des+'%\
                    </div>\
                    <div class="col-md-1">';
                    if(sucursal_s==array.datos.sucursal){
                        html+='<a type="button" class="btn-sm btn-info mr-1 mb-1" onclick="editcliente('+array.datos.rid+')"><i class="fa fa-pencil fa-fw"></i></a>';
                    }
              html+='</div>';
          $('.infocliente').html(html);
        }else{
          $('.infocliente').html('');
        }
        if(array.consignatarios.length>0){
          var htmloption='';
          array.consignatarios.forEach(function(element) {
              

              htmloption+='<option value="'+element.consign+'">'+element.con_nombre+' '+element.con_paterno+' '+element.con_materno+'</option>';
              
          });
          $('#cot_consign').html(htmloption);
        }else{
          $('#cot_consign').html('<option value="">Sin consignatarios</option>');
        }
        if(array.facturas.length>0){
          var htmloption='';
          array.facturas.forEach(function(element) {
              

              htmloption+='<option value="'+element.factura+'">'+element.fac_rfc+' '+element.fac_nrs+'</option>';
              
          });
          $('#cot_factura').html(htmloption);
        }else{
          $('#cot_factura').html('<option value="">Sin Factura</option>');
        }
      },
      error: function(response){
          toastr["error"]("Algo salio mal", "Error"); 
           
      }
  });
}

function addpaquete(){
  var cotizacion = $('#cotizacion').val();
  addpaquete_row(numberrow,0,cotizacion,1,0,0,0,0);
  numberrow++;
}
function addpaquete_row(row,id,cot,cant,lar,anc,alt,peso){

  //var vol=(((parseFloat(alt)*parseFloat(anc)*parseFloat(lar))/6000)*parseFloat(cant)).toFixed(2);
  var vol=(((parseFloat(alt)*parseFloat(anc)*parseFloat(lar))/6000)*parseFloat(cant));
      //vol=Math.floor(vol * 100) / 100;//solo se trunca no se redondea
      vol=vol.toFixed(2);

  var html='<tr class="row_tr_pa_'+row+'">\
                <th>\
                  <input type="hidden" id="rid" value="'+id+'">\
                  <input type="number" id="paq_num" class=" form-control paq_num_'+row+'" value="'+cant+'" onchange="recalcular('+row+')">\
                </th>\
                <th>\
                    <input type="number" id="paq_lar" class=" form-control paq_lar_'+row+'" value="'+lar+'" onchange="recalcular('+row+')">\
                </th>\
                <th>\
                    <input type="number" id="paq_anc" class=" form-control paq_anc_'+row+'" value="'+anc+'" onchange="recalcular('+row+')">\
                </th>\
                <th>\
                    <input type="number" id="paq_alt" class=" form-control paq_alt_'+row+'" value="'+alt+'" onchange="recalcular('+row+')">\
                </th>\
                <th>\
                    <input type="number" id="paq_pes" class="form-control" value="'+peso+'" onchange="recalcular('+row+')">\
                </th>\
                <th>\
                    <input type="number" id="volumen" class="form-control volumen_'+row+'" value="'+vol+'" readonly>\
                </th>\
                <th>\
                  <button type="button" class="btn btn-sm btn-danger mr-1 mb-1"  onclick="deletepaqu('+id+','+row+')"><i class="fa fa-trash-o"></i></button>\
                </th>\
            </tr>';
  $('.tbody_paquetes').append(html);
}
function deletepaqu(id,row){
  if(id==0){
    $('.row_tr_pa_'+row).remove();
  }else{
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma borrar el paquete',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Documentar/deletepaquete",
                        data: {
                            id:id
                        },
                        success: function (response){
                          $('.row_tr_pa_'+row).remove(); 
                          recalculartotales();
                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
  }
}
function recalcular(row){
  var cant = $('.paq_num_'+row).val(); 
  var lar = $('.paq_lar_'+row).val(); 
  var anc = $('.paq_anc_'+row).val(); 
  var alt = $('.paq_alt_'+row).val(); 
  //var vol=(((parseFloat(alt)*parseFloat(anc)*parseFloat(lar))/6000)*parseFloat(cant)).toFixed(2);
  var vol=(((parseFloat(alt)*parseFloat(anc)*parseFloat(lar))/6000)*parseFloat(cant));
      //vol=Math.floor(vol * 100) / 100;//solo se trunca no se redondea
      vol=vol.toFixed(2);
     
  $('.volumen_'+row).val(vol);
  recalculartotales();
}
function recalculartotales(){
  var total_paq_num=0;
  var total_paq_pes=0;
  var total_volumen=0;
  var cot_cos = $('#cot_cos').val()==''?0:$('#cot_cos').val();
  var TABLAp   = $("#table_paquetes tbody > tr");
    TABLAp.each(function(){         
        
        var total_paq_num0 = $(this).find("input[id*='paq_num']").val();
        total_paq_num+=Number(total_paq_num0);

        var total_paq_pes0 = $(this).find("input[id*='paq_pes']").val();
        total_paq_pes+=Number(total_paq_pes0);

        var total_volumen0 = $(this).find("input[id*='volumen']").val();
        total_volumen+=Number(total_volumen0);
        
    });
      total_volumen=total_volumen.toFixed(2);
    $('.totalpaquetes').html(total_paq_num);
      total_paq_pes=total_paq_pes.toFixed(2);
    $('.totalpeso').html(total_paq_pes);
    var cpc = $('#cot_clasif option:selected').data('cpc');
    var pesoconsumible=parseFloat(total_paq_pes)*parseFloat(cpc);
    $('#tar_cpc').val(pesoconsumible);
    console.log('total_volumen:'+total_volumen);
    $('.totalvolumen').html(total_volumen);
    $('.totalpreciokilo').html(cot_cos);
    var ctipo=$('#cot_clasif option:selected').data('ctipo');

    if(ctipo=='uni'){
      var tar_pov = (parseFloat(cot_cos)).toFixed(2);
    }else{
      //se hace la condicion para tomar la cantidad mayor
      if(parseFloat(total_paq_pes)>parseFloat(total_volumen)){
        var total_v_p=total_paq_pes;
      }else{
        var total_v_p=total_volumen;
      }
      //var tar_pov = (parseFloat(total_volumen)*parseFloat(cot_cos)).toFixed(2);
      var tar_pov = (parseFloat(total_v_p)*parseFloat(cot_cos)).toFixed(2);
    }
    
    $('#tar_pov').val(tar_pov);
    calculartotalgeneral();
}
function ontenerpreciosporclasificacion(){
  $('#cot_clasif')
  var ce = $('#cot_clasif option:selected').data('ce');
  var cen = $('#cot_clasif option:selected').data('cen');
  var cf = $('#cot_clasif option:selected').data('cf');
  var cfn = $('#cot_clasif option:selected').data('cfn');
  var fer = $('#cot_clasif option:selected').data('fer');
  var fern = $('#cot_clasif option:selected').data('fern');
  var cpcn = $('#cot_clasif option:selected').data('cpcn');
  var cpc = $('#cot_clasif option:selected').data('cpc');

  $('#tar_cen').val(cen);
  $('#tar_ce').val(ce);
  $('#tar_cfn').val(cfn);
  $('#tar_cf').val(cf);
  $('#tar_fern').val(fern);
  $('#tar_fer').val(fer);
  $('#tar_cpcn').val(cpcn);
  $('#tar_cpc').val(cpc);

  $('.tar_cen').val(cen);
  $('.tar_ce').val(ce);
  $('.tar_cfn').val(cfn);
  $('.tar_cf').val(cf);
  $('.tar_fern').val(fern);
  $('.tar_fer').val(fer);
  $('.tar_cpcn').val(cpcn);
  $('.tar_cpc').val(cpc);

  calculartotalgeneral();
}
function calculartotalgeneral(){
  var tar_ce = $('#tar_ce').val();
  var tar_cf = $('#tar_cf').val();
  var tar_pov = $('#tar_pov').val();
  var tar_fer = $('#tar_fer').val();
  var tar_cpc = $('#tar_cpc').val();

  var tar_sub =(parseFloat(tar_ce)+parseFloat(tar_cf)+parseFloat(tar_pov)+parseFloat(tar_fer)+parseFloat(tar_cpc)).toFixed(2);
  $('#tar_sub').val(tar_sub);
  var tar_iva =(parseFloat(tar_sub)*0.16).toFixed(2);
  $('#tar_iva').val(tar_iva);
  var tar_tot =(parseFloat(tar_sub)+parseFloat(tar_iva)).toFixed(0);//se solicito que se redondeara el total para no tener decimales
  $('#tar_tot').val(tar_tot);
  $('#cot_importe').val(tar_tot);
  //$('#cot_cantidad').prop({"min" : tar_tot});
}
function formsaveupdate1(){
  //=============================================
              var form_1 = $('#form_1');
              var form_1_v=form_1.valid();
              var cotizacion=$('#cotizacion').val();
              var DATAf1  = [];
                    var TABLAf1   = $("#table_paquetes tbody > tr");
                    TABLAf1.each(function(){         
                        item = {};

                        item ["rid"]      = $(this).find("input[id*='rid']").val();
                        item ["paq_num"]  = $(this).find("input[id*='paq_num']").val();
                        item ["paq_lar"]  = $(this).find("input[id*='paq_lar']").val();
                        item ["paq_anc"]  = $(this).find("input[id*='paq_anc']").val();
                        item ["paq_alt"]  = $(this).find("input[id*='paq_alt']").val();
                        item ["paq_pes"]  = $(this).find("input[id*='paq_pes']").val();

                        DATAf1.push(item);
                    });
                    arraypaquetes   = JSON.stringify(DATAf1);
                var datos = 'cotizacion='+cotizacion+'&'+form_1.serialize()+'&arraypaquetes='+arraypaquetes+'&'+$('#form_doc_tars').serialize();
                if(TABLAf1.length>0 && form_1_v==true){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Documentar/saveinsertupdate1",
                        data: datos,
                        success: function (response){
                            ontenerinformacion();
                            
                        },
                        error: function(response){
                          return false; 
                            toastr["error"]("Algo salio mal", "Error");
                        }
                    });
                }else{
                  validadreturn= false; 
                    toastr["error"]("Verifica los campos requeridos y/o agregar 1 o mas paquetes", "Error");
                }
          //=============================================
  
  return form_1_v;
}
function ontenerinformacion(){
  var cotizacion=$('#cotizacion').val();
  $.ajax({
      type:'POST',
      url: base_url+"index.php/Documentar/obtenerinfor",
      data: {
        cotiz:cotizacion
      },
      success: function (data){
        var array = $.parseJSON(data);
            console.log(array);
            if(array.doc_cotiza.length>0){
              $('#aero').val(array.doc_cotiza[0].aero).change();
              $('#cot_rutori').val(array.doc_cotiza[0].cot_rutori);
              $('#cot_rutdes').val(array.doc_cotiza[0].cot_rutdes);

              $('#v_origen').val(array.doc_cotiza[0].v_origen);
              $('#v_destino').val(array.doc_cotiza[0].v_destino);
              $('#vfecha').val(array.doc_cotiza[0].vfecha);
              if(array.doc_cotiza[0].novuelo!=''){
                $('#novuelo').html('<option>'+array.doc_cotiza[0].novuelo+'</option>');  
              }
              

              $('.cot_folio').html(array.doc_cotiza[0].cot_folio);
              if(array.doc_cotiza[0].cot_guia!=''){
                $('#cot_guia').val(array.doc_cotiza[0].cot_guia);
                $('.cot_guia').html(array.doc_cotiza[0].cot_guia);
                $('#singuia').hide();
              }else{
                $('#singuia').show();
              }
              
              setTimeout(function(){ 
                $('#cot_clasif').val(array.doc_cotiza[0].cot_clasif);
                $('#tiposervicio').val(array.doc_cotiza[0].tiposervicio);
                if(array.doc_cotiza[0].tiposervicio==2 || array.doc_cotiza[0].tiposervicio==3){
                  $('#facturar').prop('disabled',true);
                }else{
                  $('#facturar').prop('disabled',false);
                }
                setTimeout(function(){ 
                  $('.infohtml_rclass').html($('#cot_clasif option:selected').text());
                }, 1500);
              }, 1000);
              $('#cot_cos').val(array.doc_cotiza[0].cot_cos).change();
              $('#cot_descrip').val(array.doc_cotiza[0].cot_descrip).change();
              $('.infohtml_rdescripcion').html(array.doc_cotiza[0].cot_descrip);
              $('#cot_remite').html('<option value="'+array.doc_cotiza[0].cot_remite+'">'+array.cliente+'</option>');
              if(array.credito==0){
                $('.optioncre').prop('disabled',true);
              }
              $('.infohtml_remitente').html(array.cliente);
              obtenerinfocliente(array.doc_cotiza[0].cot_remite); 
              if(array.doc_cotiza[0].cot_consign!=''){
                setTimeout(function(){ 
                  $('#cot_consign').val(array.doc_cotiza[0].cot_consign);
                  asignar_consig2(array.doc_cotiza[0].cot_consign);
                }, 1000);
              }
              if(array.doc_cotiza[0].cot_factura!=''){
                setTimeout(function(){ 
                  $('#cot_factura').val(array.doc_cotiza[0].cot_factura);
                  asignar_factura2(array.doc_cotiza[0].cot_factura);
                }, 1000);
              }
              if(array.doc_cotiza[0].facturar==1){
                $('#facturar').prop('checked', true);
              }
              $('.infohtml_rorigen').html($('#cot_rutori option:selected').text());
              $('.infohtml_rdestino').html($('#cot_rutdes option:selected').text());
              
              if(array.doc_cotiza[0].cot_fdp=='efe'){
                $('#cot_fdp').val(array.doc_cotiza[0].cot_fdp).attr('disabled',true).change();
                $('#cot_cantidad').val(array.doc_cotiza[0].cot_cantidad);
                $('#cot_cambio').val(array.doc_cotiza[0].cot_cambio);
              }
              if(array.doc_cotiza[0].cot_fdp=='cre'){
                $('.cot_fdp_efe').hide('show');
                $('.cot_fdp_tar').hide('show');
                $('.cot_fdp_cre').show('show'); 
                $('#cot_fdp').val(array.doc_cotiza[0].cot_fdp).attr('disabled',true);
                $('#tipo_pago_credito').val(array.doc_cotiza[0].tipo_pago_credito);
                $('#cot_fecpag').val(array.doc_cotiza[0].cot_fecpag);
              }
              if(array.doc_cotiza[0].cot_fdp=='tar'){
                $('#cot_fdp').val(array.doc_cotiza[0].cot_fdp).attr('disabled',true).change();
                $('#banco_efectivo').val(array.doc_cotiza[0].banco_efectivo);
                $('#banco_credito').val(array.doc_cotiza[0].banco_credito);
              }
              if(array.doc_cotiza[0].cot_fdp=='efetar' ){
                $('#cot_fdp').val(array.doc_cotiza[0].cot_fdp).attr('disabled',true).change();
                $('#cot_cantidad').val(array.doc_cotiza[0].cot_cantidad);
                $('#cot_cambio').val(array.doc_cotiza[0].cot_cambio);
                $('#banco_efectivo').val(array.doc_cotiza[0].banco_efectivo);
                $('#banco_credito').val(array.doc_cotiza[0].banco_credito);
              }



            }
            $('.tbody_paquetes').html('');
            array.doc_paqs.forEach(function(element) {
              addpaquete_row(numberrow,element.rid,element.cotiza,element.paq_num,element.paq_lar,element.paq_anc,element.paq_alt,element.paq_pes);
              numberrow++;
            });
            recalculartotales();
            if(array.doc_tars.length>0){
              $('#tar_cen').val(array.doc_tars[0].tar_cen);
              $('#tar_ce').val(array.doc_tars[0].tar_ce);
              $('#tar_cfn').val(array.doc_tars[0].tar_cfn);
              $('#tar_cf').val(array.doc_tars[0].tar_cf);
              $('#tar_povn').val(array.doc_tars[0].tar_povn);
              $('#tar_pov').val(array.doc_tars[0].tar_pov);
              $('#tar_fern').val(array.doc_tars[0].tar_fern);
              $('#tar_fer').val(array.doc_tars[0].tar_fer);
              $('#tar_cpcn').val(array.doc_tars[0].tar_cpcn);
              $('#tar_cpc').val(array.doc_tars[0].tar_cpc);
              $('#tar_subn').val(array.doc_tars[0].tar_subn);
              $('#tar_sub').val(array.doc_tars[0].tar_sub);
              $('#tar_ivan').val(array.doc_tars[0].tar_ivan);
              $('#tar_iva').val(array.doc_tars[0].tar_iva);
              $('#tar_totn').val(array.doc_tars[0].tar_totn);
              $('#tar_tot').val(array.doc_tars[0].tar_tot);
              $('#cot_importe').val(array.doc_tars[0].tar_tot);
              //$('#cot_cantidad').prop({"min" : array.doc_tars[0].tar_tot});


              $('.tar_cen').html(array.doc_tars[0].tar_cen);
              $('.tar_ce').html(array.doc_tars[0].tar_ce);
              $('.tar_cfn').html(array.doc_tars[0].tar_cfn);
              $('.tar_cf').html(array.doc_tars[0].tar_cf);
              $('.tar_povn').html(array.doc_tars[0].tar_povn);
              $('.tar_pov').html(array.doc_tars[0].tar_pov);
              $('.tar_fern').html(array.doc_tars[0].tar_fern);
              $('.tar_fer').html(array.doc_tars[0].tar_fer);
              $('.tar_cpcn').html(array.doc_tars[0].tar_cpcn);
              $('.tar_cpc').html(array.doc_tars[0].tar_cpc);
              $('.tar_subn').html(array.doc_tars[0].tar_subn);
              $('.tar_sub').html(array.doc_tars[0].tar_sub);
              $('.tar_ivan').html(array.doc_tars[0].tar_ivan);
              $('.tar_iva').html(array.doc_tars[0].tar_iva);
              $('.tar_totn').html(array.doc_tars[0].tar_totn);
              $('.tar_tot').html(array.doc_tars[0].tar_tot);
            }
          
      },
      error: function(response){
          toastr["error"]("Algo salio mal", "Error");
      }
  });
}
function asignar_consig(){
  var consignatario=$('#cot_consign').val();
  asignar_consig2(consignatario);
}
function asignar_consig2(codigo){
  $.ajax({
      type:'POST',
      url: base_url+"index.php/Documentar/ob_datoscli_consigtarario",
      data: {
          codigo:codigo
      },
      success: function (response){
        var array = $.parseJSON(response);
        if(array.num==1){
          var html='<div class="col-md-2">\
                        <label class="col-form-label">Consignatario</label>\
                    </div>\
                    <div class="col-md-4">\
                        '+array.datos.con_nombre+' / '+array.datos.con_paterno+' / '+array.datos.con_materno+'\
                    </div>\
                    <div class="col-md-2">\
                        <label class="col-form-label">Dirección</label>\
                    </div>\
                    <div class="col-md-4">\
                        '+array.datos.con_calle+' '+array.datos.con_numext+' '+array.datos.con_numint+'\
                    </div>\
                    <div class="col-md-2">\
                        <label class="col-form-label">Colonia:</label>\
                    </div>\
                    <div class="col-md-4">\
                        '+array.datos.con_colonia+'\
                    </div>\
                    <div class="col-md-2">\
                        <label class="col-form-label">Ciudad:</label>\
                    </div>\
                    <div class="col-md-4">\
                        '+array.datos.con_municipio+' / '+array.datos.con_entidad+'\
                    </div>\
                    <div class="col-md-2">\
                        <label class="col-form-label">Teléfono:</label>\
                    </div>\
                    <div class="col-md-4">\
                        '+array.datos.con_telefono+'\
                    </div>\
                    <div class="col-md-2">\
                        <label class="col-form-label">C.P.:</label>\
                    </div>\
                    <div class="col-md-3">\
                        '+array.datos.con_cp+'\
                    </div>\
                    <div class="col-md-1">\
                        <a type="button" class="btn-sm btn-info mr-1 mb-1" onclick="editarconsig('+array.datos.rid+')"><i class="fa fa-pencil fa-fw"></i></a>\
                    </div>';
            var html2='<div class="col-md-12">\
                        <h5>Consignatario</h5>\
                        <hr>\
                    </div>\
                    <div class="col-md-6">\
                        <label class="col-form-label">Consignatario</label>\
                    </div>\
                    <div class="col-md-6">\
                        '+array.datos.con_nombre+' / '+array.datos.con_paterno+' / '+array.datos.con_materno+'\
                    </div>\
                    <div class="col-md-6">\
                        <label class="col-form-label">Dirección</label>\
                    </div>\
                    <div class="col-md-6">\
                        '+array.datos.con_calle+' '+array.datos.con_numext+' '+array.datos.con_numint+'\
                    </div>\
                    <div class="col-md-6">\
                        <label class="col-form-label">Colonia:</label>\
                    </div>\
                    <div class="col-md-6">\
                        '+array.datos.con_colonia+'\
                    </div>\
                    <div class="col-md-6">\
                        <label class="col-form-label">Ciudad:</label>\
                    </div>\
                    <div class="col-md-6">\
                        '+array.datos.con_municipio+' / '+array.datos.con_entidad+'\
                    </div>\
                    <div class="col-md-6">\
                        <label class="col-form-label">Teléfono:</label>\
                    </div>\
                    <div class="col-md-6">\
                        '+array.datos.con_telefono+'\
                    </div>\
                    <div class="col-md-6">\
                        <label class="col-form-label">C.P.:</label>\
                    </div>\
                    <div class="col-md-6">\
                        '+array.datos.con_cp+'\
                    </div>';
          $('.infoconsignatario').html(html);
          $('.infoconsignatario2').html(html2);
        }else{
          $('.infoconsignatario').html('');
          $('.infoconsignatario2').html('');
        }
        
      },
      error: function(response){
          toastr["error"]("Algo salio mal", "Error"); 
           
      }
  });
}
function asignar_factura(){
  var cotfactura=$('#cot_factura').val();
  asignar_factura2(cotfactura);
}
function asignar_factura2(codigo){
  $.ajax({
      type:'POST',
      url: base_url+"index.php/Documentar/ob_datoscli_factura",
      data: {
          codigo:codigo
      },
      success: function (response){
        var array = $.parseJSON(response);
        if(array.num==1){
          var html='<div class="col-md-2">\
                        <label class="col-form-label">Nombre ó Razón social </label>\
                    </div>\
                    <div class="col-md-4">\
                        '+array.datos.fac_nrs+'\
                    </div>\
                    <div class="col-md-2">\
                        <label class="col-form-label">R.F.C.</label>\
                    </div>\
                    <div class="col-md-4">\
                        '+array.datos.fac_rfc+'\
                    </div>\
                    <div class="col-md-2">\
                        <label class="col-form-label">Régimen Fiscal.</label>\
                    </div>\
                    <div class="col-md-4">\
                        '+obtenerregimen(array.datos.RegimenFiscalReceptor)+'\
                    </div>\
                    <div class="col-md-2">\
                        <label class="col-form-label">DIRECCIÓN:</label>\
                    </div>\
                    <div class="col-md-4">\
                        '+array.datos.fac_dir+'\
                    </div>\
                    <div class="col-md-2">\
                        <label class="col-form-label">COLONIA:</label>\
                    </div>\
                    <div class="col-md-4">\
                        '+array.datos.fac_col+'\
                    </div>\
                    <div class="col-md-2">\
                        <label class="col-form-label">CIUDAD:</label>\
                    </div>\
                    <div class="col-md-4">\
                        '+array.datos.fac_mc+' '+array.datos.fac_est+'\
                    </div>\
                    <div class="col-md-2">\
                        <label class="col-form-label">TEL.:</label>\
                    </div>\
                    <div class="col-md-4">\
                        '+array.datos.fac_la1+' '+array.datos.fac_te1+'\
                    </div>\
                    <div class="col-md-2">\
                        <label class="col-form-label">C.P.:</label>\
                    </div>\
                    <div class="col-md-4">\
                        '+array.datos.fac_cod+'\
                    </div>\
                    <div class="col-md-2">\
                        <label class="col-form-label">eMail.:</label>\
                    </div>\
                    <div class="col-md-9">\
                        '+array.datos.fac_email+'\
                    </div>\
                    <div class="col-md-1">\
                        <a type="button" class="btn-sm btn-info mr-1 mb-1" onclick="editarrfc('+array.datos.rid+')"><i class="fa fa-pencil fa-fw"></i></a>\
                    </div>';
              var html2='<div class="col-md-12">\
                        <h5>Factura</h5>\
                        <hr>\
                    </div>\
                    <div class="col-md-6">\
                        <label class="col-form-label">Nombre ó Razón social </label>\
                    </div>\
                    <div class="col-md-6">\
                        '+array.datos.fac_nrs+'\
                    </div>\
                    <div class="col-md-6">\
                        <label class="col-form-label">R.F.C.</label>\
                    </div>\
                    <div class="col-md-6">\
                        '+array.datos.fac_rfc+'\
                    </div>\
                    <div class="col-md-6">\
                        <label class="col-form-label">Regimen Fiscal.</label>\
                    </div>\
                    <div class="col-md-6">\
                        '+obtenerregimen(array.datos.RegimenFiscalReceptor)+'\
                    </div>\
                    <div class="col-md-6">\
                        <label class="col-form-label">DIRECCIÓN:</label>\
                    </div>\
                    <div class="col-md-6">\
                        '+array.datos.fac_dir+'\
                    </div>\
                    <div class="col-md-6" style="display:none">\
                        <label class="col-form-label">COLONIA:</label>\
                    </div>\
                    <div class="col-md-6" style="display:none">\
                        '+array.datos.fac_col+'\
                    </div>\
                    <div class="col-md-6" style="display:none">\
                        <label class="col-form-label">CIUDAD:</label>\
                    </div>\
                    <div class="col-md-6" style="display:none">\
                        '+array.datos.fac_mc+' '+array.datos.fac_est+'\
                    </div>\
                    <div class="col-md-6">\
                        <label class="col-form-label">TEL.:</label>\
                    </div>\
                    <div class="col-md-6">\
                        <!--'+array.datos.fac_la1+'--> '+array.datos.fac_te1+'\
                    </div>\
                    <div class="col-md-6">\
                        <label class="col-form-label">C.P.:</label>\
                    </div>\
                    <div class="col-md-6">\
                        '+array.datos.fac_cod+'\
                    </div>\
                    <div class="col-md-6">\
                        <label class="col-form-label">eMail.:</label>\
                    </div>\
                    <div class="col-md-6">\
                        '+array.datos.fac_email+'\
                    </div>';
          $('.infofacturas').html(html);
          $('.infofacturas2').html(html2);
        }else{
          $('.infofacturas').html('');
          $('.infofacturas2').html('');
        }
        
      },
      error: function(response){
          toastr["error"]("Algo salio mal", "Error"); 
           
      }
  });
}
function saveformconsignatario(){
  /*
  var form_consig = $('#form_consignatario');
  if(form_consig.valid()){
      var cot_remite = $('#cot_remite').val();
      var datos =form_consig.serialize()+'&cliente='+cot_remite; 
      $.ajax({
        type:'POST',
        url: base_url+"index.php/Documentar/saveformconsignatario",
        data: datos,
        success: function (response){
          $('#modal_consignatario').modal('hide');
          toastr["success"]("Informacion gurdada", "Hecho"); 
          obtenerinfocliente(cot_remite);
        },
        error: function(response){
            toastr["error"]("Algo salio mal", "Error"); 
             
        }
      });  
  }
  */
  var form_consig = $('#form_consignatario');
  if(form_consig.valid()){
      var cot_remite = $('#cot_remite').val();
      var datos =form_consig.serialize()+'&cliente='+cot_remite; 
      $.ajax({
        type:'POST',
        url: base_url+"index.php/Clientes/saveformconsignatario",
        data: datos,
        success: function (response){
          $('#modal_consignatario').modal('hide');
          toastr["success"]("Informacion gurdada", "Hecho"); 
          obtenerinfocliente(cot_remite);
          if($('.rid_config').val()>0){
              asignar_consig();
            }

        },
        error: function(response){
            toastr["error"]("Algo salio mal", "Error"); 
             
        }
      });  
  }
  
}
function saveformdatosfiscales(){
  /*
  var form_consig = $('#form_datosfiscales');
  if(form_consig.valid()){
      var cot_remite = $('#cot_remite').val();
      var datos =form_consig.serialize()+'&cliente='+cot_remite; 
      $.ajax({
        type:'POST',
        url: base_url+"index.php/Documentar/saveformdatosfiscales",
        data: datos,
        success: function (data){
          iddatosfiscales = parseInt(data);
          
          toastr["success"]("Informacion gurdada", "Hecho"); 
          $('#escaneofile').fileinput('upload');
          $('#situacion_fiscal_file').fileinput('upload');
          setTimeout(function(){
            $('#modal_datosfiscales').modal('hide');
          }, 2000);
          
          obtenerinfocliente(cot_remite);

        },
        error: function(response){
            toastr["error"]("Algo salio mal", "Error"); 
             
        }
      });  
  }
  */
  var form_consig = $('#form_datosfiscales');
  if(form_consig.valid()){
      var cot_remite = $('#cot_remite').val();
      var datos =form_consig.serialize()+'&cliente='+cot_remite; 
      $.ajax({
        type:'POST',
        url: base_url+"index.php/Clientes/saveformdatosfiscales",
        data: datos,
        success: function (data){
          iddatosfiscales = parseInt(data);
          
          toastr["success"]("Informacion gurdada", "Hecho"); 
          //if($('#escaneofile').val()!=''){
            $('#escaneofile').fileinput('upload');
          //}
          //if($('#situacion_fiscal_file').val()!=''){
            $('#situacion_fiscal_file').fileinput('upload'); 
          //}
          
          obtenerinfocliente(cot_remite);
          setTimeout(function(){
            $('#modal_datosfiscales').modal('hide');
            //location.reload();
            if($('.rid_df').val()>0){
              asignar_factura();
            }
          }, 1000);
          

        },
        error: function(response){
            toastr["error"]("Algo salio mal", "Error"); 
             
        }
      });  
  }
  
}
function obtenerregimen(clave){
  var obtenerregimen='';
  switch (clave) {
      case '601': 
          obtenerregimen='601 General de Ley Personas Morales';
          break;
      case '603': 
          obtenerregimen='603 Personas Morales con Fines no Lucrativos';
          break;
      case '605': 
          obtenerregimen='605 Sueldos y Salarios e Ingresos Asimilados a Salarios';
          break;
      case '606': 
          obtenerregimen='606 Arrendamiento';
          break;
      case '607': 
          obtenerregimen='607 Régimen de Enajenación o Adquisición de Bienes';
          break;
      case '608': 
          obtenerregimen='608 Demás ingresos';
          break;
      case '609': 
          obtenerregimen='609 Consolidación';
          break;
      case '610': 
          obtenerregimen='610 Residentes en el Extranjero sin Establecimiento Permanente en México';
          break;
      case '611': 
          obtenerregimen='611 Ingresos por Dividendos (socios y accionistas)';
          break;
      case '612': 
          obtenerregimen='612 Personas Físicas con Actividades Empresariales y Profesionales';
          break;
      case '614': 
          obtenerregimen='614 Ingresos por intereses';
          break;
      case '615': 
          obtenerregimen='615 Régimen de los ingresos por obtención de premios';
          break;
      case '616': 
          obtenerregimen='616 Sin obligaciones fiscales';
          break;
      case '620': 
          obtenerregimen='620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos';
          break;
      case '621': 
          obtenerregimen='621 Incorporación Fiscal';
          break;
      case '622': 
          obtenerregimen='622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras';
          break;
      case '623': 
          obtenerregimen='623 Opcional para Grupos de Sociedades';
          break;
      case '624': 
          obtenerregimen='624 Coordinados';
          break;
      case '625': 
          obtenerregimen='625 Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas';
          break;
      case '626': 
          obtenerregimen='626 Régimen Simplificado de Confianza';
          break;
      case '628': 
          obtenerregimen='628 Hidrocarburos';
          break;
      case '629': 
          obtenerregimen='629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales';
          break;
      case '630': 
          obtenerregimen='630 Enajenación de acciones en bolsa de valores';
          break;
      default:
          obtenerregimen='';
  }
  return obtenerregimen;
}
function formsaveupdate2(){
  //=============================================
              var form_2 = $('#form_2_cyf');
              var form_2_v=form_2.valid();
              var cotizacion=$('#cotizacion').val();
              
                var datos = 'cotizacion='+cotizacion+'&'+form_2.serialize();
                if(form_2_v==true){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Documentar/saveinsertupdate2",
                        data: datos,
                        success: function (response){
                            generarfolio_fyg();
                        },
                        error: function(response){
                            toastr["error"]("Algo salio mal", "Error");
                        }
                    });
                }else{
                    toastr["error"]("Verifica los campos requeridos", "Error");
                }
          //=============================================
          console.log('form_2_v:'+form_2_v);
  return form_2_v;
}
function generarfolio_fyg(){
  var cotizacion=$('#cotizacion').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Documentar/generarfoliofyg",
        data: {
          cotizacion:cotizacion
        },
        success: function (response){
            ontenerinformacion();
            
        },
        error: function(response){
            toastr["error"]("Algo salio mal", "Error");
        }
    });
}
function formsaveupdate3(){
  console.log('formsaveupdate3');
  //=============================================
              var form_3 = $('#form_fyg');
              var form_3_v=form_3.valid();
              var cotizacion=$('#cotizacion').val();
              var cot_guia = $('#cot_guia').val();
              if(bolavar>2){
                if(form_3_v==true){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Documentar/saveinsertupdate3",
                        data: {
                          cotizacion:cotizacion,
                          guia:$('#cot_guia').val()
                        },
                        success: function (response){
                        },
                        error: function(response){
                            toastr["error"]("Algo salio mal", "Error");
                        }
                    });
                }else{
                    toastr["error"]("Agregar un Numero de Guía", "Error");
                }
              }else{
                form_3_v=true;
              }
                
          //=============================================
  return form_3_v;
}
function cot_fdp(){
  $('.cot_fdp_efe').hide('show');
  $('.cot_fdp_cre').hide('show');
  $('.cot_fdp_tar').hide('show');

  var cot_fdp = $('#cot_fdp option:selected').val();
  if(cot_fdp=='efe'){
    $('.cot_fdp_efe').show('show');
    var importe=$('#cot_importe').val();
    $('#cot_cantidad').prop('min',importe);

  }else{
    $('#cot_cantidad').prop('min',0);
  }
  if(cot_fdp=='cre'){
    //$('.cot_fdp_cre').show('show'); 
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Estás seguro que este cliente tiene crédito autorizado?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Envio/notificarcredito",
                        data: {
                            cot:$('#cotizacion').val()
                        },
                        success: function (response){
                          $('.cot_fdp_cre').show('show');
                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () {
                $('#cot_fdp').val('');
            }
        }
    });
  }
  if(cot_fdp=='tar'){
    $('.cot_fdp_tar').show('show');
  }
  if(cot_fdp=='efetar'){
    $('.cot_fdp_efe').show('show');
    $('.cot_fdp_tar').show('show');
  }
}
function formsaveupdate4(){
  //=============================================
              var cot_fdp=$('#cot_fdp option:selected').val();
              if(cot_fdp=='efe'){
                var datosform=$('#form_fdp_'+cot_fdp).serialize();
                var formvalid=$('#form_fdp_'+cot_fdp).valid();
              }else if(cot_fdp=='cre'){
                var datosform=$('#form_fdp_'+cot_fdp).serialize();
                var formvalid=$('#form_fdp_'+cot_fdp).valid();
              }else if(cot_fdp=='tar'){
                var datosform=$('#form_fdp_'+cot_fdp).serialize();
                var formvalid=$('#form_fdp_'+cot_fdp).valid();
              }else if(cot_fdp=='efetar'){
                var datosform=$('#form_fdp_efe').serialize()+'&'+$('#form_fdp_tar').serialize();
                if($('#form_fdp_efe').valid()==true && $('#form_fdp_tar').valid()==true){
                  var importe=$('#cot_importe').val();
                  var efec=$('#cot_cantidad').val()==''?0:$('#cot_cantidad').val();
                  var deb=$('#banco_efectivo').val()==''?0:$('#banco_efectivo').val();
                  var cre=$('#banco_credito').val()==''?0:$('#banco_credito').val();
                  var total=parseFloat(efec)+parseFloat(deb)+parseFloat(cre);
                  if(total>=importe){
                    var formvalid=true;
                  }else{
                    var formvalid=false;
                    toastr["error"]("El monto total debe de ser mayor o igual al importe", "Error");
                  }
                  
                }else{
                  var formvalid=false;
                }
                
              }

              //var form_4 = $('#form_fdp_'+cot_fdp);
              //var form_4_v=form_4.valid();
              var cotizacion=$('#cotizacion').val();
              
                if(formvalid==true){
                    var datos='cot_fdp='+cot_fdp+'&cotizacion='+cotizacion+'&'+datosform;
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Documentar/saveinsertupdate4",
                        data: datos,
                        success: function (response){
                          //window.location.href = base_url+"Documentar/fdg/"+cotizacion;
                          if($('#facturar').is(':checked')){
                            if(cot_fdp=='efe'){
                              window.open(base_url+"Facturas/add?cot="+cotizacion+'&metodo='+cot_fdp, '_blank');
                              //window.location.href = base_url+"Facturas/add?cot="+cotizacion+'&metodo='+cot_fdp;
                              setTimeout(function(){ 
                                window.location.href = base_url+"Documentar/fdg/"+cotizacion;
                              }, 1000);
                            }
                            if(cot_fdp=='tar'){
                              window.open(base_url+"Facturas/add?cot="+cotizacion+'&metodo='+cot_fdp, '_blank');
                              //window.location.href = base_url+"Facturas/add?cot="+cotizacion+'&metodo='+cot_fdp;
                              setTimeout(function(){ 
                                window.location.href = base_url+"Documentar/fdg/"+cotizacion;
                              }, 1000);
                            }
                            if(cot_fdp=='efetar'){
                              window.open(base_url+"Facturas/add?cot="+cotizacion+'&metodo=efe', '_blank');
                              window.open(base_url+"Facturas/add?cot="+cotizacion+'&metodo=tar', '_blank');
                              setTimeout(function(){ 
                                window.location.href = base_url+"Documentar/fdg/"+cotizacion;
                              }, 1500);
                            }
                            //window.open(base_url+"Facturas/add?cot="+cotizacion, '_blank');
                            //window.location.href = base_url+"Facturas/add?cot="+cotizacion;  
                          }else{
                            window.location.href = base_url+"Documentar/fdg/"+cotizacion;
                          }
                          
                        },
                        error: function(response){
                            toastr["error"]("Algo salio mal", "Error");
                        }
                    });
                }else{
                    toastr["error"]("Complete los campos requeridos", "Error");
                }
  //=============================================
  return form_4_v;
}
function cam_cot_cantidad(){
  var cot_importe = $('#cot_importe').val();
  var cot_cantidad = $('#cot_cantidad').val();
  var cot_cambio = (parseFloat(cot_cantidad)-parseFloat(cot_importe)).toFixed(2);
  if(cot_cambio<0){
    cot_cambio=0;
  }
  $('#cot_cambio').val(cot_cambio);
}
function modalrfc(){
  $("#form_datosfiscales")[0].reset();
  $('.rid_config').val(0);
  $('#modal_datosfiscales').modal({backdrop: 'static', keyboard: false})
}
function printformdatosfiscales(){
  var form_consig = $('#form_datosfiscales');
  if(form_consig.valid()){
      var datos =form_consig.serialize(); 
      window.open(base_url+'Documentar/datosfiscales?'+datos, '_blank');
  }
}
function infofacturar(){
  setTimeout(function(){ 
    if($('#facturar').is(':checked')==true){
      if($('#cot_factura').val()==''){
        toastr["error"]("1. Precarga de datos en facturación", "Error");
      }
    }
  }, 1000);
}
function modal_consignatario(){
  $("#form_consignatario")[0].reset();
  $('.rid_df').val(0);
  $('#modal_consignatario').modal({backdrop: 'static', keyboard: false})
}
function editarconsig(rid){
  $('#myModalLabel1').html('Editar Consignatario');
  $('#modal_consignatario').modal();
  infoconsig(rid);
}
function editarrfc(rid){
  $('#myModalLabel2').html('Editar RFC');
  $('#modal_datosfiscales').modal();
  inforfc(rid);
}
function infoconsig(id){
  $.ajax({
        type:'POST',
        url: base_url+"index.php/Clientes/infoconsig",
        data: {
            rid:id
        },
        success: function (response){
          var array = $.parseJSON(response);
          //$('#cot_clasif').html(array.clasificacion);
          //$('#tiposervicio').html(array.tiposervicio);
            $('.rid_config').val(array.rid);
            $('#con_nombre').val(array.con_nombre);
      $('#con_paterno').val(array.con_paterno);
      $('#con_materno').val(array.con_materno);
      $('#con_calle').val(array.con_calle);
      $('#con_numext').val(array.con_numext);
      $('#con_numint').val(array.con_numint);
      $('#con_calles').val(array.con_calles);
      $('#con_colonia').val(array.con_colonia);
      $('#con_cp').val(array.con_cp);
      $('#con_municipio').val(array.con_municipio);
      $('#con_entidad').val(array.con_entidad);
      $('#con_telefono').val(array.con_telefono);
      $('#con_celular').val(array.con_celular);
      $('#con_email').val(array.con_email);
      $('#con_com').val(array.con_com);

        },
        error: function(response){
            toastr["error"]("Algo salio mal", "Error"); 
             
        }
    });
}
function inforfc(id){
  $.ajax({
        type:'POST',
        url: base_url+"index.php/Clientes/inforfc",
        data: {
            rid:id
        },
        success: function (response){
          var array = $.parseJSON(response);
          //$('#cot_clasif').html(array.clasificacion);
          //$('#tiposervicio').html(array.tiposervicio);
            $('.rid_df').val(array.rid);
            $('#fac_rfc').val(array.fac_rfc);
      $('#fac_nrs').val(array.fac_nrs);
      $('#fac_dir').val(array.fac_dir);
      $('#fac_cod').val(array.fac_cod);
      $('#fac_email').val(array.fac_email);
      $('#fac_te1').val(array.fac_te1);
      $('#RegimenFiscalReceptor').val(array.RegimenFiscalReceptor);
      $('#forma_pago').val(array.forma_pago);
      $('#metodo_pago').val(array.metodo_pago);
      $('#uso_cfdi').val(array.uso_cfdi);
      if(array.escaneofile=='' || array.escaneofile==null){
      }else{
        $('.divescalfile').html('<iframe src="'+base_url+'_files/_documentos/'+array.escaneofile+'" class="fileiframe" allowfullscreen="true" height="350px"></iframe>');

      }
      if(array.situacion_fiscal_file=='' || array.situacion_fiscal_file==null){
      }else{
        $('.divsituacionf').html('<iframe src="'+base_url+'_files/_documentos/'+array.situacion_fiscal_file+'" class="fileiframe" allowfullscreen="true" height="350px"></iframe>');

      }
      
      
        },
        error: function(response){
            toastr["error"]("Algo salio mal", "Error"); 
             
        }
    });
}
function addcliente(){
  window.open(base_url+"Clientes/add?modal=1", "Prefactura", "width=780, height=612");
}
function editcliente(id){
  window.open(base_url+"Clientes/add/"+id+"?modal=1", "Prefactura", "width=780, height=712");
}
function s_seletod(){
  var vo=$('#cot_rutori option:selected').data('vorigen');
  var vd=$('#cot_rutdes option:selected').data('vdestino');
  $('#v_origen option[class="'+vo+'"]').attr('selected', true);
  $('#v_destino option[class="'+vd+'"]').attr('selected', true);
}
function select_o_d(){
  var vu_o = $('#v_origen option:selected').val();
  var vu_d = $('#v_destino option:selected').val();
  var vu_f = $('#vfecha').val();
  $.ajax({
        type:'POST',
        url: base_url+"index.php/Documentar/consultarvuelos",
        data: {
            vo:vu_o,
            vd:vu_d,
            vf:vu_f
        },
        success: function (response){
          var array = $.parseJSON(response);
          var htmloption='';
          array.forEach(function(element) {
              htmloption+='<option value="'+element.vuelo+'">'+element.vuelo+'</option>';
          });
          $('#novuelo').html(htmloption);
      
        },
        error: function(response){
            toastr["error"]("Algo salio mal", "Error"); 
             
        }
    });
}
function validarcot_guia(){
  var cot_guia = $('#cot_guia').val();
  $.ajax({
        type:'POST',
        url: base_url+"index.php/General/validarcot_guia",
        data: {
            cot_guia:cot_guia
        },
        success: function (response){
          var array = $.parseJSON(response);
          if(array.total>0){
            console.log('total: '+array.total);
            var html='La guia <b>'+cot_guia+'</b> ya se encuentra ocupada';
            $.alert({boxWidth: '30%',useBootstrap: false,title: 'No permitido',theme: 'bootstrap',content: html}); 
            setTimeout(function(){
                $('#cot_guia').val('').change();
            }, 2000);
          }
      
        },
        error: function(response){
            toastr["error"]("Algo salio mal", "Error"); 
             
        }
    });
}