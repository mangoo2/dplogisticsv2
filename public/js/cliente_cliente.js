var base_url=$('#base_url').val();
var tablef;
var iddatosfiscales;
const rid_cli = $('#rid_cli').val();
$(document).ready(function () {
  tablef = $('#table_ultimas_guias').DataTable();
  tablesc = $('#table_sc').DataTable();
  loadtable();
	$("#escaneofile").fileinput({
            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["png","jpg","jpeg","bmp","pdf"],
            browseLabel: 'Seleccionar escaneo',
            uploadUrl: base_url+'Documentar/imagenrfc_escaneo',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            },
            uploadExtraData: function (previewId, index) {
              var info = {
                    id:iddatosfiscales

                  };
              return info;
          }
    });
  	$("#situacion_fiscal_file").fileinput({
            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["png","jpg","jpeg","bmp","pdf"],
            browseLabel: 'Seleccionar Constancia',
            uploadUrl: base_url+'Documentar/imagenrfc_situacionf',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            },
            uploadExtraData: function (previewId, index) {
              var info = {
                    id:iddatosfiscales

                  };
              return info;
          }
    });
    loadsubcli();
    $('#search_cli').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Documentar/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.rid,
                        text: element.cli_nombre+' '+element.cli_paterno+' '+element.cli_materno+' ('+element.rid+')',
                        credito: element.credito,

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
      var data = e.params.data;
        //obtenerinfocliente(data.id);
        //if(data.credito==0){
          //$('.optioncre').prop('disabled',true);
        //}
    });
});
function editarconsig(rid){
	$('#myModalLabel1').html('Editar Consignatario');
	$('#modal_consignatario').modal();
	infoconsig(rid);
}
function editarrfc(rid){
	$('#myModalLabel2').html('Editar RFC');
	$('#modal_datosfiscales').modal();
	inforfc(rid);
}
function addconsig(){
	$('#myModalLabel1').html('Agregar Consignatario');
	$('#modal_consignatario').modal();
	$('.rid_config').val(0);
}
function addrfc(){
	$('#myModalLabel2').html('Agregar RFC');
	$('#modal_datosfiscales').modal();
	$('.rid_df').val(0);
	$('.divescalfile').html('');
	$('.divsituacionf').html('');
}
function infoconsig(id){
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Clientes/infoconsig",
        data: {
            rid:id
        },
        success: function (response){
          var array = $.parseJSON(response);
          //$('#cot_clasif').html(array.clasificacion);
          //$('#tiposervicio').html(array.tiposervicio);
          	$('.rid_config').val(array.rid);
          	$('#con_nombre').val(array.con_nombre);
			$('#con_paterno').val(array.con_paterno);
			$('#con_materno').val(array.con_materno);
			$('#con_calle').val(array.con_calle);
			$('#con_numext').val(array.con_numext);
			$('#con_numint').val(array.con_numint);
			$('#con_calles').val(array.con_calles);
			$('#con_colonia').val(array.con_colonia);
			$('#con_cp').val(array.con_cp);
			$('#con_municipio').val(array.con_municipio);
			$('#con_entidad').val(array.con_entidad);
			$('#con_telefono').val(array.con_telefono);
			$('#con_celular').val(array.con_celular);
			$('#con_email').val(array.con_email);
			$('#con_com').val(array.con_com);

        },
        error: function(response){
            toastr.error("Algo salio mal", "Error"); 
             
        }
    });
}
function inforfc(id){
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Clientes/inforfc",
        data: {
            rid:id
        },
        success: function (response){
          var array = $.parseJSON(response);
          //$('#cot_clasif').html(array.clasificacion);
          //$('#tiposervicio').html(array.tiposervicio);
          	$('.rid_df').val(array.rid);
          	$('#fac_rfc').val(array.fac_rfc);
			$('#fac_nrs').val(array.fac_nrs);
			$('#fac_dir').val(array.fac_dir);
			$('#fac_cod').val(array.fac_cod);
			$('#fac_email').val(array.fac_email);
			$('#fac_te1').val(array.fac_te1);
			$('#RegimenFiscalReceptor').val(array.RegimenFiscalReceptor);
			$('#forma_pago').val(array.forma_pago);
			$('#metodo_pago').val(array.metodo_pago);
			$('#uso_cfdi').val(array.uso_cfdi);
			if(array.escaneofile!=''){
				$('.divescalfile').html('<iframe src="'+base_url+'_files/_documentos/'+array.escaneofile+'" class="fileiframe" allowfullscreen="true" height="350px"></iframe>');
			}
			if(array.situacion_fiscal_file!=''){
				$('.divsituacionf').html('<iframe src="'+base_url+'_files/_documentos/'+array.situacion_fiscal_file+'" class="fileiframe" allowfullscreen="true" height="350px"></iframe>');
			}
			
			
        },
        error: function(response){
            toastr.error("Algo salio mal", "Error"); 
             
        }
    });
}
function saveformconsignatario(){
  var form_consig = $('#form_consignatario');
  if(form_consig.valid()){
      var cot_remite = $('#cot_remite').val();
      var datos =form_consig.serialize()+'&cliente='+cot_remite; 
      $.ajax({
        type:'POST',
        url: base_url+"index.php/Clientes/saveformconsignatario",
        data: datos,
        success: function (response){
          $('#modal_consignatario').modal('hide');
          toastr.success("Informacion gurdada", "Hecho"); 
          setTimeout(function(){
            location.reload();
          }, 2000);

        },
        error: function(response){
            toastr.error("Algo salio mal", "Error"); 
             
        }
      });  
  }
  
}
function saveformdatosfiscales(){
  var form_consig = $('#form_datosfiscales');
  if(form_consig.valid()){
      var cot_remite = $('#cot_remite').val();
      var datos =form_consig.serialize()+'&cliente='+cot_remite; 
      $.ajax({
        type:'POST',
        url: base_url+"index.php/Clientes/saveformdatosfiscales",
        data: datos,
        success: function (data){
          iddatosfiscales = parseInt(data);
          
          toastr.success("Informacion gurdada", "Hecho"); 
          if($('#escaneofile').val()!=''){
            $('#escaneofile').fileinput('upload');
          }
          if($('#situacion_fiscal_file').val()!=''){
            $('#situacion_fiscal_file').fileinput('upload'); 
          }
          
          
          setTimeout(function(){
            $('#modal_datosfiscales').modal('hide');
            location.reload();
          }, 2000);
          

        },
        error: function(response){
            toastr.error("Algo salio mal", "Error"); 
             
        }
      });  
  }
  
}
function addclisub(){
    $('#modal_add_subcli').modal({backdrop: 'static', keyboard: false})  
    //$('#modal_add_subcli').modal();
}
function loadtable(){
    var fechaini=$('#fechaini').val();
    var fechafin=$('#fechafin').val();
    var numcliente=$('#cot_remite').val();
    
  tablef.destroy();
  tablef = $('#table_ultimas_guias').DataTable({
        
        //stateSave: true,
        //responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Clientes/getlistaultimasguias",
            type: "post",
            "data": {
                'fechaini':fechaini,
                'fechafin':fechafin,
                'numcliente':numcliente
            },
        },
        "columns": [
            {"data": "rid"},
            {"data": "emp"},
            {"data": "folio"},
            {"data": "guia"},
            {"data": "origen"},
            {"data": "destino"},
            {"data": "con"},
            {"data": "con_dir",
                render:function(data,type,row){
                    var html ='';
                        html = row.con_dir+' '+row.con_col+' '+row.con_ciu+' '+row.con_cp+' '+row.con_tel;
                    return html;
                }
            },
            {"data": "paqs"},
            {"data": "paq_des"},
            {"data": "peso"},
            {"data": "volumen"},
            {"data": "tarifa"},
            {"data": "importe"},
            {"data": "fecha"},
            {"data": "admin"},
            {"data": null,
                render:function(data,type,row){
                    var html ='<!--'+row.rid+'-->';
                        html+='<a href="'+base_url+'Cotizacion/file2/'+row.rid+'" class="btn btn-light mr-1 mb-1 btn-sm" Target="_blank" >Ver guía</a>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10,20,25, 50, 100,200,400], [10,20,25, 50,100,200,400]],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        //columnDefs: [ { orderable: false, targets: [8,9] }],
        columnDefs: [
        {
            target: 0,
            visible: false,
            searchable: false
        }
    ]
        
    });
  tablef.columns( [ 0] ).visible( false, false );
}
function loadsubcli(){
    var fechaini=$('#fechaini').val();
    var fechafin=$('#fechafin').val();
    var numcliente=$('#cot_remite').val();
    
  tablesc.destroy();
  tablesc = $('#table_sc').DataTable({
        
        //stateSave: true,
        //responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Clientes/getlista_clisb",
            type: "post",
            "data": {
                'rid_padre':rid_cli
            },
        },
        "columns": [
            {"data": "rid"},
            {"data": "con_dir",
                render:function(data,type,row){
                    var html ='';
                        html = row.cli_nombre+' '+row.cli_paterno+' '+row.cli_materno;
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html ='';
                        html+='<button type="button" class="btn btn-danger mr-1 mb-1" onclick="delete_g_sc('+row.id+')"><i class="fa fa-trash-o"></i></button>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10,20,25, 50, 100,200,400], [10,20,25, 50,100,200,400]],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        //columnDefs: [ { orderable: false, targets: [8,9] }],
        columnDefs: [
        {
            target: 0,
            visible: false,
            searchable: false
        }
    ]
        
    });
  tablef.columns( [ 0] ).visible( false, false );
}
function exportar(){
  var fechaini=$('#fechaini').val();
  var fechafin=$('#fechafin').val();
  var numcliente=$('#cot_remite').val();
  //window.open(base_url+'Clientes/exportarguias?numcliente='+numcliente+'&fechaini='+fechaini+'&fechafin='+fechafin, '_blank');
  window.open(base_url+'Reportes/exportarguias?numcliente='+numcliente+'&fechaini='+fechaini+'&fechafin='+fechafin, '_blank');
}
//==================================

var row_tr=0;
function addtrsubc(){
    var cli_selected =$('#search_cli option:selected').val();
    if(cli_selected!=rid_cli){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Clientes/verificsubcli",
            data: {
                cli:cli_selected
            },
            success: function (data){
                var array = $.parseJSON(data);
                var disable=' checked ';
                var msn='';
                    if(array.id_h_p>0){
                        disable=' disabled ';
                        msn='<span class="text_red">ya se encuentra asignado, no se podrá asignar nuevamente</span>';
                    }
                var html='<tr class="tr_sc_'+row_tr+'">';
                    html+='<td>';
                        
                            html+='<input type="checkbox" class="subcli_checked" id="factura_'+array.rid+'" value="'+array.rid+'" '+disable+'>';
                            html+=array.rid;
                    html+='</td>';
                    html+='<td>';
                        html+=array.cliname;
                    html+='</td>';
                    html+='<td>';
                            html+=array.suc;
                    html+='</td>';
                    html+='<td>';
                        html+=msn;
                    html+='</td>';
                    html+='<td>';
                        html+='<button type="button" class="btn btn-danger mr-1 mb-1" onclick="delete_sc('+row_tr+')"><i class="fa fa-trash-o"></i></button>';
                    html+='</td>';
                    html+='</tr>';
                    row_tr++;
                    var lis_table_add = $("#table_cli_selected_add tbody > tr");
                    lis_table_add.each(function(){ 
                        //var tr_idc 
                        if (array.rid==$(this).find("input[class*='subcli_checked']").val()) {
                            html='';
                        }       
                        
                    });
                    $('.tbody_sc_add').append(html);
            },
            error: function(response){
                toastr.error("Algo salio mal", "Error"); 
                 
            }
          });
    }else{
        toastr.error('No se puede seleccionar al mismo cliente');
    }
}
function delete_sc(row){
    $('.tr_sc_'+row).remove();
}
function saveformsubcli(){
    var cli_row=0;
    var lis_table_add = $("#table_cli_selected_add tbody > tr");
    var DATAa  = [];
    lis_table_add.each(function(){  
        if ($(this).find("input[class*='subcli_checked']").is(':checked')) {
            item = {};                    
            item ["subcli"]  = $(this).find("input[class*='subcli_checked']").val();
            item ["cli"]  = rid_cli;
            DATAa.push(item);
            cli_row++;
        }       
    });
    aInfo   = JSON.stringify(DATAa);

    if(cli_row>0){
        $.ajax({
            type:'POST',
            url: base_url+"Clientes/add_subclientes",
            data: {
                datos:aInfo
            },
            success: function (data){
                toastr.success('Clientes agregados');
                loadsubcli();
                $('.tbody_sc_add').html('');
                $('#modal_add_subcli').modal('hide');
            }
        });
    }else{
        toastr.error('Seleccione por lo menos un cliente valido');
    }
}
function delete_g_sc(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma borrar el subcliente',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Clientes/deletesubcli",
                    data: {
                        id:id
                    },
                    success: function (response){
                        toastr.success("Cliente eliminado", "Hecho"); 
                        loadsubcli();                          
                    },
                    error: function(response){
                        toastr.error("No se pudo procesar", "Error"); 
                         
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function addcliente(){
  window.open(base_url+"Clientes/add?modal=1", "Prefactura", "width=780, height=612");
}