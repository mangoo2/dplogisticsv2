var base_url = $('#base_url').val();
$(document).ready(function($) {
	$('#okform').click(function(event) {
		var formulario =$('#form_usuario');
		var validform=formulario.valid();
		if(validform){
			var datos = formulario.serialize();
			$.ajax({
                  type:'POST',
                  url: base_url+'index.php/Adm_usrs/inserupdate',
                  data: datos,
                  success:function(data){
                      // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                      rid=parseInt(data);
                      if(data>=1){
                          toastr["success"]("Información Guardada", "Hecho!")
                          $('#fotografia').fileinput('upload');
                          
                          setTimeout(function(){ 
                              //window.location.href = base_url+"index.php/clientes"; 
                              javascript:history.back();
                          }, 3000);
                      }
                      // En caso contrario, se notifica
                      else{
                          toastr["error"]("No se pudo guardar la información", "Error")

                      }
                  }
              });
		}
	});
	$("#fotografia").fileinput({
            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["png","jpg","jpeg","bmp"],
            browseLabel: 'Seleccionar Imagen',
            uploadUrl: base_url+'Adm_usrs/imagenusu',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            },
            uploadExtraData: function (previewId, index) {
	            var info = {
	            			id:$('#usuario').val()

	            		};
	            return info;
	        }
    })
});