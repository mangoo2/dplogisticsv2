var base_url = $('#base_url').val();
$(document).ready(function($) {
	verificardoc();
});
function verificardoc(){
	var num_docd = $('#num_docd').val();
	if(num_docd>0){
		
		console.log('Faltantes:'+$('.cot_faltantes').html());
		Swal.fire({
	      title: "Atención!",
	      text: "Para realizar corte de caja es necesario finalizar la documentación de paquetes ya Foliadas.",
	      type: "warning",
	      confirmButtonClass: 'btn btn-primary',
	      buttonsStyling: false,
	      allowOutsideClick: false,
	      confirmButtonText: 'Ir a Documentación',
	    }).then(function (result) {
	      if (result.value) {
	        window.location.href = base_url+"Documentar";
	      }
	    });
	}
}
function cerrarcorte(){
	$('.cerrarcorte').prop('disabled',true);
	setTimeout(function(){ 
             $(".cerrarcorte" ).prop( "disabled", false );
    }, 5000);
	$.ajax({
        type:'POST',
        url: base_url+'index.php/Corte_caja/cerrarcorte',
        data: {
            cor_com:$('#cor_com').val()
        },
        async: false,
        statusCode:{
            404: function(data){
                toastr["error"]("No se pudo procesar 404", "Error"); 
            },
            500: function(){
                toastr["error"]("No se pudo procesar 500", "Error");
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
          	window.location.href = base_url+"Corte_caja/cor_ver/"+array.corte;   
        }
    });
}