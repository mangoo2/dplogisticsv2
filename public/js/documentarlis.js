var base_url =$('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table_list').DataTable();
	loadtable();
});
function loadtable(){
	table.destroy();
	table=$("#table_list").DataTable({
    		stateSave: true,
    		//responsive: !0,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		       "url": base_url+"Documentar/getlista",
		       type: "post",
		       "data": function(d){
		        // d.bodegaId = $('#bodegaId option:selected').val()
		        },
		        error: function(){
		           $("#table").css("display","none");
		        }
		    },
		    "columns": [
		        {"data": "rid"},
		        {"data": "cot_feccap"},
		        {"data": "usuario"},
		        {"data": "aer_nom"},
		        {"data": "folioguia"},
				{"data": "clasifi"},
				{"data": "cot_descrip"},
				{"data": "rutao"},
				{"data": "rutad"},
				{"data": "cliente"},
		        {"data": "cot_estatus",
		        	"render": function ( data, type, row, meta ) {
		                var html="";
		                    if(row.cot_estatus=='doc'){
		                    	html="En documentación";
		                    }else if(row.cot_estatus=='cyf'){
								html="En consignatario y Factura";
		                    }else if(row.cot_estatus=='fyg'){
								html="En Folio y Guia";
		                    }else if(row.cot_estatus=='pdg'){
								html="En pago de guia";
		                    }else if(row.cot_estatus=='fdg'){
								html="Impresion de nota de guia";
		                    }else{
								html="Error de estatus";
		                    }
		            	return html;
		            }
		    	},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="<div style='width:170px;'>";
		                	if(row.cot_estatus=='doc'){
		                    	html+='<a  href="'+base_url+'Documentar/add/'+row.cotiza+'?status=doc" type="button" class="btn btn-info mr-1 mb-1"><i class="fa fa-link"></i></a>';
		                    }else if(row.cot_estatus=='cyf'){
								html+='<a  href="'+base_url+'Documentar/add/'+row.cotiza+'?status=cyf" type="button" class="btn btn-info mr-1 mb-1"><i class="fa fa-link"></i></a>';
		                    }else if(row.cot_estatus=='fyg'){
								html+='<a  href="'+base_url+'Documentar/add/'+row.cotiza+'?status=fyg" type="button" class="btn btn-info mr-1 mb-1"><i class="fa fa-link"></i></a>';
		                    }else if(row.cot_estatus=='pdg'){
								html+='<a  href="'+base_url+'Documentar/add/'+row.cotiza+'?status=pdg" type="button" class="btn btn-info mr-1 mb-1"><i class="fa fa-link"></i></a>';
		                    }else if(row.cot_estatus=='fdg'){
								html+='<a  href="'+base_url+'Documentar/add/'+row.cotiza+'?status=fdg" type="button" class="btn btn-info mr-1 mb-1"><i class="fa fa-link"></i></a>';
								if(row.sol_editar==2){
									var icosnsoledit='<span class="fa-stack fa-1x"><i class="fa-solid fa fa-edit fa-stack-1x"></i><i class="fa-solid fa fa-check  fa-stack-2x" style="color:green"></i></span>';
								}else{
									var icosnsoledit='<i class="fa fa-edit"></i>';
								}
								html+='<a  onclick="editsol('+row.rid+')" title="Solicitar Edicion" type="button" class="btn btn-info mr-1 mb-1" style="padding: 6px 15px;">'+icosnsoledit+'</a>';
		                    }

		                    var html_soldelete='<button type="button" class="btn btn-danger mr-1 mb-1 idcotiza_'+row.rid+'" data-idcotiza="'+row.cotiza+'" onclick="deletecot('+row.rid+')"><i class="fa fa-trash-o"></i></button>'
		                    var html_delete='<button type="button" class="btn btn-danger mr-1 mb-1 idcotiza_'+row.rid+'" data-idcotiza="'+row.cotiza+'" onclick="deletecotdiref('+row.rid+')"><i class="fa fa-trash-o"></i></button>'
		                    ;
		                    if(row.cot_estatus=='doc'){
		                    	html_soldelete='';
		                    }else if(row.cot_estatus=='cyf'){
								html_soldelete='';
		                    }else if(row.cot_estatus=='fyg'){
								html_delete='';
		                    }else if(row.cot_estatus=='pdg'){
								html_delete='';
		                    }else if(row.cot_estatus=='fdg'){
								html_delete='';
		                    }
		                   //if(row.cot_estatus != 'fyg' && row.cot_estatus != 'pdg' && row.cot_estatus != 'fdg'){ 
		                   		html+=html_soldelete+html_delete;
		                   //}
		                   html+="</div>";
		            	return html;
		            }
		        },
		    ],
    		"order": [[ 0, "desc" ]],
    		"lengthMenu": [[25, 50, 100], [25, 50, 100]],
  	}).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
   	});
}
/*
function searchtable_v(){
    var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}
*/
function deletecot(numcoti){
	var codigo=$('.idcotiza_'+numcoti).data('idcotiza');
	var motselect=$('#mot_can').html();
	var html='	<div>Confirma Solicitud para borrar el registro...</div>\
					<div class="col-md-12">\
	                    <label>Motivo Cancelación</label>\
	                    <select id="motselect" class="form-control form-control2">\
	                        '+motselect+'\
	                    </select>\
	                </div>\
	                <div class="col-md-12">\
	                    <label>Descripción</label>\
	                    <input type="text" class="form-control" id="descrip_can">\
	                </div>\
                ';
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
            	var motselect=$('#motselect option:selected').val();
            	var descrip_can=$('#descrip_can').val();
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Documentar/soldeletecot",
                        data: {
                            coti:numcoti,
                            motselect:motselect,
                            descrip:descrip_can,
                            codigo:codigo,
                            sol:1
                        },
                        success: function (response){
                                toastr["success"]("Solicitud de eliminación enviada", "Hecho!");
                                loadtable();
                        },
                        error: function(response){
                            toastr["error"]("No se pudo guardar la información", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function editsol(idcotiza){
	var codigo=$('.idcotiza_'+idcotiza).data('idcotiza');
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma Solicitud de edición',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Documentar/editsol",
                        data: {
                            coti:idcotiza,
                            codigo:codigo,
                            sol:1
                        },
                        success: function (response){
                                toastr["success"]("Se realizo la solicitud", "Hecho!");
                                loadtable();
                        },
                        error: function(response){
                            toastr["error"]("No se pudo realizar la solicitud", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function deletecotdiref(rid){
	var codigo=$('.idcotiza_'+rid).data('idcotiza');
	$.confirm({
        boxWidth: '41%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma borrar el registro del documento '+rid+' ('+codigo+')?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            Confirmar: function (){
                var idcotiza=$('.idcotiza_'+rid).data('idcotiza');
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Documentar/deletecot",
                        data: {
                            coti:rid,
                            codigo:'',
                        },
                        success: function (response){
                                toastr["success"]("Información Eliminada", "Hecho!");
                            loadtable();
                        },
                        error: function(response){
                            toastr["error"]("No se pudo guardar la información", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () {
                
            }
        }
    });
}