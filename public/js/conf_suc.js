var base_url=$('#base_url').val();
$(document).ready(function($) {
	
});
function editar(id){
        if(id>0){
        $('#exampleModalLiveLabel').html('Editar clasificación');
        $('.saveform').html('Actualizar');
        $('#modalform').modal({backdrop: 'static', keyboard: false});

        $('#rid').val(id);

        var suc_nombre = $('.edit_form_'+id).data('suc_nombre');
        $('#suc_nombre').val(suc_nombre);

        var suc_ubicacion = $('.edit_form_'+id).data('suc_ubicacion');
        $('#suc_ubicacion').val(suc_ubicacion);



        var suc_admin = $('.edit_form_'+id).data('suc_admin');
        $('#suc_admin').val(suc_admin);
		var suc_ruta = $('.edit_form_'+id).data('suc_ruta');
		$('#suc_ruta').val(suc_ruta);

        }else{
            $('#exampleModalLiveLabel').html('Agregar clasificación');
            $('.saveform').html('Agregar');
            $('#modalform').modal({backdrop: 'static', keyboard: false});
            $('#form_datos')[0].reset();
        }
}
function saveform() {
        var varform=$('#form_datos');
        if(varform.valid()){
            var datos = varform.serialize();
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Conf_suc/inserupdate",
                data: datos,
                success: function (response){
                  $('#modalform').modal('hide');
                  
                  toastr["success"]("Informacion Actualizada", "Hecho"); 
                  
                  setTimeout(function(){
                    location.reload();
                  }, 2000);

                },
                error: function(response){
                    toastr["error"]("Algo salio mal", "Error"); 
                     
                }
              });                
        }
}
function deleteaer(id){
        $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma borrar la Clasificación',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Conf_cla/delete",
                        data: {
                            rid:id
                        },
                        success: function (response){
                            toastr["success"]("Aerolinea eliminada", "Hecho"); 
                            location.reload();                         
                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
    }