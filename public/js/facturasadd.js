var base_url = $('#base_url').val();
var total=0;
var trunquearredondear=0;//0 redondeo 1 trunquear (dos dijitos)
$(document).ready(function(){
	//$('.chosen-select').chosen({width: "91%"});

	$('.sunidadsat').select2({
        width: '90%',
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar Unidad',
          ajax: {
            url: base_url+'Facturas/searchunidadsat',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.Clave,
                        text: element.Clave+' / '+element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
	$('.sconseptosat').select2({
        width: '90%',
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar Concepto',
          ajax: {
            url: base_url+'Facturas/searchconceptosat',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.Clave,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });

    $('#idcliente').select2({
        width: '90%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un cliente',
        ajax: {
            url: base_url+'Facturas/searchcli',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },

        processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                console.log(element);
                itemscli.push({
                    id: element.rid,
                    text: element.fac_nrs,
                    rfc: element.fac_rfc,
                    uso_cfdi:element.uso_cfdi,
                    forma_pago:element.forma_pago,
                    metodo_pago:element.metodo_pago,
                    fac_nrs:element.fac_nrs,
                    fac_cod:element.fac_cod,
                    RegimenFiscalReceptor:element.RegimenFiscalReceptor


                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    }).on('select2:select', function (e) {
    	var data = e.params.data;
        console.log(data);
        //obtenerrfc(data.id);
        $('#rfc').val(data.rfc).change();
        //console.log(array.datos.uso_cfdi);
        $('#uso_cfdi').val(data.uso_cfdi);
        //console.log(array.datos.metodo_pago);
        $('#FormaPago').val(data.metodo_pago);
        addinfodf(data.fac_nrs,data.fac_cod,data.RegimenFiscalReceptor);
        
    });

    $('.registrofac').click(function(event) {
        $('body').loading({theme: 'dark',message: 'Timbrando factura...'});
        $( ".registrofac" ).prop( "disabled", true );
        setTimeout(function(){ 
            $( ".registrofac" ).prop( "disabled", false );
        }, 5000);
        setTimeout(function(){ 
    		var form =$('#validateSubmitForm');
    		var valid =form.valid();
            var exento=0;
    		if (valid) {
                
    			var datos = form.serialize();
    			var productos = $("#table_conceptos tbody > tr");
                var band_vacio=0;
    			//==============================================
    				var DATAa  = [];
    		        productos.each(function(){   
    		            item = {};                    
                        item ["NoIdentificacion"] = $(this).find("input[id*='NoIdentificacion']").val();
                        item ["Cantidad"] = $(this).find("input[id*='cantidad']").val();
    		            item ["Unidad"]  = $(this).find("select[id*='unidadsat'] option:selected").val();
    		            item ["servicioId"]  = $(this).find("select[id*='conseptosat'] option:selected").val();
    		            item ["Descripcion"]  = $(this).find("select[id*='conseptosat'] option:selected").text();
    		            item ["Descripcion2"]  = $(this).find("textarea[id*='descripcion']").val();
    		            item ["Cu"]  = $(this).find("input[id*='precio']").val();
                        //item ["descuento"]   = $(this).find("input[id*='descuento']").val();
                        item ["descuento"]   = 0;
    		            item ["Importe"]  = $(this).find("input[id*='subtotal']").val();
    		            item ["iva"]  = $(this).find("input[id*='tiva']").val();
    		            DATAa.push(item);
                        if($(this).find("select[id*='unidadsat'] option:selected").val()=="" || $(this).find("select[id*='conseptosat'] option:selected").val()=="" ||
                            $(this).find("select[id*='unidadsat'] option:selected").val()==undefined || $(this).find("select[id*='conseptosat'] option:selected").val()==undefined)
                            band_vacio=1;
    		        });
    		        INFOa  = new FormData();
    		        aInfoa   = JSON.stringify(DATAa);
                    console.log("band_vacio: "+band_vacio);
                //==================================================================
                    var TranspInternac = $('#TranspInternac option:selected').val();
                    var TotalDistRec = $('#TotalDistRec').val();
                    var EntradaSalidaMerc = $('#EntradaSalidaMerc option:selected').val();
                    var ViaEntradaSalida = $('#ViaEntradaSalida').val();

                    var table_ubicaciones = $("#table_ubicaciones tbody > tr");
                    var DATAtu  = [];
                    table_ubicaciones.each(function(){ 
                        item = {}; 

                        //item["FacturasId"] = 
                        item["TipoUbicacion"]           = $(this).find("select[class*='TipoUbicacion'] option:selected").val();
                        item["TipoEstacion"]            = $(this).find("select[class*='TipoEstacion'] option:selected").val();
                        item["IDUbicacion"]             = $(this).find("input[class*='IDUbicacion']").val();
                        item["RFCRemitenteDestinatario"] = $(this).find("input[class*='RFCRemitenteDestinatario']").val();
                        item["NombreRFC"]               = $(this).find("select[class*='NombreRFC'] option:selected").val();
                        item["NumRegIdTrib"]            = $(this).find("input[class*='NumRegIdTrib']").val();
                        item["ResidenciaFiscal"]        = $(this).find("select[class*='ResidenciaFiscal'] option:selected").val();
                        item["NumEstacion"]             = $(this).find("select[class*='NumEstacion'] option:selected").val()==undefined?'':$(this).find("select[class*='NumEstacion'] option:selected").val();
                        item["NombreEstacion"]          = $(this).find("input[class*='NombreEstacion']").val();
                        item["FechaHoraSalidaLlegada"]  = $(this).find("input[class*='FechaHoraSalidaLlegada']").val();
                        item["DistanciaRecorrida"]      = $(this).find("input[class*='DistanciaRecorrida']").val();
                        item["Calle"]                   = $(this).find("input[class*='Calle']").val();
                        item["NumeroExterior"]          = $(this).find("input[class*='NumeroExterior']").val();
                        item["NumeroInterior"]          = $(this).find("input[class*='NumeroInterior']").val();
                        item["Colonia"]                 = $(this).find("select[class*='Colonia'] option:selected").val();
                        item["Localidad"]               = $(this).find("select[class*='Localidad'] option:selected").val();
                        item["Referencia"]              = $(this).find("input[class*='Referencia']").val();
                        item["Municipio"]               = $(this).find("select[class*='Municipio'] option:selected").val();
                        item["Estado"]                  = $(this).find("select[class*='Estado'] option:selected").val();
                        item["Pais"]                    = $(this).find("select[class*='Pais'] option:selected").val();
                        item["CodigoPostal"]            = $(this).find("input[class*='CodigoPostal']").val();
                        
                        DATAtu.push(item);
                    });
                    aInfotu   = JSON.stringify(DATAtu); 
                //==================================================================
                    var PesoBrutoTotal  = $('#PesoBrutoTotal').val();
                    var UnidadPeso      = $('#UnidadPeso').val();
                    var PesoNetoTotal   = $('#PesoNetoTotal').val();
                    var NumTotalMercancias = $('#NumTotalMercancias').val();
                    var CargoPorTasacion = $('#CargoPorTasacion').val();
                //=================================================================
                    var table_mercancias = $("#table_mercancias tbody > tr");
                    var DATAtm  = [];
                    table_mercancias.each(function(){ 
                        item = {}; 
                        //FacturasId
                        item["BienesTransp"]    = $(this).find("input[id*='BienesTransp']").val();
                        item["Descripcion"]     = $(this).find("input[id*='Descripcion']").val();
                        item["Cantidad"]        = $(this).find("input[id*='Cantidad']").val();
                        item["ClaveUnidad"]     = $(this).find("input[id*='ClaveUnidad']").val();
                        item["Dimensiones"]     = $(this).find("input[id*='Dimensiones']").val();
                        item["Embalaje"]        = $(this).find("input[id*='Embalaje']").val();
                        item["DescripEmbalaje"] = $(this).find("input[id*='DescripEmbalaje']").val();
                        item["PesoEnKg"]        = $(this).find("input[id*='PesoEnKg']").val();
                        item["ValorMercancia"]  = $(this).find("input[id*='ValorMercancia']").val();
                        item["Moneda"]          = $(this).find("input[id*='Moneda']").val();
                        item["Unidad"]          = $(this).find("input[id*='Unidad']").val();
                        DATAtm.push(item);
                    });
                    aInfotm   = JSON.stringify(DATAtm); 
                //=================================================================
                    var table_transporte = $("#table_transporte tbody > tr");
                    var DATAtf  = [];
                    table_transporte.each(function(){ 
                        item = {}; 
                        //FacturasId
                        item["PermSCT"] = $(this).find("input[id*='PermSCT']").val();
                        item["NumPermisoSCT"] = $(this).find("input[id*='NumPermisoSCT']").val();
                        item["NombreAseg"] = $(this).find("input[id*='NombreAseg']").val();
                        item["NumPolizaSeguro"] = $(this).find("input[id*='NumPolizaSeguro']").val();
                        item["ConfigVehicular"] = $(this).find("input[id*='ConfigVehicular']").val();
                        item["PlacaVM"] = $(this).find("input[id*='PlacaVM']").val();
                        item["AnioModeloVM"] = $(this).find("input[id*='AnioModeloVM']").val();
                        item["SubTipoRem"] = $(this).find("input[id*='SubTipoRem']").val();
                        item["Placa"] = $(this).find("input[id*='Placa']").val();                
                        DATAtf.push(item);
                    });
                    aInfottf   = JSON.stringify(DATAtf); 
                //==================================================================
                    var table_aereo = $("#table_aereo tbody > tr");
                    var DATAtae  = [];
                    table_aereo.each(function(){ 
                        item = {}; 
                        item["PermSCT"]         = $(this).find("input[id*='PermSCT']").val();
                        item["NumPermisoSCT"]   = $(this).find("input[id*='NumPermisoSCT']").val();
                        item["MatriculaAeronave"]= $(this).find("input[id*='MatriculaAeronave']").val();
                        item["NombreAseg"]      = $(this).find("input[id*='NombreAseg']").val();
                        item["NumPolizaSeguro"] = $(this).find("input[id*='NumPolizaSeguro']").val();
                        item["NumeroGuia"]      = $(this).find("input[id*='NumeroGuia']").val();
                        item["LugarContrato"]   = $(this).find("input[id*='LugarContrato']").val();
                        item["RFCTransportista"]= $(this).find("input[id*='RFCTransportista']").val();
                        item["CodigoTransportista"]= $(this).find("input[id*='CodigoTransportista']").val();
                        item["NumRegIdTribTranspora"]= $(this).find("input[id*='NumRegIdTribTranspora']").val();
                        item["ResidenciaFiscalTranspor"]= $(this).find("input[id*='ResidenciaFiscalTranspor']").val();
                        item["NombreTransportista"]= $(this).find("input[id*='NombreTransportista']").val();
                        item["RFCEmbarcador"]   = $(this).find("input[id*='RFCEmbarcador']").val();
                        item["NumRegIdTribEmbarc"]= $(this).find("input[id*='NumRegIdTribEmbarc']").val();
                        item["ResidenciaFiscalEmbarc"]= $(this).find("input[id*='ResidenciaFiscalEmbarc']").val();
                        item["NombreEmbarcador"] = $(this).find("input[id*='NombreEmbarcador']").val();

                                       
                        DATAtae.push(item);
                    });
                    aInfottae   = JSON.stringify(DATAtae); 
                //===============================operador===================================
                    var table_operador = $("#table_operador tbody > tr");
                    var DATAope  = [];
                    table_operador.each(function(){ 
                        item = {}; 

                        item["RFCOperador"] = $(this).find("input[id*='RFCOperador']").val();
                        item["NumLicencia"] = $(this).find("input[id*='NumLicencia']").val();
                        item["NombreOperador"] = $(this).find("input[id*='NombreOperador']").val();
                        item["NumRegIdTribOperador"] = $(this).find("input[id*='NumRegIdTribOperador']").val();
                        item["ResidenciaFiscalOperador"] = $(this).find("input[id*='ResidenciaFiscalOperador']").val();
                        item["adddomicilio"] = $(this).find("input[id*='adddomicilio']").val();
                        item["Calle"] = $(this).find("input[id*='Calle']").val();
                        item["NumeroExterior"] = $(this).find("input[id*='NumeroExterior']").val();
                        item["NumeroInterior"] = $(this).find("input[id*='NumeroInterior']").val();
                        item["Colonia"] = $(this).find("input[id*='Colonia']").val();
                        item["Localidad"] = $(this).find("input[id*='Localidad']").val();
                        item["Referencia"] = $(this).find("input[id*='Referencia']").val();
                        item["Municipio"] = $(this).find("input[id*='Municipio']").val();
                        item["Estado"] = $(this).find("input[id*='Estado']").val();
                        item["Pais"] = $(this).find("input[id*='Pais']").val();
                        item["CodigoPostal"] = $(this).find("input[id*='CodigoPostal']").val();            
                        DATAope.push(item);
                    });
                    aInfotope   = JSON.stringify(DATAope); 
                //===============================propietario===================================
                    var table_propietario = $("#table_propietario tbody > tr");
                    var DATAprop  = [];
                    table_propietario.each(function(){ 
                        item = {}; 

                        item["RFCPropietario"] = $(this).find("input[id*='RFCPropietario']").val();
                        var np=$(this).find("input[id*='NombrePropietario']").val();
                        item["NombrePropietario"] = np.replace('&', '');
                        item["NumRegIdTribPropietario"] = $(this).find("input[id*='NumRegIdTribPropietario']").val();
                        item["ResidenciaFiscalPropietario"] = $(this).find("input[id*='ResidenciaFiscalPropietario']").val();
                        item["adddomiciliop"] = $(this).find("input[id*='adddomiciliop']").val();
                        item["Calle"] = $(this).find("input[id*='Calle']").val();
                        item["NumeroExterior"] = $(this).find("input[id*='NumeroExterior']").val();
                        item["NumeroInterior"] = $(this).find("input[id*='NumeroInterior']").val();
                        item["Colonia"] = $(this).find("input[id*='Colonia']").val();
                        item["Localidad"] = $(this).find("input[id*='Localidad']").val();
                        item["Referencia"] = $(this).find("input[id*='Referencia']").val();
                        item["Municipio"] = $(this).find("input[id*='Municipio']").val();
                        item["Estado"] = $(this).find("input[id*='Estado']").val();
                        item["Pais"] = $(this).find("input[id*='Pais']").val();
                        item["CodigoPostal"] = $(this).find("input[id*='CodigoPostal']").val();            
                        DATAprop.push(item);
                    });
                    aInfotprop   = JSON.stringify(DATAprop); 
                //===============================arrendatario===================================
                //===============================arrendatario===================================
                    var table_arrendatario = $("#table_arrendatario tbody > tr");
                    var DATAarren  = [];
                    table_arrendatario.each(function(){ 
                        item = {}; 

                        item["RFCArrendatario"] = $(this).find("input[id*='RFCArrendatario']").val();
                        item["NombreArrendatario"] = $(this).find("input[id*='NombreArrendatario']").val();
                        item["NumRegIdTribArrendatario"] = $(this).find("input[id*='NumRegIdTribArrendatario']").val();
                        item["ResidenciaFiscalArrendatario"] = $(this).find("input[id*='ResidenciaFiscalArrendatario']").val();
                        item["adddomicilioa"] = $(this).find("input[id*='adddomicilioa']").val();
                        item["Calle"] = $(this).find("input[id*='Calle']").val();
                        item["NumeroExterior"] = $(this).find("input[id*='NumeroExterior']").val();
                        item["NumeroInterior"] = $(this).find("input[id*='NumeroInterior']").val();
                        item["Colonia"] = $(this).find("input[id*='Colonia']").val();
                        item["Localidad"] = $(this).find("input[id*='Localidad']").val();
                        item["Referencia"] = $(this).find("input[id*='Referencia']").val();
                        item["Municipio"] = $(this).find("input[id*='Municipio']").val();
                        item["Estado"] = $(this).find("input[id*='Estado']").val();
                        item["Pais"] = $(this).find("input[id*='Pais']").val();
                        item["CodigoPostal"] = $(this).find("input[id*='CodigoPostal']").val();            
                        DATAarren.push(item);
                    });
                    aInfotarrend  = JSON.stringify(DATAarren); 
                //==================================================================
                    var CveTransporte=$('#CveTransporte option:selected').val();
                    var cartaporte=$('#cartaporte').is(':checked')==true?1:0;

    			if (productos.length>0 && band_vacio==0) {
                    

    				//var tiporelacion = $('input:radio[name=tiporelacion]:checked').val();
    				//var ventaviculada = $('#ventaviculada').val();
                    //var ventaviculadatipo = $('#ventaviculadatipo').val();
    				var Subtotal=$('#Subtotal').val();
    				var iva=$('#iva').val();
    				var total=$('#total').val();  

                    var f_r = $('#facturarelacionada').is(':checked')==true?1:0; //agregados version 4.0
                    var f_r_t = $('#TipoRelacion option:selected').val();
                    var f_r_uuid = $('#uuid_r').val();

                    /*var visr= $('#isr').val();
                    var vriva= $('#ivaretenido').val();
                    var v5millar= $('#5almillarval').val();
                    var outsourcing= $('#outsourcing').val();*/

    				
    				//datos=datos+'&subtotal='+Subtotal+'&iva='+iva+'&visr='+visr+'&vriva='+vriva+'&v5millar='+v5millar+'&outsourcing='+outsourcing+'&total='+total+'&conceptos='+aInfoa+'&id_venta='+$("#id_venta").val();
                    datos=datos+'&subtotal='+Subtotal+'&iva='+iva+'&total='+total+'&conceptos='+aInfoa+'&f_r='+f_r+'&f_r_t='+f_r_t+'&f_r_uuid='+f_r_uuid;
                    datos+='&TranspInternac='+TranspInternac+'&TotalDistRec='+TotalDistRec+'&EntradaSalidaMerc='+EntradaSalidaMerc+'&ViaEntradaSalida='+ViaEntradaSalida+'&ubicaciones='+aInfotu;
                datos+='&PesoBrutoTotal='+PesoBrutoTotal+'&UnidadPeso='+UnidadPeso+'&PesoNetoTotal='+PesoNetoTotal+'&NumTotalMercancias='+NumTotalMercancias+'&CargoPorTasacion='+CargoPorTasacion;
                datos+='&mercancias='+aInfotm+'&transportes='+aInfottf+'&aereo='+aInfottae+'&operadores='+aInfotope+'&propietario='+aInfotprop+'&arrendatario='+aInfotarrend+'&CveTransporte='+CveTransporte+'&cartaporte='+cartaporte;
                    $.ajax({
    		            type:'POST',
    		            url: base_url+"Timbrar/generafacturarabierta",
    		            data: datos,
                        async: false,
                        statusCode:{
                            404: function(data){
                                swal.fire("Error", "404", "error");
                            },
                            500: function(){
                                swal.fire("Error", "500", "error"); 
                            }
                        },
    		            success: function (response){
                            console.log(response);
                            
                            var array = $.parseJSON(response);
                            if (array.resultado=='error') {
                                /*
                                swal({    title: "Error "+array.CodigoRespuesta+"!",
                                              text: array.MensajeError,
                                              type: "warning",
                                              showCancelButton: false
                                 });
                                */
                                retimbrar(array.facturaId,$("#id_venta").val());
                                //$('body').loading('stop');
                            }else{
                                $('body').loading('stop');
                                $.confirm({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    icon: 'fa fa-check-circle',
                                    title: 'Éxito',
                                    content: 'Se ha creado el CFDI. ¿Mostrar CFDI emitido?',
                                    type: 'green',
                                    typeAnimated: true,
                                    buttons: 
                                    {
                                        SI: function (){
                                            
                                            var tipoempresa = $('#tipoempresa option:selected').val();
                                            window.location.href = base_url+"Facturas?emp="+tipoempresa+"&envio="+array.facturaId; 
                                            
                                        },
                                        NO: function () 
                                        {
                                            var tipoempresa = $('#tipoempresa option:selected').val();
                                            window.location.href = base_url+"Facturas?emp="+tipoempresa; 
                                        }
                                    }
                                });
                                
                            }
    		                
                            
    		            },
    		            error: function(response){
    		                swal.fire('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema','warning'); 
                            $('body').loading('stop');
    		            }
    		        });
    			}else{
    				swal.fire('Atención!','Agregar por lo menos un concepto con unidad/concepto SAT','warning');
                    $('body').loading('stop');
    			}
    		}else{
    			swal.fire('Atención!','Faltan campos obligatorios','warning');
                $('body').loading('stop');
    		}
        }, 1000);
	});
    calculartotales_set(1000);

    $('#facturarelacionada').click(function(event) { //agregado version 4.0
        if($('#facturarelacionada').is(':checked')){
            $('.divfacturarelacionada').show('show');
            $('#TipoDeComprobante').val('E');
        }else{
            $('.divfacturarelacionada').hide('show');
            $('#uuid_r').val('');
            $('#TipoDeComprobante').val('I');
        }
        /* Act on the event */
    });
    $('#FormaPago').change(function(event) {
        if($('#FormaPago').val()=='PPD'){
            $('#MetodoPago').val(99);
        }
    });
});
function TipoRelacionselected(){
    var tipo=$('#TipoRelacion').val();
    if(tipo=='01'){
        $('#TipoDeComprobante').val('E');
    }else{
        $('#TipoDeComprobante').val('I');
    }
}
function calculartotal(){
    //console.log("calculartotal");
	var cantidad=$('#scantidad').val()==''?0:$('#scantidad').val();
	var precio=$('#sprecio').val()==''?0:$('#sprecio').val();
	total=parseFloat(cantidad)*parseFloat(precio);
	total=total.toFixed(2);
	//$('.montototal').html(new Intl.NumberFormat('es-MX').format(total))
}
function agregarconcepto(){
    var NoIdentificacion = $('#sNoIdentificacion').val();
    var cantidad = $('#scantidad').val();
    var unidad = $('#sunidadsat').val();
    var unidadt = $('#sunidadsat').text();
    var concepto = $('#sconseptosat').val();
    var conceptot = $('#sconseptosat').text();
    var descripcion = $('#sdescripcion').val();
    var precio = $('#sprecio').val();
    var aiva = $('#aplicariva').is(':checked')==true?1:0;
    var rfc = $('#rfc').val();
    if(rfc=='XAXX010101000'){
        if($('#pg_global').is(':checked')){
            if(unidad!='ACT'){
                $.alert({
                        boxWidth: '30%',useBootstrap: false,
                        title: 'Atención!',
                        content: 'la <b>Unidad SAT</b> para PUBLICO EN GENERAL debera de ser <b>ACT</b> "Actividad" '});   
                precio=0;
            }
            if(concepto!='01010101'){
                $.alert({
                        boxWidth: '30%',useBootstrap: false,
                        title: 'Atención!',
                        content: 'el <b>Concepto SAT</b> para PUBLICO EN GENERAL debera de ser <b>01010101</b> "No existe en el catálogo" '});  
                precio=0;
            }
            if(descripcion!='Venta'){
                $.alert({
                        boxWidth: '30%',useBootstrap: false,
                        title: 'Atención!',
                        content: 'La <b>Descripción</b> para PUBLICO EN GENERAL debera de ser <b>Venta</b>'});  
                precio=0;
            }
        }
    }
	if(precio>0){
        

		agregarconceptovalor(NoIdentificacion,cantidad,unidad,unidadt,concepto,conceptot,descripcion,precio,aiva);
		$('#sdescripcion').val('');
		$('#scantidad').val(1);
		$('#sprecio').val(0);
	}
}
var rowcobrar=0;
function agregarconceptovalor(NoIdentificacion,cantidad,unidad,unidadt,concepto,conceptot,descripcion,precio,aiva){
	var subtotal=parseFloat(cantidad)*parseFloat(precio);
    //console.log("aiva: "+aiva);
    var chk_ai="";
	if (aiva==1) {
		/*var siva =subtotal*0.16;
		siva = siva.toFixed(4);*/
    
        var siva =subtotal*0.16;
        if(trunquearredondear==0){
            siva = siva.toFixed(4);
        }else{
            siva=Math.floor(siva * 100) / 100;
        }
        chk_ai="checked";
	}else{
		var siva =0.00;
	}
	//console.log("siva: "+siva);
	var subtotal=parseFloat(subtotal)+parseFloat(siva);
		subtotal=subtotal.toFixed(2);
        console.log("subtotal: "+subtotal);
	var datoscobrar ='	<tr class="rowtrconceptos rowcobrar_'+rowcobrar+'">\
                          <td>\
                            <input type="text" name="NoIdentificacion" id="NoIdentificacion" class="form-control" value="'+NoIdentificacion+'" style="background: transparent !important; border: 0px !important;" readonly>\
                          </th>\
                          <td>\
                            <input type="number" name="cantidad" id="cantidad" class="form-control" value="'+cantidad+'" style="background: transparent !important; border: 0px !important;" readonly>\
                          </th>\
                          <td >\
                            <select name="sunidadsat" id="unidadsat" class="form-control browser-default form-control unidadsat" style="display:none;">\
                            	<option value="'+unidad+'">'+unidadt+'</option>\
                            </select>\
                            '+unidadt+'\
                          </td>\
                          <td >\
                            <select name="conseptosat" id="conseptosat" class="form-control browser-default conseptosat" style="display:none;">\
                            	<option value="'+concepto+'">'+conceptot+'</option>\
                            </select>\
                            '+conceptot+'\
                          </td>\
                          <td >\
                          <textarea name="descripcion" id="descripcion" class="form-control">'+descripcion+'</textarea>\
                          </td>\
                          <td>\
                            <input type="number" name="sprecio" id="precio" class="form-control" value="'+precio+'" style="display:none;" readonly>\
                            '+precio+'\
                          </td>\
                          <td>\
                            <input type="number"\
                            name="descuento"\
                            id="descuento"\
                            data-diva="'+aiva+'"\
                            class="form-control cdescuento cdescuento_'+rowcobrar+'"\
                            value="0" readonly>\
                          </td>\
                          <td>\
                          	<input type="number" name="subtotal" id="subtotal" class="form-control csubtotal" value="'+subtotal+'" style="background: transparent !important; border: 0px !important;" readonly>\
                          </td>\
                          <td style="padding:0px">\
                            <input type="number" id="tiva" value="'+siva+'" class="form-control siva" style="background: transparent !important; border: 0px !important;padding:0px" readonly>\
                          </td>\
                          <td>\
                            <input type="checkbox" class="filled-in" id="aplicariva" onclick="calculartotales()" '+chk_ai+'>\
                            <label for="aplicariva"></label>\
                          </td>\
                          <td >\
                            <a class="btn btn-danger" onclick="deleteconcepto('+rowcobrar+')"><i class="fa fa-trash"></i></a>\
                          </td>\
                        </tr>';

	$('.addcobrar').append(datoscobrar);
	rowcobrar++;
	calculartotales();
}
function deleteconcepto(row){
	$('.rowcobrar_'+row).remove();
	calculartotales();
}
function calculartotales_set(tiempo){
    setTimeout(function(){ 
        calculartotales();
    }, tiempo);
}

/*function exentoIva(idv){
    $(".siva_"+idv+"").val('0');
    $(".chk_pi_"+idv+"").attr("checked",false);
}*/

function calculartotales(){
	var totales = 0;
    /*
    $(".preciorow").each(function() {
        var vtotales = parseFloat($(this).val());
        totales += Number(vtotales);
    });
    */
    var TABLApr   = $("#table_conceptos tbody > tr");
    TABLApr.each(function(){ 
        var cant = $(this).find("input[name*='scantidad']").val();
        if(cant==undefined){
                cant = $(this).find("input[id*='cantidad']").val();
            }
        if($(this).find("#aplicariva").is(":checked")==true){
            var calc_iva = parseFloat($(this).find("input[id*='precio']").val());
            calc_iva = calc_iva.toFixed(2);
            var precalc_iva = calc_iva * 0.16;
            precalc_iva = precalc_iva.toFixed(4);
            $(this).find("input[id*='tiva']").attr("readonly",false);
            $(this).find("input[id*='tiva']").val(precalc_iva);
            $(this).find("input[id*='tiva']").attr("readonly",true);
            var precioi= parseFloat($(this).find("input[id*='precio']").val());
            //var sub = precioi - calc_iva;
            var sub = calc_iva;
            $(this).find("input[name*='sprecio']").val(sub);
            $(this).find("input[name*='subtotal']").val(sub*cant);
        }else{
            $(this).find("input[id*='tiva']").val("0");
            var sub_siniva=parseFloat($(this).find("input[id*='precio']").val())*cant;
            //$(this).find("input[name*='sprecio']").val($(this).find("input[id*='precio']").val());
            $(this).find("input[name*='subtotal']").val(sub_siniva);
        }

        var vtotales = parseFloat($(this).find("input[id*='cantidad']").val())*parseFloat($(this).find("input[id*='precio']").val());
            vtotales = vtotales.toFixed(2);
            totales += Number(vtotales);

    });

    //=================================================
    var ivas = 0;
    $(".siva").each(function() {
        var vivas = parseFloat($(this).val());
        ivas += Number(vivas);
    });
    var dest = 0;
    $(".cdescuento").each(function() {
        var destc = parseFloat($(this).val());
        dest += Number(destc);
    });


    var subtotalc = parseFloat(totales);
        subtotalc = subtotalc.toFixed(2);
        //ivas=ivas.toFixed(2);

        totales=totales.toFixed(2);
        ivas = parseFloat(ivas);
        ivas =ivas.toFixed(2);

    var Subtotalinfo=parseFloat(subtotalc);
    var subtotalc=parseFloat(subtotalc);

    /*if ($('#risr').is(':checked')) {
        var v_isr=Subtotalinfo*0.1;
            v_isr =v_isr.toFixed(2);
            $('#isr').val(v_isr);
            totales=totales-v_isr;
            totales=totales.toFixed(2);
    }else{
        var v_isr=0;
        $('#isr').val(0.00);
    }
    if ($('#riva').is(':checked')) {
        var v_riva=Subtotalinfo*0.106666;
            v_riva =v_riva.toFixed(2);
            $('#ivaretenido').val(v_riva);
            totales=totales-v_riva;
            totales=totales.toFixed(2);
    }else{
        $('#ivaretenido').val(0.00);
    }
    if ($('#5almillar').is(':checked')) {
        var v_5millar=(Subtotalinfo/1000)*5;
            v_5millar =v_5millar.toFixed(2);
            $('#5almillarval').val(v_5millar);
            totales=totales-v_5millar;
            totales=totales.toFixed(2);
    }else{
        $('#5almillarval').val(0.00);
    }
    if ($('#aplicaout').is(':checked')) {
        var aplicaout = Subtotalinfo*0.06;
            aplicaout = aplicaout.toFixed(2);
        
            $('#outsourcing').val(aplicaout);
            totales=totales-aplicaout;
            totales=totales.toFixed(2);
    }else{
        $('#outsourcing').val(0.00);
    }*/

    totales=totales-parseFloat(dest)+parseFloat(ivas);
    totales=totales.toFixed(2);
    $('#Subtotalinfo').val(Subtotalinfo);

    $('#Subtotal').val(subtotalc);
    $('#descuentof').val(dest);
    $('#iva').val(ivas);
    $('#total').val(totales);

}
function obtenerrfc(cliente){
    //console.log("cliente: "+cliente);
	$.ajax({
        type:'POST',
        url: base_url+"Facturas/obtenerrfc",
        data: {
        	id:cliente
        },
        success: function (data){
            //console.log("data: "+data);
        	$('#rfc').val(data).change(); 
            //$('#rfc').trigger("chosen:updated");
        }
    });
}
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
function detalle(id,tipo){
    if (tipo==0) {
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }else{
        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
}
function detallep(id){
    window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
}

function calculardescuento(idrow){
    var costo = $('.precio_'+idrow).val();
    var descuento = $('.cdescuento_'+idrow).val();
    var rowtotal =parseFloat(costo)-parseFloat(descuento);
    var ivaif = $('.cdescuento_'+idrow).data('diva');
    if (ivaif==1) {
        var siva =rowtotal*0.16;
        if(trunquearredondear==0){
            siva = siva.toFixed(4);
        }else{
            siva=Math.floor(siva * 100) / 100;
        }
    }else{
        var siva =0.00;
    }
    $('.tiva_'+idrow).val(siva);
    var totalg = parseFloat(rowtotal)+parseFloat(siva);
    $('.csubtotal_'+idrow).val(totalg);
    calculartotales();
}

function retimbrar(idfactura,id_venta){
    $.ajax({
        type:'POST',
        url: base_url+"Timbrar/retimbrar",
        data: {
            factura:idfactura, id_venta: id_venta
        },
        success:function(response){  
            console.log(response);
            $('body').loading('stop');
            //$.unblockUI();
            var array = $.parseJSON(response);
            if (array.resultado=='error') {
                
                Swal.fire({
                                icon: "error",
                                title: "Error "+array.CodigoRespuesta+"!",
                                text: array.MensajeError,
                                footer: 'Detalle: '+array.info.TimbrarCFDIResult.MensajeErrorDetallado,
                            });
            }else{
                Swal.fire("Éxito!", "Se ha creado la factura", "success");
            }
            setTimeout(function(){ 
                var tipoempresa = $('#tipoempresa option:selected').val();
                window.location.href = base_url+"Facturas?emp="+tipoempresa; 
            }, 5500);

        }
    });       
}
/*
function verPrefactura(){
    var form =$('#validateSubmitForm');
    var valid =form.valid();
    //if (valid) {
        //$( ".registrofac" ).prop( "disabled", true );
        var datos = form.serialize();
        var productos = $("#table_conceptos tbody > tr");
         //==============================================
        var DATAa  = [];
        productos.each(function(){         
            item = {};                    
            item ["Cantidad"]   = $(this).find("input[id*='cantidad']").val();
            item ["Unidad"]  = $(this).find("select[id*='unidadsat']").val();
            console.log($(this).find("select[id*='unidadsat']").val());
            if ($(this).find("select[id*='unidadsat']").val()==null || $(this).find("select[id*='unidadsat']").val()=='') {
                blockearfact=0;
            }
            item ["servicioId"]  = $(this).find("select[id*='conseptosat']").val();
            console.log($(this).find("select[id*='conseptosat']").val());
            if ($(this).find("select[id*='conseptosat']").val()==null || $(this).find("select[id*='conseptosat']").val()=='') {
                blockearfact=0;
            }
            item ["Descripcion"]  = $(this).find("select[id*='conseptosat']").text();
            item ["Descripcion2"]  = $(this).find("input[id*='descripcion']").val();
            item ["Cu"]  = $(this).find("input[id*='precio']").val();
            item ["descuento"]  = $(this).find("input[id*='descuento']").val();
            item ["Importe"]  = $(this).find("input[id*='subtotal']").val();
            item ["iva"]  = $(this).find("input[id*='tiva']").val();
            DATAa.push(item);
        });
        INFOa  = new FormData();
        aInfoa   = JSON.stringify(DATAa);
        var Subtotal=$('#Subtotal').val();
        var iva=$('#iva').val();

        var total=$('#total').val();               
        datos=datos+'&id_venta'+$("#id_venta").val()+'&subtotal='+Subtotal+'&iva='+iva+'&total='+total+'&conceptos='+aInfoa;
        if($("#uso_cfdi option:selected").val()!=""){
            window.open(base_url+"Ventas/prefactura?"+datos, "Prefactura", "width=780, height=612");
        }else{
            swal("Error!", "Seleccione uso de CFDI", "warning");
        }
    //}
}
*/
function verficartiporfc(){
    var rfc = $('#rfc').val();
    if(rfc=='XAXX010101000'){
        var html='<div>\
                            <input type="checkbox" class="filled-in" id="pg_global" name="pg_global" wtx-context="4D1A9C7C-0B37-41A5-88EB-ECB7877AFCAD" onclick="pgglobal()">\
                            <label for="pg_global">GLOBAL</label>\
                          </div><div class="infoglobal" style="display:none"><div class="row"><div class="col-md-4">\
                    <label>Periodicidad</label>\
                    <select class="form-control" name="pg_periodicidad" id="pg_periodicidad" onchange="v_periocidad()">\
                      <option value="01">01 Diario</option>\
                      <option value="02">02 Semanal</option>\
                      <option value="03">03 Quincenal</option>\
                      <option value="04">04 Mensual</option>\
                      <option value="05">05 Bimestral</option>\
                    </select>\
                  </div>\
                  <div class="col-md-4">\
                    <label>Mes</label>\
                    <select class="form-control" name="pg_meses" id="pg_meses">\
                      <option value="01" class="select_no_bimestral">01 Enero</option>\
                      <option value="02" class="select_no_bimestral">02 Febrero</option>\
                      <option value="03" class="select_no_bimestral">03 Marzo</option>\
                      <option value="04" class="select_no_bimestral">04 Abril</option>\
                      <option value="05" class="select_no_bimestral">05 Mayo</option>\
                      <option value="06" class="select_no_bimestral">06 Junio</option>\
                      <option value="07" class="select_no_bimestral">07 Julio</option>\
                      <option value="08" class="select_no_bimestral">08 Agosto</option>\
                      <option value="09" class="select_no_bimestral">09 Septiembre</option>\
                      <option value="10" class="select_no_bimestral">10 Octubre</option>\
                      <option value="11" class="select_no_bimestral">11 Noviembre</option>\
                      <option value="12" class="select_no_bimestral">12 Diciembre</option>\
                      <option value="13" class="select_bimestral">13 Enero-Febrero</option>\
                      <option value="14" class="select_bimestral">14 Marzo-Abril</option>\
                      <option value="15" class="select_bimestral">15 Mayo-Junio</option>\
                      <option value="16" class="select_bimestral">16 Julio-Agosto</option>\
                      <option value="17" class="select_bimestral">17 Septiembre-Octubre</option>\
                      <option value="18" class="select_bimestral">18 Noviembre-Diciembre</option>\
                    </select>\
                  </div>\
                  <div class="col-md-4">\
                    <label>Año</label>\
                    <input type="number" name="pg_anio" id="pg_anio" class="form-control" readonly>\
                  </div></div></div>';
        $('.agregardatospublicogeneral').html(html);
        setTimeout(function(){ 
            var mesactual = $('#mesactual').val();
            var anioactual = $('#anioactual').val();
            console.log(mesactual);
            console.log(anioactual);
            $('#pg_meses').val(mesactual);
            $('#pg_anio').val(anioactual);
            $('.select_bimestral').hide('show');
        }, 1000);
        $('#sdescripcion').val('Venta');
    }else{
        $('.agregardatospublicogeneral').html('');
    }
}
function v_periocidad(){
    var mesactual = $('#mesactual').val();
    var pg_periodicidad = $('#pg_periodicidad').val();
    if(pg_periodicidad=='05'){
        $('.select_no_bimestral').hide('show');
        $('.select_bimestral').show('show');
        if(mesactual=='01' || mesactual=='02'){
            var mesactual_n=13;
        }
        if(mesactual=='03' || mesactual=='04'){
            var mesactual_n=14;
        }
        if(mesactual=='05' || mesactual=='06'){
            var mesactual_n=15;
        }
        if(mesactual=='07' || mesactual=='08'){
            var mesactual_n=16;
        }
        if(mesactual=='09' || mesactual=='10'){
            var mesactual_n=17;
        }
        if(mesactual=='11' || mesactual=='12'){
            var mesactual_n=18;
        }
        $('#pg_meses').val(mesactual_n);
    }else{
        $('#pg_meses').val(mesactual);
        $('.select_no_bimestral').show('show');
        $('.select_bimestral').hide('show');
    }
    
}
function ontenerinformacion(cotizacion,metodo){
  $.ajax({
      type:'POST',
      url: base_url+"index.php/Documentar/obtenerinfor",
      data: {
        cotiz:cotizacion
      },
      success: function (data){
        var array = $.parseJSON(data);
            console.log(array);
            $('#rid_relacion').val(array.doc_cotiza[0].rid);     
            asignar_factura2(array.doc_cotiza[0].cot_factura);
            if(array.doc_cotiza[0].aero=='10102016224826'){
                $('#tipoempresa').val(1).change();//COECSA
            }else{
                $('#tipoempresa').val(2).change();//CADIPA
            }
            if(array.doc_cotiza[0].cot_fdp=='efe' || metodo=='efe'){
                $('#MetodoPago').val('01');
                var precio=parseFloat(array.doc_cotiza[0].cot_importe);
            }
            if(array.doc_cotiza[0].cot_fdp=='tar' || metodo=='tar'){
                $('#MetodoPago').val('04');
                //if(array.doc_cotiza[0].banco_efectivo>0){
                if(array.doc_cotiza[0].banco_efectivo!=''){
                    var banco_efectivo=array.doc_cotiza[0].banco_efectivo;
                    $('#MetodoPago').val('28');
                }else{
                    var banco_efectivo=0;
                }
                //if(array.doc_cotiza[0].banco_credito>0){
                if(array.doc_cotiza[0].banco_credito!=''){
                    var banco_credito=array.doc_cotiza[0].banco_credito;
                    $('#MetodoPago').val('04');
                }else{
                    var banco_credito=0;
                }
                var precio=parseFloat(banco_efectivo)+parseFloat(banco_credito);
                if(precio>0){

                }else{
                    var precio=parseFloat(array.doc_tars[0].tar_tot);
                }
                //var precio=array.doc_cotiza[0].banco_credito;
            }
            var precio=(parseFloat(precio)/1.16).toFixed(2);
            //var descripcion=array.tiposervicio[0].name+'__'+array.doc_cotiza[0].cot_guia+'__ al destino '+array.descr;
            var descripcion=array.tiposervicio[0].name+' '+array.doc_cotiza[0].cot_guia;
            $('#rid_relacion_cot_folio').val(array.cot_folio);
            agregarconceptovalor(array.doc_cotiza[0].cot_guia,1,array.tiposervicio[0].unidad,array.tiposervicio[0].unidadtext,array.tiposervicio[0].servicio,array.tiposervicio[0].serviciotext,descripcion,precio,1);
      },
      error: function(response){
          toastr["error"]("Algo salio mal", "Error");
      }
  });
}
function pgglobal(){
    setTimeout(function(){ 
        var pgg=$('#pg_global').is(':checked');
        if(pgg==true){
            $('.infoglobal').show();
            $('#pg_global').val(1);
        }else{
            $('.infoglobal').hide();
            $('#pg_global').val(0);
        }
    }, 1000);
}
function asignar_factura2(codigo){
  $.ajax({
      type:'POST',
      url: base_url+"index.php/Documentar/ob_datoscli_factura",
      data: {
          codigo:codigo
      },
      success: function (response){
        var array = $.parseJSON(response);
        console.log(array);
        $('#idcliente').html('<option value="'+array.datos.rid+'">'+array.datos.fac_nrs+'</option>');
        $('#rfc').val(array.datos.fac_rfc).change();
        console.log(array.datos.uso_cfdi);
        $('#uso_cfdi').val(array.datos.uso_cfdi);
        console.log(array.datos.metodo_pago);
        $('#FormaPago').val(array.datos.metodo_pago);
        addinfodf(array.datos.fac_nrs,array.datos.fac_cod,array.datos.RegimenFiscalReceptor);

        
        
      },
      error: function(response){
          toastr["error"]("Algo salio mal", "Error"); 
           
      }
  });
}
function cartaporteview(){
    setTimeout(function () {
        if($('#cartaporte').is(':checked')){
            $('.view_cartaporte').show('show');
        }else{
            $('.view_cartaporte').hide('show');
        }
    }, 1000);
}


function btn_origen(id){
    $('#modal_origen').modal();
    select_origen(id);
}

function select_origen(id){
    $('#origen_ubicacion').select2({
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Selecciona una opción:',
        ajax: {
            url: base_url+'Documentar/search_origen',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: id,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.rut_cla+'->'+element.rut_nom+' / '+element.idorigen+' / '+element.descripcion,
                        idorigen:element.idorigen,
                        nom_estacion:element.descripcion,
                        num_estacion:element.num_estacion,
                        iata:element.iata,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        $('#modal_origen').modal('hide');
        $('#IDUbicacion_0').val(data.idorigen);
        //////
        var datax = {
            id: data.num_estacion,
            text: data.num_estacion+' '+data.iata,
        };
        //$('#NumEstacion_0').html('').select2({data: [{id: '', text: ''}]});
        var id_rf=data.num_estacion;
        var newOption = new Option(datax.text, datax.id, false, false);
        $('#NumEstacion_0').append(newOption).trigger('change');
        setTimeout(function(){   
        $('#NumEstacion_0').val(id_rf).trigger('change.select2');
        }, 400);
        //////    
        $('#NombreEstacion_0').val(data.nom_estacion);
    });
}


function btn_origen_domicilio(id){
    $('#modal_origen_domicilio').modal();
    select_origen_domicilio(id);
}

function select_origen_domicilio(id){
    $('#origen_ubicacion_domicilio').select2({
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Selecciona una opción:',
        ajax: {
            url: base_url+'Documentar/search_origen_domicilio',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: id,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.rut_cla+'->'+element.rut_nom,
                        idorigen:element.idorigen,
                        calle:element.calle,
                        num_ext:element.num_ext,
                        num_int:element.num_int,
                        colonia:element.colonia,
                        cp_c:element.cp_c,
                        coloniatxt:element.coloniatxt,
                        localidad:element.localidad,
                        localidadtxt:element.localidadtxt,
                        referencia:element.referencia,
                        municipio:element.municipio,
                        municipiotxt:element.municipiotxt,
                        estado:element.estado,
                        estadotxt:element.estadotxt,
                        pais:element.pais,
                        paistxt:element.paistxt,
                        codigo_postal:element.codigo_postal,
                        c_Colonia:element.c_Colonia,
                        c_Localidad:element.c_Localidad,
                        c_Municipio:element.c_Municipio,
                        c_Estado:element.c_Estado,
                        c_Pais:element.c_Pais

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        $('#modal_origen_domicilio').modal('hide');
        $('#Calle_0').val(data.calle);
        $('#NumeroExterior_0').val(data.num_ext);
        $('#NumeroInterior_0').val(data.num_int);
        $('#CodigoPostal_0').val(data.codigo_postal);
        $('#Referencia_0').val(data.referencia);
        ///////////////////////////
        $('#Colonia_0').html('<option value="'+data.c_Colonia+'">('+data.cp_c+') '+data.coloniatxt+'</option>');
        ///////////////////////////
        $('#Localidad_0').html('<option value="'+data.c_Localidad+'">'+data.localidadtxt+'</option>');
        ///////////////////////////
        $('#Municipio_0').html('<option value="'+data.c_Municipio+'">'+data.municipiotxt+'</option>');
        ///////////////////////////
        $('#Estado_0').html('<option value="'+data.c_Estado+'">'+data.estadotxt+'</option>');
        ///////////////////////////
        $('#Pais_0').html('<option value="'+data.c_Pais+'">'+data.paistxt+'</option>');
    });
}
//////////////////////////////////////////////////////////////////////////////////
function btn_destino(id){
    $('#modal_destino').modal();
    select_destino(id);
}
function select_destino(id){
    $('#destino_ubicacion').select2({
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Selecciona una opción:',
        ajax: {
            url: base_url+'Documentar/search_destino',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: id,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.rut_cla+'->'+element.rut_nom+' / '+element.idorigen+' / '+element.descripcion,
                        idorigen:element.idorigen,
                        nom_estacion:element.descripcion,
                        num_estacion:element.num_estacion,
                        iata:element.iata,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        $('#modal_destino').modal('hide');
        $('#IDUbicacion_1').val(data.idorigen);
        //////
        var datax = {
            id: data.num_estacion,
            text: data.num_estacion+' '+data.iata,
        };
        //$('#NumEstacion_1').html('').select2({data: [{id: '', text: ''}]});
        var id_rf=data.num_estacion;
        var newOption = new Option(datax.text, datax.id, false, false);
        $('#NumEstacion_1').append(newOption).trigger('change');
        setTimeout(function(){   
        $('#NumEstacion_1').val(id_rf).trigger('change.select2');
        }, 400);
        //////    
        $('#NombreEstacion_1').val(data.nom_estacion);
    });
}
function btn_destino_domicilio(id){
    $('#modal_destino_domicilio').modal();
    select_destino_domicilio(id);
}
function select_destino_domicilio(id){
    $('#destino_ubicacion_domicilio').select2({
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Selecciona una opción:',
        ajax: {
            url: base_url+'Documentar/search_destino_domicilio',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: id,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.rut_cla+'->'+element.rut_nom,
                        idorigen:element.idorigen,
                        calle:element.calle,
                        num_ext:element.num_ext,
                        num_int:element.num_int,
                        colonia:element.colonia,
                        cp_c:element.cp_c,
                        coloniatxt:element.coloniatxt,
                        localidad:element.localidad,
                        localidadtxt:element.localidadtxt,
                        referencia:element.referencia,
                        municipio:element.municipio,
                        municipiotxt:element.municipiotxt,
                        estado:element.estado,
                        estadotxt:element.estadotxt,
                        pais:element.pais,
                        paistxt:element.paistxt,
                        codigo_postal:element.codigo_postal,
                        c_Colonia:element.c_Colonia,
                        c_Localidad:element.c_Localidad,
                        c_Municipio:element.c_Municipio,
                        c_Estado:element.c_Estado,
                        c_Pais:element.c_Pais

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        $('#modal_destino_domicilio').modal('hide');
        $('#Calle_1').val(data.calle);
        $('#NumeroExterior_1').val(data.num_ext);
        $('#NumeroInterior_1').val(data.num_int);
        $('#CodigoPostal_1').val(data.codigo_postal);
        $('#Referencia_1').val(data.referencia);
        ///////////////////////////
        $('#Colonia_1').html('<option value="'+data.c_Colonia+'">('+data.cp_c+') '+data.coloniatxt+'</option>');
        ///////////////////////////
        $('#Localidad_1').html('<option value="'+data.c_Localidad+'">'+data.localidadtxt+'</option>');
        ///////////////////////////
        $('#Municipio_1').html('<option value="'+data.c_Municipio+'">'+data.municipiotxt+'</option>');
        ///////////////////////////
        $('#Estado_1').html('<option value="'+data.c_Estado+'">'+data.estadotxt+'</option>');
        ///////////////////////////
        $('#Pais_1').html('<option value="'+data.c_Pais+'">'+data.paistxt+'</option>');
    });
}
function tipoempresas(){
    var emp=$('#tipoempresa option:selected').val();
    if(emp==1){
        $('.sucursales_view').show('show');
        $('#lugar_aplica').val('OAXACA');
    }else{
        $('.sucursales_view').hide('show');
        $('#lugar_aplica').val('PUEBLA');
        $('#sucursal').val(0);
    }
}
function obtenerdatosfactura(factura){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Facturas/obtenerdatosfactura",
        data: {
            factura:factura
        },
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            $('#tipoempresa').val(array.facturas.tipoempresa);
            $('#idcliente').html('<option value="'+array.facturas.Clientes_ClientesId+'">'+array.facturas.Nombre+'<option>');
            $('#rfc').val(array.facturas.Rfc);
            
            
            
            $('#idcliente').select2();
            //obtenerrfc(array.facturas[0].Clientes_ClientesId,array.facturas[0].Rfc);
            //console.log(array.facturas[0].Rfc);
            //$('#rfc').html('<option>'+array.facturas[0].Rfc+'<option>');
            //$('#rfc').select2();
            $('#FormaPago').val(array.facturas.FormaPago);

            $('#moneda').val(array.facturas.moneda);
            
            $('#uso_cfdi').val(array.facturas.uso_cfdi);

            $('#CondicionesDePago').val(array.facturas.CondicionesDePago);
            $('#MetodoPago').val(array.facturas.MetodoPago)
            //formapagoselect();
            
            $('#numproveedor').val(array.facturas.numproveedor);
            $('#numordencompra').val(array.facturas.numordencompra);
            $('#observaciones').val(array.facturas.observaciones);

            
        }
    });
}




//=preview===================================
function registrofac_prew(){
        $( ".registrofac" ).prop( "disabled", true );
        setTimeout(function(){ 
            $( ".registrofac" ).prop( "disabled", false );
        }, 5000);
        setTimeout(function(){ 
            var form =$('#validateSubmitForm');
            var valid =form.valid();
            var exento=0;
            if (valid) {
                
                var datos = form.serialize();
                var productos = $("#table_conceptos tbody > tr");
                var band_vacio=0;
                //==============================================
                    var DATAa  = [];
                    productos.each(function(){   
                        item = {};                    
                        item ["NoIdentificacion"] = $(this).find("input[id*='NoIdentificacion']").val();
                        item ["Cantidad"] = $(this).find("input[id*='cantidad']").val();
                        item ["Unidad"]  = $(this).find("select[id*='unidadsat'] option:selected").val();
                        item ["servicioId"]  = $(this).find("select[id*='conseptosat'] option:selected").val();
                        item ["Descripcion"]  = $(this).find("select[id*='conseptosat'] option:selected").text();
                        item ["Descripcion2"]  = $(this).find("textarea[id*='descripcion']").val();
                        item ["Cu"]  = $(this).find("input[id*='precio']").val();
                        //item ["descuento"]   = $(this).find("input[id*='descuento']").val();
                        item ["descuento"]   = 0;
                        item ["Importe"]  = $(this).find("input[id*='subtotal']").val();
                        item ["iva"]  = $(this).find("input[id*='tiva']").val();
                        DATAa.push(item);
                        if($(this).find("select[id*='unidadsat'] option:selected").val()=="" || $(this).find("select[id*='conseptosat'] option:selected").val()=="" ||
                            $(this).find("select[id*='unidadsat'] option:selected").val()==undefined || $(this).find("select[id*='conseptosat'] option:selected").val()==undefined)
                            band_vacio=1;
                    });
                    INFOa  = new FormData();
                    aInfoa   = JSON.stringify(DATAa);
                    console.log("band_vacio: "+band_vacio);
                //==================================================================
                    var TranspInternac = $('#TranspInternac option:selected').val();
                    var TotalDistRec = $('#TotalDistRec').val();
                    var EntradaSalidaMerc = $('#EntradaSalidaMerc option:selected').val();
                    var ViaEntradaSalida = $('#ViaEntradaSalida').val();

                    var table_ubicaciones = $("#table_ubicaciones tbody > tr");
                    var DATAtu  = [];
                    table_ubicaciones.each(function(){ 
                        item = {}; 

                        //item["FacturasId"] = 
                        item["TipoUbicacion"]           = $(this).find("select[class*='TipoUbicacion'] option:selected").val();
                        item["TipoEstacion"]            = $(this).find("select[class*='TipoEstacion'] option:selected").val();
                        item["IDUbicacion"]             = $(this).find("input[class*='IDUbicacion']").val();
                        item["RFCRemitenteDestinatario"] = $(this).find("input[class*='RFCRemitenteDestinatario']").val();
                        item["NombreRFC"]               = $(this).find("select[class*='NombreRFC'] option:selected").val();
                        item["NumRegIdTrib"]            = $(this).find("input[class*='NumRegIdTrib']").val();
                        item["ResidenciaFiscal"]        = $(this).find("select[class*='ResidenciaFiscal'] option:selected").val();
                        item["NumEstacion"]             = $(this).find("select[class*='NumEstacion'] option:selected").val()==undefined?'':$(this).find("select[class*='NumEstacion'] option:selected").val();
                        item["NombreEstacion"]          = $(this).find("input[class*='NombreEstacion']").val();
                        item["FechaHoraSalidaLlegada"]  = $(this).find("input[class*='FechaHoraSalidaLlegada']").val();
                        item["DistanciaRecorrida"]      = $(this).find("input[class*='DistanciaRecorrida']").val();
                        item["Calle"]                   = $(this).find("input[class*='Calle']").val();
                        item["NumeroExterior"]          = $(this).find("input[class*='NumeroExterior']").val();
                        item["NumeroInterior"]          = $(this).find("input[class*='NumeroInterior']").val();
                        item["Colonia"]                 = $(this).find("select[class*='Colonia'] option:selected").val();
                        item["Localidad"]               = $(this).find("select[class*='Localidad'] option:selected").val();
                        item["Referencia"]              = $(this).find("input[class*='Referencia']").val();
                        item["Municipio"]               = $(this).find("select[class*='Municipio'] option:selected").val();
                        item["Estado"]                  = $(this).find("select[class*='Estado'] option:selected").val();
                        item["Pais"]                    = $(this).find("select[class*='Pais'] option:selected").val();
                        item["CodigoPostal"]            = $(this).find("input[class*='CodigoPostal']").val();
                        
                        DATAtu.push(item);
                    });
                    aInfotu   = JSON.stringify(DATAtu); 
                //==================================================================
                    var PesoBrutoTotal  = $('#PesoBrutoTotal').val();
                    var UnidadPeso      = $('#UnidadPeso').val();
                    var PesoNetoTotal   = $('#PesoNetoTotal').val();
                    var NumTotalMercancias = $('#NumTotalMercancias').val();
                    var CargoPorTasacion = $('#CargoPorTasacion').val();
                //=================================================================
                    var table_mercancias = $("#table_mercancias tbody > tr");
                    var DATAtm  = [];
                    table_mercancias.each(function(){ 
                        item = {}; 
                        //FacturasId
                        item["BienesTransp"]    = $(this).find("input[id*='BienesTransp']").val();
                        item["Descripcion"]     = $(this).find("input[id*='Descripcion']").val();
                        item["Cantidad"]        = $(this).find("input[id*='Cantidad']").val();
                        item["ClaveUnidad"]     = $(this).find("input[id*='ClaveUnidad']").val();
                        item["Dimensiones"]     = $(this).find("input[id*='Dimensiones']").val();
                        item["Embalaje"]        = $(this).find("input[id*='Embalaje']").val();
                        item["DescripEmbalaje"] = $(this).find("input[id*='DescripEmbalaje']").val();
                        item["PesoEnKg"]        = $(this).find("input[id*='PesoEnKg']").val();
                        item["ValorMercancia"]  = $(this).find("input[id*='ValorMercancia']").val();
                        item["Moneda"]          = $(this).find("input[id*='Moneda']").val();
                        item["Unidad"]          = $(this).find("input[id*='Unidad']").val();
                        DATAtm.push(item);
                    });
                    aInfotm   = JSON.stringify(DATAtm); 
                //=================================================================
                    var table_transporte = $("#table_transporte tbody > tr");
                    var DATAtf  = [];
                    table_transporte.each(function(){ 
                        item = {}; 
                        //FacturasId
                        item["PermSCT"] = $(this).find("input[id*='PermSCT']").val();
                        item["NumPermisoSCT"] = $(this).find("input[id*='NumPermisoSCT']").val();
                        item["NombreAseg"] = $(this).find("input[id*='NombreAseg']").val();
                        item["NumPolizaSeguro"] = $(this).find("input[id*='NumPolizaSeguro']").val();
                        item["ConfigVehicular"] = $(this).find("input[id*='ConfigVehicular']").val();
                        item["PlacaVM"] = $(this).find("input[id*='PlacaVM']").val();
                        item["AnioModeloVM"] = $(this).find("input[id*='AnioModeloVM']").val();
                        item["SubTipoRem"] = $(this).find("input[id*='SubTipoRem']").val();
                        item["Placa"] = $(this).find("input[id*='Placa']").val();                
                        DATAtf.push(item);
                    });
                    aInfottf   = JSON.stringify(DATAtf); 
                //==================================================================
                    var table_aereo = $("#table_aereo tbody > tr");
                    var DATAtae  = [];
                    table_aereo.each(function(){ 
                        item = {}; 
                        item["PermSCT"]         = $(this).find("input[id*='PermSCT']").val();
                        item["NumPermisoSCT"]   = $(this).find("input[id*='NumPermisoSCT']").val();
                        item["MatriculaAeronave"]= $(this).find("input[id*='MatriculaAeronave']").val();
                        item["NombreAseg"]      = $(this).find("input[id*='NombreAseg']").val();
                        item["NumPolizaSeguro"] = $(this).find("input[id*='NumPolizaSeguro']").val();
                        item["NumeroGuia"]      = $(this).find("input[id*='NumeroGuia']").val();
                        item["LugarContrato"]   = $(this).find("input[id*='LugarContrato']").val();
                        item["RFCTransportista"]= $(this).find("input[id*='RFCTransportista']").val();
                        item["CodigoTransportista"]= $(this).find("input[id*='CodigoTransportista']").val();
                        item["NumRegIdTribTranspora"]= $(this).find("input[id*='NumRegIdTribTranspora']").val();
                        item["ResidenciaFiscalTranspor"]= $(this).find("input[id*='ResidenciaFiscalTranspor']").val();
                        item["NombreTransportista"]= $(this).find("input[id*='NombreTransportista']").val();
                        item["RFCEmbarcador"]   = $(this).find("input[id*='RFCEmbarcador']").val();
                        item["NumRegIdTribEmbarc"]= $(this).find("input[id*='NumRegIdTribEmbarc']").val();
                        item["ResidenciaFiscalEmbarc"]= $(this).find("input[id*='ResidenciaFiscalEmbarc']").val();
                        item["NombreEmbarcador"] = $(this).find("input[id*='NombreEmbarcador']").val();

                                       
                        DATAtae.push(item);
                    });
                    aInfottae   = JSON.stringify(DATAtae); 
                //===============================operador===================================
                    var table_operador = $("#table_operador tbody > tr");
                    var DATAope  = [];
                    table_operador.each(function(){ 
                        item = {}; 

                        item["RFCOperador"] = $(this).find("input[id*='RFCOperador']").val();
                        item["NumLicencia"] = $(this).find("input[id*='NumLicencia']").val();
                        item["NombreOperador"] = $(this).find("input[id*='NombreOperador']").val();
                        item["NumRegIdTribOperador"] = $(this).find("input[id*='NumRegIdTribOperador']").val();
                        item["ResidenciaFiscalOperador"] = $(this).find("input[id*='ResidenciaFiscalOperador']").val();
                        item["adddomicilio"] = $(this).find("input[id*='adddomicilio']").val();
                        item["Calle"] = $(this).find("input[id*='Calle']").val();
                        item["NumeroExterior"] = $(this).find("input[id*='NumeroExterior']").val();
                        item["NumeroInterior"] = $(this).find("input[id*='NumeroInterior']").val();
                        item["Colonia"] = $(this).find("input[id*='Colonia']").val();
                        item["Localidad"] = $(this).find("input[id*='Localidad']").val();
                        item["Referencia"] = $(this).find("input[id*='Referencia']").val();
                        item["Municipio"] = $(this).find("input[id*='Municipio']").val();
                        item["Estado"] = $(this).find("input[id*='Estado']").val();
                        item["Pais"] = $(this).find("input[id*='Pais']").val();
                        item["CodigoPostal"] = $(this).find("input[id*='CodigoPostal']").val();            
                        DATAope.push(item);
                    });
                    aInfotope   = JSON.stringify(DATAope); 
                //===============================propietario===================================
                    var table_propietario = $("#table_propietario tbody > tr");
                    var DATAprop  = [];
                    table_propietario.each(function(){ 
                        item = {}; 

                        item["RFCPropietario"] = $(this).find("input[id*='RFCPropietario']").val();
                        item["NombrePropietario"] = $(this).find("input[id*='NombrePropietario']").val();
                        item["NumRegIdTribPropietario"] = $(this).find("input[id*='NumRegIdTribPropietario']").val();
                        item["ResidenciaFiscalPropietario"] = $(this).find("input[id*='ResidenciaFiscalPropietario']").val();
                        item["adddomiciliop"] = $(this).find("input[id*='adddomiciliop']").val();
                        item["Calle"] = $(this).find("input[id*='Calle']").val();
                        item["NumeroExterior"] = $(this).find("input[id*='NumeroExterior']").val();
                        item["NumeroInterior"] = $(this).find("input[id*='NumeroInterior']").val();
                        item["Colonia"] = $(this).find("input[id*='Colonia']").val();
                        item["Localidad"] = $(this).find("input[id*='Localidad']").val();
                        item["Referencia"] = $(this).find("input[id*='Referencia']").val();
                        item["Municipio"] = $(this).find("input[id*='Municipio']").val();
                        item["Estado"] = $(this).find("input[id*='Estado']").val();
                        item["Pais"] = $(this).find("input[id*='Pais']").val();
                        item["CodigoPostal"] = $(this).find("input[id*='CodigoPostal']").val();            
                        DATAprop.push(item);
                    });
                    aInfotprop   = JSON.stringify(DATAprop); 
                //===============================arrendatario===================================
                //===============================arrendatario===================================
                    var table_arrendatario = $("#table_arrendatario tbody > tr");
                    var DATAarren  = [];
                    table_arrendatario.each(function(){ 
                        item = {}; 

                        item["RFCArrendatario"] = $(this).find("input[id*='RFCArrendatario']").val();
                        item["NombreArrendatario"] = $(this).find("input[id*='NombreArrendatario']").val();
                        item["NumRegIdTribArrendatario"] = $(this).find("input[id*='NumRegIdTribArrendatario']").val();
                        item["ResidenciaFiscalArrendatario"] = $(this).find("input[id*='ResidenciaFiscalArrendatario']").val();
                        item["adddomicilioa"] = $(this).find("input[id*='adddomicilioa']").val();
                        item["Calle"] = $(this).find("input[id*='Calle']").val();
                        item["NumeroExterior"] = $(this).find("input[id*='NumeroExterior']").val();
                        item["NumeroInterior"] = $(this).find("input[id*='NumeroInterior']").val();
                        item["Colonia"] = $(this).find("input[id*='Colonia']").val();
                        item["Localidad"] = $(this).find("input[id*='Localidad']").val();
                        item["Referencia"] = $(this).find("input[id*='Referencia']").val();
                        item["Municipio"] = $(this).find("input[id*='Municipio']").val();
                        item["Estado"] = $(this).find("input[id*='Estado']").val();
                        item["Pais"] = $(this).find("input[id*='Pais']").val();
                        item["CodigoPostal"] = $(this).find("input[id*='CodigoPostal']").val();            
                        DATAarren.push(item);
                    });
                    aInfotarrend  = JSON.stringify(DATAarren); 
                //==================================================================
                    var CveTransporte=$('#CveTransporte option:selected').val();
                    var cartaporte=$('#cartaporte').is(':checked')==true?1:0;

                if (productos.length>0 && band_vacio==0) {
                    

                    //var tiporelacion = $('input:radio[name=tiporelacion]:checked').val();
                    //var ventaviculada = $('#ventaviculada').val();
                    //var ventaviculadatipo = $('#ventaviculadatipo').val();
                    var Subtotal=$('#Subtotal').val();
                    var iva=$('#iva').val();
                    var total=$('#total').val();  

                    var f_r = $('#facturarelacionada').is(':checked')==true?1:0; //agregados version 4.0
                    var f_r_t = $('#TipoRelacion option:selected').val();
                    var f_r_uuid = $('#uuid_r').val();

                    /*var visr= $('#isr').val();
                    var vriva= $('#ivaretenido').val();
                    var v5millar= $('#5almillarval').val();
                    var outsourcing= $('#outsourcing').val();*/

                    
                    //datos=datos+'&subtotal='+Subtotal+'&iva='+iva+'&visr='+visr+'&vriva='+vriva+'&v5millar='+v5millar+'&outsourcing='+outsourcing+'&total='+total+'&conceptos='+aInfoa+'&id_venta='+$("#id_venta").val();
                    datos=datos+'&subtotal='+Subtotal+'&iva='+iva+'&total='+total+'&conceptos='+aInfoa+'&f_r='+f_r+'&f_r_t='+f_r_t+'&f_r_uuid='+f_r_uuid;
                    datos+='&TranspInternac='+TranspInternac+'&TotalDistRec='+TotalDistRec+'&EntradaSalidaMerc='+EntradaSalidaMerc+'&ViaEntradaSalida='+ViaEntradaSalida+'&ubicaciones='+aInfotu;
                datos+='&PesoBrutoTotal='+PesoBrutoTotal+'&UnidadPeso='+UnidadPeso+'&PesoNetoTotal='+PesoNetoTotal+'&NumTotalMercancias='+NumTotalMercancias+'&CargoPorTasacion='+CargoPorTasacion;
                datos+='&mercancias='+aInfotm+'&transportes='+aInfottf+'&aereo='+aInfottae+'&operadores='+aInfotope+'&propietario='+aInfotprop+'&arrendatario='+aInfotarrend+'&CveTransporte='+CveTransporte+'&cartaporte='+cartaporte;
                    $('#modal_previefactura').modal({
                        dismissible: false
                    });
                    setTimeout(function(){ 
                        var urlfac=base_url+"index.php/Preview/factura?"+datos;
                        var htmliframe="<iframe src='"+urlfac+"' title='description' class='ifrafac'>";
                        $('.preview_iframe').html(htmliframe);
                        //window.location.href = base_url+"index.php/Preview/factura?"+datos; 
                    }, 1000);
        
                }else{
                    swal.fire('Atención!','Agregar por lo menos un concepto con unidad/concepto SAT','warning');
                    $('body').loading('stop');
                }
            }else{
                swal.fire('Atención!','Faltan campos obligatorios','warning');
                $('body').loading('stop');
            }
        }, 1000);
}
function addinfodf(fac_nrs,fac_cod,RegimenFiscalReceptor){
            fac_nrs=fac_nrs.replace(' ', '-');
            fac_nrs=fac_nrs.replace(' ', '-');
            fac_nrs=fac_nrs.replace(' ', '-');
            fac_nrs=fac_nrs.replace(' ', '-');
            fac_nrs=fac_nrs.replace(' ', '-');
            fac_nrs=fac_nrs.replace(' ', '-');
            fac_nrs=fac_nrs.replace(' ', '-');
            fac_nrs=fac_nrs.replace(' ', '-');

        var regimen = selectregimenf(RegimenFiscalReceptor);



        var html='<div class="col-md-4">\
                        <b>Nombre Fiscal: </b><br>\
                        <span><span title="los guiones representan los espacios">'+fac_nrs+'</span></span>\
                    </div>\
                    <div class="col-md-4">\
                        <b>Direccion Fiscal(C.P.): </b><br><span>'+fac_cod+'</span>\
                    </div>\
                    <div class="col-md-4">\
                        <b>Regimen Fiscal Receptor: </b><br><span>'+regimen+'</span>\
                    </div>';
        $('.infodatosfiscales').html(html);
}
function selectregimenf(clave){
    switch (clave) {
        case '601':
            var name = '601 General de Ley Personas Morales';
            break;
        case '603':
            var name = '603 Personas Morales con Fines no Lucrativos';
            break;
        case '605':
            var name = '605 Sueldos y Salarios e Ingresos Asimilados a Salarios';
            break;
        case '606':
            var name = '606 Arrendamiento';
            break;
        case '607':
            var name = '607 Régimen de Enajenación o Adquisición de Bienes';
            break;
        case '608':
            var name = '608 Demás ingresos';
            break;
        case '609':
            var name = '609 Consolidación';
            break;
        case '610':
            var name = '610 Residentes en el Extranjero sin Establecimiento Permanente en México';
            break;
        case '611':
            var name = '611 Ingresos por Dividendos (socios y accionistas)';
            break;
        case '612':
            var name = '612 Personas Físicas con Actividades Empresariales y Profesionales';
            break;
        case '614':
            var name = '614 Ingresos por intereses';
            break;
        case '615':
            var name = '615 Régimen de los ingresos por obtención de premios';
            break;
        case '616':
            var name = '616 Sin obligaciones fiscales';
            break;
        case '620':
            var name = '620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos';
            break;
        case '621':
            var name = '621 Incorporación Fiscal';
            break;
        case '622':
            var name = '622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras';
            break;
        case '623':
            var name = '623 Opcional para Grupos de Sociedades';
            break;
        case '624':
            var name = '624 Coordinados';
            break;
        case '626':
            var name = '626 Régimen Simplificado de Confianza';
            break;
        case '628':
            var name = '628 Hidrocarburos';
            break;
        case '629':
            var name = '629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales';
            break;
        case '630':
            var name = '630 Enajenación de acciones en bolsa de valores';
            break;

      default:
        var name='';
    }
    return name;
}

