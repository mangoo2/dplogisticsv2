var base_url =$('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table_list').DataTable();
	loadtable();
});
function loadtable(){
	var tipo=$('#tipo_ed_de option:selected').val();
	table.destroy();
	table=$("#table_list").DataTable({
    		stateSave: true,
    		//responsive: !0,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		       "url": base_url+"Documentar/getlistaedicioneliminado",
		       type: "post",
		       "data": {
                'tipo':tipo
            	},
		        error: function(){
		           $("#table").css("display","none");
		        }
		    },
		    "columns": [
		        {"data": "rid"},
		        {"data": "cot_feccap"},
		        {"data": "usuario"},
		        {"data": "aer_nom"},
		        {"data": "cot_folio",
		        	"render": function ( data, type, row, meta ) {
		        		var html='';
		        			html=row.cot_folio+' '+row.cot_guia;
		        		return html;
		        	}
		    	},
				{"data": "clasifi"},
				{"data": "cot_descrip"},
				{"data": "rutao"},
				{"data": "rutad"},
				{"data": "cliente"},
		        {"data": "cot_estatus",
		        	"render": function ( data, type, row, meta ) {
		                var html="";
		                    if(row.cot_estatus=='doc'){
		                    	html="En documentación";
		                    }else if(row.cot_estatus=='cyf'){
								html="En consignatario y Factura";
		                    }else if(row.cot_estatus=='fyg'){
								html="En Folio y Guia";
		                    }else if(row.cot_estatus=='pdg'){
								html="En pago de guia";
		                    }else if(row.cot_estatus=='fdg'){
								html="Impresion de nota de guia";
		                    }else{
								html="Error de estatus";
		                    }
		            	return html;
		            }
		    	},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="";
		                	if(row.tipo_ed_de==1){
		                		html='<a class="btn btn-success btn-sm font-weight-bold btn-pill">Edición</a>';
		                	}else{
		                		html='<a class="btn btn-danger btn-sm font-weight-bold btn-pill">Eliminación</a>';
		                	}

		                    
		                   
		            	return html;
		            }
		        },
		        {"data": "sol_edit_delete_personal"},
		        {"data": "personal_confirmo"},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="<div style='width:170px;'>";
		                	if(row.cot_estatus=='doc'){
		                		html+='<a target="_blank" href="'+base_url+'docdirectorio/cotizacion_'+row.cotiza+'_'+row.cot_folio+'.pdf" type="button" class="btn btn-info mr-1 mb-1"><i class="fa fa-file-pdf-o"></i></a>';
		                	}
		                	
		                    
		                   html+="</div>";
		            	return html;
		            }
		        },
		    ],
    		"order": [[ 0, "desc" ]],
    		"lengthMenu": [[25, 50, 100], [25, 50, 100]],
  	}).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
   	});
}
/*
function searchtable_v(){
    var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}
*/
