var base_url=$('#base_url').val();
var v_rid;
var v_rrid;
var v_evi_nombre;
$(document).ready(function() {
	$("#evi_archivo").fileinput({
            showCaption: false,
            dropZoneEnabled: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["png","jpg","jpeg","bmp","pdf"],
            browseLabel: 'Seleccionar Imagen',
            uploadUrl: base_url+'Coe_rechum/inserupdateevi',
            inputGroupClass: "input-group-sm",
            maxFilePreviewSize: 5000,
            uploadExtraData: function (previewId, index) {
                var info = {
                            rid:v_rid,
							rrid:v_rrid,
							evi_nombre:v_evi_nombre
                        };
                return info;
            }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
        }).on('filebatchuploadsuccess', function(event, files, extra) {
          location.reload();
          //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        });
});
function editar(id){
	$('#modalform').modal({backdrop: 'static', keyboard: false});
	if(id>0){
		$('#modalform .modal-title').html('Actualizar requisición');
		$('.saveform').html('Actualizar');
		$('#rid').val(id);
		var evi_archivo = $('.edit_form_'+id).data('evi_archivo');
		$('.evi_archivo').val(evi_archivo);
		var evi_nombre = $('.edit_form_'+id).data('evi_nombre');
		$('#evi_nombre').val(evi_nombre);

	}else{
		$('#modalform .modal-title').html('Agregar requisición');
		$('.saveform').html('Guardar');
		$('#rid').val(0);
		$('#evi_nombre').val('');

		$('#evi_archivo').fileinput('clear');
	}
}

function saveform() {
    v_rid = $('#rid').val();
	v_rrid = $('#rrid').val();
	v_evi_nombre = $('#evi_nombre').val();
    
    var varform=$('#form_datos');
    if(varform.valid()){
         $('#evi_archivo').fileinput('upload');    
    }
}
function deleterh(id){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma borrar él registro',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Coe_rechum/deleteevi",
                        data: {
                            rid:id
                        },
                        success: function (response){
                            toastr["success"](" eliminada", "Hecho"); 
                            location.reload();                         
                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}