var base_url = $('#base_url').val();

function editarc(id){
	$('#modalformc').modal({backdrop: 'static', keyboard: false});
	if(id>0){
		$('#modalformc #exampleModalLiveLabel').html('Editar cliente');
        $('.saveformc').html('Editar');
        $('#modalformc #rid').val(id);
		var cli = $('.edit_formc_'+id).data('cli');
		var cli_nom = $('.edit_formc_'+id).data('cli_nom');
		var cli_pos = $('.edit_formc_'+id).data('cli_pos');
		$('#form_datosc #cli').val(cli);
		$('#form_datosc #cli_nom').val(cli_nom);
		$('#form_datosc #cli_pos').val(cli_pos);
	}else{
		$('#modalformc #exampleModalLiveLabel').html('Agregar cliente');
        $('.saveformc').html('Agregar');
        $('#form_datosc')[0].reset();
	}
	$('#modalformc #emp').val($('#empsss').val());
}
function editars(id){
	$('#modalforms').modal({backdrop: 'static', keyboard: false});
	if(id>0){
		$('#modalforms #exampleModalLiveLabel').html('Editar servicio');
        $('.saveformc').html('Editar');
        $('#modalforms #rid').val(id);
        var ser_pos = $('.edit_forms_'+id).data('ser_pos');
		var ser = $('.edit_forms_'+id).data('ser');
		var ser_nom = $('.edit_forms_'+id).data('ser_nom');
		$('#form_datoss #ser_pos').val(ser_pos);
		$('#form_datoss #ser').val(ser);
		$('#form_datoss #ser_nom').val(ser_nom);
	}else{
		$('#modalforms #exampleModalLiveLabel').html('Agregar servicio');
        $('.saveforms').html('Agregar');
        $('#form_datoss')[0].reset();	
	}
	$('#modalforms #emp').val($('#empsss').val());
}
function saveformc(){
	var varform=$('#form_datosc');
        if(varform.valid()){
            var datos = varform.serialize();
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Conf_sers/inserupdate_c",
                data: datos,
                success: function (response){
                  $('#modalformc').modal('hide');
                  
                  toastr["success"]("Informacion Actualizada", "Hecho"); 
                  
                  setTimeout(function(){
                    location.reload();
                  }, 2000);

                },
                error: function(response){
                    toastr["error"]("Algo salio mal", "Error"); 
                     
                }
              });                
        }
}
function saveforms(){
	var varform=$('#form_datoss');
        if(varform.valid()){
            var datos = varform.serialize();
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Conf_sers/inserupdate_s",
                data: datos,
                success: function (response){
                  $('#modalforms').modal('hide');
                  
                  toastr["success"]("Informacion Actualizada", "Hecho"); 
                  
                  setTimeout(function(){
                    location.reload();
                  }, 2000);

                },
                error: function(response){
                    toastr["error"]("Algo salio mal", "Error"); 
                     
                }
              });                
        }
}
function deletec(id,tipo){
        $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma borrar él registro',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Conf_sers/delete",
                        data: {
                            rid:id,
                            tipo:tipo
                        },
                        success: function (response){
                            toastr["success"](" eliminada", "Hecho"); 
                            location.reload();                         
                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}