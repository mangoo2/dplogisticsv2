var base_url=$('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table_list').DataTable();
	loadtable();
});
function loadtable(){
	var inicio =$('#fecha_i').val();
	var fin =$('#fecha_t').val();
	table.destroy();
	table=$("#table_list").DataTable({
    		stateSave: true,
    		responsive: !0,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		       "url": base_url+"Corte_caja/getlista",
		       type: "post",
		       "data": {
                'finicio':inicio,
                'ffin':fin
            	},
		    },
		    "columns": [
		        {"data":"corte",
		        	render:function(data,type,row){
                    		var html='';
                        
                    		html='<input type="checkbox" name="id_corte" id="id_corte" value="'+row.rid+'">';
                    	return html;
                	}
		    	},
		        {"data": "corte"},
		        {"data": "cor_fecha"},
		        {"data":"cor_efectivo",
		        	render:function(data,type,row){
                    		var html='';
                        
                    		html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(row.cor_efectivo);
                    	return html;
                	}
		    	},
				{"data":"cor_credito",
					render:function(data,type,row){
                    		var html='';
                        
                    		html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(row.cor_credito);
                    	return html;
                	}
				},
				{"data":"cor_com"},
				{"data":"cor_admnom"},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="";
		                    html+='<div class="btn-group dropdown mr-1 mb-1">\
                                        <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i></button>\
                                        <div class="dropdown-menu" style="">\
                                            <a class="dropdown-item" href="'+base_url+'Corte_caja/cor_ver/'+row.corte+'">Ver Corte</a>\
                                            <a class="dropdown-item comprobanteg_'+row.rid+'" data-corte="'+row.corte+'" onclick="comprobanteg('+row.rid+')" >Comprobante</a>\
                                        </div>\
                                    </div>';
		            return html;
		            }
		        },
		    ],
    		"order": [[ 0, "DESC" ]],
    		"lengthMenu": [[25, 50, 100], [25, 50, 100]],
  	}).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
   	});
}
function comprobanteg(rid){
	var corte = $('.comprobanteg_'+rid).data('corte');
	$('#idcorte').val(corte);
	$('#modal_comprobante').modal();
	$.ajax({
	      type:'POST',
	      url: base_url+'index.php/Corte_caja/comprobarfiles',
	      data: {
	      	corte:$('#idcorte').val()
	      },
	      success:function(data){
	      	$('.datasimages').html(data);

	      	var $gallery = new SimpleLightbox('.datasimages a', {});
	      }
	});
	
	$("#comprobante").fileinput({
            showCaption: false,
            //rtl: true,
            allowedFileExtensions: ["png","jpg","jpeg","bmp","pdf"],
            browseLabel: 'Seleccionar Imagen',
            uploadUrl: base_url+'Corte_caja/comprobanteg',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            },
            uploadExtraData: function (previewId, index) {
	            var info = {
	            			id:$('#idcorte').val()

	            		};
	            return info;
	        }
    });
    
}
function agregarcomprobanteall(){
	

	var guiaslis = $("#table_list tbody > tr");
    	var DATAa  = [];
    	var num_guias_selected=0;
        guiaslis.each(function(){  
        	if ($(this).find("input[id*='id_corte']").is(':checked')) {
        		num_guias_selected++;
        		item = {};                    
            	item ["cortesIds"]  = $(this).find("input[id*='id_corte']").val();
                
            	DATAa.push(item);
        	}       
            
        });
        aInfoa   = JSON.stringify(DATAa);
        if(num_guias_selected>0){
        	$('#modal_comprobante_all').modal();
			$("#comprobanteall").fileinput({
		            showCaption: false,
		            //rtl: true,
		            allowedFileExtensions: ["png","jpg","jpeg","bmp","pdf"],
		            browseLabel: 'Seleccionar Imagen',
		            uploadUrl: base_url+'Corte_caja/comprobantegall',
		            maxFilePreviewSize: 5000,
		            previewFileIconSettings: {
		                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
		                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
		                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
		                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
		                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
		                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
		            },
		            uploadExtraData: function (previewId, index) {
			            var info = {
			            			cortes:aInfoa

			            		};
			            return info;
			        }
		    });
		}else{
			toastr["error"]("Selecciona una o mas cortes del listado", "Advertencia")
		}
}