var base_url =$('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table_list').DataTable();
	loadtable();
});
function loadtable(){
	table.destroy();
	table=$("#table_list").DataTable({
    		stateSave: true,
    		//responsive: !0,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		       "url": base_url+"Clientes/getlista",
		       type: "post",
		       "data": function(d){
		        // d.bodegaId = $('#bodegaId option:selected').val()
		        },
		        error: function(){
		           $("#table").css("display","none");
		        }
		    },
		    "columns": [
		        {"data": "rid"},
		        {"data": "clientename"},
		        {"data":"cli_telefono"},
				{"data":"cli_celular"},
				{"data":"cli_email"},
				{"data":"cli_io"},
				{"data":"cli_cum"},
				{"data":"cli_com"},
				{"data":"cli_des"},
				{"data":"credito",
					"render": function ( data, type, row, meta ) {
						var html="";
							if(row.credito==1){
								var checkboxcredito='checked';
							}else{
								var checkboxcredito='';
							}
							html+='<input type="checkbox" id="clientecheck_'+row.rid+'" onchange="checkboxcredito('+row.rid+')" '+checkboxcredito+'>';
						return html;
					}
				},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="";
		                	html+='<div class="divbuttonstable">';
		                    html+='<a href="'+base_url+'Clientes/add/'+row.rid+'" type="button" class="btn btn-success mr-1 mb-1"><i class="fa fa-pencil fa-fw"></i></a>';
		                    html+='<a href="'+base_url+'Clientes/cliente/'+row.cliente+'" type="button" class="btn btn-info mr-1 mb-1"><i class="fa fa-folder-open fa-fw"></i></a>';
		                    html+='</div>';
		            return html;
		            }
		        },
		    ],
    		"order": [[ 1, "asc" ]],
    		"lengthMenu": [[10,25, 50, 100], [10,25, 50, 100]],
  	}).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
   	});
}
/*
function searchtable_v(){
    var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}
*/
function checkboxcredito(id){
	var checked = $('#clientecheck_'+id).is(':checked')==true?1:0;
	$.ajax({
      	type:'POST',
      	url: base_url+'index.php/Clientes/checkboxcredito',
      	data: {
      		rid:id,
      		credito:checked
      	},
      	success:function(data){
          
      	}
  	});
}