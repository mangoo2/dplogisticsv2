var base_url=$('#base_url').val();
$(document).ready(function($) {
	$('#table_list').DataTable({
    	ordering: false
	});
});

function delete_pam_bit(id){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma la eliminación del registro',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Coe_pam/bma_bor",
                        data: {
                            rid:id
                        },
                        success: function (response){
                            toastr.success('Registro eliminado');
                            setTimeout(function(){ 
                                location.reload();
                            }, 1000);

                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function add_bit(id){
	$('#mdlBmaNue').modal({backdrop: 'static', keyboard: false});
	if(id>0){
		$('#mdlBmaNue_label').html('Editar Servicio');
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Coe_pam/conultar_pam_bit",
            data: {rid:id},
            success: function (response){
                var array = $.parseJSON(response);
                console.log(array);
                $('#rid').val(array.rid);
                $('#bit_fecha').val(array.bit_fecha);
                $('#bit_odometro').val(array.bit_odometro);
                $('#bit_tipo').val(array.bit_tipo);
                $('#bit_clase').val(array.bit_clase);
                $('#bit_preventivo').val(array.bit_preventivo);
                $('#bit_correctivo').val(array.bit_correctivo);
                $('#bit_comfon').val(array.bit_comfon);
                $('#bit_comcr').val(array.bit_comcr);
                $('#bit_comine').val(array.bit_comine);
                $('#bit_piecoc').val(array.bit_piecoc);
                $('#bit_piefon').val(array.bit_piefon);
                $('#bit_piefot').val(array.bit_piefot);
                $('#bit_oso').val(array.bit_oso);
                $('#bit_costo').val(array.bit_costo);
                $('#bit_respon').val(array.bit_respon);
            },
            error: function(response){
                toastr["error"]("No se pudo procesar", "Error"); 
                 
            }
        });
	}else{
        $('#frmNue')[0].reset();
		$('#mdlBmaNue_label').html('Agregar Servicio');
	}
}
function savemdlBmaNue(){
    var form = $('#frmNue');
    var suc = $('#suc').val();
    var equ = $('#equ').val();
    var pam = $('#pam').val();
    if(form.valid()){
        var datos =form.serialize()+'&suc='+suc+'&equ='+equ+'&pam='+pam;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Coe_pam/bma_agr",
            data: datos,
            success: function (response){
                toastr.success('Registro agregado');
                setTimeout(function(){ 
                    location.reload();
                }, 1000);

            },
            error: function(response){
                toastr["error"]("No se pudo procesar", "Error"); 
                 
            }
        });
    }else{
        toastr.error('Es necesario los campos requeridos');
    }
}