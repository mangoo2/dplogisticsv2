var base_url = $('#base_url').val();
var table;
var envionfactura;
var enviocliente;
$(document).ready(function($) {
    $('#idcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Facturas/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.fac_rfc,
                        text: element.fac_nrs

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        
    });
	table = $('#tabla_facturacion').DataTable();
	loadtable();
    $('.enviocorreomail').click(function(event) {
        var DATAr  = [];
        var TABLA   = $("#table_mail_envios tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["correo"] = $(this).find("input[id*='mailcli']").val();
            DATAr.push(item);
        });
        INFO  = new FormData();
        arraycorreos   = JSON.stringify(DATAr);

        $.ajax({
            type:'POST',
            url: base_url+"index.php/Envio/enviocorreo_comp",
            data: {
                factura:envionfactura,
                correo:$('#correocliente').val(),
                asunto:$('#correoasunto').val(),
                mensaje:$('#correomensaje').val()
            },
            success:function(response){  
                console.log(response);
                Swal.fire({
                                position: "center",
                                icon: "success",
                                title: "Email",
                                text: 'Correo enviado',
                                showConfirmButton: false,
                                timer: 2500
                            });
                loadtable();
                $('#modalseleccioncorreo').modal('hide');
            }
        });
    });
});
function loadtable(){

    var idcliente=$('#idcliente option:selected').val()==undefined?0:$('#idcliente option:selected').val();
    var inicio=$('#finicial').val();
    var fin=$('#ffin').val();
    var tipoempresa = $('#tipoempresa option:selected').val();
    if(tipoempresa==1){
        $('.sucursales_view').show('show');
        var sucursal = $('#sucursal option:selected').val();
    }else{
        var sucursal =0;
        $('#sucursal').val(0);
        $('.sucursales_view').hide('show');
    }
    var cfdi = $('#t_cfdi option:selected').val();
    var ecomp = $('#e_comp option:selected').val();

	table.destroy();
	table = $('#tabla_facturacion').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/complementosPago/getlistcomplementos",
            type: "post",
            "data": {
                'cliente':idcliente,
                'finicio':inicio,
                'ffin':fin,
                'tipoempresa':tipoempresa,
                'sucursalrow':sucursal,
                'cfdi':cfdi,
                'ecomp':ecomp
            },
        },
        "columns": [
            {"data": "complementoId",
            	render:function(data,type,row){
            		var html='';
            		if(row.Estado==1){
                        
				        html='<span class="switch switch_morado switch-icon">\
                                    <label>\
                                        <input type="checkbox" class="facturas_checked" id="factura_'+row.complementoId+'" value="'+row.complementoId+'">\
                                        <span></span>\
                                    </label>\
                                </span>';
                    }else{
				        html='<span class="switch switch_morado switch-icon">\
                                    <label>\
                                        <input type="checkbox" class="facturas_checked" id="factura_'+row.complementoId+'" value="'+row.complementoId+'" disabled>\
                                        <span></span>\
                                    </label>\
                                </span>';
                    }
                    return html;
            	}
        	},
            {"data": "Serie"},
            {"data": "Folio",
                render:function(data,type,row){
                    var html='';
                        if(row.Folio>0){
                            html=row.Folio;
                        }
                    return html;
                }
            },
            {"data": "R_nombre"},
            {"data": "R_rfc"},
            {"data": "Monto",
                render:function(data,type,row){
                    var html='';
                        
                    html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(row.Monto);
                    return html;
                }
            },
            {"data": "Estado",
                render:function(data,type,row){
                    var html='';
                    if(row.Estado==0){
                        html='<a class="btn btn-danger  font-weight-bold btn-pill">Cancelado</a>';
                        html+='<div class="validarcancelacion '+row.uuid+'" data-uuid="'+row.uuid+'" style="color:red"></div>';
                    }else if(row.Estado==1){
                        html='<a class="btn btn-success font-weight-bold btn-pill">Timbrado</a>';
                    }else if(row.Estado==2){
                        html='<a class="btn btn-warning font-weight-bold btn-pill">Sin timbrar</a>';
                    }else{
                        html='';
                    }
                    return html;
                }
            },
            {"data": 'fechatimbre'},
            {"data": null,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='<div class="btns-factura">';
                    if(row.Estado==0){ // cancelado
                        html+='<a \
		                        class="btn btn-info mr-1 mb-1 tooltipped" \
		                        href="'+base_url+'Facturas/complementodoc/'+row.complementoId+'" \
		                        target="_blank"\
		                        data-position="top" data-delay="50" data-tooltip="Factura"\
		                      ><i class="fa fa-file-pdf-o fa-fw"></i>\
                      		</a> ';
		                html+='<a\
			                        class="btn btn-info mr-1 mb-1 tooltipped" \
			                        href="'+base_url+row.rutaXml+'" \
			                        target="_blank"\
			                        data-position="top" data-delay="50" data-tooltip="XML"\
			                      download ><i class="fa fa-file-code-o"></i>\
                      				</a> ';
                        html+='<a\
                                    class="btn btn-info mr-1 mb-1 tooltipped" \
                                    href="'+base_url+'files_sat/facturas/'+row.rutaAcuseCancelacion+'" \
                                    target="_blank"\
                                    data-position="top" data-delay="50" data-tooltip="Acuse de cancelación"\
                                  download ><i class="icon-xl fa fa-code" style="color: #ef7e4f;"></i>\
                                    </a> ';
                    }else if(row.Estado==1){
                        html+='<a \
		                        class="btn btn-info mr-1 mb-1 tooltipped" \
		                        href="'+base_url+'Facturas/complementodoc/'+row.complementoId+'" \
		                        target="_blank"\
		                        data-position="top" data-delay="50" data-tooltip="Factura" title="Factura"\
		                      ><i class="fa fa-file-pdf-o fa-fw"></i>\
                      		</a> ';
		                html+='<a\
			                        class="btn btn-info mr-1 mb-1 tooltipped" \
			                        href="'+base_url+row.rutaXml+'" \
			                        target="_blank"\
			                        data-position="top" data-delay="50" data-tooltip="XML" title="XML" \
			                      download ><i class="fa fa-file-code-o"></i>\
                      			</a> ';
                                
                        html+='<a\
                                    class="btn btn-info mr-1 mb-1 tooltipped enviomodal_'+row.complementoId+'" \
                                    data-position="top" data-correocliente="'+row.fac_email+'" data-delay="50" data-tooltip="XML" title="Envio" onclick="enviomodal('+row.complementoId+')"\
                                  download ><i class="fa fa-paper-plane fa-fw"></i>\
                                </a> ';
                            /*
                            if(row.correoenviado==1){
                                var stylecorreo='style="color: #12264b;"';
                            }else{
                                var stylecorreo='';
                            }
                        html+='<a type="button"\
                          class="btn btn-info mr-1 mb-1 tooltipped btn-retimbrar" onclick="enviarfactura('+row.FacturasId+','+row.clienteId+')"\
                          data-position="top" data-delay="50" data-tooltip="Editar" id="refacturar">\
                          <span class="fa-stack">\
                          <i class="icon-xl fa fa-paper-plane" '+stylecorreo+'></i>\
                          </span>\
                        </a> ';
                        */
                        
                    }else if(row.Estado==2){
                        if(row.Folio>0){
                            html+='<a class="btn btn-danger font-weight-bold btn-pill" onclick="retimbrar('+row.complementoId+',0)">Retimbrar</a>  ';
                            html+='<a class="btn btn-danger mr-1 mb-1 tooltipped" onclick="infoviewerror('+row.complementoId+')" data-position="top" data-delay="50" data-tooltip="Factura" data-original-title="" title=""><i class="fa fa-eye fa-fw"></i></a>';
                        }
                        
                        
                        html+='<a type="button"\
                          class="b-btn b-btn-primary tooltipped btn-retimbrar" onclick="deletefactura('+row.complementoId+')"\
                          data-position="top" data-delay="50" data-tooltip="Editar" id="refacturar">\
                          <span class="fa-stack">\
                          <i class="icon-xl far fa-trash-alt"></i>\
                          </span>\
                        </a> ';
                        
                        
                    }else{
                    	html+='';
                    }
                    html+='</div>';
                    return html;
                }
            }
            
          
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]]
        
    }).on('draw',function(){
        $(".facturacomplemento").each(function() {
            var factura = parseFloat($(this).data('facturacomplemeto'));
            var total = parseFloat($(this).data('facturatotal'));
            //estatusdecomplementos(factura,total);
        });
        verificarcancelacion();
    });
}
function estatusdecomplementos(factura,total){
    $.ajax({
        type:'POST',
        url: base_url+'Folios/estatusdecomplementos',
        data: {
            factura:factura
        },
        statusCode:{
            404: function(data){
                //Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                //Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            var totalcomplemento=parseFloat(data);
            console.log('complemento: '+totalcomplemento);
            console.log('factura: '+total);
            if(totalcomplemento>=total){
                console.log('se completo');
                $(".facturacomplemento_"+factura).css("color","#102243");
            }
           
        }
    });
}

function retimbrar(idfactura,contrato){
    $('body').loading({theme: 'dark',message: 'Timbrando factura...'});
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de generar el retimbrado?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Timbrar/retimbrarcomplemento",
                    data: {
                        complemento:idfactura
                    },
                    success:function(response){  
                        console.log(response);
                        //console.log(array.MensajeErrorDetallado);
                        var array = $.parseJSON(response);
                        if (array.resultado=='error') {
                            
                            Swal.fire({
				                icon: "error",
				                title: "Error "+array.CodigoRespuesta+"!",
				                text: array.MensajeError,
				                footer: 'Detalle: '+array.info.TimbrarCFDIResult.MensajeErrorDetallado,
				            });
                            
                            $('body').loading('stop');
                        }else{
                            Swal.fire({
				                position: "Éxito",
				                icon: "success",
				                title: "Se ha creado la factura",
				                showConfirmButton: false,
				                timer: 1500
				            });
                            $('body').loading('stop');
                            loadtable();
                        }

                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function cancelarfacturas(){
    var motselect=$('#mot_can').html();
    var html='<div class="row col-md-12">\
                <div class="col-md-12">\
                    ¿Está seguro de realizar la Cancelacion de la factura?<br>Se necesita permisos de administrador\
                </div>\
                <div class="col-md-12">\
                    <form method="post" action="#" autocomplete="off">\
                    <input type="password" placeholder="Contraseña" id="contrasena" class="name form-control form-control2" required/>\
                    </form>\
                </div>\
              </div>\
              <div class="row col-md-12">\
                <div class="col-md-6">\
                    <label>Motivo Cancelación SAT</label>\
                    <select id="motivocancelacion" class="form-control form-control2" onchange="mocance()">\
                        <option value="00" disabled selected> Seleccione una opción</option>\
                        <option value="01" >01 Comprobante emitido con errores con relación</option>\
                        <option value="02" >02 Comprobante emitido con errores sin relación</option>\
                        <option value="03" >03 No se llevó a cabo la operación</option>\
                        <option value="04" >04 Operación nominativa relacionada en una factura global</option>\
                    </select>\
                </div>\
                <div class="col-md-6 class_foliorelacionado" style="display:none">\
                    <label>Folio Relacionado</label>\
                    <input type="text" id="foliorelacionado" class="form-control form-control2" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">\
                </div>\
                <!--<div class="col-md-6">\
                    <label>Motivo Cancelación</label>\
                    <select id="motselect" class="form-control form-control2">\
                        '+motselect+'\
                    </select>\
                </div>-->\
              </div>';
    $.confirm({
        boxWidth: '55%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $('body').loading({theme: 'dark',message: 'Cancelando factura...'});
                var pass=$('#contrasena').val();
                var motivo = $('#motivocancelacion option:selected').val();
                //var motselect=$('#motselect option:selected').val();
                var motselect=0;
                var uuidrelacionado=$('#foliorelacionado').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                //===================================================
                    var facturaslis = $("#tabla_facturacion tbody > tr");
                    var DATAa  = [];
                    var num_facturas_selected=0;
                    facturaslis.each(function(){  
                        if ($(this).find("input[class*='facturas_checked']").is(':checked')) {
                            num_facturas_selected++;
                            item = {};                    
                            item ["FacturasIds"]  = $(this).find("input[class*='facturas_checked']").val();
                            
                            DATAa.push(item);
                        }       
                        
                    });
                    INFOa  = new FormData();
                    aInfoa   = JSON.stringify(DATAa);
                    //console.log(aInfoa);
                    if (num_facturas_selected==1) {
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Timbrar/cancelarCfdicomplemento",
                            data: {
                                facturas:aInfoa,
                                motivo:motivo,
                                uuidrelacionado:uuidrelacionado,
                                motselect:motselect
                            },
                            success:function(response){  
                                console.log(response);
                                var array = $.parseJSON(response);
                                if (array.resultado=='error') {
                                    Swal.fire({
                                        position: "Éxito", icon: "error",
                                        title: "Error "+array.CodigoRespuesta+"!",
                                        text: array.MensajeError,
                                        showConfirmButton: false, timer: 2500
                                    });
                                    $('body').loading('stop');
                                }else{
                                    Swal.fire({
                                        position: "Éxito",icon: "success",title: "Se ha cancelado la factura",showConfirmButton: false,timer: 1500
                                    });
                                    $('body').loading('stop');
                                    loadtable();
                                }

                            }
                        });
                    }else{
                        Swal.fire({
                                position: "Éxito",icon: "warning",title: "Atención!",text: 'Seleccione solo una factura',showConfirmButton: false,timer: 2500
                                    });
                        $('body').loading('stop');
                    }
                //================================================
                                }else{
                                    Swal.fire({
                                        position: "Éxito",icon: "warning",title: "Atención!",text: 'No tiene permiso',showConfirmButton: false,timer: 2500
                                    });
                                    $('body').loading('stop');
                                }
                        },
                        error: function(response){
                            Swal.fire({
                                        position: "Éxito",icon: "error",title: "Error!",text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',showConfirmButton: false,timer: 2500
                                    }); 
                            $('body').loading('stop');   
                             
                        }
                    });
                    
                }else{
                    Swal.fire({
                            position: "Éxito",icon: "warning",title: "Advertencia!",text: 'Ingrese una contraseña',showConfirmButton: false,timer: 2500
                                    });  
                }
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function mocance(){
    var motivo=$('#motivocancelacion option:selected').val();
    if(motivo=='01'){
        $('.class_foliorelacionado').show('show');
    }else{
        $('.class_foliorelacionado').hide('show');
        $('#foliorelacionado').val('');
    }
}
var facturacomplemtoid;
function complementop(facturacomplemto){
    facturacomplemtoid=facturacomplemto;
    $('#modalcomplementos').modal();
    $('.listadocomplementos').html('');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Facturas/listasdecomplementos",
        data: {
            facturas:facturacomplemto
        },
        success:function(response){  
           $('.listadocomplementos').html(response);
           $('#tableliscomplementos').DataTable();
        }
    });
}
function addcomplemento(){
    window.location.href = base_url+'complementosPago/add/'+facturacomplemtoid;
}


function search(){
    var searchtext = $('#searchtext').val();
    table.search(searchtext).draw();
}
/*
function deletefactura(id){
    

    $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Desea eliminar este registro, recuerda que no se encuentra timbrado?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    //======================================================
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Folios/deletefactura",
                        data: {
                            factura:id
                        },
                        success:function(response){  
                           loadtable();
                        }
                    });
                    //=========================================
                },
                cancelar: function () 
                {
                    
                }
            }
        });
}
function deletemailcli(row){
    $('.climailid_'+row).remove();
}
*/

function enviomodal(idfactura){
    envionfactura=idfactura;
    $('.iframepdf').html('<iframe src="'+base_url+'Facturas/complementodoc/'+idfactura+'"></iframe>');
    var correo = $('.enviomodal_'+idfactura).data('correocliente');
    $('#correocliente').val(correo);
    $('#modalenviofact').modal();
}


function excel(){
    $('.btn_ex1').css('display','none');
    $('.btn_ex2').css('display','block');
    $('.input_fecha_e').css('display','block');
}

function excel_exportal(){
    var fecha_i=$('#finicial').val();
    var fecha_f=$('#ffin').val();
    if(fecha_i!=''){
        if(fecha_f!=''){
            exportal_excel();
        }else{
            Swal.fire(
              '¡Atención!',
              'Falta agregar alguna fecha',
              'error'
            ); 
        }         
    }else{
        Swal.fire(
          '¡Atención!',
          'Falta agregar alguna fecha',
          'error'
        );    
    }
}

function exportal_excel(){
    var f1=$('#finicial').val();
    var f2=$('#ffin').val();
    var tipoempresa = $('#tipoempresa option:selected').val();
    var sucursal = $('#sucursal option:selected').val();
    var cfdi = $('#t_cfdi option:selected').val();
    var ecomp = $('#e_comp option:selected').val();
    var idcliente=$('#idcliente option:selected').val()==undefined?0:$('#idcliente option:selected').val();


    window.open(base_url+'ComplementosPago/excel_comp/'+f1+'/'+f2+'/'+tipoempresa+'/'+sucursal+'/'+cfdi+'/'+ecomp+'/'+idcliente, '_blank');
    setTimeout(function(){ 
        window.open(base_url+'ComplementosPago/comp_zip/'+f1+'/'+f2+'/'+tipoempresa+'/'+sucursal+'/'+cfdi+'/'+ecomp+'/'+idcliente, '_blank');
    }, 2000);
    
    $('.btn_ex1').css('display','block');
    $('.btn_ex2').css('display','none');
    $('.input_fecha_e').css('display','none');
    $('#fe1').val('');
    $('#fe2').val('');
}
function excel_exportalindi(){
    //===================================================
        var facturaslis = $("#tabla_facturacion tbody > tr");
        var DATAa  = [];
        var num_facturas_selected=0;
        facturaslis.each(function(){  
            if ($(this).find("input[class*='facturas_checked']").is(':checked')) {
                num_facturas_selected++;
                item = {};                    
                item ["FIds"]  = $(this).find("input[class*='facturas_checked']").val();
                
                DATAa.push(item);
            }       
            
        });
        INFOa  = new FormData();
        aInfoa   = JSON.stringify(DATAa);
        //console.log(aInfoa);
        if (num_facturas_selected>0) {
            window.open(base_url+'ComplementosPago/excel_comp_indi?facturas='+aInfoa, '_blank');
            setTimeout(function(){ 
                window.open(base_url+'ComplementosPago/comp_zip_indi?facturas='+aInfoa, '_blank');
            }, 2000);
           
        }else{
            Swal.fire({
                    position: "Éxito",icon: "warning",title: "Atención!",text: 'Seleccione una factura como minimo',showConfirmButton: false,timer: 2500
                        });
            $('body').loading('stop');
        }
    //================================================
}
function infoviewerror(idfac){
    $.ajax({
        type:'POST',
        url: base_url+'Facturas/infoviewerrorc',
        data: {
            factura:idfac
        },
        statusCode:{
            404: function(data){
                //Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                //Swal.fire("Error!", "500", "error");
            }
        },
        success:function(response){
            console.log(response);
            if(response!=''){
                var array = $.parseJSON(response);
                console.log(array);
                $.alert({boxWidth: '30%',useBootstrap: false,title: 'Error '+array.TimbrarCFDIResult.CodigoRespuesta,theme: 'bootstrap',content: array.TimbrarCFDIResult.MensajeError+'<br>'+array.TimbrarCFDIResult.MensajeErrorDetallado});    
            }
              
           
        }
    });
}
function verificarcancelacion() {
    
    var DATAvc  = [];
    $(".validarcancelacion").each(function() {
        item = {};
        //item ["idfactura"] = $(this).data('uuid');
        item ["uuid"] = $(this).data('uuid');
        DATAvc.push(item);
    });
    console.log(DATAvc);
    aInfovc   = JSON.stringify(DATAvc);
    console.log(aInfovc);
    console.log('Cancelados:'+aInfovc.length);
    var datos=aInfovc;
    
    $.ajax({
        type:'POST',
        url: base_url+"Timbrar/estatuscancelacionCfdi_all",
        data: {
            datos:aInfovc
        },
        success: function (data){
            console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $.each(array, function(index, item) {
                
                    $("."+item.uuid ).html(item.resultado);
                
            });
        }
    });
}