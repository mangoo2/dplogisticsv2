var base_url=$('#base_url').val();
var v_rid;
var v_sucursal;
var v_rh_empleado;
var v_rh_nom;
var v_rh_pat;
var v_rh_mat;
var v_rh_depto;
var v_rh_cargo;
var v_rh_serviciosa;
var v_rh_vigenciaid;
var v_rh_vigenciatia;
var v_rh_fecnac;
var v_rh_domicilio;
var v_rh_contacto;
var v_rh_identificacion;
var v_rh_obs;
var v_rh_foto;
var v_rhl_tipo;
var v_rhl_sueldo;
var v_rhl_bonos;
var v_rhl_altaimss;
var v_rhl_nss;
var v_rhl_conini;
var v_rhl_confin;
var v_rhl_horario;
var v_rhb_banco;
var v_rhb_titular;
var v_rhb_cuenta;
var v_rhb_plastico;
var v_rhb_clabe;

$(document).ready(function() {
	    /*
	    $("#table_list").DataTable({
	    	'columnDefs' : [ { 'visible': false, 'targets': [2,5,6,7,8,9,10,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27] } ],
	        dom: 'Bfrtip',
        	buttons: [
            	'excel', 'pdf', 'print'
        	]
	    });
	    */
	    $("#table_list").DataTable({
	    	'columnDefs' : [ { 'visible': false, 'targets': [2,5,6,7,8,9,10,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27] } ],
	        dom: 'Bfrtip',
        	buttons: [
        		{
        			extend: 'excel',
        			text: 'Excel <i class="fa fa-file-excel-o"></i>',
        			messageTop: 'Empleados dados de alta',
        			title: 'Recursos Humanos',
        			autoFilter: true,
        			exportOptions: {
                    	columns: [ 0, 1,2,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27 ]
                	}
        		}
        		
        	]
	    });
	    $('.dt-buttons').addClass('btn-group');
  		$('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mb-2');

  		$("#rh_foto").fileinput({
            showCaption: false,
            dropZoneEnabled: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["png","jpg","jpeg","bmp"],
            browseLabel: 'Seleccionar Imagen',
            uploadUrl: base_url+'Coe_rechum/inserupdate',
            inputGroupClass: "input-group-sm",
            maxFilePreviewSize: 5000,
            uploadExtraData: function (previewId, index) {
                var info = {
                            rid:v_rid,
							sucursal:v_sucursal,
							rh_empleado:v_rh_empleado,
							rh_nom:v_rh_nom,
							rh_pat:v_rh_pat,
							rh_mat:v_rh_mat,
							rh_depto:v_rh_depto,
							rh_cargo:v_rh_cargo,
							rh_serviciosa:v_rh_serviciosa,
							rh_vigenciaid:v_rh_vigenciaid,
							rh_vigenciatia:v_rh_vigenciatia,
							rh_fecnac:v_rh_fecnac,
							rh_domicilio:v_rh_domicilio,
							rh_contacto:v_rh_contacto,
							rh_identificacion:v_rh_identificacion,
							rh_obs:v_rh_obs,
							rhl_tipo:v_rhl_tipo,
							rhl_sueldo:v_rhl_sueldo,
							rhl_bonos:v_rhl_bonos,
							rhl_altaimss:v_rhl_altaimss,
							rhl_nss:v_rhl_nss,
							rhl_conini:v_rhl_conini,
							rhl_confin:v_rhl_confin,
							rhl_horario:v_rhl_horario,
							rhb_banco:v_rhb_banco,
							rhb_titular:v_rhb_titular,
							rhb_cuenta:v_rhb_cuenta,
							rhb_plastico:v_rhb_plastico,
							rhb_clabe:v_rhb_clabe,

                        };
                return info;
            }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
        }).on('filebatchuploadsuccess', function(event, files, extra) {
          location.reload();
          //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        });  
});
function editar(id){
	$('#modalform').modal({backdrop: 'static', keyboard: false});
	if(id>0){
		$('#modalform .modal-title').html('Actualizar requisición');
		$('.saveform').html('Actualizar');
		$('#rid').val(id);
		var sucursal = $('.edit_form_'+id).data('sucursal');
		$('#sucursal').val(sucursal);
		var rh_empleado = $('.edit_form_'+id).data('rh_empleado');
		$('#rh_empleado').val(rh_empleado);
		var rh_nom = $('.edit_form_'+id).data('rh_nom');
		$('#rh_nom').val(rh_nom);
		var rh_pat = $('.edit_form_'+id).data('rh_pat');
		$('#rh_pat').val(rh_pat);
		var rh_mat = $('.edit_form_'+id).data('rh_mat');
		$('#rh_mat').val(rh_mat);
		var rh_depto = $('.edit_form_'+id).data('rh_depto');
		$('#rh_depto').val(rh_depto);
		var rh_cargo = $('.edit_form_'+id).data('rh_cargo');
		$('#rh_cargo').val(rh_cargo);
		var rh_serviciosa = $('.edit_form_'+id).data('rh_serviciosa');
		$('#rh_serviciosa').val(rh_serviciosa);
		var rh_vigenciaid = $('.edit_form_'+id).data('rh_vigenciaid');
		$('#rh_vigenciaid').val(rh_vigenciaid);
		var rh_vigenciatia = $('.edit_form_'+id).data('rh_vigenciatia');
		$('#rh_vigenciatia').val(rh_vigenciatia);
		var rh_fecnac = $('.edit_form_'+id).data('rh_fecnac');
		$('#rh_fecnac').val(rh_fecnac);
		var rh_domicilio = $('.edit_form_'+id).data('rh_domicilio');
		$('#rh_domicilio').val(rh_domicilio);
		var rh_contacto = $('.edit_form_'+id).data('rh_contacto');
		$('#rh_contacto').val(rh_contacto);
		var rh_identificacion = $('.edit_form_'+id).data('rh_identificacion');
		$('#rh_identificacion').val(rh_identificacion);
		var rh_obs = $('.edit_form_'+id).data('rh_obs');
		$('#rh_obs').val(rh_obs);
		var rh_foto = $('.edit_form_'+id).data('rh_foto');
		$('#rh_foto').val(rh_foto);
		var rhl_tipo = $('.edit_form_'+id).data('rhl_tipo');
		$('#rhl_tipo').val(rhl_tipo);
		var rhl_sueldo = $('.edit_form_'+id).data('rhl_sueldo');
		$('#rhl_sueldo').val(rhl_sueldo);
		var rhl_bonos = $('.edit_form_'+id).data('rhl_bonos');
		$('#rhl_bonos').val(rhl_bonos);
		var rhl_altaimss = $('.edit_form_'+id).data('rhl_altaimss');
		$('#rhl_altaimss').val(rhl_altaimss);
		var rhl_nss = $('.edit_form_'+id).data('rhl_nss');
		$('#rhl_nss').val(rhl_nss);
		var rhl_conini = $('.edit_form_'+id).data('rhl_conini');
		$('#rhl_conini').val(rhl_conini);
		var rhl_confin = $('.edit_form_'+id).data('rhl_confin');
		$('#rhl_confin').val(rhl_confin);
		var rhl_horario = $('.edit_form_'+id).data('rhl_horario');
		$('#rhl_horario').val(rhl_horario);
		var rhb_banco = $('.edit_form_'+id).data('rhb_banco');
		$('#rhb_banco').val(rhb_banco);
		var rhb_titular = $('.edit_form_'+id).data('rhb_titular');
		$('#rhb_titular').val(rhb_titular);
		var rhb_cuenta = $('.edit_form_'+id).data('rhb_cuenta');
		$('#rhb_cuenta').val(rhb_cuenta);
		var rhb_plastico = $('.edit_form_'+id).data('rhb_plastico');
		$('#rhb_plastico').val(rhb_plastico);
		var rhb_clabe = $('.edit_form_'+id).data('rhb_clabe');
		$('#rhb_clabe').val(rhb_clabe);
	}else{
		$('#modalform .modal-title').html('Agregar requisición');
		$('.saveform').html('Guardar');
		$('#rid').val(0);
		$('#rh_empleado').val('');$('#rh_nom').val('');$('#rh_pat').val('');$('#rh_mat').val('');$('#rh_depto').val('');$('#rh_cargo').val('');$('#rh_serviciosa').val('');$('#rh_vigenciaid').val('');$('#rh_vigenciatia').val('');$('#rh_fecnac').val('');$('#rh_domicilio').val('');$('#rh_contacto').val('');$('#rh_identificacion').val('');$('#rh_obs').val('');$('#rhl_tipo').val('');$('#rhl_sueldo').val('');$('#rhl_bonos').val('');$('#rhl_altaimss').val('');$('#rhl_nss').val('');$('#rhl_conini').val('');$('#rhl_confin').val('');$('#rhl_horario').val('');$('#rhb_banco').val('');$('#rhb_titular').val('');$('#rhb_cuenta').val('');$('#rhb_plastico').val('');$('#rhb_clabe').val('');
	}
}

function saveform() {
    v_rid = $('#rid').val();
	v_sucursal = $('#sucursal').val();
	v_rh_empleado = $('#rh_empleado').val();
	v_rh_nom = $('#rh_nom').val();
	v_rh_pat = $('#rh_pat').val();
	v_rh_mat = $('#rh_mat').val();
	v_rh_depto = $('#rh_depto').val();
	v_rh_cargo = $('#rh_cargo').val();
	v_rh_serviciosa = $('#rh_serviciosa').val();
	v_rh_vigenciaid = $('#rh_vigenciaid').val();
	v_rh_vigenciatia = $('#rh_vigenciatia').val();
	v_rh_fecnac = $('#rh_fecnac').val();
	v_rh_domicilio = $('#rh_domicilio').val();
	v_rh_contacto = $('#rh_contacto').val();
	v_rh_identificacion = $('#rh_identificacion').val();
	v_rh_obs = $('#rh_obs').val();
	v_rhl_tipo = $('#rhl_tipo').val();
	v_rhl_sueldo = $('#rhl_sueldo').val();
	v_rhl_bonos = $('#rhl_bonos').val();
	v_rhl_altaimss = $('#rhl_altaimss').val();
	v_rhl_nss = $('#rhl_nss').val();
	v_rhl_conini = $('#rhl_conini').val();
	v_rhl_confin = $('#rhl_confin').val();
	v_rhl_horario = $('#rhl_horario').val();
	v_rhb_banco = $('#rhb_banco').val();
	v_rhb_titular = $('#rhb_titular').val();
	v_rhb_cuenta = $('#rhb_cuenta').val();
	v_rhb_plastico = $('#rhb_plastico').val();
	v_rhb_clabe = $('#rhb_clabe').val();

        var varform=$('#form_datos');
        if(varform.valid()){
             $('#rh_foto').fileinput('upload');    
        }
}
function deleterh(id){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma borrar él registro',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Coe_rechum/delete",
                        data: {
                            rid:id
                        },
                        success: function (response){
                            toastr["success"](" eliminada", "Hecho"); 
                            location.reload();                         
                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}