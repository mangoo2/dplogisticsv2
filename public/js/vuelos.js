var base_url = $('#base_url').val();
var id_regaux=0;
$(document).ready(function($) {
	$("#documento").fileinput({
            
            showUpload: true,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["xlsx"],
            browseLabel: 'Seleccionar documento',
            uploadUrl: base_url+'Vuelos/inserupdate',
            inputGroupClass: "input-group-sm",
            maxFilePreviewSize: 5000,
            preferIconicPreview: true,
	        previewFileIconSettings: { // configure your icon file extensions
	            'doc': '<i class="fas fa-file-word text-primary"></i>',
	            'xls': '<i class="fas fa-file-excel text-success"></i>',
	            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
	            'ppt': '<i class="fas fa-file-powerpoint text-danger"></i>',
	            'pdf': '<i class="fas fa-file-pdf text-danger"></i>',
	            'zip': '<i class="fas fa-file-archive text-muted"></i>',
	            'htm': '<i class="fas fa-file-code text-info"></i>',
	            'txt': '<i class="fas fa-file-alt text-info"></i>',
	            'mov': '<i class="fas fa-file-video text-warning"></i>',
	            'mp3': '<i class="fas fa-file-audio text-warning"></i>',
	            // note for these file types below no extension determination logic 
	            // has been configured (the keys itself will be used as extensions)
	            'jpg': '<i class="fas fa-file-image text-danger"></i>', 
	            'gif': '<i class="fas fa-file-image text-muted"></i>', 
	            'png': '<i class="fas fa-file-image text-primary"></i>'    
	        },
            uploadExtraData: function (previewId, index) {
                var info = {
                            rid:0

                        };
                return info;
            }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
    }).on('filebatchuploadsuccess', function(event, files, extra) {
    		setTimeout(function(){ 
    			$('#calendario').fullCalendar( 'refetchEvents' );
    		}, 2000);
          //location.reload();
          //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
    }); 
    
    
    $('#calendario').fullCalendar({
	    defaultView: 'month',
	    header: {
	      left: 'prev,next today',
	      center: 'title',
	      right: 'month,agendaWeek,agendaDay,listDay',
	    },
        
	    events: function(start, end, timezone, callback) {
	        $.ajax({
	            url: base_url+'Vuelos/listado',
	            dataType: 'json',
	            type:'POST',
	            data: {
	                start: start.unix(),
	                end: end.unix(),
	                origen:$('#select_o option:selected').val(),
					destino:$('#select_d option:selected').val(),
					vuelo:$('#novuelo').val(),
	            },
	            success: function(doc) {
	            	
	                var datos=doc;
	                var events = [];
	                datos.forEach(function(r){
	                	var colorc=''; 
	                	if(r.activo==1){
                            colorc='#ff0000';
	                	}else if(r.activo==2){	
                            colorc='gainsboro';
	                	}else{
                            colorc='';
	                	}
	                	events.push({
		                        id: r.id,
		                        title: r.origen+' '+r.destino,
		                        start: r.fecha+'T'+r.h_salida,
		                        end: r.fecha+'T'+r.h_llegada,
		                        vorigen: r.origen,
		                        vdestino: r.destino,
		                        vfecha: r.fecha,
		                        vsalida: r.h_salida,
		                        vllegada: r.h_llegada,
		                        aero: r.aerolinea,
		                        vuelo: r.vuelo,
		                        color:colorc,
		                        id_reg: r.id,
		                        activoc: r.activo,
		                });
	                });
	                callback(events);
	            }
	        });
	    },
	    eventRender: function(event, element){ 
	    	//element.dblclick(function() {
	    	element.click(function() {
	    		//obternerdatosservicio(event.asignacion,event.tipo,event.calfecha);
	    		$('.vorigen').html(event.vorigen);
				$('.vdestino').html(event.vdestino);
				$('.vaerolinea').html(event.aero);
				$('.vvuelo').html(event.vuelo);
				$('.vsalida').html(event.vsalida);
				$('.vllegada').html(event.vllegada);
				$('.vfecha').html(event.vfecha);
                $('#id_reg').val(event.id_reg);
				$('#salida_edit').val(event.vsalida);
				$('#llegada_edit').val(event.vllegada);
				$('#fecha_edit').val(event.vfecha);
                id_regaux=event.id_reg;
	    		$('#modalinfocalendario').modal({backdrop: 'static', keyboard: false});
	    		if(event.activoc==1){
	    			$('.btn_canl').css('display','block');
	    			$('.btn_edit').css('display','block');
	    		}else{
	    			$('.btn_canl').css('display','none');
	    			$('.btn_edit').css('display','none');
	    		}
	    	});
	    	
	    }
	});
	
	//------------------------------------------
	
});
function cargarvuelos(){
	$('#documento').fileinput('clear');
	$('#modalcargavuelos').modal({backdrop: 'static', keyboard: false});
}
function updatecalendar(){
	$('#calendario').fullCalendar( 'refetchEvents' );
}

function editar_reg(){
	$('#modalinfocalendario').modal('hide');
	$('#modalinfocalendario_editar').modal({backdrop: 'static', keyboard: false});
}

function editar_vuelo(){
	var id_r=$('#id_reg').val();
	var salida=$('#salida_edit').val();
	var llegada=$('#llegada_edit').val();
	var fecha=$('#fecha_edit').val();
	$.ajax({
        type:'POST',
        url: base_url+"Vuelos/edit_vuelo",
        data: {
            id_r:id_r,
			salida:salida,
			llegada:llegada,
			fecha:fecha,
        },
        success: function (data){
        	Swal.fire(
			  'Hecho!',
			  'Guardada Correctamente',
			  'success'
			);
        	$('#calendario').fullCalendar( 'refetchEvents' );
        	$('#modalinfocalendario_editar').modal('hide');
        }
    });
}

function cancelar_reg(){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma cancelar vuelo',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Vuelos/cancelar_vuelo",
                        data: {
                            id:id_regaux
                        },
                        success: function (response){
                        	$('#modalinfocalendario').modal('hide');
							Swal.fire(
							  'Hecho!',
							  'Registro cancelado',
							  'success'
							);
							$('#calendario').fullCalendar( 'refetchEvents' );                       
                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}


function eliminar_reg(){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma eliminar vuelo',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Vuelos/eliminar_vuelo",
                        data: {
                            id:id_regaux
                        },
                        success: function (response){
                        	$('#modalinfocalendario').modal('hide');
							Swal.fire(
							  'Hecho!',
							  'Registro eliminado',
							  'success'
							);
							$('#calendario').fullCalendar( 'refetchEvents' );                       
                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
