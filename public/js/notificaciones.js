var base_url=$('#base_url').val();
$(document).ready(function($) {
	
});
function aceptacionsolicitud(idcotiza,numcotiza,suc){
	$.confirm({
        boxWidth: '41%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Se a solicitado Edicion de cotizacion '+numcotiza+', desea aceptar?',
        type: 'red',
        typeAnimated: true,
        buttons:{
        	documento: function () {
                window.open(base_url+'Documentar/fdg/'+numcotiza, '_blank');
            },
            'Permitir': function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Documentar/editsol",
                    data: {
                        coti:idcotiza,
                        codigo:numcotiza,
                        sol:2,
                        suc:suc
                    },
                    success: function (response){
                            toastr["success"]("Se realizo la solicitud", "Hecho!");
                            loadtable();
                    },
                    error: function(response){
                        toastr["error"]("No se pudo realizar la solicitud", "Error"); 
                         
                    }
                });
            },
            'No Permitir': function () {
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Documentar/editsol",
                    data: {
                        coti:idcotiza,
                        codigo:numcotiza,
                        sol:0
                    },
                    success: function (response){
                            toastr["success"]("Se realizo la solicitud", "Hecho!");
                            
                    },
                    error: function(response){
                        toastr["error"]("No se pudo realizar la solicitud", "Error"); 
                         
                    }
                });

            },
            cancelar: function () {

            },
            

        }
    });
}
function deletecotsoladm(numcoti,numcotiza,suc){
    var motivo=$('.edit_cotizacion_'+numcoti).data('motivo');
    var descrip=$('.edit_cotizacion_'+numcoti).data('descrip');
    $.confirm({
        boxWidth: '41%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma borrar el registro del documento '+numcotiza+'...<br>Motivo: '+motivo+'<br>Descripción: '+descrip,
        type: 'red',
        typeAnimated: true,
        buttons:{
            documento: function () {
                window.open(base_url+'Documentar/fdg/'+numcotiza, '_blank');
            },
            Confirmar: function (){
                var idcotiza=$('.idcotiza_'+numcoti).data('idcotiza');
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Documentar/deletecot",
                        data: {
                            coti:numcoti,
                            codigo:numcotiza,
                            suc:suc
                        },
                        success: function (response){
                                toastr["success"]("Información Eliminada", "Hecho!");
                        },
                        error: function(response){
                            toastr["error"]("No se pudo guardar la información", "Error"); 
                             
                        }
                    });
            },
            'No eliminacion': function () {
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Documentar/soldeletecot",
                        data: {
                            coti:numcoti,
                            motselect:0,
                            descrip:'',
                            codigo:numcotiza,
                            sol:0,
                            suc:suc
                        },
                        success: function (response){
                                toastr["success"]("Información Eliminada", "Hecho!");
                        },
                        error: function(response){
                            toastr["error"]("No se pudo guardar la información", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () {
                
            }
        }
    });
}