var base_url = $('#base_url').val();
var rid;
$(document).ready(function($) {
	obtenerinfo();
	$('#saveform').click(function(event) {
		$( "#saveform" ).prop( "disabled", true );
        setTimeout(function(){ 
             $("#saveform" ).prop( "disabled", false );
        }, 5000);

		var formulario =$('#form_datos');
		var validform=formulario.valid();
		if(validform){
			//==================================
				var table_ser_sers = $("#table_ser_sers tbody > tr");
            	var DATAtss  = [];
		        table_ser_sers.each(function(){  
		        	if ($(this).find("input[id*='servicio-especifico']").is(':checked')) {
		        		item = {};                    
		            	item ["ser_sers1"]  = $(this).find("input[id*='servicio-especifico']").val();
		            	item ["ser_sers2"]  = $(this).find("input[id*='especificar']").val();
		            	DATAtss.push(item);
		        	}       
		        });
		        aInfotss   = JSON.stringify(DATAtss);
			//==================================
				var table_ser_per_pres = $("#table_ser_per_pres tbody > tr");
            	var DATAtspp  = [];
		        table_ser_per_pres.each(function(){  
		        	if ($(this).find("input[id*='per_pre_ser']").is(':checked')) {
		        		item = {};                    
		            	item  = $(this).find("input[id*='per_pre_ser']").val();
		            	DATAtspp.push(item);
		        	}       
		        });
		        aInfotspp   = JSON.stringify(DATAtspp);
			//==================================
				var table_ser_unis = $("#table_ser_unis tbody > tr");
            	var DATAtsu  = [];
		        table_ser_unis.each(function(){  
		        	if ($(this).find("input[id*='ser_sers']").is(':checked')) {
		        		item = {};                    
		            	item  = $(this).find("input[id*='ser_sers']").val();
		            	DATAtsu.push(item);
		        	}       
		        });
		        aInfotsu   = JSON.stringify(DATAtsu);
			//==================================
			//==================================
			//==================================
			var datos = formulario.serialize()+'&ser_sers_a='+aInfotss+'&ser_pers_a='+aInfotspp+'&ser_unis_a='+aInfotsu;
			$.ajax({
                  type:'POST',
                  url: base_url+'index.php/Servicios_Rye/updateinser_ser_coe',
                  data: datos,
                  success:function(data){
                      
                    toastr["success"]("Información Guardada", "Hecho!")      
                    setTimeout(function(){ 
                    		var codigo=$('#ser').val();
                        window.location.href = base_url+"index.php/Servicios_Rye/cad_fin/"+codigo; 
                        //javascript:history.back();
                    }, 3000);
                      
                      
                  },
                  error: function(response){
                  	console.log(response);
                  	toastr["error"]("No se pudo guardar la información", "Error");
                  }

              });
		}
	});

});
function obtenerinfo(){
	var codigo=$('#ser').val();
	$.ajax({
        type:'POST',
        url: base_url+'index.php/Servicios_Rye/get_cad_ini',
        data: {
          	codigo:codigo
        },
        success:function(data){
            //console.log(data);
            var array = $.parseJSON(data);
           	console.log(array);
           	$('#ser_fecini').val(array.datos.ser_fecini);
           	$('#ser_guia').val(array.datos.ser_guia);
           	$('#ser_fdp').val(array.datos.ser_fdp);
           	$('#ser_fdpdes').val(array.datos.ser_fdpdes);
           	$('#ser_vuelo').val(array.datos.ser_vuelo);
           	$('#ser_piezas').val(array.datos.ser_piezas);
           	$('#ser_enc').val(array.datos.ser_enc);
           	$('#ser_vuelo').val(array.datos.ser_vuelo);
           	$('#ser_piezas').val(array.datos.ser_piezas);
           	$('#ser_enc').val(array.datos.ser_enc);
           	$('#ser_aeronave').val(array.datos.ser_aeronave);
           	$('#ser_kilos').val(array.datos.ser_kilos);
           	$('#ser_firs').val(array.datos.ser_firs);
           	$('#ser_origen').val(array.datos.ser_origen);
           	$('#ser_clicorp').val(array.datos.ser_clicorp);
           	$('#ser_obs').val(array.datos.ser_obs);
           	$('#ser_destino').val(array.datos.ser_destino);
           	$('#ser_clis').val(array.datos.ser_clis);
           	array.ser_unis_a.forEach(function(element) {
           		console.log(element);
            	$('input[name=ser_sers][value='+element+']').attr('checked',true);
          	});
          	array.ser_pers_a.forEach(function(element) {
          		console.log(element);
            	$("input[name=per_pre_ser][value='"+element+"']").attr('checked',true);
          	});
          	array.ser_sers_a.forEach(function(element) {
          		console.log(element);
          		var valementos=element.split("|");
          		var valor1=valementos[0].replace(' ', "");
          		//console.log(valementos[0]+'--');
          		//console.log(valementos[1]);
            	$("input[name=servicio-especifico][value='"+valor1+"']").attr('checked',true);
            	$('.servicio-especifico_'+valor1+' #especificar').val(valementos[1]);
          	});
        }
    });
}