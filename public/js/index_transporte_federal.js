var base_url =$('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table_list').DataTable();
	loadtable();
});
function loadtable(){
	table.destroy();
	table=$("#table_list").DataTable({
    		stateSave: true,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		        "url": base_url+"Transporte_federal/get_listado",
		        type: "post",
		        error: function(){
		           $("#table").css("display","none");
		        }
		    },
		    "columns": [
		        {"data":"id"},
		        {"data":"dpl"},
		        {"data":"nombre_aseguradora"},
				{"data":"num_poliza_seguro"},
				{"data":"num_permiso_sct"},
				{"data":"configuracion_vhicular"},
				{"data":"placa_vehiculo_motor"},
				{"data":"anio_modelo_vihiculo_motor"},
				{"data":"subtipo_remolque"},
				{"data":"placa_remolque"},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="";
		                	html+='<div class="divbuttonstable">';
		                    html+='<a href="'+base_url+'Transporte_federal/registro/'+row.id+'" type="button" class="btn btn-success mr-1 mb-1"><i class="fa fa-edit"></i></a>';
		                    html+='<a onclick="eliminar_reg('+row.id+')" type="button" class="btn btn-danger mr-1 mb-1"><i class="fa fa-trash"></i></a>';
		                    html+='</div>';
		            return html;
		            }
		        },
		    ],
    		"order": [[ 0, "asc" ]],
    		"lengthMenu": [[10,25, 50, 100], [10,25, 50, 100]],
  	}).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
   	});
}


function eliminar_reg(id_reg){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma eliminar registro',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Transporte_federal/delete",
                        data: {
                            id:id_reg
                        },
                        success: function (response){
							Swal.fire(
							  'Hecho!',
							  'Registro eliminado',
							  'success'
							);         
							loadtable();            
                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error");  
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}