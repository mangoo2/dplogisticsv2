var base_url=$('#base_url').val();
$(document).ready(function($) {
	
});
function ver(id){
	$('.body_mdlRevVer').html('<img src="'+base_url+'public/plugins/loader/loader2.gif" class="imgloader">');
	var sucu = $('.ver_'+id).data('sucu');
	var equ = $('.ver_'+id).data('equ');
	var rev = $('.ver_'+id).data('rev');
	$('#mdlRevVer').modal({backdrop: 'static', keyboard: false});
	$.ajax({
            type:'POST',
            url: base_url+"index.php/Coe_brdu/ver",
            data: {
            	sucu:sucu,
				equ:equ,
				rev:rev,
            },
            success: function (response){
            	console.log(response);
               var array = $.parseJSON(response);  
               $('#mdlRevVer_Label').html(array.titulo);
               $('.body_mdlRevVer').html(array.contenido);      
            },
            error: function(response){
                toastr["error"]("No se pudo procesar", "Error"); 
                 
            }
    });
}