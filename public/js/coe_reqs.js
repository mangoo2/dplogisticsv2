var base_url =$('#base_url').val();
$(document).ready(function($) {
	
});
function editar(id){
	$('#modalform').modal({backdrop: 'static', keyboard: false});
	if (id==0) {
		$('#modalform .modal-title').html('Agregar requisición');
		$('.saveform').html('Guardar');
		$('#rid').val(id);
		$('#req_nombre').val('');
		$('#req_playera').val('');
		$('#req_camisa').val('');
		$('#req_botaind').val('');
		$('#req_impermeable').val('');
		$('#req_botaseg').val('');
		$('#req_faja').val('');
		$('#req_chalecoref').val('');
		$('#req_guantes').val('');
		$('#req_tapones').val('');
		$('#req_obs').val('');
	}else{
		$('#modalform .modal-title').html('Actualizar requisición');
		$('.saveform').html('Actualizar');
		$('#rid').val(id);
		var req_nombre = $('.edit_res_'+id).data('req_nombre');
		$('#req_nombre').val(req_nombre);
		var req_tipo = $('.edit_res_'+id).data('req_tipo');
		$('#req_tipo').val(req_tipo);
		var req_playera = $('.edit_res_'+id).data('req_playera');
		$('#req_playera').val(req_playera);
		var req_camisa = $('.edit_res_'+id).data('req_camisa');
		$('#req_camisa').val(req_camisa);
		var req_botaind = $('.edit_res_'+id).data('req_botaind');
		$('#req_botaind').val(req_botaind);
		var req_impermeable = $('.edit_res_'+id).data('req_impermeable');
		$('#req_impermeable').val(req_impermeable);
		var req_botaseg = $('.edit_res_'+id).data('req_botaseg');
		$('#req_botaseg').val(req_botaseg);
		var req_faja = $('.edit_res_'+id).data('req_faja');
		$('#req_faja').val(req_faja);
		var req_chalecoref = $('.edit_res_'+id).data('req_chalecoref');
		$('#req_chalecoref').val(req_chalecoref);
		var req_guantes = $('.edit_res_'+id).data('req_guantes');
		$('#req_guantes').val(req_guantes);
		var req_tapones = $('.edit_res_'+id).data('req_tapones');
		$('#req_tapones').val(req_tapones);
		var req_obs = $('.edit_res_'+id).data('req_obs');
		$('#req_obs').val(req_obs);
	}
}
function editar_s(id,tipo){
	if(tipo==0){
		editar(id);
	}else{
		$('#modalform2').modal({backdrop: 'static', keyboard: false});
		$('#rid2').val(id);
		var req_estatus = $('.edit_res_'+id).data('req_estatus');
		$('#info_req_estatus').val(req_estatus);

		var req_nombre = $('.edit_res_'+id).data('req_nombre');
		$('#info_req_nombre').html(req_nombre);
		var req_tipo = $('.edit_res_'+id).data('req_tipo');
		$('#info_req_tipo').html(req_tipo);
		var req_playera = $('.edit_res_'+id).data('req_playera');
		$('#info_req_playera').html(req_playera);
		var req_camisa = $('.edit_res_'+id).data('req_camisa');
		$('#info_req_camisa').html(req_camisa);
		var req_botaind = $('.edit_res_'+id).data('req_botaind');
		$('#info_req_botaind').html(req_botaind);
		var req_impermeable = $('.edit_res_'+id).data('req_impermeable');
		$('#info_req_impermeable').html(req_impermeable);
		var req_botaseg = $('.edit_res_'+id).data('req_botaseg');
		$('#info_req_botaseg').html(req_botaseg);
		var req_faja = $('.edit_res_'+id).data('req_faja');
		$('#info_req_faja').html(req_faja);
		var req_chalecoref = $('.edit_res_'+id).data('req_chalecoref');
		$('#info_req_chalecoref').html(req_chalecoref);
		var req_guantes = $('.edit_res_'+id).data('req_guantes');
		$('#info_req_guantes').html(req_guantes);
		var req_tapones = $('.edit_res_'+id).data('req_tapones');
		$('#info_req_tapones').html(req_tapones);
		var req_obs = $('.edit_res_'+id).data('req_obs');
		$('#info_req_obs').html(req_obs);
	}
}
function saveform(){
	 var form = $('#form_datos');
    var datos = form.serialize();
    if (form.valid()) {
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Coe_reqs/insertupdate",
            data: datos,
            statusCode:{
                404: function(data){
                    toastr.error('404','Error!');
                },
                500: function(){ 
                    toastr.error('500','Error!');
                }
            },
            success:function(data){  
                toastr.success('Se Registro Correctamente','Hecho!');
                setTimeout(function(){ 
                	location.reload(); 
            	}, 3000);
            }
        });
    }
}
function deletereq(id){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma borrar él registro',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Coe_reqs/delete",
                        data: {
                            rid:id
                        },
                        success: function (response){
                            toastr["success"](" eliminada", "Hecho"); 
                            location.reload();                         
                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error"); 
                             
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function updatestatus(){
	var form = $('#form_datos2');
    var datos = form.serialize();
    if (form.valid()) {
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Coe_reqs/update",
            data: datos,
            statusCode:{
                404: function(data){
                    toastr.error('404','Error!');
                },
                500: function(){ 
                    toastr.error('500','Error!');
                }
            },
            success:function(data){  
                toastr.success('Se Registro Correctamente','Hecho!');
                setTimeout(function(){ 
                	location.reload(); 
            	}, 3000);
            }
        });
    }
}