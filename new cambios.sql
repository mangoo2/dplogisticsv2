ALTER TABLE `cli_clis` ADD `RegimenFiscalReceptor` VARCHAR(3) NULL DEFAULT NULL AFTER `cli_des`;

ALTER TABLE `cli_clis` ADD `nombrefiscal` VARCHAR(300) NULL DEFAULT NULL AFTER `RegimenFiscalReceptor`;

ALTER TABLE `cli_facts` ADD `RegimenFiscalReceptor` VARCHAR(4) NULL DEFAULT NULL AFTER `fac_email`;

ALTER TABLE `doc_cotiza` ADD `tipo_pago_credito` INT NULL DEFAULT NULL AFTER `cot_cambio`;

ALTER TABLE `doc_cotiza` ADD `banco_efectivo` TEXT NULL DEFAULT NULL AFTER `cot_fecpag`, ADD `banco_credito` TEXT NULL DEFAULT NULL AFTER `banco_efectivo`;

ALTER TABLE `suc_cors` ADD `cor_tarjeta` DECIMAL(10,2) NULL DEFAULT NULL AFTER `cor_credito`;

ALTER TABLE `doc_cotiza` ADD `facturar` TINYINT(1) NOT NULL DEFAULT '0' AFTER `cot_feccap`;

ALTER TABLE `f_configuraciones` ADD `timbresvigentes` INT NULL DEFAULT NULL AFTER `retenciones`, ADD `tv_inicio` DATE NULL DEFAULT NULL AFTER `timbresvigentes`, ADD `tv_fin` DATE NULL DEFAULT NULL AFTER `tv_inicio`;

-----------------------------
ALTER TABLE `doc_cotiza` ADD `sol_editar` TINYINT NOT NULL DEFAULT '0' AFTER `facturar`;
-------------------------------
DROP TABLE IF EXISTS `tiposervicio`;
CREATE TABLE IF NOT EXISTS `tiposervicio` (
  `idtipo` int(11) NOT NULL AUTO_INCREMENT,
  `coecad` int(11) NOT NULL COMMENT '1 coe 2 cad 3 ambas',
  `name` varchar(300) NOT NULL,
  `unidad` varchar(100) NOT NULL,
  `servicio` varchar(100) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idtipo`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tiposervicio`
--

INSERT INTO `tiposervicio` (`idtipo`, `coecad`, `name`, `unidad`, `servicio`, `activo`) VALUES
(1, 3, 'servicio de logística para carga, mensajería, paquetes para envío aéreo', 'E48', '81141601', 1),
(2, 1, 'Servicio de logística de rampa', 'E48', '81141601', 1),
(3, 1, 'Servicio de logística de arrastre', 'E48', '81141601', 1),
(4, 3, 'Servicio de logística de emplayado ', 'E48', '81141601', 1),
(5, 3, 'Servicio de logística de empaquetado ', 'E48', '81141601', 1),
(6, 2, 'Servicio de logística de entrega a detalle ', 'E48', '78102200', 1),
(7, 3, 'Servicio de logística de almacenaje ', 'E48', '81141601', 1),
(8, 3, 'Servicio de logística de uso de equipo ', 'E48', '81141601', 1);
-----------------------------------------------
ALTER TABLE `doc_cotiza` ADD `tiposervicio` INT NULL DEFAULT NULL AFTER `cotiza`;
----------------------------------------------
ALTER TABLE `cli_facts` CHANGE `fac_nrs` `fac_nrs` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `fac_dir` `fac_dir` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `fac_col` `fac_col` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `fac_mc` `fac_mc` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `fac_est` `fac_est` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `fac_cod` `fac_cod` INT(5) NULL DEFAULT NULL, CHANGE `fac_la1` `fac_la1` VARCHAR(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `fac_te1` `fac_te1` VARCHAR(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
----------------------------------------
ALTER TABLE `doc_cotiza` CHANGE `cot_fdp` `cot_fdp` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
----------------------------------------
ALTER TABLE `f_facturas_servicios` ADD `NoIdentificacion` VARCHAR(100) NULL DEFAULT NULL AFTER `FacturasId`;

---------------------
CREATE VIEW 
vista_brdu_revs AS
SELECT * FROM `brdu_revs` ORDER BY `brdu_revs`.`rid` DESC LIMIT 1000

CREATE VIEW 
vista_guias AS
SELECT * FROM `guias` ORDER BY rid DESC LIMIT 1000

ALTER TABLE `guia_pags` CHANGE `forma` `forma` VARCHAR(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

CREATE VIEW 
vista_ser_coe AS
SELECT * FROM `ser_coe` ORDER BY ser_fec DESC LIMIT 1000

CREATE VIEW 
vista_ser_cad AS
SELECT * FROM `ser_cad` ORDER BY ser_fec DESC LIMIT 1000
-------------------------------------
ALTER TABLE `doc_cotiza` ADD `tipo_cancelacion` TINYINT NULL DEFAULT NULL AFTER `sol_delete`;
ALTER TABLE `doc_cotiza` ADD `descrip_can` TEXT NULL DEFAULT NULL AFTER `tipo_cancelacion`;
--2023-05-10---------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS `sucursal`;
CREATE TABLE IF NOT EXISTS `sucursal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `conserie` tinyint(1) NOT NULL DEFAULT 0,
  `serie` varchar(3) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sucursal`
--

INSERT INTO `sucursal` (`id`, `nombre`, `conserie`, `serie`, `activo`) VALUES
(1, 'HUATULCO', 1, 'B', 1),
(2, 'HUX-APTO', 0, '', 1),
(3, 'MERIDA', 1, 'D', 1),
(4, 'MID-APTO', 0, '', 1),
(5, 'MOSTRADOR', 0, '', 1),
(6, 'OAXACA', 1, 'A', 1),
(7, 'OAX-APTO', 0, '', 1),
(8, 'PBC-APTO', 0, '', 1),
(9, 'PRINCIPAL', 0, '', 1),
(10, 'VERACRUZ', 1, 'E', 1),
(11, 'VER-APTO', 0, '', 1),
(12, 'VILLAHERMOSA', 1, 'C', 1),
(13, 'VSA-APTO', 0, '', 1);


ALTER TABLE `f_facturas` ADD `sucursal` INT NULL DEFAULT '0' AFTER `tipofac`;

ALTER TABLE `f_complementopago` ADD `ConfiguracionesId` INT NULL DEFAULT NULL AFTER `activo`;

ALTER TABLE `f_complementopago` ADD `f_relacion` TINYINT(1) NOT NULL DEFAULT '0' AFTER `ConfiguracionesId`, ADD `f_r_tipo` VARCHAR(2) NULL DEFAULT NULL AFTER `f_relacion`, ADD `f_r_uuid` VARCHAR(60) NULL DEFAULT NULL AFTER `f_r_tipo`;

ALTER TABLE `f_complementopago_documento` ADD `serie` VARCHAR(10) NULL DEFAULT NULL AFTER `IdDocumento`;
ALTER TABLE `f_complementopago_documento` ADD `folio` VARCHAR(10) NULL DEFAULT NULL AFTER `serie`;
